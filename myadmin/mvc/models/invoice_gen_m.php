<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Invoice_gen_m extends MY_Model {

	protected $_table_name = 'invoice';

	protected $_primary_key = 'invoiceID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "invoiceID desc";

	function __construct() {

		parent::__construct();

	}

function AddInvoice($studentID,$array,$adminID){

$this->db->where('classesID',$array['classesID']);

$query =  $this->db->get('classes');

$ObjectsclassesFee = $query->row();



$classesFee = $ObjectsclassesFee->fee;

$yearID = $array['yearID'];

$semesterId = $array['semesterId'];



if ($array['student_status']=='fresher') {



$loopFee  = $this->db->query("SELECT * FROM  feetype WHERE  feetype IN('Registration_Fee','Prospectus') and adminID = '$adminID' ");



} elseif($array['student_status']=='lateral'){

	$loopFee  = $this->db->query("SELECT * FROM  feetype WHERE feetype IN('Registration_Fee', 'lateral_entry','Prospectus') and adminID = '$adminID' ");



}else{

  

  $loopFee  = $this->db->query("SELECT * FROM  feetype WHERE feetype IN('Registration_Fee', 'Toc','Prospectus') and adminID = '$adminID' ");



}

  // $loopFee  = $this->db->query("SELECT * FROM  feetype WHERE FIND_IN_SET('$yearID',yearIDs)");



$feeCount = 0; 



foreach ($loopFee->result() as $key => $value) {

 

$feeCount  += $value->amount;



}	

$data =  array(

'adminID'=>$adminID,

'studentID'=>$studentID,

'amount'=>$feeCount+$classesFee,

'paidamount'=>0,

'date'=>date('Y-m-d')

	);



$this->db->insert('invoice',$data);

$invoiceID = $this->db->insert_id();



foreach ($loopFee->result() as $key => $value) {

	

$data = array(

"invoiceID"=>$invoiceID,

"feeName"=>$value->feetype,

"amount"=>$value->amount

	);



$this->db->insert('feeApplied',$data);



}





}





function addInvoicewithpayment($array){
$this->db->insert('invoice',$array);
}
function updateInvoicewithpayment($array,$studentID){
$this->db->where('studentID',$studentID);	
$this->db->update('invoice',$array);
}





function get_student($studentID){



	$this->db->where('studentID',$studentID);

    $query = 	$this->db->get('student');

  return $query->row();

}







function get_feeApplied($invoiceID){



	$this->db->where('invoiceID',$invoiceID);

    $query = $this->db->get('feeApplied');

    return $query->result();

}





function get_invoice(){



		$this->db->select('*');

		$this->db->from('invoice');

		$this->db->join('student', 'student.studentID = invoice.studentID', 'LEFT');

		$query = $this->db->get();

		return $query->result();



}



function get_invoice_byID($id){



		$this->db->select('*');

		$this->db->from('invoice');

		$this->db->join('student', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('invoice.studentID',$id);

		$query = $this->db->get();

		return $query->result();



}





function get_invoice_byAdmin($adminID){



		$this->db->select('invoice.*,student.*,invoice.status as invoiceStatus');

		$this->db->from('invoice');

		$this->db->join('student', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('invoice.adminID',$adminID);

		$this->db->where('student.status',1);

		$usertype = $this->session->userdata("usertype");

		if ($usertype == "Teacher") {



		 $loginuserID = $this->session->userdata("loginuserID");

         $this->db->where('student.counsellor',$loginuserID);		

        }

		$query = $this->db->get();	

		return $query->result();



}



function get_invoice_byID_byAdmin($id,$adminID){



		$this->db->select('*');

		$this->db->from('invoice');

		$this->db->join('student', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('invoice.studentID',$id);

		$this->db->where('invoice.adminID',$adminID);

		$query = $this->db->get();

		return $query->result();



}





function get_payment($invoiceID,$year){

$this->db->where('yearsOrSemester',$year);
$this->db->where('invoiceID',$invoiceID);
$query 	= $this->db->get('payment');

return $query->result();

}

function get_payment_registration($invoiceID,$feetype){
if($feetype=='lateral'){
$this->db->where('feeType','lateral');
}else{
$this->db->where('feeType','registration_fee');	
}
$this->db->where('invoiceID',$invoiceID);
$query 	= $this->db->get('payment');

return $query->result();	
}

function get_payment_hstl($invoiceID,$month,$monthYear,$feeType){
$this->db->where('feeType',$feeType);
$this->db->where('paymentmonth',$month);
$this->db->where('paymentyear',$monthYear);
$this->db->where('invoiceID',$invoiceID);
$query 	= $this->db->get('payment');

return $query->result();

}

function TotalFee($studentID,$adminID){

	$this->db->select('amount');

	$this->db->from('invoice');

	$this->db->where('studentID',$studentID);

	$this->db->where('adminID',$adminID);

	$query   =  $this->db->get();



	return $query->row(	);

}

function get_invoice_for_student($studentID){



		$this->db->select('invoice.*,student.*,invoice.status as invoiceStatus');

		$this->db->from('invoice');

		$this->db->join('student', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('invoice.studentID',$studentID);

		$query = $this->db->get();

		return $query->result();



}





}