<div class="">
 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="fa fa-table"> </i> Passbook</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">

                       <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                      <li class="active">Passbook<!-- <?=$this->lang->line('menu_teacher')?> --></li>
                        </ol>
                    </div>
            </div>
    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">

<div class="well">
                     <div class="row">
                        <div class="col-sm-3">
                           <button class="btn btn-success btn-xs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> Print </button>
                           <!-- <a href="http://101.53.153.40/blackboard/invoice/print_preview/37" class='btn btn-success btn-xs' style='text-decoration: none;' role='button' target='_blank'><i class='fa fa-file'></i> PDF Preview</a> -->
               

                        </div>
                        <div class="col-sm-9">
                    
                           <div class="col-sm-3">Select Academic Partner</div>
                           <div class="col-sm-9">
                              <select class="form-control" name="semesterId" onchange="invoice_filter_url($(this).val())" id="yearSemesterID" value="">
                                <option>Select</option>
                                <?php 
                                foreach ($teachers as $key => $value) {
                                 ?>
                                <option value="<?php echo $value->teacherID ?>" <?php if($this->uri->segment(3)==$value->teacherID) {echo "selected";} ?>><?php echo $value->name ?></option>
                               <?php } ?>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>

        <?php 
        $usertype = $this->session->userdata("usertype");
        $id =  $this->uri->segment(3);
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"    || $usertype == "Support" || $usertype == "Super_A_P" ){
            if($id){
            ?>
            <div class="col-sm-12">
              <div class="col-sm-6">
              </div>
              <div class="col-sm-6">
   <div class="pull-right">
      <div class="btn-group">
   <!-- <a href="<?php echo base_url('teacher/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span>Add Academic Partner</a> -->
      </div>
   </div>
              </div>
              <div class="clearfix"></div>
              <div class="breakdownl_tbl">
                <style type="text/css">
                  <style>
.verticalTable th, td {
  border: 1px solid black !important;
  border-collapse: collapse;
}
.verticalTable th, td {
  padding: 5px;
  text-align: left;
}
</style>
                </style>
                <div class="col-sm-4">
                 <?php if($teacher){ ?> 
                <table class="verticalTable table-bordered" >
                  <tr>
                    <td>Organisation</td>
                    <td><?php echo $teacher->organization_name ?></td>
                  </tr>
                  <tr>
                    <td>Academic Name</td>
                    <td><?php echo $teacher->name ?></td>
                  </tr>
                  <tr>
                    <td>Available balance</td>
                    <td><?php
                    if($total_balance){
                     echo  $total_balance->balance;
                   }else {echo "0";}
                      ?></td>
                  </tr>
                </table>
              <?php } ?>
              </div>
                <div class="col-sm-4">
                <div class="">
                  <form action="<?php echo base_url() ?>teacher/passbook/<?php echo $this->uri->segment(3) ?>/1" method= 'post'>
                    <div class="form-group">
                    <label>Add Deposit</label>
                    <input type="text" class="form-control"   name="credit">
                    <input type="submit" value="Add Deposit" class="btn btn-success add-btn">
                    </div>

                  </form>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="">
                  <form action="<?php echo base_url() ?>teacher/passbook/<?php echo $this->uri->segment(3) ?>/2" method= 'post'>
                    <div class="form-group">
                    <label>Total Balance</label>
                    <input type="text" class="form-control"  value="<?php if($total_balance){ echo $total_balance->balance;}else{echo "0";} ?>" name="with_drawl">
                    <input type="submit" value="Withdrawal" class="btn btn-success add-btn">
                    </div>

                  </form>
                </div>
              </div>
              </div>
                <div id="hide-table">
                  <?php if($teachers_pasbook){ ?>
                    <table class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                <!-- <th class="col-sm-2"><?=$this->lang->line('teacher_photo')?></th> -->
                                <th class="col-sm-2">Date</th>
                                <th class="col-sm-2">TN.ID</th>
                                <th class="col-sm-2">Paid Students</th>
                                <th>Comment</th>
                                <th class="col-sm-2">Withdrawal</th>
                                <th class="col-sm-2">Deposit</th>
                                <th class="col-sm-2">Balance</th>  
                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                          foreach ($teachers_pasbook as $key => $value) {
                           ?>
                          <tr>
                            <td><?php echo $key+1  ?></td>
                            <td><?php echo date('d-m-Y',strtotime($value->date_time)); ?></td>
                            <td><?php if ($value->debit_credit_status==1) {
                             echo "txnId".$value->creditID;
                            }else{
                             echo "txnId".$value->debitID; 
                            }  ?></td>
                            <td><?php if($value->debit_credit_status==0){?><a href="<?php echo base_url() ?>teacher/paid_student/<?php echo $value->passbookID ?>">View</a><?php } ?></td>
                            <td>Payment Done For 2018 batch</td>
                            <td><?php echo $value->debit ?></td>
                            <td><?php echo $value->credit ?></td>
                            <td><?php echo $value->balance ?></td>
                            <td></td>
                          </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                  <?php } ?>
                </div>
            </div> <!-- col-sm-12 -->
          <?php } ?>
              <?php } ?>
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
</div>
</div>
<script type="text/javascript">
     function invoice_filter_url(val){
      
   window.location.href=base_url+"teacher/passbook/"+val;
   
   }
   
</script>
<script>

  var status = '';

  var id = 0;

  $('.onoffswitch-small-checkbox').click(function() {

      if($(this).prop('checked')) {

          status = 'chacked';

          id = $(this).parent().attr("id");

      } else {

          status = 'unchacked';

          id = $(this).parent().attr("id");

      }



      if((status != '' || status != null) && (id !='')) {

          $.ajax({

              type: 'POST',

              url: "<?=base_url('teacher/active')?>",

              data: "id=" + id + "&status=" + status,

              dataType: "html",

              success: function(data) {

                  if(data == 'Success') {

                      toastr["success"]("Success")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  } else {

                      toastr["error"]("Error")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  }

              }

          });

      }

  }); 

</script>

