<style>
  .AddNewSubjetcsDetails{
    margin-bottom: 10px;
  }
  .margin20{
    margin-bottom: 20px;
  }
</style>
<?php 
              if($this->input->get('type')){
              $type = $_GET['type'];
              }else{
              $type = 'clg';
              }
?>

<div class="">

    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor"><?=$this->lang->line('panel_title')?>/ Syllabus</h3>
        </div>
        <div class="col-md-6 align-self-center">
            <ol class="breadcrumb">
              <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

              <li><a href="<?=base_url("subject/index")?>"><?=$this->lang->line('menu_subject')?></a></li>

              <li class="active"><?=$this->lang->line('menu_add')?> Syllabus</li>
            </ol>
        </div>          
     </div>

    <!-- /.box-header -->

    <!-- form start -->

     <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

            <div class="col-sm-12">
                 <div class="row">
                       <ul class="nav nav-tabs">

  <li  <?php if($type=='pdf'){ echo "class='active'"; } ?> ><a href="<?php echo base_url() ?>syllabus/add_syllabus/<?php echo $this->uri->segment(3) ?>?type=pdf">Add Pdf</a></li>
  <li <?php if($type=='unit'){ echo "class='active'"; } ?>><a href="<?php echo base_url() ?>syllabus/add_syllabus/<?php echo $this->uri->segment(3) ?>?type=unit">Add Units</a></li>
</ul>
                     </div>
               
                 

<style type="text/css">
  .AddNewSyllabus{
    box-shadow: 0px 0px 0px 1px;
    padding: 22px;
  }
</style>
<?php if($type=="unit"){ ?>
<div id="appendUnitDetails">
  
 <?php foreach ($subjects as $key => $value) { ?>
  
    <div class='AddNewSubjetcsDetails AddNewSyllabus'>
      <h3>Unit <?php echo $value->unit_code ?></h3>
   <div class='col-sm-6'>
      <div class=''>
         <input type='text' class='form-control' id='subjectNameClear' onBlur='EditSubject(this, "subject", "<?php $value->unit_name ?>",$(this).val())'  placeholder='Unit Name' name='unit_name' value='<?php echo set_value('unit_name' , $value->unit_name) ?>' >
      </div>
   </div>
   <div class='col-sm-6'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,"subject_code","<?php $value->unit_code?>",$(this).val())' placeholder='Unit Code' name='unit_code' value="<?php echo set_value('unit_code',$value->unit_code) ?>" >
      </div>
   </div>
   <div class="col-sm-12">&nbsp;</div>
 <?php  $tid  = explode(',', $value->tid);
   $tn  = explode(',', $value->tn);
for ($i=0; $i <count($tid) ; $i++) { ?>
   <div class='col-sm-3'>

      <div class=''>
         <!-- <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,\"subject_code\","<?php $value->topic_name ?>",$(this).val())' placeholder='Unit Code' name='topic_name' value='<?php echo set_value('topic_name',$tn[$i]) ?>' > -->
         <div class="control-group input-group" style="margin-top:10px">
            <input type="text" name="topic_name[]" id="topic_name[]" class="form-control" onBlur='EditSubject(this,\"subject_code\","<?php $value->topic_name ?>",$(this).val())' placeholder='Unit Code' name='topic_name' value='<?php echo set_value('topic_name',$tn[$i]) ?>'>
            <div class="input-group-btn"> 
              <button class="btn btn-danger" type="button">
                <i class="glyphicon glyphicon-remove"></i></button>
            </div>
          </div>
      </div>
      
   </div>
   
<?php } ?>
   
   
   <div class='clearfix'></div>
</div>
<?php } ?>

</div>

<div class="AddNewSyllabus">
<div class="inner_part">
<h3>Add Units</h3>

<div class="copy hide">
  <div class="control-group input-group" style="margin-top:10px">
    <input type="text" name="addmore[]" class="form-control" placeholder="Enter Name Here">
    <div class="input-group-btn"> 
      <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
    </div>
  </div>
</div>
<form method="post" id="form_unit">
<div class="col-sm-6">
<input type="text" class="form-control" id="unit_name" placeholder="Unit Name" name="unit_name" value="<?=set_value('unit_name')?>" >
</div>
<div class="col-sm-6">
<input type="text" class="form-control" id="unit_code" placeholder="Unit Number" name="unit_number" value="<?=set_value('unit_number')?>" >
</div>
<input type="hidden" id="subjectID" name="subjectID" value="<?= $this->uri->segment(3)?>">
<div class="col-sm-12">&nbsp;</div>
<div class="col-sm-12">
  <div class="input-group control-group after-add-more">
          <input type="text" name="addmore" id="addmore[]" class="form-control" placeholder="Enter Name Here">
          <div class="input-group-btn"> 
            <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>

         <!-- Copy Fields -->
        <div class="copy hide">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="text" name="addmore[]" id="addmore[]" class="form-control" placeholder="Enter Name Here">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove" type="button">
                <i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>
        </div>
</div>

<div class="col-sm-12">
  <input type="button" onclick="AddUnit()" class="btn btn-success add-btn update-btn" value="Submit">
</div>
</form> 
<!-- <div class="col-sm-6">
<button>   </button>
</div> -->
  <div class="clearfix"></div>
</div>                 
              
                    </div>

               <?php } ?>     

               <style type="text/css">

.pdf_sylnus{
  padding:10px 10px 10px 10px;
}
.biit_dd{
  margin:3%;
}
               </style>
              <?php if($type=="pdf"){ ?> 
              <div class="pdf_sylnus" id="afx_khjk">
<?php foreach ($pdf_slybus as $key => $value) {
?>
              <button class="btn btn-danger" type="button" onclick='delete_pdf_sylabus("<?php echo $value->id  ?>")'><?php echo $value->pdf ?>
                <i class="glyphicon glyphicon-remove"></i></button><span style="    background: #000;
    padding: 5px;
    color: #fff;
    border-radius: 11px;"><a target="_blank" href="<?php echo base_url() ?>uploads/syllabus/<?php echo $value->pdf ?>">View</a></span>
<?php } ?>
<div > 
</div>
              </div> 
          <div class="biit_dd">    
      <div class="col-md-4"><input type="file" id="multiFiles" name="files[]" multiple="multiple"/></div>
      <div class="col-md-4">
        <button id="upload_file_multiple" class="btn btn-success add-btn update-btn">Upload</button> 
      </div> 
    </div>
  <?php } ?>

            </div> 

        </div>

    </div>

</div>

</div>

</div>

<script type="text/javascript">
           
                $('#upload_file_multiple').on('click', function () {
                  var id =  "<?php echo $this->uri->segment(3) ?>";
                    var form_data = new FormData();
                    var ins = document.getElementById('multiFiles').files.length;
                    for (var x = 0; x < ins; x++) {
                        form_data.append("files[]", document.getElementById('multiFiles').files[x]);
                    }
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/UploadFile/"+id, // point to server-side PHP script 
                        dataType: 'text', // what to expect back from the PHP script
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#afx_khjk').html(response); // display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_khjk').html(response); // display error response from the PHP script
                        }
                    });
                });
      
        </script>
        <script type="text/javascript">
           
                function delete_pdf_sylabus(id){
                

                  var confirmation = confirm("are you sure you want to remove the item?");

                  var subjectID =  "<?php echo $this->uri->segment(3) ?>";
                  if(confirmation){
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/delete_pdf_sylabus/"+id, // point to 
                        data:{subjectID:subjectID},
                        type: 'post',
                        success: function (response) {
                          console.log(response);
                            $('#afx_khjk').html(response); // display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_khjk').html(response); // display error response from the PHP script
                        }
                    });
            
            }
          }
      
        </script>



<style type="text/css">
button, input, optgroup, select, textarea{    
  
  color: inherit;
}
</style>

<script type="text/javascript"> 
function AddUnit(){

  $.ajax({
    type: "POST", 
  url:base_url+'AjaxController/AddUnit',
  data:$('#form_unit').serialize(),
  success: function(response) {
$('#appendUnitDetails').html(response);


        }

  });


}

</script>