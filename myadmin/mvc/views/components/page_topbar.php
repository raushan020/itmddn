  <style type="text/css">
    
.navbar-default .dropdown-menu.notify-drop {
  min-width: 330px;
  background-color: #fff;
  min-height: 360px;
  max-height: 360px;
}
.navbar-default .dropdown-menu.notify-drop .notify-drop-title {
  border-bottom: 1px solid #e2e2e2;
  padding: 5px 15px 10px 15px;
}
.navbar-default .dropdown-menu.notify-drop .drop-content {
  min-height: 280px;
  max-height: 280px;
  overflow-y: scroll;
}
.navbar-default .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar-track
{
  background-color: #F5F5F5;
}

.navbar-default .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar
{
  width: 8px;
  background-color: #F5F5F5;
}

.navbar-default .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar-thumb
{
  background-color: #ccc;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li {
  border-bottom: 1px solid #e2e2e2;
  padding: 10px 0px 5px 0px;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li:nth-child(2n+0) {
  background-color: #fafafa;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li:after {
  content: "";
  clear: both;
  display: block;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li:hover {
  background-color: #fcfcfc;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li:last-child {
  border-bottom: none;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li .notify-img {
  float: left;
  display: inline-block;
  width: 45px;
  height: 45px;
  margin: 0px 0px 8px 0px;
}
.navbar-default .dropdown-menu.notify-drop .allRead {
  margin-right: 7px;
}
.navbar-default .dropdown-menu.notify-drop .rIcon {
  float: right;
  color: #999;
}
.navbar-default .dropdown-menu.notify-drop .rIcon:hover {
  color: #333;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li a {
  font-size: 12px;
  font-weight: normal;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li {
  font-weight: bold;
  font-size: 11px;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li hr {
  margin: 5px 0;
  width: 70%;
  border-color: #e2e2e2;
}
.navbar-default .dropdown-menu.notify-drop .drop-content .pd-l0 {
  padding-left: 0;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li p {
  font-size: 11px;
  color: #666;
  font-weight: normal;
  margin: 3px 0;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li p.time {
  font-size: 10px;
  font-weight: 600;
  top: -6px;
  margin: 8px 0px 0px 0px;
  padding: 0px 3px;
  border: 1px solid #e2e2e2;
  position: relative;
  background-image: linear-gradient(#fff,#f2f2f2);
  display: inline-block;
  border-radius: 2px;
  color: #B97745;
}
.navbar-default .dropdown-menu.notify-drop .drop-content > li p.time:hover {
  background-image: linear-gradient(#fff,#fff);
}
.navbar-default .dropdown-menu.notify-drop .notify-drop-footer {
  border-top: 1px solid #e2e2e2;
  bottom: 0;
  position: relative;
  padding: 8px 15px;
}
.navbar-default .dropdown-menu.notify-drop .notify-drop-footer a {
  color: #777;
  text-decoration: none;
}
.navbar-default .dropdown-menu.notify-drop .notify-drop-footer a:hover {
  color: #333;
}
  </style>

<style type="text/css">

    #suggesstion-box{

        left: 40px;

        background: #f5f5f5;

        text-transform: capitalize

        margin-left: 14px;

        overflow-y: scroll;

    z-index: 99999999999999;

    height: 400px;

    display:none;

}

.selectSearchList{            

    padding: 10px;

    margin: 5px;

    border-bottom: 1px solid #ccc;

}

.paddingUl{

    padding: 10px;

}

.Bottom_line_notice a{

background: #3c8dbc !important;

}

</style>
<style type="text/css">
    .bell{
  display:black;
  width: 40px;
  height: 20px;
  font-size: 20px;
  margin:5px auto 0;
  color: red;
  -webkit-animation: ring 2s .3s ease-in-out infinite;
  -webkit-transform-origin: 50% 4px;
  -moz-animation: ring 4s .7s ease-in-out infinite;
  -moz-transform-origin: 50% 4px;
  animation: ring 4s .7s ease-in-out infinite;
  transform-origin: 50% 4px;
}

@-webkit-keyframes ring {
  0% { -webkit-transform: rotateZ(0); }
  1% { -webkit-transform: rotateZ(30deg); }
  3% { -webkit-transform: rotateZ(-28deg); }
  5% { -webkit-transform: rotateZ(34deg); }
  7% { -webkit-transform: rotateZ(-32deg); }
  9% { -webkit-transform: rotateZ(30deg); }
  11% { -webkit-transform: rotateZ(-28deg); }
  13% { -webkit-transform: rotateZ(26deg); }
  15% { -webkit-transform: rotateZ(-24deg); }
  17% { -webkit-transform: rotateZ(22deg); }
  19% { -webkit-transform: rotateZ(-20deg); }
  21% { -webkit-transform: rotateZ(18deg); }
  23% { -webkit-transform: rotateZ(-16deg); }
  25% { -webkit-transform: rotateZ(14deg); }
  27% { -webkit-transform: rotateZ(-12deg); }
  29% { -webkit-transform: rotateZ(10deg); }
  31% { -webkit-transform: rotateZ(-8deg); }
  33% { -webkit-transform: rotateZ(6deg); }
  35% { -webkit-transform: rotateZ(-4deg); }
  37% { -webkit-transform: rotateZ(2deg); }
  39% { -webkit-transform: rotateZ(-1deg); }
  41% { -webkit-transform: rotateZ(1deg); }

  43% { -webkit-transform: rotateZ(0); }
  100% { -webkit-transform: rotateZ(0); }
}

@-moz-keyframes ring {
  0% { -moz-transform: rotate(0); }
  1% { -moz-transform: rotate(30deg); }
  3% { -moz-transform: rotate(-28deg); }
  5% { -moz-transform: rotate(34deg); }
  7% { -moz-transform: rotate(-32deg); }
  9% { -moz-transform: rotate(30deg); }
  11% { -moz-transform: rotate(-28deg); }
  13% { -moz-transform: rotate(26deg); }
  15% { -moz-transform: rotate(-24deg); }
  17% { -moz-transform: rotate(22deg); }
  19% { -moz-transform: rotate(-20deg); }
  21% { -moz-transform: rotate(18deg); }
  23% { -moz-transform: rotate(-16deg); }
  25% { -moz-transform: rotate(14deg); }
  27% { -moz-transform: rotate(-12deg); }
  29% { -moz-transform: rotate(10deg); }
  31% { -moz-transform: rotate(-8deg); }
  33% { -moz-transform: rotate(6deg); }
  35% { -moz-transform: rotate(-4deg); }
  37% { -moz-transform: rotate(2deg); }
  39% { -moz-transform: rotate(-1deg); }
  41% { -moz-transform: rotate(1deg); }

  43% { -moz-transform: rotate(0); }
  100% { -moz-transform: rotate(0); }
}

@keyframes ring {
  0% { transform: rotate(0); }
  1% { transform: rotate(30deg); }
  3% { transform: rotate(-28deg); }
  5% { transform: rotate(34deg); }
  7% { transform: rotate(-32deg); }
  9% { transform: rotate(30deg); }
  11% { transform: rotate(-28deg); }
  13% { transform: rotate(26deg); }
  15% { transform: rotate(-24deg); }
  17% { transform: rotate(22deg); }
  19% { transform: rotate(-20deg); }
  21% { transform: rotate(18deg); }
  23% { transform: rotate(-16deg); }
  25% { transform: rotate(14deg); }
  27% { transform: rotate(-12deg); }
  29% { transform: rotate(10deg); }
  31% { transform: rotate(-8deg); }
  33% { transform: rotate(6deg); }
  35% { transform: rotate(-4deg); }
  37% { transform: rotate(2deg); }
  39% { transform: rotate(-1deg); }
  41% { transform: rotate(1deg); }

  43% { transform: rotate(0); }
  100% { transform: rotate(0); }
}
</style>

        

        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

            </button>
<!-- 
                <a class="navbar-brand" ><?php if(count($siteinfos)) { ?>

                    <img src="<?php echo base_url() ?>uploads/images/<?php echo $siteinfos->photo  ?>">

                <?php } ?></a> -->

        </div>

                <!-- /.navbar-header -->

                

            <ul class="nav navbar-top-links navbar-left header-search-form hidden-xs">

                <li><a class="menu-brand" id="menu-toggle"><i class="fa fa-bars" style="background-color: white;"></i></a></li>

                <!-- <li class="hidden-sm hidden-xs">

                    <div class="header-search-form input-group">

                        <span class="input-group-addon"><span class="ti-search"></span></span>

                        <input type="text" id="search-box" onkeyup="search_bar($(this).val())" class="form-control" placeholder="Search & Enter">

                    </div>

                <div id="suggesstion-box" class="suggesstion "></div>

                </li> -->

            </ul>



            <ul class="nav navbar-top-links navbar-right">

                <?php 

if($this->session->userdata("usertype")=="Student") 

{

 ?>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="hideNotice()"><i class="bell fa fa-bell "></i>
           <?php if ($notices_icon_count>0) { ?>

                            <span class="email-notify noti-count" id="icon_notification"><?php echo $notices_icon_count ?></span>

                        <?php } ?>
                      </a>

          <ul class="dropdown-menu notify-drop">
            <?php if ($notices_read_count>0) { ?>
            <div class="notify-drop-title">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">Notification <span class="notification-label bg-success">New <?php echo $notices_read_count ?></span></div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title=""><i class="fa fa-dot-circle-o"></i></a></div>
              </div>
            </div> -->
            <!-- end notify title -->
            <!-- notify content -->
            

            <!-- <div class="drop-content">
              <?php if (count($notice_list_icon)>0){ ?>

                          <?php foreach ($notice_list_icon as $key => $value) {

                            ?>
              <li>
                <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><i class="bell fa fa-bell "></i></div></div>
                <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href=""><b><?php echo $value->title ?></b></a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                
                <hr>
                <a href=""><?php echo substr($value->notice, 0,40)  ?>..</a></p>
                </div>
              </li>
               <?php } ?>
              
            </div>
            <div class="notify-drop-footer text-center">
              <a href="<?php echo base_url() ?>notice/index"> View All Notifications</a>
            </div>
            
              <?php } ?>
          </ul>
        <?php } ?>
        </li> -->

                    


                    <li class="dropdown">

                        <a class="dropdown-toggle notification-show mrgtop" onclick="hideNotice()" data-toggle="dropdown" href="#">

                            <i class="bell fa fa-bell"></i>

                             <?php if ($notices_icon_count>0) { ?>

                            <span class="email-notify noti-count" id="icon_notification"><?php echo $notices_icon_count ?></span>

                        <?php } ?>

                        </a>

                        <ul class="dropdown-menu dropdown-messages right-swip">

                            <?php if ($notices_read_count>0) { ?>
 
                            <li class="external">

                                <h3><span class="bold" style="color:black">Notifications</span></h3>

                                <span class="notification-label bg-success">New <?php echo $notices_read_count ?></span>

                            </li>

                          <?php } ?>

                          <?php if (count($notice_list_icon)>0){ ?>

                          <?php foreach ($notice_list_icon as $key => $value) {

                            ?>

                            <li>

                                <a href="<?php echo base_url() ?>notice/view/<?php echo  $value->noticeID ?>">

                                    <div class="message-apt">

                                    

                                        <div class="message-body">

                                            <strong><i class="bell fa fa-bell" style="color:blue"></i><?php echo $value->title ?></strong>

                                          <!--   <span class="pull-right text-muted">

                                                Just Now

                                            </span> -->

                                            <p><?php echo substr($value->notice, 0,25)  ?>..</p>

                                        </div>

                                    </div>

                                </a>

                            </li>

                        <?php } ?>

                            

                            <li class="Bottom_line_notice">

                                <a class="text-center" style="color:#fff;" href="<?php echo base_url() ?>notice/index">

                                    <strong>Read All Messages</strong>

                                    <i class="fa fa-angle-right"></i>

                                </a>

                            </li>

                        <?php } else { ?>

                         <li>

                        <a href="#">

                                    <div class="message-apt">

                                        <div class="message-body">

                                             <strong>There is no notification</strong>

                                        </div>

                                    </div>

                        </a>

                             

                            </li>

                        <?php } ?>

                        </ul>

         

                    </li>

           <?php } ?>

           

                    <!-- /.dropdown -->

        <li class="dropdown">

                <a class="dropdown-toggle nav-margin10" data-toggle="dropdown" href="#" style="margin-right: 15px;">

                <img src="<?=base_url("uploads/images/super_admin.png"); 

                    ?>" class="img-responsive img-circle"  alt="user" />

        <span>



            <?php



                $name = $this->session->userdata('name');



                    if(strlen($name) > 11) {



                        echo substr($name, 0,11). ".."; 



                            } else {



                                echo $name;



                                    }



            ?>



                <i class="caret"></i>



            </span>   

            </a>

                <div class="dropdown-menu dropdown-user right-swip nav-right-margin20">

                    <div class="top_img nav-right-margin">

                        <div class="profile_pic">

                            <img src="<?=base_url("uploads/images/super_admin.png"); 

                            ?>" class="img-responsive img-circle"  alt="user" />


<!-- 
                    <div class="camera_pic" title="Change Profile Picture">

                        <a href="<?php echo base_url() ?>profile/ChangePicture">

                        <i class="fa fa-camera" aria-hidden="true"></i>

                        </a>

                    </div> -->

                    </div>

                        <div class="content_popup_menu">

                            <h4>

                            <?php

                              $name = $this->session->userdata('name');

                              if(strlen($name) > 11) 
                              {
                                echo substr($name, 0,11). ".."; 
                              } else {
                                echo $name;
                              }

                            ?>

                            </h4>

                            

                            <?php  

                                 if ($this->session->userdata("usertype")=="Student") {

                            ?>

                                <p class="just_top"><?php echo  $data_single_class->classes ?></p>

                            <!-- <?php

                                if (!empty($data_single_sub_courses)) {?>

                                    <p>(<?php echo $data_single_sub_courses->sub_course ?>)</p>

                                    <?php } ?> -->

                                    <?php } ?>

                            </div>

                        </div>

                            <div class="col-md-12 nav-change">

                                <div class="forInlineClass">

                                    <!-- <div class="inlinePopup">

                                    <a class="btn btn-default btn-flat" href="<?=base_url("profile/index")?>"> <?=$this->lang->line("profile")?> 

                                    </a>

                                    </div>

                                    <div class="inlinePopup">

                                    <a class="btn btn-default btn-flat" href="<?=base_url("signin/cpassword")?>"><?=$this->lang->line("change_password")?></a>

                                    </div> -->

                                    <div class="inlinePopup">

                                         <a class="btn btn-default btn-flat" href="<?=base_url("signin/signout?panel=".$this->input->get('panel'))?>">  <?=$this->lang->line("logout")?> 

                                        </a>

                                    </div>

                                </div>

                            </div>

                    </div>

                </li>



        <!-- /.dropdown -->

        </ul>

<script type="text/javascript">

    function hideNotice(){

              $.ajax({

        type: "POST",

        url: "<?php echo base_url() ?>notice/hide_Notice_icon",

        data:'keyword=ss',

        success: function(data){

                $("#icon_notification").hide();

    }

    });  

    }

</script>



<script>

    function search_bar(val){

        $.ajax({

        type: "POST",

        url: "<?php echo base_url() ?>AjaxController/searchSitemap",

        data:'keyword='+val,

            beforeSend: function(){

                $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");

        },

        success: function(data){

            console.log(data);

                $("#suggesstion-box").show();

                $("#suggesstion-box").html(data);

                $("#search-box").css("background","#FFF");

    }

    });

}

function selectSearchList(val) {

$("#search-box").val(val);

$("#suggesstion-box").hide();

}



$(document).bind('click touch', function(event) {

    $('#suggesstion-box').hide();

});







</script>