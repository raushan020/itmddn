<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

        <meta name="description" content="">

        <meta name="author" content="">



        <title><?=$this->lang->line('panel_title')?></title>
<!-- 
        <link rel="SHORTCUT ICON"  /> -->

        <!-- <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" /> -->

        <script>

          var uri =  "<?php echo $this->uri->segment(3) ?>";

          var uri4 =  "<?php echo $this->uri->segment(4) ?>";

          var base_url="<?php echo base_url();?>";

        </script>        

        <?=link_tag('assets/etslabs/css/pignose.calendar.min.css');?>        

        <?=link_tag('assets/etslabs/css/croppie.css');?>

        <!-- Font CSS -->

        <?=link_tag('assets/etslabs/css/all_icons.css');?>

        <?=link_tag('assets/plugins/bootstrap/css/bootstrap.css');?>        

        <?=link_tag('assets/etslabs/css/style.css');?>        

         <?=link_tag('assets/etslabs/css/feedback.css');?>       

        <!-- MetisMenu CSS -->        

        <?=link_tag('assets/plugins/metisMenu/metisMenu.min.css');?>

        <!-- Custom CSS -->   

     <?=link_tag('assets/etslabs/css/weather.css');?>

        <!-- Change Color CSS -->         

        <?=link_tag('assets/etslabs/css/skin/default-skin.css');?>      

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">  

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">            
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
        <?=link_tag('assets/etslabs/css/intlTelInput.css');?>

        <?=link_tag('assets/fonts/font-awesome.css');?>

        <?=link_tag('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css');?>       

        <?=link_tag('assets/etslabs/css/styleExam.css');?> 

        <?=link_tag('assets/etslabs/css/modal-video.min.css');?> 

        <?=link_tag('assets/etslabs/css/login.css');?> 

        <?=link_tag('assets/etslabs/css/introjs.css');?>

        <?=link_tag('assets/timepicker/timepicker.css');?>

        <?=link_tag('assets/etslabs/css/examples.css');?>          

        <?=link_tag('assets/etslabs/css/kavach.min.css');?>

        <?=link_tag('assets/inilabs/EtDesigns/css/jquery.datetimepicker.min.css');?>

        <?=link_tag('assets/etslabs/css/button-style.css');?>

        <!-- <script src="http://vjs.zencdn.net/4.2/video.js"></script> -->

        <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.3.3/video-js.css" rel="stylesheet">

    <style type="text/css">

  .vjs-default-skin { color: #eb1f1f; }

  .vjs-default-skin .vjs-play-progress,

  .vjs-default-skin .vjs-volume-level { background-color: #284c78 }

  .vjs-default-skin .vjs-control-bar,

  .vjs-default-skin .vjs-big-play-button { background: rgba(34,169,189,0.7) }

  .vjs-default-skin .vjs-slider { background: rgba(34,169,189,0.2333333333333333) }

  .vjs-default-skin .vjs-control-bar { font-size: 86% }

</style>

        <script src="<?php echo base_url() ?>assets/webcamjs/MediaStreamRecorder.min.js"></script>

    	  <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>  

        <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>

        <script src="<?php echo base_url('assets/etslabs/js/html2canvas.min.js');?>"></script>
        <!--  -->

        <script src="<?php echo base_url('assets/etslabs/js/basic.js');?>"></script>        

        <script src="<?php echo base_url('assets/etslabs/js/jquery-modal-video.min.js');?>"></script>   

        <script src="https://code.highcharts.com/highcharts.js"></script>

        <script src="https://code.highcharts.com/highcharts-more.js"></script>

        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <script src="https://code.highcharts.com/modules/export-data.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

        <script src="<?php echo base_url('assets/etslabs/js/feedback.js');?>"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/epubjs/dist/epub.min.js"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/annotator/1.2.9/annotator.min.js"></script>      

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/fancy-file-uploader/fancy_fileupload.css" type="text/css" media="all" /> 



        <style type="text/css">

          .ff_fileupload_hidden { display: none; }

        </style>



    </head>

    <body onload="javascript:introJs().start();">

    <?php
      // $URI = "$_SERVER[REQUEST_URI]";
      $uriMain= $this->uri->segment(1).'/'.$this->uri->segment(2); 
    ?>   

    <input type="hidden" name="uri_findOutFrom_urls_jquery" value="<?php echo $uriMain ?>" id="uri_findOutFrom_urls_jquery" >

    <script type="text/javascript">

      var redirecturi =  $('#uri_findOutFrom_urls_jquery').val();

    </script>

<?php $usertype = $this->session->userdata("usertype");

if ($usertype== "Student") {
$activeClass = "";
}else 
{
 $activeClass = ""; 
}

 ?>

    <div id="wrapper" class="<?php echo $activeClass ?>">

    <div class="fakeLoader"></div>

    <?php if ($this->session->flashdata('success')): ?>

    <script type="text/javascript">

     swal(" ", "<?=$this->session->flashdata('success') ?>", "success");

       setTimeout( "$('.swal-overlay').hide();", 3000);

    </script>

    <?php endif ?>

    <?php if ($this->session->flashdata('error')): ?>

    <script type="text/javascript">

      swal("<?=$this->session->flashdata('error') ?>");

       setTimeout( "$('.swal-overlay').hide();", 3000);

    </script>

    <?php endif ?>


<!-- 
    <script type="text/javascript">

      swal("<?=$this->session->flashdata('error_sign') ?>");

       setTimeout( "$('.swal-overlay').hide();", 10000);

    </script> -->

 

    <script type="text/javascript">

      $('side-menu').click(function(){
        $( ".tabs" ).tabs( "option", "active", 2 );
      });

    </script>

