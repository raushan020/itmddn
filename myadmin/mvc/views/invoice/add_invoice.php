<div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="fa fa-rupee"> </i> <?=$this->lang->line('menu_add')?> Invoice </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li class="breadcrumb-item"><i class="fa fa-bullseye"></i> <a href="<?=base_url("dashboard/index")?>"><?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="breadcrumb-item"><a href="<?=base_url()?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>">Billing</a></li>

                <li class="breadcrumb-item active"><?=$this->lang->line('menu_add')?> Invoice</li> </li>

            </ol>

        </div>

     </div>

    <!-- form start -->

    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">

            <div class="col-sm-12">

               <!--  <form class="form-horizontal" autocomplete="off" role="form" name="myForm" method="post" enctype="multipart/form-data" onsubmit="return validateForm()"> -->

                <form class="form-horizontal" autocomplete="off" role="form" novalidate name="myForm" method="post" enctype="multipart/form-data" >

                    <div class="persnolInfo">

                        <p>Field are required with <span class="red-color">*</span></p>
                        
                       

                        <h3  class="border_heading">Add Invoice</h3>

                        <?php 



                            if(form_error('invoiceDate')) 



                                echo "<div class='form-group has-error' >";



                            else     



                                echo "<div class='form-group' >";



                        ?>



                        <label for="name_id" class="col-sm-2 control-label">



                            Invoice Date<span class="red-color">*</span>



                        </label>



                        <div class="col-sm-6">

                             <input type="text" class="form-control datepicker_quiz_data" required placeholder="Invoice Date"  name="invoiceDate" value="<?=set_value('invoiceDate')?>" >


                        </div>



                        <span class="col-sm-4 control-label">



                            <?php echo form_error('invoiceDate'); ?>



                        </span>



                    </div>



                    <?php 



                        if(form_error('invoiceNumber'))



                            echo "<div class='form-group has-error' >";



                        else     



                            echo "<div class='form-group' >";



                    ?>



                    <label for="guardiaName" class="col-sm-2 control-label">



                        Invoice Number<span class="red-color">*</span>



                    </label>



                    <div class="col-sm-6">



                        <div class="select2-wrapper">



                            <input type="text" class="form-control" required placeholder="Invoice Number" id="invoiceNumber" name="invoiceNumber" value="<?=set_value('invoiceNumber')?>" >



                        </div>



                    </div>



                    <span class="col-sm-4 control-label">



                        <?php echo form_error('invoiceNumber'); ?>



                    </span>



                </div>







                <?php 



                    if(form_error('invoiceAmount'))



                        echo "<div class='form-group has-error' >";



                    else     



                        echo "<div class='form-group' >";



                ?>



                <label for="guardiaName" class="col-sm-2 control-label">



                   Invoice Amount<span class="red-color">*</span>



                </label>



                <div class="col-sm-6">



                    <div class="select2-wrapper">



                        <input type="text" class="form-control" required placeholder="Invoice Amount" id="invoiceAmount" name="invoiceAmount" value="<?=set_value('invoiceAmount')?>" >



                    </div>



                </div>



                <span class="col-sm-4 control-label">



                    <?php echo form_error('invoiceAmount'); ?>



                </span>



            </div>





            <?php 



                if(form_error('dob')) 



                    echo "<div class='form-group has-error' >";



                else     



                    echo "<div class='form-group' >";



            ?>



                            <label for="paidAmount" class="col-sm-2 control-label">



                                Paid Amount<span class="red-color">*</span>



                            </label>



                            <div class="col-sm-6">



                                <input type="text" class="form-control" required placeholder="Paid Amount"  name="paidAmount" value="<?=set_value('paidAmount')?>" >



                            </div>



                            <span class="col-sm-4 control-label">

                                <?php echo form_error('paidAmount'); ?>

                            </span>

                        </div>

                            <?php 

                                if(form_error('sex')) 

                                    echo "<div class='form-group has-error' >";

                                else 

                                    echo "<div class='form-group' >";

                            ?>

                            <label for="status" class="col-sm-2 control-label">

                                Status<span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <?php 

                                    echo form_dropdown("status", array('0' => 'Not Paid','1' => 'Paid'), set_value("status"), "id='status' class='form-control' required"); 

                                ?>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('status'); ?>

                            </span>

                        </div>

                        
                    <?php 



                        if(form_error('email')) 



                            echo "<div class='form-group has-error' >";



                        else     



                            echo "<div class='form-group' >";



                    ?>



                    <label for="email" class="col-sm-2 control-label">



                   Attatchment<span class="red-color">*</span>



                    </label>



                    <div class="col-sm-6">

                        <!-- onkeyup="checkemail()" -->
                        <input type="file"  class="form-control" name="userfile" >
                        



                    </div>



                    <span class="col-sm-4 control-label"  id="emailID">



                        <?php echo form_error('email'); ?>



                    </span>



                </div>







                    





              

                    <div class="col-sm-offset-2 ">

                        <input type="submit" class="btn btn-success add-btn" value="Add Invoice" >

                    </div>

                        

                </form>



            </div> <!-- col-sm-8 -->



        </div><!-- row -->

    </div>

</div>

</div>

    </div><!-- Body -->

</div>

