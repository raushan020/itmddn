<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-upload"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
               <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active"><a href="<?=base_url("bulkimport/index")?>"><?=$this->lang->line('menu_import')?></a></li>
            </ol>
        </div>
    </div>

    <!-- /.box-header -->
    <!-- form start -->
     <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-12">
                <?php if($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <h4><strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>.</h4>
                </div>
            <?php endif ?>

       
              
                <form action="<?=base_url('bulkimport/student_bulkimport');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("bulkimport_student")?>
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control student" id="uploadFile" placeholder="Choose File" disabled />
                        </div>
                        
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload studentUpload import-btn" name="csvStudent" data-show-upload="false"
                                       data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>
                        <!-- <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_student.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div> -->
                    </div>
                </form>

                <form action="<?=base_url('bulkimport/exam_bulkimport');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                        Upload Result
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control subject" id="uploadFile" placeholder="Choose File" disabled />
                        </div>
                        
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload subjectUpload import-btn" name="csvExam" data-show-upload="false" data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>
                       <!--  <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_subject.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div> -->
                    </div>
                </form>






                



            </div>
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<div class="row">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-12">
                <h3>Upload Result</h3>
                    <form action="<?=base_url('bulkimport/black_csv');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                        Upload Result Sheet Grade wise<b>(Without Marks)</b>
                        </label>
                                           
                      <div class="col-sm-3 col-xs-4 col-md-2">
                             <select name="yos_filter" class="form-control">
                                 <option value="">Year/Semester</option>
                                 <option value="1st_Semester">1st Semester</option><option value="2nd_Semester">2nd Semester</option><option value="3rd_Semester">3rd Semester</option><option value="4th_Semester">4th Semester</option><option value="5th_Semester">5th Semester</option><option value="6th_Semester">6th Semester</option><option value="7th_Semester">7th Semester</option><option value="8th_Semester">8th Semester</option>
                             </select>
                        </div>
                        <div class="col-sm-3 col-xs-4 col-md-2">
                            <input class="form-control filename" id="filename" name="filename" placeholder="CSV File Name" type="text" required="required" />
                        </div>
                        <div class="col-sm-3 col-xs-4 col-md-2">
                            <input class="form-control subject" id="uploadFile" placeholder="Choose File" disabled />
                        </div>
  
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload subjectUpload import-btn" name="csvStudent" data-show-upload="false" data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>
                       <!--  <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_subject.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div> -->
                    </div>
                </form>



                <form action="<?=base_url('bulkimport/black_csv_two');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">Upload Result Sheet Grade wise<b>(With Marks)</b></label>
                                           
                        <div class="col-sm-3 col-xs-4 col-md-2">
                            <select name="yos_filter2" class="form-control">
                                <option value="">Year/Semester</option>
                                <option value="1st_Semester">1st Semester</option>
                                <option value="2nd_Semester">2nd Semester</option>
                                <option value="3rd_Semester">3rd Semester</option>
                                <option value="4th_Semester">4th Semester</option>
                                <option value="5th_Semester">5th Semester</option>
                                <option value="6th_Semester">6th Semester</option>
                                <option value="7th_Semester">7th Semester</option>
                                <option value="8th_Semester">8th Semester</option>
                            </select>
                        </div>
                        <div class="col-sm-3 col-xs-4 col-md-2">
                            <input class="form-control filename2" id="filename2" name="filename2" placeholder="CSV File Name" type="text" required="required" />
                        </div>
                        <div class="col-sm-3 col-xs-4 col-md-2">
                            <input class="form-control subject2" id="uploadFile2" placeholder="Choose File" disabled />
                        </div>
  
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn2" type="file" class="upload subjectUpload2 import-btn" name="csvStudent2" data-show-upload="false" data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>
                    </div>
                </form>



            </div>
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
<div class="row">
            <div class="card">
            <div class="card-body">

<table class="table table-bordered">
    <thead>
      <tr>
        <th>Csv Name</th>
        <th>Number Of Results</th>
        <th>Year/Semester</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($exam_result as $key => $value) { ?>
      <tr>
        <td><?php echo  $value->csv_name  ?></td>
        <td><?php echo  $value->total_result  ?></td>
        <td><?php echo  $value->yearsOrSemester  ?></td>
        <td>
<a href="<?php echo base_url() ?>bulkimport/delete_exam/<?php echo $value->csv_name ?>" class="btn btn-danger trash btn-xs mrg for_margR" data-placement="top" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to permanently delete all of these exams?')"><i class="fa fa-trash-o"></i></a></td>
      </tr>
  <?php } ?>
    </tbody>
  </table>


                </div>
            </div>
        </div>
<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function() {
        document.getElementById("uploadFile").value = this.value;
    };

    document.getElementById("uploadBtn2").onchange = function() {
        document.getElementById("uploadFile2").value = this.value;
    };

$('.parentUpload').on('change', function() {
  $('.parent').val($(this).val());
});
$('.userUpload').on('change', function() {
  $('.user').val($(this).val());
});
$('.bookUpload').on('change', function() {
  $('.bookImport').val($(this).val());
});
$('.studentUpload').on('change', function() {
  $('.student').val($(this).val());
});


$('.teacherUpload').on('change', function() {
  $('.teacher').val($(this).val());
});

$('.subjectUpload').on('change', function() {
  $('.subject').val($(this).val());
});
</script>

<!-- <script type="text/javascript">
    $(document).ready(function () {
        $("body").on("contextmenu",function(e){
            alert("right click functionality is disabled for this page.");
            return false;
        });        
 });
 </script> -->