 
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="ti ti-blackboard"></i> LMS</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active">lms/pdf</li>
            </ol>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">

                <div class="">
                    <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin")
                   if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
                   $display =  "block";
                   $addmorebutton = "none";
                   $Removemorebutton = "inline-block";
                   }else{
                   $display = "none";
                   $addmorebutton = "inline-block";
                   $Removemorebutton = "none";
                   }
               
                ?>
                
                
            <div class="clearfix"></div>
               
               <!-- <div id="hide-table">

                    <table id="subjectsTablesP" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th ><?=$this->lang->line('slno')?></th>

                                <th ><?=$this->lang->line('subject_code')?></th>

                                <th ><?=$this->lang->line('subject_name')?></th>

                                <th >Semester/Year</th>
                                 <th >PDF'S</th>

                                <?php  if($usertype == "Admin") { ?>

                                <th ><?=$this->lang->line('action')?></th>

                                <?php } ?>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if(count($subjects)) {
                                $i = 1;
                                $j = 1;
                             foreach($subjects as $subject) { ?>

                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">

                                        <?php echo $i; ?>
                                  </td>

                                <td data-title="<?=$this->lang->line('subject_code')?>">

                                        <?php echo $subject->subject_code; ?>

                                </td>

                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php echo $subject->subject; ?>
                                    </td>

                                    <td data-title="Semester/Year">


                                    <?php echo str_replace('_', ' ', $subject->yearsOrSemester);?>

                                    </td>
                            <td data-title="PDF'S">

                            <?php 
                        $this->db->where('subjectID',$subject->subjectID);
                        $query   = $this->db->get('pdf_lms');
                        $countpdf  = $query->result();
                        $j = 1;
                        foreach ($countpdf as $key => $value) {
                            ?>
                           <a target="_blank" href="<?php base_url() ?>assets/pdfs/<?php echo  $value->pdf ?>"><strong style="color:#3c8dbc;">Read</strong> <?php echo "&nbsp;"; ?><img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/pdfstu.png"></a><br> <br>      
                    <?php } ?>
                             </td>

                                    <?php  if($usertype == "Admin") { ?>

                                    <td data-title="<?=$this->lang->line('action')?>">

                                        <?php echo btn_edit('subject/edit/'.$subject->subjectID."/".$set, $this->lang->line('edit')) ?>

                                        <?php echo btn_delete('subject/delete/'.$subject->subjectID."/".$set, $this->lang->line('delete')) ?>

                                    </td>

                                    <?php } ?>

                                </tr>

                            <?php $i++; }} ?>

                        </tbody>

                    </table>

                </div> -->

                <div id="hide-table">
                    <div class="container">                                          
                        <div class="row"> 
                        <div class="col-md-12"> 

<video id="my-player"  class="video-js vjs-default-skin" controls preload="auto" poster="//vjs.zencdn.net/v/oceans.png" data-setup='{}'>
<!--   <source src="<?php echo base_url() ?>uploads/10.mp4" type="video/mp4"></source> -->
  <source src="<?php echo base_url() ?>uploads/<?php echo $this->uri->segment(3) ?>.webm" type="video/webm"></source>
<!--   <source src="<?php echo base_url() ?>uploads/10.ogg" type="video/ogg"></source> -->
</video>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.3.3/video.js"></script>
 <script type="text/javascript">
     var player = videojs('my-player');
 </script>
 <canvas id="canvas" 
        width="750px" height="540px"
        style="display:block;">
</canvas>
<div id="screenShots"></div>

<script type="text/javascript">

    var video = document.getElementById("my-player");
video.addEventListener("loadedmetadata", initScreenshot);
video.addEventListener("playing", startScreenshot);
video.addEventListener("pause", stopScreenshot);
video.addEventListener("ended", stopScreenshot);

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var ssContainer = document.getElementById("screenShots");
var videoHeight, videoWidth;
var drawTimer = null;

function initScreenshot() {
  videoHeight = video.videoHeight; 
  videoWidth = video.videoWidth;
  canvas.width = videoWidth;
  canvas.height = videoHeight;
}

function startScreenshot() {
  if (drawTimer == null) {
    drawTimer = setInterval(grabScreenshot, 1000);
  }
}

function stopScreenshot() {
  if (drawTimer) {
    clearInterval(drawTimer);
    drawTimer = null;
  }
}

function grabScreenshot() {
  ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
  var img = new Image();
  img.src = canvas.toDataURL("image/png");
  img.width = 120;
  ssContainer.appendChild(img);
}
</script>


                           </div>  
                            
                        </div> 
                               
                    </div>
                </div>

                </div>

                



            </div>

        </div>

    </div>

<script type="text/javascript">    
    function pdfCustomize(){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('lms/delete')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
                window.location.href = data;
            }
        });
    }
</script>


<script type="text/javascript">

    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('subject/subject_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {
        var yearSemesterID = $(this).val();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('lms/index')?>",
            data: "yearSemesterID=" + yearSemesterID,
            dataType: "html",
            success: function(data) {
                location.reload();
            }
        });
    });

</script>

<script type="text/javascript">
    function ResetSemesterYear(){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('lms/ResetSemesterYear')?>",
            data:{ResetSesession:'ResetSesession'},
            dataType: "html",
            success: function(data) {
                location.reload();
            }
        });
    }
</script>
