<div class="">
   
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active">Notice </li>
            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
            <div class="">
            <div class="card">
            <div class="card-body">

            <div class="box-body">

                <div class="row">

                    <div class="col-sm-12">
                        <?php

                            $usertype = $this->session->userdata("usertype");

                            if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Teacher" || $usertype == "Super_A_P" || $usertype == "Accountant" || $usertype == "Support" || $usertype == "Academic") {

                        ?>

                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                               <div class="pull-right">
                              <div class="btn-group">
                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>



        <div class="col-sm-12">

            <div class="theme_input_blue">
            <?php 
               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
               $display =  "block";
               $addmorebutton = "none";
               $Removemorebutton = "inline-block";
               }else{
               $display = "none";
               $addmorebutton = "inline-block";
               $Removemorebutton = "none";
               }
               
               ?>
                
                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterNoticeclassesID')), "id='classesID' onchange='CourseSDependent($(this).val())' class='form-control'");
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label"> Sub Courses </label>

                                <div class="">
                                    <select id="subCourseID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                        if ($value->sub_coursesID==$this->session->userdata('FilterNoticesubCourseID')) {
                                            $selected =  "Selected";
                                        }else{
                                          $selected =  "";
                                        }
                                        ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?>                                            
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='yearsOrSemester' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('yearsOrSemester'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterNoticeyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterNoticeyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
         
            <?php }  ?>                      

                                
                <?php  
                    if( $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin" || $usertype == "Academic" || $usertype == "Super_A_P" || $usertype == "Teacher" || $usertype == "Accountant")
                    {
                ?>
                        <div id="hide-tab">
                            <table id = "noticeTables" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>
                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>
                                        <th class="col-sm-4">Course</th>
                                        <th class="col-sm-4">Sub Course</th>
                                        <th class="col-sm-4">Year / Semester</th>
                                        <th class="col-sm-4">Notice</th>
                                        <th class="col-sm-4">Action </th>                                                             
                                    </tr>
                                </thead>
                            </table>
                        </div>
                <?php  
                    }
                    if ( $usertype == "Student") 
                    { 
                ?>
                        <div id="hide-tab">
                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>
                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>
                                        <th class="col-sm-4">Action</th>
                                    </tr>
                                </thead>
                                <tdata>
                                    <?php if(count($notice)) {$i = 1; foreach($notice as $notices) { ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $notices->title; ?></td>
                                        <td><?php echo $notices->date; ?></td>
                                        <td><?php echo substr($notices->notice,0,20); ?></td>
                                        <td><?php echo btn_view('notice/view/'.$notices->noticeID, $this->lang->line('view')); ?></td>

                                    </tr>
                                    <?php $i++; }} ?>
                                </tdata>
                            </table>
                        </div>
                    <?php 
                    } 
                    ?>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
            $.ajax({
                type: 'POST',
                url: "<?=base_url('notice/index')?>",
                data: "classesID=" + classesID,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }
            });
    });

</script>

<script type="text/javascript">

    $('#subCourseID').change(function() {
        $('#noticeTables').DataTable().state.clear();

        var subCourseID = $(this).val();

            $.ajax({
                type: 'POST',
                url: "<?=base_url('notice/index')?>",
                data: "subCourseID=" + subCourseID,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }

            });

    });

</script>

<script type="text/javascript">
    $('#yearSemesterID').change(function() {
        var yearSemesterID = $(this).val();
        $('#noticeTables').DataTable().state.clear();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('notice/index')?>",
            data: "yearSemesterID=" + yearSemesterID,
            dataType: "html",
            success: function(data) {
                 location.reload();
            }
        });
    });
</script>

<script type="text/javascript">
    
    function ResetCourses(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){
    $('#noticeTables').DataTable().state.clear();

            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

</script>
