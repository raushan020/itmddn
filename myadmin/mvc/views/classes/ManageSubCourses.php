<style type="text/css">
  
  .AddNewSubjetcsDetails{
    margin-bottom: 5px;
  }
</style>
  <div class="">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><?=$this->lang->line('add_title')?> </h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("classes/index")?>"></i> <?=$this->lang->line('menu_classes')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_classes')?></li>
        </ol>
      </div>
    </div>

<!-- /.box-header --> 

<!-- form start -->
  <div class="container-fluid">
    <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
    <div class="row">
      <div class="">
              <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">      
    <div class="modal-content" style="margin-top: 54px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Trash</h4>
        </div>
        <div class="modal-body" id="restore_subcourses">
  <table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>SubCourse Name</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        foreach ($sub_courses_trash as $key => $value) { ?>
          <tr>
            <td><?php echo $key+1; ?></td>
            <td><?php echo $value->sub_course  ?></td>
            <td><a onclick="restore_subcourses(<?php echo $value->sub_coursesID ?>)" style="cursor: pointer;">Restore </a></td>
          </tr>
      <?php } ?>
    </tbody>
  </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
            <div class="card">
            <div class="card-body">
    <div class="box-body">


        <div class="row">

<!-- Button trigger modal -->
<div class="trash-button">
  <div class="col-sm-6">
      <a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Trash</a>
  </div>
  <div class="col-sm-6" style="text-align:right;">
     <a href="<?php echo base_url() ?>classes/index" class="btn btn-primary btn-lg">Back</a>
  </div>
  <div class="clearfix"></div>

 
</div>
  

<div id="appendSubcourses">
<?php 
foreach ($sub_courses as $key => $value) {
       echo "<div class='AddNewSubjetcsDetails'>
   <div class='col-sm-3'>
      <div class=''>
         <input type='text' disabled ='disabled' class='form-control' id=''  placeholder='Subject Name' name='subject' value='".set_value('subject' , $value->subCourseCode)."' >
      </div>
   </div>
   <div class='col-sm-3'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubCourse(this,\"subject_code\",".$value->sub_coursesID.",$(this).val())' placeholder='Subject Code' name='subject' value='".set_value('subject',$value->sub_course)."' >
      </div>
   </div>
    <div class='col-sm-3'>
      <div class=''>
      <button class='btn btn-remove btn-danger' type='button'><span class='glyphicon glyphicon-minus' onclick='DeleteSubCourse(".$value->sub_coursesID.")'></span></button>
      </div>
   </div>
   <div class='clearfix'></div>
</div>";
}
?>
</div>
<!-- add subcourses -->

       <div class="form-group fvrduplicate"><div class="fvrclonned">
            <label for="classes" class="col-sm-2 control-label">
                Add Sub Courses 
            </label>

            <div class="col-sm-6">
                <input type="text" class="form-control add" id="subCoursesName" name="subcourse[]" placeholder="Add Sub Course here" value="">
            </div>

            <span><button class="btn btn-success btn-add" type="button" onclick="AddSubCourses()"><span class="glyphicon glyphicon-plus"></span></button></span></div></div>


            </div>
            </div>   
            </div>
            </div>
            </div>
            </div> 
        </div>
<script src="<?php echo base_url('assets/etslabs/js/etsClasses.js'); ?>" type="text/javascript" charset="utf-8" async defer></script>

<style type="text/css">
    .fvrclonned
{
    margin-top: 10px;
}

.glyphicon
{
    font-size: 14px;
}

.trash-button{
    margin-bottom: 2%;
}

</style>