<?php 
$usertype = $this->session->userdata("usertype");
if ($usertype == "ClgAdmin" || $usertype=="Admin" ) 
{
?> 

<style type="text/css">
  a.btn.btn-success.view,a.btn.btn-primary.primary_1,a.btn.btn-info.info_1,a.btn.btn-danger.danger {
padding-top:8px !important;
  }
</style>
<?php } ?>
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-user"></i> <?=$this->lang->line('panel_title')?> </h3>
   </div>
   <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
         <li class="active"><?=$this->lang->line('menu_student')?></li>
      </ol>
   </div>
</div>
<!-- /.box-header -->

<!-- form start -->
<div class="container-fluid">
   <div class="row">
      <div class="card">
         <div class="card-body">
            <!-- new tab bar -->

             <?php 

              $usertype = $this->session->userdata("usertype");
              if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype=="Accountant" || $usertype=="Academic" || $usertype=="Support" || $usertype=="Academic"  || $usertype == "Teacher" || $usertype == "Super_A_P" ){ ?>  
            <div class="col-sm-6 nopading">
               <div class="advance-tab boredr_none">
                  <ul class="nav nav-tabs" role="tablist">
                     <?php 
                        if (empty($this->session->userdata('DraftStudent')) && empty($this->session->userdata('TrashStudent')) && empty($this->session->userdata('ActiveStudent'))) {
                        
                            $active = "active";
                        
                        }else{
                        
                            $active = "";
                        
                        }
                        
                        ?>
                     <!-- <li class="<?php echo $active ?>">
                        <a  aria-expanded="false" onclick="AllStudent()">All <span style="font-size: 11px;">(<?php echo $all_count; ?>)</span></a>
                        
                        </li> -->
                     <li  <?php if ($this->session->userdata('ActiveStudent')){ ?> class="active" <?php  } ?>>
                        <a aria-expanded="true" onclick="ActiveStudent()">Enrolled <span style="font-size: 11px;">(<?php echo $ActiveStudent_count; ?>)</span></a>
                     </li>

                     <li <?php if ($this->session->userdata('DraftStudent')){ ?> class="active" <?php  } ?>>
                        <a  aria-expanded="false" onclick="DraftStudent()">Pending <span style="font-size: 11px;">(<?php echo $DraftStudent_count; ?>)</span></a>
                     </li>

                     <li <?php if ($this->session->userdata('recentStudent')){ ?> class="active" <?php  } ?>>
                        <a aria-expanded="false" onclick="recentStudent()">New <span style="font-size: 11px;">(<?php echo $recentStudent_count; ?>)</span></a>
                     </li>

                     <li <?php if ($this->session->userdata('TrashStudent')){ ?> class="active" <?php  } ?>>
                        <a aria-expanded="false" onclick="TrashStudent()">Rejected <span style="font-size: 11px;">(<?php echo $TrashStudent_count; ?>)</span></a>
                     </li>
                     
                  </ul>
               </div>
            </div><?php }?>
            <div class="col-sm-6 nopading">
               <div class="pull-right">
                  <div class="btn-group">
                     <?php 
                        $usertype = $this->session->userdata("usertype");
                        
                           if ($this->session->userdata('sessionType')  || $this->session->userdata('entryType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
                        
                           $display =  "block";
                        
                           $addmorebutton = "none";
                        
                           $Removemorebutton = "inline-block";
                        
                           }else{
                        
                           $display = "none";
                        
                           $addmorebutton = "inline-block";
                        
                           $Removemorebutton = "none";
                        
                           }
                        
                           
                        
                           ?> 
                     <?php 
                        if ($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Superadmin" || $usertype == "Teacher" ) {
                        
                        ?> 
      
 <?php  if ($usertype == "Teacher" ) { ?>
            <?php if($rows_academic==1){ ?>
            <a href="<?php echo base_url('student/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
         <?php } ?>

<?php } else  {  ?>
   <a href="<?php echo base_url('student/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
<?php } ?>
                     <?php  } ?>
                     <!--   <a href="#" id="morefilterButtinShow" style="display:<?php echo $addmorebutton ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus" ></span> More Filters</a> -->
                     <!--  <a  onclick="ResetMorefilter()" id="morefilterButtinHide" style="display:<?php echo $Removemorebutton ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-minus"></span> Remove Filters</a> -->
                     <a style="cursor:pointer;" onclick="ResetAllfilter_student()" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-minus"></span> Reset All Filters</a>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
         <div class="clearfix"></div>
         <div class="col-sm-12">
            <div class="theme_input_blue">
               <?php 
                  if ($this->session->userdata('sessionType')  || $this->session->userdata('entryType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
                  
                  $display =  "block";
                  
                  $addmorebutton = "none";
                  
                  $Removemorebutton = "inline-block";
                  
                  }else{
                  
                  $display = "none";
                  
                  $addmorebutton = "inline-block";
                  
                  $Removemorebutton = "none";
                  
                  }
                  
                  
                  
                  ?>
               <div class="col-sm-3">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post" >
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           <?=$this->lang->line("student_classes")?>
                           </label>
                           <div class="selectdiv">
                              <?php
                                 $array = array("0" => $this->lang->line("student_select_class"));
                                 
                                 
                                 
                                 foreach ($classes as $classa) {
                                 
                                 
                                 
                                     $array[$classa->classesID] = $classa->classes;
                                 
                                 
                                 
                                 }
                                 
                                 
                                 
                                 echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('classesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <!-- <div class="col-sm-3">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Sub Courses
                           </label>
                           <div class="selectdiv">
                              <select id="subCourseID" class="form-control">
                                 <option value="">Select</option>
                                 <?php  foreach ($subCourses as $key => $value) {
                                    if ($value->sub_coursesID==$this->session->userdata('subCourseID')) {
                                    
                                        $selected =  "Selected";
                                    
                                    }else{
                                    
                                      $selected =  "";
                                    
                                    }
                                    
                                    ?>
                                 <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div> -->
               <div class="col-sm-3">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <div>
                              <label for="classesID" class="control-label">
                              Session
                              </label>
                           </div>
                           <?php
                              $arraySessionFilter = explode('-', $this->session->userdata('sessionFilter'));

                               $SessionFrom = $this->session->userdata('SessionFrom');
                              
    
                               ?>
                           <div class="col-sm-6 nopading">
                              <div class="forpostionReletive">
                                 <select class="form-control" id="SessionFrom">
                                    <option>From</option>
                                    <?php for($i=2015; $i<=2025; $i++){
                                       if($this->session->userdata('SessionFrom')){
                                    if($SessionFrom==$i){
                                          $selected = "selected"; 
                                         }else{
                                          $selected = "";
                                       } 
                                       } else {
                                       if(count($arraySessionFilter)==2 && $arraySessionFilter[0]==$i){
                                          $selected = "selected"; 
                                         }else{
                                          $selected = "";
                                       } 
                                    }
                                     ?>
                                    <option <?php echo $selected ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                 </select>
                                 <!-- <input type="text" name="" value="<?php if(count($arraySessionFilter)==2) {echo $arraySessionFilter[0]; } ?>" id="SessionFrom" class="form-control CalenderYear" placeholder="From">
                                    <div class="postionAbsoluter"><i class="fa fa-calendar" aria-hidden="true"></i></div> -->
                              </div>
                           </div>
                           <div class="col-sm-6 nopading">
                              <div class="forpostionReletive">
                                 <select class="form-control" id="SessionTo">
                                    <option>To</option>
                                    <?php for($i=2015; $i<=2025; $i++){ ?>
                                    <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[1]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                 </select>
                                 <!-- <input type="text" name="" id="SessionTo" value="<?php if(count($arraySessionFilter)==2) {echo $arraySessionFilter[1]; } ?>" class="form-control CalenderYear" placeholder="To">
                                    <div class="postionAbsoluter"><i class="fa fa-calendar" aria-hidden="true"></i></div> -->
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset"><a style="cursor:pointer;" onclick="ResetSesession()" id="ResetSesession">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-2">
                  <div class="selectdiv">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Session Type
                           </label>
                           <div class="">
                              <?php
                                 $array = array("0" => "Select Session Type","A"=>"A","C"=>"C");
                                 
                                 
                                 
                                 echo form_dropdown("sessionType", $array, set_value("sessionType",$this->session->userdata('sessionType')), "id='sessionType' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset"><a style="cursor:pointer;" onclick="ResetsessionType()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-2">
                  <div class="selectdiv">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Entry Type
                           </label>
                           <div class="">
                              <?php
                                 $array = array("0" => "Select Entry","Fresher"=>"Fresher","Lateral"=>"Lateral");
                                 
                                 
                                 
                                 echo form_dropdown("entryType", $array, set_value("entryType",$this->session->userdata('entryType')), "id='entryType' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset"><a style="cursor:pointer;" onclick="ResetentryType()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-2">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Year/Semester
                           </label>
                           <div class="selectdiv">
                              <select name="yos_filter" id="yos_filter" class='form-control'>
                                 <option value="">Year/Semester</option>
                                 <?php 
                                    if ($classesRow) {
                                    
                                    $looping    =  (int) $classesRow->duration;
                                    
                                    if ($classesRow->mode==1) {
                                    
                                    for ($i=1; $i <=$looping; $i++) {
                                    
                                    if ($this->session->userdata('yos_filter')==CallYears($i)) {
                                    
                                    $select =  "Selected";
                                    
                                    }else{
                                    
                                    $select =  "";
                                    
                                    }
                                    
                                    echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                    
                                    
                                    
                                    }
                                    
                                    }
                                    
                                    
                                    
                                    else{
                                    
                                    for ($i=1; $i <=(2*$looping); $i++) {
                                    
                                    if ($this->session->userdata('yos_filter')==CallSemester($i)) {
                                    
                                    $select =  "Selected";
                                    
                                    }else{
                                    
                                    $select =  "";
                                    
                                    }
                                    
                                    echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                    
                                    }
                                    
                                    }
                                    
                                    }
                                    
                                    ?>
                              </select>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="Resetyos()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <!-- <div class="col-sm-6">
                  <div class="">
                  
                     <form style="" class="form-horizontal" role="form" method="post">
                  
                        <div class="form-group">
                  
                           <label for="student_position" class="control-label">
                  
                           Student Position
                  
                           </label>
                  
                           <div class="selectdiv">
                  
                             <select name="student_position" id="student_position" class='form-control'>
                               
                                 <option>Student Position</option>
                  
                                 <option <?php if($this->session->userdata('student_position')=="Passed Out"){echo "selected";}else{echo '';} ?> value="Passed Out">Passed Out</option>
                  
                                 <option <?php if($this->session->userdata('student_position')=="2nd Year with back"){echo "selected";}else{echo '';} ?> value="2nd Year with back">2nd Year with back</option>
                  
                                 <option <?php if($this->session->userdata('student_position')=="Back Out"){echo "selected";}else{echo '';} ?> value="Back Out">Back Out</option>
                  
                                 <option <?php if($this->session->userdata('student_position')=="3rd Year with back"){echo "selected";}else{echo '';} ?> value="3rd Year with back">3rd Year with back</option>
                  
                                 <option <?php if($this->session->userdata('student_position')=="Refund"){echo "selected";}else{echo '';} ?> value="Refund">Refund</option>
                  
                                                           </select>
                  
                           </div>
                  
                           <div class="clearfix"></div>
                  
                           <div class="forReset" ><a style="cursor:pointer;" onclick="Resetstudentposition()">Reset This Filter </a></div>
                  
                        </div>
                  
                     </form>
                  
                  </div>
                  
                  </div>
                  -->
               <?php if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype=="Accountant" || $usertype=="Academic" || $usertype == "Super_A_P" ){ ?>  
               <?php if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype=="Academic"  ) { ?>   
          <div class="col-sm-4">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Relationship Manager 
                           </label>
                           <div class="selectdiv">
                              <select id="subCourseID" class="form-control" onchange="ajaxGet_Teacher($(this).val())">
                                 <option value="">Relationship Manager</option>
                                 <?php  foreach ($total_rm as $key => $value) {
                                    if ($value->userID==$this->session->userdata('rmFilter')) {
                                        $selected =  "Selected";
                                    }else{
                                      $selected =  "";
                                    }
                                    ?>
                                 <option value="<?php echo $value->userID ?>" <?php echo $selected ?>><?php echo $value->name ?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetrmFilter()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>  

                 <div class="col-sm-4">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Academic Partner
                           </label>
                           <div class="selectdiv">

                              <?php  
                                 $array = array("0" => "Academic Partner");
                                 foreach ($counsellor as $counsellors) {
                                     $array[$counsellors->teacherID] = $counsellors->name;
                                 }
                                 echo form_dropdown("teacherID", $array, set_value("teacherID",$this->session->userdata('teacherID')), "id='teacherID' class='form-control'");
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetteacherID()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div> <?php }
               
                elseif($usertype=="Super_A_P")
                {

            ?>             
                     

<div class="col-sm-4">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Academic Partner
                           </label>
                           <div class="selectdiv">

                              <?php  
                               $array = array("0" => "Academic Partner");
                                 
                                 
                                 
                                 
                                 
                                 foreach ($counsellorAP as $counsellors) {
                                 
                                 
                                 
                                     $array[$counsellors->teacherID] = $counsellors->name;
                                 
                                 
                                 
                                 }
                                 
                                

                                 echo form_dropdown("teacherID", $array, set_value("teacherID",$this->session->userdata('teacherID')), "id='teacherID' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetteacherID()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
<?php  }  ?>
 
             
             
   
   <?php if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype=="Accountant" ){ 
      if($usertype=="Accountant"){?>

               <div class="col-sm-4">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Academic Partner
                           </label>
                           <div class="selectdiv">

                              <?php  
                                 $array = array("0" => "Academic Partner");
                                 foreach ($counsellor as $counsellors) {
                                     $array[$counsellors->teacherID] = $counsellors->name;
                                 }
                                 echo form_dropdown("teacherID", $array, set_value("teacherID",$this->session->userdata('teacherID')), "id='teacherID' class='form-control'");
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetteacherID()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div> 
                    <?php } ?>

               <div class="col-sm-4">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Payment Status
                           </label>
                           <div class="selectdiv">
                              <select name="payment_status" id="payment_status" class='form-control'>
                                 <option>Payment Status</option>
                                 <option <?php if($this->session->userdata('payment_status')==2){echo "selected";}else{echo '';} ?> value="2">Paid</option>
                                 <option <?php if($this->session->userdata('payment_status')==1){echo "selected";}else{echo '';} ?> value="1">Partial Paid</option>
                                 <option <?php if($this->session->userdata('payment_status')==3){echo "selected";}else{echo '';} ?> value="3">Not paid</option>
                              </select>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="Resetpaymentstatus()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div><?php } ?>
               <div class="clearfix"></div>
               <?php } ?>
            </div>
         </div>
         <div class="clearfix"></div>
         <!-- col-sm-12 -->
         <div class="col-sm-12" style="display:none;">
            <!--   <div class="col-sm-12" id="MorefilterPanel" style="display:<?php echo $display ?>"> -->
            <div class="theme_input_blue">
               <div class="col-sm-2">
                  <div class="selectdiv">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Session Type
                           </label>
                           <div class="">
                              <?php
                                 $array = array("0" => "Select Session Type","A"=>"A","C"=>"C");
                                 
                                 
                                 
                                 echo form_dropdown("sessionType", $array, set_value("sessionType",$this->session->userdata('sessionType')), "id='sessionType' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset"><a style="cursor:pointer;" onclick="ResetsessionType()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-2">
                  <div class="selectdiv">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Entry Type
                           </label>
                           <div class="">
                              <?php
                                 $array = array("0" => "Select Entry","Fresher"=>"Fresher","Lateral"=>"Lateral");
                                 
                                 
                                 
                                 echo form_dropdown("entryType", $array, set_value("entryType",$this->session->userdata('entryType')), "id='entryType' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset"><a style="cursor:pointer;" onclick="ResetentryType()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Exam Type
                           </label>
                           <div class="selectdiv">
                              <?php
                                 $array = array("0" => "Select Exam Type","o"=>"One Time","y"=>"Yearly");
                                 
                                 
                                 
                                 echo form_dropdown("examType", $array, set_value("examType",$this->session->userdata('examType')), "id='examType' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetexamType()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Education Mode
                           </label>
                           <div class="selectdiv">
                              <?php
                                 $array = array("0" => "Select Education Mode","2"=>"Semester","1"=>"Year");
                                 
                                 
                                 
                                 echo form_dropdown("education_mode", $array, set_value("education_mode",$this->session->userdata('education_mode')), "id='education_mode' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="Reseteducation_mode()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Academic Partner
                           </label>
                           <div class="selectdiv">
                              <?php
                                 $array = array("0" => "Academic Partner");
                                 
                                 
                                 
                                 
                                 
                                 foreach ($counsellor as $counsellors) {
                                 
                                 
                                 
                                     $array[$counsellors->teacherID] = $counsellors->name;
                                 
                                 
                                 
                                 }
                                 
                                 
                                 
                                 echo form_dropdown("teacherID", $array, set_value("teacherID",$this->session->userdata('teacherID')), "id='teacherID' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetteacherID()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="clearfix"></div>
         <!-- col-sm-12 dusra -->
         <div class="col-sm-12">
            <!-- start revenue -->
             <?php if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype=="Accountant"  ){ ?>
            <div class="col-md-4 col-sm-6">
               <div class="widget smart-standard-widget" style="background: #efefef;">
                  <div class="row">
                     <div class="widget-caption success">
                        <div class="col-xs-4 no-pad zoom">
                           <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                        </div>
                        <div class="col-xs-8 no-pad">
                           <div class="widget-detail totalPayment-back">
                              <!--      <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment->amount-$totalPayment_full->paidamount ?></span> -->
                              <h3 style="color: #9c1855;"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount ?></h3>
                              <span>Total Revenue</span>
                           </div>
                        </div>
                        <div class="col-xs-12">
                           <div class="widget-line bg-success" style="background: #fff">
                              <span style="width:60%; color: #9c1855; " class="bg-info widget-horigental-line"></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-sm-6">
               <div class="widget smart-standard-widget" style="background: #efefef;">
                  <div class="row">
                     <div class="widget-caption success">
                        <div class="col-xs-4 no-pad zoom">
                           <i class="icon fa fa-money" aria-hidden="true" style="background: #007bff;color: #ffffff"></i>
                        </div>
                        <div class="col-xs-8 no-pad">
                           <div class="widget-detail totalPayment-back">
                              <h3 class="cl-success" style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->Totalpaidamount ?></h3>
                              <span>Total Paid</span>
                           </div>
                        </div>
                        <div class="col-xs-12">
                           <div class="widget-line bg-success" style="background: #fff">
                              <span style="width:60%; background: #007bff;" class="widget-horigental-line">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-sm-6">
               <div class="widget smart-standard-widget" style="background: #efefef;">
                  <div class="row">
                     <div class="widget-caption warning">
                        <div class="col-xs-4 no-pad zoom">
                           <i class="icon fa fa-ioxhost"></i>
                        </div>
                        <div class="col-xs-8 no-pad">
                           <div class="widget-detail totalPayment-back">
                              <h3 class="cl-warning"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $payment_amount->TotalAmount-$payment_amount->Totalpaidamount; ?></h3>
                              <span>Due Amount</span>
                           </div>
                        </div>
                        <div class="col-xs-12">
                           <div class="widget-line bg-warning" style="background: #fff">
                              <span style="width:60%;" class="bg-warning widget-horigental-line"></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- end revenue -->
         <?php } ?>
            <div class="">
               <div class="tab-content">
                  <div id="all" class="tab-pane active">
                     <div id="hide-table">
                        <?php 
                           if ($students) { ?>
                        <?php if($usertype=="ClgAdmin" || $usertype == "Admin" ){ ?>
                        <form action="<?php echo base_url() ?>student/multipleAction" onsubmit="return confirm('Do you really want to submit the form?');" method = "post">
                           <div class="action-layout">
                              <ul>
                                 <li>
                                    <label class="nexCheckbox">Check
                                    <input type="checkbox" name="select_all" id="select_all">
                                    <span class="checkmark checkmark-action-layout"></span>
                                    </label>
                                 </li>
                                 <?php if ($this->session->userdata('TrashStudent')) {  ?>
                                 <li class="active-btn">
                                    <input type="submit" class="btn btn-success etsfilertButton disabled"  name="Active" value="Restore" disabled>
                                 </li>
                                  <?php } ?>
                           <!--       <li class="active-btn">
                                    <input type="submit"  class="btn btn-info etsfilertButton disabled" name="Draft" value="Draft" disabled>
                                 </li> -->
                           <?php 
                              if ($this->session->userdata('recentStudent') || $this->session->userdata('ActiveStudent') )
                              {  
                                 if($this->session->userdata('teacherID') == '0')
                                 {
                                    echo '';
                                 }
                                 else
                                 {
                           ?>
                                 <li class="active-btn">
                                    <input type="submit" class="btn btn-primary etsfilertButton disabled paypay" name="Pay" value="Pay" disabled>
                                  </li>
                           <?php
                                 } 
                              } 
                           ?>
                           
                           <?php 
                              if ($this->session->userdata('ActiveStudent'))
                              {
                                 if(($this->session->userdata('SessionFrom')) || ($this->session->userdata('sessionFilter')))
                                 { ?>
                                     <li class="active-btn">
                                    <input type="submit" class="btn btn-warning etsfilertButton disabled promote" name="Promote" value="Promote" disabled>
                                  </li>   
                                 <?php } ?>
                           
                           <?php 
                                 if(($this->session->userdata('classesID')) || $this->session->userdata('SessionTo') || $this->session->userdata('sessionType') || $this->session->userdata('entryType') || $this->session->userdata('yos_filter') || $this->session->userdata('subCourseID') || $this->session->userdata('teacherID'))
                                 {
                           ?>
                                    <li class="active-btn">
                                       <input type="submit" class="btn btn-primary etsfilertButton disabled zipfile" name="zipfile" value="Zip" disabled>
                                    </li>  
                           <?php         
                                 } 
                              } 
                           ?>
                                  <?php if ($this->session->userdata('recentStudent') || $this->session->userdata('ActiveStudent') || $this->session->userdata('DraftStudent') ){  ?>
                                 <li class="active-btn">
                                    <a class="btn btn-danger etsfilertButton disabled"  data-toggle="modal" id="myBtn"  name="" value="" disabled>Delete</a>
                                 </li>
                               <?php } ?>

                               <?php if ($this->session->userdata('recentStudent') || $this->session->userdata('ActiveStudent') || $this->session->userdata('DraftStudent') ){  ?>
                                 <li class="active-btn">
                                      <a class="btn btn-success etsfilertButton disabled" id="myModal_id"  name="Remider" value="Remider" disabled>Remark</a>
                                 </li>
                               <?php } ?>
                              </ul>
                           </div>
                            <div id="myModal" class="modal">
                              <div class="modal-dialog">
                                 <!-- Modal content-->
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       <h4 class="modal-title">Reminder</h4>
                                    </div>
                                    <div class="modal-body">
                                       <div class="form-group">
                                          <label>Reminder</label>
                                          <textarea class="form-control" name="reminder_text1" placeholder="Reminder"></textarea>
                                       </div>
                                       <div class="form-group">
                                          <input type="submit" class="btn btn-success etsfilertButton"  name="Remider" value="Submit">
                                       </div>
                                    </div>
                               
                                 </div>
                              </div>
                           </div>
                           <div id="myModal_for_trash" class="modal">
                              <div class="modal-dialog">
                                 <!-- Modal content-->
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       <h4 class="modal-title">Remark</h4>
                                    </div>
                                    <div class="modal-body">
                                       <div class="form-group">
                                          <label>Remark</label>
                                          <textarea class="form-control" name="reminder_text" placeholder="Reminder"></textarea>
                                       </div>
                                       <div class="form-group">
                                          <input type="submit" class="btn btn-danger etsfilertButton" name="Delete" value="Delete">
                                       </div>
                                    </div>
                               
                                 </div>
                              </div>
                           </div>

                           <table id="example4" class="table table-striped table-bordered table-hover dataTable no-footer">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th >S.no</th>
                                    <th><?=$this->lang->line('student_photo')?></th>
                                    <th>Student Id</th>
                                    <th><?=$this->lang->line('student_roll')?></th>
                                    <th><?=$this->lang->line('student_name')?></th>
                                    <th><?=$this->lang->line('father_name')?></th>
                                    <th>Semester/Year</th>
                                    <th><?=$this->lang->line("student_classes")?></th>
                                    <th><?=$this->lang->line('academic_partner')?></th>
                                    <th><?=$this->lang->line('session')?></th>
                                    <th><?=$this->lang->line('sessionType')?></th>
                                    <th><?=$this->lang->line('student_entry_type')?></th>
                                    <th><?=$this->lang->line('student_phone')?></th>
                                    <th><?=$this->lang->line('student_dob')?></th>
                                    <th><?=$this->lang->line('student_sex')?></th>
                                    <th><?=$this->lang->line('student_email')?></th>
                                    <th><?=$this->lang->line('mother_name')?></th>
                                    <th><?=$this->lang->line('aadhar')?></th>
                                    <th><?=$this->lang->line('nationality')?></th>
                                    <th ><?=$this->lang->line('create_date')?></th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Payment Status</th>
                                    <th><?=$this->lang->line('action')?></th>
                                 </tr>
                              </thead>
                           </table>
                        </form>
                        <?php } ?>


<!-- Support -->
<?php if($usertype == "Support" || $usertype == "Academic" || $usertype == "Super_A_P" ){ ?>

<table id="student_view" class="table table-striped table-bordered table-hover dataTable no-footer">
                              <thead>
                                 <tr>
                                    <th>S.no</th>
                                    <th><?=$this->lang->line('student_photo')?></th>
                                    <th>Student Id</th>
                                    <th><?=$this->lang->line('student_roll')?></th>
                                    <th><?=$this->lang->line('student_name')?></th>
                                    <th><?=$this->lang->line('father_name')?></th>
                                    <th>Semester/Year</th>
                                    <th><?=$this->lang->line("student_classes")?></th>
                                    <th><?=$this->lang->line('academic_partner')?></th>
                                    <th><?=$this->lang->line('session')?></th>
                                    <th><?=$this->lang->line('sessionType')?></th>
                                    <th><?=$this->lang->line('student_entry_type')?></th>
                                    <th><?=$this->lang->line('student_phone')?></th>
                                    <th><?=$this->lang->line('student_dob')?></th>
                                    <th><?=$this->lang->line('student_sex')?></th>
                                    <th><?=$this->lang->line('student_email')?></th>
                                    <th><?=$this->lang->line('mother_name')?></th>
                                    <th><?=$this->lang->line('aadhar')?></th>
                                    <th><?=$this->lang->line('nationality')?></th>
                                    <th ><?=$this->lang->line('create_date')?></th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Payment Status</th>
                                    <th><?=$this->lang->line('action')?></th>
                                 </tr>
                              </thead>
                           </table>

 <?php } ?>


                        <!-- teacherStudent -->
                        <?php if($usertype == "Teacher"  || $usertype == "Accountant" ){ ?>
                        <form action="<?php echo base_url() ?>student/multipleAction" onsubmit="return confirm('Do you really want to submit the form?');" method = "post">
                           <div class="action-layout">
                              <ul>
                                 <li>
                                    <label class="nexCheckbox">Check
                                    <input type="checkbox" name="select_all" id="select_all">
                                    <span class="checkmark checkmark-action-layout"></span>
                                    </label>
                                 </li>
                                 <li class="active-btn">
                                    <a class="btn btn-success etsfilertButton disabled" id="myModal_id"  name="Remider" value="Remider" disabled>Remider</a>
                                 </li>
                              </ul>
                           </div>
                           <div id="myModal" class="modal">
                              <div class="modal-dialog">
                                 <!-- Modal content-->
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       <h4 class="modal-title">Reminder</h4>
                                    </div>
                                    <div class="modal-body">
                                       <div class="form-group">
                                          <label>Reminder</label>
                                          <textarea class="form-control" name="reminder_text" placeholder="Reminder"></textarea>
                                       </div>
                                       <div class="form-group">
                                          <input type="submit" class="btn btn-success etsfilertButton"  name="Remider" value="Submit">
                                       </div>
                                    </div>
                               
                                 </div>
                              </div>
                           </div>
                           <table id="teacherStudent" class="table table-striped table-bordered table-hover dataTable no-footer">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th >S.no</th>
                                    <th><?=$this->lang->line('student_photo')?></th>
                                    <th>Student Id</th>
                                    <th><?=$this->lang->line('student_roll')?></th>
                                    <th><?=$this->lang->line('student_name')?></th>
                                    <th><?=$this->lang->line('father_name')?></th>
                                    <th>Semester/Year</th>
                                    <th><?=$this->lang->line("student_classes")?></th>
                                    <th><?=$this->lang->line('academic_partner')?></th>
                                    <th><?=$this->lang->line('session')?></th>
                                    <th><?=$this->lang->line('sessionType')?></th>
                                    <th><?=$this->lang->line('student_entry_type')?></th>
                                    <th><?=$this->lang->line('student_phone')?></th>
                                    <th><?=$this->lang->line('student_dob')?></th>
                                    <th><?=$this->lang->line('student_sex')?></th>
                                    <th><?=$this->lang->line('student_email')?></th>
                                    <th><?=$this->lang->line('mother_name')?></th>
                                    <th><?=$this->lang->line('aadhar')?></th>
                                    <th><?=$this->lang->line('nationality')?></th>
                                    <th ><?=$this->lang->line('create_date')?></th>
                                    <th>Total Amount</th>
                                    <th>Paid Amount</th>
                                    <th>Payment Status</th>
                                    <th><?=$this->lang->line('action')?></th>
                                 </tr>
                              </thead>
                           </table>
                        </form>
                        <?php } ?>
                        <?php } else { ?>
                        <div class="Noresuls">
                           <h1>Sorry we couldn't find any matches</h1>
                           <p>Maybe your search was too specific, please try searching with another term.</p>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
               <!-- nav-tabs-custom -->
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
</div>

<!-- Body -->
<!-- col-sm-12 for tab -->
<!-- end table form here -->
<script type="text/javascript">
   function  ajaxGet_Teacher(rmFilter){
      $('#example4').DataTable().state.clear();
    $.ajax({
            
                type: 'POST',
                url: "<?=base_url('student/index')?>",
                data: "rmFilter=" + rmFilter,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }
   
    
   
            }); 
   }
</script>
<script type="text/javascript">
   $(document).on("change",".knowSemester",function(){
      studentID = this.id;
      yearsOrSemester = $(this).val();
      course = $("#course_"+studentID).val();
      $.ajax({
         type:"POST",
         data:{"studentID":studentID,"yearsOrSemester":yearsOrSemester,"course":course},
         url:"<?=base_url('student/attendanceSheet')?>",
         success:function(res){
            w = window.open(window.location.href,"_blank");
            w.document.open();
            w.document.write('<html><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet"/><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/print.css" rel="stylesheet"/><body onload="window.print()">'+res+'</body></html>');
            w.document.close();
         }
      });
   });
</script>
<script type="text/javascript">
   $('#classesID').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var classesID = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "classesID=" + classesID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#subCourseID').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var subCourseID = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "subCourseID=" + subCourseID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#SessionFrom').change(function() {
     $('#example4').DataTable().state.clear();
       var SessionFrom = $(this).val();  
           $.ajax({
               type: 'POST',
               url: "<?=base_url('student/index')?>",   
               data:{SessionFrom:SessionFrom},
               dataType: "html",
               success: function(data) {
                   location.reload();

               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>

<script type="text/javascript">
   $('#SessionTo').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var SessionTo = $(this).val();
   
       var SessionFrom  = $('#SessionFrom').val();
   
   
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data:{SessionTo:SessionTo,SessionFrom:SessionFrom},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#sessionType').change(function() {
    $('#example4').DataTable().state.clear();
       var sessionType = $(this).val();
           $.ajax({
               type: 'POST',
               url: "<?=base_url('student/index')?>",
               data: "sessionType=" + sessionType,
               dataType: "html",
               success: function(data) {
                    location.reload();
               }   
           });
   });  
</script>

<script type="text/javascript">
   $('#entryType').change(function() {
    $('#example4').DataTable().state.clear();
       var entryType = $(this).val();
           $.ajax({
               type: 'POST',
               url: "<?=base_url('student/index')?>",
               data: "entryType=" + entryType,
               dataType: "html",
               success: function(data) {
                    location.reload();
               }   
           });
   });  
</script>

<script type="text/javascript">
   $('#examType').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var examType = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "examType=" + examType,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#education_mode').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var education_mode = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "education_mode=" + education_mode,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#student_position').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var student_position = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "student_position=" + student_position,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#yos_filter').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var yos_filter = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "yos_filter=" + yos_filter,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#payment_status').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var payment_status = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: "payment_status=" + payment_status,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#teacherID').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var teacherID = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/index')?>",
   
   
   
               data: " teacherID=" + teacherID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   function ResetSesession(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetSesession')?>",
   
   
   
               data:{ResetSesession:'ResetSesession'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   
   
   function ResetCourses(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetCourses')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   
   
   function ResetSubcourses(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetSubcourses')?>",
   
   
   
               data:{ResetSubcourses:'ResetSubcourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   function ResetrmFilter(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetrmFilter')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   function ResetsessionType(){
     $('#example4').DataTable().state.clear();
           $.ajax({
               type: 'POST',
               url: "<?=base_url('student/ResetsessionType')?>",
               data:{ResetCourses:'ResetCourses'},
               dataType: "html",
               success: function(data) {
                   location.reload();
               }
           });
   }

      function ResetentryType(){
     $('#example4').DataTable().state.clear();
           $.ajax({
               type: 'POST',
               url: "<?=base_url('student/ResetentryType')?>",
               data:{ResetCourses:'ResetCourses'},
               dataType: "html",
               success: function(data) {
                   location.reload();
               }
           });
   }
   
   
   
   function ResetexamType(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetexamType')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   function Reseteducation_mode(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/Reseteducation_mode')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   function ResetAllfilter_student(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetAllfilter')?>",
   
   
   
               data:{ResetAllfilter:'ResetAllfilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   function ResetMorefilter(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetMorefilter')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   function ResetteacherID(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetteacherID')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   function Resetstudentposition(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/Resetstudentposition')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   
   
   function Resetyos(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/Resetyos')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   
   
   function Resetpaymentstatus(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/Resetpaymentstatus')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
</script>
<script type="text/javascript">
   $("#morefilterButtinShow").click(function(){
   
   $("#MorefilterPanel").show();
   
   $("#morefilterButtinShow").hide();
   
   $("#morefilterButtinHide").show();
   
   
   
   });
   
   
   
   $("#morefilterButtinHide").click(function(){
   
   $("#MorefilterPanel").hide();
   
   $("#morefilterButtinShow").show();
   
   $("#morefilterButtinHide").hide();
   
   });
   
</script>
<script>
// Get the modal
var myModal_for_trash = document.getElementById("myModal_for_trash");

// Get the button that opens the modal
var myBtn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
myBtn.onclick = function() {
  myModal_for_trash.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  myModal_for_trash.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    myModal_for_trash.style.display = "none";
  }
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myModal_id");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
<style type="text/css">
  .modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
