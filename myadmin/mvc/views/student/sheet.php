<?php 
  if(empty($award))
  {
    echo "<p style='color:red;'>No Information available</p>";
  }
  else
  {
?>
<div class="col-xs-12">
  <a href="javascript:void(0)" class="btn btn-success" style="float: right;" onclick='printDiv();'>Print Sheet</a>
</div>
<div id="print_wrapp">
  <div class="col-xs-12" id="bot">
    <div class="col-xs-2">
      <img src="<?php echo base_url(); ?>uploads/images/Lingayas Vidyapeeth Logo.png" class="img-responsive" style="width: 110px;">
    </div>
    <div class="col-xs-10">
      <div class="center">
        <h3>
          Lingaya's Vidyapeeth, Fraidabad<br>
          Award Sheet<br>
          End Semester Examination<br>
          (May-June, 2019)
        </h3>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12" id="bot">
      <div class="col-xs-6">
        <div class="left">
          <p>Department: </p>
        </div>
        <div class="right">
          <p><?php echo $award[0]['department_name']?></p>
        </div>

        <div class="left">
          <p>Program/Course: </p>
        </div>
        <div class="right">
          <p><?php echo $award[0]['classes']?></p>
        </div>

        <div class="left">
          <p>Subject Name: </p>
        </div>
        <div class="right">
          <p><?php echo $award[0]['subject']?></p>
        </div>

        <div class="left">
          <p>Session: </p>
        </div>
        <div class="right">
          <p><?php echo $award[0]['session']?></p>
        </div>

      </div>
      <div class="col-xs-6">

        <div class="left">
          <p>Semester: </p>
        </div>
        <div class="right">
          <p><?php echo $award[0]['yearsOrSemester']?></p>
        </div>

        <div class="left">
          <p>Subject Code: </p>
        </div>
        <div class="right">
          <p><?php echo $award[0]['subject_code']?></p>
        </div>

        <div class="left">
          <p>Maximum Marks: </p>
        </div>
        <div class="right">
          <p></p>
        </div>

      </div>
    </div>

    <div class="col-xs-12" id="bot">
      <div class="col-xs-12">
        <div class="table-responsive">
          <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
            <thead>
              <th>S.No.</th>
              <th>Roll No.</th>
              <th>Marks(In Figure)</th>
              <th>Marks(in Words)</th>
            </thead>
            <tbody>
              <?php
                $i=1;
                foreach($award as $row)
                {
              ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['roll']; ?></td>
                    <td></td>
                    <td></td>
                  </tr>
              <?php
                $i++;
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>


    <div class="col-xs-12" id="bot">
      <div class="col-xs-6">
        <div id="left">
          <p>Total No. of Student Pass: </p>
        </div>
        <div id="right">
          <p></p>
        </div>
      </div>
      <div class="col-xs-6">
        <div id="left">
          <p>Total No. of Student Fail: </p>
        </div>
        <div id="right">
          <p></p>
        </div>
      </div>
    </div>

    <div class="col-xs-12" id="bot">
      <div class="col-xs-4">
        <p id="pp">Signature of Examiner</p>
        <div class="left">
          <p>Name: </p>
        </div>
        <div class="right">
          <p>____________________</p>
        </div>
      </div>
      <div class="col-xs-4">
        <p id="pp">Signature of Checking Assistant</p>
        <div class="left">
          <p>Name: </p>
        </div>
        <div class="right">
          <p>____________________</p>
        </div>
      </div>
      <div class="col-xs-4">
        <p id="pp">Signature of H.O.D</p>
        <div class="left">
          <p>Name: </p>
        </div>
        <div class="right">
          <p>____________________</p>
        </div>
      </div>
    </div>

  </div>
</div>
<script>
  $(document).ready(function(){
      $('#example1').DataTable({
        "searching": false,
        "info":     false,
        "lengthChange": false,
        "pageLength": 20
      });
  });
</script>
<?php
  }
?>