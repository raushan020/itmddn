<style>
    body.modal-open {
        overflow: hidden;
        position: fixed;
    }
</style>
<div class="box">
    <div class="box-header">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="fa fa-user"></i><?=$this->lang->line('panel_title')?> </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard </li>
                </ol>
            </div>
        </div>


        <h3 class="text-themecolor"><i class="fa icon-user"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active"><?=$this->lang->line('menu_student')?></li>

        </ol>

    </div><!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">    

        <div class="row">

            <div class="col-sm-12">

                <?php

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin" || $usertype == 'ClgAdmin'  || $usertype == "Support" ) {

                ?>

                <h5 class="page-header">

                    <a href="<?php echo base_url('student/add') ?>">

                        <i class="fa fa-plus"></i>

                        <?=$this->lang->line('add_title')?>

                    </a>

                </h5>

                <?php } ?>


                <div class="col-sm-4 list-group">

                    <div class="list-group-item list-group-item-warning">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="col-sm-3 control-label">

                                    <?=$this->lang->line("student_classes")?>

                                </label>

                                <div class="col-sm-9">
                                    <?php

                                        $array = array("0" => $this->lang->line("student_select_class"));

                                        foreach ($classes as $classa) {

                                            $array[$classa->classesID] = $classa->classes;

                                        }

                                        echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");

                                    ?>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>
                
                <div class="col-sm-4 list-group">

                    <div class="list-group-item list-group-item-warning">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="col-sm-3 control-label">

                                 Sub Courses

                                </label>

                                <div class="col-sm-9">
                                        <select id="subCourseID" class="form-control" onchange="Set_sub_coureses_for_filter($(this).val())">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                           ?>
                                            <option value="<?php echo $value->sub_coursesID ?>"><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                                        </select>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-4 list-group">

                    <div class="list-group-item list-group-item-warning">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="col-sm-2 control-label">

                                 Session

                                </label>

                                <div class="col-sm-3">
                                    <input type="text" name="" class="form-control CalenderYear " placeholder="From">
                                </div>


                                <div class="col-sm-3">
                                    <input type="text" name="" class="form-control CalenderYear" placeholder="To">

                                </div>

                            </div>

                        </form>

                    </div>

                </div>


            <?php if(count($students) > 0 ) { ?>
                    <div class="col-sm-12">
                        <div class="nav-tabs-custom">
                            <div class="tab-content">

                                <div id="all" class="tab-pane active">

                                    <div id="hide-table">

                                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">

                                            <thead>

                                                <tr>            
                                                    <th class="col-sm-1"><?=$this->lang->line('slno')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_photo')?></th>

                                                    <th class="col-sm-2">Student Id</th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_name')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('father_name')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('mother_name')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_roll')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line("student_classes")?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_phone')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_dob')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_sex')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_employment_status')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_email')?></th>
                                                    <th class="col-sm-2"><?=$this->lang->line('aadhar')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('nationality')?></th>
                                                    
                                                    <th class="col-sm-1"><?=$this->lang->line('academic_partner')?></th>

                                                    <th class="col-sm-1"><?=$this->lang->line('education_mode')?></th>

                                                    

                                                    <th class="col-sm-1"><?=$this->lang->line('session')?></th>

                                                    

                                                    <th class="col-sm-1"><?=$this->lang->line('sessionType')?></th>

                                                    <th class="col-sm-1"><?=$this->lang->line('session')?></th>

                                                    <th class="col-sm-1"><?=$this->lang->line('create_date')?></th>

                                                    <th class="col-sm-1"><?=$this->lang->line('counsellor')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php 
                                                    if(count($students)) 
                                                    {
                                                        $i = 1; foreach($students as $student)       
                                                        { 
                                                ?>

                                                    <tr>

                                                        <td data-title="<?=$this->lang->line('slno')?>">

                                                            <?php echo $i; ?>

                                                        </td>



                                                        <td data-title="<?=$this->lang->line('student_photo')?>">

                                                            <?php $array = array(

                                                                    "src" => base_url('uploads/images/'.$student->photo),

                                                                    'width' => '35px',

                                                                    'height' => '35px',

                                                                    'class' => 'img-rounded'



                                                                );

                                                                echo img($array);

                                                            ?>

                                                        </td>

                                                        <td data-title="Student Id">

                                                            <?php echo $student->username; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('student_name')?>">

                                                            <?php echo $student->name; ?>

                                                        </td>



                                                        <td data-title="<?=$this->lang->line('father_name')?>">

                                                            <?php echo $student->father_name; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('mother_name')?>">

                                                            <?php echo $student->mother_name; ?>

                                                        </td>
                                                        <td data-title="<?=$this->lang->line('student_roll')?>">

                                                            <?php echo $student->roll; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('student_classes')?>">

                                                            <?php echo $student->classes; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('student_phone')?>">

                                                            <?php echo $student->phone; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('student_dob')?>">

                                                            <?php echo $student->dob; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('student_sex')?>">

                                                            <?php echo $student->sex; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('student_employment_status')?>">

                                                            <?php echo $student->employment_status; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('student_email')?>">

                                                            <?php echo $student->email; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('aadhar')?>">

                                                            <?php echo $student->aadhar; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('nationality')?>">

                                                            <?php echo $student->nationality; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('sub_coursesID')?>">

                                                            <?php echo $student->sub_coursesID; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('education_mode')?>">
                                                           <?php
                                                            $education_modeArray =  array(1=>"Year",2=>"Semester");
                                                             ?>
                                                            <?php echo $education_modeArray[$student->education_mode]; ?>
                                                        </td>


                                                        <td data-title="<?=$this->lang->line('student_status')?>">

                                                            <?php echo $student->student_status; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('session')?>">

                                                            <?php echo $student->session; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('examType')?>">

                                                            <?php echo $student->examType; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('sessionType')?>">

                                                            <?php echo $student->sessionType; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('create_date')?>">

                                                            <?php echo $student->create_date; ?>

                                                        </td>


                                                        <td data-title="<?=$this->lang->line('counsellor')?>">

                                                            <?php echo $student->counsellor; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('teacher_status')?>">

                                                            <div class="onoffswitch-small" id="<?=$student->studentID?>">

                                                                <input type="checkbox" id="myonoffswitch<?=$student->studentID?>" class="onoffswitch-small-checkbox" name="paypal_demo" <?php if($student->studentactive === '1') echo "checked='checked'"; ?>>

                                                                <label for="myonoffswitch<?=$student->studentID?>" class="onoffswitch-small-label">

                                                                    <span class="onoffswitch-small-inner"></span>

                                                                    <span class="onoffswitch-small-switch"></span>

                                                                </label>

                                                            </div>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('action')?>">

                                                  <?php

             if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == 'ClgAdmin' ) {
                                                                    echo btn_view('invoice/view/'.$invoice->invoiceID, $this->lang->line('view'));

                                                                    echo btn_edit('student/edit/'.$student->studentID, $this->lang->line('edit'));

                                                                    echo btn_delete('student/delete/'.$student->studentID, $this->lang->line('delete'));
                                                                    echo btn_zip('student/zip/'.$student->studentID, $this->lang->line('zip'));


                             } elseif ($usertype == "Teacher" || $usertype == "Support") {

                                                                    echo btn_view('student/view/'.$student->studentID, $this->lang->line('view'));

                                                                    echo btn_zip('student/zip/'.$student->studentID, $this->lang->line('zip'));

                                                                }



                                                            ?>

                                                        </td>

                                                   </tr>

                                                <?php $i++; }} ?>

                                            </tbody>

                                        </table>

                                    </div>



                                </div>
                        </div> <!-- nav-tabs-custom -->

                    </div> <!-- col-sm-12 for tab -->
<!-- end table form here -->

                <?php } else { ?>

                    <div class="col-sm-12">


                        <div class="nav-tabs-custom">

                            <ul class="nav nav-tabs">

                                <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?=$this->lang->line("student_all_students")?></a></li>

                            </ul>
                            <div class="tab-content">

                                <div id="all" class="tab-pane active">

                                    <div id="hide-table">

                                        <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">

                                            <thead>

                                                <tr>

                                                    <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_photo')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_name')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_roll')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('student_phone')?></th>

                                                    <th class="col-sm-2"><?=$this->lang->line('action')?></th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <?php if(count($students)) {$i = 1; foreach($students as $student) { ?>

                                                    <tr>

                                                        <td data-title="<?=$this->lang->line('slno')?>">

                                                            <?php echo $i; ?>

                                                        </td>



                                                        <td data-title="<?=$this->lang->line('student_photo')?>">

                                                            <?php $array = array(

                                                                    "src" => base_url('uploads/images/'.$student->photo),

                                                                    'width' => '35px',

                                                                    'height' => '35px',

                                                                    'class' => 'img-rounded'


                                                                );

                                                                echo img($array);

                                                            ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('student_name')?>">

                                                            <?php echo $student->name; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('student_roll')?>">

                                                            <?php echo $student->roll; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('student_phone')?>">

                                                            <?php echo $student->phone; ?>

                                                        </td>

                                                        <td data-title="<?=$this->lang->line('action')?>">

                                                            <?php

                 if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == 'ClgAdmin') {

                                                                    echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));

                                                                    echo btn_edit('student/edit/'.$student->studentID."/".$set, $this->lang->line('edit'));

                                                                    echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'));

                        } elseif ($usertype == "Teacher" || $usertype == "Support") {

                                                                    echo btn_view('student/view/'.$student->studentID."/".$set, $this->lang->line('view'));

                                                                }



                                                            ?>

                                                        </td>

                                                   </tr>

                                                <?php $i++; }} ?>    

                                            </tbody>

                                        </table>

                                    </div>



                                </div>

                            </div>

                        </div> <!-- nav-tabs-custom -->

                    </div>

                <?php } ?>



            </div> <!-- col-sm-12 -->



        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->


<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

        if(classesID == 0) {

            $('#hide-table').hide();

            $('.nav-tabs-custom').hide();

        } else {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('student/student_list')?>",

                data: "id=" + classesID,

                dataType: "html",

                success: function(data) {

                    window.location.href = data;

                }

            });

        }

    });





    var status = '';

    var id = 0;

    $('.onoffswitch-small-checkbox').click(function() {

        if($(this).prop('checked')) {

            status = 'chacked';

            id = $(this).parent().attr("id");

        } else {

            status = 'unchacked';

            id = $(this).parent().attr("id");

        }



        if((status != '' || status != null) && (id !='')) {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('student/active')?>",

                data: "id=" + id + "&status=" + status,

                dataType: "html",

                success: function(data) {

                    if(data == 'Success') {

                        toastr["success"]("Success")

                        toastr.options = {

                            "closeButton": true,

                            "debug": false,

                            "newestOnTop": false,

                            "progressBar": false,

                            "positionClass": "toast-top-right",

                            "preventDuplicates": false,

                            "onclick": null,

                            "showDuration": "500",

                            "hideDuration": "500",

                            "timeOut": "5000",

                            "extendedTimeOut": "1000",

                            "showEasing": "swing",

                            "hideEasing": "linear",

                            "showMethod": "fadeIn",

                            "hideMethod": "fadeOut"

                        }

                    } else {

                        toastr["error"]("Error")

                        toastr.options = {

                            "closeButton": true,

                            "debug": false,

                            "newestOnTop": false,

                            "progressBar": false,

                            "positionClass": "toast-top-right",

                            "preventDuplicates": false,

                            "onclick": null,

                            "showDuration": "500",

                            "hideDuration": "500",

                            "timeOut": "5000",

                            "extendedTimeOut": "1000",

                            "showEasing": "swing",

                            "hideEasing": "linear",

                            "showMethod": "fadeIn",

                            "hideMethod": "fadeOut"

                        }

                    }

                }

            });

        }

    });

</script>

