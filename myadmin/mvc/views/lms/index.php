    <style type="text/css">

        .frm-grp{

            padding: 10px 15px;

            margin: 0;

        }

        .ctrl-label{

           display: inline-block;

           padding-right: 10px;

           font-size: 14px;

           font-weight: bold;

        }

        .col-th{

            padding: 10px 10px 0px 10px !important;

        }
        .mrgtop{
               /* margin-top: 20px;*/
        }
        .bgonline{
            background: #efefef;
        }

        .bghead{
            background: #c3cae2;
        }
    </style>

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="ti ti-blackboard"></i> LMS</h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active"><?=$this->lang->line('panel_title')?></li>

            </ol>

        </div>

    </div>

    <!-- /.box-header -->



    <!-- form start -->

    <div class="container-fluid">

        <div class="row">

        <div class="card">

            <div class="card-body">



                <div class="">

                    <?php 

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin")

                   if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {

                   $display =  "block";

                   $addmorebutton = "none";

                   $Removemorebutton = "inline-block";

                   }else{

                   $display = "none";

                   $addmorebutton = "inline-block";

                   $Removemorebutton = "none";

                   }

               

                ?>

                

                



                <div class="col-sm-6 col-sm-offset-3">

                    <div class="">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group" style="padding: 10px 15px; margin: 0;">

                                <label for="classesID" class="control-label" style="display: inline-block;

           padding-right: 10px;

           font-size: 14px;

           font-weight: bold;">Semester/Year</label>



                                <div class="">

                                    <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>' style="display: inline-block;width:75%"> 

                                    <option>Select</option>      

                                    <?php 

                                        if ($classesRow) {

                                        $looping    =  (int) $classesRow->duration;

                                        if ($classesRow->mode==1) {

                                            for ($i=1; $i <=$looping; $i++) {

                                                if (CallYears($i)==$this->session->userdata('FilterLmsyearSemesterID')) {

                                            $select = 'Selected';

                                         }else{

                                         $select = '';

                                         }

                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                         

                                        }

                                    }



                                    else{

                                          for ($i=1; $i <=(2*$looping); $i++) {

                                        if (CallSemester($i)==$this->session->userdata('FilterLmsyearSemesterID')) {

                                            $select = 'Selected';

                                         }else{

                                         $select = '';

                                         }

                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       }

                                       }

                                    }

                                    ?>

                                    </select>

                                </div>

                                <div class="clearfix"></div>

                                <div class="forReset"style="padding-right: 5px;"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>

                </div>

                <div class="col-sm-4"> </div>

            </div>

            <div class="clearfix"></div>

               

               <!-- <div id="hide-table">



                    <table id="subjectsTablesP" class="table table-striped table-bordered table-hover dataTable no-footer">



                        <thead>



                            <tr>



                                <th ><?=$this->lang->line('slno')?></th>



                                <th ><?=$this->lang->line('subject_code')?></th>



                                <th ><?=$this->lang->line('subject_name')?></th>



                                <th >Semester/Year</th>

                                 <th >PDF'S</th>



                                <?php  if($usertype == "Admin") { ?>



                                <th ><?=$this->lang->line('action')?></th>



                                <?php } ?>



                            </tr>



                        </thead>



                        <tbody>



                            <?php if(count($subjects)) {

                                $i = 1;

                                $j = 1;

                             foreach($subjects as $subject) { ?>



                                <tr>

                                    <td data-title="<?=$this->lang->line('slno')?>">



                                        <?php echo $i; ?>

                                  </td>



                                <td data-title="<?=$this->lang->line('subject_code')?>">



                                        <?php echo $subject->subject_code; ?>



                                </td>



                                    <td data-title="<?=$this->lang->line('subject_name')?>">



                                        <?php echo $subject->subject; ?>

                                    </td>



                                    <td data-title="Semester/Year">





                                    <?php echo str_replace('_', ' ', $subject->yearsOrSemester);?>



                                    </td>

                            <td data-title="PDF'S">



                            <?php 

                        $this->db->where('subjectID',$subject->subjectID);

                        $query   = $this->db->get('pdf_lms');

                        $countpdf  = $query->result();

                        $j = 1;

                        foreach ($countpdf as $key => $value) {

                            ?>

                           <a target="_blank" href="<?php base_url() ?>assets/pdfs/<?php echo  $value->pdf ?>"><strong style="color:#3c8dbc;">Read</strong> <?php echo "&nbsp;"; ?><img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/pdfstu.png"></a><br> <br>      

                    <?php } ?>

                             </td>



                                    <?php  if($usertype == "Admin") { ?>



                                    <td data-title="<?=$this->lang->line('action')?>">



                                        <?php echo btn_edit('subject/edit/'.$subject->subjectID."/".$set, $this->lang->line('edit')) ?>



                                        <?php echo btn_delete('subject/delete/'.$subject->subjectID."/".$set, $this->lang->line('delete')) ?>



                                    </td>



                                    <?php } ?>



                                </tr>



                            <?php $i++; }} ?>



                        </tbody>



                    </table>



                </div> -->



                <div id="hide-table">

                    <div class="container">                                          

                        <?php if(count($subjects)) {

                            $i = 1;

                            $j = 1;



                            foreach($subjects as $key2 => $subject) { 
                                

                        ?>

                        <div class="row"> 
                            <div class="col-md-1"></div>

                        <div class="col-md-10"> 



                        <table class="table table-responsive">

                            <tr>

                            	<th class="bghead"style="width:5%"><?php echo $i; ?></th>

                                <th class="col-th bghead"><h4>Subject Name </h4></th>

                                <td class="bghead" style="text-transform:capitalize;"><h4><?php echo $subject['subject']; ?></h4></td>

                            </tr>

                            <tr>

                            	<th class="col-md-1 bgonline"></th>

                                <th class="col-md-3 bgonline">Online Reader</th>

                                <td class="bgonline">

                                    <?php 

                                     if (count($subject['readerkey'])>0) {

                                        foreach ($subject['readerkey'] as $value) {?>

                                            <a target="_blank" href="<?php echo base_url() ?>online_reader/reader/<?php echo  $value['unique_id']; ?>"><img src="<?php echo base_url() ?>assets/img/diary.png" style="width: 40px;height: 40px;"> </a>

                                        <?php }  } else{?>

                                        <img src="<?php echo base_url() ?>assets/img/404.png" style="width: 40px;height: 40px;">

                                    <?php } ?> 

                                </td>

                            </tr>

                            <tr>

                            	<th class="col-md-1"></th>

                                <th class="col-md-3">Video</th>

                                <td class="col-md-8">

                                <?php 

                                  if (count($subject['videoKey'])>0) {     
                                    
                                  foreach($subject['videoKey'] as $key => $value1) { 

                                ?>

                                <!-- <a target="_blank" href="<?php echo base_url() ?>lms/document/<?php echo $value1['genID'] ?>" ><img src="<?php echo base_url() ?>assets/img/video-player.png" style="width: 40px;height: 40px;"></a> -->

                                <a href="#" class="js-video-button<?php echo $key2.$key;?>" data-channel="video" data-video-url="<?php echo base_url() ?>uploads/videos/<?php echo $value1['src'];  ?>"><img src="<?php echo base_url() ?>assets/img/video-player.png" style="width: 50px; height: 50px;"></a>

                                  &nbsp;&nbsp;&nbsp;

                                <script>
                                  $(".js-video-button<?php echo $key2.$key;?>").modalVideo({

                                    youtube:{
                                      controls:0,
                                      nocookie: true
                                    }
                                  });
                                </script>

    <!-- <video id="my-player"  class="video-js vjs-default-skin" controls preload="auto" poster="//vjs.zencdn.net/v/oceans.png" data-setup='{}'>
  <source src="<?php echo base_url() ?>uploads/10.mp4" type="video/mp4"></source> 
  <source src="<?php echo base_url() ?>uploads/videos/<?php echo $value1['src'];  ?>" type="video/webm"></source>
   <source src="<?php echo base_url() ?>uploads/10.ogg" type="video/ogg"></source> 
</video>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.3.3/video.js"></script>
 <script type="text/javascript">
     var player = videojs('my-player');
 </script>
 <canvas id="canvas" 
        width="750px" height="540px"
        style="display:block;">
</canvas>
<div id="screenShots"></div>

<script type="text/javascript">

    var video = document.getElementById("my-player");
video.addEventListener("loadedmetadata", initScreenshot);
video.addEventListener("playing", startScreenshot);
video.addEventListener("pause", stopScreenshot);
video.addEventListener("ended", stopScreenshot);

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var ssContainer = document.getElementById("screenShots");
var videoHeight, videoWidth;
var drawTimer = null;

function initScreenshot() {
  videoHeight = video.videoHeight; 
  videoWidth = video.videoWidth;
  canvas.width = videoWidth;
  canvas.height = videoHeight;
}

function startScreenshot() {
  if (drawTimer == null) {
    drawTimer = setInterval(grabScreenshot, 1000);
  }
}

function stopScreenshot() {
  if (drawTimer) {
    clearInterval(drawTimer);
    drawTimer = null;
  }
}

function grabScreenshot() {
  ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
  var img = new Image();
  img.src = canvas.toDataURL("image/png");
  img.width = 120;
  ssContainer.appendChild(img);
}
</script> -->




                                	<!-- <div class="col-md-2">

                                		<video target_="blank" id="example_video_1" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="120" height="120" poster="http://video-js.zencoder.com/oceans-clip.png" data-setup='{"example_option":true}'>

									  	<source src="<?php base_url() ?>uploads/videos/videoplayback.mp4" type="video/mp4"> 

										</video> 

									</div>&nbsp;&nbsp;&nbsp; -->

                           			<?php }  } else { ?>

                                        <img src="<?php echo base_url() ?>assets/img/404.png" style="width: 40px;height: 40px;">

                                    <?php } ?>                                                          

                                </td>

                            </tr>

                            <tr>

                            	<th class="col-md-1 bgonline"></th>

                                <th class="col-md-3 bgonline">PDF</th>

                                <td class="col-md-8 bgonline">

                                    

                                    <?php 

                                    if (count($subject['pdfKey'])>0) {

                                        foreach ($subject['pdfKey'] as $value2) {?>

                                           <a target="_blank" href="<?php echo base_url() ?>assets/pdfs/<?php echo $value2['pdf']; ?>"> <img src="<?php echo base_url() ?>assets/img/pdf.png" style="width: 40px;height: 40px;"></a>

                                            <?php }  } else{?>

                                              

                                        <img src="<?php echo base_url() ?>assets/img/404.png" style="width: 40px;height: 40px;" title="Read Now">

                                    <?php } ?>  </div>

                                    
                                </td>

                            </tr>

                        </table>
                           </div>        

                        </div> 

                        <?php $i++; }} ?>                    

                    </div>
                    <div class="col-md-1"></div>
                </div>
                </div>

                <div id="pagination">

                    <ul class="tsc_pagination pull-right">

                    <!--Show pagination links -->

                        <div class="col-md-12 text-center">

                            <?php echo $pagination; ?>

                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>



<script type="text/javascript">    

    function pdfCustomize(){

        $.ajax({

            type: 'POST',

            url: "<?=base_url('lms/delete')?>",

            data: "id=" + classesID,

            dataType: "html",

            success: function(data) {

                window.location.href = data;

            }

        });

    }

</script>





<script type="text/javascript">



    $('#classesID').change(function() {

        var classesID = $(this).val();

        if(classesID == 0) {

            $('#hide-table').hide();

        } else {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/subject_list')?>",

                data: "id=" + classesID,

                dataType: "html",

                success: function(data) {

                    window.location.href = data;

                }

            });

        }

    });



</script>



<script type="text/javascript">



    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('lms/index')?>",

            data: "yearSemesterID=" + yearSemesterID,

            dataType: "html",

            success: function(data) {

                location.reload();

            }

        });

    });



</script>



<script type="text/javascript">

    function ResetSemesterYear(){

        $.ajax({

            type: 'POST',

            url: "<?=base_url('lms/ResetSemesterYear')?>",

            data:{ResetSesession:'ResetSesession'},

            dataType: "html",

            success: function(data) {

                location.reload();

            }

        });

    }

</script>

