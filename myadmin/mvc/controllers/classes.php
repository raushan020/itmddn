<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Classes extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("classes_m");

		$this->load->model("setting_m");


		$this->load->model("student_m");
		$this->load->model("department_m");

$this->load->model("teacher_m");

	    $this->load->model("education_details_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('classes', $language);	

	}



	public function index() {

		$usertype = $this->session->userdata("usertype");



		$adminID = $this->session->userdata("adminID");



 		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Teacher" || $usertype == "Academic" || $usertype == "Super_A_P") {

 		$this->data['all_count']  =  $this->classes_m->all_count();

		$this->data['ActiveClasess_count']  =  $this->classes_m->ActiveClasess_count();

		$this->data['DraftClasess_count']  =  $this->classes_m->DraftClasess_count();

		$this->data['TrashClasess_count']  =  $this->classes_m->TrashClasess_count();

 			if ($usertype == "superadmin" ) {
			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
          }else{
          if ($this->input->post('classesID')){
		   $this->session->set_userdata('classesIDForfilterCourses', $this->input->post('classesID'));
		  }
          if ($this->input->post('departmentID')){
			$this->session->set_userdata('departmentID', $this->input->post('departmentID'));
		  }
          if ($this->input->post('durationID')){
			   $this->session->set_userdata('durationID', $this->input->post('durationID'));
		  }
if ($usertype == 'Teacher') {
	$loginuserID = $this->session->userdata('loginuserID');

	$this->data['classes'] = $this->classes_m->inner_join_with_fee_module($loginuserID);

}else{
   	$this->data['classes'] = $this->classes_m->get_classes_by_department($this->session->userdata('departmentID'));
   $this->data['department'] = $this->department_m->fetch_departments('department');
          }
if($usertype == "Super_A_P"){
	$loginuserID = $this->session->userdata('loginuserID');

	$this->data['classes'] = $this->classes_m->get_class_super_ap($loginuserID);
  // print_r($this->data['classes']);
  // exit(); 
}
          }

			$this->data["subview"] = "classes/index";



			$this->load->view('_layout_main', $this->data);



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);

		}

	}



	function ajaxCourse()

	{

	$usertype = $this->session->userdata("usertype");

	$adminID = $this->session->userdata("adminID");

	$loginuserID = $this->session->userdata("loginuserID");

	$totalData = $this->classes_m->get_cousre_no($adminID);

	$totalFiltered = $totalData;

	$posts = $this->classes_m->get_classes_byAdmin($adminID);



	$data = array();

	if (!empty($posts))

		{

		$i = 1;

		foreach($posts as $post)

			{

		
  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" ) {
			$nestedData['check'] = "<label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->classesID." '/>

                            <span class='checkmark checkmark-action-layout'></span>

                        </label>


			</td>";
		}


 $amount   =  $this->db->select('amount')->where('classesID',$post->classesID)->get('feetype')->result();
$coursefee =array();
foreach ($amount as $key => $value) {
$coursefee[] = $value->amount;
}

  $array_course   = array(0=>'none',1=>'Diploma',2=>'UG',3=>'PG');
			$nestedData['sn2'] ='';

			$nestedData['name'] = $post->courseCode;

			$nestedData['classes'] = $post->classes;

			$nestedData['duration'] = $post->duration." Year";

			$nestedData['course_type'] = $array_course[$post->course_type];

			$nestedData['mode'] = ($post->mode==1?"Year":"Semester");

			$nestedData['fee'] = $coursefee;

			$nestedData['notice'] = $post->note;

			$nestedData['department_name'] = $post->department_name;

			if ($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Super_A_P" )

				{

				if ($post->status!=2) {

                if ($post->IsSubCourse==1) {

               $add_button = btn_add('classes/manageSubCourses/' . $post->classesID, $this->lang->line('view'));

                 }else{

        	$add_button = "";

            }

				$buttons = $add_button. btn_edit('classes/edit/' . $post->classesID, $this->lang->line('edit')) . btn_delete('classes/delete/' . $post->classesID, $this->lang->line('delete'));

                }else{

                	$buttons = "<a class = 'appClassDelete' href = ".base_url()."classes/restore/".$post->classesID.">Restore</a><a class = 'appClassDelete' href = 'classes/restore/'".$post->classesID.">Delete</a>";

                }

  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
				$nestedData['action'] = $buttons;
	}

				}



			$data[] = $nestedData;

			}

		}
	$json_data = array(

		"draw" => intval($this->input->post('draw')) ,

		"recordsTotal" => $totalData,

		"recordsFiltered" => $totalFiltered,

		"data" => $data

	);

	echo json_encode($json_data);

	}

	protected function rules() {

		$rules = array(

			array(



				'field' => 'classes', 



				'label' => $this->lang->line("classes_name"), 



				'rules' => 'trim|required|xss_clean|max_length[60]'



			), 




			array(



				'field' => 'mode', 



				'label' => $this->lang->line("mode"), 



				'rules' => 'trim|xss_clean|max_length[60]'



			), 



			array(



				'field' => 'duration', 



				'label' => $this->lang->line("duration"), 



				'rules' => 'trim|required|xss_clean|max_length[60]'



			),			

			array(



				'field' => 'note', 



				'label' => $this->lang->line("classes_note"), 



				'rules' => 'trim|max_length[200]|xss_clean'



			)



		);



		return $rules;



	}



	public function add() {

		$usertype = $this->session->userdata("usertype");

        $loginuserID = $this->session->userdata("loginuserID");

        $adminID = $this->session->userdata("adminID");       

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == 'Academic' || $usertype == 'Support' ) {

			$SuperAdminId = 1;

			if($_POST) {

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) { 

					$this->data["subview"] = "classes/add";

					$this->load->view('_layout_main', $this->data);	

				} else {

				    $digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int

				    $char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string

                    $setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));

                    $sitenameString   =  $setingsData->sname; 						

 					$firstCharacter = substr($sitenameString, 0, 2);

 					$courseCode      =   strtoupper($firstCharacter).'-'.$char.$digit;

					$array = array(

						"adminID" =>$this->session->userdata("adminID"),

						"classes" => $this->input->post("classes"),

						'courseCode'=>$courseCode,

						"course_type" => $this->input->post("course_type"),

						"note" => $this->input->post("note"),

						"duration" => $this->input->post("duration"),

						"mode" => $this->input->post("mode"),

						"fee"=>$this->input->post('fee'),

						"create_date" => date("Y-m-d h:i:s"),

						"modify_date" => date("Y-m-d h:i:s"),

						"create_userID" => $this->session->userdata('loginuserID'),

						"create_username" => $this->session->userdata('username'),

						"create_usertype" => $this->session->userdata('usertype')

					);
					$insertId  =  $this->classes_m->insert_classes($array);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					if ($this->input->post('popup')) {

					redirect(base_url("databaseManagement/Courses"));

					}else{

				      redirect(base_url("classes/index"));

					}

				

				}



			} else {



				$this->data["subview"] = "classes/add";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}



	public function edit() {

		$loginuserID = $this->session->userdata("loginuserID");

		$SuperAdminId = 1;

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if((int)$id) {
				$this->data['teachers'] = $this->classes_m->get_teacher();
				$this->data['education_details'] = $this->education_details_m->get_eduction_details($SuperAdminId);
				$this->data['classes'] = $this->classes_m->get_classes($id);
				$this->data['sub_courses'] = $this->classes_m->get_SubCourses($id);
				$this->data['fee'] = $this->classes_m->find_fee($id);
				$this->data['check_classesID'] = $this->classes_m->get_check_classesID($id);
				$this->data['check_classesID_Subcourse'] = $this->classes_m->check_classesID_Subcourse($id);
				if($this->data['classes']) {
					if($_POST) {
						$rules = $this->rules();			
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
							$this->data["subview"] = "classes/edit";
							$this->load->view('_layout_main', $this->data);		
						} else {
	
							$array = array(
								"classes" => $this->input->post("classes"),
								"course_type" => $this->input->post("course_type"),
								"note" => $this->input->post("note"),
								"duration" => $this->input->post("duration"),
								"mode" => $this->input->post('mode'),
								"fee"=>$this->input->post('fee'),
								"modify_date" => date("Y-m-d h:i:s")

							);



							$this->classes_m->update_classes($array, $id);

							// $this->classes_m->update_sub_courses($id);


							$this->session->set_flashdata('success', $this->lang->line('menu_success'));



							redirect(base_url("classes/index"));



						}



					} else {



						$this->data["subview"] = "classes/edit";



						$this->load->view('_layout_main', $this->data);



					}



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);

		}



	}



	public function delete() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if((int)$id) {



				$data = array(

                'status'=>2

               );

				// $this->db->where('classesID',$id);

				// $this->db->update('classes',$data);

				$this->db->query("UPDATE classes
	left JOIN student ON classes.classesID = student.classesID
	left JOIN subject ON classes.classesID = subject.classesID 
	left JOIN sub_courses ON classes.classesID = sub_courses.classesID 
	left JOIN ets_quiz ON classes.classesID = ets_quiz.classesID
	
	 set student.status = 2 
,ets_quiz.status = 2, classes.status = 2, subject.status = 2 ,sub_courses.status = 2 where classes.classesID = $id");

				

				$data = array(

                'current_login'=>0

               );

				$this->db->where('classesID',$id);

				$this->db->update('school_sessions',$data);



				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("classes/index"));

			} else {



				redirect(base_url("classes/index"));



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);

		}



	}



	public function restore() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if((int)$id) {

$data = array(

'status'=>1

);

				// $this->db->where('classesID',$id);

				// $this->db->update('classes',$data);

$this->db->query("UPDATE classes
	left JOIN student ON classes.classesID = student.classesID
	left JOIN subject ON classes.classesID = subject.classesID 
	left JOIN sub_courses ON classes.classesID = sub_courses.classesID 
	left JOIN ets_quiz ON classes.classesID = ets_quiz.classesID
	
	 set student.status = 1 
,ets_quiz.status = 1, classes.status = 1, subject.status = 1 ,sub_courses.status = 1 where classes.classesID = $id");

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));



				redirect(base_url("classes/index"));



			} else {



				redirect(base_url("classes/index"));



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}



	public function unique_classes() {

        $adminID = $this->session->userdata("adminID");

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if((int)$id) {



			$classes = $this->classes_m->get_order_by_classes(array("classes" => $this->input->post("classes"), "classesID !=" => $id, "adminID"=>$adminID));



			if(count($classes)) {



				$this->form_validation->set_message("unique_classes", "%s already exists");



				return FALSE;



			}



			return TRUE;



		} else {



			$classes = $this->classes_m->get_order_by_classes(array("classes" => $this->input->post("classes"),"adminID"=>$adminID));



			if(count($classes)) {



				$this->form_validation->set_message("unique_classes", "%s already exists");



				return FALSE;

			}



			return TRUE;



		}	

	}



	public function unique_classes_numeric() {



		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if((int)$id) {



			$classes_numeric = $this->classes_m->get_order_by_classes(array("classes_numeric" => $this->input->post("classes_numeric"), "classesID !=" => $id));



			if(count($classes_numeric)) {



				$this->form_validation->set_message("unique_classes_numeric", "%s already exists");



				return FALSE;



			}



			return TRUE;



		} else {



			$classes_numeric = $this->classes_m->get_order_by_classes(array("classes_numeric" => $this->input->post("classes_numeric")));



			if(count($classes_numeric)) {



				$this->form_validation->set_message("unique_classes_numeric", "%s already exists");



				return FALSE;

			}



			return TRUE;



		}	

	}



	function ManageSubCourses() {

		$id  = $this->uri->segment(3);

		$this->data['sub_courses']  =  $this->classes_m->get_SubCourses($id);

		$this->data['sub_courses_trash']  =  $this->classes_m->get_SubCourses_trash($id);

		$this->data["subview"] = "classes/ManageSubCourses";

		$this->load->view('_layout_main', $this->data);

	}

	

	function addSubCourse()

	{

		$classID = $this->input->post('classID');

		$sub_course = $this->input->post('sub_course');

		if($sub_course!=null &&$classID!=null ){

			$adminID = $this->session->userdata("adminID");

			$digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int



			$char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string



			$setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$sitenameString   =  $setingsData->sname;



			$firstCharacter = substr($sitenameString, 0, 2);



			$subCourseCode      =   strtoupper($firstCharacter).'-'.$char.$digit;

			$result	= $this->classes_m->addSubCourse($classID,$sub_course,$subCourseCode); 

		}else {

			

			echo json_decode("False");

		}

	}



	function allteacher() {



		if($this->input->post('teacherID') == 0) {



			$this->form_validation->set_message("allteacher", "The %s field is required");



	     	return FALSE;

		}

		return TRUE;

	}



	function multipleAction(){

		if ($this->input->post('Delete')) {

			$checked_id =  $this->input->post('checked_id');

		for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) { 

				$data = array(

                'status'=>2

               );

				// $this->db->where('classesID',$checked_id[$i]);

				// $this->db->update('classes',$data);

	$this->db->query("UPDATE classes
	left JOIN student ON classes.classesID = student.classesID
	left JOIN subject ON classes.classesID = subject.classesID 
	left JOIN sub_courses ON classes.classesID = sub_courses.classesID 
	left JOIN ets_quiz ON classes.classesID = ets_quiz.classesID
	 set student.status = 2 
,ets_quiz.status = 2, classes.status = 2, subject.status = 2 ,sub_courses.status = 2 where classes.classesID = $checked_id[$i]");



				$data = array(

                'current_login'=>0

               );

				$this->db->where('classesID',$checked_id[$i]);

				$this->db->update('school_sessions',$data);

			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("classes/index"));

}



		if ($this->input->post('Draft')) {

			$checked_id =  $this->input->post('checked_id');

		for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) { 

				$data = array(
                'status'=>0
               );
				// $this->db->where('classesID',$checked_id[$i]);
				// $this->db->update('classes',$data);
				$this->db->query("UPDATE classes
	left JOIN student ON classes.classesID = student.classesID
	left JOIN subject ON classes.classesID = subject.classesID 
	left JOIN sub_courses ON classes.classesID = sub_courses.classesID 
	left JOIN ets_quiz ON classes.classesID = ets_quiz.classesID
	 set student.status = 0 
,ets_quiz.status = 0, classes.status = 0, subject.status = 0 ,sub_courses.status = 0 where classes.classesID = $checked_id[$i]");


				$data = array(

                'current_login'=>0

               );

				$this->db->where('classesID',$checked_id[$i]);

				$this->db->update('school_sessions',$data);


			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("classes/index"));

}

		if ($this->input->post('Active')) {

			$checked_id =  $this->input->post('checked_id');

		for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) { 

				$data = array(

                'status'=>1

               );

				// $this->db->where('classesID',$checked_id[$i]);

				// $this->db->update('classes',$data);

				$this->db->query("UPDATE classes
	left JOIN student ON classes.classesID = student.classesID
	left JOIN subject ON classes.classesID = subject.classesID 
	left JOIN sub_courses ON classes.classesID = sub_courses.classesID 
	left JOIN ets_quiz ON classes.classesID = ets_quiz.classesID
	
	 set student.status = 1 
,ets_quiz.status = 1, classes.status = 1, subject.status = 1 ,sub_courses.status = 1 where classes.classesID = $checked_id[$i]");

			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("classes/index"));

}

	}



// ajax function



function ResetCourses(){

	

$this->session->unset_userdata('classesIDForfilterCourses');	   

}

function ResetDepartmentID(){
$this->session->unset_userdata('departmentID');	  
$this->session->unset_userdata('classesIDForfilterCourses'); 

}

function ResetdurationID(){

$this->session->unset_userdata('durationID');   

}

function ActiveClasess(){

$this->session->unset_userdata('durationID');

$this->session->unset_userdata('classesIDForfilterCourses');

$this->session->unset_userdata('TrashClasess');

$this->session->unset_userdata('DraftClasess');

$this->session->unset_userdata('AllClasess');

$this->session->set_userdata('ActiveClasess',1);

}

function DraftClasess(){

$this->session->unset_userdata('durationID');

$this->session->unset_userdata('classesIDForfilterCourses');

$this->session->unset_userdata('ActiveClasess');

$this->session->unset_userdata('TrashClasess');

$this->session->unset_userdata('AllClasess');

$this->session->set_userdata('DraftClasess',3);

}

function TrashClasess(){

$this->session->unset_userdata('durationID');

$this->session->unset_userdata('classesIDForfilterCourses');

$this->session->unset_userdata('ActiveClasess');

$this->session->unset_userdata('DraftClasess');

$this->session->unset_userdata('AllClasess');

$this->session->set_userdata('TrashClasess',2);

}

function AllClasess(){

$this->session->unset_userdata('durationID');

$this->session->unset_userdata('classesIDForfilterCourses');

$this->session->unset_userdata('ActiveClasess');

$this->session->unset_userdata('DraftClasess');

$this->session->unset_userdata('TrashClasess');

$this->session->set_userdata('AllClasess',4);

}

function valid_number() {



		if($this->input->post('classes_numeric') < 0) {



			$this->form_validation->set_message("valid_number", "%s is invalid number");



			return FALSE;



		}



		return TRUE;



	}



}



/* End of file class.php */



/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */