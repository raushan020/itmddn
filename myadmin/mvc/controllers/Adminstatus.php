<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminstatus extends Admin_Controller
{
	function __construct()
    {
        parent::__construct();
		$this->load->model('user_m');
		$language = $this->session->userdata('lang');
		$this->lang->load('invoice', $language);
		require_once(APPPATH."libraries/Omnipay/vendor/autoload.php");
    }

    public function index()
    {
    	$panel=$this->input->get('panel');
    	$this->data['users']=$this->user_m->getallsuperadmin();
    	$this->data["subview"] = "clgheader/adminstatus";

		$this->load->view('_layout_main', $this->data);
    }
}