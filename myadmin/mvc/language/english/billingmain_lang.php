<?php



/* List Language  */

$lang['panel_title'] = "Super Super Admin";

$lang['add_title'] = "Add a Student";

$lang['slno'] = "#";
$lang['student_employment_status'] = "Employment Status";
$lang['father_name'] = "Father Name";
$lang['mother_name'] = "Mother Name";
$lang['nationality'] = "Nationality";
$lang['employment'] = "employment Status";
$lang['student_photo'] = "Photo";
$lang['aadhar'] = "Aadhar No.";
$lang['mode'] = "Mode";
$lang['admission_mode'] = "Admission Mode";

$lang['Semester_Year'] = "Semester/Year";
$lang['session'] = "Session";
$lang['calender_from'] = "Calender From";
$lang['Calender_To'] = "Calender To";
$lang['Academic_From'] = "Academic From";
$lang['Academic_To'] = "Academic To";
$lang['student_name'] = "Student's Name";

$lang['student_email'] = "Email";

$lang['sessionType'] = "Session Type";

$lang['examType'] = "Exam Type";

$lang['student_entry_type'] = "Entry Type";

$lang['student_dob'] = "Date of Birth";

$lang['student_sex'] = "Gender";

$lang['student_sex_male'] = "Male";

$lang['student_sex_female'] = "Female";

$lang['student_religion'] = "Religion";

$lang['student_phone'] = "Mobile No.";

$lang['student_address'] = "Address";

$lang['academic_partner'] = "Academic Partner";

$lang['street'] = "Street ";
$lang['pin'] = "Pin Code";

$lang['student_classes'] = "Course";

$lang['student_roll'] = "Roll No";

$lang['student_username'] = "Username";

$lang['student_password'] = "Password";

$lang['student_cpassword'] = "Change Password";

$lang['sub_coursesID'] = "Sub Course";

$lang['education_mode'] = "Education Mode";

$lang['student_select_class'] = "Select Course";

$lang['student_guargian'] = "Parent";

$lang['student_status'] = "Status";



/* Parent */

$lang['parent_guargian_name'] = "Guardian";

$lang['parent_father_name'] = "Father's Name";

$lang['parent_mother_name'] = "Mother's Name";

$lang['parent_father_profession'] = "Father's Profession";

$lang['parent_mother_profession'] = "Mother's Profession";

$lang['parent_email'] = "Email";

$lang['parent_phone'] = "Phone";

$lang['parent_address'] = "Address";

$lang['parent_username'] = "Username";

$lang['parent_error'] = "Parents have not been added yet! Please add parents information.";



$lang['action'] = "Action";

$lang['view'] = 'View';

$lang['print'] = 'Print';

$lang['edit'] = 'Edit';

$lang['delete'] = 'Delete';

$lang['parent'] = 'Add a Parents';

$lang['pdf_preview'] = 'PDF Preview';

$lang['idcard'] = 'ID Card';

$lang['print'] = 'Print';

$lang["mail"] = "Send Pdf to Mail";



$lang['to'] = 'To';

$lang['subject'] = 'Subject';

$lang['message'] = 'Message';

$lang['send'] = 'Send';

$lang['mail_to'] = "The To field is required.";

$lang['mail_valid'] = "The To field must contain a valid email address.";

$lang['mail_subject'] = "The Subject field is required.";

$lang['mail_success'] = 'Email send successfully!';

$lang['mail_error'] = 'oops! Email not send!';



/* Add Language */

$lang['personal_information'] = "Personal Information";

$lang['parents_information'] = "Parents Information";

$lang['add_student'] = 'Add Student';

$lang['add_parent'] = 'Add Parents';

$lang['create_date'] = 'Create Date';

$lang['counsellor'] = 'Counsellor';

$lang['update_student'] = 'Update Student';



$lang['student_section'] = 'Section';

$lang['student_select_section'] = 'Select Section';



$lang['student_all_students'] = 'All Students';









