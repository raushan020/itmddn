
function inline_edit_quiz(val,column,quiz_id,editValue){
   
          $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
            $.ajax({
                type: "POST",
                url:base_url+'exam/inline_edit_quiz',
                data:{"quiz_id":quiz_id,"column":column,"editValue":editValue},
                success: function(response) {
                  
                 $(val).css("background","#FDFDFD");
                }
                    });

          }
   
function Comman_field_quiz_setting(val,column,id,editValue){
          $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
            $.ajax({
                type: "POST",
                url:base_url+'exam/quizSetting',
                data:{"id":id,"column":column,"editValue":editValue},
                success: function(response) {
                  
                 $(val).css("background","#FDFDFD");
                }
                    });

          }
function ajaxGet_subCourses_exam(id){
 $('.showLoaderSubcour').show();
    $.ajax({
        type: "POST",
        url:base_url+"AjaxStudent/Get_subCourses_exam",
        data:{"id":id},
        success: function(response) {
        if(response=='exitPanel'){
            window.location.href=base_url+"signin/index?redirect="+redirecturi;
        }
            $("#subCourseID").html(response);
            $('.showLoaderSubcour').hide();
        }
            });

        $.ajax({
        type: "POST",
        url:base_url+"AjaxStudent/fetch_SemesterYear",
        data:{"id":id},
        success: function(response) {
        if(response=='exitPanel'){
            window.location.href=base_url+"signin/index?redirect="+redirecturi;
        }
            $("#yearAndSemester").html(response);
        }
    }); 


}

  function AllExams(){
     $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"exam/AllExams",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

        function ActiveExams(){
             $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"exam/ActiveExams",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
        function DraftExams(){
             $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"exam/DraftExams",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
        function TrashExams(){
             $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"exam/TrashExams",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.mycheckbox').each(function(){
                this.checked = true;
                $(".etsfilertButton").removeClass("disabled");
                $(".etsfilertButton").removeAttr("disabled");
            });
        }else{
             $('.mycheckbox').each(function(){
                this.checked = false;
                $(".etsfilertButton").addClass("disabled");
                $(".etsfilertButton").attr("disabled", true);
            });
        }
    });
    $('.table').on('click', '.mycheckbox', function(){

        if($('.mycheckbox:checked').length == $('.mycheckbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
         if($('.mycheckbox:checked').length >=1){
    $(".etsfilertButton").removeClass("disabled");
     $(".etsfilertButton").removeAttr("disabled");
}else{
$(".etsfilertButton").addClass("disabled");
$(".etsfilertButton").attr("disabled", true);
}
    });
});