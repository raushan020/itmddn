<?php



/* List Language  */

$lang['panel_title'] = "Course View";

$lang['fee'] = "Course Fee";

$lang['mode'] = "Course Mode";

$lang['duration'] = "Course Duration";


$lang['add_title'] = "Add a course";

$lang['slno'] = "#";

$lang['courses_name'] = "Course Name";

$lang['courses_numeric'] = "Course Numeric";

$lang['teacher_name'] = "Teacher Name";

$lang['courses_note'] = "Note";

$lang['action'] = "Action";

$lang['subcourse'] = "Sub Courses";

$lang['courses_select_teacher'] = "Select Teacher";

$lang['view'] = 'View';

$lang['edit'] = 'Edit';

$lang['delete'] = 'Delete';



/* Add Language */



$lang['add_course'] = 'Add Course';

$lang['update_course'] = 'Update course';