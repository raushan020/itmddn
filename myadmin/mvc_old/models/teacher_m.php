<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class teacher_m extends MY_Model {



	protected $_table_name = 'teacher';



	protected $_primary_key = 'teacherID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "name asc";



	function __construct() {



		parent::__construct();



	}



	function get_username($table, $data=NULL) {



		$query = $this->db->get_where($table, $data);



		return $query->result();



	}



	function get_teacher($array=NULL, $signal=FALSE) {



		$query = parent::get($array, $signal);

		return $query;

	}

function get_teacher_all_super_AP($loginuserID){

	$this->db->where('status',1);

	$this->db->where('spa',$loginuserID);

	  return $this->db->get('teacher')->result();

 }
	

 function get_teacher_all($adminID){

	$this->db->where('status',1);

	$this->db->where('adminID',$adminID);

	  return $this->db->get('teacher')->result();

 }

  function get_teacher_single($id){
    $this->db->from('teacher');
   // $this->db->join('user','user.userID = teacher.spa','left');
	$this->db->where('teacher.status',1);
	$this->db->where('teacher.teacherID',$id);
	  return $this->db->get()->row();

 }

   function passbook_balance($id){
    $this->db->from('passbook');
	$this->db->where('teacherID',$id);
	$this->db->limit(1);
	  return $this->db->get()->row();

 }
   
   function get_teacher_by_rm($loginuserID)
   {
   	$this->db->where('rm',$loginuserID);
   	$query = $this->db->get('teacher')->result();
   	return $query;
   }

  function total_balance_passbook($id){

	  $this->db->where('teacherID',$id);
	  $this->db->limit(1);
	  $this->db->order_by('passbookID','desc');
	  return $this->db->get('passbook')->row();

 }

 function get_passbook_all($teacherID){
$this->db->order_by('passbookID','desc');
$this->db->where('teacherID',$teacherID);
return $this->db->get('passbook')->result();

 }



	function get_teacher_ClgAdmin() {
        $usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		 if($usertype == 'Super_A_P'){
		 	$loginuserID = $this->session->userdata("loginuserID");
		 	 $this->db->where('spa',$loginuserID);
		 }else{
		 	$this->db->where('adminID',$adminID);
		 }
	    

	    $this->db->where('status',1);

		if(isset($_POST["search"]["value"]))  

           {  



			$searches  =  $_POST["search"]["value"];



			$order_column = array( 



                            0 =>'sn', 

                            1 =>'name',

                            2 => 'username',

                            3 => 'email',

                            4=> 'action'

                        );



           $where = "(teacher.name LIKE '%$searches%' OR teacher.username LIKE '%$searches%' OR teacher.email LIKE '%$searches%')";

           $this->db->where($where);

        

           

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('teacher.teacherID', 'DESC');  

           } 

          

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       } 



	}

	
function get_paid_students($id){
$this->db->select('student.name,student.studentID,student.father_name,invoice.amount,invoice.paidamount,invoice.status as invoice_status,teacher.name as TeacherName,classes.classes,student.yearsOrSemester');
$this->db->from('student_x_passbook');
$this->db->join('student','student_x_passbook.studentID=student.studentID','inner');
$this->db->join('teacher','student.counsellor=teacher.teacherID','inner');
$this->db->join('classes','student.classesID=classes.classesID','inner');
$this->db->join('invoice','invoice.studentID=student.studentID','inner');	
$this->db->where('student_x_passbook.passbookID',$id);

return $this->db->get()->result();


}
	   function make_datatables(){  

        $this->get_teacher_ClgAdmin();  

   	    $query = $this->db->get('teacher'); 

		return $query->result();  

      } 

	

	function get_teacher_no($adminID)

	{
		 $usertype = $this->session->userdata("usertype");
		

		 if($usertype == 'Super_A_P'){
		 	$loginuserID = $this->session->userdata("loginuserID");
		 	 $this->db->where('spa',$loginuserID);
		 }else{
		 	$this->db->where('adminID',$adminID);
		 }

		// raushan

		 if(isset($_POST["search"]["value"]))  

            {  



		 	$searches  =  $_POST["search"]["value"];



		 	$order_column = array( 



                             0 =>'sn', 

                             1 =>'name',

                             2 => 'username',

                             3 => 'email',

                             4=> 'action'

                         );



            $where = "(teacher.name LIKE '%$searches%' OR teacher.username LIKE '%$searches%' OR teacher.email LIKE '%$searches%')";

          $this->db->where($where);

       }

		 	// raushan end



		$this->db->where('adminID',$adminID);

		   $this->db->where('status',1);

	    $query = $this->db->get('teacher'); 

		return $query->num_rows();

	}


 
	function get_teacher_ClgAdminID($adminID) {

	$this->db->where('adminID',$adminID);

	   $this->db->where('status',1);

	$query = $this->db->get('teacher');

	 return $query->result();

	}



	function get_order_by_teacher($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}



	function get_single_teacher($array) {



		$query = parent::get_single($array);



		return $query;



	}

   function map_teacher_fee($classes_array,$feetypeID,$teacherID){
   	$b=array();
   	for($a=0; $a < count($feetypeID); $a++)
   	{
   		if($feetypeID[$a]==0)
   		{

   		}
   		else
   		{
   			$b[]=$feetypeID[$a];
   		}
   	}

for ($i=0; $i <count($classes_array) ; $i++) { 

$data = array(
	'classesID' =>$classes_array[$i] ,
	'feetypeID' =>$b[$i],
	'teacherID' =>$teacherID 
);
// print_r($b);die;
$this->db->insert('fee_x_classes_ap',$data);

}

   }


function inner_join_with_fee_module($teacherID,$department){
	$this->db->select('*');
	$this->db->from('classes');
	$this->db->join('fee_x_classes_ap', 'classes.classesID = fee_x_classes_ap.classesID', 'INNER');
	$this->db->where('fee_x_classes_ap.teacherID',$teacherID);
	$this->db->where('classes.departmentID',$department);
	$this->db->where('classes.status',1);
return	 $this->db->get()->result();
}

function inner_join_with_fee_module_with_student($departmentID){
	$this->db->select('*');
	$this->db->from('classes');
	$this->db->join('department', 'classes.departmentID = department.departmentID', 'INNER');
	$this->db->where('department.departmentID',$departmentID);
	$this->db->where('classes.status',1);
return	 $this->db->get()->result();
}


function update_map_teacher_fee($classes_array,$feetypeID,$teacherID){
$this->db->where('teacherID',$teacherID);
$this->db->where_not_in('classesID',$classes_array);
$this->db->delete('fee_x_classes_ap');

   	$b=array();
   	for($a=0; $a < count($feetypeID); $a++)
   	{
   		if($feetypeID[$a]==0)
   		{

   		}
   		else
   		{
   			$b[]=$feetypeID[$a];
   		}
   	}

for ($i=0; $i <count($classes_array) ; $i++) { 

$numb= $this->db->where('classesID',$classes_array[$i])->where('teacherID',$teacherID)->get('fee_x_classes_ap')->num_rows();
if($numb==0){
$data = array(
	'classesID' =>$classes_array[$i] ,
	'feetypeID' =>$b[$i],
	'teacherID' =>$teacherID 
);
$this->db->insert('fee_x_classes_ap',$data);
}else{
$data = array(
'feetypeID' =>$feetypeID[$i],
);
$this->db->where('classesID',$classes_array[$i]);
$this->db->where('teacherID',$teacherID);
$this->db->update('fee_x_classes_ap',$data);

}

}

}


   function teacher_x_fee($id){
return $this->db->where('teacherID',$id)->get('fee_x_classes_ap')->result();

   }

      function teacher_x_fee_single($id,$classesID){
return $this->db->where('teacherID',$id)->where('classesID',$classesID)->get('fee_x_classes_ap')->row();

   }
    function fee_single($feetypeID){
return $this->db->where('feetypeID',$feetypeID)->get('feetype')->row();

   }



	function insert_teacher($array) {


		$error = parent::insert($array);

		return $error;




	}



	function update_teacher($data, $id = NULL) {



		parent::update($data, $id);



		return $id;



	}





	function delete_teacher($id){



$data  = array(

	'status' =>2,

	 );

	   // $this->db->where('teacherID',$id);

	   // $this->db->update('teacher',$data);

$this->db->query("UPDATE teacher
	INNER JOIN student ON teacher.teacherID = student.counsellor
	
	
	set student.status = 2, teacher.status = 2 where teacher.teacherID = $id");

	}



	function hash($string) {



		return parent::hash($string);



	}



}



/* End of file teacher_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/teacher_m.php */



