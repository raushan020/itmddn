<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Online_reader_m extends MY_Model {

	protected $_table_name = 'student';

	protected $_primary_key = 'studentID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "roll asc";


	function __construct() {

		parent::__construct();

	}


	public function getonlinereaderpass($loginuserID,$id){
		
		
		$this->db->where('epubid',$id);
		$this->db->where('studentID',$loginuserID);
		$rowdata = $this->db->get('onlineReader_status')->row();
		
if ($rowdata) {

		$timeArray = explode(',',$rowdata->individual_time);
		$wordArray = explode(',',$rowdata->words_count);
		$percentage = 0;
		//print_r($timeArray);
		//exit();
		for ($i=0; $i < count($timeArray); $i++) { 
			for ($j=0; $j < count($wordArray); $j++) { 
				if ($i==$j) {

					$totalTime = $wordArray[$j]*0.1;
					if ($totalTime == 0) {
						$percentage += 0;
					}else{
						$percentage +=  ($timeArray[$i]*100)/$totalTime;
					}
				}
			}
		}


		$realActivity = round($percentage)/$rowdata->chapter_count;
return round($realActivity);
		
	}else{
	return 0;	
	}
}


	public function getdetail_validation_epub($subjectID,$uri4){
		$this->db->where('subjectID',$subjectID);
		$this->db->where('id',$uri4);
		
		return $this->db->get('epub',1)->row();
	}
}