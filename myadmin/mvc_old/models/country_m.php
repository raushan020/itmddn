<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class country_m extends MY_Model {



	protected $_table_name = 'countries';

	protected $_primary_key = 'num_code';

	protected $_primary_filter = 'intval';

	protected $_order_by = "en_short_name asc";



	function __construct() {

		parent::__construct();

	}

	function get_country($array=NULL, $signal=FALSE) {

		$query = parent::get($array, $signal);

		return $query;

	}


	function get_order_by_alert($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}



	function insert_alert($array) {

		$error = parent::insert($array);

		return TRUE;

	}



	function update_alert($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}



	public function delete_alert($id){

		parent::delete($id);

	}





	



}



/* End of file alert_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/alert_m.php */