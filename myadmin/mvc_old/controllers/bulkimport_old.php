<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulkimport extends Admin_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->load->model("teacher_m");
        $this->load->model("parentes_m");
        $this->load->model("student_m");
        $this->load->model("invoice_m");
        $this->load->model("payment_m");
        $this->load->model("subject_m");
        $this->load->model("user_m");
        $this->load->model("book_m");
        $this->load->model("admin_m");
        $this->load->model("exam_m");
        $this->lang->load('bulkimport', $language);
        $this->load->library('csvimport');
    }
    
    public function index()
    {
        
        
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Teacher" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support") {
            $this->data["adminData"] = $this->admin_m->get_systemadmin();
            $this->data["subview"]   = "bulkimport/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }
    public function unique_email()
    {
        $this->db->select('email');
        $query          = $this->db->get('teacher');
        $teacher_emails = $query->result();
        $this->db->select('email');
        $query1     = $this->db->get('student');
        $std_emails = $query1->result();
        $this->db->select('email');
        $query2        = $this->db->get('parent');
        $parent_emails = $query2->result();
        $this->db->select('email');
        $query3      = $this->db->get('user');
        $user_emails = $query3->result();
        $this->db->select('email');
        $query4             = $this->db->get('systemadmin');
        $systemadmin_emails = $query4->result();
        $emails             = array_merge($teacher_emails, $std_emails, $parent_emails, $user_emails, $systemadmin_emails);
        $result             = array();
        foreach ($emails as $key => $value) {
            array_push($result, $value->email);
        }
        return $result;
    }
    public function unique_username()
    {
        $this->db->select('username');
        $query             = $this->db->get('teacher');
        $teacher_usernames = $query->result();
        $this->db->select('username');
        $query1        = $this->db->get('student');
        $std_usernames = $query1->result();
        $this->db->select('username');
        $query2           = $this->db->get('parent');
        $parent_usernames = $query2->result();
        $this->db->select('username');
        $query3         = $this->db->get('user');
        $user_usernames = $query3->result();
        $this->db->select('username');
        $query4                = $this->db->get('systemadmin');
        $systemadmin_usernames = $query4->result();
        $usernames             = array_merge($teacher_usernames, $std_usernames, $parent_usernames, $user_usernames, $systemadmin_usernames);
        $result                = array();
        foreach ($usernames as $key => $value) {
            array_push($result, $value->username);
        }
        return $result;
    }
  
    public function student_bulkimport()
    {
                
        $usertype = $this->session->userdata("usertype");
        $adminID  = $this->session->userdata("adminID");
        if ($usertype == "Admin" || $usertype == "Teacher" || $usertype == "superadmin" || $usertype == "ClgAdmin") {
            if (isset($_FILES["csvStudent"])) {
                // print_r($_FILES["csvStudent"]);
                // exit();
                $config['upload_path']   = "./uploads/csv/";
                $config['allowed_types'] = 'text/plain|text/csv|csv';
                $config['file_name']     = $_FILES["csvStudent"]['name'];
                $config['max_size']      = "2048000000";
                $config['overwrite']     = TRUE;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload("csvStudent")) {
                    $this->session->set_flashdata('error', $this->lang->line('import_error'));
                    redirect(base_url("bulkimport/index"));
                } else {
                    $file_data = $this->upload->data();
                    $file_path = './uploads/csv/' . $file_data['file_name'];
                    
                    if ($this->csvimport->get_array($file_path)) {
                        $csv_array = $this->csvimport->get_array($file_path);
// var_dump($csv_array);
// exit();
                        foreach ($csv_array as $key => $row){
                
                            if (!isset($row['Name'])) {
                               echo $this->session->set_flashdata('error', 'Oops Name Are Missing!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Registration_ID'])) {
                               echo $this->session->set_flashdata('error', 'Oops Registration id Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Roll_number'])) {
                               echo $this->session->set_flashdata('error', 'Oops Roll number Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['FatherName'])) {
                                echo $this->session->set_flashdata('error', 'Oops Father name Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['MotherName'])) {
                                echo $this->session->set_flashdata('error', 'Oops Mother Name Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Dob'])) {
                                echo $this->session->set_flashdata('error', 'Oops Date of Birth Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Gender'])) {
                                echo $this->session->set_flashdata('error', 'Oops Gender Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Address'])) {
                                echo $this->session->set_flashdata('error', 'Oops Address Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Pincode'])) {
                                echo $this->session->set_flashdata('error', 'Oops Pincode Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Nationality'])) {
                                echo $this->session->set_flashdata('error', 'Oops Nationality Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Aadhar'])) {
                                echo $this->session->set_flashdata('error', 'Oops Aadhar Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Photo'])) {
                                echo $this->session->set_flashdata('error', 'Oops Photo Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Course'])) {
                                echo $this->session->set_flashdata('error', 'Oops Course Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['student_status'])) {
                               echo $this->session->set_flashdata('error', 'Oops Student Status Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Session'])) {
                                echo $this->session->set_flashdata('error', 'Oops Session Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Phone'])) {
                                echo $this->session->set_flashdata('error', 'Oops Phone Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Email'])) {
                                echo $this->session->set_flashdata('error', 'Oops Email Are Mismatch!!! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['TotalTuitionFees'])) {
                                echo $this->session->set_flashdata('error', 'Oops Total Tuiton Fee Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['PaymentReceived'])) {
                                echo $this->session->set_flashdata('error', 'Oops Payment Recieved Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Paymenttype'])) {
                               echo $this->session->set_flashdata('error', 'Oops Payment Type Are Mismatch! Please Try Again.');
                               redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Paymentdate'])) {
                               echo $this->session->set_flashdata('error', 'Oops Payment Date Are Mismatch! Please Try Again.');
                               redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['10th'])) {
                                echo $this->session->set_flashdata('error', 'Oops 10th Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['12th'])) {
                               echo $this->session->set_flashdata('error', 'Oops 12th Are Mismatch! Please Try Again.');
                               redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Graduation'])) {
                                echo $this->session->set_flashdata('error', 'Oops Graduation Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['PostGraduation'])) {
                                echo $this->session->set_flashdata('error', 'Oops Post Graduation Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Receipt_No'])) {
                                echo $this->session->set_flashdata('error', 'Oops Receipt Number Field Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['TotalDiscount'])) {
                                echo $this->session->set_flashdata('error', 'Oops Discount Field Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['Lf_No'])) {
                                echo $this->session->set_flashdata('error', 'Oops Discount Field Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['StudentWelfareFund'])) {
                                echo $this->session->set_flashdata('error', 'Oops Student Welfare fund Field Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (!isset($row['TotalHostelFee'])) {
                                echo $this->session->set_flashdata('error', 'Oops Hostel Fee Field Are Mismatch! Please Try Again.');
                                redirect(base_url("bulkimport/index"));
                            }



                            if (!empty($row['Registration_ID'])) {                             
                            if (empty($row['Name'])) {
                             
                                echo $this->session->set_flashdata('error', $row['Registration_ID']."  ".'Oops Name is blank! Please Fill Name Field.');
                                redirect(base_url("bulkimport/index"));

                               

                            }elseif (empty($row['Registration_ID'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Registration Id is blank! Please Fill Registration Id Field.');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (empty($row['Dob'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Date of birth is blank! Please Fill Date of birth Field.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['Course'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Course is blank! Please Fill Course Field.');
                                redirect(base_url("bulkimport/index"));
                            }
                            // elseif (empty($row['student_status'])) {
                            //     echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Student Status is blank! Please Fill Student Status Field.');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            elseif (empty($row['Session'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Sesion is blank! Please Fill Sesion Field.');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['TotalTuitionFees'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Total tuition Fees is blank! Please Fill Total Fees Field.');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (empty($row['Paymentdate'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Payment Date is blank! Please Fill Payment Date Field.');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (empty($row['Paymenttype'])){
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Payment Type is blank! Please Fill Payment type Field.');
                                redirect(base_url("bulkimport/index"));
                            }
                            
                         
                                  
                   

                 //  if (($row['Examtype']!='y') && ($row['Examtype']!='o')){
                 // $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Exam Type Should be y Or o.');
                 //    redirect(base_url("bulkimport/index"));
                 //  }

                    

                // if (($row['SessionType']!='A') && ($row['SessionType']!='C') && ($row['SessionType']!='AC')){
                //  $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Session Type Should be A, C Or AC');
                //     redirect(base_url("bulkimport/index"));
                // }


                if (($row['Paymenttype']!='cash') && ($row['Paymenttype']!='Cheque') && ($row['Paymenttype']!='Bank') && ($row['Paymenttype']!='Draft') && ($row['Paymenttype']!='PayuMoney') && ($row['Paymenttype']!='onlinePayment')){
                    $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Payment Type Should be Cash, Cheque, Bank, Draft, PayuMoney, onlinePayment');
                    redirect(base_url("bulkimport/index"));
                }


                            $findsingleStudent = $this->student_m->get_student(array(
                                'registration_ID' => $row['Registration_ID']
                            ));                            
                            
                            $ClassesRow = $this->getClass($row['Course'], $adminID);

                            if ($ClassesRow->mode==1) {
                            $semester = '';   
                            $sessionArray  = explode('-', $row['Session']);

                            $startYear  = $sessionArray[0];
                            $endYear  = $sessionArray[1];

                            $StartYearobject = new DateTime($startYear.'-01-06');
                            $endYearObject = new DateTime($endYear.'-01-06');

                            $diff = $endYearObject->diff($StartYearobject);

                            $duration = $diff->y;

                            $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                            $d2 = new DateTime('now');

                            $diff2 = $d2->diff($d1);

                            $findYears =  $diff2->y;

                            if ($findYears>=$duration) {

                            $year = CallYears($duration);

                            }else{
                                $findYearsAddOne = $findYears+1;
                                $year = CallYears($findYearsAddOne);

                            }

                            }

                            else{
                            $year = '';
                            $sessionArray  = explode('-', $row['Session']);

                            $startYear  = $sessionArray[0];
                            $endYear  = $sessionArray[1];

                            $StartYearobject = new DateTime($startYear.'-01-06');
                            $endYearObject = new DateTime($endYear.'-01-06');

                            $diff = $endYearObject->diff($StartYearobject);

                            $duration = $diff->y;
                            $Durationsemester = $duration*2;

                            $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                            $d2 = new DateTime('now');

                            $date1 = $startYear.'-06-01';
                            $date2 = date("Y-m-d");

                            $ts1 = strtotime($date1);
                            $ts2 = strtotime($date2);

                            $year1 = date('Y', $ts1);
                            $year2 = date('Y', $ts2);

                            $month1 = date('m', $ts1);
                            $month2 = date('m', $ts2);

                            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

                            $totalSemester =  floor($diff/6);

                            $semesterRemainder = fmod($diff,6);
                            if ($totalSemester>=$Durationsemester) {
                             $semester  = CallSemester($Durationsemester);
                            }else{
                            if ($semesterRemainder) {
                             $semeterAddone = $totalSemester+1;
                            $semester       = CallSemester($semeterAddone);
                            }else{
                              $semester = CallSemester($totalSemester);
                            }
                            }
                            }

                            if ($ClassesRow->mode==1) {
                            $yearsOrSemester = $year;
                            }else{
                            $yearsOrSemester = $semester;  
                            }

                            if (empty($ClassesRow) || $ClassesRow == 'error') {
                                $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'course code is empty or blank');
                                redirect(base_url("bulkimport/index"));
                            }
                            if ($ClassesRow->IsSubCourse == 0) {
                                $subCoursesID = 0;
                         
                            } else {
                                                           
                                if ($row['SubCourse'] == '') {    
                                    $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Subcourse code is empty');
                                    redirect(base_url("bulkimport/index"));
                                    
                                }else{
                                        $subCoursesID              = $this->getSubCourse($row['SubCourse'], $adminID);

                                        $subCoursecheckwithClasses = $this->subCoursecheckwithClasses($row['SubCourse'], $ClassesRow->classesID, $adminID);



                                    if ($subCoursecheckwithClasses==0){
                                        $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Subcourse code is mismatch');
                                        redirect(base_url("bulkimport/index"));
                                    }
                                }
                            }
                            
                            $student_status  =  strtolower($row['student_status']);
           
                            $explodeSession = explode('-', $row['Session']);

                        if (count($explodeSession)!=2) {
                              $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session format is wrong.  Please try again.');
                                redirect(base_url("bulkimport/index"));
                        }
                            for( $m = 2000; $m < 2025; $m++ ){
                             $yearsArray[]  =  $m;
                            }
     
                          
                            if (in_array($explodeSession[0], $yearsArray) == false) {
                                
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                               

                            }
                            if (in_array($explodeSession[1], $yearsArray) == false) {
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                            }


                            // if (($row['SessionType']!='A') && ($row['SessionType']!='C') && ($row['SessionType']!='A-C')){
                            //     $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session Type is mismatch Please try again');
                            //     redirect(base_url("bulkimport/index"));
                            // }

                                $payment_recive =  $row['PaymentReceived']+$row['payment2']+$row['payment3'];
                            if ($row['TotalTuitionFees']<$payment_recive) {
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Payment Recieved is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                            }

                            $totalfeesnum = $row['TotalTuitionFees'];
                            

                            $paymentfeesnum = $payment_recive;
                            

                            $intger = intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
                            // random(ish) 5 digit int
                            
                            
                            $usernamePlusName = $row['Registration_ID'];
                            $slashToHypne     = str_replace('/', '-', $row['Dob']);
                            $dobIndian        = date("d-m-Y", strtotime($row['Dob']));
                            $passwordPlusName = str_replace('-', '', $dobIndian);
                            // $passwordPlusName = $row['Enrollment'];
                            $dob = date("Y-m-d", strtotime($row['Dob']));
                            
                            if (!empty($row['Phone'])) {
                                $phoneArray = explode('/', preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Phone']));
                                $mainPhone  = $phoneArray[0];
                            } else {
                                $mainPhone = '';
                            }
                            if (!empty($row['Email'])) {
                                $EmailArray = explode('/', $row['Email']);
                                $mainEmail  = $EmailArray[0];
                            } else {
                                $mainEmail = '';
                            }
                            
                            
                                                        
                            
                            $newstydentstatus = strtolower($row['student_status']);
                            $afterName = ucwords($row['Name']);
                                       
                            $afterMotherName = ucwords($row['MotherName']);
                            $afterFatherName = ucwords($row['FatherName']);

                                if (count($findsingleStudent)>0) {
                                    $img_obserb = $findsingleStudent->photo;
                                }else{
                                    $img_obserb ='defualt.png';
                                }

                            $insert_data = array(
                                'adminID' => $adminID,
                                'name' => preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $afterName),
                                'mother_name' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $afterMotherName),
                                'father_name' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $afterFatherName),
                                'dob' => $dob,
                                'sex' => $row['Gender'],
                                'email' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $mainEmail),
                                'phone' => $mainPhone,
                                'address' => $row['Address'],
                                'aadhar' => $row['Aadhar'],
                                'nationality' => $row['Nationality'],
                                'pin' => $row['Pincode'],
                                'education_mode' => $ClassesRow->mode,
                                'student_status' => $newstydentstatus,
                                'session' => $row['Session'],
                                'classesID' => $ClassesRow->classesID,
                                'sub_coursesID' => $subCoursesID,
                                'roll' => $row['Roll_number'],
                                'enrollment' => $row['Enrollment'],
                                'username' => $usernamePlusName,
                                'password' => $this->student_m->hash($passwordPlusName),
                                'usertype' => 'Student',
                                "yearsOrSemester"=>$yearsOrSemester,
                                'library' => 0,
                                'hostel' => 0,
                                'transport' => 0,
                                'photo' => $img_obserb,
                                'yearID' => return_year($yearsOrSemester),
                                'year' => date('Y'),
                                'extraPhone' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['Phone']),
                                'extraEmail' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['Email']),
                                "create_date" => date("Y-m-d h:i:s"),
                                "yosPosition" => $year,
                                "modify_date" => date("Y-m-d h:i:s"),
                                "create_userID" => $this->session->userdata('loginuserID'),
                                "create_username" => $this->session->userdata('username'),
                                "create_usertype" => $this->session->userdata('usertype'),
                                "studentactive" => 1,                                
                                'registration_ID' => $row['Registration_ID']
                            );
                            
                            // var_dump($insert_data);
                            $Education['tenth']          = explode(',', $row['10th']);
                            $Education['twelth']         = explode(',', $row['12th']);
                            $Education['Graduation']     = explode(',', $row['Graduation']);
                            $Education['PostGraduation'] = explode(',', $row['PostGraduation']);
                            $Education['Other']          = explode(',', $row['Other']);
                             
                if (($student_status=='fresher') || ($student_status=='credit_transfer') || ($student_status=='lateral') || ($student_status=='fast_track')){

                            if (count($findsingleStudent) > 0) {
                                $insetID = $this->student_m->update_student_byCSV($insert_data, $Education, $findsingleStudent->studentID);
                            } else {
                                $insetID = $this->student_m->insert_student_byCSV($insert_data, $Education);
                            }

                            }else{

                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'please check Status -> fresher, lateral, credit_transfer, fast_track');
                                                                    redirect(base_url("bulkimport/index"));

                            }                            
                            
                            $dueFee = $row['TotalTuitionFees'] - $payment_recive;
                            
                            if ($dueFee == 0) {
                                $feeStatus = 2;
                            }
                            if ($dueFee > 0 and $dueFee < $row['TotalTuitionFees']) {
                                $feeStatus = 1;
                            }
                            if ($dueFee == $row['TotalTuitionFees']) {
                                $feeStatus = 0;
                            }
                        $totalTuitionfee = $row['TotalTuitionFees']-$row['TotalDiscount'];
                        $totalWelfarefund = $row['StudentWelfareFund']*$ClassesRow->duration;
                        $totalAmount = $totalWelfarefund + $totalTuitionfee;

                        $totalAmountpaidperyear = $totalAmount/$ClassesRow->duration;          
    
                            $InsertDataInvoice = array(
                                "adminID" => $adminID,
                                "studentID" => $insetID,
                                "discount" => round($row['TotalDiscount']),
                                "amount" => round($totalAmount),
                                "tuitionFee"=>round($row['TotalTuitionFees']),
                                "totalfeepaidperyear"=>floor($totalAmountpaidperyear),
                                "paidamount" =>round($payment_recive),
                                "lf_no"=> $row['Lf_No'],
                                "hostelFee"=> round($row['TotalHostelFee']),
                                "usertype" =>$usertype,
                                "status" => $feeStatus,
                                "welfareFund"=>round($row['StudentWelfareFund']),
                                "paymenttype" => "Cash"
                            );

                           
                            $UpdateDataInvoice = array(
                               "discount" => round($row['TotalDiscount']),
                                "amount" => $totalAmount,
                                "tuitionFee"=>round($row['TotalTuitionFees']),
                                "totalfeepaidperyear"=>floor($totalAmountpaidperyear),
                                "paidamount" =>$payment_recive,
                                "lf_no"=> $row['Lf_No'],
                                "hostelFee"=> round($row['TotalHostelFee']),
                                "welfareFund"=>round($row['StudentWelfareFund']),
                                "usertype" => $usertype,
                                "status" => $feeStatus,
                                "paymenttype" => "Cash"
                            );
                            
                            if (count($findsingleStudent) > 0) {
                                $insertInvoice = $this->invoice_m->update_invoice_byCsv($UpdateDataInvoice, $findsingleStudent->studentID);
                            } else {
                                $insertInvoice = $this->invoice_m->insert_invoice($InsertDataInvoice);
                            }

                            $payDate =date('Y-m-d',strtotime($row['Paymentdate']));

                            $payDateM = date('m',strtotime($row['Paymentdate']));
                            $payDateY = date('Y',strtotime($row['Paymentdate']));

                            $payment_array = array(
                                
                                "invoiceID" => $insertInvoice,
                                
                                "adminID" => $adminID,
                                
                                "studentID" => $insetID,
                                'receipt'=>$row['Receipt_No'],
                                "paymentamount" =>round($payment_recive),
                                
                                "paymenttype" => $row['Paymenttype'],
                                
                                "paymentdate" => $payDate,

                                "feeType" =>'clg',
                                
                                "paymentmonth" =>  $payDateM,
                                "uname"=>$this->session->userdata('username'),
                                "paymentyear" =>$payDateY,
                                "paymentdateUpdate"=>date('Y-m-d'),
                                "yearsOrSemester"=>return_year($yearsOrSemester),
                                "receipt_src"=>" ",
                                "userID"=>$this->session->userdata('loginuserID'),
                                "data_source" => '1',
                                "usertype"=>$this->session->userdata('usertype')
                                
                            );

                 			$Updatepayment_array = array(
                                'receipt'=>$row['Receipt_No'],
                                "paymentamount" =>round($payment_recive),                               
                                "paymenttype" => $row['Paymenttype'],                                
                                "paymentdate" => $payDate,
                                "feeType" =>'clg',                                
                                "paymentmonth" =>  $payDateM,
                                "uname"=>$this->session->userdata('username'),
                                "paymentyear" =>$payDateY,
                                "paymentdateUpdate"=>date('Y-m-d'),
                                "yearsOrSemester"=>return_year($yearsOrSemester),
                                "receipt_src"=>" ",
                                "userID"=>$this->session->userdata('loginuserID'),
                                "data_source" => '1',
                                "usertype"=>$this->session->userdata('usertype')
                                
                            );
                            // $this->payment_m->insert_payment($payment_array);
                            if (count($findsingleStudent) > 0) {
                                $this->payment_m->update_paymentCsv($Updatepayment_array, $findsingleStudent->studentID,return_year($yearsOrSemester));
                            } else {
                                
                                $this->payment_m->insert_payment($payment_array);
                            }
                            
                        }
                    }
                        $this->session->set_flashdata('success', $this->lang->line('import_success'));
                        redirect(base_url("bulkimport/index"));
                    } else {
                    
                        $this->session->set_flashdata('error', $this->lang->line('import_error'));
                        redirect(base_url("bulkimport/index"));
                    }
                    
                }
            } else {
               
                $this->session->set_flashdata('error', $this->lang->line('import_error'));
                redirect(base_url("bulkimport/index"));
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('unauthorised'));
            redirect(base_url("bulkimport/index"));
        }
    }
    
    
    
    
    
    
    
    public function subCoursecheckwithClasses($sub_coursesCorsesCode,$classesID,$adminID)
    {
        
        $this->db->where('adminID', $adminID);
        $this->db->where('classesID', $classesID);
        $this->db->where('subCourseCode', $sub_coursesCorsesCode);
        $query = $this->db->get('sub_courses');
        return $query->num_rows();
        
        
    }
    public function quiz_bulkimport(){
        $adminID = $this->session->userdata('adminID');
        
        if (isset($_FILES["csvQuestion"])) {
            $config['upload_path']   = "./uploads/csv/";
            $config['allowed_types'] = 'text/plain|text/csv|csv';
            $config['max_size']      = '2048';
            $config['file_name']     = $_FILES["csvQuestion"]['name'];
            $config['overwrite']     = TRUE;
           
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("csvQuestion")) {
                $this->session->set_flashdata('error', $this->lang->line('import_error'));
                redirect(base_url("bulkimport/index"));
            } else {
                $file_data = $this->upload->data();
                $file_path = './uploads/csv/' . $file_data['file_name'];
                
                if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);
                    if (isset($csv_array[0]['title']) && isset($csv_array[0]['QuestionOption']) && isset($csv_array[0]['YesOrNo'])) {
                        
                        // $otherdb = $this->load->database('otherdb', TRUE);
                        $quid     = $this->exam_m->insert_quiz();
                        $noqStart = 1;
                        foreach ($csv_array as $key => $row) {
                            
                            $title          = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['title']);
                            $QuestionOption = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['QuestionOption']);
                            
                            if ($title == 'Multiple Choice Single Response') {

                                $this->session->unset_userdata('Match_the_Column');
                                $this->session->set_userdata('Multiple_Choice_Single_Answer', 'Multiple Choice Single Answer');
                            } elseif ($title == 'True / False') {
                                $this->session->set_userdata('Multiple_Choice_Single_Answer', 'Multiple Choice Single Answer');
                            } elseif ($title == 'Match the following') {
                                
                                $this->session->unset_userdata('Multiple_Choice_Single_Answer');
                                $this->session->set_userdata('Match_the_Column', 'Match the Column');
                            }
                            if ($this->session->userdata('Multiple_Choice_Single_Answer')) {
                                if ($row['title'] != 'Multiple Choice Single Response' && $title != 'Question' && $title != 'True / False') {
                                    if ($title != '') {
                                        $userdata = array(
                                            'question' => $title,
                                            'question_type' => 'Multiple Choice Single Answer'
                                        );
                                        $this->db->insert('ets_qbank', $userdata);
                                        $qid = $this->db->insert_id();
                                        $this->session->set_userdata('qid', $qid);

                                        if($this->input->post('ch')==1){
                                            if($row['YesOrNo'] != '') {
                                            $score = 1;
                                        } else {
                                            $score = 0;
                                        }
                                    }else{
                                        // remove code after  
                                        for ($i=0; $i < 4 ; $i++) { 
                                            $randi = rand(1,4);
                                            if ($i==3) {
                                                if ($randi==1) {
                                                    $score = 1; 
                                                }else{
                                                    $score = 0; 
                                                }
                                            }
                                        }
                                    }
                                        $userdata = array(
                                            'q_option' => $QuestionOption,
                                            'qid' => $qid,
                                            'score' => $score
                                        );

                        
                                        $this->db->insert('ets_options', $userdata);
                                        
                                        $this->db->where('quid', $quid);
                                        $query    = $this->db->get('ets_quiz');
                                        $quizData = $query->row();
                                        
                                        $qids = $quizData->qids;
                                        $noq  = $quizData->noq;
                                        if ($qids == '') {
                                            $updatedQid = $qid;
                                        } else {
                                            $updatedQid = $qids . ',' . $qid;
                                        }
                                        if ($noq == '') {
                                            $noqUpdate = $noqStart;
                                        } else {
                                            $noqUpdate += 1;
                                        }
                                        $arrayName = array(
                                            'qids' => $updatedQid,
                                            'noq' => $noqUpdate
                                        );
                                        $this->db->where('quid', $quid);
                                        $this->db->update('ets_quiz', $arrayName);
                                        
                                    } else {
                                        if($this->input->post('ch')==1){
                                        if ($row['YesOrNo'] != '') {
                                            $score = 1;
                                        } else {
                                            $score = 0;
                                        }
                                    }else{
                                        $this->db->where('qid',$this->session->userdata('qid'));
                                        $this->db->where('score',1);
                                      $chuh    =  $this->db->get('ets_options');

                                        $this->db->where('qid',$this->session->userdata('qid'));
                                       $Three   =  $this->db->get('ets_options');

                                    if ($chuh->num_rows()==1) {
                                       $score = 0;
                                    }else{
                                    for ($i=0; $i < 4 ; $i++) { 
                                         $randi = rand(1,4);
                                         if ($i==3) {
                                             if ($randi==1) {
                                               $score = 1; 
                                             }else{
                                                $score = 0; 
                                             }
                                         }
                                        }
                                        if ($Three->num_rows()==3) {
                                           if ($chuh->num_rows()==0) {
                                              $score = 1;
                                           }
                                        }                                        

                                    }
                               }
        
                                        $userdata = array(
                                            'q_option' => $QuestionOption,
                                            'qid' => $this->session->userdata('qid'),
                                            'score' => $score
                                        );
                                        $this->db->insert('ets_options', $userdata);                                        
                                        
                                    }
                                }
                                
                            } elseif ($this->session->userdata('Match_the_Column')) {
                                if ($title != 'Match the following') {

                                    
                                    if ($title == 'Question') {
                                        $userdata = array(
                                            'question' => 'Match the following',
                                            'question_type' => 'Match the Column'
                                        );
                                        $this->db->insert('ets_qbank', $userdata);
                                        $qid = $this->db->insert_id();
                                        $this->session->set_userdata('qid', $qid);
                                        
                                        $this->db->where('quid', $quid);
                                        $query    = $this->db->get('ets_quiz');
                                        $quizData = $query->row();
                                        
                                        $qids       = $quizData->qids;
                                        $updatedQid = $qids . ',' . $qid;
                                        $arrayName  = array(
                                            'qids' => $updatedQid
                                        );
                                        $this->db->where('quid', $quid);
                                        $this->db->update('ets_quiz', $arrayName);
                                        
                                    } else {
                                        $score    = (1 / 4);
                                        $userdata = array(
                                            'q_option' => $title,
                                            'q_option_match' => $QuestionOption,
                                            'qid' => $this->session->userdata('qid'),
                                            'score' => $score
                                        );
                                        $this->db->insert('ets_options', $userdata);
                                    }
                                    
                                }
                                
                                
                                
                            }
                        }
                        
                    } else {
                        $this->session->set_flashdata('error', "Please Check Excel Sheet Heading Name!");
                        redirect(base_url("exam/index"));
                    }
                    
                    $this->session->set_flashdata('success', $this->lang->line('import_success'));
                    redirect(base_url("exam/index"));
                    
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('import_error'));
                    redirect(base_url("exam/index"));
                }
                
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('import_error'));
            redirect(base_url("bulkimport/index"));
        }
    }
    
    
    public function getClass($className, $adminID)
    {
        $usertype = $this->session->userdata("usertype");
 
        if ($className) {
            $query = $this->db->query("SELECT * FROM `classes` WHERE `courseCode` = '$className' and `adminID` = '$adminID' ");
            if (empty($query)) {
                return 'error';
            } else {
                return $query->row();
            }
            
        } else {
            return "error";
        }
    }
    
    public function getSubCourse($subCourseCode, $adminID)
    {
        $usertype = $this->session->userdata("usertype");
        if ($subCourseCode) {
            $query = $this->db->query("SELECT sub_coursesID FROM `sub_courses` WHERE `subCourseCode` = '$subCourseCode' and `adminID` = '$adminID' ");
            if (empty($query)) {
                return 'error';
            } else {
                return $query->row('sub_coursesID');
            }
            
        } else {
            return "error";
        }
    }
    
    public function getTeacher($username)
    {
        $usertype = $this->session->userdata("usertype");
        if ($username) {
            $query = $this->db->query("SELECT teacherID FROM `teacher` WHERE `username` = '$username' ");
            if (empty($query)) {
                return 'error';
            } else {

                return $query->row('teacherID');
            }
            
        } else {
            return "error";
        }
    }
    
    public function getSection($className, $section)
    {
        
        if ($className) {
            $query = $this->db->query("SELECT sectionID, section FROM `section` WHERE `classesID` = '$className' AND `section` = '$section'");
            if (empty($query)) {
                return 'error';
            } else {
                return $query->row();
            }
            
        } else {
            return "error";
        }
    }   
  
    
}