<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("teacher_m");
		$this->load->model("user_m");
		$this->load->model("systemadmin_m");
		$this->load->model("professor_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('profile', $language);

		$language = $this->session->userdata('lang');

		$this->lang->load('dashboard', $language);
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata('username');
		$loginuserID = $this->session->userdata('loginuserID');

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
			$this->data['admin'] = $this->systemadmin_m->get_systemadmin(array('username' => $username));
			$this->data['user'] = $this->user_m->get_user_by_username(array('username' => $username));
		
			$this->data["subview"] = "profile/index";
			$this->load->view('_layout_main', $this->data);
		}
		 elseif($usertype == "Librarian" || $usertype == "Accountant" || $usertype == "Support" || $usertype == "Academic" || $usertype == "Super_A_P") {
			$user = $this->user_m->get_single_user(array("username" => $username));
			  $this->data['Profile'] = $this->user_m->get_single_user_Profile($loginuserID);
			
			if($user) {
				$this->data['user'] = $user;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Teacher") {
			$teacher = $this->teacher_m->get_single_teacher(array('username' => $username));
			if($teacher) {
				$this->data['teacher'] = $teacher;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		}elseif($usertype == "Professor") {
			$professor = $this->professor_m->get_single_professor(array('username' => $username));
			if($professor) {
				$this->data['professor'] = $professor;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		}elseif($usertype == "Student") {
			$student = $this->student_m->get_single_student(array('username' => $username));
			$this->data["student"] = $this->student_m->get_student(array('studentID'=>$student->studentID,'adminID'=>$student->adminID));
			$this->data["class"] = $this->student_m->get_class($student->classesID);
			

			
			if($this->data["class"]) {
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
		$this->data["education"] = $this->student_m->get_educations($student->studentID,$ArrayForRequired);

			     $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

				// $this->data["parent"] = $this->parentes_m->get_parentes($student->parentID);

				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$parentes = $this->parentes_m->get_single_parentes(array("username" => $username));
			if($parentes) {
				$this->data['parentes'] = $parentes;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}




function ChangePicture(){
		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata('username');
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Student" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "Professor" || $usertype == "Academic" || $usertype == "Teacher" || $usertype == "Super_A_P") {
			$this->data['admin'] = $this->systemadmin_m->get_systemadmin(array('username' => $username));
$this->data["subview"] = "profile/ChangePicture";
$this->load->view('_layout_main', $this->data);

}

}

function uploadPic(){
    $uri = $this->uri->segment(3);
	$data = $_POST["image"];
	/*print_r($data);
	exit();*/
	$image_array_1 = explode(";", $data);
	$image_array_2 = explode(",", $image_array_1[1]);
	$data = base64_decode($image_array_2[1]);
	$imageName = time() . '.png';
$root = 'uploads/images/';
	file_put_contents($root.$imageName, $data);

		 $table = strtolower($this->session->userdata("usertype"));
 
if ($uri=='undefined') {
		if($table == "admin") {

			$table = "setting";

		}

		if($table == "Student") {

			$table = "student";

		}

		if($table == "super_a_p") {

			$table = "user";

		}
		if($table == "accountant") {

			$table = "user";

		}
		if($table == "academic") {

			$table = "user";

		}
		if($table == "support") {

			$table = "user";

		}

		if($table == "clgadmin") {

			$table = "admin";

		}

		if($table == "librarian") {

			$table = "user";

		}
	}else{
		$table = "student";	
	}

		$username = $this->session->userdata("username");
$data =  array(
'photo'=>$imageName
);
  
if($uri=='undefined'){
		$this->db->where('username',$username);	
	
}else{
$this->db->where('studentID',$uri);	
}

		
		$this->db->update($table,$data);

if($uri=='undefined'){
    $this->session->set_userdata('photo',$imageName);
}
	echo '<img src="'.base_url().$root.$imageName.'" class="img-thumbnail" />';
}

  
}

/* End of file book.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/book.php */
