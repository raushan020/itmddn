<?php 
class WpProQuiz_Model_AnswerTypes
{
    protected $_answer;
    protected $_html;
    protected $_points;
    protected $_correct;
    protected $_sortString;
    protected $_sortStringHtml;
    protected $_graded;
    protected $_gradingProgression;
    protected $_gradedType;
    protected $_mapper;



function setter($_answerSet ,$_htmlSet,$_pointsSet,$_correctSet,$_sortStringSet,$_sortStringHtmlSet,$_gradedSet,$_gradingProgressionSet,$_gradedTypeSet,$_mapperSet){

    $this->_answer=$_answerSet;
    $this->_html= $_htmlSet;
    $this->_points = $_pointsSet;
    $this->_correct = $_correctSet;
    $this->_sortString = $_sortStringSet;
    $this->_sortStringHtml =$_sortStringHtmlSet;
    $this->_graded = $_gradedSet;
    $this->_gradingProgression =$_gradingProgressionSet;
    $this->_gradedType =$_gradedTypeSet;
    $this->_mapper =$_mapperSet;

}

function _answerSet(){
  
  return $this->_answer;

}

function _htmlSet(){
return $this->_html;

}

function _pointsSet(){
return $this->_points;

}

function _correctSet(){

return $this->_correct;
}
function _sortStringSet(){

return $this->_sortString;
}
function _sortStringHtmlSet(){

return $this->_sortStringHtml;
}
function _gradedSet(){

return $this->_graded;
}
function _gradingProgressionSet(){
return $this->_gradingProgression;

}
function _gradedTypeSet(){
return $this->_gradedType;

}
function _mapperSet(){
return $this->_mapper;

}




}





?>