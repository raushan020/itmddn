<?php $usertype =  $this->session->userdata('usertype'); ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<style type="text/css">
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>

<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>

                <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_notice')?></li>

            </ol>
        </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body">                  

                        <div class="col-sm-12">

                            <div class="tab">
  <button class="tablinks" onclick="openCity(event, 'admin')" id="defaultOpen" style="color: black">Admin</button>
  <button class="tablinks" onclick="openCity(event, 'rm')" style="color: black">Relationship Manager</button>
    <button class="tablinks" onclick="openCity(event, 'super_ap')" style="color: black">Super Academic Partner</button>
  <button class="tablinks" onclick="openCity(event, 'academic_partner')"  style="color: black">Academic Partner</button>
  <button class="tablinks" onclick="openCity(event, 'student')"  style="color: black">Students</button>
</div>
	<div id="admin" class="tabcontent">
		<form class="form-horizontal" role="form" method="post"> 

    <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Admin" >

            <div class="custom-control custom-checkbox" for="check">
                <input type="checkbox" class="custom-control-input chkPassport1"  name="all_admin" value="All" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">All Admin</label>
            </div>

            <div class="auto-widget" id="dvPassport1" style="display: none">
                
                <p>Tag Your Admin: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
            </div>

            <script type="text/javascript">
    $(function () {
        $(".chkPassport1").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport1").hide();
            } else {
                $("#dvPassport1").show();
            }
        });
    });
</script> 


            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                      	<span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form>
	</div>

	<div id="rm" class="tabcontent">
		<form class="form-horizontal" role="form" method="post">
        <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Accountant" >

            <div class="custom-control custom-checkbox" for="check">
                <input type="checkbox" class="custom-control-input chkPassport2"  name="all_accountant" value="All" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">All Relationship Manager</label>
            </div>

            <div class="auto-widget" id="dvPassport2" style="display: none">
                
                <p>Tag Your Relationship Manager: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
            </div>

            <script type="text/javascript">
    $(function () {
        $(".chkPassport2").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport2").hide();
            } else {
                $("#dvPassport2").show();
            }
        });
    });
</script>                
            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                      	<span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form> 
	</div>

    <div id="super_ap" class="tabcontent">
        <form class="form-horizontal" role="form" method="post">
        <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="super_AP" >

            <div class="custom-control custom-checkbox" for="check">
                <input type="checkbox" class="custom-control-input chkPassport3"  name="all_Super_ap" value="All" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">All Super Academic Partners</label>
            </div>

            <div class="auto-widget" id="dvPassport3" style="display: none">
                
                <p>Tag Your Super Academic Partners: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
            </div>

            <script type="text/javascript">
    $(function () {
        $(".chkPassport3").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport3").hide();
            } else {
                $("#dvPassport3").show();
            }
        });
    });
</script>                
            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                        <span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form> 
    </div>

	<div id="academic_partner" class="tabcontent">
		<form class="form-horizontal" role="form" method="post">
 <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Teacher" >

			<div class="custom-control custom-checkbox" for="check">
			    <input type="checkbox" class="custom-control-input chkPassport4"  name="all_academic" value="All" id="defaultUnchecked">
			    <label class="custom-control-label" for="defaultUnchecked">All Academic Partners</label>
			</div>

			<div class="auto-widget" id="dvPassport4" style="display: none">
                
    			<p>Tag your Academic Partner: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
			</div>

			<script type="text/javascript">
    $(function () {
        $(".chkPassport4").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport4").hide();
            } else {
                $("#dvPassport4").show();
            }
        });
    });
</script>

            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                      	<span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form>
	</div>
			<div id="student" class="tabcontent">
			  <form class="form-horizontal" role="form" method="post">
                <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Student">
                                     <!-- <label for="title" class="col-sm-1 control-label">

                                           Course

                                        </label> -->
                                    
                            <?php 

                                if(form_error('classesID')) 

                                    echo "<div class='form-group has-error' >";

                                else     

                                    echo "<div class='form-group' >";

                            ?>

                        <label for="classesID" class="col-sm-2 control-label">

                           Course*

                        </label>

                        <div class="col-sm-6">
                            
                            <?php

                                $array = array();


                                $array[0] = "Select Course";
                              

                                foreach ($classes as $classa) {

                                    $array[$classa->classesID] = $classa->classes;

                                }
                                
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control' onchange = 'CourseSDependent1($(this).val())' value='Select Course' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classesID'); ?>

                        </span>

                    </div>

                    <div id="subCourseID">
                    </div>      

                    <div id="fetchYearsAndSem">
                        
                    </div>

                    <div id="appendSujectsDetails">
                        
                    </div>
                
                                   <?php 

                                        if(form_error('title')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="title" class="col-sm-2 control-label">

                                            Notice Title

                                        </label>

                                        <div class="col-sm-6">

                                            <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('title'); ?>

                                        </span>

                                    </div>

  
                                    <?php 

                                        if(form_error('date')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="date" class="col-sm-2 control-label">

                                            <?=$this->lang->line("notice_date")?>

                                        </label>

                                        <div class="col-sm-6">

                                            <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('date'); ?>

                                        </span>

                                    </div>

                                    <?php 

                                        if(form_error('notice')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="notice" class="col-sm-2 control-label">

                                            Descriptions

                                        </label>

                                        <div class="col-sm-6">

                                            <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('notice'); ?>

                                        </span>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-8">
                                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                                        </div>
                                    </div>
                                </form>
			</div>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>

<script type="text/javascript">

jq('#date').datepicker();

jq('#notice').jqte();

</script>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>

