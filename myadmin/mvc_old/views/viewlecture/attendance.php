

<div class="">

    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="ti ti-id-badge"></i>Attendance </h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">

         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

        <li class="active">Attendance</li>
          </ol>
      </div>
    </div>
    <!-- form start -->

    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">
            <div class="col-sm-12">

<form style="" class="form-horizontal"  autocomplete="off" role="form">
<div class="col-sm-4">
                    <div class="">
                      <?php 
    $getClasses = $this->input->get('course');

    $getSubcourse = $this->input->get('subcourse');

    $getSemester = $this->input->get('yearsOrSemester');

    $getSubject = $this->input->get('subject');

    $date = $this->input->get('date');

                       ?>
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("course", $array, set_value("classesID",$getClasses), "id='classesID' required  class='form-control'");
                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                      
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                            <div class="form-group">
                                <label for="classesID" class="control-label"> Sub Courses </label>

                                <div class="">
                                    <select id="subCourseID" name="subcourse" class="form-control">
                                        <option value="0">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                            if ($value->sub_coursesID==$getSubcourse) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                       
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$getSemester) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$getSemester) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                              
                            </div>
                      
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Subject</label>

                                <div class="">
                                    <select id="subjectID" name="subject" required class="form-control">
                                        <?php  foreach ($subjects as $key => $value) {
                                            if ($value->subjectID==$getSubject) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                                        <option value="<?php echo $value->subjectID ?>" <?php echo $selected ?>><?php echo $value->subject ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                      
                    </div>
                </div>

                                <div class="col-sm-4">
                    <div class="">
                      
                    <div class="form-group">
                          <label for="classesID" class="control-label">Date</label>
                          <div class="">

                            <input type="text" placeholder="Select Date"  required class="form-control datepicker_quiz_data" name="date" value="<?php echo $date ?>">
                          </div>
                          <div class="clearfix"></div>
                    </div>
                       
                    </div>
                </div>


          <div class="col-sm-4">
                    <div class="">
                      <div class="form-group">
                            <label for="classesID" class="control-label"></label>
                          <div class="">
                  <input type="submit" value="Search" name="submit" class="btn-success" style="    margin-top: 20px;" name="Search">
                </div>
                </div>
                    </div>
                  </div>

                 </form>




              <div class="col-sm-6">
                
              </div>
              <div class="col-sm-6">
          <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"){
            ?>
   <div class="pull-right">
      <div class="btn-group">
   <a href="<?php echo base_url('professor/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
      </div>
   </div>
   <?php  }  ?>

</div>
              
              
              <div class="clearfix"></div>

     


            <div class="table table-responsive">
              <div class="col-sm-12">
                <div id="hide-table">
 <?php  if($getStudent){ ?>                 
<form method="post" autocomplete="off">
  <input type="hidden" name="classesID" value="<?php echo $getClasses ?>">
  <input type="hidden" name="sub_coursesID" value="<?php echo $getSubcourse ?>">
  <input type="hidden" name="yearsOrSemester" value="<?php echo $getSemester ?>">
  <input type="hidden" name="subjectID" value="<?php echo $getSubject ?>">
  <input type="hidden" name="date" value="<?php echo $date ?>">

                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                      <div class="">
                      </div>
                    </div>
                    <div class="col-sm-4"></div>

  <input type="hidden" name="subjectID" value="<?php echo $_GET['subject'] ?>">

  
                    <table class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                               
                                 <th class="col-sm-2">Name</th>

                                
                               

                                <?php if($usertype == "Admin"){ ?>

                                <th class="col-sm-2"><?=$this->lang->line('professor_status')?></th>


                                <?php } ?>

                                <th class="col-sm-2">Action</th>

                            </tr>

                        </thead>

                        <tbody>
                      <?php
                        foreach ($getStudent as $key => $value) {
                         ?>
                            <tr>

                                <td class="col-sm-2"><?php echo $key+1; ?></td>

                                <input type="hidden" name="studentID[]" value="<?php echo $value->studentID ?>">
                                <td class="col-sm-2"><?php echo $value->name; ?></td>
                                 
                                <?php if($usertype == "Admin"){ ?>

                                <td class="col-sm-2"><?=$this->lang->line('professor_status')?></td>

                                <?php } ?>

                                <td class="col-sm-2">
                                  <?php if ($count_students_atd) { ?>
                                    <input type="hidden" name="atdID[]" value="<?php echo $value->atdID ?>">
                                  <?php } ?>
                                  
                                  <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" required id="present<?php echo $key; ?>" value = "2" <?php if($count_students_atd){if($value->atd=='2'){echo "checked";}} ?> name="atd<?php echo $key; ?>">
                                    <label class="custom-control-label" for="present<?php echo $key; ?>">Present</label>
                                    <input type="radio" required class="custom-control-input" id="absent<?php echo $key; ?>" value = "0" <?php if($count_students_atd){if($value->atd=='0'){echo "checked";}} ?> name="atd<?php echo $key; ?>">
                                    <label class="custom-control-label" for="absent<?php echo $key; ?>">Absent</label>
                                        <input type="radio" required class="custom-control-input" id="half<?php echo $key; ?>" value = "1" <?php if($count_students_atd){if($value->atd=='1'){echo "checked";}} ?> name="atd<?php echo $key; ?>">
                                    <label class="custom-control-label" for="half<?php echo $key; ?>">Half</label>
                                  </div>

                                </td>
                                

                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                        <?php  if ($count_students_atd) {  ?>
                     <div class="" style="width:50%;margin:20px auto;">
                        <div class="form-group">
                          <label for="classesID" class="control-label">Select Date</label>
                          <div class="">

                            <input type="text" placeholder="Select Date" required  class="form-control datepicker_quiz_data" name="new_date" value="<?php echo $date ?>">
                          </div>
                          <div class="clearfix"></div>
                    </div>
                      <div class="pdf_form">
                        <input type="submit" name="submit" value="Update" class="btn btn-success">
                      </div>
                    </div>

                    <?php } else{ ?>
                      <div class="" style="width:50%;margin:20px,auto;">
                        <div class="form-group">
                          <label  class="control-label">Select Date</label>
                          <div class="">
                            <input type="text" placeholder="Select Date" required  class="form-control datepicker_quiz_data" name="new_date" value="<?php echo $date ?>">
                          </div>
                          <div class="clearfix"></div>
                    </div>
                      <div class="pdf_form">
                        <input type="submit" name="submit" class="btn btn-success">
                      </div>
                      </div>
                    <?php } ?>

                  </form>
                <?php } else{ ?>
      <div class="Noresuls">

               <h1>Sorry we couldn't find any matches</h1>

               <p>Maybe your search was too specific, please try searching with another term.</p>

               <img src="https://www.itmddn.online/uploads/images/crying.png">

            </div>
      
                <?php } ?>

                </div>
 </div>
 </div>


            </div> <!-- col-sm-12 -->

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->
</div>
</div>


<script type="text/javascript">
  function GetsubjectAtd(val){
    var classesID =  $('#classesID').val();

                $.ajax({
                url: base_url+'viewlecture/subject_get',
                type: 'POST',
                data:{val:val,classesID:classesID},
                success: function(data){
                  console.log(data);
                $('#subjectID').html(data);
                }
            }); 

  }
</script>

<script>

  var status = '';

  var id = 0;

  $('.onoffswitch-small-checkbox').click(function() {

      if($(this).prop('checked')) {

          status = 'chacked';

          id = $(this).parent().attr("id");

      } else {

          status = 'unchacked';

          id = $(this).parent().attr("id");

      }



      if((status != '' || status != null) && (id !='')) {

          $.ajax({

              type: 'POST',

              url: "<?=base_url('professor/active')?>",

              data: "id=" + id + "&status=" + status,

              dataType: "html",

              success: function(data) {

                  if(data == 'Success') {

                      toastr["success"]("Success")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  } else {

                      toastr["error"]("Error")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  }

              }

          });

      }

  }); 

</script>

