<div class="">
   
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i> Video </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active">Video</li>
            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
            <div class="">
            <div>
            <div class="">

            <div class="box-body">

                <div class="row">

                    <div class="col-sm-12">
                        <?php
                            $uri = $this->uri->segment(3);
                            $usertype = $this->session->userdata("usertype");
                            $SpecialUsertype = $this->session->userdata('SpecialUsertype');
                            if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == 'Support' || $usertype == 'Academic' || $SpecialUsertype=='Btech') {

                        ?>

                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                               <div class="pull-right">
                              <div class="btn-group">
                           <a href="<?php echo base_url('videos/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span>Add a video</a>

                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>



        <div class="col-sm-12">

            <div class="theme_input_blue">
            <?php 
               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
               $display =  "block";
               $addmorebutton = "none";
               $Removemorebutton = "inline-block";
               }else{
               $display = "none";
               $addmorebutton = "inline-block";
               $Removemorebutton = "none";
               }
               
               ?>
                
                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterNoticeclassesID')), "id='classesID' onchange='CourseSDependent($(this).val())' class='form-control'");
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label"> Sub Courses </label>

                                <div class="">
                                    <select id="subCourseID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                        if ($value->sub_coursesID==$this->session->userdata('FilterNoticesubCourseID')) {
                                            $selected =  "Selected";
                                        }else{
                                          $selected =  "";
                                        }
                                        ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?>                                            
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='yearsOrSemester' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('yearsOrSemester'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
         
            <?php }  ?>                      

                                
                <?php  if( $usertype == "ClgAdmin" || $usertype == "Academic"  || $usertype == "Admin" || $SpecialUsertype=='Btech'){?>
 
                        <!-- <div id="hide-tab">

                            <table id = "noticeTables" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>

                                        <th class="col-sm-4">Course</th>

                                        <th class="col-sm-4">Sub Course</th>

                                        <th class="col-sm-4">Year / Semester</th>

                                        <th class="col-sm-4">Notice</th>
                                        
                                        <th class="col-sm-4">Action</th>
                                       
                                    </tr>

                                </thead>
                                

                            </table>



                        </div> -->



                        <div id="hide-table">
                    <div class="container">                                          
                         <?php if(count($subjects)) {
                            $i = 1;
                            $j = 1;

                            foreach($subjects as $key2 => $subject) { 
                                

                        ?> 
                        <div class="row"> 
                        <div class="col-md-12"> 

                        <table class="table table-responsive">
                            <tr>
                                <th class="col-md-1"><?php echo $i; ?></th>
                                <th class="col-md-3"><h3>Subject Name </h3></th>
                                <td class="col-md-8" style="text-transform:capitalize;"><h3><?php echo $subject['subject']; ?></h3></td>
                            </tr>
                            <tr>
                                <th class="col-md-1"></th>
                                <th class="col-md-3">Video</th>
                                <td class="col-md-8"><a href="#" target="_blank">
                                     <?php 
                                        foreach ($subject['videoKey'] as $key => $value1) { 
                                    ?> 
                                   <!--  <a target="_blank" href="<?php echo base_url() ?>lms/document/<?php echo $value1['genID'];  ?>"><img src="<?php echo base_url() ?>assets/img/video-player.png" style="width: 50px; height: 50px;"></a> -->
                                     
                                     <a href="#" class="js-video-button<?php echo $key2.$key;?>" id="<?php echo $value1['videoID'];?>" data-channel="video" data-video-url="<?php echo base_url() ?>uploads/videos/<?php echo $value1['src'];  ?>"><img src="<?php echo base_url() ?>assets/img/video-player.png" style="width: 50px; height: 50px;"></a><a href="#" style="color: red" class="videodelete" data-toggle="modal" data-target="#Modals<?php echo $value1['videoID'];?>">delete</a>
                                    &nbsp;&nbsp;&nbsp;
                                                     <script>
                                                        $(".js-video-button<?php echo $key2.$key;?>").modalVideo({
                                                            youtube:{
                                                                controls:0,
                                                                nocookie: true
                                                            }
                                                        });
                                                    </script>
                                                    <div class="modal" id="Modals<?php echo $value1['videoID'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                                     <div class="modal-dialog modal-sm">
                                                       <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h6 class="modal-title"> 
                                                              <span><?php echo $value1['src']; ?></span>
                                                              
                                                            </h6>
                                                          </div>
                                                          <div class="modal-body">
                                                            
                                                            <h5>Are You Sure Remove This Video?</h5></div>
                                                          <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                            <button type="button" id="removestudent" class="btn btn-success" data-dismiss="modal" onclick="validate(this)" value="<?php echo $value1['videoID'] ?>">Yes</button>
                                                          </div>
                                                        </div>
                                                       </div>
                                                     </div>
                                                    </div>
                                                    <script type="text/javascript">
                                                      function validate(a)
                                                      {
                                                          var ID= '<?php echo $value1['videoID'];?>';
                                                        
                                                            $.ajax({
                                                                    type: "POST",
                                                                      url: '<?php echo base_url(); ?>videos/deletevideos',
                                                                      dataType: "json",
                                                                        data: {'ID':ID},
                                                                      success:function(data)
                                                                      {
                                                                        alert("Video is Remove Successfully");
                                                                        location.reload();
                                                                      }
                                                                  });
                                                      }
                                                    </script>
                                <?php } ?>                                                           
                                </td>
                            </tr>
                            
                        </table>

                            </div> 

            
                                
                        </div> 
                         <?php $i++; }} ?>                     
                    </div>
                    <div id="pagination">
                        <ul class="tsc_pagination pull-right">

                        <!--Show pagination links -->
                            <div class="col-md-12 text-center">
                                <?php echo $pagination; ?>
                            </div>
                        </ul>
                    </div>
                </div>



                    <?php  } ?>
                        <?php 

                            if ( $usertype == "Student") { ?>
                            
                            <div id="hide-tab">

                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>

                                        <th class="col-sm-4">Notice</th>
                                        
                                        <th class="col-sm-4">Action</th>                                      
                                    </tr>

                                </thead>
                             
                                <tdata>
                                    <?php if(count($notice)) {$i = 1; foreach($notice as $notices) { ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $notices->title; ?></td>
                                        <td><?php echo $notices->date; ?></td>
                                        <td><?php echo substr($notices->notice,0,20); ?></td>
                                        <td><?php echo btn_view('notice/view/'.$notices->noticeID, $this->lang->line('view')); ?></td>
                                    </tr>
                                        <?php $i++; }} ?>
                                </tdata>

                            </table>

                        </div>
                    
                       <?php } ?>
              
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
            $.ajax({
                type: 'POST',
                url: "<?=base_url('notice/index')?>",
                data: "classesID=" + classesID,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }
            });
    });

</script>

<script type="text/javascript">

    $('#subCourseID').change(function() {
        $('#noticeTables').DataTable().state.clear();

        var subCourseID = $(this).val();

            $.ajax({
                type: 'POST',
                url: "<?=base_url('notice/index')?>",
                data: "subCourseID=" + subCourseID,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }

            });

    });

</script>

<script type="text/javascript">
    $('#yearSemesterID').change(function() {
        var yearSemesterID = $(this).val();
        $('#noticeTables').DataTable().state.clear();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('notice/index')?>",
            data: "yearSemesterID=" + yearSemesterID,
            dataType: "html",
            success: function(data) {
                 location.reload();
            }
        });
    });
</script>

<script type="text/javascript">
    
    function ResetCourses(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){
    $('#noticeTables').DataTable().state.clear();

            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

</script>



