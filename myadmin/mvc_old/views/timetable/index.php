<div class="">



    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor">Time Table</h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>



            <li class="active">Time Table</li>

            </ol>

        </div>

    

     </div>



    <!-- /.box-header -->



    <!-- form start -->



     <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">
				<div class="col-sm-4"></div>
<style type="text/css">
  .time_table_drppdo{
  width: 159px !important;
  }
</style>
			<div class="col-sm-4">
               <div class="col-sm-12">Select Department</div>
               <div class="col-sm-12">
                  <select class="form-control time_table_drppdo" name="department" onchange="department_filter_url($(this).val())" id="department" value="">
                  	 <option> Select Department  </option>
                  	 <?php foreach ($department as $key => $value) {
                      if ($_GET['id']!='') {
                        
                      
                      ?>
                  	 	<option value="<?php echo $value->departmentID; ?>" <?php if($value->departmentID==$_GET['id']){echo "selected";}else{echo "";}?>><?php echo $value->department_name; ?></option>
                     <?php }else{ ?>
                      <option value="<?php echo $value->departmentID; ?>" ><?php echo $value->department_name; ?></option>
                  	<?php } } ?>
                     
                                 
                  </select>
               </div>
            </div>
            <div class="col-sm-4"></div>  
            <div class="col-sm-12">&nbsp;</div>          

            <div class="col-sm-12">
<div class="table-responsive">
        <table class="table table-responsive table-striped table-bordered table-hover" id="timeTable">

          <thead>

            <tr>

            <th>Course</th>

            <th>9:00-9:45am</th>

            <th>9:45-10:30am</th>

            <th>10:30-11:15am</th>

            <th>11:15-12:00noon</th>

            <th>12:00-12:45pm</th>

            <th>12:45-1:00pm</th>

            <th>1:00-1:45pm</th>

            <th>1:45-2:30pm</th>

            <th>2:30-3:15pm</th>

            <th>3:15-4:00pm</th>

            </tr>

          </thead>

          <tbody>
         
<?php
foreach ($timetable as $key1 => $value) {

  foreach ($value as $key2 => $values) {
	?>

            <tr>

	            <td><?=$values['classes'] ?><br><?=$values['yearsOrSemester'] ?></td>

	            <td>
              <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'9:00','9:45', '1<?php echo $key1.$key2 ?>','<?php echo $values['classesID'] ?>','<?php echo $values['yearsOrSemester'] ?>')" >
              <option>Subject</option>
                <?php
                $professor_name = 'Break';
                
                foreach ($values['subjects'] as $key => $subject) {

                   if($subject->startTime=='9:00'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
            <select class="form-control time_table_drppdo"  id="oprtionProfessor1<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
              </td>

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'9:45','10:30', '2<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php

                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='9:45'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor2<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>


	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'10:30','11:15', '3<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='10:30'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor3<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'11:15','12:00', '4<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='11:15'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor4<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'12:00','12:45', '5<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='12:00'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor5<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td> 

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'12:45','1:00', '6<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='12:45'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor6<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'1:00','1:45', '7<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='1:00'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor7<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

              <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'1:45','2:30', '8<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='1:45'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor8<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'2:30','3:15', '9<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='2:30'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor9<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

	            <td> <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'3:15','4:00', '10<?php echo $key1.$key2 ?>')">
              <option>Subject</option>
                <?php
                $professor_name = 'Break'; 
                foreach ($values['subjects'] as $key => $subject) {
                  if($subject->startTime=='3:15'){
                     $selected  = "selected";
                    $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                    }else{
                     $selected  = "";
                    } 
                 ?>
              <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
               <?php } ?>
                <option value="0">Break</option>
              </select>
              <select class="form-control time_table_drppdo" id="oprtionProfessor10<?php echo $key1.$key2 ?>">
              <option><?php echo $professor_name; ?></option>
            </select>
            </td>

            </tr>
<?php
}}
?>
          </tbody>

        </table>

          </div>

            </div> 



        </div>



    </div>



</div>



</div>



</div>







<style type="text/css">

button, input, optgroup, select, textarea{    

    

    color: inherit;

}

</style>

<script type="text/javascript">
  function timeTableChange(subjectID ,startTime,endTime,id,classesID,yearsOrSemester){

           $.ajax({
                url: base_url+'timetable/timeTableChange',
                type: 'POST',
                data:{subjectID:subjectID,startTime:startTime,endTime:endTime,classesID:classesID,yearsOrSemester:yearsOrSemester},
                success: function(data){
                     var datawithjs = JSON.parse(data);

                    if(datawithjs.error=='error'){
                      $('#flashmsgError'+id).html('Subject');
                    }

                    $('#oprtionProfessor'+id).html('<option>'+datawithjs.professor_name+'</option>');


                
                }
            }); 

  }
</script>

<script type="text/javascript">
	function department_filter_url(val){
  
  
   window.location.href=base_url+"timetable/index?id="+val;
   
   }
   
</script>