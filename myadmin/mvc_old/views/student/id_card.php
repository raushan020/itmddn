  <style>
    .wrap {
      width: 88%;
      /*min-height:62%;*/
      margin: auto;
      border: 1px solid #80808063;
      padding: 14px;
      /*margin-top: 4em;
      margin-bottom: 4em;*/
      box-shadow: 0 2px 4px 0 rgba(0,0,0,.08);
    }
    #img_l {
      float: left;
      width: 25%;
      margin-top: 20px;
    }
    #img_l img {
      width:80%;
    }
    #right_t {
      float: right;
      width: 75%;
      text-align: center;
      line-height: 15px;
    }
    #col-p {
      padding-bottom: 10px;
    }
    #col-top {
      border-top: 2px solid #717171;
      padding-top: 10px;
    }
    #col-top #p {
      text-align: center;
    }
    .left {
      float: left;
      width: 19%;
      border: 2px solid #80808033;
      border-radius: 5px;
    }
    .left img {
      width: 135px;
    }
    .right {
      float:right;
      width:81%;
      padding: 0px 20px;
      line-height: 15px;
    }
    .right #in_l {
      width: 35%;
      float: left;
    }
    .right #in_r {
      width: 60%;
      float: right;
    }
    .inside_left {
      float: left;
      width: 55%;
    }
    .inside_right {
      float: right;
      width: 45%;
    }
  </style>
  <div class="row page-titles">
    <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
    </div>
    <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
        <li>
          <a href="<?=base_url("dashboard/index")?>">
              <i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?>
          </a>
        </li>
          <li class="active">Notice </li>
      </ol>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="card">
        <div class="card-body">
          <div class="box-body">
          	<?php
                  if(count($student)) 
                  {
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Teacher" || $usertype == "Student") 
                    {
              ?>
          	<a href="<?php echo base_url('student/print_id_preview') ?>" class="btn btn-success" style="margin-left:53px">Print ID Card</a>
          	<br>
          </br>
            <div class="wrap">
              
                      <div class="row">
                        <div class="col-lg-12" id="col-p">
                          <div id="img_l">
                            <img src="<? base_url()?>../uploads/images/Lingayas-University.jpeg">
                          </div>
                          <div id="right_t">
                            <h2 style="color: red">
                              Lingya's Vidhyapeeth
                            </h2>
                            <p>
                              (Under Section-3 of UGC Act, 1956)
                            </p>
                            <p>
                              NACHAULI,JASANA-OLD FARIDABAD ROAD, FARIDABAD-121002 
                            </p>
                            <p>
                              PHONE 0129-2598200 TO 240
                            </p>
                          </div>
                        </div>
                        <div class="col-lg-12" id="col-top">
                          <p id="p"><b>
                              Student Identity Card No. : <?=$student->username?></b>
                          </p>
                          <div id="top">
                            <div class="left">
                              <center>
                                <a href="#">

                    <?=img(base_url('uploads/images/'.$student->photo))?>

                </a>
                              </center>
                            </div>
                            <div class="right">
                              <div class="inside_left">
                                <div id="in_l">
                                  <p><b>Name: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->name?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Father's Name: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->father_name?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Branch: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$class->classes?></p>
                                </div>
                                <div id="in_l">
                                  <p style="margin-top:14px"><b>D.O.B: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=date("d M Y", strtotime($student->dob))?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Tel: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->phone?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Address: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->address?></p>
                                </div>
                              </div>
                              <div class="inside_right">
                                <div id="in_l">
                                  <p><b>Roll No.: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->roll?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Blood Group: </b></p>
                                </div>
                                <div id="in_r">
                                  <p>-</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-12" style="padding-top:10px;">
                          <div class="col-lg-4" style="padding-left: 15px; padding-top:15px">
                            <p><b>Issuing Authority</b></p>
                          </div>
                          <div class="col-lg-4" style="padding-left: 15px; padding-top:15px">
                            <p style="text-align: center;"><b>Valid Upto: </b><span><?=$student->session?></span></p>
                          </div>
                          <div class="col-lg-4" style="padding-left: 15px; padding-top:15px">
                            <p style="text-align: right;"><b>(Dy. Registrar)</b></p>
                          </div>
                        </div>
                      </div>
             
            </div>
            <div class="wrap">
              <table>
              	<h3 align="center"><b>Rules To Be Followed</b></h3>
              	<tr>
              		<td>1. Always carry the Identity card and show it on demand</td>
              	</tr>
              	<tr>
              		<td>2. The loss of Identity card must be reported immediately in writing.</td>
              	</tr>
              	<tr>
              		<td>3. Replacement of Identity card will be issued against a payment of Rs. 200.</td>
              	</tr>
              	<tr>
              		<td>4. Deposite your card in the University after completion of Course.</td>
              	</tr>
              </table>

              <div class="row">
              	        <div class="col-lg-12" style="padding-top:10px;">
                          <div class="col-lg-8" style="padding-left: 15px; padding-top:15px">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('uploads/images/barcode.jpg'); ?>" height="50px" width="70%">
                            <p><center><b><?=$student->username?></b></center></p>
                          </div>
                          <div class="col-lg-4" style="padding-left: 15px; padding-top:15px">
                          	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('uploads/images/barcode.jpg'); ?>" height="50px" width="30%">
                            <p style="text-align: center;"><b>Auth. Signatory</b></p>
                          </div>

                        </div>
<!--                              
                                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('uploads/images/barcode.jpg'); ?>" height="50px" width="50%"> -->

             
              	
              </div>
              </div>
               <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>