
<div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="fa fa-bullhorn"></i> Fee </h3>
                    </div>
                    <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("invoice/index")?>">Fee</a></li>
            <li class="active"><?=$this->lang->line('add_payment')?></li>
                </ol>
                    </div>
            </div>

<div class="">
    <!-- /.box-header -->
    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-8">
                <?php 
                    $usertype = $this->session->userdata("usertype"); 
                    if($usertype == "Admin" || $usertype == "Accountant") { 
                ?>

                <?php } elseif($usertype == "Student" || $usertype == "Parent") { ?>

                        <form action="<?= $action; ?>" method="post" id="payuForm" name="payuForm">
                        <input type="hidden" name="key" value="<?= $key ?>" />
                        <input type="hidden" name="hash" value="<?= $hash ?>"/>
                        <input type="hidden" name="txnid" value="<?= $txnid ?>" />
                        <div class="form-group">
                            <label class="control-label">Total Payable Amount</label>
                            <input class="form-control" name="amount" value="<?= $amount ?>"  readonly/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Your Name</label>
                            <input class="form-control" name="firstname" id="firstname" value="<?= $firstname ?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input class="form-control" name="email" id="email" value="<?= $email ?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone</label>
                            <input class="form-control" name="phone" value="<?= $phone ?>" readonly />
                        </div>
                        <div class="form-group">
                            <label class="control-label"> Booking Info</label>
                            <textarea class="form-control" name="productinfo" readonly><?= $productinfo ?></textarea>
                        </div>
                        <div class="form-group">
                            <input name="surl" value="<?= $surl ?>" size="64" type="hidden" />
                            <input name="furl" value="<?= $furl ?>" size="64" type="hidden" />                             
                            <input type="hidden" name="service_provider" value="<?= $service_provider ?>" size="64" /> 
                            <!-- <input name="curl" value="<?= $cancel ?> " type="hidden" /> -->
                        </div>
                        <div class="form-group text-center">
                        <input type="submit" value="Pay Now" class="btn btn-success" /></td>
                        </div>
                    </form>                                  
                <?php } ?>
            </div>
        </div>
    </div>
</div>
