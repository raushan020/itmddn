<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-rupee"></i> Fee </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Fee</li>

            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

    <div class="box-body">

        <div class="row">

            <div class="col-sm-12">



                <?php

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Teacher") {

                ?>
<div class="clearfix"></div>

<div class="col-md-4 col-sm-4 fpaid">
  <a>
  <div class="widget smart-standard-widget">
  <div class="row">
  <div class="widget-caption info">
  <div class="col-xs-4 no-pad zoom">
  <i class="icon fa fa-check-circle"></i>
  </div>
  <div class="col-xs-8 no-pad">
<div class="widget-detail">
<span><i class="fa fa-inr" aria-hidden="true"></i> </span>

<h3 class="cl-info" id="fpaid"><?php 

if ($total_revenue) {
  echo $total_revenue->amount;
}else{
 echo 0;
}
 ?></h3>

<span>Total Revenue</span>
</div>
  </div>
    <div class="col-xs-12">
    <div class="widget-line bg-info">
    <span style="width:48%;" class="bg-info widget-horigental-line"></span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </a>
    </div>


    <div class="col-md-4 col-sm-4 fpaid">
  <a >
  <div class="widget smart-standard-widget">
  <div class="row">
  <div class="widget-caption info">
  <div class="col-xs-4 no-pad zoom">
  <i class="icon fa fa-check-circle"></i>
  </div>
  <div class="col-xs-8 no-pad">
<div class="widget-detail">
<span><i class="fa fa-inr" aria-hidden="true"></i> </span>

<h3 class="cl-info"><?php
if ($total_paid) {
  if ($this->session->userdata('yos_filter_invoice')){
    $paidamount = 0;
    foreach ($total_paid as $key => $value) {
    $paidamount  += $value->paid;
    }
   $peryearpaid  = $paidamount;
   echo($peryearpaid );
  }else{
   $peryearpaid =  $total_paid->paid;    
   echo $peryearpaid;
  }
}else{
  $peryearpaid = 0;
echo(0);
}
 ?></h3>
<span>Total Paid</span>
</div>
  </div>
    <div class="col-xs-12">
    <div class="widget-line bg-info">
    <span style="width:48%;" class="bg-info widget-horigental-line"></span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </a>
    </div>

<div class="col-md-4 col-sm-4 fpaid">
  <a>
  <div class="widget smart-standard-widget">
  <div class="row">
  <div class="widget-caption info">
  <div class="col-xs-4 no-pad zoom">
  <i class="icon fa fa-check-circle"></i>
  </div>
  <div class="col-xs-8 no-pad">
<div class="widget-detail">
<span><i class="fa fa-inr" aria-hidden="true"></i> </span>

<h3 class="cl-info"><?php

if ($total_revenue) {
 echo $total_revenue->amount-$peryearpaid;
}
else{
echo(0);
}
  ?></h3>

<span>Total due</span>
</div>
  </div>
    <div class="col-xs-12">
    <div class="widget-line bg-info">
    <span style="width:48%;" class="bg-info widget-horigental-line"></span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </a>
    </div>
    <div class="col-sm-6 nopading"></div>
    <div class="col-sm-6 nopading">

   <div class="pull-right">

      <div class="btn-group">


   <a href="#" onclick="ResetAllfilter_student()" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-minus"></span> Reset All Filters</a>
  

      </div>

   </div>

</div>
<div class="clearfix"></div>

            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post" >

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Courses

                        </label>

                        <div class="selectdiv">

                           <?php

                              $array = array("0" => 'Courses');

                              

                              foreach ($classes as $classa) {

                              

                                  $array[$classa->classesID] = $classa->classes;

                              

                              }

                              

                              echo form_dropdown("classesID", $array, set_value("classesID_invoice",$this->session->userdata('classesID_invoice')), "id='classesID' class='form-control'");

                              

                              ?>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>


            <div class="col-sm-3">
               <div class="">
                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <div>

                        <label for="classesID" class="control-label">

                        Session

                        </label>

                     </div>

                        <?php

                           $arraySessionFilter = explode('-', $this->session->userdata('sessionFilter_invoice'));
                            ?>
                        <div class="col-sm-6 nopading">
                           <div class="forpostionReletive">
                            <select class="form-control" id="SessionFrom">
                              <option>From</option>
                              <?php for($i=2015; $i<=2025; $i++){ ?>
                              <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[0]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                            </select>
                           </div>

                        </div>

                        <div class="col-sm-6 nopading">

                           <div class="forpostionReletive">

                              <select class="form-control" id="SessionTo">

                                <option>To</option>

                                <?php for($i=2015; $i<=2025; $i++){ ?>

                                <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[1]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                              <?php } ?>

                              </select>

                              <!-- <input type="text" name="" id="SessionTo" value="<?php if(count($arraySessionFilter)==2) {echo $arraySessionFilter[1]; } ?>" class="form-control CalenderYear" placeholder="To">

                              <div class="postionAbsoluter"><i class="fa fa-calendar" aria-hidden="true"></i></div> -->

                           </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetSesession()" id="ResetSesession">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>


            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Year/Semester

                        </label>

                        <div class="selectdiv">
                        <select name="yos_filter" id="yos_filter" class='form-control'>
                   <option>Year/Semester</option>
                    <option value="1st_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="1st_Year") {echo "selected";}else{echo "";}?>>1st Year</option>
                    <option value="2nd_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="2nd_Year") {echo "selected";}else{echo "";}?>>2nd Year</option>
                    <option value="3rd_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="3rd_Year") {echo "selected";}else{echo "";}?>>3rd Year</option>
                    <option value="4th_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="4th_Year") {echo "selected";}else{echo "";}?>>4th Year</option>
                  </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset" ><a style="cursor:pointer;" onclick="Resetyos()">Reset This Filter </a></div>
                     </div>
                  </form>
               </div>
            </div>

  <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Payment Status

                        </label>

                        <div class="selectdiv">

                          <select name="payment_status" id="payment_status" class='form-control'>

                           

                              <option>Payment Status</option>

                              <option <?php if($this->session->userdata('payment_status_invoice')==2){echo "selected";}else{echo '';} ?> value="2">Paid</option>

                              <option <?php if($this->session->userdata('payment_status_invoice')==1){echo "selected";}else{echo '';} ?> value="1">Partial Paid</option>

                              <option <?php if($this->session->userdata('payment_status_invoice')==3){echo "selected";}else{echo '';} ?> value="3">Not paid</option>

                           </select>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="Resetpaymentstatus()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>

<table id="invoice_data_table" class="table table-striped table-bordered table-hover dataTable no-footer">
               <thead>
                  <tr>
                    
                    <!--  <th >S.no</th> -->
                     <th><?=$this->lang->line('student_roll')?></th>

                     <th><?=$this->lang->line('student_name')?></th>

                      <th>Total Amount</th>

                     <th>Paid Amount</th>

                     <th>Due Amount</th>

                     <th><?=$this->lang->line('action')?></th>

                  </tr>

               </thead>

            </table>

                <?php } ?>


<?php if($usertype=='Student'){ ?>

                <div id="hide-table">
                  <table id="noticeAdmin" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                 <th><?=$this->lang->line('slno')?></th>

                                 <th><?=$this->lang->line('invoice_student')?></th>

                                 <th>Course Fee</th>

                                 <th><?=$this->lang->line('invoice_due')?></th>

                                <!-- <th><?=$this->lang->line('invoice_date')?></th> -->

                                <th><?=$this->lang->line('invoice_status')?></th>

                                <th><?=$this->lang->line('action')?></th>

                                <!-- <th><?=$this->lang->line('invoice_paymentmethod')?></th> -->
                                

                            </tr>

                        </thead>

                        <tbody>

                            <?php  if(count($invoices)) {$i = 1; foreach($invoices as $invoice) { ?>

                                <tr>

                                    <td data-title="<?=$this->lang->line('slno')?>">

                                        <?php echo $i; ?>

                                    </td>

                                    <!-- <td data-title="<?=$this->lang->line('invoice_date')?>">

                                        <?php echo $invoice->date; ?>

                                    </td> -->

                                    <td data-title="<?=$this->lang->line('invoice_student')?>">

                                        <?php echo $invoice->name; ?>

                                    </td>



                                    <!-- <td data-title="<?=$this->lang->line('invoice_paymentmethod')?>">

                                        <?php echo $invoice->paymenttype; ?>

                                    </td> -->



                                    <td data-title="<?=$this->lang->line('invoice_amount')?>">

                                        <?php echo $siteinfos->currency_symbol. $invoice->amount; ?>

                                    </td>



                                    <td data-title="<?=$this->lang->line('invoice_due')?>">

                                        <?php echo $siteinfos->currency_symbol. ($invoice->amount - $invoice->paidamount); ?>

                                    </td>


                                     <td data-title="<?=$this->lang->line('invoice_status')?>">

                                        <?php 
                                            $status = $invoice->invoiceStatus;

                                            $setstatus = '';

                                            if($status == 0) {

                                                $status = $this->lang->line('invoice_notpaid');

                                            } elseif($status == 1) {

                                                $status = $this->lang->line('invoice_partially_paid');

                                            } elseif($status == 2) {

                                                $status = $this->lang->line('invoice_fully_paid');

                                            }



                                            echo "<button class='btn btn-success btn-xs disabled'>".$status."</button>";



                                        ?>

                                    </td>


                                    <td data-title="<?=$this->lang->line('action')?>">
                                        
                                        <?php echo btn_view('invoice/view/'.$invoice->invoiceID, $this->lang->line('view')) ?>

                                        <?php if($usertype == "Admin" || $usertype == "Accountant") { ?>

                                        <?php echo btn_edit('invoice/edit/'.$invoice->invoiceID, $this->lang->line('edit')) ?>

                                        <?php echo btn_delete('invoice/delete/'.$invoice->invoiceID, $this->lang->line('delete'))?>

                                        <?php } ?>

                                    </td>

                                </tr>

                            <?php $i++; }} ?>

                        </tbody>

                    </table>
                </div>
              <?php } ?>



            </div>

        </div>

    </div>
</div>
</div>
</div>
</div>
</div>


</div>
<script type="text/javascript">

   $('#classesID').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

       var classesID = $(this).val();
   
           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "classesID_invoice=" + classesID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#subCourseID').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var subCourseID = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "subCourseID_invoice=" + subCourseID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#SessionTo').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var SessionTo = $(this).val();

       var SessionFrom  = $('#SessionFrom').val();

   

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data:{SessionTo_invoice:SessionTo,SessionFrom_invoice:SessionFrom},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#sessionType').change(function() {

    $('#invoice_data_table').DataTable().state.clear();

       var sessionType = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "sessionType=" + sessionType,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#examType').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var examType = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "examType=" + examType,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#education_mode').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var education_mode = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "education_mode=" + education_mode,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>



<script type="text/javascript">

   $('#student_position').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var student_position = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "student_position=" + student_position,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>



<script type="text/javascript">

   $('#yos_filter').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var yos_filter = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "yos_filter_invoice=" + yos_filter,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#payment_status').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var payment_status = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "payment_status_invoice=" + payment_status,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#teacherID').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var teacherID = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: " teacherID=" + teacherID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   function ResetSesession(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetSesession')?>",

   

               data:{ResetSesession:'ResetSesession'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }

   

   

   function ResetCourses(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetCourses')?>",

   

               data:{ResetCourses:'ResetCourses'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }   

   function ResetsessionType(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetsessionType')?>",

   

               data:{ResetCourses:'ResetCourses'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }

   function ResetAllfilter_student(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetAllfilter')?>",

   

               data:{ResetAllfilter:'ResetAllfilter'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }

   
   
   function Resetyos(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/Resetyos')?>",

   

               data:{ResetMorefilter:'ResetMorefilter'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }





   function Resetpaymentstatus(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/Resetpaymentstatus')?>",

   

               data:{ResetMorefilter:'ResetMorefilter'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }
</script>



<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

        if(classesID == 0) {

            $('#hide-table').hide();

        } else {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('setfee/setfee_list')?>",

                data: "id=" + classesID,

                dataType: "html",

                success: function(data) {

                    window.location.href = data;

                }

            });

        }

    });

</script>

