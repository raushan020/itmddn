       function AllStudent(){
            $.ajax({
                type: 'POST',

                url: base_url+"student/AllStudent",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

        function ActiveStudent(){
            $.ajax({
                type: 'POST',

                url: base_url+"student/ActiveStudent",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
        function DraftStudent(){
            $.ajax({
                type: 'POST',

                url: base_url+"student/DraftStudent",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
        function TrashStudent(){
            $.ajax({
                type: 'POST',

                url: base_url+"student/TrashStudent",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}  

$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.mycheckbox').each(function(){
                this.checked = true;
                $(".etsfilertButton").removeClass("disabled");
            });
        }else{
             $('.mycheckbox').each(function(){
                this.checked = false;
                $(".etsfilertButton").addClass("disabled");
            });
        }
    });
    $('.table').on('click', '.mycheckbox', function(){

        if($('.mycheckbox:checked').length == $('.mycheckbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
         if($('.mycheckbox:checked').length >=1){
    $(".etsfilertButton").removeClass("disabled");
}else{
$(".etsfilertButton").addClass("disabled");
}
    });
});
