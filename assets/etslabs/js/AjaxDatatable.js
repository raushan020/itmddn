  function  EditSubCourse(val,column,SubcourseID,editValue){



  $(val).css("background","#FFF url("+base_url+"uploads/images/loaderIcon.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/EditSubCourse',

        data:{"val":editValue,"SubcourseID":SubcourseID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  }

//1. student 

$(document).ready(function(){  

      var dataTable = $('#example4').DataTable({

         scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [                  
                   { extend: 'excelHtml5', text: 'Download Excel',
                      exportOptions: {
                        columns: [1,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,21,22,23]
                    }
                     }
                  
              ],

          fixedColumns:   {

            leftColumns: 4,

            rightColumns:1

        },

          search: false, 

        language: {

      processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
      paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }
       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'student/ajaxStudents',  

                type:"POST"

           }, 

      "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":2},

             { "orderable": false,"targets":7},

             { "orderable": false,"targets":18},

             { "orderable": false,"targets":19},

             { "orderable": false,"targets":20},

             { "orderable": false,"targets":21},

             { "orderable": false,"targets":22},

             { "orderable": false,"targets":23},

              ], 



      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "img" },

              { "data": "username" },

              { "data": "enrollment" },

              { "data": "name" },              

              { "data": "father_name" },

              { "data": "yearsOrSemester" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "session" },

              { "data": "sessionType" },

              { "data": "student_status" },

              { "data": "phone" },

              { "data": "dob" },

              { "data": "sex" },

              { "data": "email" },

              { "data": "mother_name" },

              { "data": "aadhar" },
              { "data": "address" },

              { "data": "nationality" },

              { "data": "create_date" },

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "invoiceStatus" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });
//a/c
$(document).ready(function(){  

      var dataTable = $('#students_for_ac').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

              { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          fixedColumns:   {

            leftColumns: 4,

            rightColumns:1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
       paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'student/ajaxStudents',  

                type:"POST"

           }, 

      "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":2},

             { "orderable": false,"targets":7},

             { "orderable": false,"targets":18},

             { "orderable": false,"targets":19},

             { "orderable": false,"targets":20},

             { "orderable": false,"targets":21},

             { "orderable": false,"targets":22},

             { "orderable": false,"targets":23},

              ], 



      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "img" },

              { "data": "username" },

              { "data": "enrollment" },

              { "data": "name" },              

              { "data": "father_name" },

              { "data": "yearsOrSemester" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "session" },

              { "data": "sessionType" },

              { "data": "student_status" },

              { "data": "phone" },

              { "data": "dob" },

              { "data": "sex" },

              { "data": "email" },

              { "data": "mother_name" },

              { "data": "aadhar" },

              { "data": "nationality" },

              { "data": "create_date" },

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "invoiceStatus" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });



$(document).ready(function(){  

      var dataTable = $('#invoice_data_table').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],



          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",

             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'invoice/ajaxStudents',  

                type:"POST"

           }, 





      "columns": [

              { "data": "check" },
             
              { "data": "roll" },

              { "data": "name" },
              
              { "data": "yearsOrSemester" },              

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "dueamount" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });

// 2.subject 

$(document).ready(function(){  

      var dataTable = $('#subjectsTables').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

               { extend: 'excelHtml5', text: 'Download Excel',
                  exportOptions: {
                        columns: [1,2,3,4,5,6]
                    }

                }

              ],

              fixedColumns:   {

                  leftColumns: 1,

                  rightColumns:1

              },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'subject/AjaxTable',  

                type:"POST"

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":1},

    { "orderable": false,"targets":7}

  ],

      "columns": [   

              { "data": "check" },

              { "data": "sn2" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "action" },



           ], 

      });  



dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  


// 2.1 subject views


$(document).ready(function(){  

      var dataTable = $('#subjectsTables_views').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'subject/AjaxTable',  

                type:"POST"

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

  ],

      "columns": [   

              { "data": "sn2" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },



           ], 

      });  



dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  


// 3. result admin


 $(document).ready(function(){  

      var dataTable = $('#MarkTables').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],
              fixedColumns:   {

                  leftColumns:2,
                  rightColumns:1

              },
          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],

           "bStateSave": true,  

           "ajax":{  

                url:base_url+'mark/AjaxTable/'+uri,  

                type:"POST"

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":4}

  ],

      "columns": [   

              { "data": "sn" },

              // { "data": "date" },

              { "data": "name" },

              // { "data": "subject_code" },

              // { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "r_noq" },

              { "data": "score_obtained" },

              { "data": "percentage_obtained" },

              { "data": "action" },



           ], 

      }); 

      dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

});  

 });



//4 notice
 


$(document).ready(function(){

      var dataTable = $('#noticeTables').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          search: false,

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,

           "serverSide":true,

           "order":[],

           "bStateSave": true, 

           "ajax":{

                url:base_url+'notice/AjaxTable',

                type:"POST"

           },

  "columnDefs": [

    { "orderable": false,"targets":0},
     { "orderable": false,"targets":2},

    { "orderable": false,"targets":4}    

 ],
        

      "columns": [

              { "data": "sn" },

              { "data": "date" },

              { "data": "classes" },

              { "data": "title" },

              { "data": "notice_type"},

              { "data": "notice" },

              { "data": "yearorsemester" },

              { "data": "sender" },

              { "data": "action" },

           ],
 
 


      });

 });


// QA for admin and super admin
$(document).ready(function(){

      var dataTable = $('#qaTables').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          search: false,

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,

           "serverSide":true,

           "order":[],

           "bStateSave": true, 

           "ajax":{

                url:base_url+'Qa/AjaxTable',

                type:"POST"

           },

  "columnDefs": [

    { "orderable": false,"targets":0},
     { "orderable": false,"targets":2},

    { "orderable": false,"targets":4}    

 ],
        

      "columns": [

              { "data": "sn" },
              
              { "data": "date" },

              { "data": "subject" },

              { "data": "unit" },

              { "data": "student" },

              { "data": "question" },

           ],
 
 


      });

 });







 // 5. Pdf list

 $(document).ready(function(){  

      var dataTable = $('#subjectsTablesPdf1').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'Bfrtip',

              scrollX:true,

              scrollCollapse: true,

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],
            
          search: false, 

        language: {

       processing: "<img src='"+ base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'online_reader/AjaxTable',  

                type:"POST"

           },  

      "columns": [

              { "data": "sn" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "countpdf" },

              { "data": "action" },

           ], 

      });  

 });  


// Online Reader

$(document).ready(function(){  

      var dataTable = $('#subjectsTablesPdf').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'Bfrtip',

              scrollX:true,

              scrollCollapse: true,

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          search: false, 

        language: {

       processing: "<img src='"+ base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'lms/AjaxTable',  

                type:"POST"

           },  

      "columns": [

              { "data": "sn" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "countpdf" },

              { "data": "action" },

           ], 

      });  

 });  


// 6.exam 

$(document).ready(function(){  

      var dataTable = $('#examTables').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

            lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":9},

             { "orderable": false,"targets":10},

              ], 

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel',

   exportOptions: {
                        columns: [1,2,3,4,5,6]
                    }
                     }

              ],

          fixedColumns:   {

            leftColumns: 3,

            rightColumns: 1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'exam/AjaxTable',  

                type:"POST"

           },  

      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "quiz_name"},

              { "data": "subject_code" },

              { "data": "start_date" },

              { "data": "end_date" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  





 // 7. question table 



$(document).ready(function(){  

      var dataTable = $('#questionTables').DataTable({
              scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

              dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":2},

              ], 

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],



          search: false, 

        language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

        },

           "processing":true,  

           "serverSide":true,  

           "order":[],  

           "ajax":{  

                url:base_url+'exam/AjaxTableQuestion/'+uri,  

                type:"POST"

           },  

      "columns": [

              { "data": "sn" },

              { "data": "question"},

              { "data": "correct"},

              { "data": "action" },

           ], 

      });  

 });  



//8. courses clasess

$(document).ready(function(){  

      var dataTable = $('#courses').DataTable({
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

        dom: 'lBfrtip',

        scrollX:true,

        scrollCollapse: true,

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":10},

            

              ], 

       buttons: [

                  {extend: 'excelHtml5', text: 'Download Excel',
                  exportOptions: {
                        columns: [1,3,4,5,6,7,8,9,10]
                    }
}
                 

              ],

        fixedColumns:   {

            leftColumns: 0,

            rightColumns:1

        },      

          search: false,

          language: {

            processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
                  paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

          },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

              url:base_url+'classes/ajaxCourse',  

              type:"POST"

            },  

      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "name" },

              { "data": "classes" },

              { "data": "duration" },

              { "data": "course_type" },

              { "data": "mode" },

              { "data": "fee" },

              { "data": "notice"},

              { "data": "department_name"},

              { "data": "action" }

           ], 

      }); 



      dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

});  

 });  


 // 8.1 courcses only view
//8. courses clasess
$(document).ready(function(){  

      var dataTable = $('#courses_views').DataTable({
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

        dom: 'lBfrtip',

        scrollX:true,

        scrollCollapse: true,

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},
             { "orderable": false,"targets":7},

              ], 

       buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          search: false,

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'classes/ajaxCourse',  

                type:"POST"

           },  

      "columns": [

              { "data": "sn2" },

              { "data": "name" },

              { "data": "classes" },

              { "data": "duration" },

              { "data": "course_type" },

              { "data": "mode" },

              { "data": "fee" },

              { "data": "notice"},

           ], 

      }); 



      dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

});  

 });  





// 9. teacher 

$(document).ready(function(){  

      var dataTable = $('#example1').DataTable({
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

        dom: 'lBfrtip',

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":4},

              ],

          search: false,

        language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],

           "bStateSave": true,  

           "ajax":{  

                url:base_url+'teacher/ajaxTeachers',  

                type:"POST"

           },  

      "columns": [

              { "data": "sn" },

              { "data": "name" },

              { "data": "username" },

              { "data": "email" },

              { "data": "action" },

           ], 

      });  

       dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });  



 // 10.add subject



 $(document).ready(function(){  

      var dataTable = $('#professor').DataTable({
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

        dom: 'lBfrtip',

              buttons: [

                  'excelHtml5'

                  // 'csvHtml5',

                  // 'pdfHtml5'
                  // exportOptions: {
                  //       columns: [1,2,3,4,5,6,7]
                  //   }

              ],

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},
             { "orderable": false,"targets":1},
             { "orderable": false,"targets":6},
             { "orderable": false,"targets":7},

              ],

              fixedColumns:   {

            leftColumns: 3,

            rightColumns:1

        },

          search: false,

        language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],

           "bStateSave": true,  

           "ajax":{  

                url:base_url+'professor/ajaxProfessors',  

                type:"POST"

           },  

      "columns": [

              // { "data": "check" },

              { "data": "sn" },

              { "data": "name" },

              { "data": "username" },

              { "data": "designation_name" },

              { "data": "department_name" },

              { "data": "email" },

              { "data": "status" },

              { "data": "action" },

           ], 

      });  

       dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 }); 





function  AddSubject(){

  var classesID =  $('#classesID').val();
  var subjectName =  $('#subjectName').val();
  var shortCodeofSubject =  $('#shortCodeofSubject').val();
  var subjectCode =  $('#subjectCode').val();
  var subjectmode= $('#subjectmode').val();
  var yearSemesterID =  $('#yearSemesterID').val();
  var subCourseID =  $('#sub_CourseID').val();
  var professorID =  $('#professorID').val();
  var startTime =  $('#startTime').val();
  var endTime =  $('#endTime').val();
  // alert(professorID);
// alert(endTime);
    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/AddSubject',

        data:{"classesID":classesID,"subjectName":subjectName,"shortCodeofSubject":shortCodeofSubject, "subjectCode":subjectCode, "subjectmode":subjectmode, "yearSemesterID":yearSemesterID,"subCourseID":subCourseID,"professorID":professorID,"startTime":startTime,"endTime":endTime},

        success: function(response) {

            $('#subjectCode').val('');

            $('#subjectName ').val('');

            $('#shortCodeofSubject ').val('');
            $('#subjectmode').val('');
            $('#professorID').val('');

            $("#appendSujectsDetails").html(response);

          }

       });

  }

//student

    $(document).ready(function() {

    $('#subjectsTablesStudent,#subjectsTablesStudentSyllabus,#noticeTableStudent,#noticeAdmin,#usersTable,#suspend_exam_listtables').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           'excel'

        ]

    } );

} );

     $(document).ready(function() {

    $('#studentattendanceTableforProfessor').DataTable( {
   
        dom: 'lBfrtip',

        lengthMenu: [[-1,10,25,50,100], [ "All",10,25,50,100]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           'excel'

        ]

    } );

} );

//progress export

    $(document).ready(function() {

    $('#professorexportexcel').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           {
               extend: 'excelHtml5',
               exportOptions: {
                   columns:[0,1,2,3,4]
               }
           }

        ]

    } );

} );

    //Professor view attendance export

 $(document).ready(function() {

    $('#professorexportattendance').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           {
               extend: ['excelHtml5'],
               exportOptions: {
                   columns:[1,2,3,4]
               }
           }

        ]

    } );

} );   

// ebook download by professor
 $(document).ready(function() {

    $('#professorebookdownload').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           {
               extend: ['excelHtml5'],
               exportOptions: {
                   columns:[0,1,2,3]
               }
           }

        ]

    } );

} );   


    //Professor view attendance export debarredlist

 $(document).ready(function() {

    $('#professorexportdebarredlist').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             // "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           {
                extend: 'excelHtml5', text: 'Download Excel' , className: 'btn-primary',
               exportOptions: {
                   columns:[1,2,3,4]
               }
           }

        ]

    } );

} );   
 //Professor view attendance Report export

 $(document).ready(function() {

    $('#professorexportattendancereport').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             // "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[-1,10,25,50,100], ["All",10,25,50,100]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           {
               extend: ['excelHtml5'],
               // exportOptions: {
               //     columns:[1,2,3]
               // }
           }

        ],
        fixedColumns:   {

            leftColumns: 3,

            rightColumns:3

        },

    } );

} );   

//11. call subject 

 function  CallSubject(){

var classesID =  $('#classesID').val();

var subjectName =  $('#subjectName').val();

var subjectCode =  $('#subjectCode').val();

var yearSemesterID =  $('#yearSemesterID').val();

var subCourseID =  $('#sub_CourseID').val();

var professorID =  $('#professorID').val();

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/CallSubject',

        data:{"classesID":classesID,"subjectName":subjectName,"subjectCode":subjectCode,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID,"professorID":professorID},

        success: function(response) {

            $('#subjectCode').val('');

            $('#subjectName ').val('');

            $("#appendSujectsDetails").html(response);



        }

            });
$.ajax({

        type: "POST",

        // dataType: "json",

        url:base_url+'AjaxController/CallSubjectData',

        data:{"classesID":classesID,"subjectName":subjectName,"subjectCode":subjectCode,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID,"professorID":professorID},

        success: function(response) { 

         
            response2 = JSON.parse(response);
          
            $('#startDate').val(response2.startDate);

            $('#endDate ').val(response2.endDate);

           
        }

            });





  }



//12.delete subject

 function  DeleteSubject(subjectID){

var classesID =  $('#classesID').val();

var yearSemesterID =  $('#yearSemesterID').val();

var subCourseID =  $('#sub_CourseID').val();

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/DeleteSubject',

        data:{"classesID":classesID,"subjectID":subjectID,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID},

        success: function(response) {

         

            $("#appendSujectsDetails").html(response);

        }

            });



  }



 // 13.edit subject         

 function  EditSubject(val,column,subjectID,editValue){
 
  $(val).css("background","#FFF url("+base_url+"uploads/images/loaderIcon.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/EditSubject',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  }

  function  EditStarttime(val,column,subjectID,editValue){

  $(val).css("background","#FFF url("+base_url+"uploads/images/loaderIcon.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/EditStarttime',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  }

  function  Editendtime(val,column,subjectID,editValue){

  $(val).css("background","#FFF url("+base_url+"uploads/images/loaderIcon.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/Editendtime',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  } 
  function  CallProfessor(val,column,subjectID,editValue){
    

  $(val).css("background","#FFF url("+base_url+"uploads/images/loaderIcon.gif) no-repeat right");

// alert($val);

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/editProfessor',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  } 

  function Callsubjectmode(val,column,subjectID,editValue)
  {
     $(val).css("background","#FFF url("+base_url+"uploads/images/loaderIcon.gif) no-repeat right");

// alert($val);

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/editsubjectmode',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            }); 
  }

// Delete Online Reader
         
function  deleteLMS(id){
 var deleteLms = confirm('Are You sure! You want to delete this item ?');
 
// var classesID =  uri;
// alert(classesID);
if (deleteLms == true) {
    $.ajax({
        type: "POST",
        url:base_url+"online_reader/deleteLMS",
        data:{"id":id},
        success: function(response) {
          
          location.reload();
            $("#appendSubcourses").html(response);
        }
    });
}else{
    return false;
    }
  }

  function  deleteLMSPdf(id){
 var deleteLMSPdf = confirm('Are You sure! You want to delete this item ?');

if (deleteLMSPdf == true) {
    $.ajax({
        type: "POST",
        url:base_url+"lms/deleteLMS",
        data:{"id":id},
        success: function(response) {
          
          location.reload();
            $("#appendSubcourses").html(response);
        }
    });
}else{
    return false;
    }
  }


// TimeTable Datatable

  $(document).ready(function(){  
    var dataTable = $('#timeTable').DataTable({
      dom: 'lBfrtip',
        scrollX:true,
        scrollCollapse: true,
        "bPaginate": false,
      buttons: [
        'print'
      ],

    });
  });

//live stream
$(document).ready(function(){

      var dataTable = $('#StreamTables').DataTable({
            scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

   { extend: 'excelHtml5', text: 'Download Excel' }

              ],
               fixedColumns:   {

            leftColumns: 3

        },

          search: false,

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
             paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

           "processing":true,

           "serverSide":true,

           "order":[],

           "bStateSave": true, 

           "ajax":{

                url:base_url+'live/AjaxTable',

                type:"POST"

           },

  "columnDefs": [

    { "orderable": false,"targets":0},
     { "orderable": false,"targets":2},

    { "orderable": false,"targets":4}    

 ],
        

      "columns": [

              { "data": "sn" },
              {"data":"stream_name"},

              { "data": "channel_name" },

              { "data": "av" },

              { "data": "status" },

              { "data": "action" },

           ],
 
 


      });

 });