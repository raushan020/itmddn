<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Admin_Controller extends MY_Controller {



	function __construct () {



		parent::__construct();


		$this->load->model("signin_m");



		$this->load->model("message_m");



		$this->load->model("site_m");



		  $this->load->model("student_m");



		$this->load->library("session");



		$this->load->helper('language');



		$this->load->helper('date');



		$this->load->helper('form');



		$this->load->library('form_validation');



        $this->data["siteinfos"] = $this->site_m->get_site(array('adminID'=>$this->session->userdata("adminID")));
  


      if($this->session->userdata("usertype")=="Student"){

		$this->data['data_single_student'] = $this->student_m->get_single_student(array('username'  => $this->session->userdata('username')));



		 $this->data["data_single_sub_courses"] = $this->student_m->get_subCourses($this->data["data_single_student"]->sub_coursesID);

	

		 $this->data["data_single_class"] = $this->student_m->get_class($this->data["data_single_student"]->classesID);

      }

		

		/* Alert System Start.........*/

		$this->load->model("notice_m");

		$this->load->model("alert_m");

		$this->data['all'] = array();

		$this->data['alert'] = array();

		$notices = $this->notice_m->get_notice();

		$i = 0;

		$this->data['alert'];

		/* Alert System End.........*/

		/*message counter*/

		$email = $this->session->userdata('email');

		$usertype = $this->session->userdata('usertype');

		if ($usertype=='Student') {

	    $loginuserID = $this->session->userdata('loginuserID');  

	    $classesID = $this->student_m->Student_class($loginuserID);
	    $cls =  $classesID->classesID;
	    

		$this->data['notices_icon_count'] = $this->notice_m->notice_icon_count($loginuserID); 

	 	$this->data['notices_read_count'] = $this->notice_m->notice_read_count($loginuserID);
		


		$this->data['notice_list_icon'] = $this->notice_m->notice_list_icon($loginuserID);

		//echo $this->db->last_query();



        }

		// $userID = $this->userID();



		$this->data['unread'] = $this->message_m->get_order_by_message(array('email' => $email, 'receiverType' => $usertype, 'to_status' => 0, 'read_status' => 0));



		/*message counter end*/



		$language = $this->session->userdata('lang');



		$this->lang->load('topbar_menu', $language);



		$exception_uris = array(



			"signin/index",



			"signin/signout"



		);

if ($this->signin_m->loggedin() == TRUE) {

	    $this->signin_m->checkin(); 

   }

  
		if(in_array(uri_string(), $exception_uris) == FALSE) {



			if($this->signin_m->loggedin() == FALSE) {
				$URI = "$_SERVER[REQUEST_URI]";

            $uriMain= str_replace('','',$URI);

				redirect(base_url("signin/index?redirect=".$uriMain));



			}



		}
// echo $_SERVER['HTTP_USER_AGENT'] . "\n\n";
// $browser = get_browser(null, true);
// print_r($browser);
// exit();


	}



}





/* End of file Admin_Controller.php */



/* Location: .//D/xampp/htdocs/school/mvc/libraries/Admin_Controller.php */