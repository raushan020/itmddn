<?php

class Signup_m extends Admin_Model{

	public function __construct(){
		parent::__construct();
		}

	public function insertData($array)
	{
		// print_r($array);
		// exit();
		$this->db->insert('company',$array);
		return $this->db->insert_id();
	}

	public function get_country(){
		$query = $this->db->get('country');
		return $query->result();
	}

	
	public function hash($string) {
		return hash("sha512", $string ."24gfhfymgtrfhuhtrfg");
	}
 

}