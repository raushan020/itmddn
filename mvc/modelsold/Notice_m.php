<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Notice_m extends Admin_Model {



	protected $_table_name = 'notice';

	protected $_primary_key = 'noticeID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "date DESC";



	function __construct() {

		parent::__construct();

	}



	function get_notice($array=NULL, $signal=FALSE) {

		$query = parent::get($array, $signal);

		return $query;

	}




	function notic_record_admin($array=NULL, $signal=FALSE) {
        $adminID = $this->session->userdata("adminID");
		$this->db->select('*');

		$this->db->from('notice');
		$this->db->where('adminID',$adminID);

         $this->db->order_by('date', 'DESC');
         
           $this->db->limit('5');
         
		$query = $this->db->get();
 
		return $query->result_array();

	}


	public function notic_record($array){
		$adminID = $this->session->userdata("adminID");
		$this->db->select('*');
		$this->db->from('notice');
		$this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');

         $this->db->where($array); 
         $this->db->where('adminID',$adminID);
         $this->db->order_by('date','DESC');       
		$query = $this->db->get(); 
		return $query->result_array(); 
	}

		public function notic_record_5($array){
			$adminID = $this->session->userdata("adminID");
		$this->db->select('*');
		$this->db->from('notice');
		$this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
         $this->db->where($array);  
         $this->db->where('adminID',$adminID); 
        $this->db->limit(5);     
		$query = $this->db->get(); 
		return $query->result_array(); 
	}

 
	function get_notice_Acc($array) {   
        $adminID = $this->session->userdata("adminID");
		$this->db->select('*');
	    $this->db->from('notice');
        $this->db->join('professor','professor.professorID = notice.userID','inner');
        $this->db->where('notice.adminID',$adminID);
        $this->db->where($array);
        $this->db->order_by('noticeID', 'DESC');
        $this->db->limit('5');
		$query = $this->db->get(); 
		return $query->result_array();		

	}


function get_classes_by_notice($noticeID){

$this->db->where('noticeID',$noticeID);
return $this->db->get('notice_x_classes')->result();

}


function get_notice_array($array) {
 
		$this->db->select('*');

		$this->db->from('notice');
     
        $this->db->where($array);

      	$this->db->order_by('date','DESC');
        $this->db->limit('5');
		$query = $this->db->get(); 
		return $query->result_array();

	}


	function sender_data($userID,$type){
		 
		$this->db->select('*');
		$this->db->from('notice');

		if($type =='Accountant'){
			$this->db->join('user','user.usertype = notice.usertype');
			$this->db->where('user.userID',$userID);
		}
		if($type =='Admin'){
			$this->db->join('user','user.usertype = notice.usertype');
			$this->db->where('user.userID',$userID);
		}
		elseif($type =='Professor'){
			$this->db->join('professor','professor.usertype = notice.usertype');
			$this->db->where('professor.professorID',$userID);
		}
		elseif($type =='ClgAdmin'){  
			 $this->db->join('admin','admin.usertype = notice.usertype' ,'inner');
			 $this->db->where('admin.adminID',$userID);

		}
	
		  $query = $this->db->get()->row();
		  	          /*print_r( $query);
	          exit();*/
	return $query;
	}



	function get_user_notice($userID=null)
	{
		$usertype  = $this->session->userdata('usertype');
		
		$this->db->select('*');

		$this->db->from('notice');	 	

	 	if ($usertype=="Accountant") {
	 	 $this->db->where('usertype',$usertype);
	 	 $this->db->where('adminID',$userID);
	 	}
	    if ($usertype=="Professor") {
	    	$this->db->where('userID',$userID);
	 	 $this->db->where('usertype',$usertype);
	 	}
	 if ($usertype=="Admin") {
	 	 $this->db->where('usertype !=','ClgAdmin');
	 	} 
 		$this->db->order_by('date','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
 


//

	function get_join_notice_count($adminID) {

		$this->db->select('*');

		$this->db->from('notice');

		$this->db->join('classes', 'classes.ClassesID = notice.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = notice.sub_coursesID', 'LEFT');


		if ($this->session->userdata('FilterNoticeclassesID')) {
		 $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		}

		if ($this->session->userdata('FilterNoticesubCourseID')) {
		 $this->db->where('notice.sub_coursesID', $this->session->userdata('FilterNoticesubCourseID'));
		}

		if ($this->session->userdata('FilterNoticeyearSemesterID')) {
		 $this->db->where('notice.yearsOrSemester', $this->session->userdata('FilterNoticeyearSemesterID'));
		}

	     if ($this->session->userdata('ActiveNotices')) {
		 $this->db->where('notice.status', $this->session->userdata('ActiveNotice'));
		}

	   if ($this->session->userdata('DraftNotices')) {
		 $this->db->where('notice.status', 0);
		}

	   if ($this->session->userdata('TrashNotices')) {
		 $this->db->where('notice.status', $this->session->userdata('TrashNotice'));
		}	

		$this->db->where('notice.adminID', $adminID);
		$this->db->order_by('notice.date','DESC');

		$query = $this->db->get();

		return $query->num_rows();

	}

	function make_datatables($adminID){  
           $this->get_join_notice();  
           $query = $this->db->get();  
           return $query->result();  
      } 

      function get_join_notice() {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('notice.*,classes.*,sub_courses.*,notice.status as studentStatus,notice.date as noticeDate');
		$this->db->from('notice');
		$this->db->join('classes', 'classes.ClassesID = notice.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = notice.sub_coursesID', 'LEFT');


		if ($this->session->userdata('FilterNoticeclassesID')) {
		 $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		}

		if ($this->session->userdata('FilterNoticesubCourseID')) {
		 $this->db->where('notice.sub_coursesID', $this->session->userdata('FilterNoticesubCourseID'));
		}

		if ($this->session->userdata('FilterNoticeyearSemesterID')) {
		 $this->db->where('notice.yearsOrSemester', $this->session->userdata('FilterNoticeyearSemesterID'));
		}

	   if ($this->session->userdata('ActiveNotices')) {
		 $this->db->where('notice.status', $this->session->userdata('ActiveNotice'));
		}

	   if ($this->session->userdata('DraftNotice')) {
		 $this->db->where('notice.status', 0);
		}

	   if ($this->session->userdata('TrashNotice')) {
		 $this->db->where('notice.status', $this->session->userdata('TrashNotice'));
		}	


		$this->db->where('notice.adminID', $adminID);


if(isset($_POST["search"]["value"]))  
           {  
 $searches  =  $_POST["search"]["value"];

		$order_column = array( 

                            0 =>'sn', 
                            1 =>'title',
                            2 =>'date',
                            3 => 'classes',
                            4 => 'sub_course',
                            5 => 'yearsOrSemester',
                            6 =>'notice',
                            
                            
                        );
           $where = "(notice.title LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR notice.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                // $this->db->order_by('notice.noticeID', 'DESC');  
           	$this->db->order_by('notice.date','DESC');
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       }

	}


	 

	 

	function insert_notice($array) {

		$error = parent::insert($array);

		return $error;

	}

	function notice_byname($table_name, $userID, $pk){
		$this->db->where($pk, $userID);
		return $this->db->get($table_name)->row();
}

	function get_notice_limit($array) {


		 $query = $this->db->query('SELECT noticeID, noticename from notice order by noticeID desc limit 6');
		 return $query->result();
	}


	function notice_x_classes($array, $i, $count, $classesID) {

$this->db->where('noticeID',$array['noticeID']);
$this->db->where('classesID',$array['classesID']);
$num = $this->db->get('notice_x_classes')->num_rows();
if($num==0){

 	 	$this->db->insert('notice_x_classes',$array);
 }else{
 $this->db->where('noticeID',$array['noticeID']);
 $this->db->where('classesID',$array['classesID']);	
 $this->db->update('notice_x_classes',$array);	
 }
$count_one = $count-1;   
if($count_one==$i){
$this->db->where('noticeID',$array['noticeID']);
$this->db->where_not_in('classesID',$classesID);
$this->db->delete('notice_x_classes');	
}

	}


	function notice_x_student($classesID,$semesterOryear,$noticeID,$i,$count) {


 

	}

	function notice_x_email_student($array) {

  if ($array['notice_type']=='course') {
 	 $this->db->where('classesID',$array['classesID']);
 }elseif ($array['notice_type']=='sub_course') {
 	 $this->db->where('classesID',$array['classesID']);
 	 $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 }elseif ($array['notice_type']=='with_year_semester') {
 		 $this->db->where('classesID',$array['classesID']);
 	     $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']); 
 }elseif ($array['notice_type']=='only_year_semester') { 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']);
 }

 $object = $this->db->get('student')->result();
 	 // foreach ($object as $key => $value) {

 	 // 	$data = array(
 	 //    'studentID' =>$value->studentID,
 	 //    'noticeID'=>$array['noticeID'] 
 	 // 	);

 	 // 	$this->db->insert('notice_x_student',$data);
 	 // }
 
 

	}



	function notice_x_student_update($array) {


$this->db->where('noticeID',$array['noticeID']);
$this->db->delete('notice_x_student');

  if ($array['notice_type']=='course') {
 	 $this->db->where('classesID',$array['classesID']);
 }elseif ($array['notice_type']=='sub_course') {
 	 $this->db->where('classesID',$array['classesID']);
 	 $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 }elseif ($array['notice_type']=='with_year_semester') {
 		 $this->db->where('classesID',$array['classesID']);
 	     $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']); 
 }elseif ($array['notice_type']=='only_year_semester') { 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']);
 }

 $object = $this->db->get('student')->result();
 	 foreach ($object as $key => $value) {

 	 	$data = array(
 	    'studentID' =>$value->studentID,
 	    'noticeID'=>$array['noticeID'] 
 	 	);

 	 	$this->db->insert('notice_x_student',$data);
 	 }
 

	}
	
function delete_notice_x_student($id){
	$this->db->where('noticeID',$id);
    $this->db->delete('notice_x_student');
}



	function getExamByID($id){
		$this->db->where('quid',$id);
		$query  = $this->db->get('ets_quiz');
		 return $query->row();
	}

function notice_icon_count($id){

		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
	     $this->db->where('notice_x_student.studentID',$id);
		$this->db->where('notice_x_student.iconStatus',1);
		$this->db->limit(6);  
		//print_r($this->db->last_query());  
	//exit();	
	    return $this->db->get()->num_rows();
}

function notice_read_count($id){

		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
	    $this->db->where('notice_x_student.studentID',$id);
		$this->db->where('notice_x_student.readStatus',1);
	    return	$this->db->get()->num_rows();
}
 
function notice_list_icon($id)  {
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$where = "notice_x_student.studentID = '".$id."' and notice_x_student.readStatus = '1'";
	    $this->db->where($where);
		$this->db->order_by('notice.noticeID','DESC');
	    $this->db->limit(6);		  
	    return	$this->db->get()->result();
	}


function notice_list_all($id){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');	    
	    $this->db->where('notice_x_student.studentID',$id);
	    return	$this->db->get()->result();
}

function notice_single($studentID,$noticeID)
{
	$usertype = $this->session->userdata("usertype");
	if($usertype == "Professor" || $usertype == "Accountant" )
	{
		$this->db->from('notice');
		$this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		//$this->db->where('notice_x_student.studentID',$studentID);
		$this->db->where('notice.userID',$studentID);
		$this->db->where('notice.noticeID',$noticeID);
		return	$this->db->get()->row();
	}
	else
	{
		$this->db->select('*');
		$this->db->from('notice');
		$this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$studentID);
		// $this->db->where('notice.userID',$studentID);
		$this->db->where('notice.noticeID',$noticeID);
		return	$this->db->get()->row();
	}
}

function update_notice_x_student_read($studentID,$noticeID){	
$data = array(
'readStatus'=>0,
'iconStatus'=>0
);
	$this->db->where('studentID',$studentID);
	$this->db->where('noticeID',$noticeID);
	$this->db->update('notice_x_student',$data);
}

	function Getnotice($adminID,$classesID,$sub_coursesID,$semester_idSelect){

		$this->db->where('adminID',$adminID);
		$this->db->where('classesID',$classesID);
		$this->db->where('sub_coursesID',$sub_coursesID);
		$this->db->where('yearsOrSemester',$semester_idSelect);
		$query = $this->db->get('notice');
		 return $query->result();
	}


	function update_notice($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}



	public function delete_notice($id){

		parent::delete($id);

	}

	public function get_join_where_notice($classesID,$sub_coursesID){

	$this->db->select('notice.*,classes.*,sub_courses.*,notice.date as noticeDate');
		$this->db->from('notice');
	    $this->db->join('classes', 'classes.ClassesID = notice.classesID', 'LEFT');
		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = notice.sub_coursesID', 'LEFT');
		$this->db->where('notice.classesID',$classesID);
		$this->db->where('notice.sub_coursesID',$sub_coursesID);

	 return	$this->db->get()->result();


}



}



	


/* End of file notice_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/notice_m.php */