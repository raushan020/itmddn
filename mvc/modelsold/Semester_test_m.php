<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Semester_test_m extends Admin_Model {

	// protected $_table_name = 'subject';
	// protected $_primary_key = 'subjectID';
	// protected $_primary_filter = 'intval';
	// protected $_order_by = "classesID asc";

	function __construct() {
		parent::__construct();
	}

	function get_semester() {

		$query = $this->db->get('semester');
		return $query->result_array();
		
	}


}

/* End of file subject_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/subject_m.php */
