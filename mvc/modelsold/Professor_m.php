<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professor_m extends Admin_Model {

	protected $_table_name = 'professor';

	protected $_primary_key = 'professorID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "name asc";

	function __construct() {

		parent::__construct();

	}

	function get_username($table, $data=NULL) {

		$query = $this->db->get_where($table, $data);

		return $query->result();

	}

	function get_professor($array=NULL, $signal=FALSE) {
        $adminID = $this->session->userdata("adminID");
        $this->db->where('adminID',$adminID);
		$query = parent::get($array, $signal);
		return $query;
	}
	function get_professor_bycustommail($adminID)
	{
		$this->db->select('*');
		$this->db->from('professor');
		$this->db->where('status',1);
		$this->db->where('adminID',$adminID);
		return $this->db->get()->result();
	}

	function change_password_with_professor_by_superadmin($id,$newPassword){
		
		$data = array(
			'password' => $newPassword, 
		);
		$adminID = $this->session->userdata("adminID");
		$this->db->where('adminID',$adminID);
		$this->db->where('professorID',$id);
		$this->db->update('professor',$data);
	}

function total_lecture(){
	$loginuserID  =$this->session->userdata('loginuserID');
	return $this->db->where('professorID',$loginuserID)->get('subject')->num_rows();
}
	function get_professor_with_join($array=NULL, $signal=FALSE) {
		$adminID = $this->session->userdata("adminID");
		$this->db->join('department','professor.departmentID=department.departmentID','LEFT');
		$this->db->where('professor.adminID',$adminID);
		$query = parent::get($array, $signal);
		return $query;
	}



	function get_single_professor($array) {
$this->db->join('designation', 'professor.designationID = designation.designationID','LEFT');
$this->db->join('department','professor.departmentID=department.departmentID','LEFT');
		$query = parent::get_single($array);

		return $query;

	}


		function get_professor2($array=NULL, $signal=FALSE) {
		
		return $this->db->where($array)->get('professor')->row();
	}
	
 function get_professor_all($adminID){
	$this->db->where('status',1);
	$this->db->where('adminID',$adminID);
	  return $this->db->get('professor')->result();
 }
 function get_professor_all_by_superAdmin()
 {
 	$this->db->where('status',1);
	  return $this->db->get('professor')->result();
 }

function get_department($departmentID){

	$this->db->where('department');
	$this->db->where('departmentID',$departmentID);
		return $this->db->get('department')->row();

}


	function get_professor_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");
		$this->db->join('designation', 'professor.designationID = designation.designationID', 'left');
		$this->db->join('department', 'professor.departmentID = department.departmentID', 'left');
		if ($this->session->userdata('ActiveProfessor')) {
		 $this->db->where('professor.status', $this->session->userdata('ActiveProfessor'));
		}
	   	if ($this->session->userdata('DraftProfessor')) {
		 $this->db->where('professor.status', 0);
		}
	   	if ($this->session->userdata('TrashProfessor')) {
		 $this->db->where('professor.status', 2);
		}
		if (empty($this->session->userdata('DraftProfessor')) && empty($this->session->userdata('TrashProfessor'))) {
           $this->db->where('professor.status',1);
        }
	    $this->db->where('professor.adminID',$adminID);
	 
		if(isset($_POST["search"]["value"]))  
           {  

			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 => 'sn', 
                            1 => 'name',
                            2 => 'username',
                            3 => 'designation_name',
                            4 => 'department_name',
                            5 => 'email',
                            6 => 'subject_progress',
                            7 => 'action'
                        );

           $where = "(professor.name LIKE '%$searches%' OR professor.username LIKE '%$searches%' OR professor.email LIKE '%$searches%')";
           $this->db->where($where);
        
           
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('professor.professorID', 'DESC');  
           } 
          
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       } 

	}
	
	   function make_datatables(){  
	   	
        $this->get_professor_ClgAdmin();  
   	    $query = $this->db->get('professor'); 
		return $query->result();  
      } 
	
	function get_professor_no($adminID)
	{
		if ($this->session->userdata('ActiveProfessor')) {
		 $this->db->where('status', $this->session->userdata('ActiveProfessor'));
		}
	   	if ($this->session->userdata('DraftProfessor')) {
		 $this->db->where('status', $this->session->userdata('DraftProfessor'));
		}
	   	if ($this->session->userdata('TrashProfessor')) {
		 $this->db->where('status', $this->session->userdata('TrashProfessor'));
		}
		if (empty($this->session->userdata('DraftProfessor')) && empty($this->session->userdata('TrashProfessor'))) {
           $this->db->where('status',1);
        }
		// raushan
		 if(isset($_POST["search"]["value"]))  
            {  

		 	$searches  =  $_POST["search"]["value"];

		 	$order_column = array( 

                            0 => 'sn', 
                            1 => 'name',
                            2 => 'username',
                            3 => 'designation_name',
                            4 => 'department_name',
                            5 => 'email',
                            6 => 'action'
                         );

            $where = "(professor.name LIKE '%$searches%' OR professor.username LIKE '%$searches%' OR professor.email LIKE '%$searches%')";
          $this->db->where($where);
       }
		 	// raushan end


		$this->db->where('adminID',$adminID);
		   
	    $query = $this->db->get('professor'); 
		return $query->num_rows();
	}

	function get_professor_ClgAdminID($adminID) {
	$this->db->where('adminID',$adminID);
	   $this->db->where('status',1);
	$query = $this->db->get('professor');
	 return $query->result();
	}

	function get_order_by_professor($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}

	function getstudentforcondition($getClasses,$getSubcourse,$getSemester,$getSubject)
	{
        $this->db->select('subject_mode');
        $this->db->from('subject');
        $this->db->where('subjectID',$getSubject);
        $a=$this->db->get()->row();

        if($a->subject_mode==0)
        {
        	$this->db->select('name,studentID,roll');
			$this->db->where('classesID',$getClasses);
			// $this->db->where('sub_coursesID',$getSubcourse);
			$this->db->where('yearsOrSemester',$getSemester);
		   	$this->db->where('status',1);
		   	$this->db->order_by('name','ASC');
			$query = $this->db->get('student');
			 return $query->result();
        }
        else
        {
        	$this->db->select('student.name,optionalsubject.studentID,student.roll');
        	$this->db->from('optionalsubject');
        	$this->db->join('student','student.studentID=optionalsubject.studentID','left');
			$this->db->where('optionalsubject.courseID',$getClasses);
			$this->db->where('optionalsubject.yearsOrSemester',$getSemester);
			$this->db->where('optionalsubject.subjectID',$getSubject);
		   	$this->db->where('student.status',1);
		   	$this->db->order_by('student.name','ASC');
			$query = $this->db->get();
			return $query->result();
        }

		
	}


	function getstudent_from_atnd($getSubject,$loginuserID,$date){

	$this->db->from('student');
	$this->db->join('attendance_student','student.studentID=attendance_student.studentID','INNER');
	$this->db->where('attendance_student.subjectID',$getSubject);
	// $this->db->where('attendance_student.professorID',$loginuserID);
   $this->db->where('attendance_student.atd_date',$date);

   return $this->db->get()->result();


	}

	
	function get_classes($departmentID){
		$this->db->where('departmentID',$departmentID);
		return $this->db->get('classes')->result();
		// print_r($a);die;
	
	}


		function get_student($departmentID){
		$this->db->join('student','classes.classesID=student.classesID','INNER');	
		$this->db->where('departmentID',$departmentID);
		return $this->db->get('classes')->result();
	
	}

	function get_subject($departmentID){
		$this->db->join('subject','classes.classesID=subject.classesID','INNER');	
		$this->db->where('departmentID',$departmentID);
		return $this->db->get('classes')->result();
	
	}




	function get_professor_datatables() {
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->join('sub_courses', 'sub_courses.sub_coursesID=subject.sub_coursesID', 'left');
		$this->db->join('classes', 'classes.classesID = subject.classesID', 'left');
	    $this->db->where('subject.professorID',$loginuserID);
		$query = $this->db->get('subject')->result();

		return $query;
	}

	function get_attendance() {
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		
		$this->db->from('attendance_student');
		$this->db->join('student', 'attendance_student.studentID = student.studentID', 'INNER');
	    
		$query = $this->db->get()->result();
		return $query;
	}


	function insert_professor($array) {

		$error = parent::insert($array);
		return TRUE;

	}

	function update_professor($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}

	function delete_professor($id){

$data  = array(
	'status' =>2,
	 );
       $adminID = $this->session->userdata("adminID");
       $this->db->where('adminID',$adminID);
	   $this->db->where('professorID',$id);
	   $this->db->update('professor',$data);
	}

	function insert_attendance($array) {

		$this->db->insert('attendance_student',$array);
		return TRUE;

	}

	function hash($string) {

		return parent::hash($string);

	}
	// and subject.status = '1'
	function get_professor_data($loginuserID)
	{
		$this->db->select('professor_lecture.id,professor_lecture.department_id,professor_lecture.course_id,professor_lecture.semester,professor_lecture.subject_id,professor_lecture.professor_id,professor_lecture.days,professor_lecture.times,classes.classes,subject.subject,professor.name,department.department_name');
		$this->db->from('professor_lecture');
		$this->db->join('classes','professor_lecture.course_id = classes.classesID','inner');
		$this->db->join('subject','professor_lecture.subject_id = subject.subjectID','inner');
		$this->db->join('professor','professor_lecture.professor_id = professor.professorID','inner');
		$this->db->join('department','professor_lecture.department_id = department.departmentID','inner');
		$where = "professor_lecture.professor_id = '".$loginuserID."' and subject.status = '1' and professor.status = '1'";
		$this->db->where($where);
		$this->db->group_by(array('professor_lecture.subject_id','professor_lecture.times'));
		$sqls = $this->db->get();
		if($sqls->num_rows() > 0)
		{
			$results = $sqls->result();
			return $results;
		}
		else
		{
			$this->session->set_flashdata('msg','<p style="color:red;text-align:center;">No record found!</p>');
		}
	}
	
	function professors_notification()
    {
    	$query = $this->db->query('select create_usertype, notification, notification_date from professor where status = 1 and notification = "1" and notification_date = "'.date('Y-m-d').'"');
    	if($query->num_rows() > 0)
    	{
    		return $query->result();
    	}
    	else
    	{
    		echo "";
    	}
    }
    public function checksubjectid($subjectid)
    {
    	$this->db->select('subjectid');
    	$this->db->from('complete_topic');
    	$this->db->where('subjectid',$subjectid);
    	return $this->db->get()->row();
    }
    public function insertcompletetopic($insertcompletetopic)
    {
    	$this->db->insert('complete_topic',$insertcompletetopic);
    	return TRUE;
    }
    public function updatecompletetopic($updatecompletetopic,$subjectid)
    {
    	$this->db->where('subjectid',$subjectid);
    	$this->db->update('complete_topic',$updatecompletetopic);
    	return TRUE;
    }
    public function getsubjectandtopic($loginuserID)
    {
    	// $this->load->library('subquery');
    	$this->db->select('s.subjectID,s.subject,s.yearsOrSemester,c.classes');
    	$this->db->from('subject as s');
    	$this->db->join('classes as c','c.classesID=s.classesID','left');
    	$this->db->where('s.professorID',$loginuserID);
    	$this->db->where('s.status',1);
    	$a= $this->db->get()->result_array();
    	
    	foreach ($a as $key => $value)
    	{
    		// print_r($a->subjectID);die;
    		$this->db->select('s.professorID,s.subjectID,c.unitid,c.topicid,s.subject,u.unitID,count(ut.unitID) as totalunit');
    		$this->db->from('subject as s');
    		$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	    $this->db->join('complete_topic as c','c.subjectid=s.subjectID','left');
    	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
    	    $this->db->where('c.professorID',$loginuserID);
    	    $this->db->where('s.subjectID',$value['subjectID']);
            $this->db->where('s.status',1);
    	$a[$key]['object']= $this->db->get()->result();
    	
    		
    	}

    	// echo "<pre>";
    	// print_r($a);
    	// exit();
    	 return $a;
    	    // print_r($b);die;count(att.studentID) as totalattendance,
    	
    }

    function getstudentforprogress($getClasses,$getSubcourse,$getSemester,$getSubject)
    {
    	// print_r($getSubcourse);die;
    	$this->db->select('subject_mode');
        $this->db->from('subject');
        $this->db->where('subjectID',$getSubject);
        $b=$this->db->get()->row();

        if($b->subject_mode==0)
        {
	 		$this->db->select('name,studentID,roll');
			$this->db->where('classesID',$getClasses);
			$this->db->where('sub_coursesID',$getSubcourse);
			$this->db->where('yearsOrSemester',$getSemester);
		   	$this->db->where('status',1);
		   	$this->db->order_by('name','ASC');
			$a = $this->db->get('student')->result_array();
			foreach ($a as $key => $value)
			{
				$this->db->select('att.atd,studentID');
				$this->db->from('attendance_student as att');
				$this->db->where('att.studentID',$value['studentID']);
				$this->db->where('att.classesID',$getClasses);
				$this->db->where('att.subjectID',$getSubject);
				$a[$key]['object']= $this->db->get()->result();
			}
			// echo "<pre>";
			// print_r($a);die;
			return $a;
		}
		else
		{
			$this->db->select('student.name,optionalsubject.studentID,student.roll');
			$this->db->from('optionalsubject');
        	$this->db->join('student','student.studentID=optionalsubject.studentID','left');
			$this->db->where('optionalsubject.courseID',$getClasses);
			$this->db->where('optionalsubject.yearsOrSemester',$getSemester);
			$this->db->where('optionalsubject.subjectID',$getSubject);
		   	$this->db->where('student.status',1);
		   	$this->db->order_by('student.name','ASC');
			$a = $this->db->get()->result_array();
			foreach ($a as $key => $value)
			{
				$this->db->select('att.atd,studentID');
				$this->db->from('attendance_student as att');
				$this->db->where('att.studentID',$value['studentID']);
				$this->db->where('att.classesID',$getClasses);
				$this->db->where('att.subjectID',$getSubject);
				$a[$key]['object']= $this->db->get()->result();
			}
			// echo "<pre>";
			// print_r($a);die;
			return $a;
		}
	}
	public function getstudentattendance($loginuserID)
	{
		// print_r($loginuserID);die();
		$this->db->select('subject.subjectID,student.classesID,student.yearsOrSemester,student.studentID,student.name as studentname');
		$this->db->from('professor');
		$this->db->join('subject','subject.professorID=professor.professorID','left');
	    $this->db->join('student','student.classesID=subject.classesID','left');
	    $this->db->where('professor.professorID',$loginuserID);
	    $this->db->where('student.status',1);
	    $this->db->order_by('student.name','ASC');
	    $a=$this->db->get()->result_array();
	    foreach ($a as $key => $value)
		{
			$this->db->select('att.atd,studentID');
			$this->db->from('attendance_student as att');
			$this->db->where('att.studentID',$value['studentID']);
			$this->db->where('att.classesID',$value['classesID']);
			$this->db->where('att.subjectID',$value['subjectID']);
			$a[$key]['object']= $this->db->get()->result();
		}
	    // echo "<pre>";
	    // print_r($a);die;
	    return $a;
	}
	public function get_alldepartment($adminID)
	{
		$this->db->select('*');
		// $this->db->where('departmentID',$departmentID);
		return $this->db->where('adminID',$adminID)->get('department')->result();
	}
	public function get_classlist($departmentID)
	{
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->where('departmentID',$departmentID);
		return $this->db->get()->result();
	}

   
	function ActiveProfessor_count($adminID){

		$this->db->from('professor');
		$this->db->where('adminID',$adminID);
		$this->db->where('status',1);
		$query	= $this->db->get();
		return $query->num_rows();

	}

	function DraftProfessor_count($adminID){

		$this->db->from('professor');
		$this->db->where('adminID',$adminID);
		$this->db->where('status',0);
		$query	= $this->db->get();
		return $query->num_rows();
		
	}

	function TrashProfessor_count($adminID){

		$this->db->from('professor');
		$this->db->where('adminID',$adminID);
		$this->db->where('status',2);
		$query	= $this->db->get();
		return $query->num_rows();
		
	}
	public function getall_professor($adminID)
	{
		$this->db->where('adminID',$adminID);
		$this->db->where('status',1);
		$this->db->from('professor');
		
		$query	= $this->db->get()->result();
		return $query;
	}
	public function all_student_attendance_bymonth($month,$adminID)
	{
		$date=date('Y');
		// print_r($date);die;
		$this->db->select('classesID,classes');
		$this->db->from('classes');
		$this->db->where('adminID',$adminID);
		$getallcourse=$this->db->get()->result_array();
		foreach ($getallcourse as $key => $value)
		{
			$this->db->select('count(atd) as totalpresent');
			$this->db->from('attendance_student');
			$this->db->where('classesID',$value['classesID']);
			$this->db->where('month(atd_date)',$month); 
			$this->db->where('year(atd_date)',$date);  
			$this->db->where('atd',1); 
			$getallcourse[$key]['student_attendance']=$this->db->get()->result();

		}
		if ($getallcourse) {
			return $getallcourse;
			
		}
		else
		{
			// $getallcourse="FALSE";
			return false;
		}
		// print_r($getallcourse);die;
		
	}
	public function totalattendancebycoursewise($month,$classesID)
	{
		$date=date('Y');
        $this->db->select('count(atd_date) as totalstudent');
			$this->db->from('attendance_student');
			$this->db->where('classesID',$classesID);
			$this->db->where('month(atd_date)',$month);
			$this->db->where('year(atd_date)',$date);    
			return $this->db->get()->row();
	}
	public function getprofessoremailid($professorid)
	{
		$this->db->select('email,name');
		$this->db->from('professor');
		$this->db->where('professorID',$professorid);
		$this->db->where('status',1);
		return $this->db->get()->row();
	}
	public function getsuperadminid($superadminid)
	{
		$this->db->select('email,name');
		$this->db->from('admin');
		$this->db->where('adminID',$superadminid);
		return $this->db->get()->row();
	}
	public function getadminid($adminid)
	{
		$adminID = $this->session->userdata("adminID");
		$this->db->select('email,name');
		$this->db->from('user');
		$this->db->where('adminID',$adminID);
		$this->db->where('userID',$adminid);
		return $this->db->get()->row();
	}
	public function getemailidsender($loginuserID)
	{
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Accountant")
		{
			$this->db->select('email');
			$this->db->from('user');
			$this->db->where('userID',$loginuserID);
			return $this->db->get()->row();
		}
		if($usertype == "ClgAdmin")
		{
			$this->db->select('email');
			$this->db->from('admin');
			$this->db->where('adminID',$loginuserID);
			return $this->db->get()->row();
		}
		if($usertype == "Professor")
		{
			$this->db->select('email');
			$this->db->from('professor');
			$this->db->where('professorID',$loginuserID);
			return $this->db->get()->row();
		}
		
	}
	public function superadminid($adminID)
	{
		$this->db->select('adminID,email');
		$this->db->from('admin');
		$this->db->where('adminID',$adminID);
	 	return $this->db->get()->result();

	}
	public function adminid($adminID)
	{
		$this->db->select('userID,email');
		$this->db->from('user');
		$this->db->where('usertype',"Admin");
		$this->db->where('adminID',$adminID);
	 	return $this->db->get()->result();

	}
	public function accountantid($adminID)
	{
		$this->db->select('userID');
		$this->db->from('user');
		$this->db->where('usertype',"Accountant");
		$this->db->where('adminID',$adminID);
	 	return $this->db->get()->result();
	}
	public function getreciverstudentid($studentID)
	{
		$this->db->select('email,name');
		$this->db->from('student');
		$this->db->where('studentID',$studentID);
		return $this->db->get()->row();

	}


}

