<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Signin_m extends Admin_Model {



	function __construct() {

		parent::__construct();

		$this->load->model("setting_m");

	}

	public function newusersignin()
	{ 
	    $firstName=$this->input->post('firstName');
		$lastName=$this->input->post('lastName');
		$timezone=$this->input->post('timezone');
		$fullname=$firstName.' '.$lastName;
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		$ip_address=$this->input->ip_address();
		$institutional=$this->input->post('institutional');
		$pass=$firstName.mt_rand(1,100);
		$username=$email;
		$password=$this->hash($pass);
		$insertnewuser= array('name' =>$fullname,'email'=>$email,'phone'=>$phone,'username'=>$username,'password'=>$password,'usertype'=>'ClgAdmin','create_date'=>date('y-m-d H:i:s'),'create_userID'=>1,'create_username'=>'ecampus','systemadminactive'=>0,'timezone'=>$timezone,'status'=>1 );
		$this->db->insert('admin',$insertnewuser);
		$adminid=$this->db->insert_id();
		$insertsettingnewuser= array('adminID' =>$adminid,'sname'=>$institutional,'phone'=>$phone,'address'=>"B-108,Sector 63, Noida",'email'=>$email,'automation'=>5,'currency_code'=>'INR','currency_symbol'=>'Rs.','footer'=>'Edge Technosoft Pvt. Ltd.','photo'=>'defualt.png','purchase_code'=>'f541d688-9d40-40db-99fb-65e6f80692ab','language'=>'english','theme'=>'Basic','fontorbackend'=>1,'updateversion'=>1,'attendance'=>'day' );
		$this->db->insert('setting',$insertsettingnewuser);
		$alluserdata=0;
		if($adminid)
		{
			$user = $this->db->get_where('admin', array("username" => $username, "password" => $password,'status'=>1));
			$alluserdata = $user->row();
			// print_r($alluserdata);die;
		}
		$i=1;
		
		if(count($alluserdata))
		{
			$userdata = $alluserdata;
			$array['permition'][$i] = 'yes';
		}
		else
		{
			$array['permition'][$i] = 'no';

		}
		// print_r($array['permition']);die;
		if(in_array('yes', $array['permition']))
		{
			if($userdata->usertype == "ClgAdmin")
			{
				$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
				$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->adminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					//"userID" => $userdata->userID,

					 "phone" => $userdata->phone,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"adminpackage" =>$userdata->adminpackage,
					
					"totalstudent" =>$userdata->totalstudent,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				 $sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$ip_address,
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'adminID'=>$userdata->adminID
			    );   
			    $this->db->insert('school_sessions',$sessionData);
			    $pass1['pass']=$pass;
			    $pass1['password']=$password;
			    $pass1['adminID']=$userdata->adminID;
				return $pass1;
			}
		}
		else
		{
			return null;
			// print_r("expression");die;
		}
	}
	public function checknewuserdetails($email,$phone)
	{
		$this->db->select('email,phone');
		$this->db->from('admin');
		$this->db->where('email',$email);
		$this->db->where('phone',$phone);
		return $this->db->get()->row();
	}

	public function signin() {

		$ip_address=$this->input->ip_address();
		
		$tables = array('student' => 'student', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin','professor'=>'professor');
	// print_r($tables);
	// exit();
		$array = array();

		$i = 0;

		$username = $this->input->post('username');

		$password = $this->hash($this->input->post('password'));
	
		$userdata = '';
// print_r($username);
// print_r($password);
// exit();
		foreach ($tables as $table) {

			$user = $this->db->get_where($table, array("username" => $username, "password" => $password,'status'=>1));
// print_r($user);die;
			$alluserdata = $user->row();
			// print_r($alluserdata);die;
			if(count($alluserdata)) {

				$userdata = $alluserdata;
// print_r($userdata);die;
				$array['permition'][$i] = 'yes';

			} else {

				$array['permition'][$i] = 'no';

			}

			$i++;

		}




		if(in_array('yes', $array['permition'])) {

		if($userdata->usertype == "superadmin") {
		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->adminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					//"userID" => $userdata->userID,

					 "phone" => $userdata->phone,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'adminID'=>$userdata->adminID
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;

			}
			elseif($userdata->usertype == "ClgAdmin") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->adminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

                     "phone" => $userdata->phone,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"adminpackage" =>$userdata->adminpackage,

					"totalstudent" =>$userdata->totalstudent,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

			   $sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'adminID'=>$userdata->adminID
			    );   
			   $checkipaddress=$this->db->select('ip_address')->from('school_sessions')->where(array('adminID' =>$userdata->adminID,'ip_address'=>$ip_address ))->get()->row();
				 if($checkipaddress == null)
				 {
				 	$subject="E-campus Alerts";
				 	$dataEamil['institute']=$settings->sname;
			        $dataEamil['logo']=$settings->photo;
			        $dataEamil['superadminemail']="";
		            $dataEamil['adminemail']="";
			    	$dataEamil['title'] = $subject;
					$dataEamil['date'] = date("d-m-Y");
					$dataEamil['description'] = "you are login";
				    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
				    $email=$userdata->email;
				    $fullname=$userdata->name;
				    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
				 }
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;

			}
			
			elseif($userdata->usertype == "Student") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->studentID,

					"name" => $userdata->name,

					"examType" => $userdata->examType,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

                     "phone" => $userdata->phone,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);
				$this->db->where('classesID',$userdata->classesID);
				$this->db->where('status',1);
			    $classCount =	$this->db->get('classes')->row();
			    if (count($classCount)>0) {
			     if($classCount->IsSubCourse==1){

			    $this->db->where('sub_coursesID',$userdata->sub_coursesID);
				$this->db->where('status',1);
			    $subCourseCount =	$this->db->get('sub_courses')->num_rows();	
			    if ($subCourseCount>0) {
			    $this->session->set_userdata($data);
			    $sessionData = array(
			    	'session_id' =>session_id(),
			    	'studentID'=>$userdata->studentID,
			    	'classesID'=>$userdata->classesID,
			    	'sub_coursesID'=>$userdata->sub_coursesID,
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;
			    }
			    }else{
			    $this->session->set_userdata($data);
			     $sessionData = array(
			    	'session_id' =>session_id(),
			    	'studentID'=>$userdata->studentID,
			    	'classesID'=>$userdata->classesID,
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;
			    }
			}

			} elseif($userdata->usertype == "Teacher") {
				
		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->teacherID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'teacherID'=>$userdata->teacherID
			    );   
			    $this->db->insert('school_sessions',$sessionData);


				return TRUE;

			} elseif($userdata->usertype == "Parent") {

				$data = array(

					"loginuserID" => $userdata->parentID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				return TRUE;

			} elseif($userdata->usertype == "Admin" || $userdata->usertype == "Support" || $userdata->usertype == "Accountant" || $userdata->usertype == "HOD") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
// print_r($settings);die;
		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->userID,
					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"phone" => $userdata->phone,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);
// 	print_r(session_id());
// exit();		
// $this->session->userdata('session_id') 
				$this->session->set_userdata($data);
                // $session='session_id'=>session_id();
				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->userID
			    );   

			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			}elseif($userdata->usertype == "Professor") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));

		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->professorID,
					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					 "phone" => $userdata->phone,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					 "lang" => $lang,

					 "photo" => $userdata->photo,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->professorID
			    );   
			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			}else {

 				return FALSE;

			}

		} else {

			return FALSE;

		}

	}


	function checkin(){
		$this->db->where('session_id',session_id());
		$this->db->where('current_login',1);
		$row  = $this->db->get('school_sessions')->num_rows();
		if ($row==0) {
			// print_r("axbx");die;
			$this->signout();
		}
		}


function logoutOtherDevice(){
	$usertype = $this->session->userdata("usertype");
	$loginuserID = $this->session->userdata("loginuserID");

			  $data = array(
                'current_login'=>0
               );
			if ($usertype=='Student') {
				$this->db->where('studentID',$loginuserID);
			}elseif ($usertype=='ClgAdmin') {
			$this->db->where('adminID',$loginuserID);
			}elseif ($usertype=='Teacher') {
			  $this->db->where('teacherID',$loginuserID);
			}
			$this->db->update('school_sessions',$data);

}

	function change_password() {
	
		$table = strtolower($this->session->userdata("usertype"));
// print_r($table);die;
		if($table == "admin") {

			$table = "setting";

		}

		if($table == "accountant") {

			$table = "user";

		}

		if($table == "clgadmin") {

			$table = "admin";

		}

		if($table == "librarian" || $table == "hod") {

			$table = "user";

		}

		$username = $this->session->userdata("username");

		$old_password = $this->hash($this->input->post('old_password'));

		$new_password = $this->hash($this->input->post('new_password'));

		$user = $this->db->get_where($table, array("username" => $username, "password" => $old_password));

		$alluserdata = $user->row();

		if(count($alluserdata)) {

			if($alluserdata->password == $old_password){

				$array = array(

					"password" => $new_password

				);

				$this->db->where(array("username" => $username, "password" => $old_password));

				$this->db->update($table, $array);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				return TRUE;

			}

		} else {

			return FALSE;

		}

	}

	public function signout() {

		$this->session->sess_destroy();

	}

	public function loggedin_exam() {

		return (bool) $this->session->userdata("loggedin_exam");

	}

	public function loggedin() {

		return (bool) $this->session->userdata("loggedin");

	}
	public function verifymailbyadminid($uri)
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('adminID',$uri);
		$this->db->where('systemadminactive',0);
		$admin= $this->db->get()->row();
        if($admin)
        {
        	$this->db->update('admin',array('systemadminactive' => 1 ));
        	return $value=true;
        }
        else
        {
        	return $value=false;
        }
		
	}

}

/* End of file signin_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/signin_m.php */