<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timetable extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_info_m");

		$this->load->model("parentes_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("teacher_m");

		$this->load->model("department_m");

		$this->load->model("student_m");

		$this->load->model("Sub_course_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('timetable', $language);

	}

	public function index() {
		// print_r($this->session->userdata('sessionyear'));
		// exit();
		$this->data['panel_title'] = 'Time Table';
		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		if($this->input->get('id')){
			$dep_id = $_GET['id'];
		}else{
			$dep_id = 'no';
		}
	

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Professor") {

			
	     	$this->data['all_count']  =  $this->subject_m->all_count($adminID);
	     	$this->data['department'] = $this->department_m->fetch_departments('department',$adminID);

	     	$this->data['mainCourse'] = $this->classes_m->get_classes_byAdmin($adminID);


	     	if ($this->input->post('departmentID')) {
				$this->session->unset_userdata('FilterCoursewithTimetable');
				$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
				$this->session->set_userdata('FilterDepartmentTimetable', $this->input->post('departmentID'));
			}

			if ($this->input->post('classesID')) {
				$this->session->set_userdata('FilterCoursewithTimetable', $this->input->post('classesID'));
			}

			if ($this->input->post('sub_courses')) {
				$this->session->set_userdata('Filtersub_courseswithTimetable', $this->input->post('sub_courses'));
			}


			if ($this->input->post('yearSemesterID')) {

				$this->session->set_userdata('FilterSubjectyearSemesterwithtimetable', $this->input->post('yearSemesterID'));

			}

			
				$this->data['classesData'] = $this->classes_m->get_single_department($this->session->userdata('FilterDepartmentTimetable'));
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterCoursewithTimetable'));
	     		$this->db->where('departmentID',$this->session->userdata('FilterDepartmentTimetable'));
	     		if ($this->session->userdata('FilterCoursewithTimetable')) {
					 $this->db->where('classesID', $this->session->userdata('FilterCoursewithTimetable'));
				}
				// echo $this->session->userdata('FilterSubjectyearSemesterwithtimetable');
				// exit();
				// $this->db->group_by('classesID');
	     	$value = $this->db->get('classes')->row();


	     $timeTableCourse = array();
	     		if($value){
     			if ($value->mode==2) {
     				$loop = $value->duration*2;
     			}else{
     				$loop = $value->duration;	
     			}

	     		for ($i=1; $i <= $loop; $i++) { 
	     		$timeTableCourse[$i]['classes'] = $value->classes;
	     		$timeTableCourse[$i]['classesID'] = $value->classesID;
	     		$timeTableCourse[$i]['IsSubCourse'] = $value->IsSubCourse;

	     		if($value->mode==2){
	     		$yearsOrSemester =  CallSemester($i);	
	     		} else{
	     		$yearsOrSemester =  CallYears($i);
	     		} 
	     		if($value->IsSubCourse==1){
	     			// if($this->session->userdata('Filtersub_courseswithTimetable'))
	     			// {
	     				// print_r($this->session->userdata('Filtersub_courseswithTimetable'));die;
	     				$sucourse	= $this->Sub_course_m->get_SubCourses_forID($this->session->userdata('Filtersub_courseswithTimetable'));

			     	    $timeTableCourse[$i]['sub_course'] = $sucourse->sub_course;
			     		$timeTableCourse[$i]['sub_coursesID'] = $sucourse->sub_coursesID;

		     			$sub_courseID  =  $sucourse->sub_coursesID;
	     			// }
	     			// else
	     			// {
	     			// 	$sub_courseID =0;
	     			// }
	     		

	     		}else{
	     			$sub_courseID  =  0;
	     		}

	     		 $this->db->where('classesID',$value->classesID);
	     		 $this->db->where('sub_coursesID',$sub_courseID);
	     		 $this->db->where('yearsOrSemester',$yearsOrSemester);
	     		 $this->db->group_by('yearsOrSemester');
	     		$timeTableCourse[$i]['subjects']  = $this->db->get('subject')->result();
	     		$timeTableCourse[$i]['yearsOrSemester'] = $yearsOrSemester;	
	     		$timeTableCourse[$i]['classesID'] = $value->classesID;	

}

	     		}
	     	

	   		$this->data['timetable'] = 	$timeTableCourse;
// print_r($this->data['timetable']);die;

            $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
			$this->data['timing'] = $this->teacher_m->get_lectureTiming($adminID);
			$this->data['lecture'] = $this->subject_m->assign_lecture($adminID);
			// print_r($this->data['lecture']);
			// exit();
			$this->data["subview"] = "timetable/index3";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Student" && $usertype != "Accountant") {
            
			$student = $this->student_info_m->get_student_info();

			$this->data['subjects'] = $this->subject_m->get_subject_proffessor_join($student->yearsOrSemester,$student->classesID,$student->sub_coursesID);
			$this->data['student_subject'] = $this->subject_m->get_subject_proffessor_join_optionalsubjects($student->yearsOrSemester,$student->classesID,$student->studentID,$student->sub_coursesID);
			// print_r($this->data['subjects']);
			// exit();
			
			$this->data["subview"] = "timetable/student_timetable";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			$id = htmlentities(($this->uri->segment(3)));

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					$classesID = $checkstudent->classesID;

					$this->data['set'] = $id;

					$this->data['subjects'] = $this->subject_m->get_join_subject($classesID);

					$this->data["subview"] = "subject/index_parent";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "subject/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function add() {
		$this->data['panel_title'] = 'Add Lecture Timing';
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") 
		{
			if($_POST) {
				$data = array(
								'adminID' => $adminID,
								'start_time' => $this->input->post('start_time'),
								'end_time' => $this->input->post('end_time'),
								'create_usertype' => $usertype,
								'status' => 1,
								'create_date' => date('Y-m-d')
							);
				$insert = $this->db->insert('lectureTiming',$data);
				if($insert){
					$this->session->set_flashdata('success',$this->lang->line('menu_success'));
					redirect(base_url("timetable/add/"));
				}else{
					$this->session->set_flashdata('success',$this->lang->line('menu_success'));
					redirect(base_url("timetable/add/"));
				}
			}
			$this->data['timing'] = $this->teacher_m->get_lectureTiming($adminID);
			$this->data["subview"] = "timetable/addTime";
			$this->load->view('_layout_main', $this->data);
		}
		else
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function deleteTiming() {
		$timeID = $this->input->get('q');
		if(isset($timeID)){
			$this->db->where('timeID',$timeID);
			$sql = $this->db->delete('lectureTiming');
			if($sql){
				$this->session->set_flashdata('success',$this->lang->line('menu_success'));
				redirect(base_url("timetable/add/"));
			}else{
				$this->session->set_flashdata('success',$this->lang->line('menu_success'));
				redirect(base_url("timetable/add/"));
			}
		}else{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}


function timeTableChange(){
$subjectID  = $this->input->post('subjectID');
$startTime  = $this->input->post('startTime');
$endTime  = $this->input->post('endTime');
$classesID  = $this->input->post('classesID');
$yearsOrSemester  = $this->input->post('yearsOrSemester');

if ($subjectID==0) {

$data = array(
'startTime'=>0,
'endTime'=>0
);
	  		     $this->db->where('classesID',$classesID);
	     		 $this->db->where('sub_coursesID',0);
	     		 $this->db->where('yearsOrSemester',$yearsOrSemester);
	     		 $this->db->where('startTime',$startTime);
	             $this->db->update('subject',$data);

	             $arrayMsg['professor_name'] = 'Break';
	             $arrayMsg['error'] = 'change';
}else{

$subjectRow   = $this->db->where('subjectID',$subjectID)->get('subject')->row();

$data = array(
'startTime'=>$startTime,
'endTime'=>$endTime,
);
$this->db->where('subjectID',$subjectID);
$this->db->update('subject',$data);
$this->db->select('name');

$arrayMsg['professor_name']   = $this->db->where('professorID',$subjectRow->professorID)->get('professor')->row()->name;

if($subjectRow->startTime){
 $arrayMsg['error'] = "error";
}else{
  $arrayMsg['error'] = "change";
}

}
echo json_encode($arrayMsg);





}

	function ResetDepartmentwithtimetable(){
		$this->session->unset_userdata('FilterDepartmentTimetable');
		$this->session->unset_userdata('FilterCoursewithTimetable');
		$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
	}

	function ResetCourseswithtimetable(){
		$this->session->unset_userdata('FilterCoursewithTimetable');
		$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
	}

	function ResetSemesterYearwithtimetable(){
		$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
	}

	function course()
	{
		$deptid = $this->input->post('id');
		$this->session->set_userdata('FilterDepartmentTimetable', $deptid);
		echo '<select name="classesID" id="course" class="form-control">
				<option value="">Select Course</option>';
				$this->db->select('classesID,classes');
				$this->db->from('classes');
				$this->db->where('departmentID',$deptid);
				$this->db->where('status',1);
				$query = $this->db->get();
				$result = $query->result_array();
				foreach($result as $row)
				{
					echo '<option value="'.$row['classesID'].'">'.$row['classes'].'</option>';
				}
		echo '</select>';
	}

	function semester()
	{
		$id = $this->input->post('courseID');
		$subCourse=$this->input->post('subCourse');
		if($subCourse)
		{
			$this->session->set_userdata('Filtersub_courseswithTimetable', $subCourse);
		}
		$classesRow = $this->classes_m->get_single_classes($id,$subCourse);
		
		echo '<select name="course_semester" id="course_semester" class="form-control">
				<option value="">Select Semester</option>';
					if ($classesRow) 
					{
						$looping = (int) $classesRow->duration;
						if ($classesRow->mode==1) 
						{
							for ($i=1; $i <=$looping; $i++) {
							if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterwithtimetable')) 
							{
								$select = 'Selected';
							}
							else
							{
								$select = '';
							}
							echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
							}
						}
						else
						{
							for ($i=1; $i <=(2*$looping); $i++) 
							{
								if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterwithtimetable')) 
								{
									$select = 'Selected';
								}
								else
								{
									$select = '';
								}
								echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
							}
						}
					}
		echo '</select>';
	}


	function sub_courses()
	{
		$adminID = $this->session->userdata("adminID");
		$id = $this->input->post('courseID');
	
		$classesRow = $this->sub_courses->get_sub_courses_by_session($adminID,$id);

		if($classesRow){
		echo '<select name="sub_courses" id="sub_courses" class="form-control">
				<option value="">Select Sub Course</option>';
				foreach ($classesRow as $key => $value) {
			    echo "<option  value = '".$value->sub_coursesID."'>".$value->sub_course."</option>";
			    }
		echo '</select>';
	}else{
		echo "0";
	}
	}


	
	function semester_wise_course()
	{
		$adminID = $this->session->userdata("adminID");
	    if ($this->input->post('courseID')) {
			$this->session->set_userdata('FilterCoursewithTimetable', $this->input->post('courseID'));
		}
		
	    if ($this->session->userdata('FilterCoursewithTimetable')) {
			 $this->db->where('classesID', $this->session->userdata('FilterCoursewithTimetable'));
		}
		
		$getDepartmentdata = $this->db->get('classes')->result();

        $timeTableCourse = array();
        foreach ($getDepartmentdata as $key => $value) 
        {
        
            if ($value->mode==2) 
            {
                $loop = $value->duration*2;
            }
            else
            {
                $loop = $value->duration;	
            }
            
            for ($i=1; $i <= $loop; $i++) 
            { 
                $timeTableCourse[$key][$i]['classes'] = $value->classes;
                $timeTableCourse[$key][$i]['classesID'] = $value->classesID;
            
                if($value->mode==2)
                {
                    $yearsOrSemester =  CallSemester($i);	
                } 
                else
                {
                    $yearsOrSemester =  CallYears($i);
                } 
                $this->db->where('classesID',$value->classesID);
                $this->db->where('sub_coursesID',0);
                $this->db->where('yearsOrSemester',$yearsOrSemester);
                $timeTableCourse[$key][$i]['subjects']  = $this->db->get('subject')->result();
                $timeTableCourse[$key][$i]['yearsOrSemester'] = $yearsOrSemester;
            }
        }
        $this->data['timing'] = $this->teacher_m->get_lectureTiming($adminID);
        $this->data['timetable'] = 	$timeTableCourse;

        $this->load->view('timetable/semester', $this->data);
	}

	function subjects()
	{
		$yearsOrSemester = $this->input->post('yearsOrSemester');
		$classId = $this->input->post('course');
		if(empty($this->input->post('subCourse'))){
			$subCourse = 0;
		}else{
		$subCourse = $this->input->post('subCourse');
		}
		echo '<select name="subjects" id="subjects" class="form-control">
				<option value="">Select Subject</option>';
				$this->db->select('subjectID,subject,professorID');
				$this->db->from('subject');
				$where = "yearsOrSemester = '".$yearsOrSemester."' and classesID = '".$classId."'";
				$this->db->where($where);
				$this->db->where('sub_coursesID',$subCourse);
				$query = $this->db->get();
				$result = $query->result_array();
				foreach($result as $row)
				{
					echo '<option value="'.$row['subjectID'].'" data-id="'.$row['professorID'].'">'.$row['subject'].'</option>';
				}
		echo '</select>';
	}

	function professors()
	{
		$subjectID = $this->input->post('subjectID');
		$professorID = $this->input->post('professorID');
		
		echo '<select name="professors" id="professors" class="form-control">';
				$this->db->select('professorID,name');
				$this->db->from('professor');
				$where = "professorID = '".$professorID."'";
				$this->db->where($where);
				$query = $this->db->get();
				$result = $query->result_array();
				foreach($result as $row)
				{
					echo '<option value="'.$row['professorID'].'">'.$row['name'].'</option>';
				}
		echo '</select>';
	}

	function professors_lecture_data()
	{


		$currentmonth=date('m');
		$admin_id = $this->session->userdata('adminID');
		$department_id = $this->input->post('department_id');
		$course_id = $this->input->post('course_id');
		$sub_courses = $this->input->post('sub_courses');
		$semester = $this->input->post('semester');
		$subject_id = $this->input->post('subject_id');
		$professor_id = $this->input->post('professor_id');
		$days = $this->input->post('days');
		$times = $this->input->post('times');
		$year=date('Y');
		$created_date = date('d-m-Y');
		
		if($department_id=='' || $course_id=='' || $semester=='' || $subject_id=='' || $professor_id=='' || $days=='' || $times=='')
		{
			echo '<p style="color:red;margin-left: 17px;">All fields are mandatory!</p>';
		}
		else
		{
			if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year" || $semester=="4th_Year")
			{
			    $data = array(
					'admin_id'=>$admin_id,
	   				'department_id'=>$department_id,
					'course_id'=>$course_id,
					'semester'=>$semester,
					'subject_id'=>$subject_id,
					// "sub_coursesID"=>0,
					'professor_id'=>$professor_id,
					'days'=>$days,
					'times'=>$times,
					'year'=>$year,
					'year_mode'=>3,
					'created_date'=>$created_date
				);
			}
			else
			{
				$data = array(
					'admin_id'=>$admin_id,
					'department_id'=>$department_id,
					'course_id'=>$course_id,
					'semester'=>$semester,
					'subject_id'=>$subject_id,
					// "sub_coursesID"=>0,
					'professor_id'=>$professor_id,
					'days'=>$days,
					'times'=>$times,

					'year'=>$year,
					'year_mode'=>2,
					'created_date'=>$created_date
				);
			}		
			// print_r($data);
			// exit();				
			$sql = $this->db->insert('professor_lecture',$data);
			if($sql==true)
			{
				echo '<p style="color:green;margin-left: 17px;">Lecture Assign!</p>';
			}
			else
			{
				echo '<p style="color:red;margin-left: 17px;">Error!</p>';
			}
		}
	}
	
	function edit_professor_classes($id)
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$this->data['professor'] = $this->subject_m->edit_professor_lecture($id);
		$this->data['department'] = $this->department_m->fetch_departments('department',$adminID);
		$this->data["subview"] = "timetable/edit_professor_classes";
		$this->load->view('_layout_main2', $this->data);
	}
	
	function professors_lecture_data_edit()
	{
		$adminID=$this->session->userdata('adminID');
		$currentmonth=date('m');
		$id = $this->input->post('ids');
		$admin_id=$this->input->post('adminID');
		if($admin_id)
		{
			$admin_id=$admin_id;
		}
		else
		{
			$admin_id=$adminID;
		}
		$department_id = $this->input->post('department_id');
		$course_id = $this->input->post('course_id');
		$sub_courses = $this->input->post('sub_courses');
		$semester = $this->input->post('semester');
		$subject_id = $this->input->post('subject_id');
		$professor_id = $this->input->post('professor_id');
		$days = $this->input->post('days');
		$times = $this->input->post('times');
		$year=date('Y');
		$created_date = date('d-m-Y');
		if($currentmonth <= 6)
		{
			if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year")
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								"sub_coursesID"=>$sub_courses,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year-1,
								'year_mode'=>3,
								'created_date'=>$created_date
							);
				}
				else
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								"sub_coursesID"=>$sub_courses,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>2,
								'created_date'=>$created_date
							);
				}
		}
		else if($currentmonth >= 7)
		{
		    if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year")
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								"sub_coursesID"=>$sub_courses,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>3,
								'created_date'=>$created_date
							);	
				}
				else
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								"sub_coursesID"=>$sub_courses,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>1,
								'created_date'=>$created_date
							);	
				}
		}
		else
		{
			echo '<p style="color:red;margin-left: 17px;">Error!</p>';
		}
		
		$this->db->where('id',$id);
		// print_r($data);die;
		$sql = $this->db->update('professor_lecture',$data);
		if($sql==true)
		{
			echo '<p style="color:green;margin-left: 17px;">Update Success!</p>';
		}
		else
		{
			echo '<p style="color:red;margin-left: 17px;">Error!</p>';
		} 
	}
	
	function delete_professor_classes($id)
	{
		$this->db->where('id',$id);
		$sql = $this->db->delete('professor_lecture');
		if($sql==true)
		{
			redirect(base_url().'timetable/index');
		}
	}
}

