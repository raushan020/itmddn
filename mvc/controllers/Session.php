<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Session extends Admin_Controller {



	function __construct() {

		parent::__construct();

		$this->load->model("session_m");
	    $this->load->model("education_details_m");
		$language = $this->session->userdata('lang');

		$this->lang->load('session', $language);	

	}



	public function index() {

		$usertype = $this->session->userdata("usertype");

 		if($usertype == "Admin") {

			// $this->data['session'] = $this->session_m->get_classes_byAdmin();

  		     $query  =	$this->db->get('session');
  		     $this->data['std_session']  = $query->result();
  		      //model me lagana hai

			$this->data["subview"] = "session/index";

			$this->load->view('_layout_main', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	protected function rules() {

		$rules = array(

			array(

				'field' => 'session', 

				'label' => $this->lang->line("session_name"), 

				'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_session'

			), 


			array(

				'field' => 'education_details', 

				'label' => 'education details', 

				'rules' => 'required|xss_clean'

			), 

			array(

				'field' => 'note', 

				'label' => $this->lang->line("session_note"), 

				'rules' => 'trim|max_length[200]|xss_clean'

			)

		);

		return $rules;

	}

	public function add() {

		$usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
		if($usertype == "Admin") {
			$this->data['education_details'] = $this->education_details_m->get_eduction_details($loginuserID);
			$this->data['teachers'] = $this->session_m->get_teacher();

			if($_POST) {

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) { 

					$this->data["subview"] = "session/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
				    $education_details = implode(' , ' , $this->input->post('education_details'));
					$array = array(
						"adminID" =>$this->session->userdata("loginuserID"),
						"education_detailsID" =>$education_details,

						"session" => $this->input->post("session"),

						"session_numeric" => $this->input->post("session_numeric"),

						"teacherID" => $this->input->post("teacherID"),

						"note" => $this->input->post("note"),

						"create_date" => date("Y-m-d h:i:s"),

						"modify_date" => date("Y-m-d h:i:s"),

						"create_userID" => $this->session->userdata('loginuserID'),

						"create_username" => $this->session->userdata('username'),

						"create_usertype" => $this->session->userdata('usertype')

					);

					$this->session_m->insert_session($array);

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					redirect(base_url("session/index"));

				}

			} else {

				$this->data["subview"] = "session/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function edit() {

		
		$loginuserID = $this->session->userdata("loginuserID");

		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin") {

			$id = htmlentities(($this->uri->segment(3)));

			if((int)$id) {
				$this->data['teachers'] = $this->session_m->get_teacher();
				$this->data['education_details'] = $this->education_details_m->get_eduction_details($loginuserID);
				$this->data['session'] = $this->session_m->get_session($id);

				if($this->data['session']) {

					if($_POST) {

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) {

							$this->data["subview"] = "session/edit";

							$this->load->view('_layout_main', $this->data);			

						} else {
							$education_details = implode(' , ' , $this->input->post('education_details'));
							$array = array(
								"education_detailsID" =>$education_details,
								"session" => $this->input->post("session"),

								"session_numeric" => $this->input->post("session_numeric"),

								"teacherID" => $this->input->post("teacherID"),

								"note" => $this->input->post("note"),

								"modify_date" => date("Y-m-d h:i:s")

							);


							$this->session_m->update_session($array, $id);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("session/index"));

						}

					} else {

						$this->data["subview"] = "session/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin") {

			$id = htmlentities(($this->uri->segment(3)));

			if((int)$id) {

				$this->session_m->delete_session($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("session/index"));

			} else {

				redirect(base_url("session/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function unique_session() {

		$id = htmlentities(($this->uri->segment(3)));

		if((int)$id) {

			$session = $this->session_m->get_order_by_session(array("session" => $this->input->post("session"), "sessionID !=" => $id));

			if(count($session)) {

				$this->form_validation->set_message("unique_session", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$session = $this->session_m->get_order_by_session(array("session" => $this->input->post("session")));



			if(count($session)) {

				$this->form_validation->set_message("unique_session", "%s already exists");

				return FALSE;

			}

			return TRUE;

		}	

	}



	public function unique_session_numeric() {

		$id = htmlentities(($this->uri->segment(3)));

		if((int)$id) {

			$session_numeric = $this->session_m->get_order_by_session(array("session_numeric" => $this->input->post("session_numeric"), "sessionID !=" => $id));

			if(count($session_numeric)) {

				$this->form_validation->set_message("unique_session_numeric", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$session_numeric = $this->session_m->get_order_by_session(array("session_numeric" => $this->input->post("session_numeric")));



			if(count($session_numeric)) {

				$this->form_validation->set_message("unique_session_numeric", "%s already exists");

				return FALSE;

			}

			return TRUE;

		}	

	}



	function allteacher() {

		if($this->input->post('teacherID') == 0) {

			$this->form_validation->set_message("allteacher", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

function EditSession(){

	$data = array(
     "session_name"=>$this->input->post('session_name'),
		);
	$id =  (int) $this->input->post('sessionID');
	$this->db->where('sessionID',$id);
     $query = $this->db->update('session',$data);
   
     exit();

}
	function valid_number() {

		if($this->input->post('session_numeric') < 0) {

			$this->form_validation->set_message("valid_number", "%s is invalid number");

			return FALSE;

		}

		return TRUE;

	}

}



