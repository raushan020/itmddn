<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Syllabus extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_info_m");

		$this->load->model("parentes_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("teacher_m");

		$this->load->model("student_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('subject', $language);

	}

		protected function rules() {



		$rules = array(



				array(



					'field' => 'classesID', 



					'label' => $this->lang->line("subject_class_name"), 



					'rules' => 'trim|numeric|required|max_length[11]|callback_allclasses'



				),



				array(



					'field' => 'teacherID', 



					'label' => $this->lang->line("subject_teacher_name"), 



					'rules' => 'trim|required|max_length[60]|callback_allteacher'



				),



				array(



					'field' => 'subject', 



					'label' => $this->lang->line("subject_name"), 



					'rules' => 'trim|required|max_length[60]|callback_unique_subject'



				), 



				array(



					'field' => 'subject_author', 



					'label' => $this->lang->line("subject_author"), 



					'rules' => 'trim|max_length[100]'



				), 



				array(



					'field' => 'subject_code', 

					'label' => $this->lang->line("subject_code"),

					'rules' => 'trim|required|max_length[20]|callback_unique_subject_code'



				),



			);



		return $rules;



	}


	public function index()
	{

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata('adminID');
		$uri = $this->uri->segment(3);
		//print_r($uri);
		//exit();

		}

		
public function  UploadFile($id)
{
	if (isset($_FILES['files']) && !empty($_FILES['files'])) 
	{
		$filename = $_FILES["files"]["name"][0];
		$rename =  'file_no-'.time().$filename;
		move_uploaded_file($_FILES["files"]["tmp_name"][0], 'uploads/syllabus/' . $rename);
		$data =  array(
			'pdf'=>$rename,
			'subjectID'=>$id,
			'unique_id'=>time()
		);
		$this->db->insert('sylabus_pdf',$data);
		$insert_id = $this->db->insert_id();
		//echo 'File successfully uploaded : uploads/syllabus/' . $rename . ' ';
		$this->commondata($id);
	} 
	else 
	{ 
		echo 'Please choose at least one file';
	}
}


  function delete_pdf_sylabus($id){
  	//$this->db->where('id',$id);
//$pdf= $this->db->get('sylabus_pdf')->row()->pdf;
 //unlink(FCPATH.'uploads/syllabus/'.$pdf);
$this->db->where('id',$id);
$this->db->delete('sylabus_pdf');
$subjectID =$this->input->post('subjectID');
$this->commondata($subjectID);

  }

public function  UploadFile_notes($id){
if (isset($_FILES['files']) && !empty($_FILES['files'])) {
    $no_files = count($_FILES["files"]['name']);
    for ($i = 0; $i < $no_files; $i++) {
        if ($_FILES["files"]["error"][$i] > 0) {
            echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
        } else {
$rename =  'file_no-'.time().$_FILES["files"]["name"][$i];       
                move_uploaded_file($_FILES["files"]["tmp_name"][$i], 'uploads/syllabus/' . $rename);
                $data =  array(
                	'pdf'=>$rename,
                	'subjectID'=>$id,
                	'unique_id'=>time()
                );
                $this->db->insert('notes_pdf',$data);
                $insert_id = $this->db->insert_id();
                // echo 'File successfully uploaded : uploads/syllabus/' . $rename . ' ';
        }

    }
      $this->commondata_notes($id);
} else {
    echo 'Please choose at least one file';
}
    
/* 
 * End of script
 */
	
}

  function delete_pdf_notes($id){
  	//$this->db->where('id',$id);
//$pdf= $this->db->get('notes_pdf')->row()->pdf;
 //unlink(FCPATH.'uploads/syllabus/'.$pdf);
$this->db->where('id',$id);
$this->db->delete('notes_pdf');
$subjectID =$this->input->post('subjectID');
$this->commondata_notes($subjectID);

  }

public function commondata($id){
$dataPf = $this->db->where('subjectID',$id)->get('sylabus_pdf')->result();
foreach ($dataPf as $key => $value) {
            echo '<button class="btn btn-danger" type="button" onclick="delete_pdf_sylabus('.$value->id.')" >'.$value->pdf.'
                <i class="glyphicon glyphicon-remove"></i></button><span style="    background: #000;
    padding: 5px;
    color: #fff;
    border-radius: 11px;"><a target="_blank" href="'.base_url().'uploads/syllabus/'.$value->pdf.'">View</a></span>';

}
}



public function commondata_notes($id){
$dataPf = $this->db->where('subjectID',$id)->get('notes_pdf')->result();
foreach ($dataPf as $key => $value) {
            echo '<button class="btn btn-danger" type="button" onclick="delete_pdf_note('.$value->id.')" >'.$value->pdf.'
                <i class="glyphicon glyphicon-remove"></i></button><span style="    background: #000;
    padding: 5px;
    color: #fff;
    border-radius: 11px;"><a target="_blank" href="'.base_url().'uploads/syllabus/'.$value->pdf.'">View</a></span>';

}
}

	public function add_syllabus() 
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata('adminID');
		$uri = $this->uri->segment(3);
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Professor") 
		{
			$units = $this->subject_m->GetSubjectBySyllabus(array('subjectID' => $uri));
			foreach ($units as $key => $value) 
			{
				$units[$key]['topic'] = $this->db->where('unitID',$value['unitID'])->get('unit_topic')->result_array();
			}
			$this->data['subjects'] = $units;
			if ($uri) 
			{
				$this->data['subject'] = $this->db->select('subject')->where('subjectID',$uri)->get('subject')->row();
				$this->data['pdf_slybus'] = $this->db->where('subjectID',$uri)->get('sylabus_pdf')->result();
				$this->data['pdf_slybus_notes'] = $this->db->where('subjectID',$uri)->get('notes_pdf')->result();
			}
			if($_POST) 
			{
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == TRUE) { 
					$this->data["subview"] = "syllabus/add";
					$this->load->view('_layout_main', $this->data);			
				} 
				else 
				{
					$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));
					$syllabus_name= $this->input->post('syllabus_name');
					$topic_name = $this->input->post('topic_name');
					$array = array(
						"subjectID" => $uri,
                        'syllabus_name' => $syllabus_name,
						'topic_name' =>$topic_name 
					);					
					$result =$this->subject_m->insert_syllabus($array);
					$this->load->view('syllabus/add');
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("syllabus/index"));
				}
			} 
			else 
			{
				$this->data["subview"] = "syllabus/add";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}



public function view() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata('adminID');

		$uri = $this->uri->segment(3);
		
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Student") {
		

	 $units = $this->subject_m->GetSubjectBySyllabus(array('subjectID' => $uri));
		foreach ($units as $key => $value) {
			
			$units[$key]['topic'] = $this->db->where('unitID',$value['unitID'])->get('unit_topic')->result_array();

		}
		$this->data['subjects'] = $units;

			$this->data["subview"] = "syllabus/view";

			$this->load->view('_layout_main', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



function DeleteTopic(){

$id= $this->input->post('id');

$this->db->where('topicID',$id); 
 $delete= $this->db->delete('unit_topic');
if($delete){
  echo "true";
}else{
  echo "false";
}
}

function unitEdit(){


$colum_name = $this->input->post('colum_name');
$id = $this->input->post('id');
$val = $this->input->post('val');
$data = array(
$colum_name => $val
);
$this->db->where('unitID',$id); 
 $update= $this->db->update('units',$data);
if($update){
  echo "true";
}else{
  echo "false";
}
}

function topicEdit(){


$colum_name = $this->input->post('colum_name');
$id = $this->input->post('id');
$val = $this->input->post('val');
$data = array(
$colum_name => $val
);
$this->db->where('topicID',$id); 
 $update= $this->db->update('unit_topic',$data);
if($update){
  echo "true";
}else{
  echo "false";
}
}




function DeleteUnit(){

$id= $this->input->post('id');
$this->db->where('unitID',$id); 
 $delete= $this->db->delete('units');
if($delete){
  echo "true";
}else{
  echo "false";
}
}



}