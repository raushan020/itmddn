<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Customemail extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("notice_m");
		$this->load->model("subject_m");
		$this->load->model("alert_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$this->load->model("classes_m");
		$this->load->model("student_info_m");
		$this->load->model("professor_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('customemail', $language);	

	}



	public function index() {

		date_default_timezone_set('Asia/Kolkata');
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$year = date("Y");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin' || $usertype == 'Professor' || $usertype == 'Accountant') 
		{
		    $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		    $this->data['Professor'] = $this->professor_m->get_professor_bycustommail();
		    $this->data['superadmin'] = $this->professor_m->superadminid();
		    $this->data['admin'] = $this->professor_m->adminid();
		    // print_r($this->data['admin']);
		    // exit();
		    $this->data['Accountant'] = $this->professor_m->accountantid();
			if($_POST) 
			{
		        $rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) 
				{
					$this->data['form_validation'] = validation_errors();
					$this->data["subview"] = "customemail/index";
					$this->load->view('_layout_main', $this->data);			
				} 
				else 
				{

					
					$dataEamil['title'] = $this->input->post("title");
					$dataEamil['date'] = date("d-m-Y");
					$dataEamil['description'] = strip_tags(html_entity_decode($this->input->post("notice")));
					$subject="New notice from ITM software";
		            $dataEamil['superadminemail']="";
		            $dataEamil['adminemail']="";
		            $dataEamil['institute']=$this->data["siteinfos"]->sname;
					$sendmail="";
					$super=$this->input->post('super');
					$professor=$this->input->post('professor');
					$professoridCheck=$this->input->post('professorid');
					$admin=$this->input->post('admin');					
					$accountant=$this->input->post('act');
					$from=$this->session->userdata('email');  
					// $getemailidsender=$this->professor_m->getemailidsender($loginuserID);
					$send_mail="";
					if($super)
					{
	
						$superadmin=$this->input->post('superadmin');
						for($i=0;$i< count($superadmin);$i++)
                    	{
							$getemail=$this->professor_m->getsuperadminid($superadmin[$i]);
							if($getemail)
                    		{
                    			$email=$getemail->email;
								$fullname=$getemail->name;
								$dataEamil['name']=$getemail->name;;
								$html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
								$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);                 			
								if($sendmail==true)
								{
									$arrayName = array('sentform' =>$from,'sentto'=>$getemail->email,'src'=>"emailTemplates/studentaddition/noticeEmailer",'subject'=>$dataEamil['title'],'msg'=>$dataEamil['description'],'sentdate'=>date('Y-m-d h:i:s'),'sentby'=>$loginuserID );
									$savesentemail=$this->professor_m->sentemailsave($arrayName);
									// print_r($savesentemail);die;
								}
                    		}
                    	}
					}
					if($admin)
					{

						$admin=$this->input->post('adminid');
						for($i=0;$i< count($admin);$i++)
                    	{
							$getemail=$this->professor_m->getadminid($admin[$i]);
							// print_r($getemail);
							// exit();
							if($getemail)
                    		{
                    			$email=$getemail->email;
								$fullname=$getemail->name;
								$dataEamil['name']=$getemail->name;;
								$html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
								$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);                 			
								if($sendmail==true)
								{
									$arrayName = array('sentform' =>$from,'sentto'=>$getemail->email,'src'=>"emailTemplates/studentaddition/noticeEmailer",'subject'=>$dataEamil['title'],'msg'=>$dataEamil['description'],'sentdate'=>date('Y-m-d h:i:s'),'sentby'=>$loginuserID );
									$savesentemail=$this->professor_m->sentemailsave($arrayName);
									// print_r($savesentemail);die;
								}

                    		}
                    	}
					}
					if($accountant)
					{
		
						$accountant=$this->input->post('accountant');
						for($i=0;$i< count($accountant);$i++)
                    	{
							$getemail=$this->professor_m->getadminid($accountant[$i]);
							if($getemail)
                    		{
                    			$email=$getemail->email;
								$fullname=$getemail->name;
								$dataEamil['name']=$getemail->name;;
								$html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
								$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);                 			
								if($sendmail==true)
								{
									$arrayName = array('sentform' =>$from,'sentto'=>$getemail->email,'src'=>"emailTemplates/studentaddition/noticeEmailer",'subject'=>$dataEamil['title'],'msg'=>$dataEamil['description'],'sentdate'=>date('Y-m-d h:i:s'),'sentby'=>$loginuserID );
									$savesentemail=$this->professor_m->sentemailsave($arrayName);
									// print_r($savesentemail);die;
								}
                    		}
                    	}
					}
					
					// echo $this->data["siteinfos"]->email." ".$this->data['siteinfos']->sname;die;
                    if($professoridCheck)
                    {

                    	$professorid=$this->input->post('professorid');
       
                    	for($i=0;$i< count($professorid);$i++)
                    	{
                    		$getemail=$this->professor_m->getprofessoremailid($professorid[$i]);
                    		if($getemail)
                    		{
                    			$email=$getemail->email;
								$fullname=$getemail->name;
								$dataEamil['name']=$getemail->name;;
								$html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
								$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);                 			
								if($sendmail==true)
								{
									$arrayName = array('sentform' =>$from,'sentto'=>$getemail->email,'src'=>"emailTemplates/studentaddition/noticeEmailer",'subject'=>$dataEamil['title'],'msg'=>$dataEamil['description'],'sentdate'=>date('Y-m-d h:i:s'),'sentby'=>$loginuserID );
									$savesentemail=$this->professor_m->sentemailsave($arrayName);
									// print_r($savesentemail);die;
								}
                    		}
                    	}
                    }
                    if($sendmail==true)
					{
						// print_r($send_mail);die;
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("customemail/index"));
					}
					else
					{
						$this->session->set_flashdata('error', "Email Is Not Send !!");
						redirect(base_url("customemail/index"));
					}
				}
			} 
			else 
			{
				$this->data["subview"] = "customemail/index";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}

	}



	protected function rules() {

		$rules = array(
				
				array(

					'field' => 'title', 

					'label' => $this->lang->line("notice_title"), 

					'rules' => 'trim|required|max_length[128]'

				), 



			);

		return $rules;

	}
	public function sendmailbyprofessor()
	{
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$usertype = $this->session->userdata("usertype");
		$loginname=$this->session->userdata("name");
		$studentID=$this->input->post('checked_id');
		$stid=implode($studentID);
		$laststid=(explode(",",$stid));
		$date=$this->input->post('date');
		$title=$this->input->post('title');
		$notice=$this->input->post('notice');
		// print_r($writemail);die;
		if($laststid)
	    {

	    	$config['upload_path'] = './uploads/notice';
	        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|xls|xlsx|txt|php|ppt|pptx';
	        $config['max_size'] = 1024 * 8;
	        $config['encrypt_name'] = TRUE;
	 
	        $this->load->library('upload', $config);
	 
	        if (!$this->upload->do_upload('userfile'))
	        {
	            $array_notice = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => date('Y'),
						"date" => date("Y-m-d", strtotime($date)),
						"create_date" => date('Y-m-d H:i:s'),
						"status" => 1,
						'usertype' => $usertype,						
						'userID'=> $loginuserID
					);
	            // print_r($array_notice);
	            // exit();
					$noticeID = $this->notice_m->insert_notice($array_notice);
					$putclassdata=array('noticeID'=>$noticeID,'classesID'=>$_POST['course'],'yearsOrSemester'=>$_POST['yearsOrSemester']);
					$classesdata=$this->notice_m->insertclassdatabynoticeid($putclassdata);
					// print_r($laststid);die;
					for($i=0;$i < count($laststid);$i++){
					$data = array(
            					 	    'studentID' =>$laststid[$i],
            					 	    'noticeID'=>$noticeID,
            					 	    'sendByUSer'=>$this->session->userdata('usertype'),
            					 	    'sendByUSerName'=>$this->session->userdata('name')
        					 	 	);
        					 	 	$this->db->insert('notice_x_student',$data);
        					 	 }
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $file_id =($data['file_name']);
	            if($file_id)
	            {
	                $array_notice = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => date('Y'),
						"date" => date("Y-m-d", strtotime($date)),
						"create_date" => date('Y-m-d H:i:s'),
						"status" => 1,
						'usertype' => $usertype,
						'noticeDocument' => $file_id,
						'userID'=> $loginuserID
					);
					// print_r($array_notice);
	    //         exit();
					$noticeID = $this->notice_m->insert_notice($array_notice);
					$putclassdata=array('noticeID'=>$noticeID,'classesID'=>$_POST['course'],'yearsOrSemester'=>$_POST['yearsOrSemester']);
					$classesdata=$this->notice_m->insertclassdatabynoticeid($putclassdata);
					for($i=0;$i < count($laststid);$i++){
					$data = array(
            					 	    'studentID' =>$laststid[$i],
            					 	    'noticeID'=>$noticeID,
            					 	    'sendByUSer'=>$this->session->userdata('usertype'),
            					 	    'sendByUSerName'=>$this->session->userdata('name')
        					 	 	);
        					 	 	$this->db->insert('notice_x_student',$data);
        					 	 }
	            }
	            else
	            {
	                
	            }
	        }


	    	$dataEamil['title'] = $title;
			$dataEamil['date'] = date("Y-m-d", strtotime($date));
			$dataEamil['description'] = strip_tags(html_entity_decode($notice));
			$subject="New notice received from $loginname";
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']=""; 
            $dataEamil['institute']=$this->data["siteinfos"]->sname;
			
			if($laststid)
			{
				for($i=0;$i < count($laststid);$i++)
				{
					$emailreciverby=$this->professor_m->getreciverstudentid($laststid[$i]);
            					 	    
        					 	 
					// print_r($emailreciverby);die;
					$email=$emailreciverby->email;
					$fullname=$emailreciverby->name;
					$dataEamil['name']=$emailreciverby->name;
					// print_r($email);die;
					$html = $this->load->view('emailTemplates/sendmailbyanyuser', $dataEamil , true);
					// print_r($html);die;
					$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
				}
				$sendmail=1; 
				// print_r($sendmail);die;
				if($sendmail==true || $sendmail==1)
				{
					// print_r($sendmail);die;
					// $a="E-mail Sent Successfuly";
					// echo $a;
					$this->session->set_flashdata('success', "Notice Sent Successfuly");
					redirect(base_url("viewlecture/viewattendance?type=".$_POST['type']."&department=".$_POST['department']."&course=".$_POST['course']."&yearsOrSemester=".$_POST['yearsOrSemester']."&subject=".$_POST['subject']."&year=".$_POST['year']."&submit=".$_POST['submits']));
				}
			}
			else
			{
				$a="E-mail Not Sent Because Student Is Not Active";
				echo $a;
			}
	    }	    
		else
		{
			$a="E-mail Not Sent!!";
			echo $a;
		}
		
		
	}


	// Rahul ============== Start ===============================
	 
	public function sendmailbyprofessorwithstudent()
	{
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$usertype = $this->session->userdata("usertype");
		$loginname=$this->session->userdata("name");
		$studentID=$this->input->post('checked_id');
		
		// $stid=implode($studentID);
		$laststid=(explode(",",$studentID));
		$date=now();
		$title=$this->input->post('subjectname');
		$notice=$this->input->post('writemail');
		// print_r($writemail);die;
		if($laststid)
	    {

	    	$config['upload_path'] = './uploads/notice';
	        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|xls|xlsx|txt|php|ppt|pptx';
	        $config['max_size'] = 1024 * 8;
	        $config['encrypt_name'] = TRUE;
	 
	        $this->load->library('upload', $config);
	 
	        if (!$this->upload->do_upload('userfile'))
	        {
	            $array_notice = array(
						'adminID' => $adminID,
						"title" => $title,
						"notice" => $notice,
						"notice_type" => '',
						"year" => date('Y'),
						"date" => date("Y-m-d", strtotime($date)),
						"create_date" => date('Y-m-d H:i:s'),
						"status" => 1,
						'usertype' => $usertype,						
						'userID'=> $loginuserID
					);
	            // print_r($array_notice);
	            // exit();
					// $noticeID = $this->notice_m->insert_notice($array_notice);
					// $putclassdata=array('noticeID'=>$noticeID,'classesID'=>$_GET['course'],'yearsOrSemester'=>$_GET['yearsOrSemester']);
					// $classesdata=$this->notice_m->insertclassdatabynoticeid($putclassdata);
					// print_r($laststid);die;
					// for($i=0;$i < count($laststid);$i++){
					// $data = array(
     //        					 	    'studentID' =>$laststid[$i],
     //        					 	    'noticeID'=>$noticeID,
     //        					 	    'sendByUSer'=>$this->session->userdata('usertype'),
     //        					 	    'sendByUSerName'=>$this->session->userdata('name')
     //    					 	 	);
     //    					 	 	$this->db->insert('notice_x_student',$data);
     //    					 	 }
	        }
	        else
	        {
	            $data = $this->upload->data();
	            $file_id =($data['file_name']);
	            if($file_id)
	            {
	                $array_notice = array(
						'adminID' => $adminID,
						"title" => $title,
						"notice" => $notice,
						"notice_type" => '',
						"year" => date('Y'),
						"date" => date("Y-m-d", strtotime($date)),
						"create_date" => date('Y-m-d H:i:s'),
						"status" => 1,
						'usertype' => $usertype,
						'noticeDocument' => $file_id,
						'userID'=> $loginuserID
					);
					// print_r($array_notice);
	    //         exit();
					// $noticeID = $this->notice_m->insert_notice($array_notice);
					// $putclassdata=array('noticeID'=>$noticeID,'classesID'=>$_POST['course'],'yearsOrSemester'=>$_POST['yearsOrSemester']);
					// $classesdata=$this->notice_m->insertclassdatabynoticeid($putclassdata);
					for($i=0;$i < count($laststid);$i++){
					$data = array(
            					 	    'studentID' =>$laststid[$i],
            					 	    'noticeID'=>$noticeID,
            					 	    'sendByUSer'=>$this->session->userdata('usertype'),
            					 	    'sendByUSerName'=>$this->session->userdata('name')
        					 	 	);
        					 	 	$this->db->insert('notice_x_student',$data);
        					 	 }
	            }
	            else
	            {
	                
	            }
	        }


	    	$dataEamil['title'] = $title;
			$dataEamil['date'] = date("Y-m-d", strtotime($date));
			$dataEamil['description'] = strip_tags(html_entity_decode($notice));
			$subject="New notice received from $loginname";
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']=""; 
            $dataEamil['institute']=$this->data["siteinfos"]->sname;
			
			if($laststid)
			{
				for($i=0;$i < count($laststid);$i++)
				{
					$emailreciverby=$this->professor_m->getreciverstudentid($laststid[$i]);
            					 	    
        					 	 
					// print_r($emailreciverby);die;
					$email=$emailreciverby->email;
					$fullname=$emailreciverby->name;
					$dataEamil['name']=$emailreciverby->name;
					// print_r($email);die;
					$html = $this->load->view('emailTemplates/sendmailbyanyuser', $dataEamil , true);
					// print_r($html);die;
					$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
				}
				$sendmail=1; 
				// print_r($sendmail);die;
				if($sendmail==true || $sendmail==1)
				{
					// print_r($sendmail);die;
					// $a="E-mail Sent Successfuly";
					// echo $a;
					$this->session->set_flashdata('success', "Notice Sent Successfuly");
					redirect(base_url("viewlecture/viewattendance?type=".$_POST['type']."&department=".$_POST['department']."&course=".$_POST['course']."&yearsOrSemester=".$_POST['yearsOrSemester']."&subject=".$_POST['subject']."&year=".$_POST['year']."&submit=".$_POST['submits']));
				}
			}
			else
			{
				$a="E-mail Not Sent Because Student Is Not Active";
				echo $a;
			}
	    }	    
		else
		{
			$a="E-mail Not Sent!!";
			echo $a;
		}
		
		
	}

	// Rahul===================End ==============================


	public function sent()
	{
		$loginuserID = $this->session->userdata("loginuserID");
		$this->data['sent']=$this->professor_m->getsentemail($loginuserID);
		$this->data["subview"] = "customemail/sent";
		$this->load->view('_layout_main', $this->data);
	}
	function verifyemailsuperadmin()
	{
		$adminID=$this->input->post('adminID');
		if($adminID)
		{
			// $setting = $this->setting_m->get_setting_byAdmin($adminID);
			$getadmin=$this->professor_m->getalluserid($adminID);
			// print_r($getadmin);die;
	    	// $institute=$setting->sname;
	    	if($getadmin)
	    	{
	    		$superadminemail=$getadmin->email;
	    	}
	    	$fullname=$getadmin->name;
			$email=$superadminemail;
			$dataEamil['logo']=$this->data["siteinfos"]->photo;
			$dataEamil['adminID']=$adminID;
			$dataEamil['name']=$fullname;
			$dataEamil['password']="d6162a34495e2a5c0a43598d480a31b916347e85e8bd0d4b33a2be16922d0435cbda249cea34bdb2e869e0ac3d1aa4a2ad4d17390cdcc0be0144f14698e17927";
			$dataEamil['institute']=$this->data["siteinfos"]->sname;
	        $dataEamil['superadminemail']="";
	        $dataEamil['adminemail']="";
			$subject="E-mail Confirmation";
			$html = $this->load->view('emailTemplates/verifyemail', $dataEamil , true);
			$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
			// print_r(expression)
			if($sendmail == true || $sendmail == 1)
			{
				// print_r("expression");die;
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	
public function reminderbystudent()
{
	$usertype=$this->session->userdata('usertype');
	$loginuserID=$this->session->userdata('loginuserID');
	$adminID=$this->session->userdata('adminID');
	if($usertype == "Student")
	{
	    $arr = array();
	    $this->db->select('email');
	    $this->db->from('user');
	    $where = "usertype = 'Admin' and status = 1 and adminID=$adminID ";
	    $this->db->where($where);
	    $sqls = $this->db->get();
	    $results = $sqls->result_array();
	    foreach($results as $rows)
	    {
	        $arr[] = array(
	                        $rows['email']
	                        );
	    }
	    $this->db->select('student.*,classes.classes');        
        $this->db->from('student');
        $this->db->join('classes','classes.classesID=student.classesID','left');
        $this->db->where('studentID',$loginuserID);
        $query2  = $this->db->get()->row();
        $dataEamil['emailData'] = $query2;
		$dataEamil['superadminemail']="";
    	$dataEamil['adminemail']=$arr;
    	$subjectadmin="Student Send Fee Update Reminder";
    	$dataEamil['institute']=$this->data["siteinfos"]->sname;
    	$htmladmin=$this->load->view('emailTemplates/studentaddition/updatereminderbystudent',$dataEamil, true);
    	// $sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
    	$accountant=$this->db->select('email')->where(array('usertype' => 'Accountant','status' => 1, 'adminID' => $adminID))->get('user')->row();
    	// print_r($accountant);die;
    	if($accountant)
    	{
    		$dataEamil['email']=$accountant->email;
    		$subjectaccountant="Student Send Fee Update Reminder";
	    	$htmlaccountant=$this->load->view('emailTemplates/studentaddition/updatereminderbystudent',$dataEamil, true);
	    	$sendmail=emailBySendGridaccountant($subjectaccountant,$htmlaccountant,$dataEamil);
	    	// print_r($sendmail);die;
    	
    	}
    	
        $this->session->set_flashdata('success', $this->lang->line('mail_success'));
        redirect(base_url("invoice/index"));
	    // print_r($arr);die;
	}
	else
	{
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	}
}
public function sendfeedbacktohelp()
{
	$loginuserID=$this->session->userdata('loginuserID');
	$usertype=$this->session->userdata('usertype');
	$getadmin=$this->professor_m->getalluserid($loginuserID);
	if($getadmin)
	{
		$name=$getadmin->name;
	}
	$hidescreenshot=$this->input->post('hidescreenshot');
	$imagename=$this->input->post('imagename');
	$basename=basename($imagename);
	$writecomment=$this->input->post('writecomment');	
	$dataEamil['writecomment']=$writecomment;
	$institute=$this->data["siteinfos"]->sname;
	$subject="New Feedback received from ".$name." (".$usertype.") via ".$institute;

	$htmlaccountant=$this->load->view('emailTemplates/alluserhelpfeedback',$dataEamil, true);
	$help=emailBySendGridsupport($htmlaccountant,$basename,$hidescreenshot,$subject);
	if($help == true || $help == 1)
	{
		unlink("uploads/".$basename);
		$this->session->set_flashdata('success', "Feedback sent successfully");
    	redirect(base_url("dashboard/index"));
	}
	else
	{
		$this->session->set_flashdata('error', "Your Feedback Not Send Successfuly");
    	redirect(base_url("dashboard/index"));
	}
	

}

	

}



