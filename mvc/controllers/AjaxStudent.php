<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AjaxStudent extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("semester_test_m");
		$this->load->model("invoice_gen_m");
		$this->load->model("sub_courses");
		$this->load->model("country_m");
		$this->load->model("year_m");
		$this->load->model("teacher_m");
		$this->load->model("classes_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('student', $language);
	}




function Get_subCourses(){
			if($this->signin_m->loggedin() == FALSE) {
        	header("Refresh:0");
			}

     $id = $this->input->post('id');
     $getSubcourses =  $this->classes_m->get_SubCourses($id);
     if ($getSubcourses) {
							if(form_error('subCourseID')) 
								echo "<div class='form-group has-error' >";
							else     
								echo "<div class='form-group' >";
							echo "<label for='sectionID' class='col-sm-2 control-label' >Course Subcategory<span class='red-color'>*</span></label>
										<div class='col-sm-6'> 
											<select id='sub_CourseID' required  name='sub_coursesID' class='form-control' value='<?php echo set_value('sub_coursesID'); ?>'>
												<option value='' selected='selected' >Select Cource Subcategory</option>";

     foreach ($getSubcourses as $key => $value) {
    echo "<option value =".$value->sub_coursesID.">".$value->sub_course."</option>";
}
echo "</div>
		<span class='col-sm-4 control-label'>".form_error('subCourseID')."</span></div>";
     } 
}


function Get_subCourses_exam(){

			// if($this->signin_m->loggedin() == FALSE) {
   //      	header("Refresh:0");
			// }
     $id = $this->input->post('id');
     $getSubcourses =  $this->classes_m->get_SubCourses($id);
     if ($getSubcourses) {
							if(form_error('subCourseID')) 
								echo "<div class='form-group has-error' >";
							else     
								echo "<div class='form-group' >";
							echo "<label for='sectionID' class='control-label' >Course Subcategory<span class='red-color'>*</span></label>
										<div class=''> 
											<select id='sub_CourseID' required  name='sub_coursesID' class='form-control' value='<?php echo set_value('sub_coursesID'); ?>'>
												<option value='' selected='selected' >Select Cource Subcategory</option>";

     foreach ($getSubcourses as $key => $value) {
    echo "<option value =".$value->sub_coursesID.">".$value->sub_course."</option>";
}
echo "</div>
		<span class='col-sm-4 control-label'>".form_error('subCourseID')."</span></div>";
     } 
}



function validate_qualification(){
	$id = $this->input->post('id');
    $queryObject  = $this->classes_m->get_single_classes($id);
    if ($queryObject) {
    $ArrayForRequired =  explode(',', $queryObject->education_detailsID);
    		 $this->db->where_in('education_detailsID',$ArrayForRequired);
    $query = $this->db->get('education_details');
echo "<h3 class='border_heading'> Academic Details</h3>";
$k = 0;
$j = 0;
	if ($queryObject->isRequired==1) {
    		$required =  "required";
    		$span = '<span class="red-color">*</span>';
    	}else{
    		$required = "";
    		$span = '';
    	}	
    foreach ($query->result() as $key => $value) {
   		echo  "				
		   <div id=''><div class='form-group'>
				<label  class='col-sm-2 control-label'>$value->education $span</label>
					<div class='col-sm-2'>";
		  echo "<select class='form-control' name='year[]' $required id='year' value=''>";
		  echo  "<option value=''>Year</option>";

		  for($i=2025;$i>=1947;$i--){ 
		   echo "<option value='$i'>$i</option>";
		   }
		  echo "</select>";
					echo "</div>
					<div class='col-sm-2'>
						<input type='hidden' name='education_detailsID[]' value='$value->education_detailsID'>
						<input type='hidden' name='education_name[]' value='$value->education'>
						<input class='form-control' name='subject[]' $required  id='subject' placeholder='Subject' value=''>
					</div>
					<div class='col-sm-2'>
						<input class='form-control' name='board[]' $required id='board' placeholder='Board' value=''>
					</div>
					<div class='col-sm-2'>
						<input class='form-control' name='percent[]' $required id='percent' placeholder='%' value=''>
					</div>
					<span class='col-sm-1 control-label'>
					</span>
					   <div class='clearfix'></div>
					   <div class = 'certifixa'>
					   <label class='col-sm-4'>$value->education Certificate $span</label>
					   <div class = 'col-sm-4'>
					   <label>Marksheet</label>
					   <input type='file' name='marksheet".$k++."' title='Marksheet upload' $required>
					   </div>
					   <div class = 'col-sm-4'>
					   <label>Certificate</label>
					   <input type='file' name='certificate".$j++."' title='Certificate upload' $required>
					   </div>
					   <div class = 'clearfix'></div>
					   </div>
			</div></div></div>"; 
		    }
		}
	}

function validate_qualification_edit(){
	$id = $this->input->post('id');
	$studentID = $this->uri->segment(3);
    $queryObject  = $this->classes_m->get_single_classes($id);
    if ($queryObject) {
    $ArrayForRequired =  explode(',', $queryObject->education_detailsID);
    		 $this->db->where_in('education_detailsID',$ArrayForRequired);
    $query = $this->db->get('education_details');
echo "<h3 class='border_heading'> Academic Details</h3>";
$k = 0;
$j = 0;
	if ($queryObject->isRequired==1) {
    		$required =  "required";
    		$span = '<span class="red-color">*</span>';
    	}else{
    		$required = "";
    		$span = '';
    	}	
    foreach ($query->result() as $key => $value) {

    	    $this->db->where('education_detailsID',$value->education_detailsID);
			$this->db->where('studentID',$studentID);
		    $query_update  =	$this->db->get('academic_details');
		    $update_education = $query_update->row();

		    // if (count($update_education)==0) {
		    // $year_passing ='';
		    // $subject = '';
		    // $board_name = '';
		    // $percentage = '';
		    // }else{
		    // $year_passing =$update_education->year_passing;
		    // $subject = $update_education->subject;
		    // $board_name = $update_education->board_name;
		    // $percentage = $update_education->percentage;
		    // }
		    if ($update_education)
		    {
			    $year_passing =$update_education->year_passing;
			    $subject = $update_education->subject;
			    $board_name = $update_education->board_name;
			    $percentage = $update_education->percentage;
		    }
		    else
		    {
			    $year_passing ='';
			    $subject = '';
			    $board_name = '';
			    $percentage = '';
		    }


   		echo  "				
		   <div id=''><div class='form-group'>
				<label  class='col-sm-2 control-label'>$value->education $span</label>
					<div class='col-sm-2'>";
		  echo "<select class='form-control' name='year[]' $required id='year' value=''>";
		  echo  "<option value=''>Year</option>";

		  for($i=2025;$i>=1947;$i--){ 
		   if($i==$year_passing){$selected =  "selected";}else{$selected =  "selected";} 
		   echo "<option value='$i' $selected>$i</option>";
		   }
		  echo "</select>";
					echo "</div>
					<div class='col-sm-2'>
						<input type='hidden' name='education_detailsID[]' value='$value->education_detailsID'>
						<input type='hidden' name='education_name[]' value='$value->education'>
						<input class='form-control' name='subject[]' $required  id='subject' placeholder='Subject' value='$subject'>
					</div>
					<div class='col-sm-2'>
						<input class='form-control' name='board[]' $required id='board' placeholder='Board' value='$board_name'>
					</div>
					<div class='col-sm-2'>
						<input class='form-control' name='percent[]' $required id='percent' placeholder='%' value='$percentage'>
					</div>
					<span class='col-sm-1 control-label'>
					</span>
					   <div class='clearfix'></div>
					   <div class = 'certifixa'>
					   <label class='col-sm-4'>$value->education Certificate $span</label>
					   <div class = 'col-sm-4'>
					   <label>Marksheet</label>
					   <input type='file' name='marksheet".$k++."' title='Marksheet upload' $required>
					   </div>
					   <div class = 'col-sm-4'>
					   <label>Certificate</label>
					   <input type='file' name='certificate".$j++."' title='Certificate upload' $required>
					   </div>
					   <div class = 'clearfix'></div>
					   </div>
			</div></div></div>"; 
		    }
		}
	}


function fetch_mode(){
     $id = $this->input->post('id');
     $classesRow = $this->classes_m->get_single_classes($id);
     if ($classesRow) {

	if ($classesRow->mode==1) {
	echo "<input type ='hidden' value = '1' name = 'education_mode'>";
	}else{
		echo "<input type ='hidden' value = '2' name = 'education_mode'>";
	}
     echo "<div class='form-group'>
		<label for='' class='col-sm-2 control-label'>Semester <span class='red-color'>*</span></label>";
			
	echo  "<div class='col-sm-6' id='yearID2'>";

	 $id = $this->input->post('id');
	     $classesRow = $this->classes_m->get_single_classes($id);;
	    
	if ($classesRow) {
		 $looping    =  (int) $classesRow->duration;
	   if ($classesRow->mode==1) {
	   	 echo"<select class='form-control' name='yearsOrSemester' required onchange='GetEntryTypeData($(this).val())' id='yearIDSelect' value='set_value('yearId');'>";
		  for ($i=1; $i <=$looping; $i++) {
		 
		 echo "<option value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
		 
	   }
	   echo  "</select>";
	}

	else{
		
	     echo"<select class='form-control' name='yearsOrSemester' required onchange='GetEntryTypeData($(this).val())' id='semester_idSelect' value='<?php echo set_value('semesterId'); ?>'>";
	     echo "<option value ='' selected>Select Semester</option>";
		  for ($i=1; $i <=(2*$looping); $i++) {
		 echo "<option value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
	   }
	   echo  "</select>";
	   }
	}
	echo "</div>";

	 }
  
	}


function fetch_SemesterYear(){
     $id = $this->input->post('id');
     $classesRow = $this->classes_m->get_single_classes($id);;
    
		if ($classesRow) {
	 		$looping    =  (int) $classesRow->duration;
		   	if ($classesRow->mode==1) {
			   	echo "<div class='form-group'>";
			   	echo "<label for='quiz_name' class=''>Semester/Year<span class='red-color'>*</span></label>";
			   	echo"<select class='form-control' name='yearsOrSemester' onchange='Getsubjects($(this).val())' id='yearSemesterID' value='set_value('yearId');'>";
			   	echo "<option value ='' selected>Select Semester</option>";
				echo "<option value=''>Year</option>";
				  for ($i=1; $i <=$looping; $i++) {
				 
				echo "<option value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";			 
			   }
			   echo  "</select>";
			   echo "</div>";
			} else {
				echo "<div class='form-group'>";
				echo "<label for='quiz_name' class=''>Semester/Year<span class='red-color'>*</span></label>";
			     echo"<select class='form-control' name='yearsOrSemester' onchange='Getsubjects($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>";
			     echo "<option value ='' selected>Select Semester</option>";
				 echo "<option value=''>Semester</option>";
				  for ($i=1; $i <=(2*$looping); $i++) {
				 echo "<option value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
			   }
			   echo  "</select>";
			   echo "</div>";
			   }
		}

	}





function checkemail(){
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
			 echo "no";
			} else {
			 echo "yes";
			}
		
		}

}


/*	public function unique_roll_ajax() {
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "studentID !=" => $id, "classesID" => $this->input->post('classesID')));
			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$student = $this->student_m->GetRollbyArray(array("roll" => $this->input->post("roll")));
	
			if($student>0) {
			echo "no";
			}else{
			echo "yes";
			}
			
		}
	}*/


	public function lol_username_ajax() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $student_info->email));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
			 echo 'no';
			} else {
			 echo "yes";
			}
		}
	}

}
