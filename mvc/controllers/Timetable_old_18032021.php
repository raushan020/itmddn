<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timetable extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_info_m");

		$this->load->model("parentes_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("teacher_m");

		$this->load->model("department_m");

		$this->load->model("student_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('timetable', $language);

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		if($this->input->get('id')){
			$dep_id = $_GET['id'];
		}else{
			$dep_id = 'no';
		}
	

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {

	     	$this->data['all_count']  =  $this->subject_m->all_count();
	     	$this->data['department'] = $this->department_m->fetch_departments('department');

	     	$this->data['mainCourse'] = $this->classes_m->get_classes_byAdmin($adminID);

	     	if ($this->input->post('departmentID')) {
				$this->session->unset_userdata('FilterCoursewithTimetable');
				$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
				$this->session->set_userdata('FilterDepartmentTimetable', $this->input->post('departmentID'));
			}

			if ($this->input->post('classesID')) {
				$this->session->set_userdata('FilterCoursewithTimetable', $this->input->post('classesID'));
			}

			if ($this->input->post('yearSemesterID')) {

				$this->session->set_userdata('FilterSubjectyearSemesterwithtimetable', $this->input->post('yearSemesterID'));

			}

			
				$this->data['classesData'] = $this->classes_m->get_single_department($this->session->userdata('FilterDepartmentTimetable'));
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterCoursewithTimetable'));
	     		$this->db->where('departmentID',$this->session->userdata('FilterDepartmentTimetable'));
	     		if ($this->session->userdata('FilterCoursewithTimetable')) {
					 $this->db->where('classesID', $this->session->userdata('FilterCoursewithTimetable'));
				}
				// echo $this->session->userdata('FilterSubjectyearSemesterwithtimetable');
				// exit();
				
	     	$getclassesabcd=$this->classes_m->get_classes_bysuperAdmin();
// print_r($getclassesabcd);die;
	     $timeTableCourse = array();
	     	foreach ($this->classes_m->get_classes_bysuperAdmin() as $key => $value) {
	     		
     			if ($value->mode==2) {
     				$loop = $value->duration*2;
     			}else{
     				$loop = $value->duration;	
     			}
     			// print_r("expression");die;
	     		for ($i=1; $i <= $loop; $i++) { 
	     		$timeTableCourse[$key][$i]['classes'] = $value->classes;
	     		$timeTableCourse[$key][$i]['classesID'] = $value->classesID;

	     		if($value->mode==2){
	     		$yearsOrSemester =  CallSemester($i);	
	     		} else{
	     		$yearsOrSemester =  CallYears($i);
	     		} 
	     		
	     		 $this->db->where('classesID',$value->classesID);
	     		 $this->db->where('sub_coursesID',0);
	     		 $this->db->where('yearsOrSemester',$yearsOrSemester);
	     		$timeTableCourse[$key][$i]['subjects']  = $this->db->get('subject')->result();
	     		// print_r($timeTableCourse[$key][$i]['subjects']);die;
	     		$timeTableCourse[$key][$i]['yearsOrSemester'] = $yearsOrSemester;	


	     		}
	     	}

	   		$this->data['timetable'] = 	$timeTableCourse;


            $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
			
			$this->data['lecture'] = $this->subject_m->assign_lecture($adminID);
			$this->data["subview"] = "timetable/index3";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Student") {
            
			$student = $this->student_info_m->get_student_info();

			$this->data['subjects'] = $this->subject_m->get_subject_proffessor_join($student->yearsOrSemester,$student->classesID);
			$this->data['student_subject'] = $this->subject_m->get_subject_proffessor_join_optionalsubjects($student->yearsOrSemester,$student->classesID,$student->studentID);
			
			$this->data["subview"] = "timetable/student_timetable";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {
			$loginuserID = $this->session->userdata("loginuserID");
			$student = $this->student_info_m->get_student_info($loginuserID);
			if($student)
			{
				$this->data['subjects'] = $this->subject_m->get_subject_proffessor_join($student->yearsOrSemester,$student->classesID);
				$this->data['student_subject'] = $this->subject_m->get_subject_proffessor_join_optionalsubjects($student->yearsOrSemester,$student->classesID,$student->studentID);
				
				$this->data["subview"] = "timetable/student_timetable";

				$this->load->view('_layout_main', $this->data);

			}
			else
			{

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

			}

			

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


function timeTableChange(){
$subjectID  = $this->input->post('subjectID');
$startTime  = $this->input->post('startTime');
$endTime  = $this->input->post('endTime');
$classesID  = $this->input->post('classesID');
$yearsOrSemester  = $this->input->post('yearsOrSemester');

if ($subjectID==0) {

$data = array(
'startTime'=>0,
'endTime'=>0
);
	  		     $this->db->where('classesID',$classesID);
	     		 $this->db->where('sub_coursesID',0);
	     		 $this->db->where('yearsOrSemester',$yearsOrSemester);
	     		 $this->db->where('startTime',$startTime);
	             $this->db->update('subject',$data);

	             $arrayMsg['professor_name'] = 'Break';
	             $arrayMsg['error'] = 'change';
}else{

$subjectRow   = $this->db->where('subjectID',$subjectID)->get('subject')->row();

$data = array(
'startTime'=>$startTime,
'endTime'=>$endTime,
);
$this->db->where('subjectID',$subjectID);
$this->db->update('subject',$data);
$this->db->select('name');

$arrayMsg['professor_name']   = $this->db->where('professorID',$subjectRow->professorID)->get('professor')->row()->name;

if($subjectRow->startTime){
 $arrayMsg['error'] = "error";
}else{
  $arrayMsg['error'] = "change";
}

}
echo json_encode($arrayMsg);





}

	function ResetDepartmentwithtimetable(){
		$this->session->unset_userdata('FilterDepartmentTimetable');
		$this->session->unset_userdata('FilterCoursewithTimetable');
		$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
	}

	function ResetCourseswithtimetable(){
		$this->session->unset_userdata('FilterCoursewithTimetable');
		$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
	}

	function ResetSemesterYearwithtimetable(){
		$this->session->unset_userdata('FilterSubjectyearSemesterwithtimetable');
	}

	function course()
	{
		$deptid = $this->input->post('id');
		$this->session->set_userdata('FilterDepartmentTimetable', $deptid);
		echo '<select name="classesID" id="course" class="form-control">
				<option value="">Select Course</option>';
				$this->db->select('classesID,classes');
				$this->db->from('classes');
				$this->db->where('departmentID',$deptid);
				$query = $this->db->get();
				$result = $query->result_array();
				foreach($result as $row)
				{
					echo '<option value="'.$row['classesID'].'">'.$row['classes'].'</option>';
				}
		echo '</select>';
	}

	function semester()
	{
		$id = $this->input->post('courseID');
		$classesRow = $this->classes_m->get_single_classes($id);
		
		echo '<select name="course_semester" id="course_semester" class="form-control">
				<option value="">Select Semester</option>';
					if ($classesRow) 
					{
						$looping = (int) $classesRow->duration;
						if ($classesRow->mode==1) 
						{
							for ($i=1; $i <=$looping; $i++) {
							if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterwithtimetable')) 
							{
								$select = 'Selected';
							}
							else
							{
								$select = '';
							}
							echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
							}
						}
						else
						{
							for ($i=1; $i <=(2*$looping); $i++) 
							{
								if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterwithtimetable')) 
								{
									$select = 'Selected';
								}
								else
								{
									$select = '';
								}
								echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
							}
						}
					}
		echo '</select>';
	}
	
	function semester_wise_course()
	{
	    if ($this->input->post('courseID')) {
			$this->session->set_userdata('FilterCoursewithTimetable', $this->input->post('courseID'));
		}
		
	    if ($this->session->userdata('FilterCoursewithTimetable')) {
			 $this->db->where('classesID', $this->session->userdata('FilterCoursewithTimetable'));
		}
		
		$getDepartmentdata = $this->db->get('classes')->result();

        $timeTableCourse = array();
        foreach ($getDepartmentdata as $key => $value) 
        {
        
            if ($value->mode==2) 
            {
                $loop = $value->duration*2;
            }
            else
            {
                $loop = $value->duration;	
            }
            
            for ($i=1; $i <= $loop; $i++) 
            { 
                $timeTableCourse[$key][$i]['classes'] = $value->classes;
                $timeTableCourse[$key][$i]['classesID'] = $value->classesID;
            
                if($value->mode==2)
                {
                    $yearsOrSemester =  CallSemester($i);	
                } 
                else
                {
                    $yearsOrSemester =  CallYears($i);
                } 
                $this->db->where('classesID',$value->classesID);
                $this->db->where('sub_coursesID',0);
                $this->db->where('yearsOrSemester',$yearsOrSemester);
                $timeTableCourse[$key][$i]['subjects']  = $this->db->get('subject')->result();
                $timeTableCourse[$key][$i]['yearsOrSemester'] = $yearsOrSemester;
            }
        }
        // echo "<pre>";
        // print_r($timeTableCourse);die;
        $this->data['timetable'] = 	$timeTableCourse;
        $this->load->view('timetable/semester', $this->data);
	}

	function subjects()
	{
		$yearsOrSemester = $this->input->post('yearsOrSemester');
		$classId = $this->input->post('course');
		
		echo '<select name="subjects" id="subjects" class="form-control">
				<option value="">Select Subject</option>';
				$this->db->select('subjectID,subject,professorID');
				$this->db->from('subject');
				$where = "yearsOrSemester = '".$yearsOrSemester."' and classesID = '".$classId."'";
				$this->db->where($where);
				$query = $this->db->get();
				$result = $query->result_array();
				foreach($result as $row)
				{
					echo '<option value="'.$row['subjectID'].'" data-id="'.$row['professorID'].'">'.$row['subject'].'</option>';
				}
		echo '</select>';
	}

	function professors()
	{
		$subjectID = $this->input->post('subjectID');
		$professorID = $this->input->post('professorID');
		
		echo '<select name="professors" id="professors" class="form-control">';
				$this->db->select('professorID,name');
				$this->db->from('professor');
				$where = "professorID = '".$professorID."'";
				$this->db->where($where);
				$query = $this->db->get();
				$result = $query->result_array();
				foreach($result as $row)
				{
					echo '<option value="'.$row['professorID'].'">'.$row['name'].'</option>';
				}
		echo '</select>';
	}

	function professors_lecture_data()
	{
		$currentmonth=date('m');
		$admin_id = $this->session->userdata('adminID');
		$department_id = $this->input->post('department_id');
		$course_id = $this->input->post('course_id');
		$semester = $this->input->post('semester');
		$subject_id = $this->input->post('subject_id');
		$professor_id = $this->input->post('professor_id');
		$days = $this->input->post('days');
		$times = $this->input->post('times');
		$year=date('Y');
		$created_date = date('d-m-Y');
		
		if($department_id=='' || $course_id=='' || $semester=='' || $subject_id=='' || $professor_id=='' || $days=='' || $times=='')
		{
			echo '<p style="color:red;margin-left: 17px;">All fields are mandatory!</p>';
		}
		else
		{
			if($currentmonth <= 6)
			{
				if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year")
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year-1,
								'year_mode'=>3,
								'created_date'=>$created_date
							);
				}
				else
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>2,
								'created_date'=>$created_date
							);
				}
					
			}
			else if($currentmonth >= 7)
			{
				if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year")
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>3,
								'created_date'=>$created_date
							);	
				}
				else
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>1,
								'created_date'=>$created_date
							);	
				}
				
			}
			else
			{
				echo '<p style="color:red;margin-left: 17px;">Error!</p>';
			}
						
				$sql = $this->db->insert('professor_lecture',$data);
				if($sql==true)
				{
					echo '<p style="color:green;margin-left: 17px;">Lecture Assign!</p>';
				}
				else
				{
					echo '<p style="color:red;margin-left: 17px;">Error!</p>';
				}
//			}
		}
	}
	
	function edit_professor_classes($id)
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$this->data['professor'] = $this->subject_m->edit_professor_lecture($id);
		$this->data['department'] = $this->department_m->fetch_departments('department');
		$this->data["subview"] = "timetable/edit_professor_classes";
		$this->load->view('_layout_main2', $this->data);
	}
	
	function professors_lecture_data_edit()
	{
		$currentmonth=date('m');
		$id = $this->input->post('ids');
		$admin_id=$this->session->userdata("adminID");
		$department_id = $this->input->post('department_id');
		$course_id = $this->input->post('course_id');
		$semester = $this->input->post('semester');
		$subject_id = $this->input->post('subject_id');
		$professor_id = $this->input->post('professor_id');
		$days = $this->input->post('days');
		$times = $this->input->post('times');
		$year=date('Y');
		$created_date = date('d-m-Y');
		if($currentmonth <= 6)
		{
			if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year")
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year-1,
								'year_mode'=>3,
								'created_date'=>$created_date
							);
				}
				else
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>2,
								'created_date'=>$created_date
							);
				}
		}
		else if($currentmonth >= 7)
		{
		    if($semester=="1st_Year" || $semester=="2nd_Year" || $semester=="3rd_Year")
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>3,
								'created_date'=>$created_date
							);	
				}
				else
				{
					$data = array(
								'admin_id'=>$admin_id,
								'department_id'=>$department_id,
								'course_id'=>$course_id,
								'semester'=>$semester,
								'subject_id'=>$subject_id,
								'professor_id'=>$professor_id,
								'days'=>$days,
								'times'=>$times,
								'year'=>$year,
								'year_mode'=>1,
								'created_date'=>$created_date
							);	
				}
		}
		else
		{
			echo '<p style="color:red;margin-left: 17px;">Error!</p>';
		}
		
		$this->db->where('id',$id);
		$sql = $this->db->update('professor_lecture',$data);
		if($sql==true)
		{
			echo '<p style="color:green;margin-left: 17px;">Update Success!</p>';
		}
		else
		{
			echo '<p style="color:red;margin-left: 17px;">Error!</p>';
		} 
	}
	
	function delete_professor_classes($id)
	{
		$this->db->where('id',$id);
		$sql = $this->db->delete('professor_lecture');
		if($sql==true)
		{
			redirect(base_url().'timetable/index');
		}
	}
}

