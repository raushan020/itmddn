<?php if(!defined('BASEPATH')) exit('No direct script access allowed');







class Live extends Admin_Controller {







	function __construct() {



			parent::__construct();



		$this->load->model("classes_m");



		$this->load->model("student_m");



		$this->load->model("section_m");



			$this->load->model("live_m");



		$language = $this->session->userdata('lang');



		$this->lang->load('balance', $language);	



	}















public function index(){

$this->data['panel_title'] = 'Live Class';

$adminID = $this->session->userdata('adminID');

$streamID = $this->uri->segment(3);

$this->data['broad_info'] =$this->live_m->broad_info($streamID,$adminID);

$this->data['subject']=$this->live_m->broad_info_subject($this->data['broad_info']->subjectID);

$this->load->view("live/index",$this->data);







}



public function audience()

{







	$this->data['panel_title'] = 'Live Class';

	$adminID = $this->session->userdata('adminID');

	$streamID = $this->uri->segment(3);

	$this->data['broad_info'] = $this->live_m->broad_info($streamID,$adminID);

	$this->data['subject']=$this->live_m->broad_info_subject($this->data['broad_info']->subjectID);

	$this->load->view("live/audience",$this->data);

}







public function table(){

$this->data['panel_title'] = 'Live Class';

			    $this->data["subview"] = "live/table";

				$this->load->view('_layout_main', $this->data);



}



public function AjaxTable(){



	$usertype = $this->session->userdata("usertype");

	$adminID = $this->session->userdata("adminID");

	$loginuserID = $this->session->userdata("loginuserID");

	$totalData = $this->live_m->get_join_broad_count($adminID,$usertype,$loginuserID);

	$totalFiltered = $totalData;

	$posts = $this->live_m->make_datatables($adminID,$usertype,$loginuserID);

            // $totalFiltered  = $this->student_m->get_filtered_data();

	$av_array = array(1 =>'Video Stream',2=>'Audio Stream');

	$status_array = array(1 =>'Live Class Created',2=>'Unpublish',3=>'Live Class Over');

        $data = array();

        if(!empty($posts))

        {

        	$i = 1;

            foreach ($posts as $post)

            {



				$nestedData['sn'] = $i++;

				//$nestedData['date']=date('d-m-Y H:i',strtotime($post->createTime));

				//$nestedData['subject'] = $post->subject;

				$nestedData['stream_name'] = $post->stream_name;

                $nestedData['channel_name'] = $post->channel_name;

                $nestedData['av'] = $av_array[$post->av];

                $nestedData['status'] = $status_array[$post->live_status];





   			if($post->live_status==3)

   			{

   				$buttons     = btn_delete('live/delete_stream/'.$post->streamID, $this->lang->line('delete'));

   			}

   			else

   			{

   				$buttons     = btn_view('live/index/'.$post->streamID.'/'.$post->av.'/'.$post->classtiming.'?channel='.$post->channel_name, 'View').btn_edit('live/edit_stream/'.$post->streamID, $this->lang->line('edit')).btn_delete('live/delete_stream/'.$post->streamID, $this->lang->line('delete'));

   			}

   		    

 		 $nestedData['action'] = $buttons;



			 

               

                $data[] = $nestedData;



            }

        }



       $json_data = array(

                    "draw"            => intval($this->input->post('draw')),  

                    "recordsTotal"    => $totalData,  

                    "recordsFiltered" => $totalFiltered, 

                    "data"            => $data   

                    );



      

            

        echo json_encode($json_data); 

}



public function add_stream()

{

	$this->data['panel_title'] = 'Create Live Class';

	$courseID=$this->uri->segment(3);

	$yearsOrSemester=$this->uri->segment(4);

	$subjectID=$this->uri->segment(5);
	$classtime=substr($this->uri->segment(6),3);
	$loginname=$this->session->userdata("name");
	$time=time();
	if($_POST)

	{

		$data = array(

				'adminID'=>$this->session->userdata('adminID'),

				'CreatedUsertype'=>$this->session->userdata('usertype'),

				'createdID'=>$this->session->userdata('loginuserID'),

				'stream_name'=>$this->input->post('stream_name'),

				'channel_name'=>$time,

				'av'=>$this->input->post('av'),

				'courseID'=>$courseID,

				'yearsOrSemester'=>$yearsOrSemester,

				'subjectID'=>$subjectID,
				'classtiming'=>$classtime,

				'live_status'=>1,

				'date'=>date('Y-m-d'),

				);

		$checkdate=$this->db->select('CreatedUsertype')->where(array('date'=>date('Y-m-d'),'courseID'=>$courseID,'yearsOrSemester'=>$yearsOrSemester,'subjectID'=>$subjectID,'classtiming'=>$classtime,'live_status'=>1,'createdID'=>$this->session->userdata('loginuserID')))->get('live_broadcast')->row();
		if($checkdate)
		{
			redirect(base_url('dashboard/index'));
		}
		else
		{
			$this->db->insert('live_broadcast',$data);
			$lastid=$this->db->insert_id();
		}

		if($courseID)

		{

			$studentlist=$this->db->select('student.studentID,student.name,student.email,student.phone,subject.subject')->from('subject')->join('student','student.classesID=subject.classesID','left')->where(array('subject.classesID' =>$courseID,'subject.yearsOrSemester'=>$yearsOrSemester,'student.classesID' =>$courseID,'student.yearsOrSemester'=>$yearsOrSemester,'subject.subjectID'=>$subjectID,'subject.professorID'=>$this->session->userdata('loginuserID'),'subject.subject_mode'=>0,'subject.status'=>1,'student.status'=>1))->get()->result();

			// print_r($studentlist);die;

			if($studentlist)

			{

				foreach ($studentlist as $key => $value)

				{

					$subject="Lecture Streaming Live Now by $loginname";

		            $dataEamil['superadminemail']="";

		            $dataEamil['adminemail']=""; 

		            $dataEamil['institute']=$this->data["siteinfos"]->sname;
		            // MSG Start
		            if($value->phone)
		            {
		            	$link=base_url("live/audience/".$lastid.'/'.$this->input->post('av').'/'.$classtime.'?channel='.$time);
		            	// $sendmsg=sendliveclasstostudent($loginname,$value->subject,$this->input->post('stream_name'),$value->phone,$link);
		            }		            
		            // MSG end
		            if(preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/",$value->email))

				    {

				    	// print_r($stu_email);die;			

						$email="team@edgetechnosoft.com";

						$fullname=$value->name;

						$dataEamil['name']=$value->name;

						$dataEamil['subject']=$value->subject;

						$dataEamil['base_url']=base_url("live/audience/".$lastid.'/'.$this->input->post('av').'/'.$classtime.'?channel='.$time);

						if($this->input->post('av')==1)

						{

							$html = $this->load->view('emailTemplates/liveclassforstudentvideo', $dataEamil , true);

						}

						else

						{

							$html = $this->load->view('emailTemplates/liveclassforstudentaudio', $dataEamil , true);

						}

						

					   // $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);

						$sendmail= true;

				    }

				    else

				    {

				    	$sendmail= true;

				    }

				}

				if($sendmail == true || $sendmail == 1)

				{

					redirect(base_url("live/index/".$lastid.'/'.$this->input->post('av').'/'.$classtime.'?channel='.$time));

				}

			}

			else

			{

				$this->session->set_flashdata('error',"No Student found in this class");

				redirect(base_url("live/table"));

			}

			

		}

		else

		{

			$this->session->set_flashdata('success',$this->lang->line('menu_success'));

			redirect(base_url("live/table"));

		}

	}

	else

	{

		$this->data["subview"] = "live/add_stream";

		$this->load->view('_layout_main', $this->data);						

	}

}



public function edit_stream(){

$this->data['panel_title'] = 'Edit Live Class';

$this->data['edit_data']= $this->db->where('streamID',$this->uri->segment(3))->get('live_broadcast')->row();





if($_POST){



$data = array(

'stream_name'=>$this->input->post('stream_name'),

'av'=>$this->input->post('av'),

'live_status'=>$this->input->post('live_status'),

);



$this->db->where('adminID',$this->session->userdata('adminID'));

$this->db->where('streamID',$this->uri->segment(3));

$this->db->update('live_broadcast',$data);



	$this->session->set_flashdata('success',$this->lang->line('menu_success'));

					redirect(base_url("live/table"));





}else{

			    $this->data["subview"] = "live/edit_stream";

				$this->load->view('_layout_main', $this->data);

			

}



}







public function delete_stream(){ 



$this->db->where('streamID',$this->uri->segment(3));

$this->db->delete('live_broadcast');

$this->session->set_flashdata('success',$this->lang->line('menu_success'));

					redirect(base_url("live/table"));



}

public function joinliveclass()

{

	$studentID=$this->input->post('studentID');

	$streamID=$this->input->post('streamID');

	$data = array('streamID' =>$streamID ,'studentID'=>$studentID,'status'=>1 );

	$insertstudent=$this->live_m->broad_live_student($data);

	$showdetail=$this->live_m->broad_live_studentdata($streamID);

	if($showdetail)

	{

		$output="";

		foreach ($showdetail as $key => $value)

		{

			$output .='<div class="data">';

			$output .='<img src="base_url(uploads/images/'.$value->photo.')">

              		<p class="name-user">'.$value->name.'</p>';

      		$output .='</div>';

		}

		echo $output;

		

	}

	

}

public function liveclass()

{

	$streamID=$this->input->post('streamID');

	$showdetail=$this->live_m->broad_live_studentdata($streamID);

	if($showdetail)

	{

		$output="";

		foreach ($showdetail as $key => $value)

		{

			$output .='<div class="data">';

			$output .='<img src='.base_url("uploads/images/$value->photo").' width="50" height="50" >';

            $output .='<p class="name-user">'.$value->name.'</p>';

      		$output .='</div>';

		}

		echo $output;

		

	}

}

public function changeliveclassstatus()

{

	$streamID=$this->input->post('streamID');

	$this->db->where('streamID',$streamID);

	$this->db->update('live_broadcast',array('live_status' =>3,'status'=>2));

	echo true;

}



}