<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qa extends Admin_Controller {

	function __construct() {
		parent::__construct();
		 $this->load->model("subject_m");
		$this->load->model("qa_m");
		$this->load->model("student_info_m");
		$this->load->model("classes_m");
		$this->load->library('pagination');
		$language = $this->session->userdata('lang');
		$this->lang->load('qa', $language);	

	}


public function index(){
		$usertype = $this->session->userdata("usertype");
	    $adminID = $this->session->userdata("adminID");

//pagination
		$config = array();
        $config['base_url'] = site_url('qa/index/all'); 
       $rem =   $this->qa_m->getPaginationrowvideo();
        $config['total_rows'] = $rem;
        $config['per_page'] = "15";
        $config["uri_segment"] = 4;
        $choice = ($config["total_rows"] / $config["per_page"]);
        $config["num_links"] = floor($choice);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0; 
        // print_r($this->data['page']);die;           
        $this->data['pagination'] = $this->pagination->create_links();
		$this->data['qa'] = $this->qa_m->get_join_where_subject_video($config["per_page"], $this->data['page'],$adminID);


//end pagination

		    $this->data["subview"] = "qa/index";

			$this->load->view('_layout_main', $this->data);



}



public function add(){

$student = $this->student_info_m->get_student_info();
			$this->data['array_subject'] = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$this->session->userdata('loginuserID'));
if($_POST){

$data = array(
'subjectID'=>$this->input->post('subjectID'),
'unitID'=>$this->input->post('unitID'),
'title'=>$this->input->post('title'),
'que'=>$this->input->post('que'),
'CreatedUserID'=>$this->session->userdata('loginuserID'),
'CreatedUsertype'=>$this->session->userdata('usertype'),
'createTime'=>date('Y-m-d H:i:s') 
);
$this->db->insert('qa',$data);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("qa/index/all"));
}else{
		    $this->data["subview"] = "qa/add";
			$this->load->view('_layout_main', $this->data);	
}

}

function deleteQue(){
$loginuserID = $this->session->userdata('loginuserID');
$usertype = $this->session->userdata('usertype');
$uri = $this->uri->segment(3);


$this->db->where('CreatedUserID',$loginuserID);
$this->db->where('CreatedUsertype',$usertype);
$this->db->where('qaID',$uri); 
$dele= $this->db->delete('qa');
if($dele){
$this->db->where('qaID',$uri);
$this->db->delete('qa_ans');

$this->db->where('qaID',$uri);
$this->db->delete('qa_user_x_vote_que');

$this->db->where('qaID',$uri);
$this->db->delete('qa_user_x_vote_ans');

$this->db->where('qaID',$uri);
$this->db->delete('qa_views');
}


$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("qa/index/all"));

}

function deleteans(){
$loginuserID = $this->session->userdata('loginuserID');
$usertype = $this->session->userdata('usertype');
$uri = $this->uri->segment(3);

$qaID= $this->db->select('qaID')->where('ansID',$uri)->get('qa_ans')->row();
if($qaID){
$no_ans= $this->db->select('no_ans,qaID')->where('qaID',$qaID->qaID)->get('qa')->row();
}
$this->db->where('CreatedUserID',$loginuserID);
$this->db->where('CreatedUsertype',$usertype);
$this->db->where('ansID',$uri);
 $dele = $this->db->delete('qa_ans');

 if($no_ans){
 	$data_ups =array(
 		'no_ans'=>$no_ans->no_ans-1
 	);
 	$this->db->where('qaID',$no_ans->qaID);
 	$this->db->update('qa',$data_ups);
 }

if($dele){

$this->db->where('ansID',$uri);
$this->db->delete('qa_user_x_vote_ans');

}




$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("qa/index/all"));

}



public function addAnswer(){
$qaID =  $this->input->post('id'); 

if($_POST){
$data = array(
'qaID'=>$qaID,
'ans'=>$this->input->post('ans'),
'CreatedUserID'=>$this->session->userdata('loginuserID'),
'CreatedUsertype'=>$this->session->userdata('usertype'), 
'createTime'=>date('Y-m-d H:i:s') 
);
$this->db->insert('qa_ans',$data);

$no_ans = $this->db->select('no_ans')->where('qaID',$qaID)->get('qa')->row();
$data_update =array(
'no_ans'=>$no_ans->no_ans+1,
);

$this->db->where('qaID',$qaID);
$this->db->update('qa',$data_update);	
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("qa/chat/".$qaID));
}

}



public function edit(){
$id = $this->uri->segment(3);

$student = $this->student_info_m->get_student_info();
			$this->data['array_subject'] = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$this->session->userdata('loginuserID'));
if($_POST){
$data = array(
'subjectID'=>$this->input->post('subjectID'),
'unitID'=>$this->input->post('unitID'),
'title'=>$this->input->post('title'),
'que'=>$this->input->post('que'),
'qa_status'=>$this->input->post('qa_status'),
);
$this->db->where('qaID',$id);
$this->db->where('CreatedUserID',$this->session->userdata('loginuserID'));
$this->db->update('qa',$data);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("qa/index/all"));
}else{

if($id){
			$this->data['qa'] = $this->db->where('qaID',$id)->where('CreatedUserID',$this->session->userdata('loginuserID'))->get('qa')->row();
			if($this->data['qa']){
			$this->data['units'] = $this->db->where('subjectID',$this->data['qa']->subjectID)->get('units')->result();

		    $this->data["subview"] = "qa/edit";
			$this->load->view('_layout_main', $this->data);	
		}else{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);	
		}

	}else{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
	}
}


}


public function chat(){
	$loginuserID = $this->session->userdata('loginuserID');
	$usertype = $this->session->userdata('usertype');
$qaID = $this->uri->segment(3);
if($qaID){

$this->db->where('qaID',$qaID);
$this->data['qa'] =  $this->db->get('qa')->row();
if($this->data['qa']){
	$num_views = $this->db->where('qaID',$qaID)->where('CreatedUserID',$loginuserID)->where('CreatedUsertype',$usertype)->get('qa_views')->num_rows();
	if($num_views==0){
		$data_qa_view = array(
		'qaID'=>$qaID,
		'CreatedUserID'=>$loginuserID,
		'CreatedUsertype'=>$usertype
		);
		$this->db->insert('qa_views',$data_qa_view);
		$data_update_que = array(
		'views'=>$this->data['qa']->views+1,
		); 
		$this->db->where('qaID',$qaID);
		$this->db->update('qa',$data_update_que);
	}
	$this->data['name_of_qa'] = $this->db->select('name,studentID,photo')->where('studentID',$this->data['qa']->CreatedUserID)->get('student')->row();
	$this->data['name_of_subject'] = $this->db->select('subject,subjectID,professorID')->where('subjectID',$this->data['qa']->subjectID)->get('subject')->row();
	$this->data['name_of_professor'] = $this->db->select('name,professorID')->where('professorID',$this->data['name_of_subject']->professorID)->get('professor')->row();

	$this->data['name_of_unit'] = $this->db->select('unit_name,unitID')->where('unitID',$this->data['qa']->unitID)->get('units')->row();

	 $ans_array   = $this->db->where('qaID',$qaID)->order_by('ansID','desc')->get('qa_ans')->result_array();

	 foreach ($ans_array as $key => $value) {
	 	if($value['CreatedUsertype']=='Professor'){
	 	 $ans_array[$key]['name_photo'] = $this->db->select('name,photo')->where('professorID',$value['CreatedUserID'])->get('professor')->row_array();	
	 	}else{
	 	$ans_array[$key]['name_photo'] = $this->db->select('name,studentID,photo')->where('studentID',$value['CreatedUserID'])->get('student')->row_array();
	 	}
	 }

	 $this->data['ans'] = $ans_array;
 
			$this->data["subview"] = "qa/chat";
			$this->load->view('_layout_main', $this->data);
		}else{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);	
		}
		}else{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
}

public function que_upvote(){
	$loginuserID = $this->session->userdata('loginuserID');
	$usertype = $this->session->userdata('usertype');
	$qaID = $this->input->post('qaID');
	$no_vote = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
    $ava_vote = $this->db->select('vote_up')->where('qaID',$qaID)
    ->where('CreatedUserID',$loginuserID)
    ->where('CreatedUsertype',$usertype)
    ->get('qa_user_x_vote_que')
    ->row();

if(count($ava_vote)==0){
	$data_insert =array(
	'vote_up'=>1,
	'CreatedUserID'=>$loginuserID,
	'CreatedUsertype'=>$usertype,
	'qaID'=>$qaID
	);
	//$this->db->where('qaID',$qaID);
	$this->db->insert('qa_user_x_vote_que',$data_insert);
	$data =array(
	'votes'=>$no_vote->votes+1,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa',$data);
		$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;

}else{
if($ava_vote->vote_up!=1){
	$data =array(
	'vote_up'=>1,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa_user_x_vote_que',$data);

	$data_update_up =array(
	'votes'=>$no_vote->votes+1,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa',$data_update_up);

	$data_update_down =array(
	'vote_down'=>0,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa_user_x_vote_que',$data_update_down);
		$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;
}else{
		$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;	
}
}
}


public function que_downvote(){
	$loginuserID = $this->session->userdata('loginuserID');
	$usertype = $this->session->userdata('usertype');
	$qaID = $this->input->post('qaID');
	$no_vote = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
    $ava_vote = $this->db->select('vote_down')->where('qaID',$qaID)
    ->where('CreatedUserID',$loginuserID)
    ->where('CreatedUsertype',$usertype)
    ->get('qa_user_x_vote_que')
    ->row();
if($no_vote->votes!=0){
if(count($ava_vote)==0){
	$data_insert =array(
	'vote_down'=>1,
	'CreatedUserID'=>$loginuserID,
	'CreatedUsertype'=>$usertype,
	'qaID'=>$qaID
	);
	//$this->db->where('qaID',$qaID);
	$this->db->insert('qa_user_x_vote_que',$data_insert);
	if($no_vote->votes!=0){
	$data =array(
	'votes'=>$no_vote->votes-1,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa',$data);
}
	$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;

}else{
if($ava_vote->vote_down!=1){
	$data =array(
	'vote_down'=>1,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa_user_x_vote_que',$data);

	$data_update_up =array(
	'votes'=>$no_vote->votes-1,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa',$data_update_up);
	$data_update_down =array(
	'vote_up'=>0,
	);
	$this->db->where('qaID',$qaID);
	$this->db->update('qa_user_x_vote_que',$data_update_down);
		$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;
}else{
		$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;	
}
}
}else{
		$no_vote_now = $this->db->select('votes')->where('qaID',$qaID)->get('qa')->row();
	echo $no_vote_now->votes;	
}
}



//ans

public function ans_upvote(){
	$loginuserID = $this->session->userdata('loginuserID');
	$usertype = $this->session->userdata('usertype');
	$ansID = $this->input->post('ansID');
	$qaID = $this->input->post('qaID');
	$no_vote = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
    $ava_vote = $this->db->select('vote_up')->where('ansID',$ansID)
    ->where('CreatedUserID',$loginuserID)
    ->where('CreatedUsertype',$usertype)
    ->get('qa_user_x_vote_ans')
    ->row();

if(count($ava_vote)==0){
	$data_insert =array(
	'vote_up'=>1,
	'CreatedUserID'=>$loginuserID,
	'CreatedUsertype'=>$usertype,
	'ansID'=>$ansID,
	'qaID'=>$qaID
	);
	//$this->db->where('ansID',$ansID);
	$this->db->insert('qa_user_x_vote_ans',$data_insert);
	$data =array(
	'votes'=>$no_vote->votes+1,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_ans',$data);
		$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;

}else{
if($ava_vote->vote_up!=1){
	$data =array(
	'vote_up'=>1,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_user_x_vote_ans',$data);

	$data_update_up =array(
	'votes'=>$no_vote->votes+1,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_ans',$data_update_up);

	$data_update_down =array(
	'vote_down'=>0,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_user_x_vote_ans',$data_update_down);
		$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;
}else{
		$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;	
}
}
}


public function ans_downvote(){
	$loginuserID = $this->session->userdata('loginuserID');
	$usertype = $this->session->userdata('usertype');
	$ansID = $this->input->post('ansID');
	$qaID = $this->input->post('qaID');
	$no_vote = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
    $ava_vote = $this->db->select('vote_down')->where('ansID',$ansID)
    ->where('CreatedUserID',$loginuserID)
    ->where('CreatedUsertype',$usertype)
    ->get('qa_user_x_vote_ans')
    ->row();
if($no_vote->votes!=0){
if(count($ava_vote)==0){
	$data_insert =array(
	'vote_down'=>1,
	'CreatedUserID'=>$loginuserID,
	'CreatedUsertype'=>$usertype,
	'ansID'=>$ansID,
	'qaID'=>$qaID
	);
	//$this->db->where('ansID',$ansID);
	$this->db->insert('qa_user_x_vote_ans',$data_insert);
	$data =array(
	'votes'=>$no_vote->votes-1,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_ans',$data);
	$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;

}else{
if($ava_vote->vote_down!=1){
	$data =array(
	'vote_down'=>1,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_user_x_vote_ans',$data);
	$data_update_up =array(
	'votes'=>$no_vote->votes-1,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_ans',$data_update_up);
	$data_update_down =array(
	'vote_up'=>0,
	);
	$this->db->where('ansID',$ansID);
	$this->db->update('qa_user_x_vote_ans',$data_update_down);
		$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;
}else{
		$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;	
}
}
}else{
		$no_vote_now = $this->db->select('votes')->where('ansID',$ansID)->get('qa_ans')->row();
	echo $no_vote_now->votes;	
}
}

public function viewqa()
{
	$this->data["subview"] = "qa/viewqa";

			$this->load->view('_layout_main', $this->data);
}

public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->qa_m->get_join_qa_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->qa_m->make_datatables($adminID);
// print_r($totalData);die;
            // $totalFiltered  = $this->student_m->get_filtered_data();
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['date']=date('d-m-Y H:i',strtotime($post->createTime));
				$nestedData['subject'] = $post->subject;
				$nestedData['unit'] = $post->unit_name;
                $nestedData['student'] = $post->name;

   			if($usertype == "Admin" || $usertype == 'ClgAdmin')
   			{
   		    // $buttons     = btn_view('notice/view/'.$post->noticeID, $this->lang->line('view')).btn_edit('notice/edit/'.$post->noticeID, $this->lang->line('edit')).btn_delete('notice/delete/'.$post->noticeID, $this->lang->line('delete'));
 		

			    $nestedData['question'] = $post->que;
			}
               
                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

      
            
        echo json_encode($json_data); 
}




}