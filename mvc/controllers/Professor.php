<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Professor extends Admin_Controller {



	function __construct() {

		parent::__construct();

		$this->load->model("professor_m");
		$this->load->model("subject_m");
		$this->load->model("classes_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");

		$this->load->model("user_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('professor', $language);	

	}



	public function index() {
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype)
		{
			$this->data['ActiveProfessor_count']  =  $this->professor_m->ActiveProfessor_count();
			$this->data['DraftProfessor_count']  =  $this->professor_m->DraftProfessor_count();
			$this->data['TrashProfessor_count']  =  $this->professor_m->TrashProfessor_count();
			if ($usertype=='superadmin') 
			{
			  $this->data['professor'] = $this->professor_m->get_professor();
			}
			else
			{ 
			  // $this->data['professor'] = $this->professor_m->get_professor_ClgAdmin($adminID);
			 // echo $this->db->last_query();
			 // die;
			}
			// print_r("expression");die;
			$this->data["subview"] = "professor/index";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}

	}


	//Praveen Pathak
	
	function ajaxProfessors()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$totalData = $this->professor_m->get_professor_no($adminID);
		$totalFiltered = $totalData;
		$posts = $this->professor_m->make_datatables();

        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {
            	// $nestedData['check'] = "<label class='nexCheckbox'>
             //            <input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->professorID." '/>
             //            <span class='checkmark checkmark-action-layout'></span>
             //            </label>";
				$nestedData['sn'] = $i;
				$totaltopic=0;
	        	$totalunit=0;
	        	$unit=0;
	        	$topic=0;
	        	$button1=$this->professor_m->getsubjectandtopic($post->professorID);
                foreach($button1 as $row)
	      		{
	      			$unitavg1=0;$unitavg2=0;$unitavg3=0;
	      			// print_r($row['object']);
	        		foreach ($row['object'] as $key )
					{
						// print_r($key);die;
						$a=$key->topicid;
						$b=$key->totalunit;
						if($a)
						{
							
			                $topicconut=explode(",",$a);
			     			$totaltopic=(count($topicconut));
			     			$totalunit=$b;
			     			// print_r($totaltopic);die;
			             
	               		}
	               		
	        		}
	        		$topic += $totaltopic;
	               		$unit += $totalunit;
	        		
			    }
			    if($unit != 0)
			    {
			    	$per=($topic*100)/$unit;
				    // print_r($per);
				    $unitavg=round($per,0);
				}
				else
				{
					$unitavg=0;
                    
				}
				$percentage="%";
				$base_url=base_url('viewlecture/subjectprogress/');
				$nestedData['name'] = "<a href='$base_url$post->professorID' style='float: inherit;'><button class='warning_1' style='float: left;'><span>$unitavg$percentage</span></button></a>".' '.$post->name;
				$nestedData['username'] = $post->username;
				$nestedData['designation_name'] = $post->designation_name;
				$nestedData['department_name'] = $post->department_name;
				$nestedData['email'] = $post->email;
				$nestedData['status'] = '';
			    if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin") 
			    {
			        
			        if ($post->status!=2) 
                    {
                    	// print_r("expression");die;
                        $buttons     =  btn_view('professor/view/'.$post->professorID, $this->lang->line('view')).btn_edit('professor/edit/'.$post->professorID, $this->lang->line('edit')).btn_delete('professor/delete/'.$post->professorID, $this->lang->line('delete')).'<a class="btn btn-default default_1" href="'.base_url().'professor/passwordReset/'.$post->professorID.'" title="Reset Password"><i class="fa fa-undo" aria-hidden="true"></i></a>';

                    }
                    else
                    {
                    	// print_r("expression1");die;
                        $buttons = "<a class = 'appClassDelete' href = ".base_url()."professor/restore/".$post->professorID.">Restore</a>";
                    }


			        $nestedData['action'] = $buttons;
			    }
			    else
			    {
			        $buttons     =  btn_view('professor/view/'.$post->professorID, $this->lang->line('view'));
			        $nestedData['action'] = $buttons;
			    }
                $data[] = $nestedData;
                $i++;
            }
        }
       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

        echo json_encode($json_data); 
    }



	protected function rules() {

		$rules = array(

			array(

				'field' => 'name', 

				'label' => $this->lang->line("professor_name"), 

				'rules' => 'trim|required|max_length[60]'

			), 

			array(

				'field' => 'shortcode', 

				'label' => "Professor's short code", 

				'rules' => 'trim|required'

			),

			array(

				'field' => 'dob', 

				'label' => 'Date of Birth', 

				'rules' => 'trim|required'

			), 

			array(

				'field' => 'sex', 

				'label' => $this->lang->line("professor_sex"), 

				'rules' => 'trim|required'

			),

			

			array(

				'field' => 'email', 

				'label' => $this->lang->line("professor_email"), 

				'rules' => 'trim|required|max_length[40]|valid_email|callback_unique_email'

			),

			array(

				'field' => 'phone', 

				'label' => $this->lang->line("professor_phone"), 

				'rules' => 'trim|numeric|min_length[10]|max_length[10]|required'

			),

			array(

				'field' => 'username', 

				'label' => $this->lang->line("professor_username"), 

				'rules' => 'trim|required|min_length[4]|max_length[40]'

			),

			array(

				'field' => 'password',

				'label' => $this->lang->line("professor_password"), 

				'rules' => 'trim|required|min_length[4]|max_length[40]'

			),

			array(

				'field' => 'departmentID',

				'label' => 'Department Name', 

				'rules' => 'trim|required'

			)

		);


		return $rules;

	}



	function insert_with_image($username) {

	    $random = rand(1, 10000000000000000);

	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));

	    return $makeRandom;

	}


	// RAHUL
	function passwordReset(){
		$uri = $this->uri->segment(3);
		if ($_POST) 
		{
			$mainpassword = $this->input->post("new_password");
			$this->session->set_userdata('emailpass',$mainpassword);
			$newPassword = $this->hash($mainpassword);
			$this->professor_m->change_password_with_professor_by_superadmin($uri,$newPassword);
			$this->send_mail_reset_Password($uri);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("professor/index"));
		}else{
			$this->data["subview"] = "professor/passwordReset";

			$this->load->view('_layout_main', $this->data);
		}
		
		
	}
	
	public function hash($string) {
		return hash("sha512", $string . config_item("encryption_key"));
	}

	public function add() {

		$usertype = $this->session->userdata("usertype");
        $adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "superadmin"){

			$this->data['department'] = $this->db->get('department')->result();
			$this->data['designation'] = $this->db->get('designation')->result();

			if($_POST) {
				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors(); 

					$this->data["subview"] = "professor/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
                    
                    
                    date_default_timezone_set('Asia/Kolkata');
					$array = array();
                    
					$array['name'] = $this->input->post("name");

					$array["shortCodeofProfessor"] = $this->input->post("shortcode");

					$array['designation'] = $this->input->post("designation");

					$array['designationID'] = $this->input->post("designationID");

					$array['departmentID'] = $this->input->post("departmentID");

					$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));

					$array["sex"] = $this->input->post("sex");

					$array['religion'] = $this->input->post("religion");

					$array['email'] = $this->input->post("email");

					$array['phone'] = $this->input->post("phone");

					$array['address'] = $this->input->post("address");
					$array['adminID'] = $adminID;

					$array['jod'] = date("Y-m-d", strtotime($this->input->post("jod")));

					$array['username'] = $this->input->post("username");

					$array['password'] = $this->professor_m->hash($this->input->post("password"));

					$array['usertype'] = "Professor";

					$array["create_date"] = date("Y-m-d h:i:s");

					$array["modify_date"] = date("Y-m-d h:i:s");

					$array["create_userID"] = $this->session->userdata('loginuserID');

					$array["create_username"] = $this->session->userdata('username');

					$array["create_usertype"] = $this->session->userdata('usertype');
					
					$array['notification'] = '1';
					$array['notification_date'] = date('Y-m-d');
                    $array["s_date"] = date('Y-m-d H:i:s');
					$array["professoractive"] = 1;
			        
			        $new_file = "defualt.png";
						$array["photo"] = $new_file;
					
						$insertid=$this->professor_m->insert_professor($array);
						// $message =  "Hello ".$this->input->post('name')."\nYour username is ".$this->input->post('username')."\ndefault password is ".$this->input->post('password')." \nHappy Learning!";		 
						// 		// print_r($message);die;   
				  //           	    $requestParams = array(
				  //                       'user' => 'academicedge',
				  //                       'pass' => 'bbz10@123',
				  //                       'sender' => 'EDGSIS',
				  //                       'phone' => $this->input->post('phone'),
				  //                       'text' => $message,
				  //                       'priority' => 'ndnd',
				  //                       'stype' => 'normal'
				  //                   );
				  //                   $apiUrl = "http://bhashsms.com/api/sendmsg.php?";
				  //                   foreach($requestParams as $key => $val){
				  //                       $apiUrl .= $key.'='.urlencode($val).'&';
				  //                   }

				  //                   $apiUrl = rtrim($apiUrl, "&");
				  //                   $ch = curl_init();
				  //                   curl_setopt($ch, CURLOPT_URL, $apiUrl);
				  //                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				  //                   $result = curl_exec($ch);
				  //                   curl_close($ch);
				  //                   $return = json_encode($result);
						$this->send_mail_new_regitration($insertid);
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("professor/index"));
					

				}

			} else {

				$this->data["subview"] = "professor/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function view() {
		$usertype = $this->session->userdata('usertype');

		if ($usertype) {

			$id =$this->uri->segment(3);

			if ((int)$id) {

				$this->data['professor'] = $this->professor_m->get_professor_with_join($id);

				// $this->data['department'] = $this->professor_m->get_professor($this->data['professor']->departmentID);



				if($this->data['professor']) {


					$this->data["subview"] = "professor/view";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function edit() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Professor"){

			$this->data['department'] = $this->db->get('department')->result();

			$this->data['designation'] = $this->db->get('designation')->result();

			$id = $this->uri->segment(3);

			if((int)$id) {

				$this->data['professor'] = $this->professor_m->get_professor($id);

				if($this->data['professor']) {

					if($_POST) {

						$rules = $this->rules();

				
						unset($rules[7],$rules[8],$rules[9]);

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) { 

							$this->data["subview"] = "professor/edit";

							// print_r(validation_errors());
							// exit();

							$this->load->view('_layout_main', $this->data);

						} else {

							$array = array();

							$array['name'] = $this->input->post("name");

							$array["shortCodeofProfessor"] = $this->input->post("shortcode");

							$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
							
							$array["sex"] = $this->input->post("sex");

							$array['email'] = $this->input->post("email");

					        $array['designationID'] = $this->input->post("designationID");

          					$array['departmentID'] = $this->input->post("departmentID");

							$array['phone'] = $this->input->post("phone");

							$array["modify_date"] = date("Y-m-d h:i:s");

							$array["username"] = $this->input->post("username");

	

							$this->professor_m->update_professor($array, $id);
							$this->send_mail_edit_regitration($id);
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("professor/index"));

						

						}

					} else {

						$this->data["subview"] = "professor/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"){

			$id = $this->uri->segment(3);

			if((int)$id) {

				$this->data['professor'] = $this->professor_m->get_professor($id);

				if($this->data['professor']) {

					if($this->data['professor']->photo != 'defualt.png') {

						unlink(FCPATH.'uploads/images/'.$this->data['professor']->photo);

					}

					$this->professor_m->delete_professor($id);

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					redirect(base_url("professor/index"));

				} else {

					redirect(base_url("professor/index"));

				}

			} else {

				redirect(base_url("professor/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function lol_username() {

		$id = $this->uri->segment(3);

		if((int)$id) {

			$professor_info = $this->professor_m->get_single_professor(array('professorID' => $id));

			$tables = array('student' => 'student', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();

			$i = 0;

			foreach ($tables as $table) {

				$user = $this->professor_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $professor_info->email));

				if(count($user)) {

					$this->form_validation->set_message("lol_username", "%s already exists");

					$array['permition'][$i] = 'no';

				} else {

					$array['permition'][$i] = 'yes';

				}

				$i++;

			}

			if(in_array('no', $array['permition'])) {

				return FALSE;

			} else {

				return TRUE;

			}

		} else {

			$tables = array('student' => 'student', 'parent' => 'parent', 'professor' => 'professor', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();

			$i = 0;

			foreach ($tables as $table) {

				$user = $this->professor_m->get_username($table, array("username" => $this->input->post('username')));

				if(count($user)) {

					$this->form_validation->set_message("lol_username", "%s already exists");

					$array['permition'][$i] = 'no';

				} else {

					$array['permition'][$i] = 'yes';

				}

				$i++;

			}



			if(in_array('no', $array['permition'])) {

				return FALSE;

			} else {

				return TRUE;

			}

		}			

	}



	public function date_valid($date) {

		if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);   

	        $dd = $arr[0];            

	        $mm = $arr[1];              

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    } 

	} 


	public function restore() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") {

			$id = $this->uri->segment(3);

			if((int)$id) {
				$data = array(
					'status'=>1
				);
				$this->db->where('professorID',$id);
				$this->db->update('professor',$data);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("professor/index"));

			} else {

				redirect(base_url("professor/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


	public function print_preview() {

		$id = $this->uri->segment(3);

		if ((int)$id) {

			$usertype = $this->session->userdata('usertype');

			if ($usertype == "Admin") {

			    $this->load->library('html2pdf');

			    $this->html2pdf->folder('./assets/pdfs/');

			    $this->html2pdf->filename('Report.pdf');

			    $this->html2pdf->paper('a4', 'portrait');



				$this->data["professor"] = $this->professor_m->get_professor($id);

				if($this->data["professor"]) {

					$this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('professor/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create();

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin") {

			$id = $this->input->post('id');

			if ((int)$id) {

				$this->load->library('html2pdf');

			    $this->html2pdf->folder('uploads/report');

			    $this->html2pdf->filename('Report.pdf');

			    $this->html2pdf->paper('a4', 'portrait');



				$this->data["professor"] = $this->professor_m->get_professor($id);

				if($this->data["professor"]) {

					$this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('professor/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));	

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function unique_email() {

		$id = $this->uri->segment(3);

		if((int)$id) {

			$professor_info = $this->professor_m->get_single_professor(array('professorID' => $id));

			$tables = array('admin' => 'admin','student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();

			$i = 0;

			foreach ($tables as $table) {

				$user = $this->professor_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $professor_info->username));

				if(count($user)) {

					$this->form_validation->set_message("unique_email", "%s already exists");

					$array['permition'][$i] = 'no';

				} else {

					$array['permition'][$i] = 'yes';

				}

				$i++;

			}

			if(in_array('no', $array['permition'])) {

				return FALSE;

			} else {

				return TRUE;

			}

		} else {

			$tables = array('admin' => 'admin','student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();

			$i = 0;

			foreach ($tables as $table) {

				$user = $this->professor_m->get_username($table, array("email" => $this->input->post('email')));

				if(count($user)) {

					$this->form_validation->set_message("unique_email", "%s already exists");

					$array['permition'][$i] = 'no';

				} else {

					$array['permition'][$i] = 'yes';

				}

				$i++;

			}



			if(in_array('no', $array['permition'])) {

				return FALSE;

			} else {

				return TRUE;

			}

		}	

	}




function AddUsernamePass(){
 
$password = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) ); // random(ish) 5 digit int

$username = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string

$passwordPlusName = strtok($this->input->post('name_id')," ").'@'.$password;

$usernamePlusName = strtok($this->input->post('name_id')," ").'@'.$username;

$array = array(
	'password'=>$passwordPlusName,
	'username'=>$usernamePlusName
);

 echo  json_encode(($array));

}



public function send_mail_new_regitration($insertid) {
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$clgadminemail=$this->professor_m->getsuperadminid($adminID);
		$arr = array();
	    $this->db->select('email');
	    $this->db->from('user');
	    $where = "usertype = 'Admin' and status = 1 and adminID = $adminID";
	    $this->db->where($where);
	    $sqls = $this->db->get();
	    $results = $sqls->result_array();
	    foreach($results as $rows)
	    {
	        $arr[] = array(
	                        $rows['email']
	                        );
	    }
	    
	    $arrs = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($arr), ENT_NOQUOTES));
	    $admin_user =  str_replace('"','',$arrs);
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'Professor')
		{
           $username  = $this->input->post('username');
     	   $this->db->select('*');
           $this->db->where('professorID',$insertid);
           $this->db->from('professor');
           $query2  = $this->db->get()->row();
    
  		   $dataEamil['emailData'] = $query2;
           $dataEamil['password'] = $this->input->post("password");
           $subject="Welcome To ITM Campus Automation Sofware";
           $html = $this->load->view('emailTemplates/user/useradd', $dataEamil , true);
           $email=$query2->email;
           $fullname=$query2->name;
           $dataEamil['superadminemail']="";
           $dataEamil['adminemail']="";
           $dataEamil['institute']=$this->data["siteinfos"]->sname;
           $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
		   if($sendmail == true)
		   {
		   		$dataEamil['superadminemail']=$clgadminemail->email;
            	$dataEamil['adminemail']=$arr;
            	$subjectadmin="User Addition in ITM Campus Automation Software";
            	$htmladmin=$this->load->view('emailTemplates/user/useraddadmin',$dataEamil, true);
            	$sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
				$this->session->set_flashdata('success', $this->lang->line('mail_success'));
		   }
		   else
		   {
				$this->session->set_flashdata('error', $this->lang->line('mail_error'));
		   }
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function send_mail_edit_regitration($id)
	{
		$usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        $clgadminemail=$this->professor_m->getsuperadminid($adminID);
        $arr = array();
	    $this->db->select('email');
	    $this->db->from('user');
	    $where = "adminID = '".$adminID."' and create_usertype = 'ClgAdmin' and status =1 and usertype='Admin'";
	    $this->db->where($where);
	    $sqls = $this->db->get();
	    $results = $sqls->result_array();
	    foreach($results as $rows)
	    {
	        $arr[] = array(
	                        $rows['email']
	                        );
	    }
	    
	    $arrs = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($arr), ENT_NOQUOTES));
	    $admin_user =  str_replace('"','',$arrs);
        if($usertype == "Admin" || $usertype == 'ClgAdmin') 
        {
        	$getadmin=$this->professor_m->getsuperadminid($adminID);
            $this->db->select('*');
            $this->db->where('professorID',$id);
            $this->db->from('professor');
            $query2  = $this->db->get()->row();
            
            $dataEamil['emailData'] = $query2;
            
            $subject="Details Updated on ITM Campus Automation Software";
            $html = $this->load->view('emailTemplates/user/userupdate', $dataEamil , true);
            $email=$query2->email;
            $fullname=$query2->name;
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']="";
            // $dataEamil['Accountant']=
            $dataEamil['institute']=$this->data["siteinfos"]->sname;
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
            
            if($sendmail == true) 
            {
            	$dataEamil['superadminemail']=$getadmin->email;
           		$dataEamil['adminemail']=$arr;
            	$subjectadmin="Professor Detail Updated on ITM Campus Automation Software";
            	$htmladmin=$this->load->view('emailTemplates/user/userupdateadmin',$dataEamil, true);
            	$sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
                $this->session->set_flashdata('success', $this->lang->line('mail_success'));
            } 
            else 
            {
                $this->session->set_flashdata('error', $this->lang->line('mail_error'));
            }
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}


	// RAHUL
	public function send_mail_reset_Password($id) {
		
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");

		
		$this->data['newpass'] = $this->input->post("new-password");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'Professor') {
    
	        $this->db->select('*');
	        $this->db->where('professorID',$id);
	        $this->db->from('professor');
	      	$data['emailData']  = $this->db->get()->row();

	      	 $data['coolPassword'] = $this->session->userdata('emailpass');
			$array = array("src" => base_url('uploads/images/'.$this->data["siteinfos"]->photo),'class' => 'img-rounded');

			$data['imgData'] = $array;

			$this->load->library('email');
			$config=array(
			'charset'=>'utf-8',
			'wordwrap'=> TRUE,
			'mailtype' => 'html'
			);

            $this->email->initialize($config);
			$html = $this->load->view('emailTemplates/resetPasswordPrefessor',$data , true);
		 
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($data['emailData']->email);
			$this->email->subject("Welecome to Institute of Technology & Management, Dehradun Professor Panel");
			$this->email->message($html);
				if($this->email->send()) {
					$this->session->set_flashdata('success', $this->lang->line('mail_success'));
				} else {
					$this->session->set_flashdata('error', $this->lang->line('mail_error'));
				}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}


	function active() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin") {

			$id = $this->input->post('id');

			$status = $this->input->post('status');

			if($id != '' && $status != '') {

				if((int)$id) {

					if($status == 'chacked') {

						$this->professor_m->update_professor(array('professoractive' => 1), $id);

						echo 'Success';

					} elseif($status == 'unchacked') {

						$this->professor_m->update_professor(array('professoractive' => 0), $id);

						echo 'Success';

					} else {

						echo "Error";

					}

				} else {

					echo "Error";

				}

			} else {

				echo "Error";

			}

		} else {

			echo "Error";

		}

	}

	function ActiveProfessor(){
		$this->session->set_userdata('ActiveProfessor',1);
		$this->session->unset_userdata('DraftProfessor');
		$this->session->unset_userdata('TrashProfessor');

	}

	function DraftProfessor(){
		$this->session->unset_userdata('ActiveProfessor');
		$this->session->set_userdata('DraftProfessor',0);
		$this->session->unset_userdata('TrashProfessor');

	}

	function TrashProfessor(){
		$this->session->unset_userdata('ActiveProfessor');
		$this->session->unset_userdata('DraftProfessor');
		$this->session->set_userdata('TrashProfessor',2);

	}
	public function student_attendaancerepost()
	{
		$language = $this->session->userdata('lang');
		$this->lang->load('studentattendancereport', $language);
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
	 	$this->data['professor']=$loginuserID;
	 	$getdepartment=$this->input->get('department');
		$getClasses = $this->input->get('course');
		$getSubcourse = $this->input->get('subcourse');
		$getSemester = $this->input->get('yearsOrSemester');
		$getSubject = $this->input->get('subject');
		$year=$this->input->get('year');
		if($getSubcourse == "")
		{
			$getSubcourse=0;
		}
		if($year==1)
		{
			$this->data['dateshow']=7;
			$now = new DateTime( "7 days ago", new DateTimeZone('asia/kolkata'));
            $interval = new DateInterval( 'P1D'); // 1 Day interval
            $period = new DatePeriod( $now, $interval, 7); // 7 Days 
            foreach ($period as $day)
            {
            	$key[] = $day->format( 'Y-m-d');
            	
            }
            $firstdate=$key[0];
            $lastdate=$key[7];
            $this->data['getdateweek']=$key;
            $this->data['getStudent'] = $this->professor_m->getstudentattendancebyweek($getClasses,$getSubcourse,$getSemester,$getSubject,$firstdate,$lastdate);
            
		}
		else if($year==2)
		{
			$this->data['dateshow']=30;
			$todaydate=date('d');
			$todaydate=$todaydate-1;
            $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
            $interval = new DateInterval( 'P1D'); // 1 Day interval
            $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
            foreach ($period as $day)
            {
            	$key[] = $day->format( 'Y-m-d');
            	
            }
            
            $firstdate=$key[0];
            $lastdate=$key[$todaydate];
            // print_r($firstdate);die;
			$noofdays=date('t');
			$todaydate=date('d');
			$this->data['getdateweek']=$key;
            $this->data['getStudent'] = $this->professor_m->getstudentattendancebyweek($getClasses,$getSubcourse,$getSemester,$getSubject,$firstdate,$lastdate);
			//print_r($this->data['getStudent']);die;
		}
		else if($year==3)
		{
			$this->data['dateshow']=90;
			$todaydate=date('d');
			$todaydate=$todaydate-1;
			$todaydate=$todaydate+60;
            $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
            $interval = new DateInterval( 'P1D'); // 1 Day interval
            $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
            foreach ($period as $day)
            {
            	$key[] = $day->format( 'Y-m-d');
            	
            }
            
            $firstdate=$key[0];
            $lastdate=$key[$todaydate];
            // print_r($firstdate);die;
			$noofdays=date('t');
			$todaydate=date('d');
			$this->data['getdateweek']=$key;
            $this->data['getStudent'] = $this->professor_m->getstudentattendancebyweek($getClasses,$getSubcourse,$getSemester,$getSubject,$firstdate,$lastdate);
		}
		else if($year==4)
		{
			$this->data['dateshow']=180;
			$todaydate=date('d');
			$todaydate=$todaydate-1;
			$todaydate=$todaydate+150;
            $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
            $interval = new DateInterval( 'P1D'); // 1 Day interval
            $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
            foreach ($period as $day)
            {
            	$key[] = $day->format( 'Y-m-d');
            	
            }
            
            $firstdate=$key[0];
            $lastdate=$key[$todaydate];
            // print_r($firstdate);die;
			$noofdays=date('t');
			$todaydate=date('d');
			$this->data['getdateweek']=$key;
            $this->data['getStudent'] = $this->professor_m->getstudentattendancebyweek($getClasses,$getSubcourse,$getSemester,$getSubject,$firstdate,$lastdate);
		}
		else 
		{
			$this->load->library('calendar');
			$this->data['dateshow']=365;
			$year=date('Y');
			$lastyear=$year-1;
			$days=0; 
			for($month=1;$month<=12;$month++)
			{ 
				$totalday=$this->calendar->get_total_days($month,$lastyear);
	            $days=$days+$totalday;
		    }			
			$todaydate= $days;			
            $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
            $interval = new DateInterval( 'P1D'); // 1 Day interval
            $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
            foreach ($period as $day)
            {
            	$key[] = $day->format( 'Y-m-d');
            	
            }
            
            $firstdate=$key[0];
            $lastdate=$key[$todaydate];
            // print_r($firstdate);die;
			$noofdays=date('t');
			$todaydate=date('d');
			$this->data['getdateweek']=$key;
            $this->data['getStudent'] = $this->professor_m->getstudentattendancebyweek($getClasses,$getSubcourse,$getSemester,$getSubject,$firstdate,$lastdate);
		}
		$this->data['subject_name'] = $this->db->where('subjectID',$getSubject)->get('subject')->row();
		$date = $this->input->get('date');
		$date_fordata = date('Y-m-d',strtotime($date));
		//print_r($date);die;
		if($usertype=="Professor")
		{
		    $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
		    // $this->data['department'] = $this->professor_m->get_alldepartment();
		    $this->data['department'] = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
            $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);
           
		}
			
		// print_r($this->data['getStudent']);die;
		
	 	if($usertype=="Professor")
		{

			
				$this->data['professorLecture'] = $this->professor_m->get_professor_data($loginuserID);
			
			$this->data["subject"]=$this->professor_m->getsubjectandtopic($loginuserID);
			 //	print_r($this->data["subject"]);die;
			$this->data["subview"] = "professor/student_attendaancerepost";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			// print_r("expression1");die;
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function debarred_list()
	{
		$language = $this->session->userdata('lang');
		$this->lang->load('debarredlist', $language);
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
	 	$this->data['professor']=$loginuserID;
	 	$getdepartment=$this->input->get('department');
		$getClasses = $this->input->get('course');
		$getSubcourse = $this->input->get('subcourse');
		if($getSubcourse == "")
		{
			$getSubcourse=0;
		}
		$getSemester = $this->input->get('yearsOrSemester');
		$getSubject = $this->input->get('subject');
		$percentage=$this->input->get('percentage');
		$year=$this->input->get('year');
		   
		
		$this->data['subject_name'] = $this->db->where('subjectID',$getSubject)->get('subject')->row();
		$date = $this->input->get('date');
		$date_fordata = date('Y-m-d',strtotime($date));
		if($usertype=="Professor")
		{
			// $departmentID
		    $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
		    // $this->data['department'] = $this->professor_m->get_alldepartment();
		    $this->data['department'] = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
		    // print_r($this->data['department']);die;
            $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);
            $ad=array();
           if(count($this->data['subjects']))
			{
    			$attendance= $this->professor_m->getstudentforpercentage($getClasses,$getSubcourse,$getSemester,$getSubject,$year,$percentage);
    			// print_r($attendance);
    			// exit();
    			if($attendance != 0)
    			{
    				// print_r($attendance);
    				// exit();
    				foreach ($attendance as $key => $valuess) 
			        {
			        	foreach ($valuess as $key => $value)
			        	{
			        		$value = $this->professor_m->getstudentpersentattendance($value['subjectID'],$value['studentID'],$value['yearsOrSemester'],$value['classesID'],$value['year']);
			        	}
			        	// $attendance[$key]['atd']=$value;
			        	$ad[]=$value;
			        }
			        $this->data['getStudent']=$attendance;
		            $this->data['studentattenadnce']=$ad;
    			}
    			else
    			{
    				$this->data['getStudent']=$attendance;
    				$ad=0;
		        	$this->data['studentattenadnce']=$ad;
    			}   			 
		        // print_r($ad);die;
		        
		        // print_r($this->data['studentattenadnce']);die;
    		}
			else
			{
				$this->data['getStudent'] = 0;
			}
		}
		if($usertype=="Professor")
		{
			$this->data["subview"] = "professor/debarred_list";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			// print_r("expression1");die;
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}


}


