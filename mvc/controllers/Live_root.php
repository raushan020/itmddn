<?php if(!defined('BASEPATH')) exit('No direct script access allowed');



class Live extends Admin_Controller {



	function __construct() {

			parent::__construct();

		$this->load->model("classes_m");

		$this->load->model("student_m");

		$this->load->model("section_m");

			$this->load->model("live_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('balance', $language);	

	}







public function index(){

				$this->load->view("live/index");



}

public function audience()
{
	$this->load->view("live/audience");
}



public function table(){

			    $this->data["subview"] = "live/table";
				$this->load->view('_layout_main', $this->data);

}

public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->live_m->get_join_broad_count($adminID,$usertype,$loginuserID);
	$totalFiltered = $totalData;
	$posts = $this->live_m->make_datatables($adminID,$usertype,$loginuserID);
            // $totalFiltered  = $this->student_m->get_filtered_data();
	$av_array = array(1 =>'Video Stream',2=>'Audio Stream');
	$status_array = array(1 =>'Live',2=>'Unpublish');
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				//$nestedData['date']=date('d-m-Y H:i',strtotime($post->createTime));
				//$nestedData['subject'] = $post->subject;
				$nestedData['stream_name'] = $post->stream_name;
                $nestedData['channel_name'] = $post->channel_name;
                $nestedData['av'] = $av_array[$post->av];
                $nestedData['status'] = $status_array[$post->live_status];

   		
   		    $buttons     = btn_view('live/audience/'.$post->streamID.'?channel='.$post->stream_name, 'View').btn_edit('live/edit_stream/'.$post->streamID, $this->lang->line('edit')).btn_delete('live/delete_stream/'.$post->streamID, $this->lang->line('delete'));
 		 $nestedData['action'] = $buttons;

			 
               
                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

      
            
        echo json_encode($json_data); 
}

public function add_stream()
{
	$courseID=$this->uri->segment(3);
	$yearsOrSemester=$this->uri->segment(4);
	$subjectID=$this->uri->segment(5);
	if($_POST)
	{
		$data = array(
				'adminID'=>$this->session->userdata('adminID'),
				'CreatedUsertype'=>$this->session->userdata('usertype'),
				'createdID'=>$this->session->userdata('loginuserID'),
				'stream_name'=>$this->input->post('stream_name'),
				'channel_name'=>time(),
				'av'=>$this->input->post('av'),
				'courseID'=>$courseID,
				'yearsOrSemester'=>$yearsOrSemester,
				'subjectID'=>$subjectID
				);
		$this->db->insert('live_broadcast',$data);
		$lastid=$this->db->insert_id();
		if($courseID)
		{
			$studentlist=$this->db->select('student.studentID,student.name,student.email,student.phone')->from('subject')->join('student','student.classesID=subject.classesID','left')->where(array('subject.classesID' =>$courseID,'subject.yearsOrSemester'=>$yearsOrSemester,'student.classesID' =>$courseID,'student.yearsOrSemester'=>$yearsOrSemester,'subject.subjectID'=>$subjectID,'subject.professorID'=>$this->session->userdata('loginuserID'),'subject.subject_mode'=>0,'subject.status'=>1,'student.status'=>1))->get()->result();
			// print_r($studentlist);die;
			redirect(base_url("live/audience/".$lastid.'?channel='.$this->input->post('stream_name')));
		}
		else
		{
			$this->session->set_flashdata('success',$this->lang->line('menu_success'));
			redirect(base_url("live/table"));
		}
	}
	else
	{
		$this->data["subview"] = "live/add_stream";
		$this->load->view('_layout_main', $this->data);						
	}
}

public function edit_stream(){

$this->data['edit_data']= $this->db->where('streamID',$this->uri->segment(3))->get('live_broadcast')->row();


if($_POST){

$data = array(
'stream_name'=>$this->input->post('stream_name'),
'av'=>$this->input->post('av'),
'live_status'=>$this->input->post('live_status'),
);

$this->db->where('adminID',$this->session->userdata('adminID'));
$this->db->where('streamID',$this->uri->segment(3));
$this->db->update('live_broadcast',$data);

	$this->session->set_flashdata('success',$this->lang->line('menu_success'));
					redirect(base_url("live/table"));


}else{
			    $this->data["subview"] = "live/edit_stream";
				$this->load->view('_layout_main', $this->data);
			
}

}



public function delete_stream(){ 

$this->db->where('streamID',$this->uri->segment(3));
$this->db->delete('live_broadcast');
$this->session->set_flashdata('success',$this->lang->line('menu_success'));
					redirect(base_url("live/table"));

}

}