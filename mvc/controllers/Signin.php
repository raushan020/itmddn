<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Signin extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("signin_m");
        if($this->data["siteinfos"])
        {
		$data = array(

			"lang" => $this->data["siteinfos"]->language,

		);

		$this->session->set_userdata($data);
	}

		$language = $this->session->userdata('lang');

		$this->lang->load('signin', $language);

	}	



	protected function rules() {

		$rules = array(

				 array(

					'field' => 'username',

					'label' => "Username",

					'rules' => 'trim|required|max_length[40]'

				),

				array(

					'field' => 'password',

					'label' => "Password",

					'rules' => 'trim|required|max_length[40]'

				)

			);

		return $rules;

	}



	protected function rules_cpassword() {

		$rules = array(

				array(

					'field' => 'old_password',

					'label' => $this->lang->line('old_password'),

					'rules' => 'trim|required|max_length[40]|min_length[3]|callback_old_password_unique'

				),

				array(

					'field' => 'new_password',

					'label' => $this->lang->line('new_password'),

					'rules' => 'trim|required|max_length[40]|min_length[4]'

				),

				array(

					'field' => 're_password',

					'label' => $this->lang->line('re_password'),

					'rules' => 'trim|required|max_length[40]|min_length[4]|matches[new_password]'

				)

			);

		return $rules;

	}



	public function index() {


		$this->signin_m->loggedin() == FALSE || redirect(base_url('dashboard/index'));

		$this->data['form_validation'] = 'No';

		if($_POST) {

			$rules = $this->rules();

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE) {

				$this->data['form_validation'] = validation_errors();

				$this->data["subview"] = "signin/index";

				$this->load->view('_layout_signin', $this->data);

			} else {

				if($this->signin_m->signin() == TRUE) {
				 
             if (!empty($this->input->get('redirect'))) {
              redirect(base_url($this->input->get('redirect')));
              }else{
					redirect(base_url('dashboard/index'));
              }

				} else {

					$this->session->set_flashdata("errors", "That user does not signin");

					$this->data['form_validation'] = "Your username or password is incorrect";

					$this->data["subview"] = "signin/index";

					$this->load->view('_layout_signin', $this->data);

				}

			}

		} else {

			$this->data["subview"] = "signin/index";
			$this->data["siteinfos"] = $this->site_m->get_site(array('adminID'=>0));
			$this->load->view('_layout_signin', $this->data);

			$this->session->sess_destroy();

		}



	}



	public function cpassword() 
	{
		$username = $this->session->userdata("username");
		$email = $this->session->userdata("email");
        $adminID = $this->session->userdata("adminID");
        
		$this->load->library("session");
		if($_POST) 
		{
            if ($username=='demo') 
            {
                $this->session->set_flashdata('error',"Demo user can not change password");
                redirect(base_url('signin/cpassword'));
            }
            $rules = $this->rules_cpassword();
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) 
            {
                $this->data["subview"] = "signin/cpassword";
                $this->load->view('_layout_main', $this->data);
            } 
            else 
            {
                redirect(base_url('signin/cpassword'));
            }
		} 
		else 
		{
			$this->data["subview"] = "signin/cpassword";
			$this->load->view('_layout_main', $this->data);
		}
	}



	function old_password_unique() {

		if($this->signin_m->change_password() == TRUE) {
            $this->send_mail_password_reset();
			return TRUE;

		} else {

			$this->form_validation->set_message("old_password_unique", "%s does not match");

			return FALSE;

		}

	}
	
	public function send_mail_password_reset()
	{
	    $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        if($usertype) 
        {
        	$dataEamil['username'] = $this->session->userdata("username");
        	$dataEamil['name'] = $this->session->userdata("name");
            $dataEamil['password']=$this->input->post('new_password');
            $subject="New password created for ITM software";
            $html = $this->load->view('emailTemplates/passwordresetmypanel', $dataEamil , true);
            $email=$this->session->userdata("email");
            $fullname=$dataEamil['name'];
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']="";
            $dataEamil['institute']=$this->data["siteinfos"]->sname;           
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);      
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}



	public function signout() {

		$this->signin_m->signout();

		redirect(base_url("signin/index"));

	}

		public function logoutAllDevices() {

		$this->signin_m->logoutOtherDevice();

		$this->session->set_flashdata('success', 'Log out Successfully');
				redirect(base_url("signin/cpassword"));

		redirect(base_url("signin/index"));

	}
	function Verifyemail()
	{
		$uri=$this->uri->segment(4);
		$adminID=$this->signin_m->verifymailbyadminid($uri);
        if($adminID == true)
        {
        	
        	$this->session->set_flashdata('success',"Your Email-ID is Verifyed Successfully");
		    redirect(base_url('signin/index'));
        }
        else
        {
        	$this->session->set_flashdata('success',"Your Email-ID is Already Verifyed");
		    redirect(base_url('signin/index'));
        }
		
	}

}








