<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Viewlecture extends Admin_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model("professor_m");
		$this->load->model("subject_m");
		$this->load->model("classes_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('professor', $language);
	}
	
	public function index() {
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		if($usertype=="Professor")
		{
			if ($usertype=='superadmin') 
			{
				$this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);
			}
			else
			{ 
				//$this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);
				$this->data['professorLecture'] = $this->professor_m->get_professor_data($loginuserID);
			}
			$this->data["subview"] = "viewlecture/index";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function attendance() 
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");   
		$getClasses = $this->input->get('course');
		$getdepartment=$this->input->get('department');
		$getSubcourse = $this->input->get('subcourse');
		if($getSubcourse == "")
		{
			$getSubcourse=0;
		}
		$getSemester = $this->input->get('yearsOrSemester');
		$getSubject = $this->input->get('subject');
		$this->data['subject_name'] = $this->db->where('subjectID',$getSubject)->get('subject')->row();
		$date = $this->input->get('date');
		$date_fordata = date('Y-m-d',strtotime($date));
		$currentyear=$this->session->userdata('sessionyear');
		if($usertype=="Professor")
		{
		    $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
            // $this->data['department'] = $this->professor_m->get_alldepartment();
            $this->data['department'] = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
            $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);
            
			// $progress=$this->subject_m->progress_report($subjectid);
			// print_r($progress);die;
            if($date)
            {
            	// print_r("expression");die;
			    $student_from_atdsd =  $this->professor_m->getstudent_from_atnd($getSubject,$loginuserID,$date_fordata);
			    foreach ($student_from_atdsd as $key => $value) {
			    	$student_from_atdsd[$key]['professorcheckstudentonline']=$this->professor_m->checkstudentattendclass($getSubject,$value['studentID'],$getSemester,$getClasses,$loginuserID);
			    }
			    $this->data['student_from_atd']=$student_from_atdsd;
			    // print_r($this->data['student_from_atd']);die;
			    $this->data['count_students_atd']=  count($this->data['student_from_atd']);
		    }
		    else
		    {
			    $this->data['count_students_atd']=  0;
		    }
			if(count($this->data['subjects']))
			{
    			if ($this->data['count_students_atd']) 
    			{ 
    				// print_r("expression");die;
    			    $this->data['getStudent'] =  $this->data['student_from_atd'];
    			} 
    			else
    			{
    				// print_r("expression123");die;
    				// $checkstudentattendclass=$this->professor_m->checkstudentattendclass($getSubject,$date);
    			    $getStudentss= $this->professor_m->getstudentforconditiononlinecheck($getClasses,$getSubcourse,$getSemester,$getSubject);
    			    foreach ($getStudentss as $key => $value)
    			    {
    			    	$getStudentss[$key]['professorcheckstudentonline']=$this->professor_m->checkstudentattendclass($getSubject,$value['studentID'],$getSemester,$getClasses,$loginuserID);
    			    }	
    			     $this->data['getStudent']=$getStudentss;
    			     // print_r($this->data['getStudent']);die;
    			}
			}
			else
			{
				$this->data['getStudent'] = 0;
			}
			if ($_POST) 
			{
				if($this->data['count_students_atd']==0)
				{
				
				  $topic=$this->input->post('topic');
				  if($topic)
				  {
					$d=count($topic);
					$topicid=implode(", ",array_values($topic));
					for($i=0;$i<$d;$i++)
					{
						
	                   $a=$topic[$i];
	                   $savedata = array('topicid' =>$a,'date'=>date('Y-m-d',strtotime($_POST['date'])));
                       $savetopiconlyforadmin=$this->subject_m->savetopicdatewise($savedata,$a);
	                   $getunit=$this->subject_m->getunit($a);
	                   $unitidArray = json_decode(json_encode($getunit), true); 
	                   $sa[]=$unitidArray;
					}
					$zx=$sa;
					$unitid=json_encode($zx);
					$classid=$_POST['classid'];
					$yearsorsemester=$_POST['yearsorsemester'];
					$subjectid=$_POST['subjectid'];
					$professorid=$_POST['professorid'];
					// $unitid=$_POST['unitid'];
					if($yearsorsemester=="1st_Semester" || $yearsorsemester=="3rd_Semester" || $yearsorsemester=="5th_Semester")
				    {
				       $year_mode=1;
				    }
				    else if($yearsorsemester=="1st_Year" || $yearsorsemester=="2nd_Year" || $yearsorsemester=="3rd_Year")
				    {
				    	$year_mode=3;
				    }
				    else
				    {
				      $year_mode=2;
				    }

					$insertcompletetopic = array('classid' =>$classid,'unitid'=>$unitid,'topicid'=>$topicid,'subjectid'=>$subjectid,'yearsorsemester'=>$yearsorsemester,'year'=>$currentyear,'year_mode'=>$year_mode,'professorID'=>$professorid,'status'=>1,'create_at'=>date('Y-m-d H:i:s'),'created_by'=>$professorid);
					$updatecompletetopic= array('unitid'=>$unitid,'topicid'=>$topicid,'professorID'=>$professorid,'modified_at'=>date('Y-m-d H:i:s'),'modified_by'=>$professorid,'year_mode'=>$year_mode);
					$subjectidbyid=$this->professor_m->checksubjectid($subjectid,$currentyear,$year_mode);
	                if($subjectidbyid)
	                {
	                	$this->professor_m->updatecompletetopic($updatecompletetopic,$subjectid,$currentyear,$year_mode);
	                }
	                else
	                {
	                	$this->professor_m->insertcompletetopic($insertcompletetopic);
	                }
	              }
               }
				

				// print_r($topic);die();
			 	$current_date = date('d-m-Y');
			 	if (strtotime($_POST['date']) <=strtotime($current_date)) 
			 	{
			 		
			 		if($_POST['yearsOrSemester']=="1st_Semester" || $_POST['yearsOrSemester']=="3rd_Semester" || $_POST['yearsOrSemester']=="5th_Semester")
				    {
				       $year_mode=1;
				    }
				    else if($_POST['yearsOrSemester']=="1st_Year" || $_POST['yearsOrSemester']=="2nd_Year" || $_POST['yearsOrSemester']=="3rd_Year")
				    {
				    	$year_mode=3;
				    }
				    else
				    {
				      $year_mode=2;
				    }
			 	    $array = array();
			 		for ($i=0; $i <count($_POST['studentID']) ; $i++) 
			 		{ 
			 			$arraykeyID = $_POST['keyIDs'][$i];
    			 	    $array['studentID'] = $_POST['studentID'][$i];
    			 	    $array['classesID'] =  $_POST['classesID'];
    			 	    $array['sub_coursesID'] =  $_POST['sub_coursesID'];
    			 	    $array['yearsOrSemester'] =  $_POST['yearsOrSemester'];
    			 	    $array['subjectID'] =  $_POST['subjectID'];
    					$array['professorID'] = $loginuserID;
    					$array['atd'] = $_POST['atd'.$arraykeyID];
    					$array['year']=$currentyear;
    					$array['year_mode']=$year_mode;
					    if ($this->data['count_students_atd']) 
					    {
				            //$array['atd_update_date'] = $_POST['date'];
						    $array['atd_date'] =$date_fordata;
						    $this->db->where('atdID',$_POST['atdID'][$i]);
						    $this->db->update('attendance_student',$array);
					    }
					    else
					    {
					        $array['atd_date'] =$date_fordata;
				            $this->professor_m->insert_attendance($array);
					    }
					    $this->db->where('studentID',$_POST['studentID'][$i]);
			            $this->db->where('classesID',$_POST['classesID']);
			            $this->db->where('yearsOrSemester',$_POST['yearsOrSemester']);
			            $this->db->where('subjectID',$_POST['subjectID']);
			            $this->db->where('professorID',$loginuserID);
			            $this->db->where('year_mode',5);
			            $this->db->where('atd_date',$date_fordata);
			            $this->db->delete('attendance_student');
			 		}
				    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
				    redirect(base_url("viewlecture/attendance?type=".$_POST['type']."&department=".$getdepartment."&course=".$_POST['classesID']."&subcourse=".$_POST['sub_coursesID']."&yearsOrSemester=".$_POST['yearsOrSemester']."&subject=".$_POST['subjectID']."&date=".$_POST['date'].""));
			    }
			    else
			    {
				    $this->session->set_flashdata('error', 'Please Select Present Date');
				    redirect(base_url("viewlecture/index"));
			    }
			}
			else
			{
			    $this->data["subview"] = "viewlecture/attendance";
			    $this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "viewlecture/attendance";
			$this->load->view('_layout_main', $this->data);
		}
	}




	


	

	//Praveen Pathak

	

	function ajaxProfessors(){



		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$loginuserID = $this->session->userdata("loginuserID");

		$totalData = $this->professor_m->get_professor_no($adminID);

		$totalFiltered = $totalData;

		$posts = $this->professor_m->make_datatables();

        $data = array();

        if(!empty($posts))

        {

        	$i = 1;

            foreach ($posts as $post)

            {

				

				

				$nestedData['sn'] = '';

				$nestedData['name'] = $post->name;

				$nestedData['username'] = $post->username;

				$nestedData['email'] = $post->email;

			  if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {



			   $buttons     =  btn_view('professor/view/'.$post->professorID, $this->lang->line('view')).btn_edit('professor/edit/'.$post->professorID, $this->lang->line('edit')).btn_delete('professor/delete/'.$post->professorID, $this->lang->line('delete'));



			   $nestedData['action'] = $buttons;



			  }else{



			   $buttons     =  btn_view('professor/view/'.$post->professorID, $this->lang->line('view'));



			   $nestedData['action'] = $buttons;

			  }



                $data[] = $nestedData;



            }

        }



       $json_data = array(

                    "draw"            => intval($this->input->post('draw')),  

                    "recordsTotal"    => $totalData,  

                    "recordsFiltered" => $totalFiltered, 

                    "data"            => $data   

                    );

            

        echo json_encode($json_data); 



}

public function subject_get(){
	
$subject = $this->subject_m->get_subject_proffessor($this->input->post('classesID'),$sub_coursesID=0,$this->input->post('val'),$this->session->userdata('loginuserID'));

  foreach ($subject as $key => $value) {
echo '<option value="'.$value->subjectID.'">'.$value->subject.'</option>';
                                       
}
}





	protected function rules() {



		$rules = array(



			array(



				'field' => 'name', 



				'label' => $this->lang->line("professor_name"), 



				'rules' => 'trim|required|xss_clean|max_length[60]'



			), 



			array(



				'field' => 'sex', 



				'label' => $this->lang->line("professor_sex"), 



				'rules' => 'trim|required|xss_clean'



			),



			// array(



			// 	'field' => 'religion', 



			// 	'label' => $this->lang->line("teacher_religion"), 



			// 	'rules' => 'trim|max_length[25]|xss_clean'



			// ),



			array(



				'field' => 'email', 



				'label' => $this->lang->line("professor_email"), 



				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'



			),



			array(



				'field' => 'phone', 



				'label' => $this->lang->line("professor_phone"), 



				'rules' => 'trim|min_length[5]|max_length[25]|xss_clean|required'



			),



			array(



				'field' => 'username', 



				'label' => $this->lang->line("professor_username"), 



				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'



			),



			array(



				'field' => 'password',



				'label' => $this->lang->line("professor_password"), 



				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_password'



			)



		);



		return $rules;



	}







	function insert_with_image($username) {



	    $random = rand(1, 10000000000000000);



	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));



	    return $makeRandom;



	}







	public function add() {



		$usertype = $this->session->userdata("usertype");

        $adminID = $this->session->userdata("adminID");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "superadmin"){



			if($_POST) {



				$rules = $this->rules();



				$this->form_validation->set_rules($rules);



				if ($this->form_validation->run() == FALSE) {



					$this->data['form_validation'] = validation_errors(); 



					$this->data["subview"] = "professor/add";



					$this->load->view('_layout_main', $this->data);			



				} else {



					$array = array();



					$array['name'] = $this->input->post("name");



					$array['designation'] = $this->input->post("designation");



					$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));



					$array["sex"] = $this->input->post("sex");



					$array['religion'] = $this->input->post("religion");



					$array['email'] = $this->input->post("email");



					$array['phone'] = $this->input->post("phone");



					$array['address'] = $this->input->post("address");

					$array['adminID'] = $adminID;



					$array['jod'] = date("Y-m-d", strtotime($this->input->post("jod")));



					$array['username'] = $this->input->post("username");



					$array['password'] = $this->professor_m->hash($this->input->post("password"));



					$array['usertype'] = "Professor";



					$array["create_date"] = date("Y-m-d h:i:s");



					$array["modify_date"] = date("Y-m-d h:i:s");



					$array["create_userID"] = $this->session->userdata('loginuserID');



					$array["create_username"] = $this->session->userdata('username');



					$array["create_usertype"] = $this->session->userdata('usertype');



					$array["professoractive"] = 1;

			        

			        $new_file = "defualt.png";

						$array["photo"] = $new_file;

						$this->professor_m->insert_professor($array);

						// $this->send_mail_new_regitration();

						$this->session->set_flashdata('success', $this->lang->line('menu_success'));

						redirect(base_url("professor/index"));

					



				}



			} else {



				$this->data["subview"] = "professor/add";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}



	public function view() {



		$usertype = $this->session->userdata('usertype');



		if ($usertype) {



			$id = htmlentities(($this->uri->segment(3)));



			if ((int)$id) {



				$this->data['professor'] = $this->professor_m->get_professor($id);



				if($this->data['professor']) {



					$this->data["subview"] = "professor/view";



					$this->load->view('_layout_main', $this->data);



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function edit() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Professor"){



			$id = htmlentities(($this->uri->segment(3)));



			if((int)$id) {



				$this->data['professor'] = $this->professor_m->get_professor($id);



				if($this->data['professor']) {



					if($_POST) {



						$rules = $this->rules();



				

						unset($rules[4],$rules[5]);



						$this->form_validation->set_rules($rules);



						if ($this->form_validation->run() == FALSE) { 



							$this->data["subview"] = "professor/edit";



							$this->load->view('_layout_main', $this->data);



						} else {



							$array = array();



							$array['name'] = $this->input->post("name");

							$array["sex"] = $this->input->post("sex");



							$array['email'] = $this->input->post("email");



							$array['phone'] = $this->input->post("phone");



							$array["modify_date"] = date("Y-m-d h:i:s");



	



								$this->professor_m->update_professor($array, $id);



								$this->session->set_flashdata('success', $this->lang->line('menu_success'));



								redirect(base_url("professor/index"));



						



						}



					} else {



						$this->data["subview"] = "professor/edit";



						$this->load->view('_layout_main', $this->data);



					}



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function delete() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"){



			$id = htmlentities(($this->uri->segment(3)));



			if((int)$id) {



				$this->data['professor'] = $this->professor_m->get_professor($id);



				if($this->data['professor']) {



					if($this->data['professor']->photo != 'defualt.png') {



						unlink(FCPATH.'uploads/images/'.$this->data['professor']->photo);



					}



					$this->professor_m->delete_professor($id);



					$this->session->set_flashdata('success', $this->lang->line('menu_success'));



					redirect(base_url("professor/index"));



				} else {



					redirect(base_url("professor/index"));



				}



			} else {



				redirect(base_url("professor/index"));



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function lol_username() {



		$id = htmlentities(($this->uri->segment(3)));



		if((int)$id) {



			$professor_info = $this->user_m->get_single_user(array('professorID' => $id));



			$tables = array('student' => 'student', 'parent' => 'parent', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $professor_info->email));



				if(count($user)) {



					$this->form_validation->set_message("lol_username", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}



			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		} else {



			$tables = array('student' => 'student', 'parent' => 'parent', 'professor' => 'professor', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("username" => $this->input->post('username')));



				if(count($user)) {



					$this->form_validation->set_message("lol_username", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}







			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		}			



	}







	public function date_valid($date) {



		if(strlen($date) <10) {



			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");



	     	return FALSE;



		} else {



	   		$arr = explode("-", $date);   



	        $dd = $arr[0];            



	        $mm = $arr[1];              



	        $yyyy = $arr[2];



	      	if(checkdate($mm, $dd, $yyyy)) {



	      		return TRUE;



	      	} else {



	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");



	     		return FALSE;



	      	}



	    } 



	} 







	public function print_preview() {



		$id = htmlentities(($this->uri->segment(3)));



		if ((int)$id) {



			$usertype = $this->session->userdata('usertype');



			if ($usertype == "Admin") {



			    $this->load->library('html2pdf');



			    $this->html2pdf->folder('./assets/pdfs/');



			    $this->html2pdf->filename('Report.pdf');



			    $this->html2pdf->paper('a4', 'portrait');







				$this->data["professor"] = $this->professor_m->get_professor($id);



				if($this->data["professor"]) {



					$this->data['panel_title'] = $this->lang->line('panel_title');



					$html = $this->load->view('professor/print_preview', $this->data, true);



					$this->html2pdf->html($html);



					$this->html2pdf->create();



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function send_mail() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin") {



			$id = $this->input->post('id');



			if ((int)$id) {



				$this->load->library('html2pdf');



			    $this->html2pdf->folder('uploads/report');



			    $this->html2pdf->filename('Report.pdf');



			    $this->html2pdf->paper('a4', 'portrait');







				$this->data["professor"] = $this->professor_m->get_professor($id);



				if($this->data["professor"]) {



					$this->data['panel_title'] = $this->lang->line('panel_title');



					$html = $this->load->view('professor/print_preview', $this->data, true);



					$this->html2pdf->html($html);



					$this->html2pdf->create('save');



					



					if($path = $this->html2pdf->create('save')) {



					$this->load->library('email');



					$this->email->set_mailtype("html");



					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);



					$this->email->to($this->input->post('to'));



					$this->email->subject($this->input->post('subject'));



					$this->email->message($this->input->post('message'));	



					$this->email->attach($path);



						if($this->email->send()) {



							$this->session->set_flashdata('success', $this->lang->line('mail_success'));



						} else {



							$this->session->set_flashdata('error', $this->lang->line('mail_error'));



						}



					}



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function unique_email() {



		$id = htmlentities(($this->uri->segment(3)));



		if((int)$id) {



			$professor_info = $this->professor_m->get_single_professor(array('professorID' => $id));



			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $professor_info->username));



				if(count($user)) {



					$this->form_validation->set_message("unique_email", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}



			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		} else {



			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("email" => $this->input->post('email')));



				if(count($user)) {



					$this->form_validation->set_message("unique_email", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}







			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		}	



	}









function AddUsernamePass(){

 

$password = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) ); // random(ish) 5 digit int



$username = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string



$passwordPlusName = strtok($this->input->post('name_id')," ").'@'.$password;



$usernamePlusName = strtok($this->input->post('name_id')," ").'@'.$username;



$array = array(

	'password'=>$passwordPlusName,

	'username'=>$usernamePlusName

);



 echo  json_encode(($array));



}







// public function send_mail_new_regitration() {

// 		$usertype = $this->session->userdata("usertype");

// 		$loginuserID = $this->session->userdata("loginuserID");

// 		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'Professor') {

//      $username  = $this->input->post('username');

//          $this->db->select('*');

//         $this->db->where('username',$username);

//         $this->db->from('teacher');

//       $query2  = $this->db->get();



//       $dataEmail['emailData'] = $query2->row();



// $array = array("src" => base_url('uploads/images/'.$this->data["siteinfos"]->photo),'class' => 'img-rounded');



// $dataEmail['imgData'] = $array;



// //  print_r($dataEmail['emailData']);

// // exit();

// $this->load->library('email');

// $config=array(

// 'charset'=>'utf-8',

// 'wordwrap'=> TRUE,

// 'mailtype' => 'html'

// );



//                     $this->email->initialize($config);

// 					$html = $this->load->view('emailTemplates/newRegisterTeacher', $dataEmail , true);

				 

// 					$this->email->set_mailtype("html");

// 					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

// 					$this->email->to($dataEmail['emailData']->email);

// 					$this->email->subject("Welecome to Distance Education School Counselor Panel for SVSU");

// 					$this->email->message($html);

// 						if($this->email->send()) {

// 							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

// 						} else {

// 							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

// 						}

// 		} else {

// 			$this->data["subview"] = "error";

// 			$this->load->view('_layout_main', $this->data);

// 		}

// 	}





	function active() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin") {



			$id = $this->input->post('id');



			$status = $this->input->post('status');



			if($id != '' && $status != '') {



				if((int)$id) {



					if($status == 'chacked') {



						$this->professor_m->update_professor(array('professoractive' => 1), $id);



						echo 'Success';



					} elseif($status == 'unchacked') {



						$this->professor_m->update_professor(array('professoractive' => 0), $id);



						echo 'Success';



					} else {



						echo "Error";



					}



				} else {



					echo "Error";



				}



			} else {



				echo "Error";



			}



		} else {



			echo "Error";



		}



	}

	public function subjectprogress()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$professor=$this->uri->segment(3);
		$getdepartment=$this->input->get('department');
		$getSubcourse = $this->input->get('subcourse');
		$getSemester = $this->input->get('yearsOrSemester');
		$getSubject = $this->input->get('subject');
		$year=$this->input->get('year');
		$mode=$this->input->get('mode');
		$getClasses = $mode;
		if($professor)
		{
			$loginuserID=$professor;
			$this->data['professor']=$loginuserID;
		}
		else
		{
	 	   $loginuserID = $this->session->userdata("loginuserID");
	 	   $this->data['professor']=$loginuserID;
	    }
		   
		
		$this->data['subject_name'] = $this->db->where('subjectID',$getSubject)->get('subject')->row();
		$date = $this->input->get('date');
		$date_fordata = date('Y-m-d',strtotime($date));
		if($usertype=="Professor" || $usertype=="ClgAdmin" || $usertype=="Admin" || $usertype == "HOD")
		{
		    $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
		    // $this->data['department'] = $this->professor_m->get_alldepartment();
		    $this->data['department'] = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
            $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);
        }

		
	    // print_r("expression");die;
		if($usertype=="Professor" || $usertype=="ClgAdmin" || $usertype=="Admin" || $usertype == "HOD")
		{

			if ($usertype=='superadmin' || $usertype=="ClgAdmin" || $usertype == "HOD") 
			{
				$this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);
			}
			else
			{ 
				//$this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);
				$this->data['professorLecture'] = $this->professor_m->get_professor_data($loginuserID);
			}
			if($year != 0)
			{
				if($mode==13)
				{
					$this->data["subject"]=$this->professor_m->getsubjectandtopicyearwise($loginuserID,$year,$getSemester);
				}
				else
				{
					$this->data["subject"]=$this->professor_m->getsubjectandtopic($loginuserID,$year,$getSemester);
				}
				$this->data["subjectyear"]=0;
				
			}
			else
			{
				$currentmonth=date('m');
				$currentyear=$this->session->userdata('sessionyear');
				$this->data["subject"]=$this->professor_m->getsubjectandtopic($loginuserID);
				$year=$currentyear;
					$this->data["subjectyear"]=$this->professor_m->getsubjectandtopicyearwiseloadpage($loginuserID,$year);
			}
			
			// print_r($this->data["subject"]);die;
			$this->data["subview"] = "professor/subjectprogress";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			// print_r("expression1");die;
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
       // print_r("expression");die;
	}
	public function viewattendance()
	{
        $usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
	 	$this->data['professor']=$loginuserID;
	 	$getdepartment=$this->input->get('department');
		$getClasses = $this->input->get('course');
		$getSubcourse = $this->input->get('subcourse');
		if($getSubcourse == "")
		{
			$getSubcourse=0;
		}
		$getSemester = $this->input->get('yearsOrSemester');
		$getSubject = $this->input->get('subject');
		$year=$this->input->get('year');
		   
		
		$this->data['subject_name'] = $this->db->where('subjectID',$getSubject)->get('subject')->row();
		$date = $this->input->get('date');
		$date_fordata = date('Y-m-d',strtotime($date));
		if($usertype=="Professor")
		{
		    $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
		    // $this->data['department'] = $this->professor_m->get_alldepartment();
		    $this->data['department'] = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
            $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);
            
			// $progress=$this->subject_m->progress_report($subjectid);
			// print_r($progress);die;
            if($date)
            {
			    $this->data['student_from_atd'] =  $this->professor_m->getstudent_from_atnd($getSubject,$loginuserID,$date_fordata);
			    $this->data['count_students_atd']=  count($this->data['student_from_atd']);
			   
		    }
		    else
		    {
			    $this->data['count_students_atd']=  0;
		    }
			if(count($this->data['subjects']))
			{
    			if ($this->data['count_students_atd']) 
    			{ 
    			    $this->data['getStudent'] =  $this->data['student_from_atd'];
    			} 
    			else
    			{
    			    $this->data['getStudent'] = $this->professor_m->getstudentforprogress($getClasses,$getSubcourse,$getSemester,$getSubject,$year);
    			   	
    			}
			}
			else
			{
				$this->data['getStudent'] = 0;
			}
		}
			
		// print_r($this->data['getStudent']);die;
		
	 	if($usertype=="Professor")
		{

			
				$this->data['professorLecture'] = $this->professor_m->get_professor_data($loginuserID);
			
			$this->data["subject"]=$this->professor_m->getsubjectandtopic($loginuserID);
			// print_r($this->data["subject"]);die;
			$this->data["subview"] = "professor/viewattendance";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			// print_r("expression1");die;
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function ebook()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
	 	$this->data['professor']=$loginuserID;
	 	$getdepartment=$this->input->get('department');
		$getClasses = $this->input->get('course');
		$getSubcourse = 0;
		$getSemester = $this->input->get('yearsOrSemester');

		if($usertype=="Professor" || $usertype=="Admin" || $usertype=="ClgAdmin")
		{
		    // $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
		    $this->data['department'] = $this->professor_m->get_alldepartment();
		    $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_ebookproffessor($getClasses,$getSubcourse,$getSemester);
            if(count($this->data['subjects']))
			{
    			$this->data['getStudent']=$this->data['subjects'];    			
			}
			else
			{
			
				$this->data["getStudent"]=$this->subject_m->getsubjectebook();
			}
		}
		if($usertype=="Professor" || $usertype=="Admin" || $usertype=="ClgAdmin")
		{			
			// print_r($this->data["getStudent"]);die;
			$this->data["subview"] = "professor/ebook";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function requesttoattendclass()
	{
		$time=$this->input->post('checktime');
		$studentID=$this->input->post('studentID');
		$classid=$this->input->post('course_id');
		$yearsorsemester=$this->input->post('yearsOrSemester');
		$atd_date=date('Y-m-d');
		$subjectID=$this->input->post('subjectID');
		$professorID=$this->input->post('professorID');
		$year=date('Y');
		$year_mode=5;
		$activestatus=1;
		$alldata= array('classesID' =>$classid,'sub_coursesID'=>0,'yearsOrSemester'=>$yearsorsemester,'studentID'=>$studentID,'atd_date'=>$atd_date,'subjectID'=>$subjectID,'professorID'=>$professorID,'year'=>$year,'year_mode'=>$year_mode,'activestatus'=>$activestatus);
		// print_r($studentID);die;
		$checkdata=$this->student_m->checkrequestattend($year_mode,$alldata,$studentID);
		if($checkdata)
		{
			return $checkdata;
		}
		else
		{
			$updatestudent=$this->student_m->requesttoattend($year_mode,$alldata,$studentID);
			return $updatestudent;
		}
		
	}
	public function requesttoofline()
	{
		$loginuserID = $this->session->userdata("loginuserID");
		$logouttime=$this->input->post('logouttime');
		$updatestudent=$this->student_m->requesttooflinestudent($loginuserID);
		return $updatestudent;
	}
	function lectureactive() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Professor") {

			$id = $this->input->post('id');

			$status = $this->input->post('status');

			if($id != '' && $status != '') {

				if((int)$id) {

					if($status == 'chacked') {

						$this->professor_m->update_user(array('status' => 0), $id);

						echo 'Success';

					} elseif($status == 'unchacked') {

						$this->professor_m->update_user(array('status' => 1), $id);

						echo 'Success';

					} else {

						echo "Errors";

					}

				} else {

					echo "Errorss";

				}

			} else {

				echo "Errorsss";

			}

		} else {

			echo "Errorssss";

		}

	}




}







/* End of file teacher.php */



/* Location: .//D/xampp/htdocs/school/mvc/controllers/teacher.php */