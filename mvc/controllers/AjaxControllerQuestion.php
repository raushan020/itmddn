<?php

class AjaxControllerQuestion extends Admin_Controller {

  function __construct() {

parent::__construct();

$this->load->library("session");
$this->load->helper('language');
$this->load->helper('form');
$this->load->database();
$this->load->model('subject_m');
$this->load->model('setting_m');
$this->load->model('classes_m');
$this->load->model('exam_m');
    $language = $this->session->userdata('lang');

    $this->lang->load('subject', $language);

}


function Get_subCourses(){

     $id = $this->input->post('id');

     $this->db->where('classesID',$id);
     $this->db->where('status',1);
     $getSubcourses =  $this->db->get('sub_courses');

     if ($getSubcourses->result_array()) {

echo "<div class='form-group'>
              <label for='subCourse' class='col-md-2 col-form-label'>SubCourse</label>
               <div class='col-md-4'>
               <select id='sub_CourseIDExcel' required class='form-control' name = 'sub_coursesID'>";
              echo "<option value = ''>Select Sub Course</option>";
     foreach ($getSubcourses->result_array() as $key => $value) {
    echo "<option value =".$value['sub_coursesID'].">".$value['sub_course']."</option>";
}
      echo "</select>
               </div>";
}
     $id = $this->input->post('id');
     $classesRow = $this->classes_m->get_single_classes($id);
     if ($classesRow) {
               echo "<label for='semester' class='col-md-2 col-form-label'>Semester</label>
               <div class='col-md-4'>";
                 $looping    =  (int) $classesRow->duration;
   if ($classesRow->mode==1) {
     echo"<select class='form-control' required name='yearsOrSemester' onchange='Getsubjects($(this).val())' id='semester_idSelect' value='set_value('yearId');'>";
     echo "<option value = ''>Select</option>";
    for ($i=1; $i <=$looping; $i++) {
   echo "<option value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
   }
   echo  "</select>";
}

else{
  
     echo"<select class='form-control' name='yearsOrSemester' required onchange='Getsubjects($(this).val())' id='semester_idSelect' value='<?php echo set_value('semesterId'); ?>'>";
     echo "<option value = ''>Select</option>";
    for ($i=1; $i <=(2*$looping); $i++) {
   echo "<option value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
   }
   echo  "</select>";
   }

              echo"</div>
            </div>";
           }
}


function Getsubjecteithmarks(){
$adminID = $this->session->userdata("adminID");
$subject = $this->exam_m->Getsubjects($adminID, $this->input->post('classesID'),$this->input->post('semester_idSelect'));
  echo "<label for='subject' class='col-md-2 col-form-label'>Subject</label>
                 <div class='col-md-4'>
               <select class ='form-control' required id = 'subject_id' name = 'subjectID'>
               <option value = ''>Select Subject</option>";
               foreach ($subject as $key => $value) { 
                echo "<option value ='".$value->subjectID."'>".$value->subject."</option>";
                }
              echo "</select>
                 </div>
                ";
}

function Getsubjects(){
$adminID = $this->session->userdata("adminID");
$subject = $this->exam_m->Getsubjects($adminID, $this->input->post('classesID'),$this->input->post('semester_idSelect'));
  echo "<label for='subject' class='col-md-2 col-form-label'>Subject</label>
                 <div class='col-md-4'>
               <select class ='form-control' required onchange= 'AddQuizName($(this).val())' id = 'subject_id' name = 'subjectID'>
               <option value = ''>Select Subject</option>";
               foreach ($subject as $key => $value) { 
                echo "<option value ='".$value->subjectID."'>".$value->subject."</option>";
                }
              echo "</select>
                 </div>
                <label for='exam' class='col-md-2 col-form-label'>Exam</label>
                 <div class='col-md-4'>
                    <input type='text' id = 'quizID' required name = 'quiz_name' />
                 </div>";
}
public function Getunits()
{
  $subjectID=$this->input->post('subjectID');
  $units=$this->subject_m->getunitsname($subjectID);
  if($units)
  { 
    echo $this->session->userdata('FilterVideounitID'); 
     echo "<option value = ''>Select Unit</option>";
     foreach($units as $units)
      {
        if ($units->unitID===$this->session->userdata('FilterVideounitID')) {
                           $selected =  "Selected";
                       }else{
                         $selected =  "";
                       }
        echo "<option value='".$units->unitID."' ".$selected."'>".$units->unit_name."</option>";
      }
  }
  else
  {
    echo $this->session->userdata('FilterVideounitID');
    echo "<option value='0'>No Data Found</option>";
  }
}



}

 ?>