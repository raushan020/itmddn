<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courseware extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("professor_m");
		$this->load->model("subject_m");
		$this->load->model("classes_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('professor', $language);
	}


    public function index()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$professor=$this->uri->segment(3);
		$getdepartment=$this->input->get('department');
		$getSubcourse = $this->input->get('subcourse');
		$getSemester = $this->input->get('yearsOrSemester');
		$getSubject = $this->input->get('subject');
		$year=$this->input->get('year');
		$mode=$this->input->get('mode');
		$getClasses = $mode;
		if($professor)
		{
			$loginuserID=$professor;
			$this->data['professor']=$loginuserID;
		}
		else
		{
	 	   $loginuserID = $this->session->userdata("loginuserID");
	 	   $this->data['professor']=$loginuserID;
	    }
		   
		
		$this->data['subject_name'] = $this->db->where('subjectID',$getSubject)->get('subject')->row();
		$date = $this->input->get('date');
		$date_fordata = date('Y-m-d',strtotime($date));
		if($usertype=="Professor" || $usertype=="ClgAdmin" || $usertype=="Admin" || $usertype == "HOD")
		{
		    $this->data['professor_details']  = $this->professor_m->get_single_professor(array("professorID"=>$loginuserID));
		    // $this->data['department'] = $this->professor_m->get_alldepartment();
		    $this->data['department'] = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
            $this->data['classes'] = $this->professor_m->get_classes($getdepartment);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);	
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);
        }

		
	    // print_r("expression");die;
		if($usertype=="Professor" || $usertype=="ClgAdmin" || $usertype=="Admin" || $usertype == "HOD")
		{

			if ($usertype=='superadmin' || $usertype=="ClgAdmin" || $usertype == "HOD") 
			{
				$this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);
			}
			else
			{ 
				//$this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);
				$this->data['professorLecture'] = $this->professor_m->get_professor_data($loginuserID);
			}
			if($year != 0)
			{
				if($mode==13)
				{
					$this->data["subject"]=$this->professor_m->getsubjectandtopicyearwise($loginuserID,$year,$getSemester);
				}
				else
				{
					$this->data["subject"]=$this->professor_m->getsubjectandtopic($loginuserID,$year,$getSemester);
				}
				$this->data["subjectyear"]=0;
				
			}
			else
			{
				$currentmonth=date('m');
				$currentyear=date('Y');
				$this->data["subject"]=$this->professor_m->getsubjectandtopic($loginuserID);
				if($currentmonth <= 6)
				{
					$year=$currentyear-1;
					$this->data["subjectyear"]=$this->professor_m->getsubjectandtopicyearwiseloadpage($loginuserID,$year);
				}
				else
				{
					$year=$currentyear;
					$this->data["subjectyear"]=$this->professor_m->getsubjectandtopicyearwiseloadpage($loginuserID,$year);
				}
				// print_r($this->data["subjectyear"]);die;
				
			}
			
			// print_r($this->data["subject"]);die;
			$this->data["subview"] = "courseware/index";
			$this->load->view('_layout_main', $this->data);
		} 
		else 
		{
			// print_r("expression1");die;
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
       // print_r("expression");die;
	}
}
?>