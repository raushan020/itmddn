<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model('admin_m');

		$this->load->model("dashboard_m");

		$this->load->model("automation_shudulu_m");

		$this->load->model("automation_rec_m");

		$this->load->model("setting_m");

		$this->load->model("notice_m");

		$this->load->model("etsexam_m");

		$this->load->model("user_m");

		$this->load->model("student_m");

		$this->load->model("classes_m");

		$this->load->model("teacher_m");

		$this->load->model("invoice_gen_m");

		$this->load->model("parentes_m");

		$this->load->model("sattendance_m");

		$this->load->model("subject_m");

		$this->load->model("feetype_m");

		$this->load->model("invoice_m");

		$this->load->model("exam_m");

		$this->load->model("expense_m");

		$this->load->model("payment_m");

		$this->load->model("lmember_m");

		$this->load->model("book_m");

		$this->load->model("issue_m");

		$this->load->model("student_info_m");

		$this->load->model('hmember_m');

		$this->load->model('tmember_m');

		$this->load->model('professor_m');

		$this->load->model('event_m');

		$this->load->model('holiday_m');

		$this->load->model('visitorinfo_m');

		$language = $this->session->userdata('lang');

		$this->lang->load('dashboard', $language);
	}

	public function index() 
	{

		$usertype = $this->session->userdata('usertype');
		$day = abs(date('d'));
		$monthyear = date('m-Y');
	
		if($usertype == "superadmin") 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
	// print_r($this->data['user']);
	// exit();
			//$this->data['notices'] = $this->notice_m->notice_icon_count($loginuserID);
            $callAllNotice  = $this->notice_m->notic_record_admin(array('notice.adminID' => $adminID));
// print_r($callAllNotice);
// exit();
            if ($callAllNotice) {           	
         
				foreach($callAllNotice as $key => $value) 
				{
					if($value['usertype']=='Professor')
					{					 
					   $callAllNotice[$key]['name'] = "Professor";
						$callAllNotice[$key]['bysend'] = "Professor";

					}
					elseif($value['usertype']=='ClgAdmin')
					{
						$callAllNotice[$key]['name'] = "Director";
						$callAllNotice[$key]['bysend'] = "Director";	
					}
					elseif ($value['usertype']=='Accountant') 
					{
						$callAllNotice[$key]['name'] = "Accountant";
						$callAllNotice[$key]['bysend'] = "Accountant1";
					}
					elseif ($value['usertype']=='Admin') 
					{
						$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
						$callAllNotice[$key]['name'] = $name->name;
						$callAllNotice[$key]['bysend'] = "Admin";
					}
				}
			}
			$this->data['notices']  = $callAllNotice;
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();
          	$this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			// $this->data['student'] = $this->student_m->get_student_by_superAdmin();
			// $this->data['teacher'] = $this->teacher_m->get_teacher();
			// $this->data['parents'] = $this->parentes_m->get_parentes();
			// $this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			// $this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			// $this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		if($usertype == "ClgAdmin" || $usertype == "Support") 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			$loginuserID = $this->session->userdata('loginuserID');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
	// print_r($this->data['user']);
	// exit();
			//$this->data['notices'] = $this->notice_m->notice_icon_count($loginuserID);
            $callAllNotice  = $this->notice_m->notic_record_admin(array('notice.adminID' => $adminID));
// print_r($callAllNotice);
// exit();
            if ($callAllNotice) {           	
         
				foreach($callAllNotice as $key => $value) 
				{
					if($value['usertype']=='Professor')
					{					 
					   $callAllNotice[$key]['name'] = "Professor";
						$callAllNotice[$key]['bysend'] = "Professor";

					}
					elseif($value['usertype']=='ClgAdmin')
					{
						$callAllNotice[$key]['name'] = "Director";
						$callAllNotice[$key]['bysend'] = "Director";	
					}
					elseif ($value['usertype']=='Accountant') 
					{
						$callAllNotice[$key]['name'] = "Accountant";
						$callAllNotice[$key]['bysend'] = "Accountant1";
					}
					elseif ($value['usertype']=='Admin') 
					{
						$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
						$callAllNotice[$key]['name'] = $name->name;
						$callAllNotice[$key]['bysend'] = "Admin";
					}
					elseif ($value['usertype']=='Senior_Examiner') 
					{
						$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
						$callAllNotice[$key]['name'] = $name->name;
					}
				}
			}
			$this->data['notices']  = $callAllNotice;
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();
          	$this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		if($usertype == "Accountant" ) 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
	
			$this->data['user'] = $this->admin_m->get_admin_data(array('username'  => $username)); 
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();
          	$this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
          	$this->data['notices']  = $this->notice_m->get_notice_Acc(array('userID'=>$this->data['user']->userID));
          
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
		
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		elseif($usertype == "Admin") 
		{
			$adminID = $this->session->userdata("adminID");
			$userID = $this->session->userdata("loginuserID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

            $callAllNotice  = $this->notice_m->notic_record_admin(array('notice.adminID' => $adminID));
            // print_r($this->db->last_query());die;
			/*print_r($callAllNotice);
			exit(); */
            foreach($callAllNotice as $key => $value) 
			{
				if($value['usertype']=='Professor')
				{					 
				   $callAllNotice[$key]['name'] = "Professor";
					$callAllNotice[$key]['bysend'] = "Professor";

				}
				elseif($value['usertype']=='ClgAdmin')
				{
					$callAllNotice[$key]['name'] = "Director";
					$callAllNotice[$key]['bysend'] = "Director";	
				}
				elseif ($value['usertype']=='Accountant') 
				{
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant1";
				}
				elseif ($value['usertype']=='Admin') 
				{
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
				elseif ($value['usertype']=='Senior_Examiner') 
					{
						$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
						$callAllNotice[$key]['name'] = $name->name;
					}
			}
			$this->data['notices']  = $callAllNotice;
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();
            $this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		elseif($usertype == "Senior_Examiner") 
		{
			$adminID = $this->session->userdata("adminID");
			$userID = $this->session->userdata("loginuserID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_examiner_detail(array('username'  => $username));

			
			// $this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			// $this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		elseif($usertype == "Professor") 
		{
			$adminID = $this->session->userdata("adminID");
            $loginuserID = $this->session->userdata("loginuserID");
			$username = $this->session->userdata('username');
		
			$professor = $this->professor_m->get_single_professor(array('username' => $username));

			$this->data['professorLecture'] = $this->professor_m->get_professor_data($loginuserID);

			$this->data['todaystudentattendance']=$this->professor_m->get_todayattendance($loginuserID);

			$this->data['assignedsubjectsemester']=$this->professor_m->get_assignedsubjectsemesterwise($loginuserID);

			$this->data['assignedsubjectyear']=$this->professor_m->get_assignedsubjectyearwise($loginuserID);

			$this->data['professor'] = $professor;
			$this->data['notices']  = $this->notice_m->get_notice_Acc(array('professorID'=>$professor->professorID)); //d

	        $this->data['professor_signle']	=  $this->professor_m->get_professor2(array('professorID'=>$loginuserID));

			$this->data['courses'] = $this->professor_m->get_classes($this->data['professor_signle']->departmentID);

            $this->data['student'] = $this->professor_m->get_student($this->data['professor_signle']->departmentID);//d
            				       
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->data['subject'] = $this->professor_m->get_subject($this->data['professor_signle']->departmentID);

			$record=$this->professor_m->getsubjectandtopic($loginuserID);

        } 
		elseif($usertype == "Teacher") 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
			// print_r($this->data['Totaluser']);
			// exit();
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();
          	$this->data['totalPayment'] = $this->invoice_m->totalPayment();
           	$this->data['duePayment'] = $this->invoice_m->duePayment();
          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
	     	$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->data['subject'] = $this->subject_m->get_subject();
		} 
		elseif($usertype == "Accountant") {
			$this->data['account'] = $this->invoice_m->get_invoice(array('username'  => $username));
			$username = $this->session->userdata('username');
			/*$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));*/
			/*$this->data['notices']  = $this->notice_m->get_notice_account(array('userID'=>$this->data['user']->userID));*/
			//$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['invoice'] = $this->invoice_m->get_invoice();
			$this->data['feetype'] = $this->feetype_m->get_feetype();
			$this->data['expense'] = $this->expense_m->get_expense();
		} 
		elseif($usertype == "Librarian") {
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));
			$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['lmember'] = $this->lmember_m->get_lmember();
			$this->data['book'] = $this->book_m->get_book();
			$this->data['issue'] = $this->issue_m->get_order_by_issue(array('return_date' => NULL));
		} 
		elseif($usertype == "Student" || $usertype == "Parent") 
		{
			$loginuserID = $this->session->userdata("loginuserID");
			// print_r($loginuserID);die;
			$username = $this->session->userdata('username');
			$adminID = $this->session->userdata('adminID');
			if ($this->input->post('weatherfunctionSession')) {
				$this->session->set_userdata('weatherfunctionSession');
			}
			// student Lecture Start
			$student = $this->student_info_m->get_student_info($loginuserID);
			$studentsubjects= $this->subject_m->get_subject_proffessor_join_studentdashboard($student->yearsOrSemester,$student->classesID);

			foreach ($studentsubjects as $key => $value)
			{
				$studentsubjects[$key]['onlinecheck']=$this->subject_m->get_subject_proffessor_join_studentonline($value['semester'],
					$value['course_id'],$value['subjectID'],$value['professorID'],$loginuserID);
			}
			$this->data['subjects']=$studentsubjects;

			$studentoptionalsubjects = $this->subject_m->get_subject_proffessor_join_optionalsubjects_studentdashboard($student->yearsOrSemester,$student->classesID,$student->studentID);

			foreach ($studentoptionalsubjects as $key => $value)
			{
				$studentoptionalsubjects[$key]['optionalsubjects_onlinecheck']=$this->subject_m->get_subject_proffessor_join_optionalsubjects_studentonline($value['semester'],
					$value['course_id'],$value['subjectID'],$value['professorID'],$loginuserID);
			}
			$this->data['student_subject']=$studentoptionalsubjects;
			// student Lecture End
			$this->data['user'] = $this->student_m->get_single_student(array('username'  => $student->username));
			
			

			$this->data['Examtime'] = $this->etsexam_m->exam_dateStart($this->data['user']->sessionType,$this->data['user']->session,$this->data['user']->examType,$this->data['user']->yearsOrSemester,$this->data['user']->education_mode,$this->data['user']->yosPosition);

				

			//$this->data['NoticeD'] = $this->notice_m->order_Desc($this->data['user']->classesID);
			$this->data['totalamountSend'] = $this->invoice_gen_m->TotalFee($this->data['user']->studentID,$adminID);

			$callAllNotice  = $this->notice_m->notic_record_5(array('notice_x_student.studentID'=>$this->session->userdata('loginuserID')));
			
			foreach($callAllNotice as $key => $value) 
			{
				if($value['usertype']=='Professor')
				{					 
				   $callAllNotice[$key]['name'] = "Professor";
				   $callAllNotice[$key]['bysend'] = "Professor";

				}
				elseif($value['usertype']=='ClgAdmin')
				{
					$callAllNotice[$key]['name'] = "Director";
					$callAllNotice[$key]['bysend'] = "Director";	
				}
				elseif ($value['usertype']=='Accountant') 
				{
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant1";
				}
				elseif ($value['usertype']=='Admin') 
				{
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
			}
			$this->data['NoticeD']  = $callAllNotice;
			// $this->data['exams'] = $this->exam_m->get_exam_count($this->data['user']->studentID);			
		
					
			$this->data['subject'] = $this->subject_m->get_join_where_subject_count($loginuserID,$this->data['user']->classesID,$this->data['user']->sub_coursesID);

			$this->data["student"] = $this->student_m->get_student(array('studentID'=>$this->data['user']->studentID,'adminID'=>$this->data['user']->adminID));

			$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);

			$this->data["totalsubject"]=$this->student_m->gettotalsubject_bystudent($this->data['user']->yearsOrSemester,$this->data["student"]->classesID); //d
			
			$this->data["totalcoursestatus"]=$this->student_m->get_totalcoursestatus_bystudent($this->data['user']->studentID,$this->data["student"]->classesID,$this->data['user']->yearsOrSemester);

		}  
		
		$this->data["subview"] = "dashboard/index";
		$this->load->view('_layout_main', $this->data);
	}
	
	function feedbackMessage(){

    $usertype = $this->session->userdata("usertype");
    $loginuserID = $this->session->userdata("loginuserID");
    $superadmin=$this->professor_m->superadminid();
    $admin=$this->professor_m->adminid();
    foreach($admin as $rows)
    {
        $arr[] = array(
                        $rows['email']
                        );
    }
    $this->db->where('studentID',$loginuserID);
    $students = $this->db->get('student')->row();
    $class= $this->student_m->get_class($students->classesID);
    $category = $this->input->post('category');
		$data = array(

		'studentID'=>$loginuserID,

		'name'=>$students->name,

		'message'=>$this->input->post('val'),

		'email'=>$this->input->post('email'),
		'category'=>$category

		);
        $this->db->insert('feedback',$data);
		$dataEamil['name']=$students->name;
		$dataEamil['course']=$class->classes;
		$dataEamil['semester']=$students->yearsOrSemester;
		$dataEamil['mobile']=$students->phone;
		$dataEamil['email']=$this->input->post('email');
		$dataEamil['msg']=$this->input->post('val');
		$dataEamil['institute']=$this->data["siteinfos"]->sname;
		$dataEamil['superadminemail']=$superadmin->email;
    	$dataEamil['adminemail']="anjugairola@gmail.com";
    	$subjectadmin="Student Feedback from ITM Software";
    	$htmladmin=$this->load->view('emailTemplates/studentaddition/studentfeedback',$dataEamil, true);
    	echo $sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
// team@edgetechnosoft.com,nishantitm5@gmail.com,drvkhanna51@gmail.com




	}





	function paymentscall() {
		
		$usertype = $this->session->userdata('usertype');
		$adminID = $this->session->userdata('adminID');


		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" ||  $usertype == "Teacher" || $usertype == "Accountant") {
if ($usertype == "superadmin") {
			$payments = $this->payment_m->get_payment();
			$invoices = $this->invoice_m->get_invoice();
}else{
			$payments = $this->payment_m->get_payment_byAdminID($adminID);
			$invoices = $this->invoice_m->get_invoice_byAdminID($adminID);

}

			
			$npaid = 0;



			$ppaid = 0;



			$fpaid = 0;



			$cash = 0;



			$cheque = 0;



			$paypal = 0;



			$PayuMoney = 0;



			$OnlinePayment = 0;



			$Bank = 0;



			if(count($invoices)) {



				foreach ($invoices as $invoice) {



					if($invoice->status ==3) {

						$npaid++;

					} elseif($invoice->status == 1) {

						$ppaid++;

					} elseif($invoice->status == 2) {

						$fpaid++;

					}



				}



			}

			if(count($payments)) {
				foreach ($payments as $payment) {
					if('Cash' == $payment->paymenttype) {
						$cash++;
					} elseif('Cheque' == $payment->paymenttype) {
						$cheque++;
					} elseif('Paypal' == $payment->paymenttype) {
						$paypal++;
					}
					elseif('PayuMoney' == $payment->paymenttype) {
						$PayuMoney++;
					}elseif('onlinePayment' == $payment->paymenttype) {

						$OnlinePayment++;

					}elseif('Bank' == $payment->paymenttype) {



						$Bank++;



					}



				}



			}







			if(count($invoices)) {



				$json = array("npaid" => $npaid, "ppaid" => $ppaid, "fpaid" => $fpaid, "cash" => $cash, "cheque" => $cheque, "paypal" => $paypal,"PayuMoney" => $PayuMoney,'OnlinePayment'=>$OnlinePayment,'Bank'=>$Bank, "st" => 1);





				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			} else {



				$json = array("npaid" => $npaid, "ppaid" => $ppaid, "fpaid" => $fpaid, "cash" => $cash, "cheque" => $cheque, "paypal" => $paypal, "PayuMoney" => $PayuMoney,'OnlinePayment'=>$OnlinePayment,'Bank'=>$Bank, "st" => 0);



				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			}



		}



	}







	function graphcall() {
		$usertype = $this->session->userdata('usertype');
		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Teacher" || $usertype == "Accountant") {
			$payments = $this->payment_m->get_order_by_payment();
	      	$lastEarn = 0;
	      	$percent = 0;
	      	$monthBalances = array();
	      	$hightEarn['hight'] = 0;
	      	$dataarr = array();
	      	$allyear = array('2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026');
			if(count($payments)) {
				foreach ($allyear as $key => $allyears) {
					foreach ($payments as $key => $payment) {
                    $paymentsa = date("Y", strtotime($payment->paymentdate));
					    if($allyears ==  $paymentsa) {
					      	$lastEarn+=$payment->paymentamount;
					      	$monthBalances[$allyears] = $lastEarn;
					    } else {
					      	if(!array_key_exists($allyears, $monthBalances)) {
					        	$monthBalances[$allyears] = 0;
					      	}
					    }
					 }
				  	if($lastEarn > $hightEarn['hight']) {
				    	$hightEarn['hight'] = $lastEarn;
				  	}
				  	$lastEarn = 0;
				}
				foreach ($monthBalances as $monthBalancekey => $monthBalance) {
					$dataarr[] = $monthBalance;
				}
				$json = array("balance" => $dataarr);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				foreach ($allyear as $allyears) {
					$dataarr[] = 0;
				}
				$json = array("balance" => $dataarr);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			}
		}
	}

function professor_gender_count(){

$male = 0;

$female = 0;

$na = 0;

	$query  =  $this->db->query("SELECT COUNT(CASE WHEN sex = 'Male' THEN 1 END) m, COUNT(CASE WHEN sex = 'Female' THEN 1 END) f, COUNT(CASE WHEN sex = '0' THEN 1 END) na FROM professor where status = 1 ")->row();
if($query){

$male = $query->m;

$female = $query->f;

$na = $query->na;
}
				$json = array("male" => $male, "female" => $female, "na" => $na);

				header("Content-Type: application/json", true);

				echo json_encode($json,JSON_NUMERIC_CHECK);
	

}

    function student_gender_count()
    {
        $male = 0;
        $female = 0;
        $na = 0;
        $query  =  $this->db->query("SELECT COUNT(CASE WHEN sex = 'Male' THEN 1 END) m, COUNT(CASE WHEN sex = 'Female' THEN 1 END) f, COUNT(CASE WHEN sex = '0' THEN 1 END) na FROM student where status = 1")->row();
        if($query)
        {
            $male = $query->m;
            $female = $query->f;
            $na = $query->na;
        }
        $json = array("male" => $male, "female" => $female, "na" => $na);
        header("Content-Type: application/json", true);
        echo json_encode($json,JSON_NUMERIC_CHECK);
    }

    public function changeli()
    {
        $id = $this->input->post('id');
        $ids = explode("_",$id);
        $type = $ids[0];
        $userid = $ids[1];
        $this->dashboard_m->notification($type,$userid);
    }
    public function pie_chart_js()
    {
    	$this->load->helper('string'); 
    	$loginuserID = $this->session->userdata('loginuserID');
    	$usertype=$this->session->userdata('usertype');
    	if($usertype == "Professor")
    	{
	    	$record=$this->professor_m->piechart_getsubjectandtopic($loginuserID);
	    	// print_r($record);die;
	    	$responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Topping", 
	            "pattern" => "", 
	            "type" => "string" 
	        ); 
	        $responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Total", 
	            "pattern" => "", 
	            "type" => "number" 
	        ); 

	        $count=0;$count1=0;$count2=0;$data4=array();$data5=array();$data6=array();
	        foreach($record as $row)
	        {
	         $unitavg1=0;$unitavg2=0;$unitavg3=0;
	         $data1=array();
			 $data2=array();
			 $data3=array();
			if(($row['totalunits']) != 0)//count
			{		
			// print_r($row['object']);die;	
	      	 foreach ($row['object'] as $key )
			 {
			 	if($key->totalunit != 0)
			 	{
				 	$a=$key->topicid;
					$b=$key->totalunit;
					if($a)
					{
						
		                $topicconut=explode(",",$a);
		     			$avg=(count($topicconut)*100)/$b;
		                $unitavg=round($avg,0);
		                if($unitavg >= 1 && $unitavg <= 50)
		                {
		                	
		                	$count1=$count1+1;
			            	$data1=$row['subject'];
		                }
			            else if($unitavg >= 51 && $unitavg <= 75)
		                {
	                	
		                	$count2=$count2+1;
		                	$data2=$row['subject'];
		                }
		                else if($unitavg >= 76)
		                {
		                	
		                	$count=$count+1;
		                	$data3=$row['subject'];
		                } 
		               
		            }
		        }
		        
	         }
	         
	        }
	         $data4[]=$data1; 
	         $data5[]=$data2; 
	         $data6[]=$data3; 
	        }
       
	        $rt[]=$count1;
	        $rt[]=$count2;
	        $rt[]=$count;
      // print_r($count1);die;
      		for ($i=0; $i <count($rt) ; $i++)
            { 
            	$q=array();
            	// print_r(($rt[$i]));die;
            	if($i==0)
            	{
            		if($data4)
            		{
            			foreach($data4 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data4[$key]);
				    }
				    foreach($data4 as $key => $value)  
				    {
				        $q[]=$value;
				    }
            			// print_r($a);die;
            			// $data['label'][]=$data4;
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $q
                			) 
            		);
            		}
            		else
            		{
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
            		}
            	
            	 	
            	}
            	else if($i==1)
            	{
            		foreach($data5 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data5[$key]);
				    }
				    foreach($data5 as $key => $value)  
				    {
				        $y[]=$value;
				    }
                   // print_r($b);die;
                    // $data['data'][]=$rt[$i];
                    if($data5)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $y
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
            	 	
            	}
            	else
            	{
            		
            		foreach($data6 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data6[$key]);
				    }
				    foreach($data6 as $key => $value)  
				    {
				        $z[]=$value;
				    } 
				    if($data6)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $z
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
               
            	 	
            	}
            	

            }
        echo json_encode($responce);
        }
      // $this->load->view('pie_chart',$data);
    }
    public function pie_chart_js_byadmin()
    {
        $this->load->helper('string'); 
    	$professor = $this->professor_m->getall_professor();
    	$count=0;$count1=0;$count2=0;$data4=array();$data5=array();$data6=array();
        foreach ($professor as $professor)
	    {
	    	// print_r($professor);die;
        	$loginuserID=$professor->professorID;
        	$professorname=$professor->name;
           	$record=$this->professor_m->getsubjectandtopic($loginuserID);
           	// print_r($record);die;
           	 $responce->cols[] = array( 
            "id" => "", 
            "label" => "Topping", 
            "pattern" => "", 
            "type" => "string" 
        	); 
        	$responce->cols[] = array( 
            "id" => "", 
            "label" => "Total", 
            "pattern" => "", 
            "type" => "number" 
        	);
        	$totaltopic="";
        	$totalunit=0;
        	$unit=0;
        	$topic="";
        	// print_r($record);die;
        	foreach($record as $row)
      		{
      			$unitavg1=0;$unitavg2=0;$unitavg3=0;
      			// print_r($row['object']);
        		foreach ($row['object'] as $key )
				{
					// print_r($key);die;
					$a=$key->topicid;
					$b=$key->totalunit;
					if($a)
					{
						
		                $topicconut=explode(",",$a);
		     			$totaltopic=(count($topicconut));
		     			$totalunit=$b;
		     			// print_r($totalunit);die;
		             
               		}
               		
        		}
        		$topic += $totaltopic;
               		$unit += $totalunit;
        		
		    }
		    if($unit != 0)
		    {
		    	$per=($topic*100)/$unit;
			    // print_r($per);
			    $unitavg=round($per,0);
			    // print_r($unitavg);
			    $data1=array();
				$data2=array();
				$data3=array();
	                if($unitavg >= 1 && $unitavg <= 50)
	                {
	                	$count1=$count1+1;
		            	$data1=$professorname;
	                }
		            else if($unitavg >= 51 && $unitavg <= 75)
	                {
			        	$count2=$count2+1;
	                	$data2=$professorname;
	                }
		            else if($unitavg >= 76)
		            {
		            	$count=$count+1;
		            	$data3=$professorname;
	                } 
			    $data4[]=$data1; 
				$data5[]=$data2; 
				$data6[]=$data3; 
		    }
		    else
		    {

		    }
		    
        }
        
      $rt[]=$count1;
      $rt[]=$count2;
      $rt[]=$count;
      for ($i=0; $i <count($rt) ; $i++)
      { 
       	if($i==0)
      	{
        	if($data4)
        	{
        		// print_r($rt[$i]);die;
        		foreach($data4 as $key => $value) 
          		{         
		          if(empty($value)) 
		          unset($data4[$key]);
		        }
			    foreach($data4 as $key => $value)  
			    {
			        $q[]=$value;
			    }
            	$responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Below 50%" 
                    ),
                	array( 
		            "v" => $rt[$i],
		            "f" => $q
                    ) 
            		);
            }
            else
            {
                $responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Below 50%"
                    ),
            	    array( 
		            "v" => 0, 
		            "f" => null 
                	) 
            		);
            }
        }
        else if($i==1)
        {
        	foreach($data5 as $key => $value) 
        	{         
		        if(empty($value)) 
		        unset($data5[$key]);
		    }
		    foreach($data5 as $key => $value)  
		    {
		        $y[]=$value;
		    }
            if($data5)
            {
               	$responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Upto 51%-75%"
                    ),
            		array( 
		            "v" => $rt[$i], 
		            "f" => $y
                	) 
            		);
            }
            else
            {
               	$responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Upto 51%-75%"
                    ),
            		array( 
		            "v" => 0, 
		            "f" => null 
                	) 
            		);
            }
        }
        else
        {
        	
        	if($i==2)
        	{
        		// print_r($data6);die;
	        	foreach($data6 as $key => $value) 
	    		{         
			        if(empty($value)) 
			        unset($data6[$key]);
			    }
			    foreach($data6 as $key => $value)  
			    {
			        $z[]=$value;
			    } 
			    if($data6)
	            {
	            	$responce->rows[]["c"] = array( 
	                    array(
	                    "v" => $rt[$i],
	                    "f" => "Above 75%" 
	                    ),
	        			array( 
	                    "v" => $rt[$i], 
	                    "f" => $z
		    			) 
	            		);
	            }
	            else
	            {
	            	$responce->rows[]["c"] = array( 
	                    array(
		                "v" => $rt[$i],
	                    "f" => "Above 75%" 
	                    ),
	        			array( 
	                    "v" => 0, 
	                    "f" => null 
	        			) 
	            		);
	            }
	        }
    	}
      }
      echo json_encode($responce);
    }
    public function pie_chart_js_showattendance()
    {
       	$this->load->helper('string'); 
    	$loginuserID = $this->session->userdata('loginuserID');
    	$usertype=$this->session->userdata('usertype');
    	if($usertype == "Professor")
    	{
	    	$record=$this->professor_m->getstudentattendance($loginuserID);
	    	$responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Topping", 
	            "pattern" => "", 
	            "type" => "string" 
	        ); 
	        $responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Total", 
	            "pattern" => "", 
	            "type" => "number" 
	        ); 
            $totalstudentattendance=0;
	        $count=0;$count1=0;$count2=0;$data4=array();$data5=array();$data6=array();
	        $data1=array();
					$data2=array();
					$data3=array();
	        foreach($record as $row)
	        {
	         $countattendance=0;	
	         $unitavg1=0;$unitavg2=0;$unitavg3=0;
	         $totalstudentattendance=count($row['object']);
	      	 foreach ($row['object'] as $key )
			 {
                $present1=0;
	            $present=$key->atd;
				$b=$totalstudentattendance;
				if($present==1)
	            {
	              $countattendance=$countattendance+1;
	            }
	         }
	         if($countattendance >=1)
             {

             	 $avg=($countattendance*100)/$b;
           		 $unitavg=round($avg,0);
           		 if($unitavg >= 0 && $unitavg <= 50)
           		 {
                    $count1=$count1+1;
		            $data1=$row['studentname'];
           		 }
           		 else if($unitavg >= 51 && $unitavg <= 75)
           		 {
                    $count2=$count2+1;
	                $data2=$row['studentname'];
           		 }
           		 else if($unitavg >= 76)
           		 {
                    $count=$count+1;
	                $data3=$row['studentname'];
           		 }
           	 }
	        
	         $data4[]=$data1; 
	         $data5[]=$data2; 
	         $data6[]=$data3; 
	        }
       
	        $rt[]=$count1;
	        $rt[]=$count2;
	        $rt[]=$count;
      // print_r($count1);die;
      		for ($i=0; $i <count($rt) ; $i++)
            { 
            	// print_r(($rt[$i]));die;
            	if($i==0)
            	{
            		if($data4)
            		{
            			foreach($data4 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data4[$key]);
				    }
				    foreach($data4 as $key => $value)  
				    {
				        $q[]=$value;
				    }
            			// print_r($a);die;
            			// $data['label'][]=$data4;
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $q
                			) 
            		);
            		}
            		else
            		{
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
            		}
            	
            	 	
            	}
            	else if($i==1)
            	{
            		foreach($data5 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data5[$key]);
				    }
				    foreach($data5 as $key => $value)  
				    {
				        $y[]=$value;
				    }
                   // print_r($b);die;
                    // $data['data'][]=$rt[$i];
                    if($data5)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $y
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
            	 	
            	}
            	else
            	{
            		
            		foreach($data6 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data6[$key]);
				    }
				    foreach($data6 as $key => $value)  
				    {
				        $z[]=$value;
				    } 
				    if($data6)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $z
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
               
            	 	
            	}
            	

            }
        echo json_encode($responce);
        }
      // $this->load->view('pie_chart',$data);
    }
    public function bar_chart_all_student_attendance()
    {
    	$month=$this->input->post('year');
    	$result = $this->professor_m->all_student_attendance_bymonth($month);
    	foreach($result as $row)
		 {
		   $totalattendance=$this->professor_m->totalattendancebycoursewise($month,$row["classesID"]);
		   if($totalattendance)
		   {
		   	  if($totalattendance->totalstudent != 0)
		   	  {
		   	  	  foreach ($row['student_attendance'] as $key => $value)
				  {

				  	 $output[] = array(
				   'month'   => $row["classes"],'profit' =>floatval(($value->totalpresent*100)/$totalattendance->totalstudent));
				  	
				  }
		   	  }
		   	  else
		   	  {
		   	  	foreach ($row['student_attendance'] as $key => $value)
			    {

			  	   $output[] = array(
			       'month'   => $row["classes"],'profit' =>floatval($value->totalpresent));
			  	
			    }
		   	  }
		   	  
		   }
		   else
		   {
		   	foreach ($row['student_attendance'] as $key => $value)
			  {

			  	 $output[] = array(
			   'month'   => $row["classes"],'profit' =>floatval($value->totalpresent));
			  	
			  }
		   }
		  
		     
		  
		 }
		 echo json_encode($output);
    }
    public function requestbysuperadmin()
    {
    	$usertype=$this->session->userdata('usertype');
    	if($usertype=="ClgAdmin")
    	{
    		if($_POST)
	    	{
	    		$newemail=$this->input->post('newemail');
	    		$requestmobilenumber=$this->input->post('requestmobilenumber');
	    		if($newemail)
	    		{
	    			$dataEamil['name']=$this->session->userdata('name');
	    			$dataEamil['newemail']=$newemail;
	    			$dataEamil['institute']=$this->data["siteinfos"]->sname;
	    			$subject="Request to Change E-Mail";
		            $html = $this->load->view('emailTemplates/Requesttochangeemail', $dataEamil , true);
		            $email="Support@edgetechnosoft.com";
		            $fullname="Support";
		            $dataEamil['superadminemail']="";
		            $dataEamil['adminemail']="";
		            // $dataEamil['Accountant']=
		            
		            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
		            if($sendmail==true)
		            {
		            	$email=$this->session->userdata('email');
		            	$fullname=$dataEamil['name'];
		            	$subject="Request to update email accepted";
		            	$html=$this->load->view('emailTemplates/Requesttochangeemailbysuperadmin',$dataEamil, true);
		            	$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
		            	if($sendmail==true)
		            	{
		            		// print_r("expression");die;
		            		$this->session->set_flashdata('success', $this->lang->line('mail_success'));
		            		redirect(base_url("dashboard/index"));
		            	}
		            	else
		            	{
		            		$this->session->set_flashdata('error', "Mail Not Send Successfully");
		            		redirect(base_url("dashboard/index"));
		            	}
		            }
		            else
		            {
		            	$this->session->set_flashdata('error', "Mail Not Send Successfully");
		            		redirect(base_url("dashboard/index"));
		            }
	    		}
	    		if($requestmobilenumber)
	    		{
	    			$dataEamil['name']=$this->session->userdata('name');
	    			$dataEamil['requestmobilenumber']=$requestmobilenumber;
	    			$dataEamil['institute']=$this->data["siteinfos"]->sname;
	    			$subject="Request to Change Mobile Number";
		            $html = $this->load->view('emailTemplates/Requesttochangemobileno', $dataEamil , true);
		            $email="Support@edgetechnosoft.com";
		            $fullname="Support";
		            $dataEamil['superadminemail']="";
		            $dataEamil['adminemail']="";
		            // $dataEamil['Accountant']=
		            
		            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
		            if($sendmail==true)
		            {
		            	$email=$this->session->userdata('email');
		            	$fullname=$dataEamil['name'];
		            	$subject="Request to update mobile number accepted";
		            	$html=$this->load->view('emailTemplates/Requesttochangemobilebysuperadmin',$dataEamil, true);
		            	$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
		            	if($sendmail==true)
		            	{
		            		$this->session->set_flashdata('success', $this->lang->line('mail_success'));
		            		redirect(base_url("dashboard/index"));
		            	}
		            	else
		            	{
		            		$this->session->set_flashdata('error', "Mail Not Send Successfully");
		            		redirect(base_url("dashboard/index"));
		            	}
		            }
		            else
		            {
		            	$this->session->set_flashdata('error', "Mail Not Send Successfully");
		            		redirect(base_url("dashboard/index"));
		            }
	    		}
	    		// print_r($newemail);die;
	    	}
    	}
    	// else
    	// {

    	// }
    	
    }
    public function verifymob()
    {
    	$usertype=$this->session->userdata('usertype');
    	$loginuserID=$this->session->userdata('loginuserID');
    	$otp=$this->input->post('otp');
    	$userotp=$this->input->post('otpuser');
    	// echo $otp."   ".$userotp;die();
    	if($otp==$userotp)
    	{
    		
    		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "HOD")
    		{
    			$this->db->where('userID',$loginuserID);
    			$this->db->where('usertype',$usertype);
    			$this->db->update('user',array('verifymobileno' =>1));    			
        		$this->session->unset_userdata('verifymobileno');
        		$this->session->set_userdata(array('verifymobileno' =>1));
    			
    		}
    		else if($usertype == "Professor")
    		{
    			$this->db->where('professorID',$loginuserID);
    			$this->db->where('usertype',$usertype);
    			$this->db->update('professor',array('verifymobileno' =>1));
    		}
    		else if($usertype == "Student")
    		{
    			$this->db->where('studentID',$loginuserID);
    			$this->db->where('usertype',$usertype);
    			$this->db->update('student',array('verifymobileno' =>1));
    		}
    		else
    		{
    			$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
    		}
    		$this->session->set_flashdata('success', "Your Mobile Number Is verifyed Successfully");
		    redirect(base_url("dashboard/index"));
    	}
    	else
        {
        	$this->session->set_flashdata('error', "Wrong OTP. Add 'Request New OTP' after mobile number");
        	redirect(base_url("dashboard/index"));
        }
    }

}



