<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



Class Online_reader extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_info_m");

		$this->load->model("parentes_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("teacher_m");

		$this->load->model("student_m");

		$this->load->model("lms_m");

		$this->load->model("online_reader_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('subject', $language);	

	}



	public function index() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterSubjectsubCourseID');
					$this->session->unset_userdata('FilterSubjectyearSemesterID');
					$this->session->set_userdata('FilterSubjectclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterSubjectsubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterSubjectyearSemesterID', $this->input->post('yearSemesterID'));
				}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
                   $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "online_reader/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$student = $this->student_info_m->get_student_info();

			$this->data['subjects'] = $this->lms_m->get_join_where_subject($student->classesID,$student->sub_coursesID);
			// $this->data['online_reader'] = $this->online_reader_m->getonlinereaderpass($student->studentID,);
			$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);

		if ($this->input->post('yearSemesterID')) {
			if ($this->input->post('yearSemesterID')=='Select') {
			$this->session->unset_userdata('FilterLmsyearSemesterID');
			}else{
			$this->session->set_userdata('FilterLmsyearSemesterID', $this->input->post('yearSemesterID'));
		  }
			}


			$this->data["subview"] = "online_reader/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					$classesID = $checkstudent->classesID;

					$this->data['set'] = $id;

					$this->data['subjects'] = $this->subject_m->get_join_subject($classesID);

					$this->data["subview"] = "subject/index_parent";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "subject/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


	public function onlinereader_readingbook(){

		$uri = $this->uri->segment(3);


		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->where('unique_id',$uri);
		$recievedata = $this->db->get('epub')->row();
		
		$orbookreading = $this->input->post("length");		
			
		$zeros = array();

		 for($i=1; $i<=$orbookreading; $i++){
		 $zeros[]=0;
		 }
		 $userdata=array(
		 	'studentID'=>$loginuserID,
		 	'subjectID'=>$recievedata->subjectID,
		 	'epubID'=>$recievedata->id,
			'individual_time'=>implode(',',$zeros),
			'chapter_count'=> $orbookreading,
			'words_count'=>implode(',',$zeros)
			
		);

		 $this->db->where('epubID',$recievedata->id);
		 $this->db->where('studentID',$loginuserID);
		 $this->db->where('subjectID',$recievedata->subjectID);
		$rowcount = $this->db->get('onlineReader_status')->num_rows();

// echo $rowcount;
		/*

		*/

		 if ($rowcount==0) {
		 	
		 $this->db->insert('onlineReader_status',$userdata); 	
		 }

		 

		


		
	 

	}


	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->subject_m->get_join_subject_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->subject_m->make_datatables($adminID);
            // $totalFiltered  = $this->student_m->get_filtered_data();

        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
        	$j= 0;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['subject_code'] = $post->subject_code;
				$nestedData['subject'] = $post->subject;
                $nestedData['classes'] = $post->classes;
                $nestedData['sub_course'] = $post->sub_course;
                $nestedData['yearsOrSemester'] = $post->yearsOrSemester;
                $this->db->where('subjectID',$post->subjectID);
                $query   = $this->db->get('epub');
                $countpdf  = $query->result();
  
                foreach ($countpdf as $key => $value) {
                	$storeValue .= $value->pdf." ".'<a href="#">
                	 <span class="glyphicon glyphicon-remove"></span>
        			</a>';
                }
                $nestedData['countpdf'] = $storeValue;
        
  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {


   $buttons     = "<input type = 'file' name = 'userfile".$j++."' /><input type = 'hidden' name ='subject_ID[]' value = '".$post->subjectID."' /><input type = 'hidden' name ='subject_name[]' value = '".$post->subject."' />";



   $nestedData['action'] = $buttons;
}
               
                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
}



	protected function rules() {

		$rules = array(

				array(

					'field' => 'classesID', 

					'label' => $this->lang->line("subject_class_name"), 

					'rules' => 'trim|numeric|required|xss_clean|max_length[11]|callback_allclasses'

				),

				array(

					'field' => 'teacherID', 

					'label' => $this->lang->line("subject_teacher_name"), 

					'rules' => 'trim|required|xss_clean|max_length[60]|callback_allteacher'

				),

				array(

					'field' => 'subject', 

					'label' => $this->lang->line("subject_name"), 

					'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_subject'

				), 

				array(

					'field' => 'subject_author', 

					'label' => $this->lang->line("subject_author"), 

					'rules' => 'trim|xss_clean|max_length[100]'

				), 

				array(

					'field' => 'subject_code', 

					'label' => $this->lang->line("subject_code"),

					'rules' => 'trim|required|max_length[20]|xss_clean|callback_unique_subject_code'

				),

			);

		return $rules;
	}

	public function add() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata('adminID');
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

			$this->data['teachers'] = $this->teacher_m->get_teacher();

			if($_POST) {

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) { 

					$this->data["subview"] = "subject/add";

					$this->load->view('_layout_main', $this->data);			

				} else {

					$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));

					$array = array(

						"classesID" => $this->input->post("classesID"),

						"teacherID" => $this->input->post("teacherID"),

						"subject" => $this->input->post("subject"),

						"subject_author" => $this->input->post("subject_author"),

						"subject_code" => $this->input->post("subject_code"),

						"teacher_name" => $teacher->name,

						"create_date" => date("Y-m-d h:i:s"),

						"modify_date" => date("Y-m-d h:i:s"),

						"create_userID" => $this->session->userdata('loginuserID'),

						"create_username" => $this->session->userdata('username'),

						"create_usertype" => $this->session->userdata('usertype')

					);

					$this->subject_m->insert_subject($array);

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					redirect(base_url("subject/index"));

				}

			} else {

				$this->data["subview"] = "subject/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


	public function edit() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));

			if((int)$id && (int)$url) {

				$this->data['classes'] = $this->subject_m->get_classes();

				$this->data['teachers'] = $this->teacher_m->get_teacher();

				$this->data['subject'] = $this->subject_m->get_subject($id);

				if($this->data['subject']) {

					$this->data['set'] = $url;

					if($_POST) {

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) {

							$this->data['form_validation'] = validation_errors(); 

							$this->data["subview"] = "subject/edit";

							$this->load->view('_layout_main', $this->data);			

						} else {

							$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));

							$array = array(

								"classesID" => $this->input->post("classesID"),

								"teacherID" => $this->input->post("teacherID"),

								"subject" => $this->input->post("subject"),

								"subject_author" => $this->input->post("subject_author"),

								"subject_code" => $this->input->post("subject_code"),

								"teacher_name" => $teacher->name,

								"modify_date" => date("Y-m-d h:i:s")

							);

							$this->subject_m->update_subject($array, $id);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("subject/index/$url"));

						}

					} else {

						$this->data["subview"] = "subject/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}




	public function submit_Online_reader(){
		
	$subjectID =  $this->input->post('subject_ID');
	$subject_name =  $this->input->post('subject_name');
	// print_r($_FILES);
	// exit();
	for ($i=0; $i<count($this->input->post('subject_ID')); $i++) { 
		//print_r($i);
		// print_r($_FILES['userfile'.$i]['name']);
		// exit();

	if ($_FILES['userfile'.$i]['name']!='') {
			$intger = intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));

	$config['upload_path'] = 'uploads/online_reader';
	$config['allowed_types'] = 'epub';
	$config['max_size'] = '5120';
	$this->load->library('upload', $config);
	$this->upload->do_upload('userfile'.$i);
	$image_data = $this->upload->data();
// print_r($image_data);
// exit();
	$data = array(
	"subjectID"=>$subjectID[$i],
	"title"=>$subject_name[$i],
	"pdf"=>$image_data['file_name'],
	"unique_id"=>$intger
	  );
	  $query = $this->db->insert('epub',$data);
	}
	}

	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	 redirect('online_reader/index');
		// $this->session->set_flashdata('error', 'Problem');
		// redirect('lms/index');
	}


	public function lmsEpub(){
		
		


	
}


	public function reader(){
		
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		$id_refer = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
		$this->db->where('subjectID',$id);
		$this->db->where('id',$id_refer);
		$data_row  = $this->db->get('epub')->row();
		// print_r($data_row);
		// exit();
		
		$this->data['read'] =  $data_row->pdf;
		$this->data['subjectTitle'] =  $data_row->title;

		if($id && $id_refer){
		$this->db->where('subjectID',$id);
		$subjectRow  = $this->db->get('subject')->row();

		if($subjectRow){
              $this->db->where('classesID',$subjectRow->classesID);
              $this->db->where('sub_coursesID',$subjectRow->sub_coursesID);
          }
          $this->db->where('studentID',$loginuserID);
		$validate = $this->db->get('student')->row();

		if($subjectRow && $validate){
			$this->data['online_reader'] = $this->online_reader_m->getdetail_validation_epub($id,$id_refer);
			$this->data['videos_url'] = $this->lms_m->get_report_url_epub($loginuserID,$id,$id_refer);          

		if($this->data['online_reader'])
		{
			$this->data["subview"] = "online_reader/reader";
			$this->load->view('_layout_or', $this->data);
		}else{
			$this->load->view('errorLms');
		}

		}else{
		 $this->load->view('errorLms');
		}


 }else{
 	$this->load->view('errorLms');
 }
		

		
		
		
	}	


	public function getAjaxdata_or(){
		
		$uri = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);

		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->where('unique_id',$uri);
		$recievedata = $this->db->get('epub')->row();
		


		$orbookreading = $this->input->post("length");

		
		$this->db->where('epubID',$recievedata->id);
		$this->db->where('studentID',$loginuserID);
		$this->db->where('subjectID',$recievedata->subjectID);
		$this->data['readerdata']  =  $this->db->get('onlineReader_status')->row(); 
		$strToArray   =  $this->data['readerdata']->individual_time;
		$getArray			 =  explode(',', $strToArray);
		$this->data['readerdata']->extra	 =     $getArray[$uri4];	

	echo json_encode($this->data['readerdata']);

	}

public function update_onlineReader(){
		$uri = $this->uri->segment(3);

		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->where('unique_id',$uri);
		$recievedata = $this->db->get('epub')->row();
		


		$lastTime = $this->input->post("lastTime");
		$lastword = $this->input->post("lastword");
		$singleTime = $this->input->post("singleTime");
		$word_count = $this->input->post("word_count");


		$completeTime = $word_count * 0.1;

		if ($singleTime>$completeTime) {
		exit();
		}


		 $userdata=array(
			'individual_time'=>$lastTime,
			'words_count'=>$lastword
			
		);
		 $this->db->where('epubID',$recievedata->id);
		 $this->db->where('studentID',$loginuserID);
		 $this->db->where('subjectID',$recievedata->subjectID);
		 $this->db->update('onlineReader_status', $userdata);



}

	public function Online_reader()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterSubjectsubCourseID');
					$this->session->unset_userdata('FilterSubjectyearSemesterID');
					$this->session->set_userdata('FilterSubjectclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterSubjectsubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterSubjectyearSemesterID', $this->input->post('yearSemesterID'));
				}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
                   $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "lms/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$student = $this->student_info_m->get_student_info();



			$this->data['subjects'] = $this->lms_m->get_join_where_subject($student->classesID,$student->sub_coursesID);

			$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);

		if ($this->input->post('yearSemesterID')) {
			if ($this->input->post('yearSemesterID')=='Select') {
			$this->session->unset_userdata('FilterLmsyearSemesterID');
			}else{
			$this->session->set_userdata('FilterLmsyearSemesterID', $this->input->post('yearSemesterID'));
		  }
			}


			$this->data["subview"] = "online_reader/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					$classesID = $checkstudent->classesID;

					$this->data['set'] = $id;

					$this->data['subjects'] = $this->subject_m->get_join_subject($classesID);

					$this->data["subview"] = "subject/index_parent";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "subject/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function showActivity(){
		$uri = $this->uri->segment(3);

		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->where('unique_id',$uri);
		$recievedata = $this->db->get('epub')->row();
		
		echo $this->online_reader_m->getonlinereaderpass($loginuserID,$recievedata->id)."%";

	}

	

	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));

			if((int)$id && (int)$url) {

				$this->subject_m->delete_subject($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("subject/index/$url"));

			} else {

				redirect(base_url("subject/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function unique_subject() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			$subject = $this->subject_m->get_order_by_subject(array("subject" => $this->input->post("subject"), "subjectID !=" => $id, "classesID" => $this->input->post("classesID")));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$subject = $this->subject_m->get_order_by_subject(array("subject" => $this->input->post("subject"), "classesID" => $this->input->post("classesID"), "subject_code" => $this->input->post("subject_code")));



			if(count($subject)) {

				$this->form_validation->set_message("unique_subject", "%s already exists");

				return FALSE;

			}

			return TRUE;

		}	

	}

	public function deleteLMS(){
		$id = $this->input->post('id');
		// $pdf = $this->input->post('pdf');
		// $Path = echo base_url().'uploads/online_reader'.$pdf;
		
		$this->db->where('id',$id);
		$this->db->delete('epub');
		// if ($deleteMailEpubFile) {
		// 	unlink($Path);
		// }
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	}

	public function unique_subject_code() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			$subject = $this->subject_m->get_order_by_subject(array("subject_code" => $this->input->post("subject_code"), "subjectID !=" => $id));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject_code", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$subject = $this->subject_m->get_order_by_subject(array("subject_code" => $this->input->post("subject_code")));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject_code", "%s already exists");

				return FALSE;

			}

			return TRUE;

		}	

	}

	public function subject_list() {

		$classID = $this->input->post('id');

		if((int)$classID) {

			$string = base_url("subject/index/$classID");

			echo $string;

		} else {

			redirect(base_url("subject/index"));

		}

	}

	public function student_list() {

		$studentID = $this->input->post('id');

		if((int)$studentID) {

			$string = base_url("subject/index/$studentID");

			echo $string;

		} else {

			redirect(base_url("subject/index"));

		}

	}

function ResetCourses(){

	$this->session->unset_userdata('FilterSubjectclassesID');
	   
}

function ResetSubcourses(){

	$this->session->unset_userdata('FilterSubjectsubCourseID');
	   
}

function ResetSemesterYear(){

	$this->session->unset_userdata('FilterSubjectyearSemesterID');
	$this->session->unset_userdata('FilterLmsyearSemesterID');	   
}
function ResetAllfilter(){

	$this->session->unset_userdata('FilterSubjectsubCourseID');
	$this->session->unset_userdata('FilterSubjectclassesID');
	$this->session->unset_userdata('FilterSubjectyearSemesterID');
	   
}

function allclasses() {

		if($this->input->post('classesID') == 0) {

			$this->form_validation->set_message("allclasses", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

	public function allteacher() {

		if($this->input->post('teacherID') === '0') {

			$this->form_validation->set_message("allteacher", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

}


