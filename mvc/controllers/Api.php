<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
     
class Api extends REST_Controller
{
    
    public function __construct()
    {
       parent::__construct();
       $this->load->library("session");
        $this->load->helper('language');
        $this->load->helper('form');
        $this->load->database();
        $this->load->model('subject_m');
        $this->load->model('setting_m');
        $this->load->model('classes_m');
        $this->load->model("signin_m");
        $this->load->model("student_info_m");
        $this->load->model("student_m");
        $this->load->model("notice_m");
        $this->load->model("etsexam_m");
        $this->load->model("invoice_gen_m");
        $this->load->model("lms_m");
        $this->load->model("qa_m");
        $this->load->library('pagination');
        
    }

    public function datademo_get()
    {
        $message="hello this is a demo api";
        $res['responseCode']=200;          
        $res['dashboad_data']=$message;
        $this->set_response($res, REST_Controller::HTTP_OK);
    }
    protected function signinrules() {

        $rules = array(

                 array(

                    'field' => 'username',

                    'label' => "Username",

                    'rules' => 'trim|required|max_length[40]'

                ),

                array(

                    'field' => 'password',

                    'label' => "Password",

                    'rules' => 'trim|required|max_length[40]'

                )

            );

        return $rules;

    }
    protected function rules_email() {
        $rules = array(
                 array(
                    'field' => 'email',
                    'label' => "Email",
                    'rules' => "trim|required|max_length[40]|valid_email"
                )
            );
        return $rules;
    }
    public function signin_post()
    {
        if($_POST)
        {
            $rules = $this->signinrules();
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $res['responseCode']=0;
                $res['message']=validation_errors();      
                $this->set_response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                if($this->signin_m->signin() == TRUE)
                {      
                    if($this->session->userdata('usertype')=="Student")
                    {           
                        $device_token = $this->input->post('device_token');
                        $device_type = $this->input->post('device_type');
                        $data["loginuserID"] = $this->session->userdata('loginuserID');
                        $data["name"] = $this->session->userdata('name');
                        // $data["examType"] = $this->session->userdata('examType');
                        $data["adminID"] = $this->session->userdata('adminID');
                        $data["email"] = $this->session->userdata('email');
                        $data["phone"] = $this->session->userdata('phone');
                        $data["usertype"] = $this->session->userdata('usertype');
                        $data["education_mode"]=$this->session->userdata('education_mode');
                        $data["username"] = $this->session->userdata('username');
                        $data["photo"] = base_url('uploads/images/'.$this->session->userdata('photo'));
                        $data["loggedin"] = TRUE;
                        $res['responseCode']=200;
                        $res['message']="Login Successfully ";
                        $res['user_data']=$data;
                        if($device_type && $device_token)
                        {
                            $this->db->where('studentID',$data["loginuserID"]);
                            $this->db->update('student',array('device_token' =>$device_token,'device_type'=>$device_type));
                        }
                        $this->response($res, REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $res['responseCode']=0;
                        $res['message']="Your username or password is incorrect";
                        $this->response($res, REST_Controller::HTTP_OK);
                    }
                    
                }
                else
                {
                    $res['responseCode']=0;
                    $res['message']="Your username or password is incorrect";
                    $this->response($res, REST_Controller::HTTP_OK);
                }
            }
        }
    }

    public function resetpassword_post()
    {
        $this->load->model("reset_m");
        $this->load->model("site_m");
        $array = array();
        $reset_key = "";
        $tmp_url = "";
        $i = 0;
        $this->data['form_validation'] = "No";
        if($_POST)
        {
            $rules = $this->rules_email();
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $res['responseCode']=0;
                $res['message']=validation_errors();
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $email = $this->input->post('email');
                $tables = array('admin'=>'admin','student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','professor' => 'professor');
                foreach ($tables as $table)
                {
                    $dbuser = $this->reset_m->get_table_users($table, $email);
                    if(($dbuser))
                    {
                        $this->data['siteinfos'] = $this->site_m->get_site(array('adminID'=>$dbuser->adminID));
                        $reset_key = $this->reset_m->hash($dbuser->usertype.$dbuser->username.$dbuser->name);
                        $tmp_url = base_url("reset/password/".$reset_key);
                        $array['permition'][$i] = 'yes';
                    }
                    else
                    {
                        $array['permition'][$i] = 'no';
                    }
                    $i++;
                }

                if(in_array('yes', $array['permition']))
                {
                    $dbreset = $this->reset_m->get_reset();
                    if(count($dbreset))
                    {
                        if($this->db->truncate('reset'))
                        {
                            $this->reset_m->insert_reset(array('keyID' => $reset_key, 'email' => $email));
                        }
                        else
                        {
                            $this->session->set_flashdata('reset_error', 'reset access off!');
                        }
                    }
                    else
                    {
                        $this->reset_m->insert_reset(array('keyID' => $reset_key, 'email' => $email));
                    }
                    $html = 'Reset your password by e-mail -> ' .$tmp_url;
                    $subject="Reset Password";
                    $fullname="";
                    $dataEamil['superadminemail']="";
                    $dataEamil['adminemail']="";
                    // $dataEamil['Accountant']=
                    $dataEamil['institute']=$this->data["siteinfos"]->sname;
                    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
                    

                    if($sendmail == true || $sendmail == 1)
                    {
                        $res['responseCode']=200;
                        $res['message']="Link Send Your Register Email-ID";
                        $this->response($res, REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $res['responseCode']=0;
                        $res['message']="Email not Send!";
                        $this->response($res, REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $res['responseCode']=0;
                    $res['message']="Email not found!";
                    $this->response($res, REST_Controller::HTTP_OK);
                }
            }
        }
    }
    public function dashboard_post()
    {
        $loginuserID=$this->input->post('loginuserID');
        $emptyArray = array();
        $resultstudentsubject=array();
        if($loginuserID)
        {
            $student = $this->student_info_m->get_student_info($loginuserID);
            if($student)
            {
                // Total subject show start
                $studentsubjects= $this->subject_m->get_subject_proffessor_join_studentdashboard($student->yearsOrSemester,$student->classesID,$student->studentID);
                if($studentsubjects)
                {
                    foreach ($studentsubjects as $key => $value)
                    {
                        $studentsubjects[$key]['onlinecheck']=$this->subject_m->get_subject_proffessor_join_studentonline($value['semester'],
                            $value['course_id'],$value['subjectID'],$value['professorID'],$loginuserID);
                    }
                    $studentsubject=$studentsubjects;
                    foreach ($studentsubject as $key => $values)
                    {
                        $days=explode(",", $values['days']);
                        $today=date('D');
                        $todaydate=date('Y-m-d');
                        for($g=0;$g<count($days);$g++)
                        {
                            if($days[$g]==$today)
                            {
                               $data1['days']=$values['days'];
                               $data1['times']=$values['times'];
                               $data1['subject']=$values['subject'];
                               $data1['subjectID']=$values['subjectID'];
                               $data1['studentactive']=$values['studentactive'];
                               $data1['name']=$values['name'];
                               $data1['professorID']=$values['professorID'];
                               $data1['semester']=$values['semester'];
                               $data1['course_id']=$values['course_id'];
                               if($values['onlinecheck'])
                               {
                                $data1['onlinecheck']=$values['onlinecheck']->activestatus;
                               }
                               else
                               {
                                $data1['onlinecheck']="";
                               }
                               $checkliveclass=$this->db->select('live_status,status,streamID,av,channel_name')->where(array('courseID' => $values['course_id'],'yearsOrSemester'=>$values['semester'],'subjectID'=>$values['subjectID'],'createdID'=>$values['professorID'],'date'=>date('Y-m-d')))->get('live_broadcast')->row();
                               if($checkliveclass)
                               {
                                  if($checkliveclass->status==1)
                                  {
                                    $data1['live_class']="Live Class Created";
                                    $data1['live_class_link']=base_url('live/audience/'.$checkliveclass->status.'/'.$checkliveclass->av.'?channel='.$checkliveclass->channel_name);
                                  }
                                  else
                                  {
                                    $data1['live_class']="Live Class Over";
                                    $data1['live_class_link']="";
                                  }                                  
                               }
                               else
                               {
                                  $data1['live_class']="";
                                  $data1['live_class_link']="";
                               }
                               $resultstudentsubject[]=$data1;
                            }
                            
                       }
                       
                    }
                    if($resultstudentsubject)
                    {

                    }
                    else
                    {
                        $resultstudentsubject=$emptyArray;
                    }

                }
                else
                {
                    $resultstudentsubject=$emptyArray;
                }
                
                $studentoptionalsubjects = $this->subject_m->get_subject_proffessor_join_optionalsubjects_studentdashboard($student->yearsOrSemester,$student->classesID,$student->studentID);
                if($studentoptionalsubjects)
                {
                    foreach ($studentoptionalsubjects as $key => $value)
                    {
                        $studentoptionalsubjects[$key]['optionalsubjects_onlinecheck']=$this->subject_m->get_subject_proffessor_join_optionalsubjects_studentonline($value['semester'],
                            $value['course_id'],$value['subjectID'],$value['professorID'],$loginuserID);
                    }
                    $studentoptionalsubject=$studentoptionalsubjects;
                    foreach ($studentoptionalsubject as $key => $values)
                    {
                        $days=explode(",", $values['days']);
                        $today=date('D');
                        $todaydate=date('Y-m-d');
                        for($g=0;$g<count($days);$g++)
                        {
                            if($days[$g]==$today)
                            {
                               $data2['days']=$values['days'];
                               $data2['times']=$values['times'];
                               $data2['subject']=$values['subject'];
                               $data2['subjectID']=$values['subjectID'];
                               $data2['studentactive']=$values['studentactive'];
                               $data2['name']=$values['name'];
                               $data2['professorID']=$values['professorID'];
                               $data2['semester']=$values['semester'];
                               $data2['course_id']=$values['course_id'];
                               if($values['optionalsubjects_onlinecheck'])
                               {
                                $data1['onlinecheck']=$values['optionalsubjects_onlinecheck']->activestatus;
                               }
                               else
                               {
                                $data1['onlinecheck']="";
                               }
                               $resultstudentoptionalsubject[]=$data2;
                            }

                        }
                       
                    }
                }
                else
                {
                    $resultstudentoptionalsubject=$emptyArray;
                }
                // Total subject show end
                // Total notice start
                $callAllNotice  = $this->notice_m->notic_record_5(array('notice_x_student.studentID'=>$loginuserID));
                if($callAllNotice)
                {
                    foreach($callAllNotice as $key => $value) 
                    {
                        $data4['noticeID']=$value['noticeID'];
                        $data4['adminID']=$value['adminID'];
                        $data4['title']=$value['title'];
                        $noticedata = $value['notice'];
                        $datanotice="";
                        preg_match_all('/<p><a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $noticedata,$noticeresult);
                        if (!empty($noticeresult))
                        {
                            // echo count($noticeresult); print_r($noticeresult[2][0]);die;
                            foreach ($noticeresult['href'] as $key => $valuefd)
                            {
                                $datanotice=$valuefd;
                            }
                            
                        }
                        // print_r($datanotice);die;
                        $noticetext=strip_tags(str_replace(array("\n", "\r","&nbsp;","&quot;"), '', $value['notice']));
                        $data4['notice']=$noticetext." ".$datanotice;
                        if($value['noticeDocument'])
                        {
                            $data4['noticeDocument']=base_url('uploads/notice/'.$value['noticeDocument']);
                        }
                        else
                        {
                            $data4['noticeDocument']="";
                        }                        
                        $data4['year']=$value['year'];
                        $data4['date']=date('d-m-Y H:i',strtotime($value['create_date']));
                        $data4['sendByUSerName']=$value['sendByUSerName'];
                        $allnotice[]=$data4;
                    }
                }
                else
                {
                    $allnotice=$emptyArray;
                }
                // Total notice end
                // Total subject start
                $totalsubject= $this->subject_m->get_join_where_subject_count($loginuserID,$student->classesID,$student->sub_coursesID);
                if($totalsubject)
                {
                    $countcard['totalsubject']=array('totalsubject' =>count($totalsubject));
                }
                else
                {
                    $countcard['totalsubject']=array('totalsubject' =>0);
                }
                // Total subject end
                // Total amount start
                $totalamountSend = $this->invoice_gen_m->TotalFee($loginuserID,$student->adminID);
                if($totalamountSend)
                {
                    $countcard['totalamountSend']=$totalamountSend;
                }
                else
                {
                    $countcard['totalamountSend']= array('amount' =>0);
                }
                // Total amount end
                //student detail start
                $studentdetails=$this->student_m->get_student(array('studentID'=>$loginuserID,'adminID'=>$student->adminID));
                $data['name']=$studentdetails->name;
                $data['father_name']=$studentdetails->father_name;
                $data['mother_name']=$studentdetails->mother_name;
                $data['sex']=$studentdetails->sex;
                $data['email']=$studentdetails->email;
                $data['phone']=$studentdetails->phone;
                $data['enrollment']=$studentdetails->enrollment;
                $data['yearsOrSemester']=$studentdetails->yearsOrSemester;
                $data['verifyemail']=$studentdetails->verifyemail;
                $data['verifymobileno']=$studentdetails->verifymobileno;
                $data['photo']=base_url('uploads/images/'.$studentdetails->photo);
                //student detail end
                // class details start
                $studentclassdetails = $this->student_m->get_class($student->classesID);
                $data3['classesID']=$studentclassdetails->classesID;
                $data3['adminID']=$studentclassdetails->adminID;
                $data3['classes']=$studentclassdetails->classes;
                $data3['duration']=$studentclassdetails->duration;
                $data3['mode']=$studentclassdetails->mode;
                $data3['fee']=$studentclassdetails->fee;
                // class details end
                $res['responseCode']=200;
                $res['message']="All Dashboard List";
                $res['cardvalue']= $countcard;
                $res['permanent_subject']=$resultstudentsubject;
                $res['optional_subject']=$resultstudentoptionalsubject;
                $res['studentdetails']=$data;
                $res['studentclassdetails']=$data3;
                $res['notice']=$allnotice;
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Loginuser-ID is incorrect";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="please Enter Loginuser-ID!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function clickattend_post()
    {
        $variable=$this->input->post('checktime');
        $loginuserID=$this->input->post('loginuserID');
        $subjectID=$this->input->post('subjectID');
        $professorID=$this->input->post('professorID');
        $semester=$this->input->post('semester');
        $course_id=$this->input->post('course_id');
        $time= date('h:i');
        $endTime = strtotime("+15 minutes", strtotime($variable));
        $starttime=strtotime("+16 minutes", strtotime($time));
        $lasttime= date('h:i', $starttime);
        $checktime= date('h:i', $endTime);
        // print_r($time);die;
        if($checktime && $loginuserID && $subjectID && $professorID && $semester && $course_id)
        {
            if($lasttime >= $checktime && $checktime >= $time)
            {
                $atd_date=date('Y-m-d');
                $year=date('Y');
                $year_mode=5;
                $activestatus=1;
                $alldata= array('classesID' =>$course_id,'sub_coursesID'=>0,'yearsOrSemester'=>$semester,'studentID'=>$loginuserID,'atd_date'=>$atd_date,'subjectID'=>$subjectID,'professorID'=>$professorID,'year'=>$year,'year_mode'=>$year_mode,'activestatus'=>$activestatus);
                $checkdata=$this->student_m->checkrequestattend($year_mode,$alldata,$loginuserID);
                if($checkdata)
                {
                    $res['responseCode']=200;
                    $res['message']="Online Lecture attending confirmation will be sent.";
                    $this->response($res, REST_Controller::HTTP_OK);
                }
                else
                {
                    $updatestudent=$this->student_m->requesttoattend($year_mode,$alldata,$loginuserID);
                    $res['responseCode']=200;
                    $res['message']="Online Lecture attending confirmation will be sent.";
                    $this->response($res, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="You can't attend class right now !";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Your field missing";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studentsubjectandattendance_post()
    {
        $loginuserID=$this->input->post('loginuserID');
        $yearsOrSemester=$this->input->post('yearsOrSemester');
        $emptyArray = array();
        if($loginuserID)
        {
            $student = $this->student_info_m->get_student_info($loginuserID);
            if($student)
            {
                // permanent Subject and attendance Start
                $array_subject = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$loginuserID,$yearsOrSemester);
                foreach ($array_subject as $key => $value)
                {
                    $array_subject[$key]['atd'] = $this->subject_m->attd_percent($value['subjectID'],$student->studentID);
                    $array_subject[$key]['subjectprogrees']=$this->subject_m->getsubjectprogrees($value['subjectID']);
                }
                $array_subjects=$array_subject;
                if($array_subjects)
                {
                  foreach ($array_subjects as $key => $values)
                  {
                    $data['subjectID']=$values['subjectID'];
                    $data['adminID']=$values['adminID'];
                    $data['yearsOrSemester']=$values['yearsOrSemester'];
                    $data['subject']=$values['subject'];
                    if($values['pdf'])
                    {
                        $pdf='uploads/syllabus/'.$values['pdf'];
                        if(file_exists($pdf))
                        {
                            $data['pdf']=base_url('uploads/syllabus/'.$values['pdf']);
                        }
                        else
                        {
                            $data['pdf']="File not found";
                        }                        
                    }
                    else
                    {
                        $data['pdf']="";
                    }
                    if($values['assignmentpdf'])
                    {
                        $pdf='uploads/syllabus/'.$values['assignmentpdf'];
                        if(file_exists($pdf))
                        {
                            $data['assignmentpdf']=base_url('uploads/syllabus/'.$values['assignmentpdf']);
                        }
                        else
                        {
                            $data['assignmentpdf']="File not found";
                        }
                    }
                    else
                    {
                        $data['assignmentpdf']="";
                    }
                    
                    if($values['atd'])
                    {
                        
                        if($values['atd']->present)
                        {
                            $data['atd']=$values['atd'];
                        } 
                        else
                        {
                            $a['num_attd']=$values['atd']->num_attd;
                            $a['present']=0;
                            $data['atd']=$a;
                        }  
                    }
                    else
                    {
                        $a['num_attd']=0;
                        $a['present']=0;
                        $data['atd']=$a;
                    }
                    if($values['subjectprogrees'])
                    {                     
                        foreach ($values['subjectprogrees'] as $key => $valuesss)
                        {
                            if($valuesss->topicid)
                            {
                                $data['subjectprogrees']=$values['subjectprogrees']; 
                            } 
                            else
                            {
                                $data['subjectprogrees']=$emptyArray;
                            }                           
                        }   
                                             
                    }
                    else
                    {
                        $data['subjectprogrees']=$emptyArray;
                    }
                    $result[]=$data;
                    
                  }
                }
                else
                {
                    $result=$emptyArray;
                }
                // permanent Subject and attendance End
                // optional Subject and attendance Start
                $arrayoptionalsubject=$this->subject_m->showoptionalsubjectbystudent($student->classesID,$student->sub_coursesID,$loginuserID,$yearsOrSemester);
                foreach ($arrayoptionalsubject as $key => $value) 
                {
                    $arrayoptionalsubject[$key]['atd'] = $this->subject_m->attd_percent($value['subjectID'],$student->studentID);
                    $arrayoptionalsubject[$key]['optionalsubjectprogress']=$this->subject_m->getoptionalsubjectprogrees($value['subjectID']);
                } 
                $arrayoptionalsubjects=$arrayoptionalsubject;
                if($arrayoptionalsubjects)
                {
                  foreach ($arrayoptionalsubjects as $key => $values)
                  {
                    $data1['subjectID']=$values['subjectID'];
                    $data1['adminID']=$values['adminID'];
                    $data1['yearsOrSemester']=$values['yearsOrSemester'];
                    $data1['subject']=$values['subject'];
                    if($values['pdf'])
                    {
                        $pdf='uploads/syllabus/'.$values['pdf'];
                        if(file_exists($pdf))
                        {
                            $data1['pdf']=base_url('uploads/syllabus/'.$values['pdf']);
                        }
                        else
                        {
                            $data1['pdf']="File not found";
                        }                        
                    }
                    else
                    {
                        $data1['pdf']="";
                    }
                    if($values['assignmentpdf'])
                    {
                        $pdf='uploads/syllabus/'.$values['assignmentpdf'];
                        if(file_exists($pdf))
                        {
                            $data1['assignmentpdf']=base_url('uploads/syllabus/'.$values['assignmentpdf']);
                        }
                        else
                        {
                            $data['assignmentpdf']="File not found";
                        }
                    }
                    else
                    {
                        $data1['assignmentpdf']="";
                    }
                    
                    if($values['atd'])
                    {
                        $data1['atd']=$values['atd'];
                    }
                    else
                    {
                        $data1['atd']="";
                    }
                    if($values['optionalsubjectprogress'])
                    {                        
                        $data1['optionalsubjectprogress']=$values['optionalsubjectprogress'];                       
                    }
                    else
                    {
                        $data1['optionalsubjectprogress']="";
                    }
                    $result1[]=$data1;
                    
                  }
                }
                else
                {
                    $result1=$emptyArray;
                }
                // optional Subject and attendance end
                $res['responseCode']=200;
                $res['message']="All Subject and attendance List";          
                $res['permanentsubject_attendance']=$result;
                $res['optionalsubject_attendance']=$result1;
                $this->response($res, REST_Controller::HTTP_OK);
                // print_r($this->data['classesRow']);die;
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Loginuser-ID is incorrect";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="please Enter Loginuser-ID!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function viewsubjecttopic_post()
    {
        $currentmonth=date('m');
        $currentyear=date('Y');
        $subjectID=$this->input->post('subjectID');
        $yearsOrSemester=$this->input->post('yearsOrSemester');
        $usertype=$this->input->post('usertype');
        $subjectname=$this->subject_m->getsubjectname($subjectID);
        $completeunit=$this->subject_m->get_subject_unit_topic($subjectID);
        if($subjectID && $yearsOrSemester)
        {
        if($currentmonth <= 6)
        {
          if($yearsOrSemester=="1st_Year" || $yearsOrSemester=="2nd_Year" || $yearsOrSemester=="3rd_Year")
          {
            $year=$currentyear-1;
          }
          else
          {
            $year=$currentyear;
          }
        }
        else
        {
            $year=$currentyear;
        }

        if($completeunit)
        {
            $data['subjectname']=$subjectname->subject;
            // $a=array();
            foreach ($completeunit as $key)
            {
              $topic=$this->subject_m->get_subject_unit_checktopic($key->unitID);
              $a[]=$topic;
            }
            $completetopic=$a;
            $topicconutvalue=0;
            foreach($completeunit as $completeunit)
            {
                $data['unitname']=$completeunit->unit_name;
                foreach($completetopic as $rows)
                { 
                    foreach($rows as $row)
                    {
                        if($completeunit->unitID==$row->unitID)
                        {
                            $checkunit=$this->subject_m->checkcompleteunit($subjectID,$yearsOrSemester,$year);
                            if($checkunit)
                            {
                                foreach($checkunit as $checkunit)
                                {
                                    $checktopic=$checkunit->topicid;
                                    $topicdate="";
                                    $totalattendance="";
                                    $totalpresent="";
                                    $create_at=$checkunit->create_at;
                                    $topicconut=explode(",",$checktopic);
                                    $checked =  '';
                                    foreach($topicconut as $topicconut=> $topicconutvalue)
                                    {
                                      if($topicconutvalue==$row->topicID)
                                      {
                                        $checked="checked";
                                        break;                                      
                                      }
                                    }
                                    if($checked)
                                    {
                                        $data['checked']=$checked;
                                        $data['topicID']=$row->topicID;
                                        $data['topicname']=$row->topic_name;
                                        $result[]=$data;
                                    }
                                    else
                                    {
                                        $data['checked']="";
                                        $data['topicID']=$row->topicID;
                                        $data['topicname']=$row->topic_name;
                                        $result[]=$data;
                                    }
                                }
                                $res['responseCode']=200;
                                $res['message']="All Topic List";          
                                $res['topic']=$result;
                                $this->response($res, REST_Controller::HTTP_OK);
                            } 
                            else
                            {
                                $res['responseCode']=0;
                                $res['message']="No Unit Found";
                                $this->response($res, REST_Controller::HTTP_OK);
                            }
                        }
                        else
                        {
                            $res['responseCode']=0;
                            $res['message']="No Unit Found";
                            $this->response($res, REST_Controller::HTTP_OK);
                        }
                    }
                }
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="No Data Found";
            $this->response($res, REST_Controller::HTTP_OK);
        }
      }
      else
      {
        $res['responseCode']=0;
        $res['message']="Any field missing!! ";
        $this->response($res, REST_Controller::HTTP_OK);
      }
    }
    function showattendance_post()
    {
        $loginuserID=$this->input->post('loginuserID');
        $subjectID=$this->input->post('subjectID');
        $yearsOrSemester=$this->input->post('yearsOrSemester');
        $currentyear=date('Y');
        $currentmonth=date('m');
        $emptyArray = array();
        if($currentmonth <= 6)
        {
          if($yearsOrSemester == "1st_Year" || $yearsOrSemester == "2nd_Year" || $yearsOrSemester == "3rd_Year")
          {
              $year=$currentyear-1;
              // print_r($year);die;
          }
          else
          {
            $year=$currentyear;
          }           
        }
        else
        {
          $year=$currentyear;
        }
        $subjectname=$this->db->select('subject')->where('subjectID',$subjectID)->where('status',1)->get('subject')->row();
        $data['subjectname']=$subjectname->subject;
        $checkinformation = array('subjectID'=>$subjectID,'studentID'=>$loginuserID );
        $getattendance=$this->student_m->getallstudentattendance($checkinformation,$yearsOrSemester,$year);
        if($getattendance)
        {
            foreach ($getattendance as $key)
            {
                if($key->year_mode != 5)
                {
                    $data['atdID']=$key->atdID;
                    $data['classesID']=$key->classesID;
                    $data['sub_coursesID']=$key->sub_coursesID;
                    $data['yearsOrSemester']=$key->yearsOrSemester;
                    $data['studentID']=$key->studentID;
                    $data['atd_date']=date('d-m-Y',strtotime($key->atd_date));
                    $data['atd']=$key->atd;
                    $data['subjectID']=$key->subjectID;
                    $data['professorID']=$key->professorID;
                    $data['year']=$key->year;
                    $data['year_mode']=$key->year_mode;
                    $result[]=$data;
                }
            }
            // $arr= array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            // $p=0;$ab=0;$pa=0;$aba=0;$abs=0;$pr=0;$ho=0;
            // for($a=0;$a<count($arr);$a++)
            // {
                // $data['monthname']=$arr[$a];
                // $month = sprintf("%02d", $a+1);
                // $this->load->library('calendar');
                // $days= $this->calendar->get_total_days($month,$year);
                // for($z=1;$z<=31;$z++)
                // {                                            
                    // $day = sprintf("%02d", $z);
                    // $dateselect =$year.'-'.$month.'-'.$day; 
                    // print_r($getattendance);die();
                    // foreach ($getattendance as $key)
                    // {
                    //   $present=$key->atd;
                    //   if($dateselect==$key->atd_date)
                    //   {
                    //       if($present==1)
                    //       { 
                    //         $pr=1;
                    //         $data['date']=$dateselect;
                    //         $data['studentattendance']="P";
                    //       }
                    //       else
                    //       { 
                    //         if($present==0)
                    //         { 
                    //           $ab=1;
                    //           $data['date']=$dateselect;
                    //           $data['studentattendance']="A";
                    //         }
                    //         else
                    //         { 
                    //           $ho=1;
                    //           $data['date']=$dateselect;
                    //           $data['studentattendance']="";
                    //         } 
                    //       }
                    //   }
                    //   else
                    //   {
                        // $ho=1;
                        // $data['date']=$dateselect;
                        // $data['studentattendance']="";
                      // }
                      // if($pr==1)
                      // { 
                      //   $pr=0;  
                      //   $data['date']=$day;
                      //   $data['studentattendance']="P";
                      //   $p=$p+1;
                      // }
                      // else if($ab==1)
                      // { 
                      //   $ab=0; 
                      //   $data['date']=$day;
                      //   $data['studentattendance']="A";
                      //   $abs=$abs+1;
                      // }
                      // else if($ho==1)
                      // { 
                      //   $ho=0; 
                      //   $data['date']=$day;
                      //   $data['studentattendance']="";
                      //   $aba=$aba+1;
                      // }
                      // else
                      // {
                      //   $data['date']=$day;
                      //   $data['studentattendance']="";
                      //   $aba=$aba+1;
                      // }
                      
                    // }
                    // $result[]=$data;
                // }
            // } 
            $res['responseCode']=200;
            $res['message']="Student Attendance";          
            $res['attendance']=$result;
            // $res['totalpresent']=$p;
            // $res['totalabsent']=$abs;
            $this->response($res, REST_Controller::HTTP_OK);
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="No data Found";
            $res['attendance']=$emptyArray;
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studentnotice_post()
    {
        $loginuserID=$this->input->post('loginuserID');
        $emptyArray = array();
        if($loginuserID)
        {
            $student = $this->student_info_m->get_student_info($loginuserID); 
            // print_r($student);die;
            if($student)
            {
                $year = date("Y");
                $callAllNotice = $this->notice_m->notic_record(array('studentID'=>$student->studentID));
                // print_r($callAllNotice);die;
                if($callAllNotice)
                {
                    foreach ($callAllNotice as $key => $value)
                    {
                        $data4['noticeID']=$value['noticeID'];
                        $data4['adminID']=$value['adminID'];
                        $data4['title']=$value['title'];
                        $data4['notice']=strip_tags(str_replace(array("\n", "\r","&nbsp;"), '', $value['notice']));
                        if($value['noticeDocument'])
                        {
                            $data4['noticeDocument']=base_url('uploads/notice/'.$value['noticeDocument']);
                        }
                        else
                        {
                            $data4['noticeDocument']="";
                        }                        
                        $data4['year']=$value['year'];
                        $data4['date']=date('d-m-Y H:i',strtotime($value['create_date']));
                        $data4['sendByUSerName']=$value['sendByUSerName'];
                        $allnotice[]=$data4;
                    }
                }
                else
                {
                    $allnotice=$emptyArray;
                }
                $res['responseCode']=200;
                $res['message']="Notice List";
                $res['notice']=$allnotice;
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="No data Found";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="please enter loginuserID";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studnetlms_post()
    {
        $usertype = $this->input->post("usertype");
        $adminID = $this->input->post("adminID");
        $loginuserID = $this->input->post("loginuserID");
        if($loginuserID)
        {
            $student = $this->student_info_m->get_student_info($loginuserID);
            if($student)
            {
                $subjects = $this->lms_m->get_join_where_subject_lms($loginuserID,$student->classesID,$student->sub_coursesID,$limitDefine=1,$student->yearsOrSemester); 
                if($subjects)
                {
                    foreach ($subjects as $key => $value)
                    {
                        $data['subjectID']=$value['subjectID'];
                        $data['adminID']=$value['adminID'];
                        $data['classesID']=$value['classesID'];
                        $data['sub_coursesID']=$value['sub_coursesID'];
                        $data['yearsOrSemester']=$value['yearsOrSemester'];
                        $data['subject']=$value['subject'];
                        $data['subject_mode']=$value['subject_mode'];
                        $data['percentage']=$value['percentage'];
                        $result[]=$data;
                    }
                    $res['responseCode']=200;
                    $res['message']="LMS Subject Name";
                    $res['subject']=$result;
                    $this->response($res, REST_Controller::HTTP_OK);
                }  
                else
                {
                    $res['responseCode']=0;
                    $res['message']="No subject assgin";
                    $res['subject']="";
                    $this->response($res, REST_Controller::HTTP_OK);
                }        
                
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="No data found";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="please enter loginuserID";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studentlmsshow_post()
    {
        $loginuserID = $this->input->post("loginuserID");
        $emptyArray = array();
        $id=$this->input->post('subjectID');
        $adminID=$this->input->post('adminID');
        if($loginuserID && $id && $adminID)
        {
            $this->db->where('subjectID',$id);
            $subjectname  = $this->db->get('subject')->row();
            if($subjectname)
            {
                $epub_notes = $this->lms_m->get_join_simple_epub_notes_with_pdf($adminID,$id);
                if($epub_notes)
                {
                    foreach ($epub_notes as $key => $value)
                    {
                        $data1['id']=$value->id;
                        $data1['subjectID']=$value->subjectID;
                        $data1['subject']=$subjectname->subject;
                        $data1['title']=$value->title;
                        if(file_exists('uploads/notes/'.$value->pdf))
                        {
                            $data1['pdf']=base_url('uploads/notes/'.$value->pdf);
                        }
                        else
                        {
                            $data1['pdf']="";
                        }
                        
                        $result1[]=$data1;
                    }
                }
                else
                {
                    $result1=$emptyArray;
                }
                $videos= $this->lms_m->get_join_simple_videos($adminID,$id);
                if($videos)
                {
                    // print_r($videos);die;
                    foreach ($videos as $key => $value)
                    {
                        $data['videoID']=$value->videoID;
                        $data['genID']=$value->genID;
                        $data['classesID']=$value->classesID;
                        $data['yearsOrSemester']=$value->yearsOrSemester;
                        $data['subjectID']=$value->subjectID;
                        $data['unitID']=$value->unitID;
                        // $data['professorID']=$value->professorID;
                        if(file_exists('uploads/videos/'.$value->src))
                        {
                            $avc=str_replace(" ", "", $value->src);
                            $data['src']=base_url('uploads/videos/'.$avc);
                        }
                        else
                        {
                            $data['src']="";
                        }
                        if($value->youtube_scr)
                        {
                            $data['youtube_scr']=$value->youtube_scr;
                        }
                        else
                        {
                            $data['youtube_scr']="";
                        }
                        $sql = $this->db->select('unit_name')->from('units')->where('unitID',$value->unitID)->get()->row();
                        $data['unit_name']=$sql->unit_name;
                        
                        $result[]=$data;
                    }
                }
                else
                {
                    $result=$emptyArray;
                }
                // $epub = $this->lms_m->get_join_simple_epub($adminID,$id);
                if(file_exists("uploads/pdf/".$id.".pdf"))
                {
                    $data3['subject']=$subjectname->subject;
                    $data3['pdf']=base_url('uploads/pdf/'.$id.".pdf");
                    $result3=$data3;
                    // base_url('uploads/videos/'.$id);
                }
                else
                {
                    $data3['subject']="";
                    $data3['pdf']="";                
                    $result3=$data3;
                }
                $res['responseCode']=200;
                $res['message']="LMS Data";
                $res['epub_notes']=$result1;
                $res['videos']=$result;
                $res['epub']=$result3;
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="No subject found";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field missing";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studentfee_post()
    {
        $username = $this->input->post("username");
        if($username)
        {
            $student = $this->student_m->get_single_student(array("username" => $username));
            if($student)
            {
                $invoices = $this->invoice_gen_m->get_invoice_for_student($student->studentID);
                if($invoices)
                {
                    foreach ($invoices as $key => $value)
                    {
                        $data['invoiceID']=$value->invoiceID;
                        $data['adminID']=$value->adminID;
                        $data['studentID']=$value->studentID;
                        $data['feetype']=$value->feetype;
                        $data['amount']=$value->amount;
                        $data['total_amount']=$value->total_amount;
                        if($value->paidamount)
                        {
                             $data['paidamount']=$value->paidamount;
                        }
                        else
                        {
                             $data['paidamount']="";
                        }
                       
                        $data['status']=$value->status;
                        if($value->paymenttype)
                        {
                            $data['paymenttype']=$value->paymenttype;
                        }
                        else
                        {
                            $data['paymenttype']="";
                        }
                        if($value->paiddate)
                        {
                            $data['paiddate']=$value->paiddate;
                        }
                        else
                        {
                            $data['paiddate']="";
                        }
                        
                        $data['discount']=$value->discount;
                        $data['tuitionFee']=$value->tuitionFee;
                        $data['totalfeepaidperyear']=$value->totalfeepaidperyear;
                        $data['yearsOrSemester']=$value->yearsOrSemester;
						if($value->outstanding_fees)
						{
							$data['outstanding_fees']=$value->outstanding_fees;
						}
						else
						{
							$data['outstanding_fees']="";
						}
                        
                        $result[]=$data;
                    }
                    $res['responseCode']=200;
                    $res['message']="Fee Data";
                    $res['fee']=$result;
                    $this->response($res, REST_Controller::HTTP_OK);
                }
                else
                {
                    $res['responseCode']=0;
                    $res['message']="No data found";
                    $this->response($res, REST_Controller::HTTP_OK);
                }
                
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Your username is incorrect";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field missing";
            $this->response($res, REST_Controller::HTTP_OK);
        }

    }
    function questionandanswer_view_post()
    {
        $usertype = $this->input->post("usertype");
        $adminID = $this->input->post("adminID");
        $loginuserID = $this->input->post('loginuserID');
        $uri_segment=$this->input->post('uri_segment');
        $emptyArray = array();
        $result= array();
        if($usertype=="Student" && $loginuserID)
        {
//pagination
        $config = array();
        $config['base_url'] = site_url('qa/index/all'); 
       $rem =   $this->qa_m->getPaginationrowvideo();
        $config['total_rows'] = $rem;
        $config['per_page'] = "30";
        $config["uri_segment"] = 4;
        $choice = ($config["total_rows"] / $config["per_page"]);
        $config["num_links"] = floor($choice);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;            
        $pagination = $this->pagination->create_links();
        $qa = $this->qa_m->get_join_where_subject_video($config["per_page"], $page,$usertype,$loginuserID);
        if($qa)
        {
            foreach ($qa as $key => $value)
            {
                if($loginuserID)
                {
                   $data['qaID']=$value->qaID;
                   if($value->subjectID)
                   {
                      $data['subjectID']=$value->subjectID;
                   }
                   else
                   {
                      $data['subjectID']="";
                   }
                   if($value->unitID)
                   {
                      $data['unitID']=$value->unitID;
                   }
                   else
                   {
                      $data['unitID']="";
                   }
                   if($value->title)
                   {
                      $data['title']=$value->title;
                   }
                   else
                   {
                      $data['title']="";
                   }
                   
                   $data['que']=strip_tags(str_replace(array("\n", "\r","&nbsp;"), '', $value->que));
                   $data['CreatedUserID']=$value->CreatedUserID;
                   $data['CreatedUsertype']=$value->CreatedUsertype;
                   $data['createTime']=date('d-m-Y h:i',strtotime($value->createTime));
                   $data['views']=$value->views;
                   $data['votes']=$value->votes;
                   $data['no_ans']=$value->no_ans;
                   $data['qa_status']=$value->qa_status;
                   $data['adminID']=$value->adminID;
                   $data['classesID']=$value->classesID;
                   $data['sub_coursesID']=$value->sub_coursesID;
                   $data['yearsOrSemester']=$value->yearsOrSemester;
                   $data['subject']=$value->subject;
                   if($value->unit_name)
                   {
                      $data['unit_name']=$value->unit_name;
                   }
                   else
                   {
                      $data['unit_name']="";
                   }
                   
                   $result[]=$data;
               }
            }
            $res['responseCode']=200;
            $res['message']="Question and Answer Data";
            if($result)
            {
                $res['Q&A']=$result;
            }
            else
            {
                $res['Q&A']=$emptyArray;
            }
            
            $res['pagination']=$pagination;
            $res['page']=$page;
            $this->response($res, REST_Controller::HTTP_OK);
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="No data found";
            $this->response($res, REST_Controller::HTTP_OK);
        }
        
       }
       else
       {
            $res['responseCode']=0;
            $res['message']="Any field missing";
            $this->response($res, REST_Controller::HTTP_OK);
       }
    }
    function answerview_post()
    {
        $loginuserID = $this->input->post('loginuserID');
        $usertype = $this->input->post('usertype');
        $qaID = $this->input->post('qaID');
        $emptyArray = array();
        if($qaID && $loginuserID && $usertype=="Student")
        {
            $this->db->where('qaID',$qaID);
            $qa =  $this->db->get('qa')->row();
            if($qa)
            {
                $num_views = $this->db->where('qaID',$qaID)->where('CreatedUserID',$loginuserID)->where('CreatedUsertype',$usertype)->get('qa_views')->num_rows();
                if($num_views==0)
                {
                    $data_qa_view = array(
                    'qaID'=>$qaID,
                    'CreatedUserID'=>$loginuserID,
                    'CreatedUsertype'=>$usertype
                    );
                    $this->db->insert('qa_views',$data_qa_view);
                    $data_update_que = array(
                    'views'=>$this->data['qa']->views+1,
                    ); 
                    $this->db->where('qaID',$qaID);
                    $this->db->update('qa',$data_update_que);
                }
                $name_of_qa = $this->db->select('name,studentID,photo')->where('studentID',$qa->CreatedUserID)->get('student')->row();
                $name_of_subject = $this->db->select('subject,subjectID,professorID')->where('subjectID',$qa->subjectID)->get('subject')->row();
                $this->data['name_of_professor'] = $this->db->select('name,professorID')->where('professorID',$name_of_subject->professorID)->get('professor')->row();

                $name_of_unit = $this->db->select('unit_name,unitID')->where('unitID',$qa->unitID)->get('units')->row();

                $ans_array   = $this->db->where('qaID',$qaID)->order_by('ansID','desc')->get('qa_ans')->result_array();
                if($ans_array)
                {
                    foreach ($ans_array as $key => $value)
                    {
                        if($value['CreatedUsertype']=='Professor')
                        {
                            $ans_array[$key]['name_photo'] = $this->db->select('name,photo')->where('professorID',$value['CreatedUserID'])->get('professor')->row_array(); 
                        }
                        else
                        {
                            $ans_array[$key]['name_photo'] = $this->db->select('name,studentID,photo')->where('studentID',$value['CreatedUserID'])->get('student')->row_array();
                        }   
                    }                
                    foreach ($ans_array as $key => $value)
                    {
                        $data['ansID']=$value['ansID'];
                        $data['qaID']=$value['qaID'];
                        $data['ans']=strip_tags(str_replace(array("\n", "\r","&nbsp;"), '', $value['ans']));
                        $data['CreatedUserID']=$value['CreatedUserID'];
                        $data['CreatedUsertype']=$value['CreatedUsertype'];
                        $data['createTime']=date('d-m-Y h:i',strtotime($value['createTime']));
                        $data['votes']=$value['votes'];
                        
                        $data1['name']=$value['name_photo']['name'];                        
                        if($value['name_photo']['studentID'])
                        {
                            $data1['studentID']=$value['name_photo']['studentID'];
                        }
                        else
                        {
                            $data1['studentID']="";
                        }
                        $data1['photo']=base_url('uploads/images/'.$value['name_photo']['photo']);
                        $data['name_photo']=$data1;
                       
                        $ans[] = $data;
                    }
                    
                }
                else
                {
                    $ans=$emptyArray;
                }
                $res['responseCode']=200;
                $res['message']="Answer View";          
                $res['ans']=$ans;
                $res['name_of_unit']=$name_of_unit;
                $res['name_of_subject']=$name_of_subject;
                $res['name_of_qa']=$name_of_qa;
                $res['num_views']=$num_views;
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="No data found";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field missing";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function askquestion_post()
    {
        $loginuserID=$this->input->post('loginuserID');
        $usertype=$this->input->post('usertype');
        $subjectID=$this->input->post('subjectID');
        $unitID=$this->input->post('unitID');
        $title=$this->input->post('title');
        $que=$this->input->post('que');
        $emptyArray = array();
        if($loginuserID && $usertype=="Student")
        {
            $student = $this->student_info_m->get_student_info($loginuserID);
            if($student)
            {
                $array_subject = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$loginuserID);
                if($array_subject)
                {
                    foreach ($array_subject as $key => $value)
                    {
                       $data['subjectID']=$value['subjectID'];
                       $data['adminID']=$value['adminID'];
                       $data['classesID']=$value['classesID'];
                       $data['sub_coursesID']=$value['sub_coursesID'];
                       $data['yearsOrSemester']=$value['yearsOrSemester'];
                       $data['subject']=$value['subject'];
                       $result[]=$data;
                    }
                }
                else
                {
                    $result=$emptyArray;
                }
                if($subjectID)
                {
                 $data = array(
                 'subjectID'=>$this->input->post('subjectID'),
                 'unitID'=>$this->input->post('unitID'),
                 'title'=>$this->input->post('title'),
                 'que'=>$this->input->post('que'),
                 'CreatedUserID'=>$loginuserID,
                 'CreatedUsertype'=>$usertype,
                 'createTime'=>date('Y-m-d H:i:s') 
                 );
                 $this->db->insert('qa',$data);
                $res['responseCode']=200;
                $res['message']="Your Question Send";          
                $this->response($res, REST_Controller::HTTP_OK);
                }
                else
                {
                    $res['responseCode']=200;
                    $res['message']="show subject";
                    $res['subject']=$result;
                    $this->response($res, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="No data found";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="any field missing";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function getunit_post()
    {
        $subjectID=$this->input->post('subjectID');
        $adminID=$this->input->post('adminID');
        if($subjectID && $adminID)
        {
            $showsubject=$this->db->select('subject')->from('subject')->where(array('subjectID' =>$subjectID,'adminID'=>$adminID))->get()->row();
            if($showsubject)
            {
               $getunit=$this->db->select('units.unitID,units.unit_name')->from('subject')->join('units','units.subjectID=subject.subjectID','left')->where(array('subject.subjectID' =>$subjectID,'subject.adminID'=>$adminID))->get()->result();
                if($getunit)
                {
                    foreach ($getunit as $key => $value)
                    {
                        if($value->unitID)
                        {
                            $data['unitID']=$value->unitID;
                        }
                        else
                        {
                            $data['unitID']="";
                        }
                        if($value->unit_name)
                        {
                            $data['unit_name']=$value->unit_name;
                        }
                        else
                        {
                            $data['unit_name']="";
                        }
                        $result[]=$data;               
                    }
                    $res['responseCode']=200;
                    $res['message']="show unit name";
                    $res['unit']=$result;
                    $this->response($res, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="No data found";
                $this->response($res, REST_Controller::HTTP_OK);
            }
                        
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="any field missing";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function hashkey_post()
    {
        $key=$this->input->post('key');
        $txnid=$this->input->post('txnid');
        $amount=$this->input->post('amount'); //Please use the amount value from database
        $productinfo=$this->input->post('productinfo');
        $firstname=$this->input->post('firstname');
        $email=$this->input->post('email');
        // $status = $this->input->post('status');
        // $postedhash= $this->input->post('hash');
        $salt="3dcVUBu24Z"; //Please change the value with the live salt for production environment

        //hash sequence
        //$responseHashSeq = $salt.'|'.$status.'|udf1|udf2|||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
// key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt;
        $responseHashSeq = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'||||||'.$salt;
        // print_r($responseHashSeq);die;
        $calculated_hash = hash("sha512", $responseHashSeq);
        if($calculated_hash)
        {
            $res['responseCode']=200;
            $res['message']="Transaction is valid";          
            $res['calculated_hash']=$calculated_hash;
            $this->response($res, REST_Controller::HTTP_OK);
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Transaction tampered";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function enquiryform_post()
    {
        $name=$this->input->post('name');
        $phone=$this->input->post('phone');
        $emailid=$this->input->post('emailid');
        $course=$this->input->post('course');
        $enquiry=$this->input->post('enquiry');
        if($name && $phone && $emailid && $course)
        {
            $formdata = array('name' =>$name,'phone'=>$phone,'email'=>$emailid,'course'=>$course,'enquiry'=>$enquiry,'adminID'=>2,);

            $registrationform=$this->student_m->registrationform($formdata);
            if($registrationform)
            {
                $email="itm@itmddn.com"; 
                $dataEamil['email']="itm@itmddn.com";                   
                $subject="A New Enquiry received from ITM Mobile App";
                $html='<h4>A New Enquiry received</h4><br/><label>Name : </lable><label>'.$name.'</lable><br/><lable>Email-Id : </lable><label>'.$emailid.'</lable><br/><lable>Phone : </lable><label>'.$phone.'</lable><br/><lable>Course Name : </lable><lable>'.$course.'</lable><br/><lable>Enquiry : </lable><label>'.$enquiry.'</lable><br/>';
                $fullname="ITM Mobile App";
                $dataEamil['institute']="ITM Mobile App";
                $dataEamil['superadminemail']="";
                $dataEamil['adminemail']="";
                $ssadmin=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
                $res['responseCode']=200;
                $res['message']="Enquiry Form Saved"; 
                $res['studentid']=$registrationform;         
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Data not saved";
                $this->response($res, REST_Controller::HTTP_OK);
            }

        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field is blank!!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    } 
    function applicationform_post()
    {
        $course=$this->input->post('course');
        $studentname=$this->input->post('studentname');
        $dob=$this->input->post('dob');
        $email=$this->input->post('email');
        $phone=$this->input->post('phone');
        $address=$this->input->post('address');
        $hsboard=$this->input->post('hsboard');
        $hsboardpercentage=$this->input->post('hsboardpercentage');
        $iboard=$this->input->post('iboard');
        $iboardpercentage=$this->input->post('iboardpercentage');
        $graduation=$this->input->post('graduation');
        $graduationpercentage=$this->input->post('graduationpercentage');
        $amount=$this->input->post('amount');
        if($course && $studentname && $dob && $email && $address)
        {
            $application = array('course' =>$course,'studentname'=>$studentname,'dob'=>$dob,'email'=>$email,'phone'=>$phone,'address'=>$address,'hsboard'=>$hsboard,'hsboardpercentage'=>$hsboardpercentage,'iboard'=>$iboard,'iboardpercentage'=>$iboardpercentage,'graduation'=>$graduation,'graduationpercentage'=>$graduationpercentage,'adminID'=>2,'paymentstatus'=>0 );
            $appilicationform=$this->student_m->applicationformsave($application);
            if($appilicationform)
            {
                $emailid="itm@itmddn.com"; 
                $dataEamil['email']="itm@itmddn.com";                   
                $subject="New Registration Form received from ITM Mobile App";
                $html='<h4>New Registration Form received</h4><br/><label>Name : </lable><label>'.$studentname.'</lable><br/><lable>Email-Id : </lable><label>'.$email.'</lable><br/><lable>Phone Number : </lable><label>'.$phone.'</lable><br/><lable>Date of Birth : </lable><label>'.$dob.'</lable><br/><lable>Course Name : </lable><lable>'.$course.'</lable><br/><lable>Address : </lable><label>'.$address.'</lable><br/>';
                $fullname="ITM Mobile App";
                $dataEamil['institute']="ITM Mobile App";
                $dataEamil['superadminemail']="";
                $dataEamil['adminemail']="";
                $ssadmin=emailBySendGrid($emailid,$fullname,$subject,$html,$dataEamil);
                $res['responseCode']=200;
                $res['message']="Registration Form Saved"; 
                $res['studentid']=$appilicationform;         
                $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Data not saved";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field is blank!!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function paymentstatus_post()
    {
        $paymentstaus=$this->input->post('paymentstatus');
        $studentid=$this->input->post('studentid');
        if($paymentstaus && $student)
        {
            $arrayName = array('paymentstatus' =>$paymentstaus);
            $checkpaymentstaus=$this->student_m->checkpaymentstaus($paymentstaus,$studentid);            
                $res['responseCode']=200;
                $res['message']="payment status saved";          
                $this->response($res, REST_Controller::HTTP_OK);           

        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field is blank!!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
        
    }
    function studentphotochange_post()
    {
        $studentID=$this->input->post('studentID');
        $photo=$this->input->post('photo');
        if($studentID && $photo)
        {
            $image = base64_decode($this->input->post("photo"));
            $image_name = md5(uniqid(rand(), true));
            $filename = $image_name . '.' . 'png';
            $path = "uploads/images/".$filename;
            file_put_contents($path . $filename, $image);
            $data_insert = array('photo'=>$filename);
            $this->db->where('studentID',$studentID);
            $this->db->update('student',$data_insert);
            $res['responseCode']=200;
            $res['message']="photo Change Successfully";          
            $this->response($res, REST_Controller::HTTP_OK);           

        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field is blank!!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studenttimetable_post()
    {
        $loginuserID=$this->input->post('loginuserID');
        $emptyArray = array();
        if($loginuserID)
        {
            $student = $this->student_info_m->get_student_info($loginuserID);
            if($student)
            {
                // Total subject show start
                $studentsubjects= $this->subject_m->get_subject_proffessor_join_studentdashboard($student->yearsOrSemester,$student->classesID,$student->studentID);
                if($studentsubjects)
                {
                    foreach ($studentsubjects as $key => $value)
                    {
                        $studentsubjects[$key]['onlinecheck']=$this->subject_m->get_subject_proffessor_join_studentonline($value['semester'],
                            $value['course_id'],$value['subjectID'],$value['professorID'],$loginuserID);
                    }
                    $studentsubject=$studentsubjects;
                    foreach ($studentsubject as $key => $values)
                    {
                        
                               $data1['days']=$values['days'];
                               $data1['times']=$values['times'];
                               $data1['subject']=$values['subject'];
                               $data1['subjectID']=$values['subjectID'];
                               $data1['studentactive']=$values['studentactive'];
                               $data1['name']=$values['name'];
                               $data1['professorID']=$values['professorID'];
                               $data1['semester']=$values['semester'];
                               $data1['course_id']=$values['course_id'];
                               if($values['onlinecheck'])
                               {
                                $data1['onlinecheck']=$values['onlinecheck']->activestatus;
                               }
                               else
                               {
                                $data1['onlinecheck']="";
                               }
                               $resultstudentsubject[]=$data1;                            
                       
                    }


                }
                else
                {
                    $resultstudentsubject=$emptyArray;
                }
                
                $studentoptionalsubjects = $this->subject_m->get_subject_proffessor_join_optionalsubjects_studentdashboard($student->yearsOrSemester,$student->classesID,$student->studentID);
                if($studentoptionalsubjects)
                {
                    foreach ($studentoptionalsubjects as $key => $value)
                    {
                        $studentoptionalsubjects[$key]['optionalsubjects_onlinecheck']=$this->subject_m->get_subject_proffessor_join_optionalsubjects_studentonline($value['semester'],
                            $value['course_id'],$value['subjectID'],$value['professorID'],$loginuserID);
                    }
                    $studentoptionalsubject=$studentoptionalsubjects;
                    foreach ($studentoptionalsubject as $key => $values)
                    {
                        
                               $data2['days']=$values['days'];
                               $data2['times']=$values['times'];
                               $data2['subject']=$values['subject'];
                               $data2['subjectID']=$values['subjectID'];
                               $data2['studentactive']=$values['studentactive'];
                               $data2['name']=$values['name'];
                               $data2['professorID']=$values['professorID'];
                               $data2['semester']=$values['semester'];
                               $data2['course_id']=$values['course_id'];
                               if($values['optionalsubjects_onlinecheck'])
                               {
                                $data1['onlinecheck']=$values['optionalsubjects_onlinecheck']->activestatus;
                               }
                               else
                               {
                                $data1['onlinecheck']="";
                               }
                               $resultstudentoptionalsubject[]=$data2;                       
                    }
                }
                else
                {
                    $resultstudentoptionalsubject=$emptyArray;
                }
                $res['responseCode']=200;
                $res['message']="Student Time Table";
                $res['permanent_subject']=$resultstudentsubject;
                $res['optional_subject']=$resultstudentoptionalsubject;
                 $this->response($res, REST_Controller::HTTP_OK);
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Loginuser-ID is incorrect";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="please Enter Loginuser-ID!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studentfeedback_post()
    {
        $msg=$this->input->post('feedback');
        $studentname=$this->input->post('studentname');
        $course=$this->input->post('course');
        $yearsOrSemester=$this->input->post('yearsOrSemester');
        if($msg && $studentname && $course)
        {
            $emailid="anjugairola@gmail.com"; 
            $dataEamil['email']="anjugairola@gmail.com";                   
            $subject="ITM App Feedback";
            $html='<label>Feedback : </lable><label>'.$msg.'</lable><br/><lable>Student Name : </lable><label>'.$studentname.'</lable><br/><lable>Course Name : </lable><lable>'.$course.'</lable><br/><lable> Years-Semester : </lable><lable>'.$yearsOrSemester.'</lable><br/>';
            $fullname="ITM Mobile App";
            $dataEamil['institute']="ITM Mobile App";
            $dataEamil['superadminemail']="itmcollegeddn@gmail.com";
            $dataEamil['adminemail']="nishantitm5@gmail.com";
            $ssadmin=emailBySendGrid($emailid,$fullname,$subject,$html,$dataEamil);
            $res['responseCode']=200;
            $res['message']="Feedback Sent Successfully";        
            $this->response($res, REST_Controller::HTTP_OK);
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Any field is blank!!";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function getallcourses_get()
    {
        $class=$this->student_m->get_classes();
        if($class)
        {
            foreach ($class as $key => $value)
            {
                $data['classesID']=$value->classesID;
                $data['adminID']=$value->adminID;
                $data['classes']=$value->classes;
                $data['duration']=$value->duration;
                $data['mode']=$value->mode;
                $data['fee']=$value->fee;
                $data3[]=$data;
            }
            
            // class details end
            $res['responseCode']=200;
            $res['message']="All Course List";
            $res['studentclassdetails']=$data3;
            $this->response($res, REST_Controller::HTTP_OK);
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="No data Found";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function studentgallery_get()
    {
        for($i=1;$i<=29;$i++)
        {
            $data["photo"] = base_url('uploads/appgallery/'.$i.'.jpg');
            $data3[] = $data;
        }
        
        // $data1['photo']=base_url('uploads/images/'.$value['name_photo']['photo']);
        //                 $data['name_photo']=$data1;
                       
                        
        // $data["photo2"] = base_url('uploads/appgallery/2.jpg');
        // $data["photo3"] = base_url('uploads/appgallery/3.jpg');
        // $data["photo4"] = base_url('uploads/appgallery/4.jpg');
        // $data["photo5"] = base_url('uploads/appgallery/5.jpg');
        // $data["photo6"] = base_url('uploads/appgallery/6.jpg');
        // $data["photo7"] = base_url('uploads/appgallery/7.jpg');
        // $data["photo8"] = base_url('uploads/appgallery/8.jpg');
        // $data["photo9"] = base_url('uploads/appgallery/9.jpg');
        // $data["photo10"] = base_url('uploads/appgallery/10.jpg');
        // $data["photo11"] = base_url('uploads/appgallery/11.jpg');
        // $data["photo12"] = base_url('uploads/appgallery/12.jpg');
        // $data["photo13"] = base_url('uploads/appgallery/13.jpg');
        // $data["photo14"] = base_url('uploads/appgallery/14.jpg');
        // $data["photo15"] = base_url('uploads/appgallery/15.jpg');
        // $data["photo16"] = base_url('uploads/appgallery/16.jpg');
        // $data["photo17"] = base_url('uploads/appgallery/17.jpg');
        // $data["photo18"] = base_url('uploads/appgallery/18.jpg');
        // $data["photo19"] = base_url('uploads/appgallery/19.jpg');
        // $data["photo20"] = base_url('uploads/appgallery/20jpg');
        // $data["photo21"] = base_url('uploads/appgallery/21.jpg');
        // $data["photo22"] = base_url('uploads/appgallery/22.jpg');
        // $data["photo23"] = base_url('uploads/appgallery/23.jpg');
        // $data["photo24"] = base_url('uploads/appgallery/24.jpg');
        // $data["photo25"] = base_url('uploads/appgallery/25.jpg');
        // $data["photo26"] = base_url('uploads/appgallery/26.jpg');
        // $data["photo27"] = base_url('uploads/appgallery/27.jpg');
        // $data["photo28"] = base_url('uploads/appgallery/28.jpg');
        // $data["photo29"] = base_url('uploads/appgallery/29.jpg');
        // $data3[]=$data;
        $res['responseCode']=200;
        $res['message']="All Gallery List";
        $res['gallery']=$data3;
        $this->response($res, REST_Controller::HTTP_OK);
        
    }
    function notificationsendnonloginuser_post()
    {
        $androidID=$this->input->post('androidID');
        $devicetoken=$this->input->post('devicetoken');
        $devicetype=$this->input->post('devicetype');
        if($androidID && $devicetoken && $devicetype)
        {
            $arrayName = array('androidID' =>$androidID,'androidkey'=>$devicetoken,'androiddevice'=>$devicetype,'date'=>date('Y-m-d'));
            $nonloginuser = $this->student_info_m->notificationsendnonlogin($arrayName,$androidID);
            if($nonloginuser)
            {
                $message="Welcome To ITM";
                $title="ITM";
                $getdevicetoken1=$devicetoken;
                if($getdevicetoken1)
                {                               
                    $this->sendnotification($getdevicetoken1,$message,$title);
                    $res['responseCode']=200;
                    $res['message']="Notification Sent";
                    $this->response($res, REST_Controller::HTTP_OK);

                }
            }
            else
            {
                $res['responseCode']=0;
                $res['message']="Error";
                $this->response($res, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $res['responseCode']=0;
            $res['message']="Error";
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    function sendnotification($deviceToken,$messages,$titles)
    {
        define( 'API_ACCESS_KEY', 'AAAABWWT24g:APA91bFmgbOGaD5AZtsljmy4M2qsINg9nGlRGqGGhiyqCtSgkhhr4ybaZUSyDg6luIg6nWKfQ2ohpVCEXGXORIAn15QGf8UUjsVOCOMhd8vGIoFylZcQxAaVUPjNrL43cnnTXHrHSKoR' );


        $to = $deviceToken;

        // prep the bundle
        $msg = array
        (
            'message'   => $messages,
            'title'     => $titles,
            'body' => $messages,
            'vibrate'   => 1,
            'sound'     => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );

        $fields = array
        (
            'to'    => $to,
            'data'  => $msg
        );
         
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
         
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        return $result;
    }
}
    	
