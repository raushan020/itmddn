<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Notice extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("notice_m");
		$this->load->model("subject_m");
		$this->load->model("alert_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$this->load->model("classes_m");
		$this->load->model("student_info_m");
		$this->load->model("professor_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('notice', $language);	

	}



	public function index() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");
		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
		$this->data['professor'] = $this->notice_m->get_professor_by_superAdmin($adminID);
		if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner" || $usertype == "Professor") {
			$year = date("Y");
			// $this->data['notices'] = $this->notice_m->get_order_by_notice($adminID);
			if ($this->input->post('noticetypeFilter')) {
				$this->session->set_userdata('FilterNoticetype', $this->input->post('noticetypeFilter'));
			}
			if ($this->input->post('noticefromFilter')) {
				// print_r($this->input->post('noticefromFilter'));die;
				$this->session->set_userdata('Filternoticefroms', $this->input->post('noticefromFilter'));
				// echo date('Y-m-d',strtotime(($this->session->userdata('Filternoticefrom'))));die;
			}
			if ($this->input->post('noticetoFilter')) {
				$this->session->set_userdata('Filternoticeto', $this->input->post('noticetoFilter'));
			}
			if ($this->input->post('noticeprofessorFilter')) {
				$this->session->set_userdata('Filternoticeprofessor', $this->input->post('noticeprofessorFilter'));
			}
			if ($this->input->post('classesID')) {
				$this->session->unset_userdata('FilterNoticesubCourseID');
				$this->session->unset_userdata('FilterNoticeyearSemesterID');
				$this->session->set_userdata('FilterNoticeclassesID', $this->input->post('classesID'));
			}
	        if ($this->input->post('subCourseID')) {
				$this->session->set_userdata('FilterNoticesubCourseID', $this->input->post('subCourseID'));
			}
		  	if ($this->input->post('yearSemesterID')) {
				$this->session->set_userdata('FilterNoticeyearSemesterID', $this->input->post('yearSemesterID'));
			}
	
			$this->data["subview"] = "notice/index";
			$this->load->view('_layout_main', $this->data);
		}elseif ($usertype == "Accountant"){
					$year = date("Y");
 		/*=> $userdata->userID,*/
			$usertype = $this->session->userdata("usertype");
			$adminID = $this->session->userdata("loginuserID");
			$this->data['notices'] = $this->notice_m->get_user_notice($adminID);
			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);
		}elseif ($usertype == "Professor"){
			$year = date("Y");
			$usertype = $this->session->userdata("usertype");
			$userID = $this->session->userdata("loginuserID");

			$this->data['notices'] = $this->notice_m->get_user_notice($userID); 

			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);
		
		}elseif ($usertype == "Student" || $usertype == "Parent") {

			$loginuserID = $this->session->userdata("loginuserID");
			$student = $this->student_info_m->get_student_info($loginuserID); 

			
			$year = date("Y");
			$callAllNotice = $this->notice_m->notic_record(array('studentID'=>$student->studentID));
			 foreach ($callAllNotice as $key => $value) {

				if($value['usertype']=='Professor'){

				 	$callAllNotice[$key]['name'] ="Professor" ;
					$callAllNotice[$key]['bysend'] = "Professor";

				}elseif($value['usertype']=='ClgAdmin'){
					$callAllNotice[$key]['name'] = "Super Admin";
					$callAllNotice[$key]['bysend'] = "Super Admin";	
				}elseif ($value['usertype']=='Accountant') {
					 
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant";
				}elseif ($value['usertype']=='Admin') {
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
			}	

			$this->data['notice']  = $callAllNotice;	


			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);

		}else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	protected function rules() {

		$rules = array(
				
				array(

					'field' => 'title', 

					'label' => $this->lang->line("notice_title"), 

					'rules' => 'trim|required|max_length[128]'

				), 

				array(

					'field' => 'date', 

					'label' => $this->lang->line("notice_date"),

					'rules' => 'trim|required|max_length[10]'

				),

				array(

					'field' => 'classesID[]', 

					'label' => 'Course',

					'rules' => 'callback_courseWith_semester',

				)


			);

		return $rules;

	}



	public function add() {
		// print_r($this->data["siteinfos"]->email);die;
		date_default_timezone_set('Asia/Kolkata');
		$usertype = $this->session->userdata("usertype");
		$loginname=$this->session->userdata("name");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$year = date("Y");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin' || $usertype == 'Professor' || $usertype == 'Accountant' || $usertype == 'Senior_Examiner' || $usertype == 'Assistant_Examiner') 
		{
			// if($usertype == 'Professor')
			// {
			// 	$department = $this->professor_m->get_alldepartmentbyprofessor($loginuserID);
			// 	foreach ($department as $key => $value)
			// 	{
			// 		$this->data['classes'] = $this->professor_m->get_classes($value->departmentID);
			// 	}
			// 	// print_r($this->data['classes']);die;
                
			// }
			// else
			// {
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			// }
		    
		 
			if($_POST) 
			{

		        $rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) 
				{
					$this->data['form_validation'] = validation_errors();
					$this->data["subview"] = "notice/add";
					$this->load->view('_layout_main', $this->data);			
				} 
				else 
				{
					$subCourseID  = $this->input->post('sub_coursesID');
					// $yearSemesterID = $this->input->post('yearOrSemester');
					$num = 1;
					$config['upload_path'] = "./uploads/notice";
					$config['allowed_types'] = "gif|jpg|png|pdf|docx|doc|xls|xlsx|txt|php|ppt|pptx";
					$config['encrypt_name'] = TRUE;
					$this->load->library('upload', $config);
					$this->upload->do_upload();
					$file_name = $this->upload->data();
					$array_notice = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"create_date" => date('Y-m-d H:i:s'),
						"status" => $num,
						'usertype' => $usertype,
						'noticeDocument' => $file_name['file_name'],
						'userID'=> $loginuserID
					);
					$noticeID = $this->notice_m->insert_notice($array_notice);
					$classesID  = $this->input->post('classesID');
					$coursemixyear = '';
					$class_count = count($classesID);

					for ($i=0; $i < count($classesID); $i++) 
					{
    					$semesterOryear = $this->input->post('yearOrSemester'.$classesID[$i]);
    					$array['classesID'] = $classesID[$i];
    					$array['yearsOrSemester'] = implode(',', $semesterOryear);
    					$array['noticeID'] = $noticeID;
    					//$this->notice_m->notice_x_classes($array_clases, $i, $class_count , $classesID);
    					$this->db->where('noticeID',$array['noticeID']);
    					$this->db->where('classesID',$array['classesID']);
    					$num = $this->db->get('notice_x_classes')->num_rows();
    					if($num==0)
    					{
    					 	$this->db->insert('notice_x_classes',$array);
    					}
    					else
    					{
                            $this->db->where('noticeID',$array['noticeID']);
                            $this->db->where('classesID',$array['classesID']);	
                            $this->db->update('notice_x_classes',$array);	
    					}
    					$count_one = $class_count-1;   
    					if($count_one==$i)
    					{
        					$this->db->where('noticeID',$array['noticeID']);
        					$this->db->where_not_in('classesID',$classesID);
        					$this->db->delete('notice_x_classes');	
    					}
    					// for student
                        $this->db->select('phone,studentID,email,name,device_token');
                        $this->db->where('classesID',$classesID[$i]);
                        $this->db->where('status',1);
                        $this->db->where_in('yearsOrSemester',$semesterOryear);
                        $object = $this->db->get('student')->result();
    					if($object)
    					{
    			// 			$this->load->library('fcm');
    			// 			foreach ($object as $key => $value) 
    			// 		 	{
	    		// 				$token = $value->studentID; // push token
							// 	$message = "Test notification message";
							// 	$this->fcm->setTitle('Test FCM Notification');
							// 	$this->fcm->setMessage($message);
							// 	$payload = array('notification' => '');
							// 	$this->fcm->setPayload($payload);
							// 	$json = $this->fcm->getPush();
							// 	$p = $this->fcm->send($token, $json);
							// }
							// echo "<pre>";
							// print_r($object);
							// exit();
    					 	foreach ($object as $key => $value) 
    					 	{
								$student_email[] = $value->email;
    					 	    $student_phone[] = $value->phone;
            					$studentID_array[] = $value->studentID; 		
            					$this->db->where('studentID',$value->studentID);
            					$this->db->where('noticeID',$noticeID);
            					$num = $this->db->get('notice_x_student')->num_rows();
            					if($num==0)
            					{
        					 	 	$data = array(
            					 	    'studentID' =>$value->studentID,
            					 	    'noticeID'=>$noticeID,
            					 	    'sendByUSer'=>$this->session->userdata('usertype'),
            					 	    'sendByUSerName'=>$this->session->userdata('name')
        					 	 	);
        					 	 	$this->db->insert('notice_x_student',$data);
            					}
            					$message="New notice received from $loginname";
            					$title="Notice";
            					$getdevicetoken1=$value->device_token;
            					if($getdevicetoken1)
            					{            					
            						// $this->sendnotification($getdevicetoken1,$message,$value->studentID,$title);

            					}
            					
    					    }
                        	
        					$count_one = $class_count-1;   
        					if($count_one==$i)
        					{
            					$this->db->where('noticeID', $noticeID);
            					$this->db->where_not_in('studentID',$studentID_array); 
            					$this->db->delete('notice_x_student');	
    					    }
    					}
					}
					// mobile MSG Started
					// $sendmsg=sendnoticetostudent($this->input->post("title"),$this->input->post("date"),$this->input->post("notice"),$student_phone);
					// mobile msg end
					$where = "adminID = '".$adminID."' and create_usertype = 'ClgAdmin' and status = 1 and usertype = 'Admin'";
					$arr = array();
				    $results =$this->db->select('email')->from('user')->where($where)->get()->result_array();
				    foreach($results as $rows)
				    {
				        $arr[] = array($rows['email']);
				    }
				    $arrs = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($arr), ENT_NOQUOTES));
				    $admin_user =  str_replace('"','',$arrs);
					$dataEamil['title'] = $this->input->post("title");
					$dataEamil['date'] = date("Y-m-d", strtotime($this->input->post("date")));
					$dataEamil['description'] = strip_tags(html_entity_decode($this->input->post("notice")));
					$superadmin=$this->professor_m->getsuperadminid($adminID);
		            $subject="New notice received from $loginname";
		            $dataEamil['superadminemail']="";
		            $dataEamil['adminemail']=""; 
		            $dataEamil['institute']=$this->data["siteinfos"]->sname;
		             

					foreach($student_email as $stu_email)
					{
						
						if($stu_email != null)
						{	
						    if(preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/",$stu_email))
						    {
						    	// print_r($stu_email);die;			
								$email=$stu_email;
				            	$name=$this->db->select('name')->where('email',$stu_email)->get('student')->row();
								$fullname=$name->name;
								$dataEamil['name']=$name->name;
								$html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
								$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil); 
								// $sendmail= true;
						    }
						    else
						    {
						    	$sendmail= true;
						    }		
					 	    
						// print_r($sendmail);die;
					   }
					   else
					   {
					   	 $sendmail= true;
					   }
						
						// print_r($send_mail);die;
					}
					if($sendmail == true || $sendmail == 1)
					{
						// $subjectadmin="A new notice from ITM software";
		    //         	$dataEamil['superadminemail']=$superadmin->email;
			   //          $dataEamil['adminemail']=$arr; 
			   //          $htmladmin=$this->load->view('emailTemplates/studentaddition/noticeemaileradmin',$dataEamil, true);
            			// $sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
            			if($usertype=='Professor')
			            	{
			            		$dataEamil['email']=$this->session->userdata('email');
			            		$dataEamil['proferrorname']=$this->session->userdata('name');
			            		$date=date("Y-m-d", strtotime($this->input->post("date")));
			            		$subjectaccountant="New notice published successfully on $date";
			            	$htmlaccountant=$this->load->view('emailTemplates/studentaddition/noticeemailerprofessor',$dataEamil, true);
			            	$sendmail=emailBySendGridaccountant($subjectaccountant,$htmlaccountant,$dataEamil);
			            	
			            	}
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("notice/index"));
					}
					else
					{
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				}
			} 
			else 
			{
				$this->data["subview"] = "notice/add";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$loginuserID = $this->session->userdata("loginuserID");


		$year = date("Y");

		if($usertype == "Admin" ||  $usertype == 'ClgAdmin' || $usertype == 'Professor' || $usertype == 'Accountant') {

			$id = htmlentities(($this->uri->segment(3)));
			if((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
				$this->data['notice_x_classes'] = $this->notice_m->get_classes_by_notice($this->data['notice']->noticeID);
				
					if($_POST) {

$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors();

					$this->data["subview"] = "notice/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
					$subCourseID  = $this->input->post('sub_coursesID');
					// $yearSemesterID = $this->input->post('yearOrSemester');
					$num = 1;



					$array_notice = array(
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num,
						'usertype' => $usertype,
						'userID'=> $loginuserID
					);

if($_FILES['userfile']['name']!=''){
	if ($this->data['notice']->noticeDocument!='') {
		unlink(FCPATH.'uploads/notice/'.$this->data['notice']->noticeDocument);
	}

				    $config['upload_path'] = "./uploads/notice";
					$config['allowed_types'] = "gif|jpg|png|pdf|jpeg";
					$config['encrypt_name'] = TRUE;
					$config['max_size'] = '5120';
					$config['max_width'] = '3000';
					$config['max_height'] = '3000';
					$this->load->library('upload', $config);
					$this->upload->do_upload();
					$file_name = $this->upload->data();
					$array_notice['noticeDocument'] = $file_name['file_name'];
}

$this->notice_m->update_notice($array_notice, $id);
$noticeID = $id;


$classesID  = $this->input->post('classesID');
$coursemixyear = '';
 $class_count = count($classesID);
for ($i=0; $i < count($classesID); $i++) { 


$semesterOryear = $this->input->post('yearOrSemester'.$classesID[$i]);
					$array['classesID'] = $classesID[$i];
					$array['yearsOrSemester'] = implode(',', $semesterOryear);
					$array['noticeID'] = $noticeID;
					//$this->notice_m->notice_x_classes($array_clases, $i, $class_count , $classesID);
$this->db->where('noticeID',$array['noticeID']);
$this->db->where('classesID',$array['classesID']);
$num = $this->db->get('notice_x_classes')->num_rows();

if($num==0){
 	 	$this->db->insert('notice_x_classes',$array);
 }else{
 $this->db->where('noticeID',$array['noticeID']);
 $this->db->where('classesID',$array['classesID']);	
 $this->db->update('notice_x_classes',$array);	
 }
$count_one = $class_count-1;   
if($count_one==$i){
$this->db->where('noticeID',$array['noticeID']);
$this->db->where_not_in('classesID',$classesID);
$this->db->delete('notice_x_classes');	
}
// for student
       $this->db->select('studentID');
       $this->db->where('classesID',$classesID[$i]);
        $this->db->where('status',1);
       $this->db->where_in('yearsOrSemester',$semesterOryear);
 	   $object = $this->db->get('student')->result();
if($object){
 	foreach ($object as $key => $value) {
$studentID_array[] = $value->studentID; 		
$this->db->where('studentID',$value->studentID);
$this->db->where('noticeID',$noticeID);
$num = $this->db->get('notice_x_student')->num_rows();
if($num==0){
 	 	$data = array(
 	    'studentID' =>$value->studentID,
 	    'noticeID'=>$noticeID,
 	    'sendByUSer'=>$this->session->userdata('usertype'),
 	    'sendByUSerName'=>$this->session->userdata('name')
 	 	);
 	 	$this->db->insert('notice_x_student',$data);
 }

 	 }
$count_one = $class_count-1;   
if($count_one==$i){
$this->db->where('noticeID', $noticeID);
$this->db->where_not_in('studentID',$studentID_array);
$this->db->delete('notice_x_student');	
}
}

}
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					// print_r($array);
					// exit();

					redirect(base_url("notice/index"));

				}

					} else {

						$this->data["subview"] = "notice/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}




	public function send_notice_email()
	{
 

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");
		
		$year = date("Y");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin') {

		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

		$this->data['classes'] = $this->notice_m->get_notice_limit($adminID);

			if($_POST) {
			
				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors();

					$this->data["subview"] = "notice/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
					
					$classesID  = $this->input->post('classesID');
					$subCourseID  = $this->input->post('sub_coursesID');
					$yearSemesterID = $this->input->post('yearsOrSemester');
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						'classesID' => $classesID, 
						'sub_coursesID' => $subCourseID,
						'yearsOrSemester' => $yearSemesterID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num,
						

					);

				
					$noticeID = $this->notice_m->insert_notice($array);

					$array['notice_type'] = $this->input->post("notice_type");

					$array['noticeID'] = $noticeID;

					           $this->notice_m->notice_x_email_student($array);
					           foreach ($object as $name => $address)
								{
								        $this->email->clear();

								        $this->email->to($address);
								        $this->email->from('your@example.com');
								        $this->email->subject('Here is your info '.$name);
								        $this->email->message('Hi '.$name.' Here is the info you requested.');
								        $this->email->send();
								}

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));


					redirect(base_url("notice/index"));

				}

			} else {

				$this->data["subview"] = "notice/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}
	}

	
	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->notice_m->get_join_notice_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->notice_m->make_datatables($adminID);
// print_r($posts);die;
            // $totalFiltered  = $this->student_m->get_filtered_data();
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['date'] = date('d-m-Y H:i',strtotime($post->noticeDate));
				$nestedData['classes'] = $post->classes;
				$nestedData['title'] = $post->title;
				$nestedData['notice_type'] = $post->notice_type;
                $nestedData['notice'] = substr($post->notice,0,20).'...';
                $nestedData['yearorsemester'] = $post->yearsemester;
                if($post->usertype=="Admin" || $post->usertype=="Senior_Examiner" || $post->usertype=="Assistant_Examiner")
                {
                	$name = $this->notice_m->notice_byname('user',$post->userID,'userID');
                	$nestedData['sender']=$name->name;
                }
                else
                {
                	$nestedData['sender']=$post->professorname;
                }
                

   			if($usertype == "Admin" || $usertype == 'ClgAdmin' || $post->usertype=="Professor" || $usertype == "superadmin" || $usertype == 'Professor' || $usertype == 'Accountant') {
   		    $buttons     = btn_view('notice/view/'.$post->noticeID, $this->lang->line('view')).btn_edit('notice/edit/'.$post->noticeID, $this->lang->line('edit')).btn_delete('notice/delete/'.$post->noticeID, $this->lang->line('delete'));
 		

			    $nestedData['action'] = $buttons;
			}
               
                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

      
            
        echo json_encode($json_data); 
}


	

	public function view() 
	{
		$id = htmlentities(($this->uri->segment(3)));
		if((int)$id) 
		{
			$usertype = $this->session->userdata("usertype");
			if($usertype == "ClgAdmin" || $usertype == "Admin" || $usertype == "Accountant" || $usertype == "Professor") 
			{ 
				$this->data['notice'] = $this->notice_m->get_notice($id);
				if($this->data['notice']!=null)
				{
					$this->data['sender'] = $this->notice_m->sender_data($this->data['notice']->userID,$this->data['notice']->usertype);
					if($this->data['notice']) 
					{
						$this->data["subview"] = "notice/view";
						$this->load->view('_layout_main', $this->data);
					}
				}
				else 
				{
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			}
			else if($usertype=='Student')
			{
				$this->data['notice'] = $this->notice_m->notice_single($this->session->userdata('loginuserID'),$id);
				$this->data['sender'] = $this->notice_m->sender_data($this->data['notice']->userID,$this->data['notice']->usertype);  
					
					$this->notice_m->update_notice_x_student_read($this->session->userdata('loginuserID'),$id);
					$this->data["subview"] = "notice/view";
					$this->load->view('_layout_main', $this->data);
				/*if($this->data['notice']!=null)
				{
					
				} 
				else 
				{
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}*/
			}
		}   
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function hide_Notice_icon(){		
$data = array( 
'iconStatus'=>0
);
	$this->db->where('studentID',$this->session->userdata('loginuserID'));
	$this->db->update('notice_x_student',$data);
}

	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor") {

			$id = htmlentities(($this->uri->segment(3)));

			if((int)$id) {

				$this->notice_m->delete_notice($id);
				$this->notice_m->delete_notice_x_student($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("notice/index"));

			} else {

				redirect(base_url("notice/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	function date_valid($date) {

		if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);   

	        $dd = $arr[0];            

	        $mm = $arr[1];              

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    } 

	} 



	public function print_preview() {

		$id = htmlentities(($this->uri->segment(3)));

		if((int)$id) {

			$this->data['notice'] = $this->notice_m->get_notice($id);

			if($this->data['notice']) {



				$this->load->library('html2pdf');

			    $this->html2pdf->folder('./assets/pdfs/');

			    $this->html2pdf->filename('Report.pdf');

			    $this->html2pdf->paper('a4', 'portrait');

			    $this->data['panel_title'] = $this->lang->line('panel_title');

				$html = $this->load->view('notice/print_preview', $this->data, true);

				$this->html2pdf->html($html);

				$this->html2pdf->create();

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}	

	}


function ResetCourses(){

$this->session->unset_userdata('FilterNoticeclassesID');
$this->session->unset_userdata('FilterNoticesubCourseID');
$this->session->unset_userdata('FilterNoticeyearSemesterID');	   
}
function ResetSubcourses(){
$this->session->unset_userdata('FilterNoticesubCourseID');
}

function ResetSemesterYear(){

$this->session->unset_userdata('FilterNoticeyearSemesterID');
	   
}
function ResetAllfilter(){
	$this->session->unset_userdata('FilterNoticetype');
	$this->session->unset_userdata('Filternoticefroms');
	$this->session->unset_userdata('Filternoticeto'); 
	$this->session->unset_userdata('Filternoticeprofessor');
}

function ResetNoticetype(){
	$this->session->unset_userdata('FilterNoticetype');
}
function ResetNoticefrom(){
	$this->session->unset_userdata('Filternoticefroms');
	$this->session->unset_userdata('Filternoticeto');
	
}
function ResetProfessorFitlter(){
	$this->session->unset_userdata('Filternoticeprofessor');
}
	

	function courseWith_semester(){

		if (isset($_POST['classesID'])) {
		   $classes_countID =count($_POST['classesID']);
		   $classesID  =  $_POST['classesID']; 
		}else{
		 $classes_countID =0;
		}

   if($classes_countID==0){

	   	$this->form_validation->set_message("classesID","Please Select Atleast One Course");
	   	$retrun =  false;
   	}else{
   	 	for ($i=0; $i <$classes_countID; $i++) { 

			if (isset($_POST['yearOrSemester'.$classesID[$i]])) {
			   $semesterOryear =count($_POST['yearOrSemester'.$classesID[$i]]);
			}else{
				 $semesterOryear =0;
			}
		    if($semesterOryear==0){
		    	$this->form_validation->set_message("classesID","Please Select Atleast One Semester");
		  		$retrun =  false;
		  	}else{
	
		  		$retrun = true;
			}
		}
	
   }
   return $retrun;

	}

	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" ||  $usertype == 'ClgAdmin') {

			$id = $this->input->post('id');

			if ((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {



					$this->load->library('html2pdf');

				    $this->html2pdf->folder('uploads/report');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('notice/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));	

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function ratingmail(){

		$mail = $this->session->userdata("email");
	  $name = $this->session->userdata('name');

		 
		
		

$angry= $this->input->post('angry');

 
		$this->load->library('email');

		$this->email->from($mail, $name);
		$this->email->to('support@edgetechnosoft.com');


		$this->email->subject($angry);
		$this->email->message($angry);

		$this->email->send();

		echo 'Thanks For Rating';
	}

	// push Notification
	
	function sendnotification($deviceToken,$messages,$id,$titles)
	{
		define( 'API_ACCESS_KEY', 'AAAABWWT24g:APA91bFmgbOGaD5AZtsljmy4M2qsINg9nGlRGqGGhiyqCtSgkhhr4ybaZUSyDg6luIg6nWKfQ2ohpVCEXGXORIAn15QGf8UUjsVOCOMhd8vGIoFylZcQxAaVUPjNrL43cnnTXHrHSKoR' );


		$to = $deviceToken;

		// prep the bundle
		$msg = array
		(
			'message' 	=> $messages,
			'title'		=> $titles,
			'body' => $messages,
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		);

		$fields = array
		(
			'to' 	=> $to,
			'data'	=> $msg
		);
		 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

		return $result;
	}

	public function generateXls()
	{
		$adminID = $this->session->userdata("adminID");
		// print_r($adminID);die;
		// create file name
        $fileName = 'data-'.time().'.xlsx';  
		// load excel library
        $this->load->library('excel');
        $listInfo = $this->notice_m->make_datatables($adminID);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Subject');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Description');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Sender');
        // $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Contact_No');       
        // set Row
        // print_r($listInfo);die;
        $rowCount = 2;
        foreach ($listInfo as $list) {
        		if($list->usertype=="Admin")
                {
                	$name = $this->notice_m->notice_byname('user',$list->userID,'userID');
                	$nestedData=$name->name;
                }
                else
                {
                	$nestedData=$list->professorname;
                }
        	
        	
		        $noticedata = $list->notice;
                $datanotice="";
                preg_match_all('/<p><a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $noticedata,$noticeresult);
                if (!empty($noticeresult))
                {
                    // echo count($noticeresult); print_r($noticeresult[2][0]);die;
                    foreach ($noticeresult['href'] as $key => $valuefd)
                    {
                        $datanotice=$valuefd;
                    }
                    
                }
                // print_r($datanotice);die;
                $noticetext=strip_tags(str_replace(array("\n", "\r","&nbsp;","&quot;"), '', $list->notice));
                $notice=$noticetext." ".$datanotice;
	            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-m-Y H:i',strtotime($list->noticeDate)));
	            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->title);
	            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $notice);
	            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $nestedData);
	            // $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->contact_no);
	            $rowCount++;
        	
        	
            
        }
        $filename = "Notice". date("Y-m-d-H-i-s").".csv";
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0'); 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
		$objWriter->save('php://output'); 

    }

}



