<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("exam_m");
		$this->load->model("student_m");
		$this->load->model("sub_courses");
		$this->load->model("classes_m");
		$this->load->model("subject_m");
		$this->load->model("student_info_m");
		$language = $this->session->userdata('lang');
		// $this->data['otherdb']  = $this->load->database('otherdb', TRUE);
		$this->lang->load('exam', $language);	

	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$adminpackage = $this->session->userdata("adminpackage");
		$this->data['quiz_settings']   =	$this->exam_m->get_quizSettting();
		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
			
			
			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		    $this->data['all_count']  =  $this->exam_m->all_count();
			$this->data['ActiveExams_count']  =  $this->exam_m->ActiveExams_count();
			$this->data['DraftExams_count']  =  $this->exam_m->DraftExams_count();
			$this->data['TrashExams_count']  =  $this->exam_m->TrashExams_count();
		
			if ($this->input->post('classesID')) {
				$this->session->unset_userdata('FilterExamsubCourseID');
				$this->session->unset_userdata('FilterExamyearSemesterID');
				$this->session->unset_userdata('FilterExamsubjectID');
				$this->session->set_userdata('FilterExamclassesID', $this->input->post('classesID'));
			}
	        if ($this->input->post('subCourseID')) {
				$this->session->set_userdata('FilterExamsubCourseID', $this->input->post('subCourseID'));
			}
		    if ($this->input->post('yearSemesterID')) {
				$this->session->set_userdata('FilterExamyearSemesterID', $this->input->post('yearSemesterID'));
			}
			if ($this->input->post('subjectID')) {
				$this->session->set_userdata('FilterExamsubjectID', $this->input->post('subjectID'));
			}
			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterExamclassesID'));
			
			$this->data['subject'] = $this->subject_m->get_subject_by_session($adminID,$this->session->userdata('FilterExamclassesID'),$this->session->userdata('FilterExamyearSemesterID'));
			
			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterExamclassesID'));
            $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);
			$this->data["subview"] = "exam/index";
			$this->load->view('_layout_main', $this->data);
		}
		elseif ($usertype == "Student")
		{
			$this->data["subview"] = "exam/studentindex";
			$this->load->view('_layout_main', $this->data);
		}
		 else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	
	/*public function subject_search()
	{
		$yearSemesterID = $this->input->post('yearSemesterID');
		$classesID = $this->input->post('classesID');
		$subCourseID = $this->input->post('subCourseID');
		
		$where = "classesID = '".$classesID."' and sub_coursesID = '".$subCourseID."' and yearsOrSemester = '".$yearSemesterID."' and status = '1'";
		$sql = $this->db->select('subjectID,subject')->from('subject')->where($where)->get()->result_array();
		foreach($sql as $row)
		{
			echo $row['subject'];
		}
	}*/


	public function examStatus()
	{

	    $usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");


		if($usertype == "Admin" || $usertype == "ClgAdmin") {

	     	$otherdb    = $this->load->database('otherdb', TRUE);
	     	$this->data['totalResult'] =	$otherdb->get('ets_result')->num_rows();
	     								$otherdb->where('result_status','Open');	
	      	$this->data['OpenResult'] =	$otherdb->get('ets_result')->num_rows();

	           						    $otherdb->where('result_status','0');	
	      	$this->data['CompResult'] =	$otherdb->get('ets_result')->num_rows();

									      //$otherdb->where('result_status','0');	
									      $otherdb->group_by('uid');
	      	$this->data['totalStu'] =	$otherdb->get('ets_result')->num_rows();

	      									$otherdb->where('result_status','0');
	      									$otherdb->group_by('uid');
	      	$this->data['totalCompStu'] =	$otherdb->get('ets_result')->num_rows();

	      									$otherdb->where('result_status','Open');
	      									$otherdb->group_by('uid');
	      	$this->data['totalOpenStu'] =	$otherdb->get('ets_result')->num_rows();

			$this->data["subview"] = "exam/examStatus";
			$this->load->view('_layout_main',$this->data);

		}else{
			      $this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);
		}

	}
	

// DAU(Delete After Use)

	public function dau() {
		
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$this->data['quiz_settings']   =	$this->exam_m->get_quizSettting();
		if($usertype == "Admin" || $usertype == "ClgAdmin") {
	     $this->data['all_count']  =  $this->exam_m->all_count();
		$this->data['ActiveExams_count']  =  $this->exam_m->ActiveExams_count();
		$this->data['DraftExams_count']  =  $this->exam_m->DraftExams_count();
		$this->data['TrashExams_count']  =  $this->exam_m->TrashExams_count();
		$otherdb        = $this->load->database('otherdb', TRUE);
		$query = $otherdb->get('wp_wp_pro_quiz_master');
        $this->data['quiz']  = $query->result();

			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterExamsubCourseID');
					$this->session->unset_userdata('FilterExamyearSemesterID');
					$this->session->set_userdata('FilterExamclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterExamsubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterExamyearSemesterID', $this->input->post('yearSemesterID'));
				}
				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterExamclassesID'));
                $this->data['subject']  =   $this->subject_m->GetSubjectByParam(array('classesID'=>$this->session->userdata('FilterExamclassesID'),'sub_coursesID'=>$this->session->userdata('FilterExamsubCourseID'), 'yearsOrSemester'=>$this->session->userdata('FilterExamyearSemesterID')));
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterExamclassesID'));

                 $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);

			$this->data["subview"] = "exam/dau";

			$this->load->view('_layout_main', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}
// DAU

	public function studentcheckexam(){
		
		$usertype = $this->session->userdata('usertype');
		$adminID = $this->session->userdata('adminID');
		
			$this->db->where('exam_status',1);
 $result=			$this->db->get('student')->result();
		
$i = 1;
$sd= strtotime('1/26/2021');
foreach ($result as $key => $value) {
        $result2 =  $this->db->query("SELECT * from ets_result where uid = $value->studentID and start_time > $sd")->result();

        if($result2){
        	echo $i++ ."-".$value->name."<br>";
        }else{
        echo  "<p style='color:red'>".$value->name."(Not Apear)</p>";	
        }




}

	} 

	public function AjaxTable()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$totalData = $this->exam_m->get_join_exam_count($adminID);
		$totalFiltered = $totalData;
		$posts = $this->exam_m->make_datatables($adminID);
		// $totalFiltered  = $this->student_m->get_filtered_data();
		$data = array();
		if(!empty($posts))
		{
			$i = 1;
			$subjec_name = array();
			foreach ($posts as $post)
			{
				$nestedData['check'] = "<label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->quid." '/>
					<span class='checkmark checkmark-action-layout'></span>
				</label>
				</td>";
				$nestedData['sn2'] =''; 
				$nestedData['quiz_name'] = $post->quiz_name;
				$nestedData['subject_code'] = $post->subject_code;
				if($post->exam_for==2)
				{
					$this->db->where('unitID',$post->unitID);
					$sql =	$this->db->get('units')->result_array();
					foreach($sql as $units)
					{
						$subjec_name =$post->subject .'(Unit Name-'.$units['unit_name'].')';
					}
				}
				else
				{
					$subjec_name = $post->subject.'(Main Exam)';
				}
				$nestedData['subject'] = $subjec_name;
				$nestedData['classes'] = $post->classes;
				$nestedData['sub_course'] = $post->sub_course;
				$nestedData['yearsOrSemester'] = $post->yearsOrSemester;

				$start_date = '<input type="text" name="start_date" id="" value="'.date('Y-m-d H:i',$post->start_date).'" class="form-control datetimepicker_quiz_data" onclick = "subject_date()" onBlur= "inline_edit_quiz(this,\'start_date\','.$post->quid.', $(this).val())" placeholder="Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )" required disabled>';

				$end_date = '<input type="text" name="start_date" id="" value="'.date('Y-m-d H:i',$post->end_date).'" class="form-control datetimepicker_quiz_data" onclick = "subject_date()" onBlur= "inline_edit_quiz(this,\'end_date\','.$post->quid.', $(this).val())" placeholder="Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )" required disabled>';
				$nestedData['start_date'] = $start_date;
				$nestedData['end_date'] =$end_date;
				if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"  || $usertype == "Professor" || $usertype == "Parent" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") 
				{
					if ($post->studentStatus!=2) 
					{
						$buttons = "<a href = '".base_url('etsexam/index/'.$post->quid)."' class='btn btn-info' style='background:#85942f;'>Start Exam</a><a href = '".base_url('exam/questionadd/'.$post->quid)."' class='btn btn-primary primary_1 plus'><i class='fa fa-plus' title='Add Question'></i></a><a href='javascript:void(0)' onclick='lookUp(".$post->quid.")'><button type='button' data-toggle='modal' data-target='#exam_view' style='' class='btn btn-success  view_1 btn-xs'><i class='fa fa-eye' title='View Exam'></i></button></a>".btn_edit('exam/edit/'.$post->quid, $this->lang->line('edit'));
					}
					else
					{
						$buttons = "<a class = 'appClassDelete' href = ".base_url()."exam/restore/".$post->quid.">Restore</a>";
					}
					$nestedData['action'] = $buttons;
				}
				$data[] = $nestedData;
			}
		}
		$json_data = array(
			"draw"            => intval($this->input->post('draw')),  
			"recordsTotal"    => $totalData,  
			"recordsFiltered" => $totalFiltered, 
			"data"            => $data   
		);
		echo json_encode($json_data); 
	}

	public function exam_formP(){
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$username = $this->session->userdata('username');

			
		if($usertype == "Student") {
			$student = $this->student_m->get_single_student(array('username' => $username));			

			$this->data["student"] = $this->student_m->get_student(array('studentID'=>$student->studentID));
			$this->data["class"] = $this->student_m->get_class($student->classesID);	
			if($this->data["class"]) {
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
		$this->data["education"] = $this->student_m->get_educations($student->studentID,$ArrayForRequired);

			     $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);		
			
			$this->data["subview"] = "exam/exam_form";
			$this->load->view('_layout_main', $this->data);

		}
		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);
		}

	}


	public function exam_list() {

		$student = $this->student_info_m->get_student_info();
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
			if($student){
				$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);
			// print_r($this->data['classesRow']);
			// exit();
			$this->data['examType'] = $student->examType;
			}
			
			$this->data["subview"] = "exam/exam_list";
			$this->load->view('_layout_exam', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_exam', $this->data);

		}

	}


	public function AjaxTable_exam_list(){

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
        $uri=$this->uri->segment(3);
		$this->db->where('studentID',$loginuserID);
        $this->db->where('adminID',$adminID);
        $query	 = $this->db->get('student');
        $StudentArray =  $query->row();
		// check options subject 
		$this->db->select('subjectID');
		$this->db->where('yearsOrSemester',$StudentArray->yearsOrSemester);
		$this->db->where('studentID',$StudentArray->studentID);
		$options_eligible = $this->db->get('optionalsubject')->result();
		$posts = $this->exam_m->make_datatables_exam_list($adminID,$uri,$StudentArray,$array=null,$indivi=0);

		if ($options_eligible) {
		 $posts2 = $this->exam_m->make_datatables_exam_list($adminID,$uri,$StudentArray,$options_eligible,$indivi=0);
		 $posts = array_merge($posts,$posts2);
		}

		// $uri = htmlentities(($this->uri->segment(3)));
		$totalData = count($posts);
		$totalFiltered = $totalData ;
	
            // $totalFiltered  = $this->student_m->get_filtered_data();
		
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;

            foreach ($posts as $post)
            {

            	$ex_date = $post->start_date;
            	$new_date = $post->end_date;
            	$timestart = date('h:i:A',$ex_date);
            	$timeend = date('h:i:A',$new_date);
				$nestedData['sn'] = $i++;
				$nestedData['subject_code'] = $post->subject_code;
				$nestedData['quiz_name'] = $post->quiz_name;
				
				$nestedData['start_date'] = date('d-m-Y', $ex_date).' '.'('.$timestart.')';
                // $nestedData['yearsOrSemester'] = $timestart.' - '.$timeend ;
                 $nestedData['yearsOrSemester'] = date('d-m-Y', $new_date).' '.'('.$timeend.')';
  if($usertype == "Student") {
    $this->db->select('move_activity,result_status');
    $this->db->from('ets_result');
	$this->db->where('quid',$post->quid);
	$this->db->where('uid',$loginuserID);
    $exitsQuizrows	 = $this->db->get()->row();
// print_r($exitsQuizrows);
// exit();
if($exitsQuizrows){
if ($exitsQuizrows->result_status=='0') {
	$buttons     = "<p  class='btn btn-info disabled' >Completed</p>";
}elseif($exitsQuizrows->result_status=='Suspended'){
	$buttons     = "<p  class='btn btn-info disabled' style='background:red'>Suspended</p>";
}elseif($exitsQuizrows->result_status=='Open' || $exitsQuizrows->result_status=='Warning'){
$buttons     = "<a href = '".base_url('etsexam/index/'.$post->quid)."' class='btn btn-info' style='background:#85942f;'>Start Exam</a>";
}
}else{
	$buttons     = "<a href = '".base_url('etsexam/index/'.$post->quid)."' class='btn btn-info' style='background:#85942f;'>Start Exam</a>";
}

   $nestedData['action'] = $buttons;
}
               
                $data[] = $nestedData;

            }

            
        }
   
       
       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );
           
        echo json_encode($json_data); 
}



	protected function rules() {

		$rules = array(

			array(

				'field' => 'exam', 

				'label' => $this->lang->line("exam_name"), 

				'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_exam'

			), 

			array(

				'field' => 'date', 

				'label' => $this->lang->line("exam_date"),

				'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid'

			), 

			array(

				'field' => 'note', 

				'label' => $this->lang->line("exam_note"), 

				'rules' => 'trim|max_length[200]|xss_clean'

			)

		);

		return $rules;

	}



	public function add()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

				
					$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			   


        $this->data['quiz_settings']   =	$this->exam_m->get_quizSettting();
	if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner")
	{

	  if($_POST)
	  {
        $unitid=$this->input->post('unitID');
        $subjectid=$this->input->post('subjectID');
if($this->input->post('exam_for')==1){
$checksubjectid=$this->exam_m->checksubjectid($subjectid);
	if(!empty($checksubjectid))
	  {
	$this->session->set_flashdata('error',"Assignment already assign this Subject!!");
redirect(base_url("exam/add"));
}
}else{
	$checkunitid=$this->exam_m->checkunitid($unitid);
	if(count($checkunitid)>0)
	  {
	  $this->session->set_flashdata('error',"Assignment already assign this unit!!");
			redirect(base_url("exam/add"));
		}
}

        	$logged_in=$this->session->userdata('logged_in');
			$this->load->library('form_validation');
			$quid=$this->exam_m->insert_quiz();
	        $this->session->set_flashdata('success', $this->lang->line('menu_success'));

			redirect(base_url("exam/index"));
        
	   }
	   else
	   {

				$this->data["subview"] = "exam/add";

				$this->load->view('_layout_main', $this->data);

		}

	}
	else
	{

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

	}

	}



	public function edit() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		
		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			// $id = htmlentities(($this->uri->segment(3)));
			$id=$this->uri->segment(3);
// $this->data['exam']->sub_coursesID,
			if((int)$id) {
				$this->data['exam'] = $this->exam_m->getExamByID($id);

				if($this->data['exam']) {
				$this->data['get_sub_courses'] = $this->student_m->get_sub_courses($this->data['exam']->classesID);
				$this->data['subject'] = $this->exam_m->Getsubjects($adminID, $this->data['exam']->classesID,$this->data['exam']->yearsOrSemester);

				$this->data['classes_single'] = $this->classes_m->get_single_classes($this->data['exam']->classesID);

					if($_POST) {

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if (0) {

							$this->data["subview"] = "exam/edit";

							$this->load->view('_layout_main', $this->data);			

						} else {


							$this->exam_m->update_exam($id);
							
							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("exam/index"));

						}

					} else {

						$this->data["subview"] = "exam/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);	

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



public function copy_question($id){

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

        $this->data['exam'] = $this->exam_m->getExamByID($id);
       $this->db->join('units','units.unitID=ets_quiz.unitID','inner');
		$this->db->where('ets_quiz.subjectID',$this->data['exam']->subjectID);
		$this->db->where('ets_quiz.exam_for',2);
		$this->data['units'] = $this->db->get('ets_quiz')->result();

		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
        $this->data['quiz_settings']   =	$this->exam_m->get_quizSettting();


	$this->data["subview"] = "exam/copy_question";

	$this->load->view('_layout_main', $this->data);

}

public function  delete_temp(){
	$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
	if($usertype == "ClgAdmin") 
		{
	// $id =$this->input->post('qid');
$this->db->select('qids,quid');	
$examDetail  = $this->db->get('ets_quiz')->result();


$qids[] = '';
foreach ($examDetail as $key => $examDetails) {
$array_qid   =  explode(',',$examDetails->qids);

$this->db->where_in('qid',$array_qid);
$ets_qbank = $this->db->get('ets_qbank')->result();

foreach ($ets_qbank as $key => $value) {
 $qids[] = $value->qid;	
}
$array_dif  = array_intersect($qids, $array_qid);
$data = array(
'qids'=>implode(',',$array_dif),
'noq'=>count($array_dif)
);
$this->db->where('quid',$examDetails->quid);
$this->db->update('ets_quiz',$data);

}

			// $this->db->where('qid',$id);
		 //    $sql = $this->db->delete('ets_qbank');
			// $this->db->where('qid',$id);
			//  $this->db->delete('ets_options');
}
}

	public function delete($id) {
		$refering_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Academic" || $usertype == "Teacher") 
		{
$uri4 =$this->uri->segment(4); 		
$examDetails  = $this->exam_m->quiz_details($uri4,$adminID);

$array_qid   =  explode(',',$examDetails->qids);

$array_dif  = array_diff($array_qid, [$id]);

$data = array(
'qids'=>implode(',',$array_dif),
'noq'=>count($array_dif)
);
$this->db->where('quid',$uri4);
$this->db->update('ets_quiz',$data);
			$this->db->where('qid',$id);
		    $sql = $this->db->delete('ets_qbank');
			$this->db->where('qid',$id);
			 $this->db->delete('ets_options');




			if($sql==true)
			{
				redirect($refering_url);
			}
			else
			{
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
			/*$id = htmlentities(($this->uri->segment(3)));
			if((int)$id) 
			{
				$data = array(
					'status'=>2
				);
				$this->db->where('quid',$id);
				$this->db->update('ets_quiz',$data);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("exam/index"));
			} 
			else 
			{
				redirect(base_url("exam/index"));
			}*/
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}



	public function unique_exam() {

		// $id = htmlentities(($this->uri->segment(3)));
		$id=$this->uri->segment(3);

		if((int)$id) {

			$exam = $this->exam_m->get_order_by_exam(array("exam" => $this->input->post("exam"), "examID !=" => $id));

			if(count($exam)) {

				$this->form_validation->set_message("unique_exam", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$exam = $this->exam_m->get_order_by_exam(array("exam" => $this->input->post("exam")));



			if(count($exam)) {

				$this->form_validation->set_message("unique_exam", "%s already exists");

				return FALSE;

			}

			return TRUE;

		}	

	}


function manageQuestion(){
$usertype = $this->session->userdata("usertype");
$adminID = $this->session->userdata("adminID");

	if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
 $id =  $this->uri->segment(3);


if ($id){
            $examDetails  = $this->exam_m->quiz_details($id,$adminID);
 				$this->data["question"]  = $this->exam_m->getQuestion($examDetails->qids);
				$this->data["subview"] = "exam/indexQuestion";
				$this->load->view('_layout_main', $this->data);

}
else{

	
	    $this->data["subview"] = "error";
        $this->load->view('_layout_main', $this->data);
}

}else{
        $this->data["subview"] = "error";
        $this->load->view('_layout_main', $this->data);
}

}


public function AjaxTableQuestion(){

$usertype = $this->session->userdata("usertype");
$adminID = $this->session->userdata("adminID");
$loginuserID = $this->session->userdata("loginuserID");
$id =  $this->uri->segment(3);

$examDetails  = $this->exam_m->quiz_details($id,$adminID);
// print_r($examDetails);
// exit();
// if($examDetails){
$totalData = $this->exam_m->get_Question_count($examDetails->qids);

$totalFiltered = $totalData;
$posts = $this->exam_m->getQuestion($examDetails->qids);
// print_r($posts);
// exit();
            // $totalFiltered  = $this->student_m->get_filtered_data();
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['question'] = $post->question;

				if ($post->question_type=="Multiple Choice Single Answer") {
				    $q_type = 1;
				}elseif($post->question_type=="Multiple Choice Multiple Answer"){
                    $q_type = 2;
				}elseif($post->question_type=="Match the Column"){
					$q_type = 3;
				}elseif($post->question_type=="Short Answer"){
					$q_type = 4;
				}else{
					$q_type = 5;
				}
				$nestedData['correct'] = '';
  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"  || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {

   $buttons     = btn_edit('exam/questionEdit/'.$q_type.'/'.$post->qid.'/'.$this->uri->segment(3), $this->lang->line('edit')).btn_delete('exam/delete/'.$post->qid.'/'.$id, $this->lang->line('delete'));

   $nestedData['action'] = $buttons;
}
               
                $data[] = $nestedData;

            }
        }
// print_r($data);
// exit();
       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
}


	public function questionadd() 
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		
		$uri = $this->uri->segment(3);
		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") 
		{
			if($_POST) 
			{
				//print_r($_POST);
				$quid = $this->input->post('quid');
		
				$this->exam_m->question_add($quid);
		        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("exam/questionadd/".$uri.'/'.$this->input->post('question_type')));                      
			} 
			else 
			{
				$this->data["subview"] = "exam/questionadd";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

public function questionEdit() {
	    $qid = $this->uri->segment(4);
	    $uri5 = $this->uri->segment(5);
	    $uri = $this->uri->segment(3);
	    // echo($uri).'<br>';
	    // echo($qid).'<br>';
	    // echo($uri5);
	    // echo($uri);
	    
	    // exit();

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		
		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		$this->data['question']=$this->exam_m->get_question($qid);
		$this->data['options']=$this->exam_m->get_option($qid);
		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {

	   if($_POST) {

		$quid  =  $this->input->post('quid');
		// print_r($quid);
		// print_r($qid);
		// exit();
		$this->exam_m->update_question($quid);
		// $uri = $this->uri->segment(3);
        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("exam/questionadd/".$quid));
                      
			} else {

				$this->data["subview"] = "exam/questionEdit";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function dau_quiz()
	{
		$data = array(
			// "duration"=>$this->input->post('duration'),
		 //   "start_date" =>strtotime($this->input->post('start_date')) ,
		 //   "end_date" =>strtotime($this->input->post('end_date')),
		   "video_record" =>$this->input->post('video'),
		);

		$query = $this->db->update('ets_quiz',$data);
        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("exam/quizSetting/"));
	}

	public function quizSetting() {
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		
	    $this->data['quiz_settings']   =	$this->exam_m->get_quizSettting();

		if($usertype == "Admin" || $usertype == "ClgAdmin"  || $usertype == "Academic" || $usertype == "Professor" || $usertype == "Support" || $usertype == "Academic" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
			if($_POST) {
//universal	
		
			$column = $this->input->post('column');
			$id = (int) $this->input->post('id');
			$val =  $this->input->post('editValue');
			$this->data = array(
			   "$column" =>$val, 
			);
 //universal	

// $this->db->where('id',$id);
			$query = $this->db->update('ets_quiz_setting',$this->data);
 


                      

			} else{

				$this->data["subview"] = "exam/quizSetting";

				$this->load->view('_layout_main', $this->data);


			}

		}

	}

	function inline_edit_quiz(){
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == "Academic") {
			  $column = $this->input->post('column');
			  $quiz_id = (int) $this->input->post('quiz_id');
			  $val =  $this->input->post('editValue');
			  $this->data = array("$column" =>strtotime($val), 
			);
			$this->db->where('quid',$quiz_id);
			$query = $this->db->update('ets_quiz',$this->data);
		}
	}
// $this->input->post('sub_coursesID'),
function Getsubjects(){
$adminID = $this->session->userdata("adminID");
$subject = $this->exam_m->Getsubjects($adminID, $this->input->post('classesID'),$this->input->post('semester_idSelect'));
	echo "<div class='form-group'>     
               <label for=''>Select Subject</label> 
               <select class ='form-control' onchange= 'AddQuizName($(this).val())' id = 'subject_id' name = 'subjectID'>
               <option value = ''>Select Subject</option>";
               foreach ($subject as $key => $value) { 
                echo "<option value ='".$value->subjectID."'>".$value->subject."</option>";
                }
               echo "</select>";
          echo "</div>";

}



function find_question(){

if($this->input->post('extype')==1){

$unitID =  $this->input->post('unitID');
$this->db->select('qids');
$this->db->where('exam_for',2);
$this->db->where_in('unitID',$unitID);
$qids  = $this->db->get('ets_quiz')->result();
$qids_array =  '';
foreach ($qids as $key => $value) {
   $qids_array .= ','.$value->qids;
}
 $qids_with_array = explode(',',$qids_array);
 $this->db->where_in('qid',$qids_with_array);
 $this->data['qbank'] = $this->db->get('ets_qbank')->result();
}else{
	$subjectID =  $this->input->post('subjectID');
	$this->db->select('qids');
$this->db->where('exam_for',1);	
$this->db->where('subjectID',$subjectID);
$qids  = $this->db->get('ets_quiz')->row();
if($qids){
 $qids_with_array = explode(',',$qids->qids);
}else{
$qids_with_array  = array('');	
}
 $this->db->where_in('qid',$qids_with_array);
 $this->data['qbank'] = $this->db->get('ets_qbank')->result();
}


 	$this->data["subview"] = "exam/copy_question";
	$this->load->view('_layout_main', $this->data);


}

function copy_qids(){
$quid =	$this->input->post('quid');
$checkedID   = $this->input->post('checkedID');
$qid   = implode(',',$checkedID);

$this->db->where('quid',$quid);
$quiz = $this->db->get('ets_quiz')->row();

if($quiz->qids==''){
	$noq = 0;
	$qids =  $qid;
	$noq += count($checkedID);
}else{
	$noq = $quiz->noq;
	$qids = $quiz->qids;
	$qids .= ','.$qid;
	$noq += count($checkedID);
}

$data = array(
	'qids' =>$qids,
	'noq'=>$noq 
);

$this->db->where('quid',$quid);
$this->db->update('ets_quiz',$data);

$this->session->set_flashdata('success', $this->lang->line('menu_success'));
redirect(base_url("exam/questionadd/".$quid));

}

function GetUnits(){
	$adminID = $this->session->userdata("adminID");
 $this->db->where('subjectID',$this->input->post('subjectID'));
$units =			$this->db->get('units')->result();
	echo "<div class='form-group'>     
               <label for=''>Select Unit</label> 
               <select class ='form-control'  id = 'unit_id' name = 'unitID'>
               <option value = ''>Select Unit</option>";
               foreach ($units as $key => $value) { 
                echo "<option value ='".$value->unitID."'>".$value->unit_name."</option>";
                }
               echo "</select>";
          echo "</div>";
}

function copyType(){
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
$val =  $this->input->post('val');
$id  =  $this->input->post('id');
        $this->data['exam'] = $this->exam_m->getExamByID($id);
       $this->db->join('units','units.unitID=ets_quiz.unitID','inner');
		$this->db->where('ets_quiz.subjectID',$this->data['exam']->subjectID);
		$this->db->where('ets_quiz.exam_for',2);
		$units = $this->db->get('ets_quiz')->result();

		$classes = $this->student_m->get_classes_by_superAdmin($adminID);
        $this->data['quiz_settings']   =	$this->exam_m->get_quizSettting();


if($val==2){
echo '<div class="form-group">
                      <label for="classesID" class="">Course<span class="red-color">*</span></label>
                      <div class="">
                        <select name="classesID" required onchange="ajaxGet_subCourses_exam($(this).val())" id="classesID" class="form-control" >
                        <option value="">Select Course</option>';
                         foreach($classes as $courses){ 
                         echo '<option value="'.$courses->classesID.'">'.$courses->classes.'</option>';
                          }
                    echo'</select>                       
                    </div>
                    </div>
                    <div id="subCourseID">
                    </div>    
                    <div id="yearAndSemester">
                    </div>
                  <div id="subjectID">
                  </div>';
}else{
echo "<ul class='ul_li_class'>";
foreach ($units as $key => $value) {
  echo "<li>";	
  echo '<label><input type="checkbox" name="unitID[]" value = "'.$value->unitID.'" checked>'.$value->unit_name.'</label>';
  echo "</li>";
 }
   echo "</ul>";

}

}

function date_valid($date) {

	  	if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);   

	        $dd = $arr[0];            

	        $mm = $arr[1];              

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    } 



	} 

	public function restore() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == "Academic") {

			// $id = htmlentities(($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {
$data = array(
'status'=>1
);
				$this->db->where('quid',$id);
				$this->db->update('ets_quiz',$data);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("exam/index"));

			} else {

				redirect(base_url("exam/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function multipleAction()
	{
		if ($this->input->post('Delete')) 
		{
			$checked_id =  $this->input->post('checked_id');
			for ($i=0; $i <count($checked_id) ; $i++) 
			{ 
				$data = array(
					'status'=>2
				);
				$this->db->where('quid',$checked_id[$i]);
				$this->db->update('ets_quiz',$data);
			}
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("exam/index"));
		}

		if ($this->input->post('Draft')) 
		{
			$checked_id =  $this->input->post('checked_id');
			for ($i=0; $i <count($checked_id) ; $i++) { 
				$data = array(
					'status'=>0
				);
				$this->db->where('quid',$checked_id[$i]);
				$this->db->update('ets_quiz',$data);
			}
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("exam/index"));
		}
		if ($this->input->post('Active')) 
		{
			$checked_id =  $this->input->post('checked_id');
			for ($i=0; $i <count($checked_id) ; $i++) 
			{ 
				$data = array(
					'status'=>1
				);
				$this->db->where('quid',$checked_id[$i]);
				$this->db->update('ets_quiz',$data);
			}
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("exam/index"));
		}
	}

function ResetCourses(){

$this->session->unset_userdata('FilterExamclassesID');
$this->session->unset_userdata('FilterExamsubCourseID');
$this->session->unset_userdata('FilterExamyearSemesterID');
$this->session->unset_userdata('FilterExamsubjectID');	   
}

function ResetSubcourses(){

$this->session->unset_userdata('FilterExamsubCourseID');
	   
}

function ResetSemesterYear(){

$this->session->unset_userdata('FilterExamyearSemesterID');
	   
}

function ResetSubject(){

$this->session->unset_userdata('FilterExamsubjectID');
	   
}


function ResetAllfilter(){
$this->session->unset_userdata('FilterExamsubCourseID');
$this->session->unset_userdata('FilterExamclassesID');
$this->session->unset_userdata('FilterExamyearSemesterID');  
$this->session->unset_userdata('TrashExams');
$this->session->unset_userdata('DraftExams');
$this->session->unset_userdata('AllExams');
$this->session->unset_userdata('ActiveExams');
$this->session->unset_userdata('FilterExamsubjectID');
}

function ActiveExams(){
$this->session->unset_userdata('FilterExamsubCourseID');
$this->session->unset_userdata('FilterExamclassesID');
$this->session->unset_userdata('FilterExamyearSemesterID');
$this->session->unset_userdata('TrashExams');
$this->session->unset_userdata('DraftExams');
$this->session->unset_userdata('AllExams');
$this->session->unset_userdata('ActiveExams');
// $this->session->set_userdata('ActiveExams',1);
}
function DraftExams(){
$this->session->unset_userdata('FilterExamsubCourseID');
$this->session->unset_userdata('FilterExamclassesID');
$this->session->unset_userdata('FilterExamyearSemesterID');
$this->session->unset_userdata('ActiveExams');
$this->session->unset_userdata('TrashExams');
$this->session->unset_userdata('AllExams');
$this->session->set_userdata('DraftExams',3);
}
function TrashExams(){
$this->session->unset_userdata('FilterExamsubCourseID');
$this->session->unset_userdata('FilterExamclassesID');
$this->session->unset_userdata('FilterExamyearSemesterID');
$this->session->unset_userdata('ActiveExams');
$this->session->unset_userdata('DraftExams');
$this->session->unset_userdata('AllExams');
$this->session->set_userdata('TrashExams',2);
}
function AllExams(){
$this->session->unset_userdata('FilterExamsubCourseID');
$this->session->unset_userdata('FilterExamclassesID');
$this->session->unset_userdata('FilterExamyearSemesterID');
$this->session->unset_userdata('ActiveExams');
$this->session->unset_userdata('DraftExams');
$this->session->unset_userdata('TrashExams');
$this->session->set_userdata('AllExams',4);
}

function ResetSemesterYear_exam_list(){

$this->session->unset_userdata('FilterExamListyearSemesterID');
	   
}



function dau_exam(){

	 $this->data['otherdb']->where('id',$_POST['Submit']);
     $query = $this->data['otherdb']->get('wp_wp_pro_quiz_master');

     $quiz  = $query->row();
   
     $quid     = $this->exam_m->insert_quiz();

     		  $this->data['otherdb']->where('quiz_id',$quiz->id);
     $query = $this->data['otherdb']->get('wp_wp_pro_quiz_question');
     $i = 1;
     foreach ($query->result() as $key => $value) {	
  
     		              $userdata = array(
                                             'question' =>$value->question,
                                            'question_type' => 'Multiple Choice Single Answer'
                                        );
                                        $this->db->insert('ets_qbank', $userdata);
                                        $qid = $this->db->insert_id();
			                            $this->session->set_userdata('qid', $qid);


                   $this->data['otherdb']->where('q_id',$value->id);
	    	      $this->data['otherdb']->where('quiz_id',$value->quiz_id);
     $getsingle = $this->data['otherdb']->get('tableBysunil');	
     $rowSingleOption = $getsingle->result();

     foreach ($rowSingleOption as $key => $row) {

			                            $this->db->where('qid',$this->session->userdata('qid'));
                                        $this->db->where('score',1);
                                        $chuh    =  $this->db->get('ets_options');

                                        $this->db->where('qid',$this->session->userdata('qid'));
                                        $Three   =  $this->db->get('ets_options');


                                    if ($chuh->num_rows()==1) {
                                       $score = 0;
                                    }else{
                                    for ($i=0; $i < 4 ; $i++) { 
                                         $randi = rand(1,4);
                                         if ($i==3) {
                                             if ($randi==1) {
                                               $score = 1; 
                                             }else{
                                                $score = 0; 
                                             }
                                         }
                                        }
                                        if ($Three->num_rows()==3) {
                                           if ($chuh->num_rows()==0) {
                                              $score = 1;
                                           }
                                        }
                                        

                                    }
                                       $userdata = array(
                                            'q_option' => $row->answer,
                                            'qid' => $qid,
                                            'score' => $score
                                        );

                        
                                        $this->db->insert('ets_options', $userdata);

}
                                        $this->db->where('quid', $quid);
                                        $query    = $this->db->get('ets_quiz');
                                        $quizData = $query->row();
                                        
                                        $qids = $quizData->qids;
                                        $noq  = $quizData->noq;
                                        if ($qids == '') {
                                            $updatedQid = $qid;
                                        } else {
                                            $updatedQid = $qids . ',' . $qid;
                                        }
                                        if ($noq == '') {
                                            $noqUpdate = 1;
                                        } else {
                                            $noqUpdate += 1;
                                        }
                                        $arrayName = array(
                                            'qids' => $updatedQid,
                                            'noq' => $noqUpdate
                                        );
                                        $this->db->where('quid', $quid);
                                        $this->db->update('ets_quiz', $arrayName);

    
}

   $this->session->set_flashdata('success', $this->lang->line('menu_success'));

		redirect(base_url("exam/dau"));


}


}