<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Student extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model("student_info_m");
		$this->load->model("subject_m");
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("semester_test_m");
		$this->load->model("invoice_gen_m");
		$this->load->model("sub_courses");
		$this->load->model("country_m");
		$this->load->model("year_m");
		$this->load->model("teacher_m");
		$this->load->model("classes_m");
		$this->load->model("professor_m");
		$this->load->model("exam_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('student', $language);
	}

	function dau2(){
		$posts = $this->db->query("SELECT * from student")->row();
//	print_r(count($posts));
	//exit();
		$sss =0;
		$blanck = 0;
		foreach ($posts as $key => $value) {
			if($value->phone!=''){
				$phones = explode(',', $value->phone);
if(count($phones)>1){
	echo $value->name." - ".$value->phone;
	echo "<br>";
$sss +=1;
}

		//  	$data = array(
		// 		'phone'=>$phones[0],
		// 		'extraPhone'=>$phones[1]
		// 	);
		// 	}
		// $this->db->where('studentID',$value->studentID);
		// $this->db->update('student',$data);
		}
		else{
echo $value->name." - ".$value->phone;		
$blanck +=1;	
}
	}
	echo $blanck;
	echo "<br>";
	echo $sss;
	}
	function dau3(){
		$posts = $this->db->query("SELECT * from student")->result();
		$sss =0;
		$blanck = 0;
		foreach ($posts as $key => $value) {
     if($value->enrollment!=$value->roll){
     	echo  $value->name.'-'.$value->roll.'-erll-'.$value->enrollment;
     	echo "<br>";

     }

     }
	
	}

	public function index() {
		
		$loginuserID = $this->session->userdata("loginuserID");
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		// print($this->session->userdata('ActiveStudent'));
		// exit();

		$this->data['all_count']  =  $this->student_m->all_count();
		$this->data['ActiveStudent_count']  =  $this->student_m->ActiveStudent_count();
		$this->data['DraftStudent_count']  =  $this->student_m->DraftStudent_count();
		$this->data['TrashStudent_count']  =  $this->student_m->TrashStudent_count();
		$this->data['passOut_count']  =  $this->student_m->passOut_count();

		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
			$id = $this->uri->segment(3);
			if((int)$id)
			{
				$this->data['set'] = $id;
				$this->data['subCourses'] = $this->sub_courses->get_sub_courses();
				$this->data['classes'] = $this->student_m->get_classes();
				$this->data['students'] = $this->student_m->get_order_by_student_with_courses_by_join($id, $loginuserID);

				$this->data['counsellor'] = $this->student_m->get_counsellor();
				if($this->data['students']) {
			    $this->data['students'] = $this->student_m->get_order_by_student(array('classesID' => $id,'adminID'=>$loginuserID));
				
				} 
				else 
				{
					$this->data['students'] = NULL;
				}

				$this->data["subview"] = "student/index";
				$this->load->view('_layout_main', $this->data);
			} 
			else 
			{
				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('subCourseID');
					$this->session->set_userdata('classesID', $this->input->post('classesID'));
				}
			   	if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('subCourseID', $this->input->post('subCourseID'));
				}
				if ($this->input->post('SessionTo') && $this->input->post('SessionFrom')) {
				    $SessionTo = $this->input->post('SessionTo');
				    $SessionFrom = $this->input->post('SessionFrom');
				    $sessionFilter = $SessionFrom.'-'.$SessionTo;
				    $this->session->set_userdata('sessionFilter', $sessionFilter);
				}
				if ($this->input->post('sessionType')) {
					$this->session->set_userdata('sessionType', $this->input->post('sessionType'));
				}
				if ($this->input->post('examType')) {
					$this->session->set_userdata('examType', $this->input->post('examType'));
				}	
				if ($this->input->post('education_mode')) {
				   $this->session->set_userdata('education_mode', $this->input->post('education_mode'));
				}
				if ($this->input->post('teacherID')) {
					$this->session->set_userdata('teacherID', $this->input->post('teacherID'));
				}
				if ($this->input->post('payment_status')) {
					$this->session->set_userdata('payment_status', $this->input->post('payment_status'));
				}
					if ($this->input->post('student_position')) {
					$this->session->set_userdata('student_position', $this->input->post('student_position'));
				}
					if ($this->input->post('yos_filter')) {
					$this->session->set_userdata('yos_filter', $this->input->post('yos_filter'));
				}

				if ($usertype == "superadmin") 
				{
					$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session_by_superAdmin();
					$this->data['classes'] = $this->student_m->get_classes();
					// $this->data['students'] = $this->student_m->get_student_by_join_superAdmin();
					$this->data['counsellor'] = $this->student_m->get_counsellor();
				}
				else
				{
					
					$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('classesID'));
					$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('classesID'));
					$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
					$this->data['students'] = $this->student_m->get_order_by_student_by_join_Count($adminID);
					$this->data['counsellor'] = $this->student_m->get_counsellor_byAdmin($adminID);
				}
				$this->data["subview"] = "student/search";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

    function ajaxStudents()
    {
        $usertype = $this->session->userdata("usertype");
        $adminID = $this->session->userdata("adminID");
        $loginuserID = $this->session->userdata("loginuserID");
        $totalData = $this->student_m->get_order_by_student_by_join_Count($adminID);
        $totalFiltered = $totalData;
        $posts = $this->student_m->make_datatables($adminID);
        $data = array();
        if(!empty($posts))
        {
            $i = 1;
            foreach ($posts as $post)
            {
            	// print_r($post);die;
                $array   =  "<div class='profile_pic stable_img'>
                <a href = ".base_url('profile/ChangePicture/'.$post->studentID).">
                <img src='".base_url('uploads/images/'.$post->photo)."' alt='user'>
                </a>
                </div>";		
                // $array = array(
                //                 "src" => base_url('uploads/images/'.$post->photo),
                //                 'width' => '35px',
                //                 'height' => '35px',
                //                 'class' => 'img-rounded'
                // 				);
                $examTypeArray =  array("o"=>"One Time","y"=>"Yearly");
                $education_modeArray =  array(1=>"Year",2=>"Semester");
                if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant") 
                {	
                    if($post->notification == '1' && $post->notification_date == date('Y-m-d') && ($usertype == 'ClgAdmin' || $usertype == "superadmin"))
                    {  
                        $nestedData['check'] = "<label class='nexCheckbox'>
                        <input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->studentID." '/>
                        <span class='checkmark checkmark-action-layout'></span><span class='btn btn-danger new' style='margin-top: -5px;display:block;'>New</span>
                        </label>";
                    }
                    else
                    {
                        $nestedData['check'] = "<label class='nexCheckbox'>
                        <input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->studentID." '/>
                        <span class='checkmark checkmark-action-layout'>
                        </label>";
                    }
                }
                else
                {
                    $nestedData['check'] = ' ';
                }
                $nestedData['sn2'] =  $i;
                $status = $post->invoiceStatus;
                if($status == 0) 
                {
                    $status = 'Not Paid';
                } 
                elseif($status == 1) 
                {
                    $status = 'Partial Paid';
                } 
                elseif($status == 2) 
                {
                    $status = 'Full Paid';
                }
                // $status_button =   "<a class='btn btn-success btn-xs'>".$status."</a>";
                $status_button =   "";
                if ($post->yosPosition=='Passed Out') 
                {
                    $yos  =   'Passed Out';
                }
                else
                {
                    $yos = str_replace('_', ' ',  $post->studentyearsOrSemester);
                }
                $nestedData['img'] = $array;
                $nestedData['username'] = $post->username;
                $nestedData['enrollment'] = $post->enrollment;
                $nestedData['name'] = $post->name;
                $nestedData['father_name'] = $post->father_name;
                $nestedData['yearsOrSemester'] =  $yos;   
                $nestedData['classes'] = $post->classes;                  
                $nestedData['sub_course'] = $post->sub_course;
                $nestedData['session'] = $post->session;
                $nestedData['sessionType'] = $post->sessionType;
                // $nestedData['education_mode'] = $education_modeArray[$post->education_mode];
                $nestedData['student_status'] = ucfirst($post->student_status);
                $nestedData['phone'] = $post->phone;
                $nestedData['dob'] = date('d-m-Y', strtotime($post->dob));
                $nestedData['sex'] = $post->sex;
                $nestedData['email'] = $post->email;
                $nestedData['mother_name'] = $post->mother_name;
                $nestedData['aadhar'] = $post->aadhar;
                $nestedData['address'] = $post->address;
                $nestedData['nationality'] = $post->nationality;
                $nestedData['create_date'] = $post->create_date;
                $nestedData['amount'] = $post->amount;
                $nestedData['paidamount'] = $post->paidamount;
                $nestedData['invoiceStatus'] = " ";
                if($usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant") 
                {
                    if ($post->studentStatus!=2) {
                        $buttons = btn_view('Student/view/'.$post->studentID, $this->lang->line('view'))."<a data-toggle='tooltip'  data-placement='top' title='Fee' href='".base_url()."invoice/view/$post->studentID?type=clg&year=".return_year($post->yearsOrSemester)."' class='btn btn-info edit for_margR'><i class='fa fa-inr'></i></a><input type='hidden' id='passwords_".$post->phone."' value='".str_replace("-","",date('d-m-Y', strtotime($post->dob)))."'><a href='javascript:void(0)' class='btn btn-primary primary_1 send_sms' id='".$post->phone."' data-id='".$post->username."' title='Send SMS'><i class='fa fa-envelope'></i></a><input type='hidden' id='student_name_".$post->phone."' value='".$post->name."'>".btn_delete('Student/delete/'.$post->studentID, $this->lang->line('delete')).'<a class="btn btn-default default_1" href="'.base_url().'Student/passwordReset/'.$post->studentID.'" title="Reset Password"><i class="fa fa-undo" aria-hidden="true"></i></a>';
                    } 
                    else
                    {
                        $buttons = "<a class = 'appClassDelete' href = ".base_url()."Student/restore/".$post->studentID.">Restore</a>";
                    }
                    $nestedData['action'] = $buttons;
                }
                else
                {
                
                        $buttons  =  btn_view('Student/view/'.$post->studentID, $this->lang->line('view'));		
                    
                    $nestedData['action'] = $buttons;  	
                }
                if ($usertype == "Admin") {
                	if ($post->studentStatus!=2) {
                        $buttons = btn_view('Student/view/'.$post->studentID, $this->lang->line('view'))."<input type='hidden' id='passwords_".$post->phone."' value='".str_replace("-","",date('d-m-Y', strtotime($post->dob)))."'><a href='javascript:void(0)' class='btn btn-primary primary_1 send_sms' id='".$post->phone."' data-id='".$post->username."' title='Send SMS'><i class='fa fa-envelope'></i></a> <input type='hidden' id='student_name_".$post->phone."' value='".$post->name."'>".btn_delete('Student/delete/'.$post->studentID, $this->lang->line('delete')).'<a class="btn btn-default default_1" href="'.base_url().'Student/passwordReset/'.$post->studentID.'" title="Reset Password"><i class="fa fa-undo" aria-hidden="true"></i></a>';
                    }
                    else
                    {
                        $buttons = "<a class = 'appClassDelete' href = ".base_url()."Student/restore/".$post->studentID.">Restore</a>";
                    }
                    $nestedData['action'] = $buttons;
                }
                $data[] = $nestedData;
                $i++;
            }
        }
        // print_r($data);
        // exit();
        $json_data = array(
        "draw"            => intval($this->input->post('draw')),  
        "recordsTotal"    => $totalData,  
        "recordsFiltered" => $totalFiltered, 
        "data"            => $data   
        );
        echo json_encode($json_data); 
    }

	protected function rules() {
		$rules = array(


			

		// array(
		// 		'field' => 'amount',
		// 		'label' =>'Total Fee',
		// 		'rules' => 'trim|required|max_length[20]|xss_clean'
		// 	),
			// array(
			// 	'field' => 'paidamount',
			// 	'label' =>'Admission Fee',
			// 	'rules' => 'trim|required|max_length[20]|xss_clean|callback_exceed_paidamount'
			// ),

			array(
				'field' => 'name',
				'label' => $this->lang->line("student_name"),
				'rules' => 'trim|required|max_length[60]'
			),
			array(
				'field' => 'father_name',
				'label' => $this->lang->line("father_name"),
				'rules' => 'trim|required|max_length[60]'
			),
			array(
				'field' => 'mother_name',
				'label' => $this->lang->line("mother_name"),
				'rules' => 'trim|required|max_length[60]'
			),

			array(
				'field' => 'dob',
				'label' => $this->lang->line("student_dob"),
				'rules' => 'trim|required|max_length[10]|callback_date_valid'
			),
			array(
				'field' => 'sex',
				'label' => $this->lang->line("student_sex"),
				'rules' => 'trim|required|max_length[10]'
			),
			array(
				'field' => 'nationality',
				'label' => $this->lang->line("nationality"),
				'rules' => 'trim|max_length[25]'
			),
			array(
				'field' => 'street',
				'label' => $this->lang->line("street"),
				'rules' => 'trim|max_length[25]'
			),
			array(
				'field' => 'pin',
				'label' => $this->lang->line("pin"),
				'rules' => 'trim|min_length[4]|max_length[6]|numeric'
			),
			array(
				'field' => 'email',
				'label' => $this->lang->line("student_email"),
				'rules' => 'trim|required|max_length[40]|valid_email|callback_unique_email'
			),
			array(
				'field' => 'phone',
				'label' => $this->lang->line("student_phone"),
				'rules' => 'trim|max_length[25]|min_length[5]|required'
			),
			array(
				'field' => 'address',
				'label' => $this->lang->line("student_address"),
				'rules' => 'trim|max_length[200]'
			),
			array(
				'field' => 'subCourseID',
				'label' => 'Sub Courses',
				'rules' => 'numeric|max_length[11]'
			),

			 array(
				'field' => 'classesID',
				'label' => $this->lang->line("student_classes"),
				'rules' => 'trim|required|numeric|max_length[11]'
			),

			array(
				'field' => 'roll',
				'label' => $this->lang->line("student_roll"),
				'rules' => 'trim|max_length[40]'
			),


			array(
				'field' => 'photo',
				'label' => $this->lang->line("student_photo"),
				'rules' => 'trim|max_length[200]'
			),

			array(
				'field' => 'sessionFrom',
				'label' => 'Session',
				'rules' => 'trim|required|max_length[200]'
			),

		    array(
				'field' => 'sessionTo',
				'label' => 'Session',
				'rules' => 'trim|required|max_length[200]'
			),

			

			array(
				'field' => 'education_mode',
				'label' =>'Session Type',
				'rules' => 'trim|required|max_length[20]'
			),

			array(
				'field' => 'yearsOrSemester',
				'label' =>'Year or Semester',
				'rules' => 'trim|max_length[20]|required'
			),	

			// array(
			// 	'field' => 'discount',
			// 	'label' =>'Total Discount',
			// 	'rules' => 'trim|xss_clean|required'
			// ),		

		);
		return $rules;
	}

	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$email_Admin = $this->session->userdata("email");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Teacher" || $usertype == "Accountant" ) 
		{
		    $arr = array();
		    $this->db->select('email');
		    $this->db->from('user');
		    $where = "adminID = '".$adminID."' and create_usertype = 'ClgAdmin'";
		    $this->db->where($where);
		    $sqls = $this->db->get();
		    $results = $sqls->result_array();
		    foreach($results as $rows)
		    {
		        $arr[] = array(
		                        $rows['email']
		                        );
		    }
		    
		    $arrs = str_replace(array('[', ']'), '',htmlspecialchars(json_encode($arr), ENT_NOQUOTES));
		    $admin_user =  str_replace('"','',$arrs);

			$this->data['classes'] = $this->student_m->get_classes($adminID);
 			$this->data['country'] = $this->country_m->get_country();
 		    $classesID = $this->input->post("classesID");
			$this->data['counsellor'] = $this->teacher_m->get_teacher_all($adminID);
			if($_POST) 
			{
			 	$getClass = $this->classes_m->get_single_classes($classesID);
			 	$regitrationID =  intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) 
				{
					//  echo validation_errors();
					// exit();
					$this->data["subview"] = "student/add";
					$this->load->view('_layout_main', $this->data);
				} 
				else 
				{
				    date_default_timezone_set('Asia/Kolkata');
					$year = date("Y");
					$std_session = $this->input->post('sessionFrom').'-'.$this->input->post('sessionTo');
					$array = array();
					$array["name"] = $this->input->post("name");
					$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
					$dob             = $this->input->post('dob');
				    $slashToHypne     = str_replace('/', '-', $dob);
                    $dobIndian        = date("d-m-Y", strtotime($dob));
                    $passwordPlusName = str_replace('-', '', $dobIndian);
                    $this->session->set_userdata('student_password',$passwordPlusName);
					$array["sex"] = $this->input->post("sex");
					$array["father_name"] = $this->input->post("father_name");
					$array["mother_name"] = $this->input->post("mother_name");
					$array["email"] = $this->input->post("email");
					$array["phone"] = $this->input->post("phone");
					$array["adminID"] = $adminID;
					$array["address"] = $this->input->post("address");
					$array["aadhar"] = $this->input->post("aadhar");
					$array["street"] = $this->input->post("street");
					$array["pin"] = $this->input->post("pin");
					$array["classesID"] = $this->input->post("classesID");
					$sub_courses=$this->input->post("sub_coursesID");
					if($sub_courses == "")
					{
						$sub_courses=0;
					}
					$array["sub_coursesID"] = $sub_courses;
					$array["nationality"] = $this->input->post("nationality");					
					$array["session"] = $std_session;
					// $array["sessionType"] = $this->input->post('sessionType');
					$array["education_mode"] = $this->input->post("education_mode");
					$array["student_status"] = 'fresher';//$this->input->post("student_status");
				    $array["yearsOrSemester"] = $this->input->post("yearsOrSemester");
					$array["roll"] = $this->input->post("enrollment");
					$array["enrollment"] = $this->input->post("enrollment");
					$array["username"] = $regitrationID;
					$array['password'] = $this->student_m->hash($passwordPlusName);
					$array['usertype'] = "Student";
					$array['library'] = 0;
					$array['hostel'] = 0;
					$array['transport'] = 0;
					$array['create_date'] = date("Y-m-d");
					$array['year'] = $year;
					$array['yearID'] = return_year($this->input->post("yearsOrSemester"));
					$array['totalamount'] = 0;
					$array["create_date"] = date("Y-m-d h:i:s");
					$array["modify_date"] = date("Y-m-d h:i:s");
					$array["create_userID"] = $this->session->userdata('loginuserID');
					$array["create_username"] = $this->session->userdata('username');
					$array["create_usertype"] = $this->session->userdata('usertype');
					$array["notification"] = '1';
					$array["notification_date"] = date('Y-m-d');
					$array["s_date"] = date('Y-m-d H:i:s');
					$array["studentactive"] = 1;
					$array['aadhar_photo']=$this->input->post('addharcardadd');
					$new_file = "defualt.png";
					$file_name_renameForDocs = $this->insert_with_image($this->input->post("username"));
					if(isset($_POST["images_data"]) && $_POST["images_data"] !="") 
					{
						$data = $_POST["images_data"];
						$image_array_1 = explode(";", $data);
						$image_array_2 = explode(",", $image_array_1[1]);
						$data = base64_decode($image_array_2[1]);
						$imageName = 'student'.time() . '.png';
					    $root = 'uploads/images/';
						file_put_contents($root.$imageName, $data);
						$array['photo']    = $imageName;	
					}
					else
					{
					   $array['photo'] = $new_file;
					}

					$insertID = $this->student_m->insert_student($array,$file_name_renameForDocs);
					
					// $sendmassage=sendmassagetostudent();  abhi kaam karna hai isme.....
					

					// $this->send_mail_new_regitration($insertID);
					

					$array_invoice = array();
					$array_invoice["amount"] = 0;
					$array_invoice["total_amount"] = 0;
					$array_invoice["tuitionFee"] =	0;
					$array_invoice["discount"] = 0;
					$array_invoice["lf_no"] = 0;					 
					$array_invoice["hostelFee"] = 0;
					$array_invoice["welfareFund"] = 0;
					$array_invoice["totalfeepaidperyear"] = 0;
					$array_invoice["feetype"] = 'clg';
					$array_invoice["yearsOrSemester"] = return_year($this->input->post("yearsOrSemester"));
					$array_invoice['adminID']=$adminID;
					$array_invoice['studentID']=$insertID;
				    // $this->invoice_gen_m->AddInvoice($insertID,$array,$adminID);

					$this->invoice_gen_m->addInvoicewithpayment($array_invoice,$getClass->duration);
			    if($usertype == "Accountant"){
					$this->session->set_flashdata('success',$this->lang->line('menu_success'));
					redirect(base_url("invoice/view/".$insertID.'?type=clg&year='.$array['yearID']));					   	
					}else{
					$this->session->set_flashdata('success',$this->lang->line('menu_success'));
					redirect(base_url("student/index"));						
					}
				}
			} 
			else 
			{
				$this->data["subview"] = "student/add";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		$email_Admin = $this->session->userdata("email");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Teacher" || $usertype == "superadmin" || $usertype == "Accountant") {
		    
			$id = $this->uri->segment(3);
			$url =$this->uri->segment(4);
			if((int)$id) {

				$this->data['classes'] = $this->student_m->get_classes();				
				if ($usertype == "Teacher") {
					$this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID,"counsellor"=>$loginuserID));
				}else{
				    $this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID));
				}

				if($this->data['student']) {

				$this->data['counsellor'] = $this->student_m->counsellor();
				$this->data['get_sub_courses'] = $this->student_m->get_sub_courses($this->data['student']->classesID);
				$this->data['get_acadamic_reqired'] = $this->student_m->get_acadamic_reqired($this->data['student']->classesID);
				$this->data['classes_single'] = $this->classes_m->get_single_classes($this->data['student']->classesID);
				$ArrayForRequired =  explode(',', $this->data["classes_single"]->education_detailsID);
				$this->data['academic_details'] = $this->student_m->get_academic_details($id,$ArrayForRequired);
				if(count($this->data['academic_details'])==0)
				{
					$updateeducation_details=$this->student_m->updateeducation_details($id,$ArrayForRequired);
					$this->data['academic_details'] = $this->student_m->get_academic_details($id,$ArrayForRequired);
				}
				$classesID = $this->data['student']->classesID;
				$this->data['set'] = $url;
					if($_POST) {
// print_r($_POST);
// exit();
						$rules = $this->rules();
					
						$this->form_validation->set_rules($rules);
						    if ($this->form_validation->run() == FALSE) 
						    {
							    // print_r($rules);
							    // echo validation_errors();
							    // exit();
							    $this->data["subview"] = "student/edit";
							    $this->load->view('_layout_main', $this->data);
						    }   
						    else 
						    {
    							$array = array();
    							$array["name"] = $this->input->post("name");
    							$array["father_name"] = $this->input->post("father_name");
    							$array["mother_name"] = $this->input->post("mother_name");
    							$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
    							$array["sex"] = $this->input->post("sex");
    							// $array["employment_status"] = $this->input->post("employment_status");
    							$array["email"] = $this->input->post("email");
    							$array["phone"] = $this->input->post("phone");
    							$array["address"] = $this->input->post("address");
    							$array["street"] = $this->input->post("street");
    							$array["pin"] = $this->input->post("pin");
    							// $array["sessionType"] = $this->input->post("sessionType");
    							// $array["examType"] = $this->input->post("examType");
    							$array["nationality"] = $this->input->post("nationality");
    							$array["aadhar"] = $this->input->post("aadhar");
    							$array["classesID"] = $this->input->post("classesID");
    							$sub_courses=$this->input->post("sub_coursesID");
    							if($sub_courses == "")
    							{
    								$sub_courses=0;
    							}
    							$array["sub_coursesID"] = $sub_courses;
    							$array["roll"] = $this->input->post("enrollment");
    							$array["enrollment"] = $this->input->post("enrollment");
    							$array['yearID'] = return_year($this->input->post("yearsOrSemester"));
    						    $array["yearsOrSemester"] = $this->input->post("yearsOrSemester");
    							$array["education_mode"] = $this->input->post("education_mode");
    							$array["student_status"] = 'fresher';
    							$array["session"] = $this->input->post("sessionFrom").'-'.$this->input->post("sessionTo");
    							$array["modify_date"] = date("Y-m-d h:i:s");
    							$file_name_renameForDocs = $this->insert_with_image($this->data['student']->username);
    							if(isset($_POST["images_data"]) && $_POST["images_data"] !="") {
    							if($this->data['student']->photo != 'defualt.png') {
    									unlink(FCPATH.'uploads/images/'.$this->data['student']->photo);
    							}
    							$data = $_POST["images_data"];
    							$image_array_1 = explode(";", $data);
    							$image_array_2 = explode(",", $image_array_1[1]);
    							$data = base64_decode($image_array_2[1]);
    							$imageName = 'student'.time() . '.png';
    						    $root = 'uploads/images/';
    							file_put_contents($root.$imageName, $data);
    							$array['photo']    = $imageName;	
							} 

							
					        // print_r($_POST['aadhar_image']);die;
						        $updatestudent = $this->student_m->update_student($array, $id,$file_name_renameForDocs);
    							//$this->session->set_flashdata('success', $this->lang->line('menu_success'));
    							// $this->send_mail_edit_regitration($updatestudent);

                            if($this->data['classes_single']->classesID != $this->input->post('classesID'))
                            {
								$this->db->where('studentID',$id);
								$this->db->delete('invoice');
								$getClass = $this->classes_m->get_single_classes($this->input->post('classesID'));	
						    				$array_invoice = array();
											$array_invoice["amount"] = 0;
											$array_invoice["total_amount"] = 0;
											$array_invoice["tuitionFee"] =	0;
											$array_invoice["discount"] = 0;
											$array_invoice["lf_no"] = 0;					 
											$array_invoice["hostelFee"] = 0;
											$array_invoice["welfareFund"] = 0;
											$array_invoice["totalfeepaidperyear"] = 0;
											$array_invoice["feetype"] = 'clg';
											$array_invoice["yearsOrSemester"] = return_year($this->input->post("yearsOrSemester"));
											$array_invoice['adminID']=$adminID;
											$array_invoice['studentID']=$id;
											$this->invoice_gen_m->addInvoicewithpayment($array_invoice,$getClass->duration);
						    }
							redirect(base_url("student/index/$url"));	
					}
					} else {
						$this->data["subview"] = "student/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {

					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function view() {
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Accountant") {
			$id = $this->uri->segment(3);
			$url =$this->uri->segment(4);
			if ((int)$id) {

			if ($usertype == "Teacher") {
					$this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID,"counsellor"=>$loginuserID));
				}else{
				    $this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID));
				}
				if($this->data["student"]){
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
			    $this->data["education"] = $this->student_m->get_educations($id,$ArrayForRequired);
			    // ...................view Attendence Student start......................................
			    $student = $this->subject_m->get_student_info($id);
			    $array_subject = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$id);
				foreach ($array_subject as $key => $value)
				{
					$array_subject[$key]['atd'] = $this->subject_m->attd_percent($value['subjectID'],$student->studentID);

				}
				$arrayoptionalsubject=$this->subject_m->showoptionalsubjectbystudent($student->classesID,$student->sub_coursesID,$id);
		        foreach ($arrayoptionalsubject as $key => $value) 
		        {
		        	$arrayoptionalsubject[$key]['atd'] = $this->subject_m->attd_percent($value['subjectID'],$student->studentID);
		        	$arrayoptionalsubject[$key]['optionalsubjectprogress']=$this->subject_m->getoptionalsubjectprogrees($value['subjectID']);
		        }
		        $this->data['optionalsubject']=$arrayoptionalsubject;
				$this->data['subjects'] = $array_subject;
				// ...................view Attendence Student end......................................
				if($this->data["student"] && $this->data["class"]) {

				    $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);
					$this->data['set'] = $url;
					if ($this->data["student"]->parentID) {
						$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
					}
					$this->data["subview"] = "student/view";
					$this->load->view('_layout_main', $this->data);
				} else {
				
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}

			}else{
				    $this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
			}


			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}


	// RAHUL
	function passwordReset(){
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype== "Accountant") {

			$uri = $this->uri->segment(3);
			if ((int)$uri) {
				if ($_POST) 
				{
					$mainpassword = $this->input->post("new_password");
					$this->session->set_userdata('emailpass',$mainpassword);
					$newPassword = $this->hash($mainpassword);
					$this->student_m->change_password_with_by_superadmin($uri,$newPassword);
					$this->send_mail_reset_Password($uri);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("student/index"));
				}else{
					$this->data["subview"] = "student/passwordReset";

					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			
			}
		}
		
	}

	// RAHUL
	public function send_mail_reset_Password($id) {
		
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");

		
		// $this->data['newpass'] = $this->input->post("new-password");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'Professor' || $usertype == "Accountant") {
    
	        $this->db->select('*');
	        $this->db->where('studentID',$id);
	        $this->db->from('student');
	      	$data['emailData']  = $this->db->get()->row();

			$dataEamil['username'] = $data['emailData']->username;
        	$dataEamil['name'] = $data['emailData']->name;
            $dataEamil['password']=$this->input->post("new_password");
            $subject="New password created for ITM software";
            $html = $this->load->view('emailTemplates/passwordresetmypanel', $dataEamil , true);
            $email=$data['emailData']->email;
            $fullname=$dataEamil['name'];
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']="";
            $dataEamil['institute']=$this->data["siteinfos"]->sname;           
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);

			if($sendmail==true) {
				$this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
				$this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function hash($string) {
		return hash("sha512", $string . config_item("encryption_key"));
	}


	public function print_preview() {


		$loginuserID = $this->session->userdata("loginuserID");
		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Student" || $usertype == "superadmin" || $usertype == "Support") {
			$id = $this->uri->segment(3);
			$url = $this->uri->segment(4);
		
	
			if ((int)$id) {

				if ($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support") {
				$this->data["student"] = $this->student_m->get_student(array('studentID'=>$id,'adminID'=>$loginuserID));
               } elseif ($usertype == "Student") {
               $this->data["student"] = $this->student_m->get_student_single(array('studentID'=>$loginuserID));
               }
				$this->data["class"] = $this->student_m->get_class($url);
		        $this->data["education"] = $this->student_m->get_educations($id);
				if($this->data["student"]) {
// echo phpinfo();
// exit();
				$this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				$this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

				    $this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);

					$html = $this->load->view('student/print_preview', $this->data, true);
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");
		if($usertype == "Admin" || $usertype == 'ClgAdmin') {
			$id = $this->input->post('id');
			$url = $this->input->post('set');

			if ((int)$id) {
				$this->data["student"] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$loginuserID));
	

				$this->data["education"] = $this->student_m->get_educations($id);
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
	
				if($this->data["student"] && $this->data["class"]) {
				   $this->load->library('html2pdf');
				   $this->html2pdf->folder('uploads/report');
				   $this->html2pdf->filename('Report.pdf');
				   $this->html2pdf->paper('a4', 'portrait');
				   $this->data['panel_title'] = $this->lang->line('panel_title');
     
				$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
				$this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
			    $this->data['get_session'] = $this->student_m->get_session_id($this->data["student"]->session);

				$this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

					$html = $this->load->view('student/print_preview', $this->data, true);
			
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));
					$this->email->attach($path);

						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

    public function send_mail_new_regitration($insertID) 
    {
        $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        $arr = array();
	    $this->db->select('email');
	    $this->db->from('user');
	    $where = "usertype = 'Admin' and status = 1 and adminID=$adminID ";
	    $this->db->where($where);
	    $sqls = $this->db->get();
	    $results = $sqls->result_array();
	    foreach($results as $rows)
	    {
	        $arr[] = array(
	                        $rows['email']
	                        );
	    }
	    
	    // $arrs = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($arr), ENT_NOQUOTES));
	    // $admin_user =  str_replace('"','',$arrs);
        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") 
        {
        	$getadmin=$this->professor_m->getsuperadminid($adminID);
            $this->db->select('*');
            $this->db->where('studentID',$insertID);
            $this->db->from('student');
            $query2  = $this->db->get()->row();
            $dataEamil['emailData'] = $query2;
            $dataEamil['password'] = $this->session->userdata('student_password');
            $subject="Welcome to ITM Campus Automation Sofware";
            $html = $this->load->view('emailTemplates/studentaddition/studentaddition', $dataEamil , true);
            $email=$query2->email;
            $fullname=$query2->name;
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']="";
            // $dataEamil['Accountant']=
            $dataEamil['institute']=$this->data["siteinfos"]->sname;
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
            
            if($sendmail == true || $sendmail == 1) 
            {
            	$dataEamil['superadminemail']=$getadmin->email;
            	$dataEamil['adminemail']=$arr;
            	$subjectadmin="New Student Addition Successful in ITM Campus Automation Software";
            	$htmladmin=$this->load->view('emailTemplates/studentaddition/studentadditionemailadmin',$dataEamil, true);
            	$sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
            	$accountant=$this->db->select('email')->where(array('usertype' => 'Accountant','status' => 1, 'adminID' => $adminID))->get('user')->row();
            	if($accountant)
            	{
            		$dataEamil['email']=$accountant->email;
            		$subjectaccountant="Update New Added Student Fee on ITM Campus Automation Sofware";
            	$htmlaccountant=$this->load->view('emailTemplates/studentaddition/studentadditionemailaccountant',$dataEamil, true);
            	$sendmail=emailBySendGridaccountant($subjectaccountant,$htmlaccountant,$dataEamil);
            	
            	}
            	
                $this->session->set_flashdata('success', $this->lang->line('mail_success'));
            } 
            else 
            {
                $this->session->set_flashdata('error', $this->lang->line('mail_error'));
            }
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}
	
	
	public function send_mail_edit_regitration($updatestudent)
	{
	    $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        $arr = array();
	    $this->db->select('email');
	    $this->db->from('user');
	    $where = "adminID = '".$adminID."' and create_usertype = 'ClgAdmin' and status = 1 and usertype = 'Admin'";
	    $this->db->where($where);
	    $sqls = $this->db->get();
	    $results = $sqls->result_array();
	    foreach($results as $rows)
	    {
	        $arr[] = array(
	                        $rows['email']
	                        );
	    }
	    
	   
	    // print_r($admin_user);die;
        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") 
        {
        	
        	$getadmin=$this->professor_m->getsuperadminid($adminID);
            $this->db->select('*');
            $this->db->where('studentID',$updatestudent);
            $this->db->from('student');
            $query2  = $this->db->get()->row();            
            $dataEamil['emailData'] = $query2;
            $subject="Details Updated on ITM Campus Automation Software";
            $html = $this->load->view('emailTemplates/studentaddition/studentedition', $dataEamil , true);
            $email=$query2->email;
            $fullname=$query2->name;
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']="";

            // $dataEamil['Accountant']=
            $dataEamil['institute']=$this->data["siteinfos"]->sname;
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
            if($sendmail == true) 
            {
            	// print_r("expression");die;
            	$dataEamil['superadminemail']=$getadmin->email;
            	$dataEamil['adminemail']=$arr;
            	// print_r(($dataEamil['adminemail'][0]));die;
            	$subjectadmin="Details Updated on ITM Campus Automation Software";
            	$htmladmin=$this->load->view('emailTemplates/studentaddition/studenteditionemailadmin',$dataEamil, true);
            	$sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);
                $this->session->set_flashdata('success', $this->lang->line('mail_success'));
            } 
            else 
            {
            	// print_r("expression1");die;
                $this->session->set_flashdata('error', $this->lang->line('mail_error'));
            }
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}
	
	public function send_mail_delete_regitration($id)
	{
	    $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        $arr = array();
	    $this->db->select('email');
	    $this->db->from('user');
	    $where = "adminID = '".$adminID."' and create_usertype = 'ClgAdmin' and status = 1 and usertype = 'Admin'";
	    $this->db->where($where);
	    $sqls = $this->db->get();
	    $results = $sqls->result_array();
	    foreach($results as $rows)
	    {
	        $arr[] = array(
	                        $rows['email']
	                        );
	    }
	    
	    $arrs = str_replace(array('[', ']'), '', htmlspecialchars(json_encode($arr), ENT_NOQUOTES));
	    $admin_user =  str_replace('"','',$arrs);
        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") 
        {
        	$getadmin=$this->professor_m->getsuperadminid($adminID);
            $this->db->select('*');
            $this->db->where('studentID',$id);
            $this->db->from('student');
            $query2  = $this->db->get()->row();
            
            $dataEamil['emailData'] = $query2;
            $subject="Profile successfully removed from ITM Campus Automation Software";
            $html = $this->load->view('emailTemplates/studentaddition/studentdelection', $dataEamil , true);
            $email=$query2->email;
            $fullname=$query2->name;
            $dataEamil['superadminemail']="";
            $dataEamil['adminemail']="";
            $dataEamil['institute']=$this->data["siteinfos"]->sname;
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);        
            if($sendmail == true) 
            {
            	$dataEamil['superadminemail']=$getadmin->email;
            	$dataEamil['adminemail']=$arr;
            	$subjectadmin="Student profile removed from ITM Campus Automation Software";
            	$htmladmin=$this->load->view('emailTemplates/studentaddition/studentdelectionemailadmin',$dataEamil, true);
            	$sendmail=emailBySendGridadmin($subjectadmin,$htmladmin,$dataEamil);	
                $this->session->set_flashdata('success', $this->lang->line('mail_success'));
            } 
            else 
            {
                $this->session->set_flashdata('error', $this->lang->line('mail_error'));
            }
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}
	
	
	public function unique_roll() {
		$id = $this->uri->segment(3);
		if((int)$id) {
			$student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "studentID !=" => $id, "classesID" => $this->input->post('classesID')));
			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "classesID" => $this->input->post('classesID')));

			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}
	}

	public function lol_username() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") {

		$id = $this->uri->segment(3);
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('admin' => 'admin','student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $student_info->email));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {	
			$tables = array('admin' => 'admin', 'student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}
}
	public function date_valid($sec) {
		$date = date("d-m-Y", strtotime($sec));
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);
	        $dd = $arr[0];
	        $mm = $arr[1];
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    }
	}

	public function unique_classesID() {
		if($this->input->post('classesID') == 0) {
			$this->form_validation->set_message("unique_classesID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	public function unique_sectionID() {
		if($this->input->post('sectionID') == 0) {
			$this->form_validation->set_message("unique_sectionID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	public function student_list() {
		$classID = $this->input->post('id');
		if((int)$classID) {
			$string = base_url("student/index/$classID");
			echo $string;
		} else {
			redirect(base_url("student/index"));
		}
	}

	public function unique_email() {
		$id = $this->uri->segment(3);
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('admin' => 'admin','student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('admin' => 'admin','student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
			
				return FALSE;
			} else {
		
				return TRUE;
			}
		}
	}


	public function exceed_paidamount(){
		$amount = $this->input->post('amount');
		$paidamount = $this->input->post('paidamount');
		
		if($amount < $paidamount){
			$this->form_validation->set_message("exceed_paidamount", "%s is Exceede");
     return FALSE;
		}else{

		return TRUE;
			
		}
	}


	function sectioncall() {
		$classesID = $this->input->post('id');
		if((int)$classesID) {
			$allsection = $this->section_m->get_order_by_section(array('classesID' => $classesID));
			echo "<option value='0'>", $this->lang->line("student_select_section"),"</option>";
			foreach ($allsection as $value) {
				echo "<option value=\"$value->sectionID\">",$value->section,"</option>";
			}
		}
	}

function checkemail(){
	$usertype = $this->session->userdata("usertype");
	$loginuserID = $this->session->userdata("loginuserID");
	if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") {
		$id = $this->uri->segment(3);
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('admin' => 'admin', 'student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('admin' => 'admin', 'student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
			 echo "no";
			} else {
			 echo "yes";
			}
		
		}
}
}
function required_mode(){

if ($this->input->post('education_mode')==2) {
	if ($this->input->post('semesterId')=='') {
	 $this->form_validation->set_message("semesterId","Please Select Semester");
	 return false;
	}else{
		return true;
	}
}else{
    if ($this->input->post('yearID')=='') {
	 $this->form_validation->set_message("yearID","Please Select Year");
	 return false;
	}else{
		return true;
	}
}

}

function Get_subCourses(){

	if($this->signin_m->loggedin() == FALSE) {
	header("Refresh:0");
	}
    $id = $this->input->post('id');
    $this->db->where('classesID',$id);
    $this->db->where('status',1);
    $getSubcourses =  $this->db->get('sub_courses');
    if ($getSubcourses->result_array()) {
		if(form_error('subCourseID')) 
			echo "<div class='form-group has-error' >";
		else     
			echo "<div class='form-group' >";
			echo "<label for='sectionID' class='col-sm-2 control-label' >Course Subcategory<span class='red-color'>*</span></label>
					<div class='col-sm-6'> 
						<select id='sub_CourseID' required  name='sub_coursesID' class='form-control' value='<?php echo set_value('sub_coursesID'); ?>'>
							<option value='' selected='selected' >Select Cource Subcategory</option>";

    foreach ($getSubcourses->result_array() as $key => $value) {
    	echo "<option value =".$value['sub_coursesID'].">".$value['sub_course']."</option>";
	}
	echo "</div>
		<span class='col-sm-4 control-label'>".form_error('subCourseID')."</span></div>";
    } 
}

function ResetSesession(){ 

	$this->session->unset_userdata('sessionFilter');
	   
}

function ResetCourses(){

	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('yos_filter');
	   
}

function ResetSubcourses(){

	$this->session->unset_userdata('subCourseID');

}

function ResetsessionType(){

	$this->session->unset_userdata('sessionType');

}

function ResetexamType(){

	$this->session->unset_userdata('examType');

}

function Reseteducation_mode(){

	$this->session->unset_userdata('education_mode');

}

function ResetteacherID(){

	$this->session->unset_userdata('teacherID');

}

function Resetpaymentstatus(){

	$this->session->unset_userdata('payment_status');

}

function Resetstudentposition(){

	$this->session->unset_userdata('student_position');

}

function Resetyos(){
	$this->session->unset_userdata('yos_filter');
}

  

function ResetAllfilter(){
	$this->session->unset_userdata('student_position');
	$this->session->unset_userdata('yos_filter');
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('payment_status');	
	$this->session->unset_userdata('teacherID');   
}

function ResetMorefilter(){
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('teacherID');
	   
}


public function uploadPicByCrop(){
 // $uri = $this->uri->segment(3);
 $data = $_POST["image"];
echo "<input type ='hidden' value = '".$data."' name = 'images_data' >";
}


	public function delete() {

		$usertype = $this->session->userdata("usertype");
		$email_Admin = $this->session->userdata("email");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") {
        
			$id = $this->uri->segment(3);
			if((int)$id) 
			{
			    $data = array(
                                'status'=>2
                            );
				$this->db->where('studentID',$id);

        	    $loginuserID = $this->session->userdata("loginuserID");
        		if ($usertype=='Teacher') 
        		{
        		    $this->db->where('counsellor', $loginuserID);
        		}
	            $affected	= $this->db->update('student',$data);
                if ($affected) {
                				$data =  array(
                				'current_login'=>0
                				);
    				$this->db->where('studentID',$id);
    				$this->db->update('school_sessions',$data);
                }

				//$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				$this->send_mail_delete_regitration($id);
				redirect(base_url("student/index"));
			} 
			else 
			{
    			redirect(base_url("student/index"));
			}

		} 
		else 
		{

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


	public function restore() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") {

			$id = $this->uri->segment(3);

			if((int)$id) {
$data = array(
'status'=>1
);
				$this->db->where('studentID',$id);
				$this->db->update('student',$data);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("student/index"));

			} else {

				redirect(base_url("student/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function multipleAction()
	{
		
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin' || $usertype == "Accountant") 
		{
			if ($this->input->post('Delete')) 
			{
				$checked_id =  array_unique($this->input->post('checked_id'));
				for ($i=0; $i <count($checked_id) ; $i++) 
				{ 
					$data = array(
						'status'=>2
					);
					$this->db->where('studentID',$checked_id[$i]);
					$loginuserID = $this->session->userdata("loginuserID");
					$usertype = $this->session->userdata("usertype");
					if ($usertype=='Teacher') 
					{
						$this->db->where('counsellor', $loginuserID);
					}
					$affected = $this->db->update('student',$data);
					if ($affected) 
					{
						$data =  array(
						'current_login'=>0
						);
						$this->db->where('studentID',$checked_id[$i]);
						$this->db->update('school_sessions',$data);
					}
				}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
			}
			if ($this->input->post('Draft')) {
				$checked_id =  array_unique($this->input->post('checked_id'));
				for ($i=0; $i <count($checked_id) ; $i++) 
				{ 
					$data = array(
						'status'=>0
					);
					$this->db->where('studentID',$checked_id[$i]);
					$loginuserID = $this->session->userdata("loginuserID");
					$usertype = $this->session->userdata("usertype");
					if ($usertype=='Teacher') 
					{
						$this->db->where('counsellor', $loginuserID);
					}
					$affected = $this->db->update('student',$data);
					if ($affected) 
					{
						$data =  array(
							'current_login'=>0
						);
						$this->db->where('studentID',$checked_id[$i]);
						$this->db->update('school_sessions',$data);
					}
				}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
			}
			if ($this->input->post('Active')) 
			{
				$checked_id =  array_unique($this->input->post('checked_id'));
				for ($i=0; $i <count($checked_id) ; $i++) 
				{ 
					$data = array(
						'status'=>1
					);
					$this->db->where('studentID',$checked_id[$i]);
					$loginuserID = $this->session->userdata("loginuserID");
					$usertype = $this->session->userdata("usertype");
					if ($usertype=='Teacher') 
					{
						$this->db->where('counsellor', $loginuserID);
					}
					$this->db->update('student',$data);
				}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
			}
			if($this->input->post('sendmailbyadmin'))
			{

				$checked_id =  array_unique($this->input->post('checked_id'));
				// print_r($checked_id);die();	
				$subjectname=$this->input->post('subjectname');
		        $writemail=$this->input->post('writemail');
		        
		       
		        
		        $dataEamil['title'] = $subjectname;
				$dataEamil['description'] = $writemail;
				$subject="New notice from ITM software";
	            $dataEamil['superadminemail']="";
	            $dataEamil['adminemail']=""; 
	            $dataEamil['institute']=$this->data["siteinfos"]->sname;
				
				for ($i=0; $i <count($checked_id) ; $i++) 	
				{
					$emailreciverby=$this->professor_m->getreciverstudentid($checked_id[$i]);
					if($emailreciverby)
					{
						$email=$emailreciverby->email;
						$fullname=$emailreciverby->name;
						$dataEamil['name']=$emailreciverby->name;
						$html = $this->load->view('emailTemplates/sendmailbyanyuser', $dataEamil , true);
						$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil); 
					}
					else
					{
						$this->session->set_flashdata('error',"E-mail Not Sent!!");
						redirect(base_url("student/index"));
					}
				}
				if($sendmail==true)
				{
					$this->session->set_flashdata('success',"E-mail Sent successfully");
					redirect(base_url("student/index"));
				}
				
			}
			
			if ($this->input->post('Promote')) 
			{
				$checked_id =  array_unique($this->input->post('checked_id'));
				$this->data['total_student']  =  $this->student_m->find_student_in_array($checked_id);
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('classesID'));
				$this->data["subview"] = "student/promote";
				$this->load->view('_layout_main', $this->data);
			}


		if ($this->input->post('examAdd')) 
			{
				$checked_id =  array_unique($this->input->post('checked_id'));


		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
        $uri=1;
		$this->db->where('studentID',$checked_id[0]);
        $this->db->where('adminID',$adminID);
        $query	 = $this->db->get('student');
        $StudentArray =  $query->row();

		$this->db->select('subjectID');
		$this->db->where('yearsOrSemester',$StudentArray->yearsOrSemester);
		$this->db->where('studentID',$StudentArray->studentID);
		$options_eligible = $this->db->get('optionalsubject')->result();


		$posts = $this->exam_m->make_datatables_exam_list($adminID,1,$StudentArray,$array=null,$indivi=0);

		if ($options_eligible) {
		 $posts2 = $this->exam_m->make_datatables_exam_list($adminID,$uri,$StudentArray,$options_eligible,$indivi=0);
		 $posts = array_merge($posts,$posts2);
		}

		// $uri = htmlentities(($this->uri->segment(3)));
		$totalData = count($posts);

$this->data['exam'] = $posts;
$this->data['studentID'] = $checked_id[0];
$this->data['studentdata'] = $StudentArray;

				$this->data["subview"] = "student/exam_list";
				$this->load->view('_layout_main', $this->data);
			}



		}
	}

function set_exam(){
$checked_id =  array_unique($this->input->post('id'));

$ex =  implode(',', $checked_id);

if(count($checked_id)==0){
$status = 0;
}else{
$status = 1;
}

$data  = array(
'examIDs' =>$ex , 
'exam_status'=>$status
);
$this->db->where('studentID',$this->input->post('studentID'));
$this->db->update('student',$data);


	$this->session->set_flashdata('success',"Student add for exam.");
					redirect(base_url("student/index"));

}

// ajax function
function ActiveStudent(){
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('TrashStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->unset_userdata('payment_status');
	$this->session->unset_userdata('AllStudent');
	$this->session->unset_userdata('passoutstudent');
	$this->session->set_userdata('ActiveStudent',1);
	$this->session->unset_userdata('student_position');
	$this->session->unset_userdata('yos_filter');
}
function DraftStudent(){
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('TrashStudent');
	$this->session->unset_userdata('passoutstudent');
	$this->session->unset_userdata('AllStudent');
	$this->session->set_userdata('DraftStudent',3);
	$this->session->unset_userdata('payment_status');
}
function TrashStudent(){
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->unset_userdata('passoutstudent');
	$this->session->unset_userdata('AllStudent');
	$this->session->set_userdata('TrashStudent',2);
	$this->session->unset_userdata('payment_status');
}
function passoutstudent(){
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->unset_userdata('AllStudent');
	$this->session->unset_userdata('TrashStudent');
	$this->session->set_userdata('passoutstudent',10);
	$this->session->unset_userdata('payment_status');
}
function AllStudent(){
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->unset_userdata('TrashStudent');
	$this->session->unset_userdata('passoutstudent');
	$this->session->set_userdata('AllStudent',4);
	$this->session->unset_userdata('payment_status');
}


function deleteAfterUSe(){

	$this->load->view('student/testing',$this->data);

}


function set_session_of_subCourse(){
	
}
	function active() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == 'ClgAdmin') {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'chacked') {
						$this->student_m->update_student(array('studentactive' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchacked') {
						$this->student_m->update_student(array('studentactive' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}
	
	function send_sms()
	{
	    $message = "Hello ".$this->input->post('name')."\nYour username is ".$this->input->post('username')."\ndefault password is ".$this->input->post('password')."\nHappy Learning!";
	    
	    $requestParams = array(
            'user' => 'academicedge',
            'pass' => 'bbz10@123',
            'sender' => 'EDGSIS',
            'phone' => $this->input->post('phone'),
            'text' => $message,
            'priority' => 'ndnd',
            'stype' => 'normal'
        );
        $apiUrl = "http://bhashsms.com/api/sendmsg.php?";
        foreach($requestParams as $key => $val){
            $apiUrl .= $key.'='.urlencode($val).'&';
        }
        $apiUrl = rtrim($apiUrl, "&");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $return = json_encode($result);
        if($return)
        {
            echo "SMS sent successfully";
        }
        else
        {
            echo "error!";
        }
	}
	
	function promote_student()
	{
		$checked_id = $this->input->post('checked_id');
		
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		$yos_filter = $this->input->post('yos_filter');
		if($usertype=='ClgAdmin' || $usertype=='Admin' || $usertype=='Superadmin' || $usertype=='Accountant')
		{
			for ($i=0; $i <count($checked_id) ; $i++) 
			{

				if($_POST['passoutstudent']==10)
				{
					$data = array(
								'status' => 10 
							);
					$this->db->where('studentID',$checked_id[$i]);
					$this->db->update('student',$data);
					$sql="";
				}
				else
				{
					$data = array(
								'yearsOrSemester' => $yos_filter[$i] 
							);
					$this->db->where('studentID',$checked_id[$i]);
					$sql = $this->db->update('student',$data);
				}
				if($sql==true)
				{ 
					$array = array('studentID' => $checked_id[$i],'student_position' => 1,'feetype' =>'clg' );
					$invoice_data = $this->db->select('*')->from('invoice')->where($array)->order_by('invoiceID','DESC')->get()->row();
                    
                   $yearwisepromte= return_year($yos_filter[$i]);
                   // print_r($yearwisepromte);die;
						if($invoice_data->yearsOrSemester==$yearwisepromte)
						{
							// $this->session->set_flashdata('error', "error");
							// redirect(base_url("student/index"));//commented by suneel

							
						}
						else
						{
							// print_r($invoice_data->feetype);die;
							$invoice_dataupdate= $this->db->where('studentID',$checked_id[$i],'student_position',1,'feetype','clg')->update('invoice',array('student_position' =>0));
							$datas = array(
									'classesID' => $invoice_data->classesID,
									'adminID' => $invoice_data->adminID,
									'studentID' => $checked_id[$i],
									'student' => $invoice_data->student,
									'roll' => $invoice_data->roll,
									'feetype' => $invoice_data->feetype,
									'amount' => $invoice_data->totalfeepaidperyear+($invoice_data->amount-$invoice_data->paidamount),
									'total_amount' => $invoice_data->total_amount,
									'paidamount' => 0,
									'userID' => $invoice_data->userID,
									'usertype' => $usertype,
									'uname' => $invoice_data->uname,
									'status' => 3,
									'paymenttype' => $invoice_data->paymenttype,
									'date' => $invoice_data->date,
									'paiddate' => $invoice_data->paiddate,
									'year' => $invoice_data->year,
									'total_install' => $invoice_data->total_install,
									'c_install' => $invoice_data->c_install,
									'hostelFee' => $invoice_data->hostelFee,
									'welfareFund' => $invoice_data->welfareFund,
									'lf_no' => $invoice_data->lf_no,
									'discount' => $invoice_data->discount,
									'tuitionFee' => $invoice_data->tuitionFee,
									'totalfeepaidperyear' => $invoice_data->totalfeepaidperyear,
									'enrollmentFee' => $invoice_data->enrollmentFee,
									'examinationFee' => $invoice_data->examinationFee,
									'yearsOrSemester' => $yearwisepromte,
									'student_position' => '1',
									'outstanding_fees' => $invoice_data->amount-$invoice_data->paidamount,
									'promote_date' => date("Y-m-d")
								);
							$invo = $this->db->insert('invoice',$datas);
						}
				}
			}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
		}
	}

	public function send_feedbackbyscreenshot()
	{
		$image = $this->input->post('base64URL');
		$location = "uploads/";
		$image_parts = explode(";base64,", $image);

		$image_base64 = base64_decode($image_parts[1]);

		$filename = "screenshot_".uniqid().'.png';

		$file = $location . $filename;

        file_put_contents($file, $image_base64);
        $path=base_url($file);
        echo $path;
		// print_r($filename);die;
	}
	public function send_feedbackbyscreenshotdeletescreenshot()
	{
		$base=$this->input->post('base');
		unlink("uploads/".$base);
		return;
	}
	public function aadharcardupload()
	{
		$id = $this->input->get('studentID');
		if(isset($_FILES["aadhar_file"]["name"]))  
           {  
           	// print_r($id);die;
                $config['upload_path'] = 'uploads/images';  
                $config['allowed_types'] = 'jpg|jpeg|png|pdf';  
                $this->load->library('upload', $config);
               // $this->upload->initialize($config);
                
                if(!$this->upload->do_upload('aadhar_file'))  
                {  
                     echo $this->upload->display_errors();  
                }  
                else  
                {  
                	$data = $this->upload->data();
                	
           //      	$this->db->select('aadhar_photo');
		        	// $this->db->where('studentID',$id);
		        	// $photo=$this->db->get('student')->row();
		        	// if($photo)
		        	// {
		        	// 	unlink(FCPATH.'uploads/images/'.$photo->aadhar_photo);
		        	// }
                    $data = $this->upload->data();  
                    $this->db->where('studentID',$id);
            		$this->db->update('student',array('aadhar_photo' =>$data['file_name']));
                    echo '<img src="'.base_url().'uploads/images/'.$data["file_name"].'" width="300" height="225" class="img-thumbnail" />';  
                }  
           } 
           else
           {
           	 echo "error";
           }	    
	}
	public function aadharcardadd()
	{
		if(isset($_FILES["aadhar_file"]["name"]))  
           {  
           	// print_r($id);die;
                $config['upload_path'] = 'uploads/images';  
                $config['allowed_types'] = 'jpg|jpeg|png|pdf';  
                $this->load->library('upload', $config);
               // $this->upload->initialize($config);
                
                if(!$this->upload->do_upload('aadhar_file'))  
                {  
                     echo $this->upload->display_errors();  
                }  
                else  
                {  
                	$data = $this->upload->data();
                    $photo=$data['file_name'];
                    echo '<img src="'.base_url().'uploads/images/'.$data["file_name"].'" width="300" height="225" class="img-thumbnail" />';
                    echo '<input type="hidden" value="'.$data["file_name"].'" name="addharcardadd" >';  
                }  
           } 
           else
           {
           	 echo "error";
           }	    
	}
	// public function updateparentdata()
	// {
	// 	$this->db->select('studentID,adminID,status,father_name,phone,dob');
	// 	$this->db->from('student');
	// 	$studentdata=$this->db->get()->result();
	// 	foreach ($studentdata as $key => $value)
	// 	{
	// 		$pass = str_replace("-", "", date('d-m-Y', strtotime($value->dob)));
	// 		$arrayName = array('adminID' =>$value->adminID,'studentID'=>$value->studentID,'username'=>$value->phone,'password'=>$this->student_m->hash($pass),'usertype'=>"Parent",'create_date'=>date('Y-m-d'),'status'=>$value->status,'name'=>$value->father_name);
	// 		$this->db->insert('parent',$arrayName);
	// 	}

	// }



	public function dau5()
	{
		$this->db->where('exam_status',1);
		$this->db->from('student');
		$studentdata=$this->db->get()->result();
		$i =1;
		foreach ($studentdata as $key => $value)
		{
			$this->db->select('ets_result.*,ets_quiz.*');
			$this->db->from('ets_quiz');
			$this->db->where_in('ets_quiz.subjectID',explode(',', $value->examIDs));
			$this->db->where('ets_result.uid',$value->studentID);
			$this->db->join('ets_result', 'ets_result.quid = ets_quiz.quid', 'inner');
			$exam = $this->db->get()->result();


   echo $i++;
   echo "<br>";
 echo  '<table class="table table-bordered">
    <thead>
      <tr>
    
        <th>name.</th>
        <th>subject</th>
  
      </tr>
    </thead>
    <tbody>';
			foreach ($exam as $keys => $values) {
	// $this->db->where('rid',$values->rid);
	// $this->db->delete('ets_result');

				echo '<tr>';

				echo "<td>".$value->name."-".$value->studentID."</td>";
				echo "<td>".$values->quiz_name."</td>";
				echo '</tr>';
			}

                      echo '</tbody>
  </table>';


		}

	}

	
}
