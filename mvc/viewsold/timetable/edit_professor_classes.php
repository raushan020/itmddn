<style>
    .attachment-block {
        border-radius: 0px;
        width: 225px;
        margin: 0px;
    }
    td,th{
        border: 2px solid gray;
    }
    #trs {
        background:#e8eef1;
    }
    .dataTables_info {
        display:none;   
    }
    .selectBox {
      position: relative;
    }
    
    .selectBox select {
        width:100%;
        color: #000;
        font-size: 13px;
        height: 36px;
        box-shadow: none;
        border-radius: 2px;
        border: 1px solid #e8eef1;
        font-weight: 600;
    }
    
    .overSelect {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
    }

    #checkboxes {
        display: none;
        border: 1px #0064f9 solid;
        line-height: 2em;
    }
    
    #checkboxes label {
        display: block;
        padding: 0px 10px;
        margin-bottom: 0px;
    }
    
    #checkboxes label:hover {
        background-color: #1e90ff;
        color:#fff;
    }
    #green {
        color:green;
    }
    #red {
        color:red;
    }
    #actions {
        text-align: right;
    }
</style>
<div class="">
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"><i class="fa fa-table"></i> Time Table</h3>
    </div>  
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Time Table</li>
        </ol>
    </div>
</div>
  <div class="container">
     <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
    <div class="row">
      <div class="">
        <div class="card" style="width: 92%;">
          <div class="card-body">
			<div id="save_data"></div>
			<?php
				if(empty($professor))
				{
					echo $this->session->flashdata('msg');
				}
				else
				{
					foreach($professor as $row)
					{
						$id = $row['id'];
						$department_id = $row['department_id'];
						$department_name =  $row['department_name'];
						$course_id =  $row['course_id'];
						$course_name =  $row['classes'];
						$semester =  $row['semester'];
						$subject_id =  $row['subject_id'];
						$subject =  $row['subject'];
						$professor_id =  $row['professor_id'];
						$professor =  $row['name'];
						$times = $row['times'];
						$days = explode(",",$row['days']);
					}
			?>
					<div id="save_data"></div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="department" class="control-label">Departments</label>
                            <input type="hidden" name="ids" id="ids" value="<?php echo $id; ?>">
                            <select class="form-control" name="department" id="department" value="">
                                <option value="<?php echo $department_id; ?>"><?php echo $department_name; ?></option>
                                <option value=''> Select Department </option>
                                <?php 
                                    foreach ($department as $key => $value) 
                                    {
                                ?>
                                        <option value="<?php echo $value->departmentID; ?>">
                                            <?php echo $value->department_name; ?>
                                        </option>
                                <?php
                                    }
                                ?>
                            </select>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="classesID" class="control-label">
                                Course
                            </label>
                            <div class="">
                                <select name="classesID" id="course" class="form-control">
                                    <option value="<?php echo $course_id; ?>"><?php echo $course_name; ?></option>
                                    <option value="">Select Course</option>
                                </select>           
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="course_semester" class="control-label">
                                Course Semester
                            </label>
                            <div class="">
                                <select name="course_semester" id="course_semester" class="form-control">
                                    <option value="<?php echo $semester; ?>"><?php echo $semester; ?></option>
                                    <option value="">Select Semester</option>
                                </select>           
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="subjects" class="control-label">
                                Subject
                            </label>
                            <div class="">
                                <select name="subjects" id="subjects" class="form-control">
                                    <option value="<?php echo $subject_id; ?>"><?php echo $subject; ?></option>
                                    <option value="">Select Subject</option>
                                </select>           
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="professors" class="control-label">
                                Professor
                            </label>
                            <div class="">
                                <select name="professors" id="professors" class="form-control">
                                    <option value="<?php echo $professor_id; ?>"><?php echo $professor; ?></option>
                                    <option value="Break">Break</option>
                                </select>           
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="startTime" class="control-label">
                                Time
                            </label>
                            <div class="">
                                <select class="form-control" id="times">
                                    <option value="<?php echo $times; ?>"><?php echo $times; ?></option>
                                    <option value="">Select Time</option>
                                    <option value="9:05 - 9:50">9:05 - 9:50</option>
                                    <option value="9:50 - 10:35">9:50 - 10:35</option>
                                    <option value="10:35 - 11:20">10:35 - 11:20</option>
                                    <option value="11:20 - 12:05">11:20 - 12:05</option>
                                    <option value="12:05 - 12:50">12:05 - 12:50</option>
                                    <option value="1:15 - 2:00">1:15 - 2:00</option>
                                    <option value="2:00 - 2:45">2:00 - 2:45</option>
                                    <option value="2:45 - 3:30">2:45 - 3:30</option>
                                    <option value="3:30 - 4:15">3:30 - 4:15</option>
                                    <option value="4:15 - 5:00">4:15 - 5:00</option>
                                </select>          
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="endTime" class="control-label">
                                Day
                            </label>
                            <div class="multiselect">
                                <div class="selectBox" onclick="showCheckboxes()">
                                  <select>
                                    <option>Select an option</option>
                                  </select>
                                  <div class="overSelect"></div>
                                </div>
                                <div id="checkboxes">
                                  <label for="Mon"><input type="checkbox" id="Mon" class="days" <?php if(in_array('Mon',$days)){ echo 'checked'; }else{ echo ''; } ?>/>Monday</label>
                                  <label for="Tue"><input type="checkbox" id="Tue" class="days" <?php if(in_array('Tue',$days)){ echo 'checked'; }else{ echo ''; } ?>/>Tuesday</label>
                                  <label for="Wed"><input type="checkbox" id="Wed" class="days" <?php if(in_array('Wed',$days)){ echo 'checked'; }else{ echo ''; } ?>/>Wednesday</label>
                                  <label for="Thu"><input type="checkbox" id="Thu" class="days" <?php if(in_array('Thu',$days)){ echo 'checked'; }else{ echo ''; } ?>/>Thursday</label>
                                  <label for="Fri"><input type="checkbox" id="Fri" class="days" <?php if(in_array('Fri',$days)){ echo 'checked'; }else{ echo ''; } ?>/>Friday</label>
                                </div>
                              </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="">
                                <input type="submit" class="btn btn-success" id="submit"/>          
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
			<?php
				}
			?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } 
        else 
        {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
</script>
<script type="text/javascript">
    $('#department').change(function() {
        id = $(this).val();
        $.ajax({
            type:"POST",
            data:{"id":id},
            url:"<?=base_url('timetable/course')?>",
            success:function(data){
                $("#course").html(data);
            }
        });
    });
    $("#course").change(function(){
        courseID = $(this).val();
        $.ajax({
            type:"POST",
            data:{"courseID":courseID},
            url:"<?=base_url('timetable/semester')?>",
            success:function(data){
                $("#course_semester").html(data);
            }
        });
        
        $.ajax({
            type:"POST",
            data:{"courseID":courseID},
            url:"<?=base_url('timetable/semester_wise_course')?>",
            success:function(data){
                $("#views").html(data);
                $('#timeTable').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ],
                    "bPaginate": false
                });
            }
        });
    });
    $("#course_semester").change(function(){
        yearsOrSemester = $("#course_semester").val();
        course = $("#course").val();
        $.ajax({
            type:"POST",
            data:{"yearsOrSemester":yearsOrSemester,"course":course},
            url:"<?=base_url('timetable/subjects')?>",
            success:function(data){
                $("#subjects").html(data);
            }
        });
    });
    $("#subjects").change(function(){
        subjectID = $(this).val();
        professorID = $(this).find(':selected').attr('data-id')
        $.ajax({
            type:"POST",
            data:{"subjectID":subjectID,"professorID":professorID},
            url:"<?=base_url('timetable/professors')?>",
            success:function(data){
                $("#professors").html(data);
            }
        });
    });
	
	$("#submit").click(function(){
	    ids = $("#ids").val();
		department_id = $("#department").val();
		course_id = $("#course").val();
		semester = $("#course_semester").val();
		subject_id = $("#subjects").val();
		professor_id = $("#professors").val();
		days = $(".days").map(function() {
                    if(this.checked){
                      return this.id;
                    }
                }).get().join(",");
		times = $("#times").val();
		$.ajax({
			type:"POST",
			data:{"ids":ids,"department_id":department_id,"course_id":course_id,"semester":semester,"subject_id":subject_id,"professor_id":professor_id,"days":days,"times":times},
			url:"<?=base_url('timetable/professors_lecture_data_edit')?>",
			success:function(data){
				$("#save_data").html(data);
				setTimeout(function(){
					window.location.href = "<?php echo base_url(); ?>timetable/index";
				},1000);
			}
		});
	});
</script>
<script type="text/javascript">
    function goBack() {
  window.history.back();
  console.log('We are in previous page');
}

function goForward() {
  window.history.forward();
  console.log('We are in next page');
}
  </script>