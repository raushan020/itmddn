<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
<style>
	#calendar {
		max-width: 97%;
		margin: 0 auto;
		margin-top: 20em;
		border-top: 1px solid #80808029;
		padding-top: 25px;
	}
	.fc-title{
		white-space: initial;
	}
	@media (min-width: 1200px)
	{
		.container {
			width: 970px;
		}
	}
	.modal {
		top: 8em;
	}
	#modalTitle {
		text-transform: capitalize;
	}
	#left {
		float: left;
		width: 20%;
	}
	#right {
		float: right;
		width: 80%;
	}
	.fc-time{
		display:none;
	}
	#edit_delete {
		float:right;
	}
	.fc-event, .fc-event-dot {
		background-color: blue!important;
	}
	.fc-event {
		
		border-radius: 0px!important;
		border: 1px solid blue!important;
	}
	a.fc-more {
		font-size: 14px;
		font-weight: 600;
		color: red;
	}
</style>
<div class="">
  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-table"></i> Time Tables</h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
              <li>
                <a href="<?=base_url(" dashboard/index ")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a>
              </li>
              <li class="active">Time Table</li>
          </ol>
      </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="">
        <div class="card">
          <div class="card-body">
			<div id="save_data"></div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="department" class="control-label">Departments</label>
                <select class="form-control" name="department" id="department" value="">
                  <option value=''> Select Department </option>
                  <?php 
                    foreach ($department as $key => $value) 
                    {
                  ?>
                        <option value="<?php echo $value->departmentID; ?>"><?php echo $value->department_name; ?></option>
                  <?php 
                    }
                  ?>
                </select>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-sm-6">
				<div class="form-group">
					<label for="classesID" class="control-label">
						Course
					</label>
					<div class="">
						<select name="classesID" id="course" class="form-control">
							<option value="">Select Course</option>
						</select>           
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-6">
				<div class="form-group">
					<label for="course_semester" class="control-label">
						Course Semester
					</label>
					<div class="">
						<select name="course_semester" id="course_semester" class="form-control">
							<option value="">Select Semester</option>
						</select>           
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-6">
				<div class="form-group">
					<label for="subjects" class="control-label">
						Subject
					</label>
					<div class="">
						<select name="subjects" id="subjects" class="form-control">
							<option value="">Select Subject</option>
						</select>           
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-4">
				<div class="form-group">
					<label for="professors" class="control-label">
						Professor
					</label>
					<div class="">
						<select name="professors" id="professors" class="form-control">
							<option value="Break">Break</option>
						</select>           
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-2">
				<div class="form-group">
					<label for="startDate" class="control-label">
						Start Month
					</label>
					<div class="">
						<input type="text" class="form-control" id="startDate"/>          
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-2">
				<div class="form-group">
					<label for="endDate" class="control-label">
						End Month
					</label>
					<div class="">
						<input type="text" class="form-control" id="endDate"/>          
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-2">
				<div class="form-group">
					<label for="startTime" class="control-label">
						Start Time
					</label>
					<div class="">
						<input type="time" class="form-control" id="startTime"/>          
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-2">
				<div class="form-group">
					<label for="endTime" class="control-label">
						End Time
					</label>
					<div class="">
						<input type="time" class="form-control" id="endTime"/>          
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div class="col-sm-2">
				<div class="form-group">
					<div class="">
						<input type="submit" class="btn btn-success" id="submit"/>          
					</div>
					<div class="clearfix"></div>
				</div>
            </div>
			
			<div id='calendar'></div>
			
          </div>
        </div>
		
		
		<div id="fullCalModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="modalTitle">Modal Header</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-9">
								<ul style="line-height:2.5em;">
									<li>
										<div id="left">Course:</div>
										<div id="right">
											<div id="classes"></div>
										</div>
									</li>
									<li>
										<div id="left">Semester:</div>
										<div id="right">
											<div id="semester"></div>
										</div>
									</li>
									<li>
										<div id="left">Subject:</div>
										<div id="right">
											<div id="subject"></div>
										</div>
									</li>
									<li>
										<div id="left">Date:</div>
										<div id="right">
											<div id="startdate"></div>
										</div>
									</li>
									<li>
										<div id="left">Timing:</div>
										<div id="right">
											<div id="timestart"></div>
										</div>
									</li>
								</ul>
							</div>
							<div class="col-lg-3">
								<div id="edit_delete">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
      </div>
    </div>
  </div>
</div>
<?php
	$array = array();
	foreach($lecture as $row)
	{
		$startDate = $row['startDate'];
		$start = date("Y-m-d", strtotime($startDate));
		
		$endDate = $row['endDate'];
		$end = date("Y-m-d", strtotime($endDate));
		
		$array[] = array(
							'id' => $row['id'],
							'title' => 'Professor: '.$row['name'].' / '.$row['classes'],
							'classes' => $row['classes'],
							'semester' => $row['semester'],
							'subject' => $row['subject'],
							'times' => $row['startTime'].' To '.$row['endTime'],
							'dates' => $start.' To '.$end,
							'start' => $start,
							'end' => $end."T23:59:00"
						);
	}
	$datas = json_encode($array);
	$datas = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $datas);
?>

<script>
$(document).ready(function () {
	$("#startDate").datepicker({
		minDate: 0,
		dateFormat:'dd-mm-yy',
		onSelect: function(date) {
			$("#endDate").datepicker('option', 'minDate', date);
		}
	});
	$("#endDate").datepicker({dateFormat:'dd-mm-yy'});
});

function minFromMidnight(tm){
	var ampm= tm.substr(-2)
	var clk = tm.substr(0, 5);
	var m  = parseInt(clk.match(/\d+$/)[0], 10);
	var h  = parseInt(clk.match(/^\d+/)[0], 10);
	h += (ampm.match(/pm/i))? 12: 0;
	return h*60+m;
}
</script>
<script type="text/javascript">
    $('#department').change(function() {
		id = $(this).val();
		$.ajax({
			type:"POST",
			data:{"id":id},
			url:"<?=base_url('timetable/course')?>",
			success:function(data){
				$("#course").html(data);
			}
		});
    });
	$("#course").change(function(){
		courseID = $(this).val();
		$.ajax({
			type:"POST",
			data:{"courseID":courseID},
			url:"<?=base_url('timetable/semester')?>",
			success:function(data){
				$("#course_semester").html(data);
			}
		});
	});
	$("#course_semester").change(function(){
		yearsOrSemester = $("#course_semester").val();
		course = $("#course").val();
		$.ajax({
			type:"POST",
			data:{"yearsOrSemester":yearsOrSemester,"course":course},
			url:"<?=base_url('timetable/subjects')?>",
			success:function(data){
				$("#subjects").html(data);
			}
		});
	});
	$("#subjects").change(function(){
		subjectID = $(this).val();
		professorID = $(this).find(':selected').attr('data-id')
		$.ajax({
			type:"POST",
			data:{"subjectID":subjectID,"professorID":professorID},
			url:"<?=base_url('timetable/professors')?>",
			success:function(data){
				$("#professors").html(data);
			}
		});
	});
	$("#submit").click(function(){
		department_id = $("#department").val();
		course_id = $("#course").val();
		semester = $("#course_semester").val();
		subject_id = $("#subjects").val();
		professor_id = $("#professors").val();
		startDate = $("#startDate").val();
		endDate = $("#endDate").val();
		startTime = $("#startTime").val();
		endTime = $("#endTime").val();
		
		st = minFromMidnight(startTime);
		et = minFromMidnight(endTime);
		if(st > et || st == et)
		{
			alert("End time must be greater than start time");
		}
		else
		{
			$.ajax({
				type:"POST",
				data:{"department_id":department_id,"course_id":course_id,"semester":semester,"subject_id":subject_id,"professor_id":professor_id,"startDate":startDate,"endDate":endDate,"startTime":startTime,"endTime":endTime},
				url:"<?=base_url('timetable/professors_lecture_data')?>",
				success:function(data){
					$("#save_data").html(data);
					setTimeout(function(){
						location.reload();
					},1000);
				}
			});
		}
	});
</script>