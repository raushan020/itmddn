<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-table"></i> Time Table</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Time Table</li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
               <div class="card-body">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for="department" class="control-label">Select Department</label>
                        <!-- onchange="department_filter_url($(this).val())" -->
                        <select class="form-control" name="department" onchange = 'departmentwiseCoursewithTimetable($(this).val())' id="department" value="">
                           <option> Select Department  </option>
                           <?php foreach ($department as $key => $value) {
                              if ($_GET['id']!='') {
                                
                              
                              ?>
                           <option value="<?php echo $value->departmentID; ?>" <?php if($value->departmentID==$_GET['id']){echo "selected";}else{echo "";}?>><?php echo $value->department_name; ?></option>
                           <?php }else{ ?>
                           <?php  
                              if ($value->departmentID==$this->session->userdata('FilterDepartmentTimetable')) {
                                  $selected =  "Selected";
                              }else{
                                $selected =  "";
                              }
                              ?>
                           <option value="<?php echo $value->departmentID; ?>" <?php echo $selected;?> ><?php echo $value->department_name; ?></option>
                           <?php } } ?>
                        </select>
                        <div class="clearfix"></div>
                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetDepartmentwithtimetable()">Reset This Filter </a></div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                           <div class="form-group">
                              <label for="classesID" class="control-label">
                              Course
                              </label>
                              <div class="">
                                 <?php
                                    $array = array("0" => "Select Course");
                                    foreach ($classesData as $classa) {
                                        $array[$classa->classesID] = $classa->classes;
                                    }
                                    echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterCoursewithTimetable')), "id='course' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");
                                    ?>
                              </div>
                              <div class="clearfix"></div>
                              <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourseswithtimetable()">Reset This Filter </a></div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- <div class="col-sm-4">
                     <div class="">
                         <form style="" class="form-horizontal" role="form" method="post">
                             <div class="form-group">
                                 <label for="classesID" class="control-label">Semester/Year</label>
                                 <div class="">
                                     <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemester' value='<?php echo set_value('semesterId'); ?>'> 
                                     <option>Select</option>      
                                      <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                        if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterwithtimetable')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                        }
                                        }
                                        
                                        else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterwithtimetable')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                        }
                                        }
                                        }
                                        ?>
                                     </select>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYearwithtimetable()">Reset This Filter </a></div>
                             </div>
                         </form>
                     </div>
                     </div> -->
                  <style type="text/css">
                     .time_table_drppdo{
                     width: 159px !important;
                     }
                  </style>
                  <!-- <div class="col-sm-4">
                     <div id="departmentCourse"></div>
                     </div>
                     <div class="col-sm-4">
                     <div id="fetchYearsAndSem"></div>
                     </div> -->  
                  <div class="col-sm-12">&nbsp;</div>
                  <div class="col-sm-12">
                     <div class="table-responsive">
                        <table class="table table-responsive table-striped table-bordered table-hover" id="timeTable">
                           <thead>
                              <tr>
                                 <th>Course</th>
                                 <th>9:30-10:15am</th>
                                 <th>10:15-11:00am</th>
                                 <th>11:00-11:45am</th>
                                 <th>11:45-12:30noon</th>
                                 <th>12:30-1:15pm</th>
                                 <th>1:15-1:30pm</th>
                                 <th>1:30-2:15pm</th>
                                 <th>2:15-3:00pm</th>
                                 <th>3:00-3:45pm</th>
                                 <th>3:45-4:30pm</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                                 foreach ($timetable as $key1 => $value) {
                                 
                                   foreach ($value as $key2 => $values) {
                                  ?>
                              <tr>
                                 <td><?=$values['classes'] ?><br><?=$values['yearsOrSemester'] ?></td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'9:30','10:15', '1<?php echo $key1.$key2 ?>','<?php echo $values['classesID'] ?>','<?php echo $values['yearsOrSemester'] ?>')" >
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break';
                                          
                                          foreach ($values['subjects'] as $key => $subject) {
                                          
                                             if($subject->startTime=='9:30'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo"  id="oprtionProfessor1<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'10:15','11:00', '2<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='10:15'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor2<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'11:00','11:45', '3<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='11:00'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor3<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'11:45','12:30', '4<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='11:45'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor4<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'12:30','1:15', '5<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='12:30'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor5<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'1:15','1:30', '6<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='1:15'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor6<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'1:30','2:15', '7<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='1:30'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor7<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'2:15','3:00', '8<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='2:15'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor8<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'3:00','3:45', '9<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='3:00'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor9<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                                 <td>
                                    <select class="form-control time_table_drppdo" onchange="timeTableChange($(this).val(),'3:45','4:30', '10<?php echo $key1.$key2 ?>')">
                                       <option>Subject</option>
                                       <?php
                                          $professor_name = 'Break'; 
                                          foreach ($values['subjects'] as $key => $subject) {
                                            if($subject->startTime=='3:45'){
                                               $selected  = "selected";
                                              $professor_name = $this->db->where('professorID',$subject->professorID)->get('professor')->row()->name;
                                              }else{
                                               $selected  = "";
                                              } 
                                           ?>
                                       <option value="<?php echo $subject->subjectID ?>" <?php echo $selected  ?>><?php echo $subject->subject ?></option>
                                       <?php } ?>
                                       <option value="0">Break</option>
                                    </select>
                                    <select class="form-control time_table_drppdo" id="oprtionProfessor10<?php echo $key1.$key2 ?>">
                                       <option><?php echo $professor_name; ?></option>
                                    </select>
                                 </td>
                              </tr>
                              <?php
                                 }}
                                 ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   button, input, optgroup, select, textarea{    
   color: inherit;
   }
</style>
<script type="text/javascript">
   function timeTableChange(subjectID ,startTime,endTime,id,classesID,yearsOrSemester){
   
            $.ajax({
                 url: base_url+'timetable/timeTableChange',
                 type: 'POST',
                 data:{subjectID:subjectID,startTime:startTime,endTime:endTime,classesID:classesID,yearsOrSemester:yearsOrSemester},
                 success: function(data){
                      var datawithjs = JSON.parse(data);
   
                     if(datawithjs.error=='error'){
                       $('#flashmsgError'+id).html('Subject');
                     }
   
                     $('#oprtionProfessor'+id).html('<option>'+datawithjs.professor_name+'</option>');
   
   
                 
                 }
             }); 
   
   }
</script>
<script type="text/javascript">
   function s(val){
    
    
     window.location.href=base_url+"timetable/index?id="+val;
     
     }
     
</script>
<script type="text/javascript">
   $('#department').change(function() {
   $('#timeTable').DataTable().state.clear();
     var departmentID = $(this).val();
       $.ajax({
         type: 'POST',
         url: "<?=base_url('timetable/index')?>",
         data: "departmentID=" + departmentID,
         dataType: "html",
         success: function(data) {
           location.reload();
         }
       });
     });
   
</script>
<script type="text/javascript">
   $('#course').change(function() {
   $('#timeTable').DataTable().state.clear();
     var classesID = $(this).val();
       $.ajax({
         type: 'POST',
         url: "<?=base_url('timetable/index')?>",
         data: "classesID=" + classesID,
         dataType: "html",
         success: function(data) {
           location.reload();
         }
       });
     });
   
</script>
<script type="text/javascript">
   $('#yearSemester').change(function() {
   $('#timeTable').DataTable().state.clear();
     var yearSemesterID = $(this).val();
       $.ajax({
           type: 'POST',
           url: "<?=base_url('timetable/index')?>",
           data: "yearSemesterID=" + yearSemesterID,
           dataType: "html",
           success: function(data) {
             location.reload();
           }
       });
     });
</script>
<script type="text/javascript">    
   function ResetDepartmentwithtimetable(){
     $('#timeTable').DataTable().state.clear();
     $.ajax({
         type: 'POST',
         url: "<?=base_url('timetable/ResetDepartmentwithtimetable')?>",
         data:{ResetDepartmentwithtimetable:'ResetDepartmentwithtimetable'},
         dataType: "html",
         success: function(data) {
             location.reload();
         }
     });
   }
   
   function ResetCourseswithtimetable(){
     $('#timeTable').DataTable().state.clear();
     $.ajax({
         type: 'POST',
         url: "<?=base_url('timetable/ResetCourseswithtimetable')?>",
         data:{ResetCourseswithtimetable:'ResetCourseswithtimetable'},
         dataType: "html",
         success: function(data) {
             location.reload();
         }
     });
   }
   
   function ResetSemesterYearwithtimetable(){
     $('#timeTable').DataTable().state.clear();
     $.ajax({
         type: 'POST',
         url: "<?=base_url('timetable/ResetSemesterYearwithtimetable')?>",
         data:{ResetSemesterYearwithtimetable:'ResetSemesterYearwithtimetable'},
         dataType: "html",
         success: function(data) {
             location.reload();
         }
     });
   }
</script>