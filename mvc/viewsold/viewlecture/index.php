<div class="">
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor"><i class="fa fa-pencil-square-o"></i> View Lecture </h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
				<li class="active">View Lecture</li>
			</ol>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="card">
				<div class="card-body">
					<div class="col-sm-12">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
							<?php 
								$usertype=$this->session->userdata("usertype");
								if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin")
								{
							?>
									<div class="pull-right">
										<div class="btn-group">
											<a href="<?php echo base_url('professor/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
										</div>
									</div>
							<?php 
								} 
							?>
						</div>
						<div class="clearfix"></div>
						<div class="table table-responsive">
							<div class="col-sm-12">
								<div id="hide-table">
									<table class="table table-striped table-bordered table-hover dataTable no-footer">
										<thead>
											<tr>
												<th class="col-sm-2"><?=$this->lang->line('slno')?></th>
												<th class="col-sm-2">Semester</th>
												<th class="col-sm-2">Course Name</th>
												<th class="col-sm-2">Subject Name</th>
												<th class="col-sm-2">Week Day</th>
												<th class="col-sm-2">Time</th>
												<?php 
													if($usertype=="Admin")
													{ 
												?>
														<th class="col-sm-2"><?=$this->lang->line('professor_status')?></th>
												<?php 
													} 
												?>
												<th class="col-sm-2">Action</th>
											</tr>
										</thead>
									<tbody>
										<?php 
											if(empty($professorLecture))
											{
												echo $this->session->flashdata('msg');
											}
											else
											{
												foreach ($professorLecture as $key=> $value) 
												{
										?>
													<tr>
														<td class="col-sm-2"><?php echo $key+1; ?></td>
														<td class="col-sm-2"><?php echo $value->semester; ?></td>
														<td class="col-sm-2"><?php echo $value->classes; ?></td>
														<td class="col-sm-2"><?php echo $value->subject; ?></td>
														<td class="col-sm-2"><?php echo $value->days; ?></td>
														<td class="col-sm-2"><?php echo $value->times; ?></td>
														<?php 
															if($usertype=="Admin") 
															{ 
														?>
																<td class="col-sm-2"><?=$this->lang->line('professor_status')?></td>
														<?php 
															} 
														?>
														<td class="col-sm-2">
															<a href="<?php echo base_url()?>viewlecture/attendance?type=wfilter&course=<?php echo $value->course_id; ?>&subcourse=&yearsOrSemester=<?php echo $value->semester; ?>&subject=<?php echo $value->subject_id; ?>&date=<?php echo date('d-m-Y') ?>" class="btn btn-success btn-xs mrg for_margR" data-placement="top" data-toggle="tooltip" title="View">Mark Attendance</a>
														</td>
													</tr>
										<?php 
												}
											} 
										?>
									</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var status = '';
	var id = 0;
	$('.onoffswitch-small-checkbox').click(function() {
		if($(this).prop('checked')) 
		{
			status = 'chacked';
			id = $(this).parent().attr("id");
		} 
		else 
		{
			status = 'unchacked';
			id = $(this).parent().attr("id");
		}
		if((status != '' || status != null) && (id !='')) 
		{
			$.ajax({
				type: 'POST',
				url: "<?=base_url('professor/active')?>",
				data: "id=" + id + "&status=" + status,
				dataType: "html",
				success: function(data) {
					if(data == 'Success') 
					{
						toastr["success"]("Success")
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"newestOnTop": false,
							"progressBar": false,
							"positionClass": "toast-top-right",
							"preventDuplicates": false,
							"onclick": null,
							"showDuration": "500",
							"hideDuration": "500",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					} 
					else 
					{
						toastr["error"]("Error")
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"newestOnTop": false,
							"progressBar": false,
							"positionClass": "toast-top-right",
							"preventDuplicates": false,
							"onclick": null,
							"showDuration": "500",
							"hideDuration": "500",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}
				}
			});
		}
	}); 
</script>