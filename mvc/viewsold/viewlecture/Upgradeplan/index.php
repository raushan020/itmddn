<?php $adminpackage=$this->session->userdata('adminpackage');

if($adminpackage == 1) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     You are using free plan upgrade now and unlock all the features of E-Campus<a href="<?php echo base_url('upgradeplan') ?>" class="alert-link"> upgrade now</a>
   </div>
  
<?php }  ?>
<?php
$productinfo = "Pay E-Campus";
$txnid = time();
$surl = $surl;
$furl = $furl;        
$key_id = RAZOR_KEY_ID;
$currency_code = $currency_code;            
$total = 100.00; 
$amount = 100.00;
$merchant_order_id = rand(10,1000) ;
$card_holder_name = 'E-Campus';
$email = 'info@edgetechnosoft.com';
$phone = '7011019114';
$name = "E-Campus";
$return_url = base_url().'razorpay/callback';
?>
<div class="row">
    <div class="col-lg-12">
        <?php if(!empty($this->session->flashdata('msg'))){ ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg'); ?>
            </div>        
        <?php } ?>
        <?php if(validation_errors()) { ?>
          <div class="alert alert-danger">
            <?php echo validation_errors(); ?>
          </div>
        <?php } ?>
    </div>
</div>
<form name="razorpay-form" id="razorpay-form" action="<?php echo $return_url; ?>" method="POST">
  <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
  <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
  <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
  <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="<?php echo $productinfo; ?>"/>
  <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
  <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
  <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/>
  <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/>
  <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/>
</form>
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-cog" aria-hidden="true"></i> Upgrade Plan </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"> Upgrade Plan </li>
         </ol>
      </div>
   </div>
   <div class="container-fluid">
    <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
          <div class="row">
            <div class="col-md-12 col-sm-12">
            
              <div class="widget pricing-widget default-widget">
                
                <!-- Single Pricing -->
                <div class="col-md-4 col-sm-4">
                  <div class="blog-box">
                    <div class="style-two pricing-table">
                      <div class="pricing-table-header">
                        <span class="price-value"><span class="currency"><i class="fa fa-inr"></i></span>0<span class="mo">per month</span></span>
                        <div class="heading">
                          <h3>Free</h3>
                        </div>
                      </div>
                      <div class="pricingContent">
                        <ul>
                          <li><i class="fa fa-check"aria-hidden="true"></i> Learning Management System</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> Student Panel(Up to 50)</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> limited Admin & User</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> E-mail Support</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> Phone Support</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> payment gateway integration</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> API integration</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> Dedicated Account manager</li>
                        </ul>
                      </div>
                      <div class="pricing-table-sign-up">
                        <a href="#" class="btn btn-block btn-default" disabled="">sign up</a>
                      </div>
                    </div>
                  </div>
                </div>
                
                <!-- Single Pricing -->
                <div class="col-md-4 col-sm-4">
                  <div class="blog-box">
                    <div class="style-two pricing-table active-pr">
                      <div class="pricing-table-header">
                        <span class="price-value"><span class="currency"><i class="fa fa-inr"></i></span>75<span class="mo">per month</span></span>
                        <div class="heading">
                          <h3>Essential</h3>
                        </div>
                      </div>
                      <div class="pricingContent">
                        <ul>
                         <li><i class="fa fa-check"aria-hidden="true"></i> Learning Management System</li>
                                    <li><i class="fa fa-check "aria-hidden="true"></i> Student Panel(Unlimited)</li>
                                    <li><i class="fa fa-check "aria-hidden="true"></i> Unlimited Admin & User</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> E-mail Support</li>
                                    <li><i class="fa fa-close "aria-hidden="true"></i> Phone Support</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> payment gateway integration</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> API integration</li>
                                    <li><i class="fa fa-close"aria-hidden="true"></i> Dedicated Account manager</li>
                        </ul>
                      </div>
                      <div class="pricing-table-sign-up">
                        <a href="#" class="btn btn-block btn-default" data-toggle="modal" data-target="#modal-2">sign up</a>
                      </div>
                    </div>
                  </div>
                </div>
                
                <!-- Single Pricing -->
                <div class="col-md-4 col-sm-4">
                  <div class="blog-box">
                    <div class="style-two pricing-table">
                      <div class="pricing-table-header">
                        <span class="price-value"><span class="currency"><i class="fa fa-inr"></i></span>100<span class="mo">per month</span></span>
                        <div class="heading">
                          <h3>Pro</h3>
                        </div>
                      </div>
                      <div class="pricingContent">
                        <ul>
                          <li><i class="fa fa-check"aria-hidden="true"></i> Learning Management System</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> Student Panel(Unlimited)</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> Unlimited Admin & User</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> E-mail Support</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> Phone Support</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> payment gateway integration</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> API integration</li>
                                    <li><i class="fa fa-check"aria-hidden="true"></i> Dedicated Account manager</li>
                        </ul>
                      </div>
                      <div class="pricing-table-sign-up">
                        <a href="#" class="btn btn-block btn-default" data-toggle="modal" data-target="#modal-3">sign up</a>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>  
</div>
 <!-- Modal Start-->
                     <div class="modal modal-box-2 fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog">
                         <div class="modal-content" id="myModalLabel">
                           <div class="modal-header theme-bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 style="color: white">Upgrade to Essensial</h3>
                            <center><h5 style="color: white">Upgrade for Unlimited reports, increased user and access to advanced features</h5></center>
                           </div>
                          <div class="modal-body">
                            <!-- <form name="sentMessage" class="contactForm"> -->
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="col-md-4" style="border-right: 2px solid silver">
                                    <h4>Basic</h4>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span> <br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <br>
                                    <strong><span>Free</span></strong>
                                  </div>
                                  <div class="col-md-8">
                                   <h4>Essensial</h4>
                                   <span>All on Basic, plus</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br> 
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <br>
                                   <input  id="submit-pay" type="submit" class="btn btn-success" onclick="razorpaySubmit(this);" value="Upgrade To Essensial For $1.00 Per User / Per Month"> 
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            <!-- </form> -->
                          </div>
                        </div>
                      </div>
                    </div>
             <!-- Modal End-->
             <!-- Modal Start-->
                     <div class="modal modal-box-2 fade" id="modal-3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog">
                         <div class="modal-content" id="myModalLabel">
                           <div class="modal-header theme-bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 style="color: white">Upgrade to Advanced</h3>
                            <center><h5 style="color: white">Upgrade for Unlimited reports, increased user and access to advanced features</h5></center>
                           </div>
                          <div class="modal-body">
                            <!-- <form name="sentMessage" class="contactForm"> -->
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="col-md-4" style="border-right: 2px solid silver">
                                    <h4>Basic</h4>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span> <br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <span>&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to 50 Users</span><br>
                                    <br>
                                    <strong><span>Free</span></strong>
                                  </div>
                                  <div class="col-md-8">
                                   <h4>Advanced</h4>
                                   <span>All on Basic + Essensial, plus</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br> 
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <span style="color: #00cc00">&#10003;</span>&nbsp;<span style="font-weight: bold;">Up to Unlimited Users</span><br>
                                   <br>
                                   <button class="btn btn-success">Upgrade To Advanced For $1.20 Per User / Per Month </button>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            <!-- </form> -->
                          </div>
                        </div>
                      </div>
                    </div>
             <!-- Modal End-->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var razorpay_options = {
    key: "<?php echo $key_id; ?>",
    amount: "<?php echo $total; ?>",
    name: "<?php echo $name; ?>",
    description: "Order # <?php echo $merchant_order_id; ?>",
    netbanking: true,
    currency: "<?php echo $currency_code; ?>",
    prefill: {
      name:"<?php echo $card_holder_name; ?>",
      email: "<?php echo $email; ?>",
      contact: "<?php echo $phone; ?>"
    },
    notes: {
      soolegal_order_id: "<?php echo $merchant_order_id; ?>",
    },
    handler: function (transaction) {
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('razorpay-form').submit();
    },
    "modal": {
        "ondismiss": function(){
            location.reload()
        }
    }
  };

  var razorpay_submit_btn, razorpay_instance;

  function razorpaySubmit(el){
    // alert("nc");
    $('#modal-2').hide();
    if(typeof Razorpay == 'undefined'){
      setTimeout(razorpaySubmit, 200);
      if(!razorpay_submit_btn && el){
        razorpay_submit_btn = el;
        el.disabled = true;
        el.value = 'Please wait...';  
      }
    } else {
      if(!razorpay_instance){
        razorpay_instance = new Razorpay(razorpay_options);
        if(razorpay_submit_btn){
          razorpay_submit_btn.disabled = false;
          razorpay_submit_btn.value = "Pay Now";
        }
      }
      razorpay_instance.open();
    }
  }  
  
</script>

