<div class="">


    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="ti ti-id-badge"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

           <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("teacher/index")?>"><?=$this->lang->line('menu_teacher')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_teacher')?></li>
            </ol>
        </div>
    </div>
    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
       <div class="card">
            <div class="card-body">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <p>Field are required with <span class="red-color">*</span></p>
                    <?php 

                        if(form_error('name')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="name_id" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_name")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name_id" onchange="AddUsernamePass()" name="name" value="<?=set_value('name')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>

                    <?php 

                        if(form_error('sex')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_sex")?>

                        </label>

                        <div class="col-sm-6">

                            <?php 

                                echo form_dropdown("sex", array($this->lang->line('teacher_sex_male') => $this->lang->line('teacher_sex_male'), $this->lang->line('teacher_sex_female') => $this->lang->line('teacher_sex_female')), set_value("sex"), "id='sex' class='form-control'"); 

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('sex'); ?>

                        </span>

                    </div>

                    <?php 

                        if(form_error('email')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_email")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('phone')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_phone")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>
                  
                    <?php 

                        if(form_error('username')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="username" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_username")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username')?>" >

                        </div>
                         <span class="col-sm-4 displayNone">
                        <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                        </span>


                         <span class="col-sm-4 control-label">
                          
                            <?php echo form_error('username'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('password')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="password" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_password")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="password" name="password" value="<?=set_value('password')?>" >

                        </div>

                        <span class="col-sm-4 displayNone">
                        <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                        </span>

                         <span class="col-sm-4 control-label">

                            <?php echo form_error('password'); ?>

                        </span>

                    </div>



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("add_teacher")?>" >

                        </div>

                    </div>



                </form>



            </div><!-- /col-sm-8 -->

        </div>

    </div>

</div>
</div>
</div>

<style type="text/css">
    .displayNone{
        display:none;
    }
</style>
<script type="text/javascript">
    function AddUsernamePass(){
    $('.displayNone').show();
    var name_id = $('#name_id').val();
    $.ajax({
    type:'post',
    url:"<?php echo base_url() ?>teacher/AddUsernamePass",
    data:{name_id:name_id},
    success:function(response) {
        var objective = jQuery.parseJSON(response);
        $('#username').val(objective.username);
        $('#password').val(objective.password);
        $('.displayNone').hide();
    
    }

});
    }
</script>



<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });

$('#jod').datepicker();

</script>

