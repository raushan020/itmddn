<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Exam_m extends MY_Model {



	protected $_table_name = 'exam';



	protected $_primary_key = 'examID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "exam asc";



	function __construct() {



		parent::__construct();



	}



	function get_exam_count($studentID) {

		 $this->db->where('studentID',$studentID);

		  $query = $this->db->get('exam');

		return $query->num_rows();



	}



 	function get_exam($studentID) {

		 $this->db->where('studentID',$studentID);

		 if ($this->session->userdata('FilterMarkyearSemesterID')) {

		 $this->db->where('yearsOrSemester', $this->session->userdata('FilterMarkyearSemesterID'));

		}

		$query = $this->db->get('exam');

		return $query->row();

		// print_r($query->row());

		// exit();

	}



 	function get_exam_Single() {

		 $this->db->where('classesID',$this->session->userdata('FilterExamclassesID'));

		 if ($this->session->userdata('FilterExamsubCourseID')) {

		 $this->db->where('sub_coursesID', $this->session->userdata('FilterExamsubCourseID'));

		  }else{

		  	$this->db->where('sub_coursesID', 0);

		  }



		 if ($this->session->userdata('FilterExamyearSemesterID')) {

		 $this->db->where('yearsOrSemester', $this->session->userdata('FilterExamyearSemesterID'));

		  	}



		  	$query = $this->db->get('exam',1);



		return $query->row();

		// print_r($query->row());

		// exit();



	}



	function get_exam_innerJoin() {

	

		$this->db->select('*');



		$this->db->from('exam');



		$this->db->join('student', 'exam.studentID = student.studentID', 'LEFT');



	    $this->db->where('exam.classesID',$this->session->userdata('FilterExamclassesID'));

		if ($this->session->userdata('FilterExamsubCourseID')) {

		$this->db->where('exam.sub_coursesID', $this->session->userdata('FilterExamsubCourseID'));

		  }



		if ($this->session->userdata('FilterExamyearSemesterID')) {

		$this->db->where('exam.yearsOrSemester', $this->session->userdata('FilterExamyearSemesterID'));

		  }



		$query = $this->db->get();

		

		return $query->result();



	}



	function get_Subjects($examID){



		$this->db->where('examID',$examID);

		$examId  = $this->db->get('mark');



		return $examId->result();



	}



	function get_order_by_exam($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}



	function insert_exam($array) {



		$error = parent::insert($array);



		return TRUE;

	}



	function absent_subject() {

	

		$this->db->select('*');

		$this->db->from('mark');

		$this->db->join('exam', 'exam.examID = mark.examID', 'LEFT');

		$this->db->join('student', 'exam.studentID = student.studentID', 'LEFT');

		$this->db->where("mark", 0);



		$query = $this->db->get();

		

		return $query->result();

	}



	function update_exam($id) {

	

	 $userdata=array(

	 'sub_coursesID'=>$this->input->post('sub_coursesID'),

	 'classesID'=>$this->input->post('classesID'),

	 'yearsOrSemester'=>$this->input->post('yearsOrSemester'),	

	 'subjectID'=>$this->input->post('subjectID'),	

	 'quiz_name'=>$this->input->post('quiz_name'),

	 'description'=>$this->input->post('description'),

	 'start_date'=>strtotime($this->input->post('start_date')),

	 'end_date'=>strtotime($this->input->post('end_date')),

	 'duration'=>$this->input->post('duration'),

	 'correct_score'=>$this->input->post('correct_score'),

	 'incorrect_score'=>$this->input->post('incorrect_score'),

	 'extype'=>$this->input->post('extype')

	 );

	 $this->db->where('quid',$id);

	  $this->db->update('ets_quiz',$userdata);

	 $quid=$this->db->insert_id();

	return $quid;



	}



	public function delete_exam($id){



		parent::delete($id);



	}



	function quiz_details($id,$adminID){





	$this->db->where('adminID',$adminID);

	$this->db->where('quid',$id);

	$query = $this->db->get('ets_quiz');



	return $query->row();



}



	function get_join_exam_count($adminID) {

		if(isset($_POST["search"]["value"]))  

		           {  

		 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 



		$this->db->select('ets_quiz.*,classes.*,sub_courses.*,subject.*,ets_quiz.status as studentStatus');

		$this->db->from('ets_quiz');

		$this->db->join('classes', 'classes.ClassesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

      	$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');	



		if ($this->session->userdata('FilterExamclassesID')) {

		 $this->db->where('ets_quiz.classesID', $this->session->userdata('FilterExamclassesID'));

		}



		if ($this->session->userdata('FilterExamsubCourseID')) {

		 $this->db->where('ets_quiz.sub_coursesID', $this->session->userdata('FilterExamsubCourseID'));

		}



		if ($this->session->userdata('FilterExamyearSemesterID')) {

		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterExamyearSemesterID'));

		}





	   if ($this->session->userdata('ActiveExams')) {

		 $this->db->where('ets_quiz.status', $this->session->userdata('ActiveExams'));

		}



	   if ($this->session->userdata('DraftExams')) {

		 $this->db->where('ets_quiz.status', 0);

		}



	   if ($this->session->userdata('TrashExams')) {

		 $this->db->where('ets_quiz.status', $this->session->userdata('TrashExams'));

		}



	 if (empty($this->session->userdata('DraftExams')) && empty($this->session->userdata('TrashExams'))) {

           $this->db->where('ets_quiz.status',1);

        }



		$this->db->where('classes.status',1);

		$this->db->where('ets_quiz.adminID', $adminID);

		$this->db->where('sub_courses.status', 1);



		$query = $this->db->get();



		return $query->num_rows();



	}



function get_quizSettting(){



   return  $this->db->get('ets_quiz_setting',1)->row();



}

// function dau_quiz(){



// 	   return  $this->db->get('ets_quiz_',1)->row();



// }



	function get_join_exam() {



		$adminID = $this->session->userdata("adminID");

		$this->db->select('ets_quiz.*,classes.*,sub_courses.*,subject.*,ets_quiz.status as studentStatus');

		$this->db->from('ets_quiz');

		$this->db->join('classes', 'classes.ClassesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

      	$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');	



		if ($this->session->userdata('FilterExamclassesID')) {

		 $this->db->where('ets_quiz.classesID', $this->session->userdata('FilterExamclassesID'));

		}



		if ($this->session->userdata('FilterExamsubCourseID')) {

		 $this->db->where('ets_quiz.sub_coursesID', $this->session->userdata('FilterExamsubCourseID'));

		}



		if ($this->session->userdata('FilterExamyearSemesterID')) {

		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterExamyearSemesterID'));

		}





	   if ($this->session->userdata('ActiveExams')) {

		 $this->db->where('ets_quiz.status', $this->session->userdata('ActiveExams'));

		}



	   if ($this->session->userdata('DraftExams')) {

		 $this->db->where('ets_quiz.status', 0);

		}



	   if ($this->session->userdata('TrashExams')) {

		 $this->db->where('ets_quiz.status', $this->session->userdata('TrashExams'));

		}

	  if (empty($this->session->userdata('DraftExams')) && empty($this->session->userdata('TrashExams'))) {

           $this->db->where('ets_quiz.status',1);

        }

		$this->db->where('classes.status',1);

		$this->db->where('ets_quiz.adminID', $adminID);

		$this->db->where('sub_courses.status', 1);



if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'quiz_name',

                            2 =>'subject_code',

                            3=> 'start_date',

                            4=> 'end_date',

                            5=> 'subject',

                            6=> 'classes',

                            7=> 'sub_course',

                            8=> 'yearsOrSemester',

                            9=>'action'

                        );

           $where = "(ets_quiz.quiz_name LIKE '%$searches%' OR subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('ets_quiz.quid', 'DESC');  

           } 

           if (isset($_POST["length"])) {

  

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       }



	}



function make_datatables($adminID){  

           $this->get_join_exam();  

           $query = $this->db->get();  

           return $query->result();  

      } 



function get_join_exam_list_count($adminID) {



	    $adminID = $this->session->userdata("adminID");

        $loginuserID = $this->session->userdata("loginuserID");

        							$this->db->where('studentID',$loginuserID);

        							$this->db->where('adminID',$adminID);

        		$query	 = $this->db->get('student');

        $StudentArray =  $query->row();



		$this->db->select('*');



		$this->db->from('ets_quiz');

		$uri= htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		$this->db->where('ets_quiz.classesID', $StudentArray->classesID);

		$this->db->where('ets_quiz.sub_coursesID', $StudentArray->sub_coursesID);

		$this->db->where('ets_quiz.extype',$uri);

if ($StudentArray->examType=='y') {

	

  $this->db->where('ets_quiz.yearsOrSemester', $StudentArray->yearsOrSemester);

}else{

	 if ($this->session->userdata('FilterExamListyearSemesterID')) {

		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterExamListyearSemesterID'));

		}

}



		$this->db->where('ets_quiz.adminID', $adminID);



		$query = $this->db->get();



		return $query->num_rows();



	}



function make_datatables_exam_list($adminID ,$uri){  

           $this->get_join_exam_list($uri);  

           $query = $this->db->get(); 

           return $query->result();  

      } 



	function get_join_exam_list($uri) {



	    $adminID = $this->session->userdata("adminID");

        $loginuserID = $this->session->userdata("loginuserID");

        							$this->db->where('studentID',$loginuserID);

        							$this->db->where('adminID',$adminID);

        		$query	 = $this->db->get('student');

        $StudentArray =  $query->row();



		$this->db->select('*');

		$this->db->from('ets_quiz');

		$this->db->join('classes', 'classes.ClassesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

      	$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');	

		$this->db->where('ets_quiz.classesID', $StudentArray->classesID);

		$this->db->where('ets_quiz.sub_coursesID', $StudentArray->sub_coursesID);



if ($StudentArray->examType=='y') {

	

  $this->db->where('ets_quiz.yearsOrSemester', $StudentArray->yearsOrSemester);

}else{

	 if ($this->session->userdata('FilterExamListyearSemesterID')) {

		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterExamListyearSemesterID'));

		}

}



		$this->db->where('ets_quiz.extype', $uri);

		 $this->db->where('ets_quiz.adminID', $adminID);



		if(isset($_POST["search"]["value"]))  

		           {  

		 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('ets_quiz.quid', 'DESC');  

           } 

           if (isset($_POST["length"])) {

  

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       }



	}



 function insert_quiz(){



// $start_time = $this->input->post('start_date').':00';

// $end_time = $this->input->post('end_date').':00';

	 $userdata=array(

	 'adminID'=>$this->session->userdata("adminID"),

	 'sub_coursesID'=>$this->input->post('sub_coursesID'),

	 'classesID'=>$this->input->post('classesID'),

	 'yearsOrSemester'=>$this->input->post('yearsOrSemester'),	

	 'subjectID'=>$this->input->post('subjectID'),	

	 'quiz_name'=>$this->input->post('quiz_name'),

	 'description'=>$this->input->post('description'),

	 'start_date'=>strtotime($this->input->post('start_date')),

	 'end_date'=>strtotime($this->input->post('end_date')),

	 'duration'=>$this->input->post('duration'),

	 // 'pass_percentage'=>$this->input->post('pass_percentage'),

	 'correct_score'=>$this->input->post('correct_score'),

	 'incorrect_score'=>$this->input->post('incorrect_score'),

	 'extype'=>$this->input->post('extype'),
	 'video_record'=>$this->input->post('video'),

	 // 'ip_address'=>$this->input->post('ip_address'),

	 // 'view_answer'=>$this->input->post('view_answer'),

	 // 'camera_req'=>$this->input->post('camera_req'),

	 // 'quiz_template'=>$this->input->post('quiz_template'),

	 // 'with_login'=>$this->input->post('with_login'),

	 // 'question_selection'=>$this->input->post('question_selection')



	 );

	  $this->db->insert('ets_quiz',$userdata);

	 $quid=$this->db->insert_id();

	return $quid;

	 

 }

 function getExamByID($id){

$this->db->where('quid',$id);

$query  = $this->db->get('ets_quiz');

 return $query->row();

 }

 function get_Question_count($qids){



 	                 $intt  = explode(',', $qids);

           

                  $this->db->where_in('qid',$intt);

               $quetion  =  $this->db->get('ets_qbank');

		      

		      return $quetion->num_rows();

 }



 function getQuestion($qids){

 	                 $intt  = explode(',', $qids);

                  $this->db->where_in('qid',$intt);



if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'question',

                            2=>'action'

                        );

           $where = "(question LIKE '%$searches%')";

           $this->db->where($where);

           } 

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('qid', 'DESC');  

           } 

           if (isset($_POST["length"])) {

  

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       }

               $quetion  =  $this->db->get('ets_qbank');

		      

		       return $quetion->result();

 }



function question_add($quid)

{



$quid = $this->uri->segment(3);

 $this->db->where('quid',$quid);

 $query  = $this->db->get('ets_quiz');



$quizData   =  $query->row();



if($this->input->post('question_type')==1)

	{

		$userdata=array(

	 	'question'=>$this->input->post('question'),

	 	'description'=>$this->input->post('description'),

	 	'question_type'=>'Multiple Choice Single Answer',	 

	);

	 $this->db->insert('ets_qbank',$userdata);

	 $qid=$this->db->insert_id();

	 foreach($this->input->post('option') as $key => $val){

		 if($this->input->post('score')==$key){

			$score=1;

		 }else{

			 $score=0;

		 }

	$userdata=array(

	 'q_option'=>$val,

	 'qid'=>$qid,

	 'score'=>$score,

	 );

	 $this->db->insert('ets_options',$userdata);	 

		 

	 }

	 

	}

	else if($this->input->post('question_type')==2)

	{

		$userdata=array(

	 'question'=>$this->input->post('question'),

	 'description'=>$this->input->post('description'),

	 'question_type'=>'Multiple Choice Multiple Answer',

	 );

	 $this->db->insert('ets_qbank',$userdata);

	 $qid=$this->db->insert_id();

	 foreach($this->input->post('option') as $key => $val){

		 if(in_array($key,$this->input->post('score'))){

			 $score=(1/count($this->input->post('score')));

		 }else{

			 $score=0;

		 }

	$userdata=array(

	 'q_option'=>$val,

	 'qid'=>$qid,

	 'score'=>$score,

	 );

	 $this->db->insert('ets_options',$userdata);	 

		 

	 }



	}

	else if($this->input->post('question_type')==3)

	{

		$userdata=array(

		 	'question'=>$this->input->post('question'),

		 	'description'=>$this->input->post('description'),

		 	'question_type'=>'Match the Column',	 

		);

	 	$this->db->insert('ets_qbank',$userdata);

	 	$qid=$this->db->insert_id();

		foreach($this->input->post('option') as $key => $val){

	  	$score=(1/count($this->input->post('option')));

		$userdata=array(

		 	'q_option'=>$val,

		 	'q_option_match'=>$_POST['option2'][$key],

		 	'qid'=>$qid,

		 	'score'=>$score,

		);

	 	$this->db->insert('ets_options',$userdata);	 

		}

	}



	else if($this->input->post('question_type')==4)

	{

		$userdata=array(

		 	'question'=>$this->input->post('question'),

		 	'description'=>$this->input->post('description'),

		 	'question_type'=>'Short Answer',	 

		);

	 	$this->db->insert('ets_qbank',$userdata);

	 	$qid=$this->db->insert_id();

	 	foreach($this->input->post('option') as $key => $val){

	  		$score=1;

			$userdata=array(

		 		'q_option'=>$val,

		 		'qid'=>$qid,

			 	'score'=>$score,

			);

	 		$this->db->insert('ets_options',$userdata);	 

		 

	 	}

	}



	else {

		$userdata=array(

	 		'question' => $this->input->post('question'),

	 		'description' => $this->input->post('description'),

	 		'question_type' => 'Long Answer',	 

	 	);

 		$this->db->insert('ets_qbank',$userdata);

	 	$qid=$this->db->insert_id();

	}



	$qids = $quizData->qids;

	// print_r($qids);

	// exit();
if ($qids=="") {
	$updatedQid   =  $qid;
}else{
  	$updatedQid   =  $qids.','.$qid;

}
   $countginegaquestion  = explode(',', $updatedQid);

	$arrayName = array(

		'qids' =>$updatedQid, 
		'noq' => count($countginegaquestion),
	);



	$this->db->where('quid',$quid);

	$this->db->update('ets_quiz',$arrayName);



}



function update_question($quid)

{

	

	$qid = $this->uri->segment(4);

	// print_r($quid)."<br>";

	// print_r($qid);

	// exit();

	// print_r($this->input->post('question_type'));

	// exit();

	if($this->input->post('question_type')==1)

	{



	$userdata=array(

	'question'=>$this->input->post('question'),

	'description'=>$this->input->post('description'),

	'question_type'=>'Multiple Choice Single Answer', 

	);



	$this->db->where('qid',$qid);

	$this->db->update('ets_qbank',$userdata);

	

	$this->db->where('qid',$qid);

	$this->db->delete('ets_options');

	foreach($this->input->post('option') as $key => $val){

		if($this->input->post('score')==$key){

			$score=1;

		}else{

			$score=0;

		}



	

	$userdata=array(

	 'q_option'=>$val,

	 'qid'=>$qid,

	 'score'=>$score,

	 );



	 $this->db->insert('ets_options',$userdata);	 

		 

	 }

	 

	}

	else if($this->input->post('question_type')==2)

	{

		$userdata=array(

		 'question'=>$this->input->post('question'),

		 'description'=>$this->input->post('description'),

		 'question_type'=>'Multiple Choice Multiple Answer',

		 'cid'=>$this->input->post('cid'),

		 'lid'=>$this->input->post('lid')	 

		 );

		 $this->db->where('qid',$qid);

		 $this->db->update('ets_qbank',$userdata);

		 $this->db->where('qid',$qid);

		$this->db->delete('ets_options');

		 foreach($this->input->post('option') as $key => $val){

		 	// print_r($this->input->post('option'));

		 	// exit();

				if($_POST['option'][$key]!=''){

				 	$score=(1/count($this->input->post('score')));

			 	}else{

				 	$score=0;

			 	}

			$userdata=array(

			 'q_option'=>$val,

			 'qid'=>$qid,

			 'score'=>$score,

			 );

			 $this->db->insert('ets_options',$userdata);				 

		}

	}

	else if($this->input->post('question_type')==3)

	{

	 $userdata=array(

	 'question'=>$this->input->post('question'),

	 'description'=>$this->input->post('description'),

	 'question_type'=>'Match the Column',

	 );

	 $this->db->where('qid',$qid);

	 $this->db->update('ets_qbank',$userdata);

	 $this->db->where('qid',$qid);

	$this->db->delete('ets_options');

	foreach($this->input->post('option') as $key => $val){

	  $score=(1/count($this->input->post('option')));

	$userdata=array(

	 'q_option'=>$val,

	 'q_option_match'=>$_POST['option2'][$key],

	 'qid'=>$qid,

	 'score'=>$score,

	 );

	 $this->db->insert('ets_options',$userdata);	 

		 

	 }

	}

	else if($this->input->post('question_type')==4)

	{

	 $userdata=array(

	 'question'=>$this->input->post('question'),

	 'description'=>$this->input->post('description'),

	 'question_type'=>'Short Answer', 

	 );

	 $this->db->where('qid',$qid);

	 $this->db->update('ets_qbank',$userdata);

	 $this->db->where('qid',$qid);

	 $this->db->delete('ets_options');

 foreach($this->input->post('option') as $key => $val){

	  $score=1;

	$userdata=array(

	 'q_option'=>$val,

	 'qid'=>$qid,

	 'score'=>$score,

	 );

	 $this->db->insert('ets_options',$userdata);	 

		 

	 }

	}

	else {

	 $userdata=array(

	 'question'=>$this->input->post('question'),

	 'description'=>$this->input->post('description'),

	 'question_type'=>'Long Answer', 

	 );

	 $this->db->where('qid',$qid);

	 $this->db->update('ets_qbank',$userdata);

	 $this->db->where('qid',$qid);

	 $this->db->delete('ets_options');

	}



	return true;



}



	function get_question($qid){

		$this->db->where('qid',$qid);

		$query=$this->db->get('ets_qbank');

		return $query->row(); 

	}



	function get_option($qid){

		$this->db->where('qid',$qid);

		$query=$this->db->get('ets_options');

		return $query->result(); 

	} 



	function Getsubjects($adminID,$classesID,$sub_coursesID,$semester_idSelect){



		$this->db->where('adminID',$adminID);

		$this->db->where('classesID',$classesID);

		$this->db->where('sub_coursesID',$sub_coursesID);

		$this->db->where('yearsOrSemester',$semester_idSelect);

		$query = $this->db->get('subject');

		return $query->result();



	}



	function all_count(){

		$this->db->where('status',1);

		$this->db->or_where('status',0);

		$query	= $this->db->get('ets_quiz');

		return $query->num_rows();

	}



	function ActiveExams_count(){

		$this->db->from('ets_quiz');

		$this->db->join('classes', 'classes.ClassesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

      	$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');

      	$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status', 1);

		

		$this->db->where('ets_quiz.status',1);

		$query = $this->db->get();



		return $query->num_rows();

		

	}



	function DraftExams_count(){

		$this->db->from('ets_quiz');

		$this->db->join('classes', 'classes.ClassesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

      	$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');

      	$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status', 1);

		$this->db->where('ets_quiz.status',0);

		$query = $this->db->get();



		return $query->num_rows();



	}



	function TrashExams_count(){

		$this->db->from('ets_quiz');

		$this->db->join('classes', 'classes.ClassesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

      	$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');

      	$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status', 1);

		$this->db->where('ets_quiz.status',2);



		$query = $this->db->get();



		return $query->num_rows();





	}



}





/* End of file exam_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/exam_m.php */