<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professor_m extends MY_Model {

	protected $_table_name = 'professor';

	protected $_primary_key = 'professorID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "name asc";

	function __construct() {

		parent::__construct();

	}

	function get_username($table, $data=NULL) {

		$query = $this->db->get_where($table, $data);

		return $query->result();

	}

	function get_professor($array=NULL, $signal=FALSE) {

		$query = parent::get($array, $signal);
		return $query;
	}
	
 function get_professor_all($adminID){
	$this->db->where('status',1);
	$this->db->where('adminID',$adminID);
	  return $this->db->get('professor')->result();
 }


	function get_professor_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");
		$this->db->join('designation', 'professor.designationID = designation.designationID', 'left');
		$this->db->join('department', 'professor.departmentID = department.departmentID', 'left');
	    $this->db->where('adminID',$adminID);
	    $this->db->where('status',1);
		if(isset($_POST["search"]["value"]))  
           {  

			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 => 'sn', 
                            1 => 'name',
                            2 => 'username',
                            3 => 'designation_name',
                            4 => 'department_name',
                            5 => 'email',
                            6 => 'action'
                        );

           $where = "(professor.name LIKE '%$searches%' OR professor.username LIKE '%$searches%' OR professor.email LIKE '%$searches%')";
           $this->db->where($where);
        
           
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('professor.professorID', 'DESC');  
           } 
          
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       } 

	}
	
	   function make_datatables(){  
	   	
        $this->get_professor_ClgAdmin();  
   	    $query = $this->db->get('professor'); 
		return $query->result();  
      } 
	
	function get_professor_no($adminID)
	{
		// raushan
		 if(isset($_POST["search"]["value"]))  
            {  

		 	$searches  =  $_POST["search"]["value"];

		 	$order_column = array( 

                            0 => 'sn', 
                            1 => 'name',
                            2 => 'username',
                            3 => 'designation_name',
                            4 => 'department_name',
                            5 => 'email',
                            6 => 'action'
                         );

            $where = "(professor.name LIKE '%$searches%' OR professor.username LIKE '%$searches%' OR professor.email LIKE '%$searches%')";
          $this->db->where($where);
       }
		 	// raushan end

		$this->db->where('adminID',$adminID);
		   $this->db->where('status',1);
	    $query = $this->db->get('professor'); 
		return $query->num_rows();
	}

	function get_professor_ClgAdminID($adminID) {
	$this->db->where('adminID',$adminID);
	   $this->db->where('status',1);
	$query = $this->db->get('professor');
	 return $query->result();
	}

	function get_order_by_professor($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}

	function getstudentforcondition($getClasses,$getSubcourse,$getSemester,$getSubject) {
	$this->db->select('name,studentID');
	$this->db->where('classesID',$getClasses);
	$this->db->where('sub_coursesID',$getSubcourse);
	$this->db->where('yearsOrSemester',$getSemester);
   	$this->db->where('status',1);
	$query = $this->db->get('student');
	 return $query->result();
	}


	function getstudent_from_atnd($getSubject,$loginuserID,$date){

	$this->db->from('student');
	$this->db->join('attendance_student','student.studentID=attendance_student.studentID','INNER');
	$this->db->where('attendance_student.subjectID',$getSubject);
	$this->db->where('attendance_student.professorID',$loginuserID);
   $this->db->where('attendance_student.atd_date',$date);
   return $this->db->get()->result();


	}

	function get_single_professor($array) {

		$query = parent::get_single($array);

		return $query;

	}


	function get_professor_datatables() {
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->join('sub_courses', 'sub_courses.sub_coursesID=subject.sub_coursesID', 'left');
		$this->db->join('classes', 'classes.classesID = subject.classesID', 'left');
	    $this->db->where('subject.professorID',$loginuserID);
		$query = $this->db->get('subject')->result();

		return $query;
	}

	function get_attendance() {
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		
		$this->db->from('attendance_student');
		$this->db->join('student', 'attendance_student.studentID = student.studentID', 'INNER');
	    
		$query = $this->db->get()->result();
		return $query;
	}


	function insert_professor($array) {

		$error = parent::insert($array);
		return TRUE;

	}

	function update_professor($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}

	function delete_professor($id){

$data  = array(
	'status' =>2,
	 );
	   $this->db->where('professorID',$id);
	   $this->db->update('professor',$data);
	}

	function insert_attendance($array) {

		$this->db->insert('attendance_student',$array);
		return TRUE;

	}

	function hash($string) {

		return parent::hash($string);

	}

}

/* End of file professor_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/professor_m.php */