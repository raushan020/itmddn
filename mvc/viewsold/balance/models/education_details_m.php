<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Education_details_m extends MY_Model {



	protected $_table_name = 'education_details';

	protected $_primary_key = '	education_detailsID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "classes_numeric asc";



	function __construct() {

		parent::__construct();

	}


function get_eduction_details($adminID){

	$this->db->where('adminID',$adminID);
	$query  =  $this->db->get('education_details');
   return $query->result();
}

}



/* End of file classes_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/classes_m.php */