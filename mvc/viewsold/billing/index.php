<?php $adminpackage=$this->session->userdata('adminpackage');

if($adminpackage == 1) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     You are using free plan upgrade now and unlock all the features of E-Campus<a href="<?php echo base_url('upgradeplan') ?>" class="alert-link"> upgrade now</a>
   </div>
  
<?php }  ?>
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-cog" aria-hidden="true"></i> Manage Bill</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"> Billing </li>
         </ol>
      </div>
   </div>
 <div class="container-fluid">
				<div class="row">
						<div class="col-md-10 col-sm-6 ">
							<div class="widget list-widget">
								<div class="card-header">
									<h4 class="card-title m-b-0">Invoice details</h4>
								</div>
									<div class="row" style="margin: 0px 180px 0px 160px;">
									<div class="todo-list todo-list-hover todo-list-divided">
										<div class="todo todo-default">
										  <span class="ct-title">Payment Paln</span>
										  <span class="badge badge-contributer"><?php $package=$plans->adminpackage; if($package == 1){ echo "Basic"; } if($package == 2){ echo "Essential";} if($package == 3){ echo "Pro";} ?></span>
										</div>
										<div class="todo todo-default">
										  <span class="ct-title">Total Users</span>
										  <span class="badge badge-contributer"><?php echo $plans->totalstudent; ?></span>
										</div>
										<div class="todo todo-default">
										  <span class="ct-title">Currency</span>
										  <span class="badge badge-contributer bage-primary"><?php echo $plans->currency_code; ?></span>
										</div>
										<div class="todo todo-default">
										  <span class="ct-title">Contract expires</span>
										  <span class="badge badge-contributer bage-primary">No contract</span>
										</div>
										<div class="todo todo-default">
										  <span class="ct-title">Time zone</span>
										  <span class="badge badge-contributer bage-primary">GMT- kolkata (India)</span>
										</div>
										<div class="todo todo-default">
										  <span class="ct-title">Promotion code</span>
										  <span class="badge badge-contributer bage-primary">N/A</span>
										</div>
									</div>
								</div>
							
							</div>
						</div>
					</div>
			</div>	  