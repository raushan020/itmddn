<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-cog" aria-hidden="true"></i> Manage Bill</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"> Billing </li>
         </ol>
      </div>
   </div>

<div class="container-fluid">
					<!-- /row -->
					<div class="row">
						<div class="col-md-12 col-sm-12">

							<div class="inbox inbox-widget">
								<div class="row">
									<div class="mail-box">
										<div class="col-md-8 col-sm-12">
												<aside class="lg-side">
											<div class="inbox-body">
												<div class="mail-card">
													<header class="card-header cursor-pointer" data-toggle="collapse" data-target="#full-message">
														<div class="card-title flexbox">
														  <div>
														  	<p>Below are all the invoices for the charges effected by e-campus.</p>
															please rate that every time happens, we also send you an e-mail titled 'e-campus payment invoice.'
																<p>there is usually a delay of a minutes before a payment shows up in this page.</p>

																<p>please dont hesitate to contact us if you have anu queries about a charge.
															</p>
														  </div>
														</div>
													</header>
												</div>
											  </div>
											  <div class="card">
								<div class="card-body">
									<div class="table-responsive"> 
										<div class="table-responsive"> 
											<div class="table-responsive"> 
												<table class="table table-striped">
													<thead>
														<tr>
															<th>S.No</th>
															<th>Date Issued </th>
															<th>Invoice #</th>
															<th>Transation ID</th>
															<th>Payment Method</th>
															<th>Status</th>
														</tr>
													</thead>
													
													<tbody>
														<?php if($admininvoice){ $count=1; foreach ($admininvoice as $key => $value)
														{?>							
														<tr>
															<td><?php echo $count; ?></td>
															<td><?php echo date('d-m-Y',strtotime($value->created_date)); ?></td>
															<td><?php echo $value->orderID; ?></td>
															<td><?php echo $value->paymentID; ?></td>
															<td><?php echo "N/A"; ?></td>
															<td><?php if($value->status == 1){ echo "Paid";} else {echo "Not Paid";} ?></td>
														</tr>
														<?php $count++; } }
														else
														{
															echo "No Payment";
														} ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
										</aside>
										</div>
							<div class="col-md-4 col-sm-12">
								<aside class="sm-side">
										  <ul class="nav nav-pills nav-stacked labels-info inbox-divider" style="margin: 20px;">
											   <h4>Invoice details</h4>
											   <p>These details will be include in all your future invoices.</p>
											   <strong><p>company name</p></strong>
											   <p>Edge</p>

											   <strong><p>Address</p></strong>
											   <p>India</p>
										  </ul>

										  <ul class="nav nav-pills nav-stacked labels-info ">
											  <h4>Billing notification</h4>
											   <p>Billing notifications are send to: </p>
											   <strong><p><?php echo $plans->email; ?></p></strong>
											  <a href="<?php echo base_url('profile/index') ?>" ><button type="button" class="btn btn-primary">update email id</button></a>
								
										  </ul>
										</aside>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>	