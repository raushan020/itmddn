			<div id="page-wrapper">
				
				<div class="container-fluid">
					<!-- /row -->
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="widget default-widget">
								<div class="error-content">
									<h4 class="four-four-error" style="font-size: 60px;">Exam<span> Not</span> Schedule</h4>
								    <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>

								    <p>
									YOUR EXAM IS NOT SCHEDULED YET, PLEASE VISIT THE <br><a href="<?php echo base_url() ?>"> <span class="btn btn-primary">dashboard</span></a>
								    </p>
								    <h3 style="font-size:22px;">Please use the feedback button to send us a screenshot to help us in fixing the error faster.</h3>

								</div>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>	
			</div>