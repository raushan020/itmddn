
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-eye"></i> Optional Subject </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("subject/index")?>"><?=$this->lang->line('menu_subject')?></a></li>
            <li class="active">Optional Subject</li>
         </ol>
      </div>
   </div>
   <!-- form start -->
   <div class="container-fluid">
  <a href="<?php echo base_url("subject/index");?>" class="btn btn-info" >Back</a>
                
      <div class="row">
         <div class="">
            <div class="card-body">
               <div class="col-sm-12">
                  <form style="" class="form-horizontal"  autocomplete="off" action="<?php echo base_url('subject/optioal');?>">
                     <?php 
                        $getClasses = $this->input->get('course');
                        $getdepartment=$this->input->get('department');
                        $getSemester = $this->input->get('yearsOrSemester');
                        $type = $this->input->get('type');
                        if($type){
                          $type = $this->input->get('type');
                        }else{
                          $type = "filter";
                        }
                        
                        
                        $date = $this->input->get('date');
                        
                        
                        
                                           ?>
                     <?php 
                        if($type=="filter") 
                        {
                         ?>
                     <input type="hidden" name="type" value="<?php echo $type ?>">
                     <div class="col-sm-3">
                        <div class="">
                           <div class="form-group">
                              <label for="classesID" class="control-label">
                              Department
                              </label>
                              <div class="">
                                 <?php
                                    $array = array("0" => "Select Department");
                                    
                                    foreach ($department as $department) {
                                    
                                        $array[$department->departmentID] = $department->department_name;
                                    
                                    }
                                    
                                    echo form_dropdown("department", $array, set_value("departmentID",$getdepartment), " onchange = 'Getclass($(this).val())' required class='form-control'");
                                    
                                    //onchange='ajaxGet_subCourses($(this).val())'
                                    
                                    
                                    
                                    ?>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <div class="form-group">
                              <label for="classesID" class="control-label">
                              Course
                              </label>
                              <div class="">
                                 <select id="classID" name="course" required class="form-control" onchange='GetsemesterAtd($(this).val())'>
                                  <option>Select</option>
                                    <?php  foreach ($classes as $classes)
                                    {
                                       if ($classes->classesID==$getClasses)
                                       {
                                       
                                           $selected =  "Selected";
                                       
                                       }
                                       else
                                       {
                                       
                                         $selected =  "";
                                       
                                       }
                                       
                                       ?>
                                    <option value="<?php echo $classes->classesID ?>" <?php echo $selected ?>><?php echo $classes->classes ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <div class="form-group">
                              <label for="classesID" class="control-label">Semester/Year</label>
                              <div class="">
                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>
                                    <option>Select</option>
                                    <?php 
                                       if ($classesRow) {
                                       
                                        $looping    =  (int) $classesRow->duration;
                                       
                                       if ($classesRow->mode==1) {
                                       
                                         for ($i=1; $i <=$looping; $i++) {
                                       
                                                if (CallYears($i)==$getSemester) {
                                       
                                           $select = 'Selected';
                                       
                                        }else{
                                       
                                        $select = '';
                                       
                                        }
                                       
                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                       
                                        
                                       
                                       }
                                       
                                       }
                                       
                                       
                                       
                                       else{
                                       
                                         for ($i=1; $i <=(2*$looping); $i++) {
                                       
                                       if (CallSemester($i)==$getSemester) {
                                       
                                           $select = 'Selected';
                                       
                                        }else{
                                       
                                        $select = '';
                                       
                                        }
                                       
                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       
                                       }
                                       
                                       }
                                       
                                       }
                                       
                                       ?>
                                 </select>
                              </div>
                              
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <div class="form-group">
                             <!--  <label for="classesID" class="control-label"></label> -->
                              <div class="">
                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 35px;" name="Search">
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     <?php } ?>
                     
                     
                  </form>
                  <div class="clearfix"></div>
                  <div class="table table-responsive">
                     <div class="col-sm-12">
                        <div id="hide-table">
                           <?php  if($getStudent){ ?>   
                           <form method="post" autocomplete="off">
                              <div class="col-sm-4"></div>
                              <div class="col-sm-4">
                                 <div class="">
                                 </div> 
                              </div>
                              <div class="col-sm-4"></div>
                              <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">
                                 <thead>
                                    <tr>
                                       <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                       <th class="col-sm-2">Name</th>
                                       <th class="col-sm-2">Subject Name</th>
                                       <th class="col-sm-2">Change Subject</th>
                                    </tr>
                                 </thead>
                                 <?php $getstudentid1=""; $getsubjectid1=0;
                                       if($showoptionalsubject)
                                       {
                                         foreach($showoptionalsubject as $showoptionalsubject)
                                         {
                                             $getsubjectid[]=$showoptionalsubject->subjectID;
                                             $getstudentid[]=$showoptionalsubject->studentID;
                                             $getsubjectname[]=$showoptionalsubject->subject;
                                         }
                                         $getsubjectid1=$getsubjectid;
                                         $getstudentid1=$getstudentid;
                                         $getsubjectname1=$getsubjectname;
                                         
                                       }?>
                                         
                                 <tbody>
                                  <?php $count=0; foreach ($getStudent as $key => $value)
                                  {?>
                                    <tr>
                                       <td class="col-sm-2"><?php echo $key+1; ?></td>
                                       <input type="hidden" name="studentID[]" id="studentname<?php echo $value['studentID'] ?>" value="<?php echo $value['studentID'] ?>">
                                       <td class="col-sm-2"><?php echo $value['name']; ?></td>
                                       <td>
                                         <?php if($getstudentid1)
                                               { $a=1;
                                                 for($i=0;$i<count($getstudentid1);$i++)
                                                 { 
                                                   if($getstudentid1[$i]==$value['studentID'])
                                                   {
                                                     echo $a.". ".$getsubjectname1[$i]; $a++;?>
                                                 </br>

                                                   <?php // break;
                                                   }
                                                   else
                                                   {

                                                    // echo $key->subjectID;
                                                   }
                                                   
                                                 }
                                               }
                                               else
                                               {
                                                 echo "No Optional Subject Assign";
                                               }
                                               ?>
                                       </td>
                                       <td>

                                       	<?php foreach ($value['object'] as $key )
                                           { $checked=""; ?>
                                                
                                           		<?php for($i=0;$i<count($getsubjectid1);$i++) 
  		                                       	{
  		                                       		if($getsubjectid1[$i]==$key->subjectID)
  		                                       		{
  		                                       			if($getstudentid1[$i]==$value['studentID'])
                                                          {
  		                                       			   $checked="checked";
  		                                       		    }
  		                                       		}

  		                                       	} ?>
                                             <input type="checkbox" name="optionalsubjectid" id="optionalsubjectid<?php echo $key->subjectID;?>" value="<?php echo $key->subjectID;?>" <?php echo $checked; ?> onclick="handleClick(this,<?php echo $key->subjectID;?>,<?php echo $value['studentID'] ?>);"><?php echo " ".$key->subject;?></br>
                                           
                                           <?php }?>
                                           
                                       <!--  <select name="optionalsubjectid[]" id="optionalsubjectid" multiple="" >
                                           <option value="0">Select</option>
                                           <?php foreach ($value['object'] as $key )
                                           {?>
                                              <option id="<?php echo $key->subjectID;?>" value="<?php echo $key->subjectID;?>"><?php echo $key->subject;?></option>
                                           <?php }?>
                                        </select> -->
                                       </td>
                                       
                                      
                                    </tr>
                                  <?php $count++; } ?>
                                   
                                 </tbody>
                              </table>
                              <center><input type="submit" value="Submit" name="submit" class="btn btn-success submitdata" style="margin-top: 20px;" name="Submit"></center>
                           </form>
                           <?php } else{ ?>
                           <div class="Noresuls">
                              <h1>Sorry we couldn't find any matches</h1>
                              <p>Maybe your search was too specific, please try searching with another term.</p>
                              <img src="<?php echo base_url()?>uploads/images/crying.png">
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- col-sm-12 -->
            </div>
            <!-- row -->
         </div>
         <!-- Body -->
      </div>
      <!-- /.box -->
   </div>
<script type="text/javascript">
   function GetsemesterAtd(classesID){
   
   
                  $.ajax({
   
                  url: base_url+'AjaxController/semester_get',
   
                  type: 'POST',
   
                  data:{classesID:classesID},
   
                  success: function(data){
   
                    console.log(data);
   
                  $('#yearSemesterID').html(data);
   
                  }
   
              }); 
   
   
    }
</script>
<script type="text/javascript">
   function GetsemesterAtd(classesID){
   
   
                  $.ajax({
   
                  url: base_url+'AjaxController/semester_get',
   
                  type: 'POST',
   
                  data:{classesID:classesID},
   
                  success: function(data){
   
                    console.log(data);
   
                  $('#yearSemesterID').html(data);
   
                  }
   
              }); 
   
   
    }
</script>
<script type="text/javascript">
   function Getclass(departmentID){
   
   // alert(departmentID);die;
                  $.ajax({
   
                  url: base_url+'AjaxController/class_get',
   
                  type: 'POST',
   
                  data:{departmentID:departmentID},
   
                  success: function(data){
   
                    console.log(data);
   
                  $('#classID').html(data);
   
                  }
   
              }); 
   
   
    }
</script>
<script type="text/javascript">
	function handleClick(check,val,student)
	{
		
		// console.log(chkbx.is(':checked'));
		var studentID=student;
		var optionalsubjectid=val;
		
		var classesID="<?php echo $this->input->get('course') ?>";
        var yearsOrSemester="<?php echo $this->input->get('yearsOrSemester') ?>";
        var departmentID="<?php echo $this->input->get('department')?>";
       
		if(check.checked==true)
		{
			
			$.ajax({
   
                  url: base_url+'subject/optioalsubjectadd',
   
                  type: 'POST',
   
                  data:{classesID:classesID,yearsOrSemester:yearsOrSemester,departmentID:departmentID,optionalsubjectid:optionalsubjectid,studentID:studentID},
   
                  success: function(data){
   
                    console.log(data);
                    // alert(data);
   
                  }
		        });
		}
		else
		{
            $.ajax({
   
                  url: base_url+'subject/optioalsubjectdelete',
   
                  type: 'POST',
   
                  data:{classesID:classesID,yearsOrSemester:yearsOrSemester,departmentID:departmentID,optionalsubjectid:optionalsubjectid,studentID:studentID},
   
                  success: function(data){
   
                    console.log(data);
                    // alert(data);
   
                  }
		        });
		}
		
	}
</script>
<script type="text/javascript">
	$(document).ready(function()
		{
			$('.submitdata').click(function()
				{
					location.reload(true);
				});
		});
</script>


