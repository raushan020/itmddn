<div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="fa fa-users"></i> <?=$this->lang->line('panel_title')?> </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("user/index")?>"><?=$this->lang->line('menu_user')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_user')?></li>
                </ol>
            </div>
    </div>

<div class="">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
         <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>

      <div class="row">
         
            <div class="card">
            <div class="card-body">

            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">



                    <?php

                        if(form_error('name'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="name_id" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_name")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name_id" name="name" value="<?=set_value('name')?>" >

                        </div>

                        <span class="col-sm-4 control-label" style="color: red">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('dob'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="dob" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_dob")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control datepicker_quiz_data" id="dob" name="dob" value="<?=set_value('dob')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('dob'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('sex'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_sex")?>

                        </label>

                        <div class="col-sm-6">

                            <?php

                                echo form_dropdown("sex", array($this->lang->line("user_sex_male") => $this->lang->line("user_sex_male"), $this->lang->line("user_sex_female") => $this->lang->line("user_sex_female")), set_value("sex"), "id='sex' class='form-control'");

                            ?>

                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('sex'); ?>
                        </span>
                    </div>
                    <?php

                        if(form_error('email'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_email")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email')?>" >

                        </div>

                        <span class="col-sm-4 control-label" style="color: red">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('phone'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_phone")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('address'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="address" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_address")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('address'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('jod'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="jod" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_jod")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control datepicker_quiz_data" id="jod" name="jod" value="<?=set_value('jod')?>" >

                        </div>

                        <span class="col-sm-4 control-label" style="color: red">

                            <?php echo form_error('jod'); ?>

                        </span>

                    </div>



                    <?php

                        if(isset($image))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">

                            <?=$this->lang->line("user_photo")?>

                        </label>

                        <div class="col-sm-4 col-xs-6 col-md-4">

                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />

                        </div>



                        <div class="col-sm-2 col-xs-6 col-md-2">

                            <div class="fileUpload btn btn-success form-control">

                                <span class="fa fa-repeat"></span>

                                <span><?=$this->lang->line("upload")?></span>

                                <input id="uploadBtn" type="file" class="upload" name="image" />

                            </div>

                        </div>

                         <span class="col-sm-4 control-label col-xs-6 col-md-4">



                            <?php if(isset($image)) echo $image; ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('usertype'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="usertype" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_type")?>

                        </label>

                        <div class="col-sm-6">

                            <?php

                                echo form_dropdown("usertype",

                                array(
                                    "Admin" => 'Admin',
                            
                                    "Accountant" => $this->lang->line("accountant"),

                                    "Principal" => 'Principal',

                                    "Director" => 'Director',
                                    "HOD" =>'HOD',

                                    // "Librarian" => $this->lang->line("librarian"),

                                    // "Receptionist" => $this->lang->line("Receptionist")
                                ),

                                    set_value("usertype"), "id='usertype' class='form-control'"

                                );

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('usertype'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('username'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="username" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_username")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username')?>" >

                        </div>

                         <span class="col-sm-4 control-label" style="color: red">

                            <?php echo form_error('username'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('password'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="password" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_password")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="password" class="form-control" id="password" name="password" value="<?=set_value('password')?>" >

                        </div>

                         <span class="col-sm-4 control-label">

                            <?php echo form_error('password'); ?>

                        </span>

                    </div>



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("add_user")?>" >

                        </div>

                    </div>



                </form>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });

$('#jod').datepicker({ dateFormat : 'dd-mm-yyyy' });

</script>

