 
  <style type="text/css">
    a.disabled {
      pointer-events: none;
      cursor: default;
    }
    .hover:hover{
      background-image: linear-gradient(to right, #3187da  , #2872b8);
       transition: .5s ease;
    }


                 .team-grids {
    position: relative;
    overflow: hidden;
    z-index: 1;
}
.team-info {
    position: absolute;
    bottom: -227px;
    margin: 0;
    background: rgba(0, 0, 0, 0.5);
    padding: 2em 0;
    -webkit-transition: .5s all;
    transition: .5s all;
    -moz-transition: .5s all;
    width: 82%;
    text-align: center;
}
.best-course-pic-text .best-course-pic {
    overflow: hidden;
    border-radius: 4px;
}
img {
    max-width: 100%;
    height: auto;
}
.trend-badge-2 {
    top: -18px;
    left: -50px;
    color: #fff;
    font-size: 12px;
    font-weight: 700;
    position: absolute;
    padding: 30px 35px 5px;
    -webkit-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    transform: rotate(-45deg);
    background-color: #ff5a00;
}
.best-course-pic-text .course-price {
    bottom: 20px;
    z-index: 1;
    top: inherit;
}
.course-price {
    top: 20px;
    left: 20px;
    font-weight: 700;
    padding: 5px 15px;
    border-radius: 4px;
    position: absolute;
}
.blakish-overlay {
    top: 0;
    opacity: 0;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    position: absolute;
    visibility: hidden;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
    background-color: rgba(0, 0, 0, 0.65);
}
.relative-position {
    position: relative;
}
.text-center {
    text-align: center!important;
}
.text-uppercase {
    text-transform: uppercase!important;
}
.best-course-pic-text:hover .blakish-overlay {
    opacity: 1;
    visibility: visible;
    z-index: 0;
}
.best-course-pic-text:hover .course-details-btn {
    opacity: 1;
    right: 25px;
    visibility: visible;
    z-index: 1;
}
.fa, .fas {
    /*font-family: 'Font Awesome 5 Free';*/
    font-weight: 900;
}
.course-details-btn a {
    font-size: 12px;
    color: #fff;
    font-weight: 700;
}
.course-price {
    top: 20px;
    left: 20px;
    font-weight: 700;
    padding: 5px 15px;
    border-radius: 4px;
    position: absolute;
}
.best-course-pic-text .course-price {
    bottom: 20px;
    z-index: 1;
    top: inherit;
}
.best-course-pic-text .best-course-text {
    background-color: #fff;
    border-radius: 4px;
    padding: 20px 25px 30px 25px;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
}
.best-course-pic-text .course-rate {
    position: absolute;
    right: 25px;
    bottom: 25px;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
}
.best-course-pic-text .course-rate li {
    font-size: 14px;
}
.ul-li ul li {
    list-style: none;
    display: inline-block;
}
.best-course-pic-text:hover .course-details-btn {
    opacity: 1;
    right: 25px;
    visibility: visible;
    z-index: 1;
}
.course-details-btn {
    left: 164px;
    opacity: 0;
    bottom: 139px;
    
    visibility: hidden;
    position: absolute;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
}
.video-play-btn {
    height: 68px;
    width: 68px;
    line-height: 42px;
    border-radius: 100%;
    color: #fff;
    position: absolute;
    top: 50%;
    right: 23px;
    left: 0;
   /* margin: 0 auto;*/
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
}
.gradient-bg, .modal-body .nws-button button, .teacher-pic-content .teacher-img-content:after, .course-details-category li:hover {
    background: #17d0cf;
    background: -moz-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, #4eb3bf), color-stop(51%, #17d0cf), color-stop(100%, #4eb3bf));
    background: -webkit-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #0071b0 100%);
    background: -o-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: -ms-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: -webkit-gradient(linear, left top, right top, from(#01a6fd), color-stop(51%, #17d0cf), to(#01a6fd));
    background: -webkit-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: linear-gradient(to right, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background-size: 200% auto;
    -webkit-transition: background 1s ease-out;
    -moz-transition: background 1s ease-out;
    -o-transition: background 1s ease-out;
    transition: background 1s ease-out;
}
.text-center {
    text-align: center!important;
}
               </style>
  <div class="container">



    <div class="page-section">
    	<div class="row">
        <div class="col-md-12">
    	     <div class="panel-footer" style="text-align: center;">

                 <h4 class="theight">

                 	<?php                 				
             			
              			echo $subject->subject;
                			
                 	  ?>
               	  	
                	  </h4>

                 <div>

                   <p class="theight"><strong style="color: #333">Course Status:</strong> In Progress</p>

                 </div>

                 <p class="theight" style="color: green; text-transform: uppercase;">You are enrolled in this course</p>

                 <div class=""></div> 
          </div>
      </div>
  </div>

      <div class="row">
        <div class="col-md-12">



          <!-- <div class="panel panel-default">

            <div class="media v-middle">

              <div class="media-left">

                <div class="bg-green-400 text-white">

                  <div class="panel-body">

                    <i class="fa fa-credit-card fa-fw fa-2x"></i>

                  </div>

                </div>

              </div>

              <div class="media-body">

                Your LMS is Here,  <span class="text-body-2">Start Learning</span>

              </div>

              <div class="media-right media-padding">

                <a class="btn btn-white paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmssubject');?>">

                Start now

            </a>

              </div>

            </div>

          </div> -->



          <div class="row" data-toggle="isotope">

            <div class="item col-xs-12 col-lg-9">


<!-- new -->
<div class="tabbable tabs-icons tabs-primary">

            <!-- Tabs -->
            <ul class="nav nav-tabs" style="background: whitesmoke;">
            <li class="active"><a href="#color-home-2" data-toggle="tab"><i class="fa fa-address-book"></i> Online Reader</a></li>
              <li><a href="#color-profile-2" data-toggle="tab"><i class="fa fa-play"></i> Watch Videos</a></li>
              <li><a href="#color-messages-2" data-toggle="tab"><i class="fa fa-sticky-note"></i> Notes</a></li>
            </ul>
            <!-- // END Tabs -->

            <!-- Panes -->
            <div class="tab-content" style="background-image: red">
              <div id="color-home-2" class="tab-pane active">
                <div class="table-responsive">
              <table class="table v-middle">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Subject Name</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody id="responsive-table-body">
                  <?php if(count($epub)) {

                   foreach($epub as $key => $value2) { 
  
                  ?>
                  <tr>
                    
                    <td>1.</td>
                    <td><img src="<?php echo base_url() ?>assets/lms/images/ebook.png" alt="">
                      <span><?php                         
                  
                    echo $subject->subject;
                      
                    ?></span>
                    </td>
                    <td style="text-align: center;"><button class="btn btn-primary"><a href="<?php echo base_url() ?>online_reader/reader/<?php echo $subject->subjectID.'/'.$value2->id; ?>" style="color: #fff;">Click here To Read</a></button> </td>
                  </tr>
                  <?php }}
                  else
                    {?>
                      <td><p>There are no recent Ebook to Read</p></td>
                   <?php }?>
                </tbody>
              </table>
            </div> 
              </div>

              <div id="color-profile-2" class="tab-pane">
                <div class="table-responsive">
              <table class="table v-middle">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Unit Name</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody id="responsive-table-body">
                  <?php 
                if(count($videos)) {
                    $count=1;
                 foreach($videos as $key => $value2) { 
                            
                 ?>
                  <tr>
                    
                    <td><?php echo $count ?></td>
                    <td><img src="<?php echo base_url() ?>assets/img/video.png" alt="" >
                      <span><?php
                        $sql = $this->db->select('unit_name')->from('units')->where('unitID',$value2->unitID)->get()->result_array();
                        foreach($sql as $row)
                        {
                          echo $row['unit_name'];
                        }
                      ?></span>
                    </td>
                    <td class="text-right"><button class="btn btn-primary"><a href="<?php echo base_url() ?>lms/lmsVideos/<?php echo $subject->subjectID.'/'.$value2->videoID; ?>" style="color: #fff;">Click here To Watch</a></button></td>
                  </tr>
                  <?php $count++;  }} ?>

                </tbody>
              </table>
            </div> 
              </div>


              <div id="color-messages-2" class="tab-pane">
                <div class="table-responsive">
              <table class="table v-middle">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Unit Name</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody id="responsive-table-body">
                  <?php 
                if(count($epub_notes)) {
                    $count=1;
                 foreach($epub_notes as $key => $value2) { 
                            
                 ?>
                  <tr>
                    
                    <td><?php echo $count ?></td>
                    <td><img src="<?php echo base_url() ?>assets/img/booknotes.png" alt="">
                      <span><?php
                        $sql = $this->db->select('unit_name')->from('units')->where('unitID',$value2->unitID)->get()->result_array();
                        foreach($sql as $row)
                        {
                          echo $row['unit_name'];
                        }
                      ?></span>
                    </td>
                    <td class="text-right"><button class="btn btn-primary"><a href="<?php echo base_url() ?>online_reader/epub_notes_reader/<?php echo $subject->subjectID.'/'.$value2->epub_notesID; ?>" style="color: #fff;">Click here To Read</a></button></td>
                  </tr>
                  <?php $count++;  }} ?>

                </tbody>
              </table>
            </div> 
              </div>
              
            </div>
            <!-- // END Panes -->

          </div>
<!-- new -->
      </div>

     <?php $this->load->view("components/page_lmssidebar"); ?>
           
        </div>

</div>

<div class="clearfix"></div>
<div class="col-xs-12 col-lg-12">

              <div class="panel panel-default">

                <div class="panel-heading">

                  <h4 class="text-headline margin-none">Assignment</h4>

                  <p class="text-subhead text-light">Your recent assignment</p>

                </div>


<table class="table">
  <thead>
    <tr>
    <th>Unit Number</th>
    <th>Unit Name</th>
    <th>Subject Name</th>
    <th>Assignment Name</th>
    <th>Start Date</th>
    <th>End Date</th>
    <th>Action</th>
    </tr>
  </thead>
  <tbody>
	<?php
		$counter_assign =0;
    if($assignment_recent)
    {
		 foreach ($assignment_recent as $key => $value) 
		 { 
	   ?> 
			<tr>
				<td>
					<?php 
						if($value['unit_code']=='')
						{
							echo "";
						}
						else
						{
							echo $value['unit_code'];
						}
					?>
				</td>
				<td> 
					<?php 
						if($value['unit_name']=='')
						{
							echo "All Units";
						}
						else
						{
							echo $value['unit_name'];
						}
					?>
				</td>
				<td><?php  echo $value['subject'] ?></td>
				<td><?php  echo $value['quiz_name'] ?></td>
				<td><?php  echo date("d-m-Y h:i:s A",($value['start_date'])); ?></td>
				<td><?php  echo date("d-m-Y h:i:s A",($value['end_date'])); ?></td>
				<td>
				<?php 
					if($value['exam_status']==1) 
					{ 
				?>
						<input type="hidden" value="<?php echo $value['subject']; ?>" id="subject_name_<?php echo $value['unit_code']; ?>">
						<input type="hidden" value="<?php echo $value['quiz_name']; ?>" id="assignment_name_<?php echo $value['unit_code']; ?>">
						<input type="hidden" value="<?php echo $value['quid']; ?>" id="qid_<?php echo $value['unit_code']; ?>">
						<a href="javascript:void(0)" class="btn btn-info result_show" id="<?php echo $value['unit_code']; ?>" data-id="<?php echo $value['unit_name']; ?>">Result</a>
						<a href="<?php echo base_url() ?>etsexam/index/<?php echo $value['quid'] ?>/2" class="btn btn-info" disabled>Completed</a>
				<?php 
					} 
					else 
					{
						if($value['exam_status_label']==1)
						{ 
							$date=date("d-m-Y h:i:s A"); $string=strtotime($date);
              // echo $date."     ".$value['start_date'];die;
							if($value['start_date'] >= $string)
							{
				?>
								<button class="btn btn-warning" id="start_date" data-toggle="modal" data-target="#myModal1">To Start</button>
				<?php 
							}
							else if($value['end_date'] <= $string)
							{
				?> 
								<button class="btn btn-danger" id="end_date" data-toggle="modal" data-target="#myModal">Over</button>
								<!-- Modal Start -->
								<div class="modal" id="myModal" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Assignment</h4>
											</div>
											<div class="modal-body">
												<p>You Miss This Assignment because Your End Date is Over. Unlock your next Assignment Please Click Okay Button, Otherwise You Miss All Assignment.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<a href="<?php echo base_url() ?>etsexam/index/<?php echo $value['quid'] ?>/2" class="btn btn-default">Okay</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Modal End -->
							<?php 
							}
							else
							{?>
							<a href="<?php echo base_url() ?>etsexam/index/<?php echo $value['quid'] ?>/2" class="btn btn-success">Started</a>
							<?php } ?>
					 
				<?php 
						} 
						else 
						{ 
				?>
						<button class="btn btn-primary" disabled>Lock</button>
				<?php 
						} 
					}	 
				?>
				</td>
       
			</tr>
			<div id="modal_show" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content" style="margin-top: 2rem;margin-bottom: 2rem;">
						<div class="modal-body">
							<button type="button" class="close" data-dismiss="modal" style="color: #000!important;">&times;</button>
							<div class="row">
								<div class="col-md-12">
									<div id="result"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
      
	<?php 
		} }
	?>
  </tbody>
</table>
   


              </div>

<!-- Modal Start -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assignment</h4>
        </div>
        <div class="modal-body">
          <p>Your Assignment Start Date is <?php  echo date("d-m-Y h:i:s A T",($value['start_date'])); ?>.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal End -->
                    <!--<div class="panel panel-default paper-shadow" data-z="0.5">

                <div class="panel-heading">

                  <h4 class="text-headline margin-none">Assignment</h4>

                  <p class="text-subhead text-light">Your recent performance</p>

                </div>

                <ul class="list-group">

              <?php
              $counter_assign =0;
               foreach ($assignment_recent_three as $key => $value) {
            $counter_assign +=1;
            if($counter_assign<5){
               ?>    
                  <li class="list-group-item media v-middle">

                    <div class="media-body">

                      <h4 class="text-subhead margin-none">

                        <a href="#" class="list-group-link"><?php  echo $value->quiz_name ?></a>

                      </h4>

                      <div class="caption">

                        <span class="text-light">Course:</span>

                        <a href="#">Basics of HTML</a>

                      </div>

                    </div>

                    <div class="media-right text-center">

                      <div class="text-display-1"><?php echo $value->score_obtained ?></div>

                      <span class="">Good</span>

                    </div>

                  </li>
                <?php } } ?>
                </ul>

                <div class="panel-footer">
                  <?php  if($counter_assign==0){ ?>

                  <a href="#" disabled="disabled" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php }else{ ?>
                  <a href="#" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php } ?>

                </div>

              </div> -->

            </div>

<!-- <script>
			var hash = window.location.hash,
				current = 0,
				demos = Array.prototype.slice.call( document.querySelectorAll( '#codrops-demos > a' ) );
			
			if( hash === '' ) hash = '#set-1';
			setDemo( demos[ parseInt( hash.match(/#set-(\d+)/)[1] ) - 1 ] );

			demos.forEach( function( el, i ) {
				el.addEventListener( 'click', function() { setDemo( this ); } );
			} );

			function setDemo( el ) {
				var idx = demos.indexOf( el );
				if( current !== idx ) {
					var currentDemo = demos[ current ];
					currentDemo.className = currentDemo.className.replace(new RegExp("(^|\\s+)" + 'current-demo' + "(\\s+|$)"), ' ');
				}
				current = idx;
				el.className = 'current-demo'; 
			}
		</script> -->
<script>
	$(".result_show").click(function(e){
		e.preventDefault();
		unit_code = this.id;
		unit_name = $(this).data('id');
		subject_name = $("#subject_name_"+unit_code).val();
		assignment_name = $("#assignment_name_"+unit_code).val();
		qid = $("#qid_"+unit_code).val();
		$.ajax({
			type:"POST",
			data:{"unit_code":unit_code,"unit_name":unit_name, "subject_name":subject_name, "assignment_name":assignment_name,"qid":qid},
			url:"<?php echo base_url(); ?>lms/result_show",
			success:function(res){
				$("#result").html(res);
				$("#modal_show").modal('show');
			}
		});
	});
</script>