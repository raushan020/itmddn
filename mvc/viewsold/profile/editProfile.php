<style type="text/css">
    #upload_images_mess{
        display:none;
        color:green;
    }
</style>

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-user"></i> Edit Profile </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?=base_url("student/index/")?>">Profile</a></li>

            <li class="active">Edit Profile </li>
            </ol>
        </div>
    </div>

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">

        

            <div class="col-sm-12">
    <p>Field are required with <span class="red-color">*</span></p>

                <form class="form-horizontal" role="form" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">

                   <h3  class="border_heading">Personal Information</h3>

                    <?php 

                        if(form_error('name')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="name_id" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_name")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name_id" required="required" name="name" value="<?=set_value('name', $student->name)?>" disabled>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>

                        <?php 

                            if(form_error('father_name'))

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="guardiaName" class="col-sm-2 control-label">

                                <?=$this->lang->line("father_name")?> <span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <div class="select2-wrapper">

                                    <input type="text" class="form-control" required="required" id="father_name" name="father_name" value="<?=set_value('father_name',$student->father_name)?>" disabled>

                                </div>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('father_name'); ?>

                            </span>

                        </div>



            <?php 

                            if(form_error('mother_name'))

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="guardiaName" class="col-sm-2 control-label">

                                <?=$this->lang->line("mother_name")?> <span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <div class="select2-wrapper">

                                    <input type="text" class="form-control" required="required" id="mother_name" name="mother_name" value="<?=set_value('mother_name',$student->mother_name)?>" disabled>

                                </div>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('mother_name'); ?>

                            </span>

                        </div>
                    <?php 

                        if(form_error('dob')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="dob" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_dob")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="dob" required="required" name="dob" value="<?=set_value('dob', date("d-m-Y", strtotime($student->dob)))?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('dob'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('sex')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_sex")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <?php 

                                echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex", $student->sex), "id='sex' class='form-control' required ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('sex'); ?>

                        </span>



                    </div>


                    <?php 

                        if(form_error('email')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_email")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" name="email" value="<?=set_value('email', $student->email)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('phone')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_phone")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" required="required"  id="phonecountry" name="phone" value="<?=set_value('phone', $student->phone)?>" >
                                                            <span id="valid-msg" class="hide">✓ Valid</span>
                                <span id="error-msg" class="hide">Invalid number</span>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('address')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_address")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $student->address)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('address'); ?>

                        </span>

                    </div>

                        

                        <?php 

                            if(form_error('pin')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="address" class="col-sm-2 control-label">

                                <?=$this->lang->line("pin")?>

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="pin" name="pin" value="<?=set_value('pin',$student->pin)?>" >

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('pin'); ?>

                            </span>

                        </div>


                        <?php 

                            if(form_error('nationality')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="address" class="col-sm-2 control-label">

                                <?=$this->lang->line("nationality")?>

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="nationality" name="nationality" value="<?=set_value('nationality',$student->nationality)?>" disabled>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('nationality'); ?>

                            </span>

                        </div>


                         <?php 

                            if(form_error('aadhar')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="address" class="col-sm-2 control-label">

                             Aadhaar no. 

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="aadhar" name="aadhar" value="<?=set_value('aadhar',$student->aadhar)?>" required >

                            </div>

                                           

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('aadhar'); ?>

                            </span>

                        </div>

                  
                    



                    



                   

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="Update Profile" >

                        </div>

                 



                </form>



            </div> <!-- col-sm-8 -->

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->

<style type="text/css">
.showLoaderSubcour{
        display:none;
    }
        .positionRelative{
      position:relative;
    }
    .positionAbsolutePassword{
    position:absolute;
    top:0px;
    position: absolute;
    top: 5px;
    right: 23px;
    }
    .hide{
        color:red;
    }
    #valid-msg{
        color:green;
    }
    #error-msg{
        color:red;
    }
</style>

<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });



$('#classesID').change(function(event) {

    var classesID = $(this).val();

    if(classesID === '0') {

        $('#classesID').val(0);

    } else {

        $.ajax({

            type: 'POST',

            url: "<?=base_url('student/sectioncall')?>",

            data: "id=" + classesID,

            dataType: "html",

            success: function(data) {

               $('#sectionID').html(data);

            }

        });

    }

});
</script>
<script type="text/javascript">
  
  function getvalCourceMode(sel)
{
// alert(sel.value);
 if (sel==2) {
$("#semester_id").show();
$("#yearID").hide();   
}else{
  $("#semester_id").hide();
  $("#yearID").show();
}

}

  function getvalEntryMode(sel)
{

 if (sel=="first_semester" || sel== "first_year") {
$("#fresher").show();
$('#fresherSelect').removeAttr("disabled",true);
$('#lateralSelect').attr("disabled",true);
$("#lateral").hide(); 
$("#transfer").hide();
$('#transferSelect').attr("disabled",true);
}
 else if(sel=="second_semester" || sel=="third_semester" || sel=="second_year") {
    $("#fresher").hide();
    $('#fresherSelect').attr("disabled",true);
    $("#lateral").show();
    $('#lateralSelect').removeAttr("disabled",true); 
    $("#transfer").hide();
    $('#transferSelect').attr("disabled",true);
}
else{
  $("#fresher").hide();
  $('#transferSelect').attr("disabled",true);
  $("#lateral").hide();
  $('#lateralSelect').attr("disabled",true);
  $("#transfer").show();
  $('#transferSelect').removeAttr("disabled",true); 
}

}
</script>
<script type="text/javascript">
    $(".CalenderYear").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
</script>
<style type="text/css">
.red-color{
    color:red;
}
    .hide{
        color:red;
    }
</style>
<div id="uploadimageModalStudent" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_student" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                          <button class="btn btn-success crop_image_student">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
//     function validateForm() {
// aadhar = document.getElementById("aadhar").value;

// var adharcard = /^\d{12}$/;
// //var adharsixteendigit = /^\d{16}$/;
// if (aadhar != '') {
//      if (!aadhar.match(adharcard))
//      {
//          alert("Invalid Aadhar Number");
//          return false;
//      }
//  }
 // if (adhar != '')
 // {
 //     if(!adhar.match(adharsixteendigit))
 //     {
 //         alert("Invalid Aadhar Number");
 //         return false;
 //     }
 // }
}
</script>
