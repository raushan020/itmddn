<div class="">
           <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="fa fa-user"></i> Profile</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-dashboard"></i> Dashboard/</a></li>
            <li class="active">Change Picture</li>
            </ol>
                    </div>
            </div>
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

<?php
    $usertype = $this->session->userdata('usertype');
    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Student" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "Professor") {
?>

<?php  
if ($this->uri->segment(3)) {
 ?>

<input type="hidden" name="" id="updloadImageByadmin" value="<?php echo $this->uri->segment(3) ?>">

 <?php } ?>


    <section class="panel">
      <div class="panel panel-default">
          <div class="panel-heading">Select Profile Image</div>
          <div class="panel-body" align="center">
            <input type="file" name="upload_image" id="upload_image" />
            <br />
            <div id="uploaded_image"></div>
          </div>
      </div>
    </section>
</div>
</div>
</div>
</div>
</div>


<?php } ?>

<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-8 text-center">
                        <div id="image_demo" style="width:350px; margin-top:30px"></div>
                  </div>
                  <div class="col-md-4" style="padding-top:30px;">
                    <br/>
                    <br/>
                    <br/>
                    <button class="btn btn-success crop_image">Crop & Upload Image</button>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->

<style type="text/css">
   .boredr_none .nav-tabs{
    border-bottom: 0px solid #e8edef;
   }
   .nopading{
      padding-right:0px;
      padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #2057a8;
   color:#fff;
   }
  .theme_input_blue .form-control::-webkit-input-placeholder {
  color: #fff;
}
.theme_input_blue .form-control::-moz-placeholder {
  color: #fff;
}
.theme_input_blue .form-control:-ms-input-placeholder { 
  color: #fff;
}
.theme_input_blue .form-control:-moz-placeholder {
  color: #fff;
}
.forpostionReletive {
    position: relative;
}
.postionAbsoluter {
    position: absolute;
    right: 7px;
    top: 8px;
}
.action-layout ul li{
   display:inline-block;
}
</style>

