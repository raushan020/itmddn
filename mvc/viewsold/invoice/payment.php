<?php 
   $feePaidPerYear = 0;
   foreach ($paymentDetails as $key => $value) {
    $feePaidPerYear += $value->paymentamount;
   }
          if($this->input->get('year')){
              $year = $_GET['year'];
              }else{
              $year = 'all';
              }

            if($this->input->get('type')){
              $feeType = $_GET['type'];
              }else{
              $feeType = 'clg';
              }
              if($this->input->get('month')){
              $month = $_GET['month'];
              }else{
              $month = date('m');
              }
                            if($this->input->get('monthYear')){
              $monthYear = $_GET['monthYear'];
              }else{
              $monthYear = date('Y');
              }
   ?>
<div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="fa fa-rupee"></i> Fee </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseys"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("invoice/index")?>">Fee</a></li>
            <li class="active"><?=$this->lang->line('add_payment')?></li>
                </ol>
            </div>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-8">
                <?php 
                    $usertype = $this->session->userdata("usertype"); 
                    if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin") { 
                ?>
                    <form class="form-horizontal" role="form" method="post" autocomplete="off">

                      <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                      ?>
                        <label for="amount" class="col-sm-3 control-label">
                           Total <?=$this->lang->line("invoice_amount")?>
                        </label>
                        <div class="col-sm-6">
                           <?php
                              if ($feeType=='hstl') { ?>
                           	<input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->hostelFee)?>"  disabled>
                             <?php } else {  ?>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', floor($invoice->amount)-$invoice->paidamount)?>"  disabled>
                             <?php } ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                      <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                      ?>
                        <label for="amount" class="col-sm-3 control-label">
                           Due <?=$this->lang->line("invoice_amount")?>
                        </label>
                        <div class="col-sm-6">
                          <?php
                              if ($feeType=='hstl') { ?>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->hostelFee-$feePaidPerYear)?>"  disabled>
                          <?php } else { ?>
                             <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->amount-$feePaidPerYear)?>" disabled>
                           <?php } ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                            Pay Now
                        </label>
                        <div class="col-sm-6">
                          <?php
                              if ($feeType=='hstl') { ?>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->hostelFee-$feePaidPerYear)?>" >
                          <?php  } else { ?>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->amount-$feePaidPerYear)?>" >
                          <?php } ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="receipt" class="col-sm-3 control-label">
                            Receipt No.
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter Receipt No." id="receipt" name="receipt" value="<?=set_value('receipt')?>">
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('receipt'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('payment_method')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="payment_method" class="col-sm-3 control-label">
                            <?=$this->lang->line("invoice_paymentmethod")?>
                        </label>
                        <div class="col-sm-6">
                          <?php
                            $array = $array = array('0' => $this->lang->line("invoice_select_paymentmethod"));
                            $array['Cash'] = 'Cash Mode';
						                $array['Cheque'] = 'Cheque Mode';
                            $array['Bank'] = 'Bank Transffer';
                            $array['Draft'] = 'DD';
                            $array['onlinePayment'] = 'Online Payment';
						                // $array['Paypal'] = $this->lang->line('invoice_paypal');
                            echo form_dropdown("payment_method", $array, set_value("payment_method"),  "id='payment_method' onchange='changeInvoice($(this).val())' class='form-control'");
                          ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('payment_method'); ?>
                        </span>
                    </div>


                    <?php 
                        if(form_error('paymentdate')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-3 control-label">
                           Payment Date
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control datepicker_quiz_data" name="paymentdate" required="required" data-lang="en" placeholder="Enter Date Here..." value="" >
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('paymentdate'); ?>
                        </span>
                    </div>

                    <div id="custome_paymentType"> </div>

                    <div class="form-group">
                <?php 
                  if ($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Superadmin" || $usertype == "Accountant") {
               ?> 
                        <div class="col-sm-offset-3 col-sm-8">
                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("add_payment")?>" ><?php } ?>
                        </div>
                    </div>

                    </form>
                <?php } elseif($usertype == "Student" || $usertype == "Parent") { ?>
                    <form class="form-horizontal" role="form" method="post">
                        <?php 
                            if(form_error('amount')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="amount" class="col-sm-2 control-label">
                                <?=$this->lang->line("invoice_amount")?>
                            </label>
                            <div class="col-sm-6">
                              <?php
                              if ($year=='all') { ?>
                              
                                <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->amount-$invoice->paidamount)?>" >
                              <?php } else{ ?>
                                <input type="text" class="form-control" id="amount" name="amount" value="<?=set_value('amount', $invoice->totalfeepaidperyear-$feePaidPerYear)?>" >
                              <?php } ?>
                            </div>
                            <span class="col-sm-4 control-label">
                                <?php echo form_error('amount'); ?>
                            </span>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("add_payment")?>" >
                            </div>
                        </div>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function changeInvoice(val){

if(val=='Cheque'){

var chagesIputs =  `<div class='form-group' >
                
                        <label for="amount" class="col-sm-3 control-label">
                            Cheque Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="cnumber" placeholder="Enter Cheque Number..." value="" >
                        </div>
                    
                    </div>

   
                        <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                            Cheque Name
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="cname" placeholder="Enter Cheque Name..." value="" >
                        </div>
                      
                    </div>
                           <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                            Account Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="acnumber" id="dob" placeholder="Enter Account Number..." value="" >
                        </div>
                    
                    </div>

                    </div>`;

$('#custome_paymentType').html(chagesIputs);
    }
else if(val =='Bank'){
        var chagesIputs = `<div class="form-group">
                        <label class="col-sm-3 control-label">
                            Bank Name
                        </label>
                        <div class="col-sm-6">                         
                        <select name="bank" class="form-control">
                           <option selected="" class="plch" value="" disabled="" >Select Bank</option>
                           <option value="SBIN" >SBI</option>
                           <option value="HDFC" data-reactid="$HDFC1">HDFC Bank</option>
                           <option value="ICIC" data-reactid="$ICIC2">ICICI Bank</option>
                           <option value="AXIS" data-reactid="$AXIS3">AXIS Bank</option>
                           <option value="CITA" data-reactid="$CITA4">CITI Bank</option>
                           <option value="ALLB" data-reactid="$ALLB5">Allahabad Bank</option>
                           <option value="ANDB" data-reactid="$ANDB6">Andhra Bank</option>
                           <option value="BAHK" data-reactid="$BAHK7">Bank of Bahrain and Kuwait</option>
                           <option value="BOBC" data-reactid="$BOBC8">Bank of Baroda - Coporate</option>
                           <option value="BOBR" data-reactid="$BOBR9">Bank of Baroda - Retail</option>
                           <option value="BOI" data-reactid="$BOI10">Bank of India</option>
                           <option value="MAHB" data-reactid="$MAHB11">Bank of Maharastra</option>
                           <option value="CANB" data-reactid="$CANB12">Canara Bank</option>
                           <option value="CSYB" data-reactid="$CSYB13">Catholic Syrian Bank</option>
                           <option value="CITU" data-reactid="$CITU14">City Union Bank</option>
                           <option value="CORB" data-reactid="$CORB15">Corporation Bank</option>
                           <option value="DENB" data-reactid="$DENB16">Dena bank</option>
                           <option value="DEUB" data-reactid="$DEUB17">Deutche Bank</option>
                           <option value="DECB" data-reactid="$DECB18">Development Credit Bank</option>
                           <option value="FEDB" data-reactid="$FEDB19">Federal Bank</option>
                           <option value="IDFC" data-reactid="$IDFC20">IDFC Bank</option>
                           <option value="INDB" data-reactid="$INDB21">Indian Bank</option>
                           <option value="IOB" data-reactid="$IOB22">Indian Overseas Bank</option>
                           <option value="INIB" data-reactid="$INIB23">IndusInd Bank</option>
                           <option value="JNKB" data-reactid="$JNKB24">Jammu and Kashmir Bank</option>
                           <option value="KRTB" data-reactid="$KRTB25">Karnataka Bank Ltd</option>
                           <option value="KARB" data-reactid="$KARB26">Karur Vyasa Bank</option>
                           <option value="KOBK" data-reactid="$KOBK27">KOTAK Bank</option>
                           <option value="LXCB" data-reactid="$LXCB28">Laxmi Vilas Bank - Corporate Net Banking</option>
                           <option value="LXRB" data-reactid="$LXRB29">Laxmi Vilas Bank - Retail Net Banking</option>
                           <option value="ORTB" data-reactid="$ORTB30">Oriental Bank of Commerce</option>
                           <option value="PJSB" data-reactid="$PJSB31">Punjab and Sind Bank</option>
                           <option value="PJRB" data-reactid="$PJRB32">Punjab National Bank</option>
                           <option value="PJRB" data-reactid="$PJRB33">Punjab National Bank - Retail Banking</option>
                           <option value="RATB" data-reactid="$RATB34">RBL Bank Limited</option>
                           <option value="SHVB" data-reactid="$SHVB35">Shamrao Vittal Cooperative Bank</option>
                           <option value="SINB" data-reactid="$SINB36">South Indian Bank</option>
                           <option value="STCB" data-reactid="$STCB37">Standard Chartered Bank</option>
                           <option value="SBJB" data-reactid="$SBJB38">State Bank of Bikaner and Jaipur</option>
                           <option value="SHYB" data-reactid="$SHYB39">State Bank of Hyderabad</option>
                           <option value="SMYB" data-reactid="$SMYB40">State Bank of Mysore</option>
                           <option value="SPTB" data-reactid="$SPTB41">State Bank of Patiala</option>
                           <option value="STVB" data-reactid="$STVB42">State Bank of Travancore</option>
                           <option value="SYNB" data-reactid="$SYNB43">Syndicate Bank</option>
                           <option value="TMEB" data-reactid="$TMEB44">Tamilnadu Mercentile Bank</option>
                           <option value="UCOB" data-reactid="$UCOB45">UCO Bank</option>
                           <option value="UBI" data-reactid="$UBI46">Union Bank of India</option>
                           <option value="UNIB" data-reactid="$UNIB47">United Bank of India</option>
                           <option value="VIJB" data-reactid="$VIJB48">Vijaya Bank</option>
                           <option value="YEBK" data-reactid="$YEBK49">YES Bank</option>
                        </select>
                        </div>
                        </div>

                        <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                            Payee Name
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="payname" value="" >
                        </div>
                    
                    </div>
                    <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                           Beneficiary Account Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ben_accNumber" value="" >
                        </div>
                    
                    </div>
                    <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                           Reference Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="refNumber" value="" >
                        </div>
                    
                    </div>`;

$('#custome_paymentType').html(chagesIputs);

    }

    else if( val == 'Draft' ){
      var chagesIputs = `<div class='form-group' >
                
                        <label for="amount" class="col-sm-3 control-label">
                            DD Name
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter DD Name..." name="ddname" value="" >
                        </div>
                    
                    </div>

                     <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                            DD Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter DD Number..." name="DDnumber" value="" >
                        </div>
                    
                    </div>
                    <div class='form-group' >
                        <label for="amount" class="col-sm-3 control-label">
                            DD Date
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter DD Date..." name="dddate" value="" >
                        </div>
                    
                    </div>`;
       $('#custome_paymentType').html(chagesIputs);             
    }

    else if( val == 'Cash' ){
      var chagesIputs = ``;
      $('#custome_paymentType').html(chagesIputs);
    }

    else if( val == 'onlinePayment' ){
      var chagesIputs = ``;
      $('#custome_paymentType').html(chagesIputs);
    }

}

</script>

<script type="text/javascript">
$('#classesID').change(function(event) {
    var classesID = $(this).val();
    if(classesID === '0') {
        $('#studentID').val(0);
    } else {
        $.ajax({
            type: 'POST',
            url: "<?=base_url('invoice/call_all_student')?>",
            data: "id=" + classesID,
            dataType: "html",
            success: function(data) {
               $('#studentID').html(data);
            }
        });
    }
});


$('#feetype').keyup(function() {
    var feetype = $(this).val();
    $.ajax({
        type: 'POST',
        url: "<?=base_url('invoice/feetypecall')?>",
        data: "feetype=" + feetype,
        dataType: "html",
        success: function(data) {
            if(data != "") {
                var width = $("#feetype").width();
                $(".book").css('width', width+25 + "px").show();
                $(".result").html(data);

                $('.result li').click(function(){
                    var result_value = $(this).text();
                    $('#feetype').val(result_value);
                    $('.result').html(' ');
                    $('.book').hide();
                });
            } else {
                $(".book").hide();
            }
           
        }
    });
});

$('#date').datepicker();
</script>