  <div class="">

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="fa fa-list"></i> Attendance </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Attendance</li>

            </ol>

        </div>

    </div>

   <!-- /.box-header -->



    <!-- form start -->

   <div class="container-fluid">

      <div class="row">         

        <div class="card">

            <div class="card-body">    



    <div class="box-body">

     

        <div class="row">

            <div class="col-sm-12">
                <div class="col-md-2">
                </div>

                        <div class="col-md-4">
                            <?php 
                                    // exit();
                                $getDate = $_GET['date'];
                                $month =  date('m',$getDate);
                                $year =  date('Y',$getDate);

                            ?>

                            <select class="form-control" onchange="changedate()" id="month_date">
                                <option value="0">Select Month</option>
                                <option selected value="01" <?php if($month=='01'){echo 'selected';} ?>>January</option>
                                <option value="02" <?php if($month=='02'){echo 'selected';} ?>>February</option>
                                <option value="03" <?php if($month=='03'){echo 'selected';} ?>>March</option>
                                <option value="04" <?php if($month=='04'){echo 'selected';} ?>>April</option>
                                <option value="05" <?php if($month=='05'){echo 'selected';} ?>>May</option>
                                <option value="06" <?php if($month=='06'){echo 'selected';} ?>>June</option>
                                <option value="07" <?php if($month=='07'){echo 'selected';} ?>>July</option>
                                <option value="08" <?php if($month=='08'){echo 'selected';} ?>>August</option>
                                <option value="09" <?php if($month=='09'){echo 'selected';} ?>>September</option>
                                <option value="10" <?php if($month=='10'){echo 'selected';} ?>>October</option>
                                <option value="11" <?php if($month=='11'){echo 'selected';} ?>>November</option>
                                <option value="12" <?php if($month=='12'){echo 'selected';} ?>>December</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" onchange="changedate()" id="year_date">
                                <option value="0">Select Year</option>
                                <?php for($i=2000; $i<=date('Y'); $i++){ ?>
                                    <option value="<?php echo $i; ?>" <?php if($year==$i){echo "Selected";} ?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                </div>

            <div class="clearfix"></div>
<div class=" table-responsive" style="margin-top: 20px;">
             <div id="hide-table">
                    <table id="" class="table table-striped table-bordered table-hover no-footer">

                             <!--    <table class="table table-hover attendance_list"> -->
                                    <thead>
                                        <tr>
                                           <!--  <th>Employee</th> -->
                                            <?php
                                                $getDate = $_GET['date'];
                                                $month =  date('m',$getDate);
                                                $year =  date('Y',$getDate);
                                                for ($i=1; $i <=$day ; $i++) { 
                                                    $dateselect =$year.'-'.$month.'-'.$i;

                                            ?>
                                            <th style='background:<?php if (date('D',strtotime($dateselect))=='Sun') {echo "#ffd4c3"; }?> !important'>
                                                <?php echo $i." ";
                                                echo "<br>";
                                            echo  date('D',strtotime($dateselect));
                                             ?></th>
                                        <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
    foreach ($arrayData as $key => $value) {
?>
                                            <tr>
                                            <!-- <td ><?php echo  $value['name']?></td> -->
                                            <?php foreach ($value['atds'] as $key => $value) {
                               ?>
                               <!-- style="<?php ?>background: #ffd4c3 !important"> -->
                                            <td style='background:<?php if (date('D',strtotime($dateselect))=='Sun') {echo "#ffd4c3"; }?> !important'>
                                            <?php if($value['Futuredate']=='N'){ ?>
                                            <?php if($value['atendStatus']==1) { ?>
                                                P
                                           <?php  } elseif ($value['atendStatus']==4) {
                                             echo '-';
                                           } elseif ($value['atendStatus']==0) {
                                               echo 'A';
                                           }else { ?>
                                           -
                                           <?php } }else{ 
                                            echo '-';
                                     } ?>
                                                </td>
                                         
                                            <?php } ?>

                                        </tr>

<?php        
    }
?>

                                
                                    </tbody>
                                </table>

                </div>

</div>

            </div>



        </div>



    </div>



</div>

</div>

</div>

</div>

</div>
<script type="text/javascript">
    function changedate(){
    var month_date =  $('#month_date').val();
    var year_date =  $('#year_date').val();
    var subjectID  =  "<?php echo $this->input->get('subjectID') ?>";
    var studentid="<?php echo $this->input->get('studentID') ?>";

    $.ajax(
    {
    url:"<?php echo base_url() ?>attendance/selectDate",
    type:"POST",
    data:{month_date:month_date,year_date:year_date},
    success:function(data){
        console.log(data);
      window.location.href = '<?php echo base_url() ?>attendance/attendance_view?date='+data+'&subjectID='+subjectID+'&studentID='+studentid;
    }

    });
}
</script>