<div class="">


    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="ti ti-id-badge"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

           <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("professor/index")?>"><?=$this->lang->line('panel_title')?></a></li>

            <li class="active">Reset Password</li>
            </ol>
        </div>
    </div>
    <!-- form start -->
    <div class="container-fluid">
         <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
        <div class="row">
       <div class="card">
            <div class="card-body">
            <div class="col-sm-12">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <p>Enter a new password <span class="red-color">*</span></p>
                    <?php 

                        if(form_error('new-password')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="name_id" class="col-sm-3 control-label">

                             New Password <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-5">

                            <input type="text" class="form-control" id="new_password" name="new_password" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('new-password'); ?>

                        </span>

                    </div>


                    <div class="form-group">

                        <div class="col-sm-offset-3 col-sm-6">

                            <input type="submit" class="btn btn-success add-btn" name="submit" id="submit" value="Reset Password" >

                        </div>

                    </div>



                </form>



            </div><!-- /col-sm-8 -->

        </div>

    </div>

</div>
</div>
</div>

<style type="text/css">
    .displayNone{
        display:none;
    }
</style>
<script type="text/javascript">
    function AddUsernamePass(){
    $('.displayNone').show();
    var name_id = $('#name_id').val();
    $.ajax({
    type:'post',
    url:"<?php echo base_url() ?>teacher/AddUsernamePass",
    data:{name_id:name_id},
    success:function(response) {
        var objective = jQuery.parseJSON(response);
        $('#username').val(objective.username);
        $('#password').val(objective.password);
        $('.displayNone').hide();
    
    }

});
    }
</script>



<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });

$('#jod').datepicker();

</script>

