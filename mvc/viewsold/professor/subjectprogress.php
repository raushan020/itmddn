<style>
  .add_sub_11{
    float: none !important;
  }
  
</style>
<div class="">

    <div class="row page-titles">

      <div class="col-md-5 align-self-center">

         <h3 class="text-themecolor"><i class="fa fa-spinner"></i> Subject Progress </h3>

      </div>

      <div class="col-md-7 align-self-center">

         <ol class="breadcrumb">
           <?php $usertype = $this->session->userdata("usertype");
                                      // print_r($usertype);die; 
                                      if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype == "HOD")
                                      {?>
                                        <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li><li><a href="<?=base_url("professor/index")?>"></i>professor</a></li>
                                      <?php } else
                                      {?>
                                          <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                                     <?php } ?>

            

            <li class="active">Subject Progress</li>

         </ol>

      </div>

   </div>

   

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card-body">

                  <div class="clearfix"></div>
            
                  <div class="container-fluid">
                    <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>


      <div class="row">


         <div class="col-md-12 col-sm-6 col-xs-12">

                  

                     <div class="table-wrapper">

                        <div id="editbankloandetails" title="Edit" style="width:900px;display:none"></div>

                        <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="professorexportexcel">

                           <thead>

                              <tr style="font-weight:bold">

                                 <td>S.N.</td>
                                 <td>Course Name</td>
                                 <td>Semester</td>
                                 <td>Subject Name</td>

                                 <td>Subject Progress</td>
                                 <?php $usertype = $this->session->userdata("usertype");
                                      // print_r($usertype);die; 
                                      if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype == "HOD")
                                      {?>
                                        <td>View Progress</td>
                                      <?php } else
                                      {?>
                                        <td>Update Progress </td>
                                     <?php }?>
                                 
                                 
                              </tr>

                           </thead>

                           <tbody>

                              <?php count($subject); $count=1; foreach($subject as $subjects => $value)

                              {?>

                                   <tr>

                                    <td><?php echo $count?></td>
                                    <td><?php echo $value['classes']?></td>
                                    <td><?php echo $value['yearsOrSemester']?></td>
                                    <td><?php echo $value['subject']?></td>

                                    <td>

                                      <?php 

                                      foreach ($value['object'] as $key )

                                    {

                                        $a=$key->topicid;

                                        $b=$key->totalunit;

                                        if($a)

                                        {

                                          $topicconut=explode(",",$a);

                                          // print_r(expression)

                                          // $array = json_decode($a, true);

                                          // $array = array_values(array_unique($array, SORT_REGULAR));

                                          $avg=(count($topicconut)*100)/$b;

                                          $unitavg=round($avg,0);



                                          ?>

                                          <div class="progress">
                                            <?php if($unitavg >= 0 && $unitavg <= 50)
                                            {?>
                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $unitavg.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);background-color: red;">
                                           <?php }
                                           else if($unitavg >= 51 && $unitavg <= 75)
                                            {?>
                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $unitavg.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);background-color: yellow;">
                                            <?php }
                                           else if($unitavg >= 76)
                                            {?>
                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $unitavg.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);background-color: green;">
                                            <?php } ?>
                                              

                                            </div>



                                            <div class="progress-value" style=" position:absolute; padding: 3px 0px 0px 65px; color: #fff;"><?php echo $unitavg.'%';?></div>
                                            

                                          </div>
                                          <span style="color:red;font-weight: bold;font-size: 10px;"><?php echo count($topicconut)."/".$b;?>(Total Complete Topics)</span>
                                        <?php }

                                        else

                                         {?>

                                           <div class="progress">

                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:0%; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);">                 

                                              </div>

                                              <div class="progress-value" style=" position: absolute;padding: 3px 0px 0px 65px;">0%</div>

                                           </div>

                                         <?php }?>

                                      <?php }?> 

                                    </td>
                                    <!-- <td>
                                      <?php foreach ($value['object'] as $key )
                                      {

                                        $a=$key->topicid;

                                        $b=$key->totalunit;

                                        if($a)

                                        {
                                          
                                          $topicconut=explode(",",$a);
                                          echo count($topicconut)."/".$b;
                                        }
                                      }?>
                                        
                                    </td> -->

                                    <td>

                                      <a href="javascript:void(0)" onclick="lookUp('<?php echo $value['subjectID'];?>','<?php echo $professor;?>')" >
                                        <button class="btn btn-warning warning_1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" title="View" style="font-size: 14px;"><i class="fa fa-spinner"></i>
                                        </button>
                                      </a>
                                      <?php $usertype = $this->session->userdata("usertype");
                                      // print_r($usertype);die; 
                                      if($usertype=="Professor")
                                      {?>
                                      <!-- <a href="<?php echo base_url("syllabus/add_syllabus/".$value['subjectID'].'?type=unit');?>" class="btn btn-success btn-xs mrg for_margR" style="font-size: 14px;">Add Syllabus</a> -->

                                      <a href="<?php echo base_url("syllabus/add_syllabus/".$value['subjectID'].'?type=unit');?>">
                                        
                                        <button class="btn btn-primary primary_2" 
                                        data-toggle="modal" data-target="#myModal" title="Add Syllabus" data-backdrop="static" data-keyboard="false" style="font-size: 14px;">
                                        <i class="fa fa-plus"></i>
                                      </button>
                                      </a>

                                    <?php }?>
                                    </td>
                                    


                                    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

                                      

                                    </div> 

                                    <script type="application/javascript">

                                      function lookUp(id,professor) {

                                      var id = id;

                                      var professor= professor;

                                      // alert(professor);die;

                                      $.post('<?php echo base_url();?>AjaxController/get_unitandtopic',

                                      {

                                      id: id,

                                      professor: professor

                                      }, function (data)

                                      {

                                      

                                        $("#myModal").html(data);

                                      });

                                      }

                                    </script> 

                                   </tr>                      

                              <?php $count++; }?>

                           </tbody>

                        </table>

                        



                     </div>

                  </div>

         </div>

      </div>

   </div>

                  <script type="text/javascript">

                     function idsd(){

                      var atd = $("#date_atd").val();

                      // var topicid=$("#topic").val();

                           var check = true;

                           $("input:radio").each(function(){

                               var name = $(this).attr("name");

                               if($("input:radio[name="+name+"]:checked").length == 0){

                                   check = false;

                                   $("#"+name).html('Please Select');

                               }

                           });

                           

                           if(check){

                               if(atd=='')

                               {

                                  alert('Please select attendance date.');

                                check = false;

                               }

                               

                               return check;

                           }else{

                               

                             return check;

                           }

                      

                     

                     }

                  </script>

               </div>

               <!-- col-sm-12 -->

            </div>

            <!-- row -->

         </div>

         <!-- Body -->

      </div>

      <!-- /.box -->

   </div>

<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classesID').val();

   

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function OnchageDateAtd(date){

   var course =  "<?php echo $this->input->get('course') ?>";

   var subcourse =  "<?php echo $this->input->get('subcourse') ?>";

   var yearsOrSemester =  "<?php echo $this->input->get('yearsOrSemester') ?>";

   var subject =  "<?php echo $this->input->get('subject') ?>";

   var type =  "<?php echo $this->input->get('type') ?>";

   var url = "<?php echo base_url() ?>viewlecture/attendance?type="+type+"&course="+course+"&subcourse=0&yearsOrSemester="+yearsOrSemester+"&subject="+subject+"&date="+date;

   window.location.replace(url);

   

   }

   

</script>

<script>

   var status = '';

   

   

   

   var id = 0;

   

   

   

   $('.onoffswitch-small-checkbox').click(function() {

   

   

   

       if($(this).prop('checked')) {

   

   

   

           status = 'chacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       } else {

   

   

   

           status = 'unchacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       }

   

   

   

   

   

   

   

       if((status != '' || status != null) && (id !='')) {

   

   

   

           $.ajax({

   

   

   

               type: 'POST',

   

   

   

               url: "<?=base_url('professor/active')?>",

   

   

   

               data: "id=" + id + "&status=" + status,

   

   

   

               dataType: "html",

   

   

   

               success: function(data) {

   

   

   

                   if(data == 'Success') {

   

   

   

                       toastr["success"]("Success")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   } else {

   

   

   

                       toastr["error"]("Error")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   }

   

   

   

               }

           });
       }
   }); 
</script>


  
<!-- <script type="text/javascript">
    function boxDisable(e) {
        
            if($(this).is(":checked"))
            {
              $('#update').show();
                // alert("Checkbox is checked.");
            }
            else if($(this).is(":not(:checked)"))
            {
                $('#update').hide();
                alert("please select atleast one checkbox!!");
            }
        
    }
</script> -->
<!-- <script type="text/javascript">
  $("#updatedataprogress").submit(function()
  {
    // alert("cdnjd");
    if ($('input:checkbox').filter(':checked').length < 1)
    {
        alert("Check at least one Game!");
        return false;
    }
  });
</script> -->









