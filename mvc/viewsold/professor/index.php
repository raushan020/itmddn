

<div class="">

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="ti ti-id-badge"></i> <?=$this->lang->line('panel_title')?> </h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">

                       <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                      <li class="active"><?=$this->lang->line('panel_title')?></li>
                        </ol>
                    </div>
            </div>
    <!-- form start -->

    <div class="container-fluid">
        <div class="row">
        <div x>
            <div class="card-body">
            <div class="col-sm-12">

              
                <div class="col-sm-6 nopading">

                <div class="advance-tab boredr_none">

                        <ul class="nav nav-tabs" role="tablist">

                          <?php 

                            if (empty($this->session->userdata('DraftProfessor')) && empty($this->session->userdata('TrashProfessor'))) {

                                $active = "active";

                            }else{

                                $active = "";

                            }

                          ?>

                          

                       <li class="<?php echo $active ?>">

                          <a aria-expanded="true" onclick="ActiveProfessor()">Active <span style="font-size: 11px;">(<?php echo $ActiveProfessor_count; ?>)</span></a>

                       </li>

                       <li <?php if ($this->session->userdata('DraftProfessor')){ ?> class="active" <?php  } ?>>

                          <a  aria-expanded="false" onclick="DraftProfessor()">Draft <span style="font-size: 11px;">(<?php echo $DraftProfessor_count; ?>)</span></a>

                       </li>

                      <li <?php if ($this->session->userdata('TrashProfessor')){ ?> class="active" <?php  } ?>>
                          <a  aria-expanded="false" onclick="TrashProfessor()">Trash <span style="font-size: 11px;">(<?php echo $TrashProfessor_count; ?>)</span></a>
                        </li>

                       

                    </ul>



                </div>


              </div>
              <div class="col-sm-6">
          <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "HOD"){
            ?>
            <form action="<?php echo base_url() ?>student/multipleAction" method = "post">
             
              <!-- <input type="button" class="btn btn-warning etsfilertButton disabled" name="sendmailbyadmin" data-toggle="modal" data-target="#myModal" value="Send Mail" disabled> -->


   <div class="pull-right">
      <div class="btn-group">
   <a href="<?php echo base_url('professor/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
   
      </div>
   </div>
   <?php  }  ?>
              </div>
              <div class="clearfix"></div>

                <div id="hide-table">

                    <table id="professor" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                        <thead>

                            <tr>
                                
                                <!-- <th class="col-sm-2"><input name="select_all" type="checkbox" id="select_all" class="checkmark"></th> -->

                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                <!-- <th class="col-sm-2"><?=$this->lang->line('professor_photo')?></th> -->

                                <th class="col-sm-2">Professor Name</th>

                                 <th class="col-sm-2">Username</th>

                                 <th class="col-sm-2">Designation </th>

                                 <th class="col-sm-2">Department</th>

                                <th class="col-sm-2"><?=$this->lang->line('professor_email')?></th>
                                <?php if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == "HOD")
                                {?>
                                  <th class="col-sm-2">Progress</th>
                                  <?php } else {?>
                                    <th class="col-sm-2"></th>
                                  <?php }?>
                                  <!-- <?php if($usertype == 'Admin'){ ?>

                                  <th class="col-sm-2"><?=$this->lang->line('professor_status')?></th>

                                  <?php } ?> -->

                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>

                            </tr>

                        </thead>
                        
                    </table>
                    
                      <!-- Modal Start-->
                            <!-- <div class="modal fade" id="myModal" role="dialog">
                              <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Custom Mail</h4>
                                  </div>
                                  
                                  <div class="modal-body">
                                    <table>
                                      <tr>
                                        <th>Subject</th>
                                        <td><input type="text" name="subjectname" id="subjectname"></td>
                                      </tr>
                                      <tr>
                                        <th>Write Mail</th>
                                        <td><input type="text" name="writemail" id="writemail"></td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="modal-footer"> -->
                                    <!-- <input type="hidden" name="checked_id[]" value="<?php echo $post->studentID;?>"> -->
                                   <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" class="btn btn-default etsfilertButton disabled" name="sendmailbyadmin" value="Send Mail">
                                  </div>
                                  
                                </div>
                              </div>
                            </div>
                          </div> -->
                          <!-- MOdel End -->

                        <div class="dataTables_scrollBody" style="position: relative;overflow: auto;width: 100%;"></div>
                </div>




            </div> <!-- col-sm-12 -->

        </div><!-- row -->
      </form>

    </div><!-- Body -->

</div><!-- /.box -->
</div>
</div>



<script type="text/javascript">
  function sendMail(){
    
  }

</script>
