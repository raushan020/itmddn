    <div class="">

        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="ti ti-id-badge"></i> <?=$this->lang->line('panel_title')?> </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                    <li><a href="<?=base_url("professor/index")?>"><?=$this->lang->line('panel_title')?></a></li>

                    <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('panel_title')?></li>
                </ol>
            </div>
        </div>


    <!-- form start -->

    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>

        <div class="row">
            <div class="card">
            <div class="card-body">
             <div class="col-sm-12">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

                   

                    <?php 

                        if(form_error('name')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="professor_name" class="col-sm-2 control-label">

                            Name

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $professor->name)?>" >



                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>


                    <?php 

                        if(form_error('shortcode')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="shortcode" class="col-sm-2 control-label">

                            Short Code of Professor Name <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="shortcode" name="shortcode" value="<?=set_value('shortcode', $professor->shortCodeofProfessor)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('shortcode'); ?>

                        </span>

                    </div>


                     <?php 

                                if(form_error('dob')) 

                                    echo "<div class='form-group has-error' >";

                                else 

                                    echo "<div class='form-group' >";

                            ?>

                            <label for="dob" class="col-sm-2 control-label">

                                Date of Birth <span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control datepicker_quiz_data" required placeholder="Date of Birth"  name="dob" value="<?=set_value('dob',date("d-m-Y", strtotime($professor->dob)))?>" >

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('dob'); ?>

                            </span>

                        </div>


                    <?php 

                        if(form_error('departmentID')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="department" class="col-sm-2 control-label">

                            Department Name<span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                          <select class="form-control" name="departmentID">
                              <option>Select</option>
                              <?php foreach ($department as $key => $value) {


                               ?>
                               <option value="<?php echo $value->departmentID ?>" <?php if($professor->departmentID==$value->departmentID){echo "Selected";} ?>><?php echo $value->department_name ?></option>                         
                              <?php } ?>
                          </select>  


                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('departmentID'); ?>

                        </span>

                    </div>

                    <?php 

                        if(form_error('designationID')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="designationID" class="col-sm-2 control-label">

                            Designation Name<span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">
                        <select class="form-control" name="designationID">
                              <option>Select</option>
                              <?php foreach ($designation as $key => $value) { ?>
                               <option value="<?php echo $value->designationID ?>" <?php if($professor->designationID==$value->designationID){echo "Selected";} ?>><?php echo $value->designation_name ?></option>                         
                              <?php } ?>
                          </select>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('designationID'); ?>

                        </span>

                    </div>
    

                    <?php 

                        if(form_error('sex')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("professor_sex")?>

                        </label>

                        <div class="col-sm-6">

                            <?php 

                                echo form_dropdown("sex", array($this->lang->line('professor_sex_male') => $this->lang->line('professor_sex_male'), $this->lang->line('professor_sex_female') => $this->lang->line('professor_sex_female')), set_value("sex", $professor->sex), "id='sex' class='form-control'"); 

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('sex'); ?>

                        </span>

                    </div>



                    <!-- <?php 

                        if(form_error('religion')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="religion" class="col-sm-2 control-label">

                            <?=$this->lang->line("teacher_religion")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="religion" name="religion" value="<?=set_value('religion', $teacher->religion)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('religion'); ?>

                        </span>

                    </div> -->



                    <?php 

                        if(form_error('email')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("professor_email")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $professor->email)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('phone')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("professor_phone")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $professor->phone)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>


                    <?php 

                        if(form_error('username')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="username" class="col-sm-2 control-label">

                           <?=$this->lang->line("professor_username")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="username" name="username" value="<?=set_value('username', $professor->username)?>" >

                        </div>
                         

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('username'); ?>

                        </span>

                    </div>




                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("update_professor")?>" >

                        </div>

                    </div>



                </form>

            </div><!-- col-sm-8 --> 

        </div>

    </div>

</div>
</div>
</div>



<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });

$('#jod').datepicker();

</script>
<script type="text/javascript">
    function goBack() {
  window.history.back();
  console.log('We are in previous page');
}

function goForward() {
  window.history.forward();
  console.log('We are in next page');
}
  </script>

