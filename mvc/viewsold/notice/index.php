<div class="">

   <style type="text/css">
       .dataTables_scroll
{
    overflow:auto !important;
}
   </style>

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Notice</li>

            </ol>

        </div>

    </div>



<!-- /.box-header -->



    <!-- form start -->

    <div class="container-fluid">

        <div class="row">

            <div class="">

            <div class="card">

            <div class="card-body">



            <div class="box-body">



                <div class="row">



                    <div class="col-sm-12">

                        <?php



                            $usertype = $this->session->userdata("usertype");



                            if( $usertype == "ClgAdmin" || $usertype == "Admin") {



                        ?>



                        <div class="col-sm-6"></div>

                        <div class="col-sm-6">

                               <div class="pull-right">

                              <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>



                              </div>

                           </div>

                        </div>

                        <div class="clearfix"></div>


        <div class="col-sm-12">



            <div class="theme_input_blue">

            <?php 

               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {

               $display =  "block";

               $addmorebutton = "none";

               $Removemorebutton = "inline-block";

               }else{

               $display = "none";

               $addmorebutton = "inline-block";

               $Removemorebutton = "none";

               }

               

               ?>

                

            </div>

            <div class="clearfix"></div>

         

            <?php }  ?>                      



                                

                <?php  if( $usertype == "ClgAdmin" || $usertype == "Admin"){?>

<!-- <div class="action-layout">

                    <ul>

                        <li>

                            <label class="nexCheckbox">Check

                                <input type="checkbox" name="select_all" id="select_all">

                                <span class="checkmark checkmark-action-layout"></span>

                            </label>

                        </li>

                        <li class="active-btn">

                            <a class="btn btn-success etsfilertButton"  data-toggle="modal" data-target="#myModal">Send mail</a>

                        </li>

                      

                    </ul>

                </div>   -->

                        <div id="hide-tab">



                            <table id = "noticeTables" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>
                                    

                                        <th><?=$this->lang->line('slno')?></th>



                                        <th><?=$this->lang->line('notice_title')?></th>



                                        <th><?=$this->lang->line('notice_date')?></th>



                                       



                                        <th>Notice</th>

                                        

                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>







                        </div>

                    <?php  } ?>







         <!--   <?php  if ( $usertype == "Admin") { ?>



                          <div class="col-sm-12">                               

                               <div class="pull-right">

                                 <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                                  </div>                            

                                </div>

                            </div>

                            

                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th ><?=$this->lang->line('slno')?></th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                         <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>

                                      <th class="col-sm-4">Notice</th>

                                       <th class="col-sm-4">Action</th>  

                                          </tr>



                                </thead>                                                      

                                    <tdata>

                                    <?php if(count($notices)) {$i = 1; foreach($notices as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>

                                        <td><?php echo $notices['title']; ?></td>

                                       

                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>

                                        <td><?php echo substr($notices['notice'],0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?></td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div> <?php } ?>

                    

                       



 -->





 <?php if ( $usertype == "Professor") { ?>



                            <div class="col-sm-12">                               

                               <div class="pull-right">

                                 <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                                  </div>                            

                                </div>

                            </div>

                            

                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                      



                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>



                                        <th class="col-sm-4">Notice</th>

                                        

                                        <th class="col-sm-4">Action</th>

                                       

                                    </tr>



                                </thead>

                             

                                <tdata>

                                    <?php if(count($notices)) {$i = 1; foreach($notices as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>

                                        <td><?php echo $notices['title']; ?></td>

                                        

                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>

                                        <td><?php echo substr(strip_tags($notices['notice']),0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?>

                                        	<?php echo btn_delete('notice/delete/'.$notices['noticeID'], $this->lang->line('delete')); ?>
                                        </td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div>  <?php } ?>

                    

                     









   <?php  if ( $usertype == "Accountant") { ?>



                            <div class="col-sm-12">                               

                               <div class="pull-right">

                                 <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                                  </div>                            

                                </div>

                            </div>





                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>                                      



                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>



                                        <th class="col-sm-4">Notice</th>

                                        

                                        <th class="col-sm-4">Action</th>

                                       

                                    </tr>



                                </thead>

                             

                                <tdata>

                                    <?php if(count($notices)) {$i = 1; foreach($notices as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>

                                        <td><?php echo $notices['title']; ?></td>

                                        

                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>

                                        <td><?php echo substr(strip_tags($notices['notice']),0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?></td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div> <?php } ?>

                    

                       





 

 



                        



  <?php  if ( $usertype == "Student") { ?>

                            

                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                         <th>Sender Name</th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>



                                        <th class="col-sm-4">Notice</th>

                                        

                                        <th class="col-sm-4">Action</th>

                                       

                                    </tr>



                                </thead>

                             

                                <tdata>

                                    <?php if(count($notice)) {$i = 1; foreach($notice as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>

                                        <td><?php echo $notices['title']; ?></td>

                                         <td><?php echo $notices['name']; ?></td>

                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>

                                        <td><?php echo substr(strip_tags($notices['notice']),0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?></td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div>

                    

                       <?php } ?>

              











                    </div>

               </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>

<!-- <script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script> -->

<!-- <script type="text/javascript">

  $('#noticeTables').DataTable({

   columnDefs: [ { type: 'date', 'targets': 5] } ],

   order: [[ 5, 'desc' ]],          

});

</script> -->





<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('notice/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });



</script>



<script type="text/javascript">



    $('#subCourseID').change(function() {

        $('#noticeTables').DataTable().state.clear();



        var subCourseID = $(this).val();



            $.ajax({

                type: 'POST',

                url: "<?=base_url('notice/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }



            });



    });



</script>



<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

        $('#noticeTables').DataTable().state.clear();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('notice/index')?>",

            data: "yearSemesterID=" + yearSemesterID,

            dataType: "html",

            success: function(data) {

                 location.reload();

            }

        });

    });

</script>



<script type="text/javascript">

    

    function ResetCourses(){

        $('#noticeTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetCourses')?>",



                data:{ResetCourses:'ResetCourses'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

}





    function ResetSubcourses(){

        $('#noticeTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetSubcourses')?>",



                data:{ResetSubcourses:'ResetSubcourses'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

}

    function ResetSemesterYear(){

        $('#noticeTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetSemesterYear')?>",



                data:{ResetSesession:'ResetSesession'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

} 



 function ResetAllfilter(){

    $('#noticeTables').DataTable().state.clear();



            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetAllfilter')?>",



                data:{ResetSesession:'ResetSesession'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

}



</script>

