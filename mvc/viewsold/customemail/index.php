<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-cog" aria-hidden="true"></i> Custom Email </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"> Custom Email</li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
   <!-- <div class="form-row">
      <div class="form-group">
          <label for="courseName" class="col-md-2 col-form-label">Type</label>
           <div class="col-md-10">
         <div class="col-sm-3">
            <input type="radio" name="cod" id="chde1" value="1"><label for="chde1">Sem</label>
         </div>
         <div class="col-sm-3">
         <input type="radio" name="cod" value="0" id="chde2" checked><label for="chde2">Year</label>
         </div>
         <div class="clearfix"></div>
           </div>
      </div>
             </div> -->
   <!-- form start -->
   <div class="container-fluid">
    <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
         <div class="">
            <div class="card">
               <div class="card-body">
                  <div class="col-sm-12">
                     <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                        <?php if(form_error('classesID[]')){ ?>
                        <div class="alert alert-danger">
                           <strong>Danger!</strong><?php echo  form_error('classesID[]'); ?>
                        </div>
                        <?php } ?>
                        
                        <div id="subCourseID">
                        </div>
                        <div id="fetchYearsAndSem"  multiple="multiple">
                        </div>
                        <div id="appendSujectsDetails">
                        </div>
                        <?php 
                           if(form_error('title')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                        <label for="title" class="col-sm-2 control-label">
                         Subject
                        </label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" placeholder="Subject" autocomplete="off" name="title" value="<?=set_value('title')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('title'); ?>
                        </span>
                  </div>
                  <?php 
                     if(form_error('date')) 
                     
                         echo "<div class='form-group has-error' >";
                     
                     else     
                     
                         echo "<div class='form-group' >";
                     
                     ?>
                  <label for="date" class="col-sm-2 control-label">
                  <?=$this->lang->line("notice_date")?>
                  </label>
                  <div class="col-sm-6">
                  <input type="" class="form-control datepicker_quiz_data" autocomplete="off" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                  </div>
                  <span class="col-sm-4 control-label">
                  <?php echo form_error('date'); ?>
                  </span>
               </div>
               <?php 
                  if(form_error('notice')) 
                  
                      echo "<div class='form-group has-error' >";
                  
                  else     
                  
                      echo "<div class='form-group' >";
                  
                  ?>
               <label for="notice" class="col-sm-2 control-label">
               Description
               </label>
               <div class="col-sm-6">
               <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
               </div>
               <span class="col-sm-4 control-label">
               <?php echo form_error('notice'); ?>
               </span>
            </div>
            <?php $usertype = $this->session->userdata("usertype"); ?>
            <?php if($usertype != "ClgAdmin")
            {?>
                <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
                <div class="dsd">
                <label class="nexCheckbox">Super Admin
                <input type="checkbox" name="super" value="<?php echo "Super Admin" ?>" id="select_all" >
                <span class="checkmark checkmark-action-layout"></span>
                </label> 
                <?php foreach ($superadmin as $superadmin)
                {?>
                  <input type="hidden" name="superadmin[]" value="<?php echo $superadmin->adminID;?>">
               <?php }   ?>
                </div>
                </div>
           <?php }?>
            <?php if($usertype != "Admin")
            {?>
              <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
              <div class="dsd">
              <label class="nexCheckbox">Admin
              <input type="checkbox" name="admin" value="<?php echo "Super Admin" ?>" id="select_all" >
              <span class="checkmark checkmark-action-layout"></span>
              </label> 
              <?php foreach ($Admin as $Admin)
              {?>
                <input type="hidden" name="admin[]" value="<?php echo $Admin->userID;?>">
             <?php }   ?>
              </div>
              </div>
           <?php } ?>
            
            <?php if($usertype != 'Professor')
            { ?>
              <div class="col-sm-12" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
              <div class="dsd">
              <label class="nexCheckbox">Professor
              <input type="checkbox" name="professor" value="<?php echo "Super Admin" ?>" id="select_all" >
              <span class="checkmark checkmark-action-layout"></span>
              </label>
              <?php $count=count($Professor); foreach($Professor as $Professor)
              {?>
                
                 <input type="checkbox" name="professorid[]" value="<?php echo $Professor->professorID; ?>" id="select_all" ><?php echo $Professor->name; ?> &nbsp;&nbsp;
                 <?php if($count == 5 || $count == 10 || $count == 15 || $count == 20 || $count == 25 || $count == 30 || $count == 35 || $count == 40 || $count == 45 || $count == 50 || $count == 55){ ?> </br>  <?php } ?>
             <?php } ?>
              </div>
              </div>
           <? } ?>
            <?php if($usertype != "Accountant")
            {?>
              <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
              <div class="dsd">
              <label class="nexCheckbox">Accountant
              <input type="checkbox" name="act" value="<?php echo "Super Admin" ?>" id="select_all" >
              <span class="checkmark checkmark-action-layout"></span>
              </label> 
              <?php foreach ($Accountant as $Accountant)
              {?>
                <input type="hidden" name="accountant[]" value="<?php echo $Accountant->userID;?>">
             <?php }   ?>
              </div>
              </div>
           <?php } ?>
            
           
       <div class="col-md-12" >
          <div class="col-md-6 pull-right">
            <input type="submit" class="btn btn-success" value="Send Mail" >
          </div>                               
         </div>
                                                                                
      </div>
      </form>
   </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">
   jq('#date').datepicker();
   
   jq('#notice').jqte();
   
</script>
<script type="text/javascript">
   function selectAll(){
      var items=document.getElementsByName('classesID[]');
      var items_sem=document.getElementsByClassName('yearOrSemester');
   
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=true;
      }
      for(var i=0; i<items_sem.length; i++){
          if(items_sem[i].type=='checkbox')
              items_sem[i].checked=true;
      }
   }
   
   function UnSelectAll(){
      var items=document.getElementsByName('classesID[]');
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=false;
      }
   }           
</script>
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script type="text/javascript">
   tinymce.init({
   
       selector:"textarea",
   
       width:"700",
   
       height:"200",
   
       relative_urls:false,
   
       remove_script_host:false,
   
       theme: "modern",
   
       plugins: [
   
           "advlist autolink lists link image charmap print preview hr anchor pagebreak",
   
           "searchreplace wordcount visualblocks visualchars code fullscreen",
   
           "insertdatetime media nonbreaking save table contextmenu directionality",
   
           "emoticons template paste textcolor colorpicker textpattern"
       ],
   
       toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager | fontsizeselect",
   
       toolbar2: "print preview media | forecolor backcolor emoticons",
   
       fontsize_formats: "1px 2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px 34px 36px",
   
         style_formats: [
   
               {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
   
               {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
   
               {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
   
               {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
   
               {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
   
               {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
   
               {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
   
               {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
   
               {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
   
               {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
   
               {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
   
               {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
   
               {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
   
           ],
   
       image_advtab: true,
   
       templates: [
   
           {title: 'Test template 1', description: 'Test 1'},
   
           {title: 'Test template 2', description: 'Test 2'}
       ],
   
      external_filemanager_path:"<?php echo base_url() ?>filemanager/",
   
      filemanager_title:"Responsive Filemanager" ,
   
      external_plugins: { "filemanager" : "<?php echo base_url() ?>filemanager/plugin.min.js"}
   
   });
   
</script>