<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>SaaS In Admin Data Management,Online Learning,Fee Management,Placement Prediction and more- E-Campus</title>
    <meta name="description" content="E-Campus is a Software-as-a-Service (SaaS) platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges and educational institutions.">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="e campus|digitized solutions for universities|digitized solutions for colleges|digitized solutions for educational institution|University Digitization|Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
 
<meta name="og:title" property="og:title" content="eCampus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<meta property="og:title" content="eCampus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />

<meta property='og:title' content='eCampus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='eCampus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />

<meta property="og:title" content="eCampus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="eCampus"/>

<!-- <link rel="shortcut icon" type="image/png" href="assets/img/icons/favicon.png"> -->

<style>
    .col1,.col2 {
      padding-bottom: 5px!important;
      border-right:1px solid!important;
      border-bottom: 1px solid!important;
    }
    .col4,.col5 {
        border-right:1px solid!important;
    }
    .col3 {
      border-bottom: 1px solid!important;
    }
</style>
<?php include('head.php'); ?>
</head>
<body>
<?php include('header.php'); ?>
    <section class="seo_home_area">
            <div class="home_bubble">
                <div class="bubble b_one"></div>
                <div class="bubble b_two"></div>
                <div class="bubble b_three"></div>
                <div class="bubble b_four"></div>
                <div class="bubble b_five"></div>
                <div class="bubble b_six"></div>
                <div class="triangle b_seven" data-parallax="{&quot;x&quot;: 20, &quot;y&quot;: 150}
                "><img src="assets/img/partner/triangle_one.png" alt="">
                </div>
                <div class="triangle b_eight" data-parallax="{&quot;x&quot;: 120, &quot;y&quot;: -10}" >
                    <img src="assets/img/partner/triangle_two.png" alt="">
                </div>
                <div class="triangle b_nine"><img src="assets/img/partner/triangle_three.png" alt=""></div>
            </div>
            <div class="banner_top" style="padding-top: 150px !important;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center seo_banner_content">
                            <div class="hero-text typewriter">
                            <h1 class="for">Unlock Power of Automation</h1>
                        </div>
                         <h2 class="cd-headline loading-bar">
                                <span style="">In</span>

                                <span class="cd-words-wrapper wrap changing-text" >
                                    <b class="is-visible">Campus Management</b>
                                    <b>Admin Data Management</b>
                                    <b>Online Learning</b>
                                    <b>Fee Management</b>
                                    <b>Placement Prediction</b>
                                </span>
                            </h2><br>
                            <p class="">
                                A technically sound software for your college & university’s administrative<br>requirements and equally focussed towards student's e-learning.
                                </p><br>
                                
                                	 <a href="#" id="demolink" class=" btn_hover_call seo_btn seo_btn_one btn_hover goto" data-toggle="modal" data-target="#demoreq">
                                <i class="fa fa-desktop"></i>
                               Request A Call</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 offset-md-3 text-center">
                            <div class="saas_home_img wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
                                <img src="assets/img/partner/home-ecampus.png" alt="" style="width:100%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



<section class="innerpage-area">
        <div class="container">
            <div class="services-content">
                <div class="row">
                    <div class="col-md-12">
                    	<div class="partner-main">
                    		<h1>Trusted by over 10,000 students across 10+ colleges & universities</h1>
                    	</div>
                    </div>
                    <div class="col-md-2">
                        <div class="partner-services">
                           
                            <div class="s-single-text">
                                
                                 <img src="assets/img/partner/new_jnu.png" alt="jaipur national University" class="image-responsive">
                            </div>
                        </div>
                    </div>
                 
                     <div class="col-md-2">
                        <div class="partner-services">
                           
                            <div class="s-single-text">
                                
                                 <img src="assets/img/partner/new_mangalayatan.png" class="image-responsive" alt="mangalayatan university">
                            </div>
                        </div>
                    </div>
                     <div class="col-md-2">
                        <div class="partner-services">
                            
                            <div class="s-single-text">
                                
                                 <img src="assets/img/partner/new_itm.png" class="image-responsive" alt="itmddn" >
                            </div>
                        </div>
                    </div>
                  <div class="col-md-2">
                        <div class="partner-services">
                           
                            <div class="s-single-text">
                                
                                 <img src="assets/img/partner/new_subharti.png" class="image-responsive" alt="swami vivekanand subharti university">
                            </div>
                        </div>
                    </div>
                      <div class="col-md-2">
                        <div class="partner-services">
                          
                            <div class="s-single-text">
                                
                                 <img src="assets/img/partner/new_Lingyas.png" alt="lingayas vidyapeeth" class="image-responsive" style="height: 45px;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="partner-services">
                            
                            <div class="s-single-text">
                                
                                 <img src="assets/img/partner/new_shobit.png" class="image-responsive" alt="shobhit university">
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div><br><hr>
    </section>
    <!-- Features Area -->
    <!-- new -->
    <section class="details-about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="made-easy-tab-content">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">
                      <div class="section-img met-img ssp-img" style="width: 700px">
                                            <img src="assets/img/partner/Dashbord_1.png"  alt="" >
                                        </div>
                                         
                                    </div>
                                </div> 
                            </div>

                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">

                      <div class="section-img met-img" style="width: 700px">
                                            <img src="assets/img/partner/Timetable_new.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">
                                       
                        <div class="section-img met-img" style="width: 700px">
                                            <img src="assets/img/partner/Fee_new.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="tab-pane" id="tabs-4" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">
        
                       <div class="section-img met-img" style="width: 700px">
                                            <img src="assets/img/partner/Lms_new.png" alt="">
                                        </div>
                                    </div
>                                </div>
                            </div>
                             <div class="tab-pane" id="tabs-5" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">
                        <div class="section-img met-img" style="width: 700px">
                                            <img src="assets/img/partner/E_pub_new.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="tab-pane" id="tabs-6" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">
                        <div class="section-img met-img" style="width: 700px">
                                            <img src="assets/img/partner/online_assessmment.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4">
                    	<div class="letssee"><h2>Let's see how it works</h2></div><br> 
                        <h5 style="font-weight: 700;color: #909090;">UNIVERSITY MANAGEMENT</h5>
                    <div class="made-easy-tab-btn">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab" title="Student Details"><span><i class="fa fa-info"></i></span> &nbsp;&nbsp;&nbsp;<span style="border-bottom: 1px solid #909090;">Student Details</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab" title="Time table"><span><i class="fa fa-calendar"></i></span> &nbsp;&nbsp;&nbsp;<span style="border-bottom: 1px solid #909090;">Time table</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab" title="Fee & Account"><span><i class="fa fa-money"></i></span>&nbsp;&nbsp;&nbsp;<span style="border-bottom: 1px solid #909090;">Fee & Account</span></a>
                            </li>
                        </ul>
                    
                    </div>
                        <h5 style="font-weight: 700;color: #909090;">LEARNING MANAGEMENT</h5>
                     <div class="made-easy-tab-btn">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab" title="Lms"><span><i class="fa fa-book"></i></span> &nbsp;&nbsp;&nbsp;<span style="border-bottom: 1px solid #909090;">Lms</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-5" role="tab" title="Online Reader"><span><i class="fa fa-laptop"></i> &nbsp;&nbsp;&nbsp;<span style="border-bottom: 1px solid #909090;">Online Reader</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-6" role="tab" title="Online Assessment"><span><i class="fa fa-asterisk"></i> &nbsp;&nbsp;&nbsp;<span style="border-bottom: 1px solid #909090;">Online Assessment</span></a>
                            </li>
                        </ul>
                    </div>
                    <div><br>
                        	<a class="" href="#" title="product tour" style="font-size: 14px;border-bottom: 1px solid #000;padding-bottom: 3px;">Take me to Full Product Tour </a> <i class="fa fa-arrow-right" style="color: 
                        	#5e2ced;"></i>
                        </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end new -->

<section class="features-area section-padding"  style="background-image: url(assets/img/partner/bg.jpg);">
	<div class="overlay_1"></div>

        <div class="container">
        
            <div class="row">
                <div class="col-md-12">
                     <div class="f-single-text">

                        <h2 style="color: #fff;text-align: center;margin-bottom: 15px;">Why Shift to e campus ?</h2>

                        </div>
                </div>
                   <div class="col-md-4 col1">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-team"></i>

                        </div>

                        <div class="f-single-text">

                        <h4>One-stop campus solution</h4>

                            <p class="p_feature">50+ modules that include everything you will ever need to run an educational institution.</p>

                        </div>

                    </div>

                </div>
                   <div class="col-md-4 col2">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-computer-graphic"></i>

                        </div>

                        <div class="f-single-text">

                        <h4>Get-set-go</h4>

                            <p class="p_feature">Get your account activated within minutes. Set up and begin your journey with the most advanced campus automation software.</p>

                        </div>

                    </div>

                </div>
                   <div class="col-md-4 col3">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-brochure"></i>

                        </div>

                        <div class="f-single-text">

                         <h4>Security & Lightning speed</h4>

                            <p class="p_feature">Having advanced features, e campus is secure and fast in all aspects which is the top concern of every business.</p>

                        </div>

                    </div>

                </div>
                <div class="col-md-4 col1">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-team"></i>

                        </div>

                        <div class="f-single-text">

                        <h4>Learning Management System</h4>

                            <p class="p_feature">Delivers educational content and online course material to the students electronically.</p>

                        </div>

                    </div>

                </div>

                <div class="col-md-4 col2">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-computer-graphic"></i>

                        </div>

                        <div class="f-single-text">

                        <h4>Student Management System</h4>

                            <p class="p_feature">Assists your students throughout their journey from pre-admission formalities to being a part of the alumni band.</p>

                        </div>

                    </div>

                </div>
                <div class="col-md-4 col3">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-brochure"></i>

                        </div>

                        <div class="f-single-text">

                         <h4>EXAMINATION MANAGEMENT SYSTEM</h4>

                            <p class="p_feature">Streamline assessment planning for the student helping them judge their own progress in an orderly manner.</p>

                        </div>

                    </div>

                </div><hr>
                
                <div class="col-md-4 col4">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-update"></i>

                        </div>

                        <div class="f-single-text">

                           <h4>highly configurable</h4>

                            <p class="p_feature">Completely customisable dashboards to suit your needs as per your institution's area of focus and requirements.</p>

                        </div>

                    </div>

                </div>

                <div class="col-md-4 col5">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-sketch"></i>

                        </div>

                        <div class="f-single-text">

                            <h4>flexible pricing</h4>

                            <p class="p_feature">Pay-as-you-go pricing model to keep your spends minimum and transparent. No lock-in commitment required.</p>

                        </div>

                    </div>

                </div>
                 <div class="col-md-4">

                    <div class="features-single card_feture product-cards">

                        <div class="f-single-icon">

                            <i class="flaticon-support"></i>

                        </div>

                        <div class="f-single-text">

                            <h4>Lifetime Support</h4>

                            <p class="p_feature">The dedicated team works in accordance round the clock to provide 24 X 7 support with their technical expertise.</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <section class="innerpage-area section-padding" style="background-image: url(assets/img/partner/main_portal.png);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-title" style="line-height: line-height: 37px;">Everything you need to make BETTER & FASTER decisions.</h2><br>
                        <img src="assets/img/partner/Lapi_screen_portal_1.png" class="img-responsive">
                </div>
            </div>
        </div>
    </section>
    <section class=" section-padding">
        
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="today" style="text-align: center;">
            		<h1>Get started with e campus today</h1>
            	</div>
                <div class="section-btn btnb_center">
                    <a href="" title="" data-toggle="modal" data-target="#signinmodal"><button class="button_new">Start For Free
                    <div class="button__horizontal"></div>
                    <div class="button__vertical"></div>
                    </button></a>

                </div>
 
            </div>
            
        </div>
        
    </div>
    </section>

    <?php include('footer.php'); ?> 


<script>
var TxtType = function(el, toRotate, period) {

        this.toRotate = toRotate;

        this.el = el;

        this.loopNum = 0;

        this.period = parseInt(period, 10) || 1000;

        this.txt = '';

        this.tick();

        this.isDeleting = false;

    };

    TxtType.prototype.tick = function() {

        var i = this.loopNum % this.toRotate.length;

        var fullTxt = this.toRotate[i];



        if (this.isDeleting) {

        this.txt = fullTxt.substring(0, this.txt.length - 1);

        } else {

        this.txt = fullTxt.substring(0, this.txt.length + 1);

        }



        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';



        var that = this;

        var delta = 200 - Math.random() * 100;



        if (this.isDeleting) { delta /= 2; }



        if (!this.isDeleting && this.txt === fullTxt) {

        delta = this.period;

        this.isDeleting = true;

        } else if (this.isDeleting && this.txt === '') {

        this.isDeleting = false;

        this.loopNum++;

        delta = 500;

        }



        setTimeout(function() {

        that.tick();

        }, delta);

    };



    window.onload = function() {

        var elements = document.getElementsByClassName('typewrite');

        for (var i=0; i<elements.length; i++) {

            var toRotate = elements[i].getAttribute('data-type');

            var period = elements[i].getAttribute('data-period');

            if (toRotate) {

              new TxtType(elements[i], JSON.parse(toRotate), period);

            }

        }

        var css = document.createElement("style");

        css.type = "text/css";

        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";

        document.body.appendChild(css);

    };

</script>
</body>
</html>