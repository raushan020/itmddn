<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>e campus|University Management System|LMS|Smart Classes|E-learning|2020</title>
    <meta name="description" content="E-Campus is a Software-as-a-Service (SaaS) platform that  provides various services to institutions for managing all Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions on the cloud for universities, colleges and educational institutions |2020 ">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="E-Campus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="E-Campus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='E-Campus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='E-Campus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="E-Campus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>

</head>
<<body>
<?php include('header.php'); ?>
<section class="pricing-area" style="margin-top: 89px;background: #0c92f3;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title1">
                        <h3>Kindly read the Terms & Conditions carefully before availing any of the services provided by e campus.
                        </h3>
                        

                            <p>By accessing and using this service, you accept and agree to be bound by the terms and provision of this agreement. In addition, when using these services, you shall be subject to any posted guidelines or rules applicable to such services. </p>
                            <p>Any participation in this service will constitute acceptance of this agreement. If you do not agree to abide by the above, please do not use the services we avail.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section class="about-area3 section-padding" style="background: #f1f4f9;">

        <div class="container">

            <div class="row">
                

                        <div class="">
                        	<ol>
    <li><strong>DEFINITIONS</strong>
        <ol>
            <li><strong>“e campus”</strong>, “we”, “us”, or “our” shall mean:
                <ol>
                    <li>
                        In case you are a Customer based in India, Edge Technosoft Pvt. Ltd.,a company incorporated under the Companies Act, 1956, with its registered office at G -75-B, Ground Floor, Abul Fazal Enclave-II, Shaheen Bagh, Jamia Nagar, Okhla New Delhi South Delhi-110025.
                    </li>

                    <li>
                        in case you are a Customer based in any other jurisdiction, Edge Technosoft Pvt. Ltd., an Indian corporation with its registered office at 
                    </li>
                </ol>
                </li>

              <li>“You“, “your” or “Customer” shall mean an individual or legal entity who is signing up for any kind of Services from us, irrespective of the nature or duration of the Services, including those availing of Free Services. Customer’s details, including name of the contracting entity and the authorized representative, are as provided in the accompanying Purchase Order.</li>

              <li>“Billing Cycle” shall mean a duration cycle for which billing is done in one go, as indicated in the Purchase Order;</li>

              <li>“Consulting Services” shall mean the professional services provided by us, which may include training services, installation, integration or consulting services. The details of the Consulting Services shall be set out in the Purchase Order signed up from time to time under these Terms of Service;</li>

              <li>“Customer Data” shall mean information pertaining to your clients that you submit or collect via the Subscription Services. Additional information that may be collated by us and provided for your use will not be included within the scope of Customer Data;</li>

              <li>“Effective Date” shall mean the date of your acceptance to these Terms of Service;</li>

              <li>“Force Majeure” shall mean an act of war, hostility, sabotage, act of God, electrical, internet, or telecommunication outage, cyber-attacks, government or regulatory restrictions (including the denial or cancellation of any export or other license), or any other event outside the reasonable control of the obligated Party;</li>

              <li>“Free Services” shall mean any products or features, including Subscription Services made available by us to you on an unpaid trial or free basis;</li>

              <li>“Order” or “Purchase Order” shall mean the form submitted by you, with your details and the Services opted by you, with relevant Service terms, pricing and payment terms being set out accordingly. Separate Purchase Orders may be submitted for different Subscription Services and Purchase Orders may be updated or modified from time to time with mutual consent;</li>

              <li>“Party” shall mean either e campus or Customer and “Parties” shall mean e campus and Customer collectively;</li>

              <li>“Planned Downtime” shall mean the period during which the Services may be shut down for planned maintenance of the Platform. To the extent possible and reasonable, such downtime will be scheduled during non-business hours for majority of our customers such as weekends and public holidays and at least 24 (twenty-four ) hours’ prior notice will be provided;</li>

              <li>“Sensitive Information” shall mean passwords, financial information such as bank account or credit card or debit card or other payment instrument details, passport numbers, driver’s license numbers, Aadhar numbers or similar identifiers, information pertaining to racial or ethnic origin, political opinions, religious or philosophical beliefs, or trade union membership, physical, physiological or mental health condition or information, medical records and history, sexual orientation, genetic data, biometric information, or other employment, financial or health information, including any information subject to regulations, laws or industry standsards designed to protect data privacy and security;</li>

              <li>“Service Fees” shall mean the amounts you are required to pay for using any of the Services;</li>

              <li>“Services” shall mean any service provided by us to you, including but not limited to Subscription Services, Consulting Services and Free Services;</li>

              <li>“Start Date” shall mean the date of commencement of the Subscription Services;</li>

              <li>“Subscription Fees” shall mean the fees payable by you for the Subscription Services;</li>

              <li>“Subscription Services” shall mean all of e campus’ web and mobile based marketing and sales applications, tools and platforms that you have subscribed to and are developed, operated and maintained by us, accessible via e campus.inor another designated URL, and any ancillary products and services that we provide to you. The details of the Subscription Services shall be as set out in the Purchase Order;</li>

              <li>“Subscription Term” shall mean the initial term for the subscription to the applicable Subscription Services, as specified in the relevant Purchase Order, and each subsequent renewal term (if any). For Free Services, the Subscription Term will be the period during which you have an account to access the Free Services;</li>

              <li>“Terms of Service” shall mean this Terms of Service entered into between e campus and you in respect of the Services, along with any modifications that may be notified from time to time; and</li>

              <li>“Users” mean individual people or accounts that are designated and authorized by you to access Subscription Services.</li>

        </ol>
    </li>

    <!-- 2nd number start -->
    <li><strong>SCOPE OF SERVICES</strong>
        <ol>
            <li><strong>Subscription Services</strong><br>
            	e campus have devised a cloud-basedSoftware-as-a-Service (SaaS) platform to maintain the record and ease all the processes of an educational institution, from the admission of new students to graduation and thereafter. Moreover, the platform is used to organize timetables that may deal in classroom and examination schedules, teacher and student attendance, examination details, hostel information, library information, transportation arranged by the Institution, events and other Institutional activities  and any other services provided by us from time to time. e campus has also extended its product line with Webcam Assessment module for the universities to conduct regular online assessments which requires the availability of webcam and microphone with the students. ThisSaaS-based model provides an option to stay updated regarding the examination, attendance etc. by SMS, e-mails and other modes adopted by e campus from time to time.<br><br>
				Licenses are available for different types of Subscription, i.e., Subscription with messaging and Subscription without messaging service. The Services shall be provided to the Licensee in accordance with the option opted by it in the Subscription Agreement.<br><br>

				During the Subscription Period, we will provide you access to use the Subscription Services in accordance with these Terms of Service and the relevant Purchase Order. You may, at any time, subscribe to additional features of the Subscription Services (existing features or new features that may be made available by us from time to time) by executing an additional Purchase Order. We may update the Platform from time to time, without affecting the Subscription Services. We, however, are under no binding obligation to release new features or updates to the Platform. We make no representations as to future features and functionalities, irrespective of any public announcements or comments in this regard.<br><br>
				Subscription Services will be made available 24 hours a day, 7 days a week, except for Planned Downtime or Force Majeure.<br><br>

            </li>

              <li><strong>Consulting Services</strong><br>Consulting Services will be provided by us in accordance with the relevant Purchase Order. Unless otherwise agreed, Consulting Services will be performed remotely and rendered in English.</li><br>

              <li>Third Party Service Providers or Third-Party Software We may use third-party service providers, including application service providers and hosting service providers, for rendering any of the Services hereunder without seeking further consent from you, but we will continue to be responsible for such Services. We, however, will not be responsible for any third-party service providers engaged by you or any third-party software that may be procured by you, whether with or without our consent and notwithstanding that the same may be integrated with the Services.</li>

        </ol>
    </li>
    <!-- end -->
    <!-- 3rd number start -->

    <li>USE OF SUBSCRIPTION SERVICES
        <ol>
            <li><strong>Grant of Rights</strong><br>
            	We will grant you a non-transferable, non-exclusive, worldwide right to permit Users authorized by you to access and use the Services in accordance with these Terms of Service, the relevant Purchase Order and all laws and regulations applicable to you.
            </li>

              <li><strong>Acceptable Use</strong><br>You will comply with the Acceptable Use Policy. Specifically, you will not:
					<ol>
                    <li>
                        use or launch any automated system, including, “robots,” “spiders,” or “offline readers,” that sends more request messages to our servers in a given period of time than a human can reasonably produce in the same period by using a conventional browser;
                    </li>

                    <li>use the Subscription Services in any manner that damages, disables, overburdens, or impairs any of our websites or interferes with any other party’s use of the Subscription Services;
                    </li>

                    <li>attempt to gain unauthorized access to the Subscription Services;
                    </li>

                    <li>make the Services available to anyone other than authorized Users;
                    </li>

                    <li>sell, resell, rent or lease the Services unless explicitly permitted in the relevant Purchase Order;
                    </li>
					
					<li>use the Services to store or transmit infringing, libelous, or otherwise unlawful or tortious material, or to store or transmit material in violation of third-party privacy rights;</li>

                    <li>use the Services to store or transmit malicious code;
                    </li>

                    <li>access the Subscription Services other than through the interface provided by us;
                    </li>

                    <li>create derivative works based on the Services or the Software unless we have been explicitly authorized by you;
                    </li>

                    <li>reverse engineer the Services or the Software or access the Services in order to:
                    	<ol><li>build a competitive product or service, or</li>
                    	<li>any features, functions or graphics of the Services; or</li></ol>
                    </li>

                    <li>use the Subscription Services for any purpose or in any manner that is unlawful under applicable laws or prohibited by under these Terms of Service.
                    </li>

                    <!-- <li>use the Subscription Services in any manner that damages, disables, overburdens, or impairs any of our websites or interferes with any other party’s use of the Subscription Services;
                    </li> -->
                </ol>
              </li>

              <li><strong>Service Usage Limitations</strong><br>
              	The use of Services  may be limited by criteria specified at e campus.com or may be more accurately described in the relevant Purchase Order. Some examples of limitation are: number of unique Users who can access the Platform, number of emails that can be sent by you in a month, number of contacts that can be managed, number of days after which visit data will be flushed and number of landing pages that can be hosted.
              </li>

              <li><strong>Service Overuse</strong><br>
              	We reserve the right to monitor and audit your usage of the Services to determine if the use is within relevant Service Usage Limitations. Any overuse of Services, if technically permitted, will be brought to your notice and may lead to pro-rata additional billing or suspension of the Services, or both.
              </li>

              <li><strong>Intimation of Unauthorized Use</strong><br>
              	You are responsible to ensure that the Services are used in accordance with these Terms of Service and will notify us immediately of any unauthorized use of your account or Users’ identifications and passwords by sending an email to <br><strong>support@edgetechnosoft.com.</strong>
              </li>


        </ol>
    </li>
    <!-- end -->

    <!-- 4th number start -->
    <li><strong>SUBSCRIPTION TERM AND FEES</strong><br>
    	The Service Term shall be the term of the Service and shall be the period as agreed upon between “e campus” and “you”, “your” in the Subscription Agreement. You may subscribe to the different subscription plans.<br><br>
    	This License Agreement and the License granted hereunder shall become effective from the date on which the License is initiated (“Effective Date“) and shall continue in terms of this Agreement unless terminated in accordance with the same. The Effective Date shall be the date on which the Services are made available to the Licensee.<br><br>
    	The Subscription Fees shall be payable on the basis of the Subscription opted by you in terms of (i) the Subscription Term; (ii) the billing cycle, i.e., monthly, quarterly or annually; and (iii) based on the number of students. The details of the Subscription opted for by you along with the Subscription Fees shall be in accordance with the Subscription Agreement.<br><br>
    	<strong>The Subscription granted to “you” by “us” will be subject to timely payment by you.</strong><br><br>
        <ol>
            <li><strong>In the event of a Quarterly Subscription:-</strong><br>
            	<ol>
            		<li>Each LicenseTerm shall begin on the date opted by you for initiation of License and expire twelve  (12) months from the Effective Date. We shall notify you of its opportunity to renew the License Termapproximat ely 30 (Thirty) days prior to the expiration of the License Term,</li>

            		<li>If you select the Quarterly Subscription Plan, then you acknowledge, and agree, that the Product will be provided in a quarterly billing cycle. A demand for payment will be automatically issued to you30 (Thirty ) days prior to the expiry of such Quarterly Subscription Plan.</li>

            		<li>While subscribing to the Quarterly Subscription Plan, you shall pay an amount as agreed upon by us and you as advance payment.</li>
            	</ol>
            </li>

            <li><strong>In the event of an Semi-Annually Subscription Plan:-</strong><br>
            	<ol>
            		<li>Each LicenseTerm shall begin on the date opted by you for initiation of License and expire twelve (12) months from  the Effective Date. You shall notify us of its opportunity to renew the License Term approximately thirty (30 ) days prior to the expiration of the License Term.</li>

            		<li>Any renewal for aSemi-Annual Subscription Plan shall require mutual agreement and all applicable fees for such renewals will be at our then-current rates. If we do not renew the Subscription, then your access to the Service will terminate at the end of the then-current License Term.</li>

            		<li>If you select the Semi-Annual Subscription Plan, you acknowledge, and agree, that the Product will be provided on a semi-yearly billing cycle. A demand for payment  will be automatically issued to you thirty (30) days prior to the expiry of such Semi-Annual Subscription Plan.</li>

            		<li>While subscribing to the Semis-Annual Subscription Plan, you shall pay an amount as agreed upon by us and you as advance payment.<br><br>
            			<strong>All fees are non-refundable. The Subscription Fees or any portions thereof paid in advance are also non-refundable if you choose to cancel the License prior to the end of the License Term.</strong><br><br>
            			<strong>All amounts and fees stated or referred  to in this Agreement are exclusive of tax, which shall be added to your invoice(s) at the appropriate rate.</strong>
            		</li>


            	</ol>

            </li>
        </ol>


    </li>
    <!-- end -->

    <!-- 5th number -->
    <li><strong>PRICING & PAYMENT TERMS:</strong>
        <ol>
            <li>Prices are quoted in Indian Rupees. You must pay in the currency in which the Product price is quoted or is selected.</li>

            <li>Payment may be required before the Service is provided and, if not, payment is due from the moment the Request for Service is accepted. After you have submitted your Request for Service, e campus may take and shall be entitled to receive such payment as is due in respect of the Request for Service.</li>

            <li>Payment may be made by Credit Card, Debit Card, cheques or any other method that e campus may introduce at its absolute discretion.</li>

            <li>e campus reserves the right to accept or refuse any payment made in any form.</li>

            <li>To maximise security, e campus does not hold any Credit/Debit Card details. They are held by third party payment providers.</li>

            <li>Your Credit Card Company may also do security checks to confirm that it is you making the purchase.</li>

            <li>Your statutory rights remain unaffected by these Terms.</li>

        </ol>
    </li>
    <!-- end -->
    <!-- 6th number -->
    <li><strong>OWNERSHIP AND PROPRIETARY RIGHTS</strong>
        <ol>
            <li><strong>e campus Rights</strong>
              <ol><li>We own or have rights to all worldwide intellectual property rights in and to the Subscription Services, Consulting Services, e campus Application/Platform and Software (including all derivatives or improvements thereof). All suggestions, enhancement requests, feedback, recommendations or other inputs provided by you or any other party relating to the Services or Software shall be owned by us, and you hereby do and shall make all assignments and take all reasonable acts necessary to accomplish the foregoing ownership. Any rights not expressly granted herein are reserved by us.</li>
              
              <li>You agree not to copy, rent, lease, sell, distribute, or create derivative works based on the Subscription Services or the Consulting Services, in whole or in part, by any means, except as expressly authorized in writing by us.</li>
              </ol>
            </li><br>

            <li><strong>Your Rights</strong><br>
              You own any data, information or material originated by you that you submit or compile in the course of using the Services. We have no ownership rights in or to Customer Data. You shall be solely responsible for the accuracy, quality, integrity, legality, reliability, appropriateness and intellectual property ownership or right to collect and use the Customer Data. You permit and grant us and our licensors the right to use the Customer Data only as necessary to provide the Subscription Services and Consulting Services under these Terms of Service.
            </li><br>

            <li><strong>Using your name and logo</strong><br>
              You hereby permit us to use your name, website address and logo in our marketing material including website, email campaigns, brochures etc. during and after active engagement.
            </li>

        </ol>
    </li>
    <!-- end -->

    <!-- 7th number -->
    <li><strong>CONFIDENTIALITY</strong>
        <ol>
            <li><strong>Confidential Information</strong><br>
              As used herein, “Confidential Information” means all confidential information disclosed by a Party (“Disclosing Party”) to the other Party (“Receiving Party”), whether orally or in writing, that is designated as confidential or that reasonably should be understood to be confidential given the nature of the information and the circumstances of disclosure. Your Confidential Information shall include Customer Data; our Confidential Information shall include the Services; and Confidential Information of each Party shall include the terms and conditions of these Terms of Service and all Orders Forms as well as business and marketing plans, technology and technical information, product plans and designs, and business processes disclosed by such Party. However, Confidential Information (other than Customer Data) shall not include any information that<br>
              <ol>
                <li>is or becomes generally known to the public without breach of any obligation owed to the Disclosing Party;
                </li>
                <li>was known to the Receiving Party prior to its disclosure by the Disclosing Party without breach of any obligation owed to the Disclosing Party;
                </li>
                <li>is received from a third party without breach of any obligation owed to the Disclosing Party; or</li>
                <li>was independently developed by the Receiving Party.</li>
              </ol>

             
            </li><br>

            <li><strong>Protection of Confidential Information</strong><br>
              The Receiving Party shall use the same degree of care to protect Confidential Information that it uses to protect the confidentiality of its own confidential information of like kind (but in no event less than reasonable care). It shall not use any Confidential Information of the Disclosing Party for any purpose outside the scope of these Terms of Service and except as otherwise authorized by the Disclosing Party in writing, limit access to Confidential Information of the Disclosing Party to those of its and its service providers’ employees, consultants, contractors and agents who need such access for purposes consistent with these Terms of Service and who have signed confidentiality agreements with the Receiving Party containing protections no less stringent than those herein.
            </li><br>
        </ol>
    </li>
    <!-- end -->

    <!-- 8th number -->
    <li><strong>REPRESENTATIONS AND WARRANTIES; WARRANTY DISCLAIMERS</strong>
        <ol>
            <li><strong>Disclaimer of warranties</strong><br>
              We make no representations or warranties about the suitability, reliability, availability, timeliness, security or accuracy of the subscription services, data made available from the subscription services, or the consulting services for any purpose. Application programming interfaces (APIS) may not be available at all times. To the extent permitted by law, the subscription services and consulting services are provided “as is” without warranty or condition of any kind and we disclaim all warranties and conditions, whether express, implied or statutory, with regard to the subscription services and the consulting services, including warranties as to merchantability or fitness for a particular purpose.<br>
            </li><br>

            <li><strong>Limitation of liability</strong><br>
              In no event shall either party, its directors, officers or any of its affiliates, be liable for any special, punitive, indirect, consequential or incidental damages, including, but not limited to, loss of data, loss of business or other loss (including substitution of services) arising out of or relating to these terms of service  or any third party services delivered in connection herewith even if previously advised of the possibility of such damages and regardless of whether such liability arises out of contract, negligence, tort, strict liability or any other theory of legal liability; and in no event shall either party’s cumulative liability hereunder (other than for claims for indemnity and payment of fees due) exceed the amount paid or payable by you to ___ in the 6 (six) month period immediately preceding any such claim or usd 5000 ( us dollars five thousdand only), whichever is lesser.
            </li><br>
        </ol>
    </li>
    <!-- end -->

    <!-- 9th number -->
    <li><strong>Customer Data Protection</strong>
        <ol>
            <li><strong>No Sensitive Information</strong><br>
              You represent that you shall not use the Subscription Services to collect, manage or process Sensitive Information and shall be solely responsible with regard to the nature and extent of the information collected from your clients and potential clients.<br>
            </li><br>

            <li><strong>Restricted use of Customer Data</strong><br>
              We will not use, or allow anyone else to use, Customer Data to contact any individual or company except as directed or otherwise permitted by you. We will use Customer Data only in order to provide the Subscription Services and Consulting Services and only as permitted by applicable law, these Terms of Service and the Privacy Policy, as set out at e campus.in/privacy-policy.
            </li><br>

            <li><strong>Security Measures</strong><br>
              We will adopt and maintain appropriate organizational and technical safeguards for the protection of the security, confidentiality and integrity of Customer Data.
            </li><br>
        </ol>
    </li>
    <!-- end -->

    <!-- 10th number -->
    <li><strong>DISCLAIMER</strong><br>
      This site and its components are offered for informational purposes only; this site shall not be responsible or liable for the accuracy, usefulness or availability of any information transmitted or made available via the site, and shall not be responsible or liable for any error or omissions in that information." This type of provision will help you protect against liability in case a user relies on your information and it causes a problem. 
    </li>
    <!-- end -->

    <!-- 11th number -->
    <li><strong>INDEMNIFICATION</strong><br>
      You agree to indemnify, defend and hold harmless e campus (“we”, “us” or “our”), its subsidiaries, affiliates, third-parties and their respective officers, directors, agents, and employees, from and against any and all losses, liabilities, claims, damages, costs and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by us that arise out of, result from, or may be payable by virtue of, any breach or non-performance of any representation, warranty, covenant or agreement made or obligation to be performed by you pursuant to this Agreement. Further, you agree to hold us harmless against any claims made by any third party due to, or arising out of, or in connection with, your use of the Product or any claim that you have or will cause damage to a third party, your violation of this Agreement, or your violation of any rights of another, including any intellectual property rights.
    </li>
    <!-- end -->

    <!-- 12th number -->
    <li><strong>GENERAL PROVISIONS</strong>
        <ol>
            <li><strong>FORCE MAJEURE.</strong><br>
              Neither Party shall be responsible for failure or delay in performance if caused by Force Majeure, except in respect of payment obligations hereunder. Each Party will use reasonable efforts to mitigate the effect of a Force Majeure event.<br>
            </li><br>

            <li><strong>SEVERABILITY </strong><br>
              If any provision of these Terms of Service is held by a court of jurisdiction to be contrary to law, or for any reason invalid, void or unenforceable, the remainder of the provisions shall, to the extent practicable, remain in full force and effect and Parties will negotiate in good faith to amend such invalid, void or unenforceable provision to give effect to the intended purpose of such provision in accordance with applicable laws.
            </li><br>

            <li><strong>RELATIONSHIP BETWEEN THE PARTIES</strong><br>
              No joint venture, partnership, employment, or agency relationship is created between you and e campus as a result of these Terms of Service or use of the Services.
            </li><br>

            <li><strong>NO WAIVER.</strong><br>
              The failure of either Party to enforce any right or provision in these Terms of Service shall not constitute a waiver of such right or provision unless acknowledged and agreed to by such Party in writing.
            </li><br>
        </ol>
    </li>
    <!-- end -->

    <!-- 13th number -->
    <li><strong>COPYRIGHT AND CONTENT OWNERSHIP</strong><br>
      All content on the Website/App, inter alia text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of Edge Technosoft Pvt. Ltd. (“Company”), its affiliates, its content suppliers or the recognized and credited third parties and is protected by relevant copyright, authors’ rights and database right laws. The compilation of all content on this Website/App is the exclusive property of the Company and/or its affiliates and is protected by relevant copyright and database right laws. All software used on the Website/App are the property of the Company, its affiliates or its software suppliers and is protected by the relevant copyright and author’ rights laws.<br><br>
      You may not systematically extract/or re-utilise parts of the contents of the Website/App without the Company’s and/or its affiliates and/or the recognized and credited third parties (as may be applicable) express written consent, apart for Your personal, non-commercial use, but only if You acknowledge the Website/App and/or the relevant, recognized and credited third parties (as may be applicable) as the source of the material so extracted or re-utilised. In particular, You may not utilise any data mining, robots, or similar data gathering and extraction tools to extract (whether once or many times) for re-utilisation of any substantial parts of the Website/App, without the Company and/or its affiliates and/or the recognized and credited third parties (as may be applicable), express written consent. You may also not create and/or publish Your own database that features substantial parts of the Website/App without the Company and/or its affiliates (as may be applicable) express written consent.
    </li>
    <!-- end -->

    <!-- 14th number -->
    <li><strong>RESOLUTION OF DISPUTES</strong><br>
      These Terms of Service shall be governed by and construed in accordance with the laws of India. The rules are framed by the courts of Uttar Pradesh. Subject to the foregoing, the courts at Bangalore, India shall have exclusive jurisdiction.
    </li>
    <!-- end -->

    <!-- 15th number -->
    <li><strong>ENTIRE AGREEMENT</strong><br>
      These Terms of Service, including its Purchase Order, the PrivacyPolicy, the Acceptable Use Policy and any additional Order Forms, modifications or addenda that may be agreed to from time to time constitutes the entire agreement between the Parties and supersedes all prior and contemporaneous agreements, proposals or representations, written or oral, concerning its subject matter. Any additional or different terms set out in a purchase order or any future correspondence shall not be binding on us. Any modification to these Terms of Service shall be notified to you within the e campus Application used to access your Subscription Services and by posting a revised copy on our website. Any modification to an Order Form shall be as mutually agreed to by the Parties.
    </li>
    <!-- end -->

     <!-- 16th number -->
    <li><strong>ORDER OF PRECEDENCE</strong><br>
      In the event of any conflict between these Terms of Service and the terms of a Purchase Order, the Purchase Order shall prevail solely with respect to the subject matter thereof.
    </li>
    <!-- end -->

    <!-- 17th number -->
    <li><strong>CONTACT INFORMATION</strong><br>
      You can contact us at <Strong>support@edgetechnosoft.com.</Strong>
    </li>
    <!-- end -->
    
</ol>


                    </div>

            </div>

        </div>

    </section>

<?php include 'footer.php';?>