<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>eCampus|University Management System|LMS|Smart Classes|E-learning|2020</title>
    <meta name="description" content="eCampus is a Software-as-a-Service (SaaS) platform that  provides various services to institutions for managing all Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions on the cloud for universities, colleges and educational institutions |2020 ">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="eCampus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="eCampus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='eCampus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='eCampus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="eCampus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>
<link rel="stylesheet" href="assets/css/tour.css">
     <link rel="stylesheet" href="assets/css/vendor.css">

</head>
<body>
<?php include('header.php'); ?>

<section class="highlight-features section b-b" style="margin-top: 40PX;background: #c8e8ff;">
      <div class="container">
         <div class="text-center">
            <h2>Describe important features</h2>
            <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, assumenda autem consequatur cum dignissimos dolores doloribus eius eum iusto laborum quasi quidem sapiente sit.</p>
         </div>
         <div class="row align-items-center">
            <div class="col-12 col-lg-5 ml-lg-auto order-lg-2">
               <div class="row highlight-items" data-event="hover">
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item active" data-highlight="1">
                        <h5 class="bold">Feature 1</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="2">
                        <h5 class="bold">Feature 2</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="3">
                        <h5 class="bold">Feature 3</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="4">
                        <h5 class="bold">Feature 4</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-lg-6 order-lg-1">
               <figure><img src="assets/img/partner/lms.jpg" class="img-responsive" alt="..."></figure>
               <div id="highlight-images" class="active-1">
                  <figure class="highlight-1"><img src="assets/img/features/highlight-1.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-2"><img src="assets/img/features/highlight-2.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-3"><img src="assets/img/features/highlight-3.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-4"><img src="assets/img/features/highlight-4.png" alt="..."></figure>
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="highlight-features section b-b" style="margin-top: 40PX;background: #c8e8ff;">
      <div class="container">
         <div class="text-center">
            <h2>Describe important features</h2>
            <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, assumenda autem consequatur cum dignissimos dolores doloribus eius eum iusto laborum quasi quidem sapiente sit.</p>
         </div>
         <div class="row align-items-center">
            <div class="col-12 col-lg-5 ml-lg-auto order-lg-2">
               <div class="row highlight-items" data-event="hover">
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item active" data-highlight="1">
                        <h5 class="bold">Feature 1</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="2">
                        <h5 class="bold">Feature 2</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="3">
                        <h5 class="bold">Feature 3</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="4">
                        <h5 class="bold">Feature 4</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-lg-6 order-lg-1">
               <figure><img src="assets/img/partner/lms.jpg" class="img-responsive" alt="..."></figure>
               <div id="highlight-images1" class="active-1">
                  <figure class="highlight-1"><img src="assets/img/features/highlight-1.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-2"><img src="assets/img/features/highlight-2.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-3"><img src="assets/img/features/highlight-3.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-4"><img src="assets/img/features/highlight-4.png" alt="..."></figure>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- <section class="highlight-features section b-b" style="margin-top: 40PX;background: red">
      <div class="container">
         <div class="text-center">
            <h2>Describe important features</h2>
            <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, assumenda autem consequatur cum dignissimos dolores doloribus eius eum iusto laborum quasi quidem sapiente sit.</p>
         </div>
         <div class="row align-items-center">
            <div class="col-12 col-lg-5 ml-lg-auto order-lg-1">
               <div class="row highlight-items" data-event="hover">
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item active" data-highlight="1">
                        <h5 class="bold">Feature 1</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="2">
                        <h5 class="bold">Feature 2</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="3">
                        <h5 class="bold">Feature 3</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
                  <div class="col-lg-12 col-6">
                     <div class="rounded p-4 mb-3 highlight-item" data-highlight="4">
                        <h5 class="bold">Feature 4</h5>
                        <p class="my-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad asperiores.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-lg-6 order-lg-2">
               <figure><img src="assets/img/partner/lms.jpg" class="img-responsive" alt="..."></figure>
               <div id="highlight-images11" class="active-1">
                  <figure class="highlight-1"><img src="assets/img/features/highlight-1.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-2"><img src="assets/img/features/highlight-2.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-3"><img src="assets/img/features/highlight-3.png" class="rounded-circle" alt="..."></figure>
                  <figure class="highlight-4"><img src="assets/img/features/highlight-4.png" alt="..."></figure>
               </div>
            </div>
         </div>
      </div>
   </section>
 -->
<?php include 'footer.php';?>