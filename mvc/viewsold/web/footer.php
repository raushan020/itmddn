
  <!-- Newsletter Area -->

    <section class="newsletter-area">

        <div class="container">

            <div class="row">
                <div class="col-md-2"></div>

                <div class="col-md-8">

                    <div class="news-letter-box" >

                        <form class="nl-form">

                            <input type="email" name="" id="" placeholder="Enter Email To Subscribe" required="">

                            <button type="submit" class="button"><i class="fa fa-paper-plane" aria-hidden="true" style="color: #2c92f3;"></i></button>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- /Newsletter Area -->

<!-- Footer Area -->


    <footer class="footer-area">

        <div class="footer-widgeet-area">

            <div class="container">

                <div class="row">

                    <div class="col-md-12">

                        <div class="footer-links-wrapper">

                            <div class="footer-widget footer-links">

                                <h4>Company</h4>

                                <ul class="fl-list">

                                    <li><a href="home">Home</a></li>
                                    <li><a href="about">Meet The Team</a></li>

                                    <li><a href="about">About Us</a></li>

                                    <li><a href="about">Why e campus Us</a></li>
                                </ul>
                                <!-- <ul class="fl-list" style="margin-top: 30px">

                                    <li><a class="btn btn-info" href="#" data-toggle="modal" data-target="#payNowmodal">Pay Now</a></li>
                                   
                                </ul> -->
                            </div>

                            <div class="footer-widget footer-links">

                                <h4>Useful Links</h4>

                                <ul class="fl-list">

                                    <li><a href="terms_of_service">Terms Of Service</a></li>

                                    <li><a href="privacy_policy">Privacy Policy</a></li>

                                    <li><a href="price">Pricing</a></li>

                                    <li><a href="getting_started">Help</a></li>

                                </ul>

                            </div>

                            <div class="footer-widget footer-links">

                                <h4>Products</h4>

                                <ul class="fl-list">
                                    <li><a href="learning_management_system">Learning Management</a></li>
                                    <li><a href="university_management_system">University Management</a></li>
                                    <li><a href="webcam_based_assessment_system">Online Examination</a></li>
                                    <li><a href="#">Webinar</a></li>
                                </ul>

                            </div>

                            <div class="footer-widget footer-links">

                                <h4>Social</h4>

                                <ul class="fl-list">

                                    <li><a href="https://www.facebook.com/E-Campus-115587966511317/" target="_blank">Facebook</a></li>

                                    <li><a href="https://twitter.com/ECampus18" target="_blank">Twitter</a></li>

                                    <li><a href="https://www.linkedin.com/company/ecampus/" target="_blank">Linkedin</a></li>


                                </ul>

                            </div>
                             <div class="footer-widget footer-links">

                              <a class="twitter-timeline" data-width="800" data-height="200" data-theme="dark" href="https://twitter.com/ECampus18?ref_src=twsrc%5Etfw">Tweets by e campus18</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="footer-copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p >© 2013-2019 All Rights Reserved | <a href="https://edgetechnosoft.com/" target="_blank">Edge Technosoft Pvt. Ltd</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Footer Area -->

    <!-- jQuery Plugin -->

    <script src="assets/js/jquery-3.2.0.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Owl Carousel Plugin -->
    <script src="assets/js/owl.js"></script>

    <script src="assets/js/shuffle.min.js"></script>

    <!-- Main Counterup Plugin-->
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/countdown.js"></script>
    <script src="assets/js/jquery.scrollUp.js"></script>
    <script src="assets/js/jquery.waypoints.min.js"></script>   

    <!-- Nav Menu CSS -->
    <script src="assets/js/jquery.slicknav.min.js"></script>

    <!-- Main Script -->
    <script src="assets/js/ecampus.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/tour.js"></script>
    <script src="assets/js/lib.js"></script>
    <script src="assets/js/jquery.datetimepicker.full.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkUOdZ5y7hMm0yrcCQoCvLwzdM6M8s5qk&callback=initMap"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      <script>
    jQuery(document).ready(function() {
        'use strict';
        jQuery('#filter-date, #search-from-date, #search-to-date').datetimepicker();
    });
</script>
    <?php
        if ($this->session->flashdata('success')) {
           
       ?>
    <script type="text/javascript">
        swal("Thank you for submitting the form.", "We will Contact you soon!");
    </script>
<?php  }
    ?>


</html>