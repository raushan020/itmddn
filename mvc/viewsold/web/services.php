<?php include 'header.php';?>
<!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h3>Sean Services</h3>
                    </div>
                </div>
            </div>
        </div>     
    </section>
    <!-- /Breadcrumb Area -->

    <!-- Innerpage Content -->
    <section class="innerpage-area section-padding pad-bot-120">
        <div class="container">
            <div class="services-content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single-services">
                            <div class="s-single-icon">
                                <i class="flaticon-computer-graphic"></i>
                            </div>
                            <div class="s-single-text">
                                <h4>UI & UX Design</h4>
                                <p>Successful design is not the achievement
                                        perfection but the minimizat and accom
                                        modation of imperfection.</p>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-services">
                            <div class="s-single-icon">
                                <i class="flaticon-project-1"></i>
                            </div>
                            <div class="s-single-text">
                                <h4>Brand Development</h4>
                                <p>Successful design is not the achievement
                                        perfection but the minimizat and accom
                                        modation of imperfection.</p>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-services">
                            <div class="s-single-icon">
                                <i class="flaticon-pencil"></i>
                            </div>
                            <div class="s-single-text">
                                <h4>Print Design</h4>
                                <p>Successful design is not the achievement
                                        perfection but the minimizat and accom
                                        modation of imperfection.</p>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-services">
                            <div class="s-single-icon">
                                <i class="flaticon-web"></i>
                            </div>
                            <div class="s-single-text">
                                <h4>Web Development</h4>
                                <p>Successful design is not the achievement
                                        perfection but the minimizat and accom
                                        modation of imperfection.</p>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-services">
                            <div class="s-single-icon">
                                <i class="flaticon-seo"></i>
                            </div>
                            <div class="s-single-text">
                                <h4>Digital Marketing</h4>
                                <p>Successful design is not the achievement
                                        perfection but the minimizat and accom
                                        modation of imperfection.</p>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single-services">
                            <div class="s-single-icon">
                                <i class="flaticon-analytics-1"></i>
                            </div>
                            <div class="s-single-text">
                                <h4>Web Analytics</h4>
                                <p>Successful design is not the achievement
                                        perfection but the minimizat and accom
                                        modation of imperfection.</p>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Innerpage Content -->

    <!-- Fun Fact Area -->
    <section class="fun-fact2-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="section-img ff2-img">
                        <div class="sIs-1-wrapper">
                            <img src="assets/img/section-img/made-easy2-1.png" alt="">
                            <ul class="sts-1-features">
                                <li><i class="fa fa-sliders" aria-hidden="true"></i><span>Easy To Filtering</span></li>
                                <li><i class="fa fa-list-alt" aria-hidden="true"></i><span>Many Features</span></li>
                                <li><i class="fa fa-universal-access" aria-hidden="true"></i><span>Always Open</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="counter-up-wrapper">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="single-counter-up">
                                            <div class="scu-icon">
                                                <i class="flaticon-project"></i>
                                            </div>
                                            <div class="scu-counter">
                                                <span class="counter-up">6152</span>
                                                <p>Projects</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="single-counter-up">
                                            <div class="scu-icon">
                                                <i class="flaticon-network"></i>
                                            </div>
                                            <div class="scu-counter">
                                                <span class="counter-up">2672</span>
                                                <p>Clients</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="single-counter-up">
                                            <div class="scu-icon">
                                                <i class="flaticon-interview"></i>
                                            </div>
                                            <div class="scu-counter">
                                                <span class="counter-up">1273</span>
                                                <p>Meetings</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="single-counter-up">
                                            <div class="scu-icon">
                                                <i class="flaticon-tea"></i>
                                            </div>
                                            <div class="scu-counter">
                                                <span class="counter-up">9125</span>
                                                <p>Cup Coffee</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Fun Fact Area -->
    
    <!-- Services Content -->
    <section class="services-content section-padding pad-top-120">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-services">
                        <div class="s-single-icon">
                            <i class="flaticon-web"></i>
                        </div>
                        <div class="s-single-text">
                            <h4>Web Development</h4>
                            <p>Successful design is not the achievement
                                perfection but the minimizat and accom
                                modation of imperfection.</p>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-services">
                        <div class="s-single-icon">
                            <i class="flaticon-seo"></i>
                        </div>
                        <div class="s-single-text">
                            <h4>Digital Marketing</h4>
                            <p>Successful design is not the achievement
                                perfection but the minimizat and accom
                                modation of imperfection.</p>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-services">
                        <div class="s-single-icon">
                            <i class="flaticon-computer-graphic"></i>
                        </div>
                        <div class="s-single-text">
                            <h4>UI & UX Design</h4>
                            <p>Successful design is not the achievement
                                perfection but the minimizat and accom
                                modation of imperfection.</p>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Services Content -->

    <!-- Newsletter Area -->
    <section class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="news-letter-box">
                        <form class="nl-form">
                            <input type="email" name="EMAIL" id="email" placeholder="Enter Email To Subscribe">
                            <button type="submit" class="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Newsletter Area -->
<?php include 'footer.php';?>