
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style>
	

html,
body {
  
  font-family: Montserrat, Helvetica, Arial, sans-serif;
    font-weight: 400;
    color: #595959;
   }
   .row{
   }

.navigation {
  
  background-color: white;
  color: black;
  text-decoration: none;
      font-family: Montserrat, Helvetica, Arial, sans-serif;
    font-weight: 400;
    color: #595959;
}
    /*box-shadow: 16px -14px 6px -13px #111;*/
}
.navigation__link {
  display: block;
  text-decoration: none;
  padding: 0.35em;
  color: black;
  /*text-shadow: 2px 2px 4px #000000;*/

}
 .a{
  color:black;
}

/*.navigation__link:hover {
  background-color: rgba(0, 0, 0, 1);
  color:white;
  border-radius: 8px;
*/

}
.navigation__link.active {
  
  background-color: rgba(0, 0, 0, 1);
  border-radius: 8px;
  
}



.link-1 {
 color: black;
 text-decoration: none;
 text-align: center;
  float: right;
  margin: 10 50px;
  width: 80px;
  
  
}

.a:hover{
	text-decoration:none;
}



body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}
hr.new4 {
  border: 3px solid grey;
  
}
.logo0{
  max-width: 500px;
  height: 60px;
    padding-top:6px;
}

.finger_icon{
        font-size: 30px;
        color:#82c91e;
}
.home{
    margin-top: 20px;
}




@media (min-width:320px)  {  
  hr.new4 {
  
  width: auto;
}


 }
  
 
@media (min-width:480px)  {  
	#mainNav {
 display: block;
   }
   hr.new4 {
    width: 1200px;
}
 
}
@media (min-width:600px)  { 

  
}

@media (min-width:801px)  { 
	
 }
@media (min-width:1025px) { 
	.navigation {
   display: block;
  }
 }
@media (min-width:1281px) {
	.navigation {
   display: block;
  }
 }

@media (min-width: 768px) {

#mainNav {
 display: block;
   
}

</style>

<header>
	<div class="container-fluid" style="background: #fff;">
		<div class="row" >
	
<div class="container-fluid topnav" id="myTopnav">
	<img src="assets/img/e_campus.png" class="logo0" alt="Logo" >
  <a href="home" class="active link-1 btn btn-primary home">Home</a>
  
</div>


	</div>
</header>

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<nav class="navigation" id="mainNav">
				<h3 class="navigation__link text-align active"><img src="assets/img/help/list.png" style="height: 40px;">&nbsp;  Getting Started</h3>


 <hr class="new4">



		

<div class="col-md-12 boxstuff">
			


   <div class="container">
        <div class="row">
        <div class="col-md-11">

        <div class="col-md-1">
      <i class="fa fa-hand-o-right" style="font-size: 30px; color:#82c91e;"></i>
    </div>

      <a class="navigation__link a" href="add_exam"><strong>How To Add Syllabus</strong></a>
      <p>Follow The Following Process To Add A Syllabus...</p>
          
          </div>
          </div></div>

                  <hr>


                <div class="container">
				<div class="row">
				<div class="col-md-11">

        <div class="col-md-1">
			<i class="fa fa-hand-o-right" style="font-size: 30px; color:#82c91e;"></i>
		</div>

      <a class="navigation__link a" href="add_exam"><strong> How To Add An Exam</strong></a>
      <p>Follow The Following Process To Add An Exam...</p>
  				
  				</div>
  				</div></div>

      <hr>

      <div class="container">
				<div class="row">
				<div class="col-md-11">
      <div class="col-md-1">
			<i class="fa fa-hand-o-right" style="font-size: 30px; color:#82c91e;"></i>
		</div>            
      <a class="navigation__link a" href="upload_video"><strong> How to upload a video unit wise</strong></a>
      <p>Follow The Following Process To Add A Video...</p>
      			</div>
      			</div>
      			</div>

      <hr>

      			<div class="container">
				<div class="row">
				<div class="col-md-11">
      <div class="col-md-1">
			<i class="fa fa-hand-o-right" style="font-size: 30px; color:#82c91e;"></i>
		</div>            
      <a class="navigation__link a" href="add_unit"><strong> How To Add Unit</strong></a>
      <p>Follow The Following Process To Add Unit....</p>
      			</div>
      			</div>
      			</div>

      <hr>
            <div class="container">
              <div class="row">
              <div class="col-md-11">
            <div class="col-md-1">
            <i class="fa fa-hand-o-right" style="font-size: 30px; color:#82c91e;"></i>
          </div>            
            <a class="navigation__link a" href="via_youtube"><strong>How to add video via YouTube </strong></a>
            <p>Follow the following process to add video via YouTube...</p>
                  </div>
                  </div>
                  </div>

            
          </div>


	</nav>
		</div>
				
	</div>
</div>

	
				
	
		