<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>e campus | University Management System | LMS | E-Learning | 2020</title>
    <meta name="description" content="e Campus is a Saas based 'Campus Automation Software' empowering colleges, universities & educational institutions to automate and manage their entire day to day operations. It provides powerful digitalised modules like Learning Management System(LMS), Webcam-based examination, Fee Management among others for improved efficiency and productivity.">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="E-Campus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="E-Campus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='E-Campus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='E-Campus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="E-Campus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>

</head>
<<body>
<?php include('header.php'); ?>
    <section class="hero-area heroV4" style="margin-top: 30px;">
    
      <div class="container">

            <div class="hero-content">

                <div class="row">

                    <div class="col-md-4">

                        <div class="hero-img heroImg-s3">

                            <img src="assets/img/section-img/webbasedmobile.jpg" alt="" class="hm_img6">

                        </div>

                    </div>

                    <div class="col-md-8">

                        <div class="hero-text">

                            <h2>Webcam based Assessment System</h2>

                            <p>The Webcam Based Assessment System helps the universities organize and conduct assessments on
                                the electronic platforms in the exact manner that the desire. In requires the use of webcams and
                                microphones while the students are attempting their exam.</p>

                            <p>E-assessments are online tests
                                conducted with the purpose of evaluating, analyzing, and documenting the academic readiness,
                                learning progress, skill acquisition, or educational needs of the test takers.</p>

                            <div class="hero-btn">

                                <a href="#" class="btn-style-1_b" data-toggle="modal" data-target="#demoreq">Request Demo</a>

                                <!-- <a href="#" class="btn-style-1">Start For Free</a> -->

                            </div>

                        </div>

                    </div>

                    

                </div>

            </div>

        </div>

    <!--     <div class="container">

            <div class="hero-content">

                <div class="row">

                     <div class="col-md-4">

                        <div class="hero-img heroImg-s3">

                            <img src="assets/img/section-img/webbasedmobile.jpg" alt="" class="hm_img6">

                        </div>

                    </div>

                    <div class="col-md-8">

                        <div class="hero-text">

                            <h2>Webcam based Assessment System</h2>

                            <p>The Webcam Based Assessment System helps the universities organize and conduct assessments on
                                the electronic platforms in the exact manner that the desire. In requires the use of webcams and
                                microphones while the students are attempting their exam. E-assessments are online tests
                                conducted with the purpose of evaluating, analyzing, and documenting the academic readiness,
                                learning progress, skill acquisition, or educational needs of the test takers.</p>

                            <div class="hero-btn">

                                <a href="#" class="btn-style-1_b" data-toggle="modal" data-target="#demoreq">Request Demo</a>

                            </div>

                        </div>

                    </div>
                   
                </div>

            </div>

        </div> -->

    </section>

    <!-- /Hero Area -->



    <section class="about-area3 section-padding">

        <div class="container">

            <div class="row">

                <div class="section-title">

                    <h3>How Webcams help in Online Assessments?</h3>

                    <!-- <p>We believe in simplifying things which is the beauty of E-Campus. We have figured the UMS module with the object to keep things free from hassle and chaos.

                    </p> -->

                </div>

            </div>

            <div class="row">

                <div class="col-md-5">

                    <div class="section-text">

                        <p>We believe in simplifying things which is the beauty of e campus. We have figured the UMS module with the object to keep things free from hassle and chaos.

                    </p>

                        <p>Our product ensures that the user at the administrative level does not have to juggle between places just to oversee and organize the entire database. Click one tab you see student’s information, go to another tab you’ll see the teachers’ information. It’s just that effortless.</p>

                        <p>The Admins can view the total no. of students enrolled, no. of professors, total revenue earned and other kinds of statistics on the homepage of the portal itself.</p>

                    </div>

                </div>

                <div class="col-md-7">

                    <div class="section-img about3-img">

                        <img src="assets/img/section-img/Laptop_WebCam.jpg" alt="">

                        <div class="hs2-mini-img">

                            <!-- <img src="assets/img/section-img/heroV2-1_mini2.jpg" alt="" class="a3_1"> -->

                            

                            <!-- <img src="assets/img/section-img/heroV2-1_mini1.jpg" alt="" class="a3_2"> -->

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <section class="about-area3 section-padding" style="background: #f1f4f9;">

        <div class="container">

            <div class="row">

                <div class="section-title">

                    <h3>Features of e campus Webcam based Assessment System:</h3>

                </div>

                <div class="col-md-6">

                        <div class="au-list-box">

                            <ul class="au-list">

                                <li><h5>Enables easy authenticity of students appearing for the e-assessment.</h5></li>

                                <li><h5>Does not require the physical availability of a teacher to keep an eye on the examinees.</h5></li>

                                <li><h5>Ease of giving exam anytime anywhere.</h5></li>

                                <li><h5>Facilitates cost and time effectiveness.</h5></li>  
                            </ul>

                    </div>

                </div>

                <div class="col-md-6">

                        <div class="au-list-box">

                            <ul class="au-list">

                                <li><h5>Quick results in case of a practice test.</h5></li>

                                <li><h5>Helps the students and the teacher evaluate and analyse students overall performance.</h5></li>

                                <li><h5>E-assessments proves environment friendly as it saves paper and its distribution cost.</h5></li>

                            </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <?php include 'footer.php';?>