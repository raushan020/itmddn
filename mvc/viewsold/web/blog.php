<?php include 'header.php';?>

    <!-- Blog Area -->
    <section class="recent-blog-area" style="margin-top: 180px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h3>Blogs & News</h3>
                        <p>Watch this corner for knowledge empowerment and the latest trends related to technological advancements.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="single-blogV2">
                        <div class="sblog-img">
                            <img src="assets/img/blog/blog-1.jpg" alt="">
                        </div>
                        <div class="sblog-date">
                            <span class="sbd">25th</span><span class="sbm">May</span>
                        </div>
                        <div class="sblog-content">
                            <div class="sblog-cat">
                                <a href="#">Business</a>
                            </div>
                            <div class="sblog-text">
                                <h4><a href="#">Why Digitalization is the ultimate future of education?</a></h4>
                                <p>Digitalization is defined as the conversion of text, pictures, or sounds into a digital form that can be processed by a computer. </p>
                            </div>
                            <div class="sblog-author">
                                <span class="sba-img">
                                    <img src="assets/img/avatar/blog-author-1.jpg" alt="">
                                </span>
                                <p>By <a href="#">Robison Croso</a></p>
                            </div>
                            <ul class="sb-lc-list">
                                <li><i class="fa fa-heart-o" aria-hidden="true"></i>15</li>
                                <li><i class="fa fa-comment" aria-hidden="true"></i>20</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-blogV2">
                        <div class="sblog-img">
                            <img src="assets/img/blog/blog-2.jpg" alt="">
                        </div>
                        <div class="sblog-date">
                            <span class="sbd">30th</span><span class="sbm">May</span>
                        </div>
                        <div class="sblog-content">
                            <div class="sblog-cat">
                                <a href="#">Business</a>
                            </div>
                            <div class="sblog-text">
                                <h4><a href="#">Significance of SIS in managing students' information.</a></h4>
                                <p>Student Information System (SIS) is the system or platform that constitutes students’ information. It is the core of any educational institution.</p>
                            </div>
                            <div class="sblog-author">
                                <span class="sba-img">
                                    <img src="assets/img/avatar/blog-author-1.jpg" alt="">
                                </span>
                                <p>By <a href="#">Robison Croso</a></p>
                            </div>
                            <ul class="sb-lc-list">
                                <li><i class="fa fa-heart-o" aria-hidden="true"></i>15</li>
                                <li><i class="fa fa-comment" aria-hidden="true"></i>20</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-blogV2">
                        <div class="sblog-img">
                            <img src="assets/img/blog/blog-3.jpg" alt="">
                        </div>
                        <div class="sblog-date">
                            <span class="sbd">22th</span><span class="sbm">May</span>
                        </div>
                        <div class="sblog-content">
                            <div class="sblog-cat">
                                <a href="#">Business</a>
                            </div>
                            <div class="sblog-text">
                                <h4><a href="#">Need for a Saas model in a Learning Management System.</a></h4>
                                <p>In the cloud technology, Software-as-a-Service (SaaS) is a type of software distribution model available to the users on a subscription basis ranging from monthly, quarterly or yearly, etc. </p>
                            </div>
                            <div class="sblog-author">
                                <span class="sba-img">
                                    <img src="assets/img/avatar/blog-author-1.jpg" alt="">
                                </span>
                                <p>By <a href="#">Robison Croso</a></p>
                            </div>
                            <ul class="sb-lc-list">
                                <li><i class="fa fa-heart-o" aria-hidden="true"></i>15</li>
                                <li><i class="fa fa-comment" aria-hidden="true"></i>20</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Blog Area -->

    <!-- Newsletter Area -->
    <section class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="news-letter-box">
                        <form class="nl-form">
                            <input type="email" name="EMAIL" id="email" placeholder="Enter Email To Subscribe">
                            <button type="submit" class="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Newsletter Area -->
<?php include 'footer.php';?>