
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<style>
	

html,
body {
  height: 100%;
  font-family: Montserrat, Helvetica, Arial, sans-serif;
    /*font-weight: 400;
    font-size: 15px;*/
  
    color: #595959;

}


}

h1 {

    color: #333333 !important;
  font-size: 30px;
  font-weight: 700;
  /*padding-bottom: 50px;*/
}


.page-section {
   padding: 8px;
}

.navigation {
  position: fixed;
  width: 30%;
  margin-left: 2%;
  background-color: #265a88;
  color: #fff;
  padding: 18px 18px 18px 18px;
  text-decoration: none;
    /*box-shadow: 16px -14px 6px -13px #111;*/
}
.navigation__link {
  display: block;
  text-decoration: none;
  
  color: #265a88;
   height: 25px;
    padding: 4px;
    text-align: center;
  
    
	    


}


.navigation__link:hover {
  
  color: #265a88;
      box-shadow: 0px 0px 2px 1px #111;
 



}



nav {
	
 
 
 /* float: right;*/
  
  box-shadow: 1px 1px 6px rgba(0, 0, 0, 0.5);
}



.link-1 {
 color: black;
 text-decoration: none;
 text-align: center;
  float: right;
  margin: 10 50px;
  width: 80px;
  
  
}
.link-1:hover { 
  text-decoration: none;
  color:black;
}
.a:hover{
	text-decoration:none;
}



body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #ffff;
}

.topnav a {
  
  display: block;
  color: #f2f2f2;
  text-align: center;
  
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  
  color: black;
}

.topnav a {
  margin-top:10px; 
  color: black;
}
hr.new4 {
  border: 1px solid white;
      margin-top: 8px;

}
.h1, .h2, .h3, h1, h2, h3 {
    margin-top: 5px;
    margin-bottom: 1px;
}
p {
    margin: -10px 0 10px;
}
.f1 {
    font-family: 'Lobster', cursive;
}



.active #linkline {
    height: auto;
    font-size : 16px;
    color     : white;
    font-weight: bold;
    padding: 10px;
    width: fit-content;

}

#linkline {
    height: auto;
    font-size : 16px;
    color     : white;
    font-weight: bold;
    padding: 0.1px;

}

.active #linkline:after {
    display      : block;
    content      : '';
    border-bottom: solid 2px white;
    transform    : scaleX(1);
    transition   : transform 300ms ease-in-out;
     transform: scaleX(1);
}


a:focus, a:hover {
    
    text-decoration: none;
}
.f2 {
    font-family: 'Lobster', cursive;
}



#cursive {
    height: auto;
    font-size : 28px;
    
    color     : #000;
    opacity   : 0.6;

}
	






@media (min-width:386px)  {  
	#mainNav {
 display: none;
}
	
}
 
@media (min-width:480px)  {  

}
@media (min-width:600px)  { 
	
}

@media (min-width:993px)  {
#mainNav {
 display: block;
} 
	
 }
@media (min-width:1025px) { 
	#mainNav {
 display: block;
}
 }
@media (min-width:1281px) {
	#mainNav {
 display: block;
}
 }

 @media (min-width: 1200px){

}

@media (min-width: 768px) {

  
}

</style>







<header>
	<div class="container-fluid" style="background: #fff; height: 70px;">
		<div class="row" >
		
		

<div class="container-fluid topnav" id="myTopnav">
	<img src="assets/img/e_campus.png" alt="Logo" style="max-width: 500px;height: 60px; padding-left: 50px; padding-top:6px;">
  <a href="home" class="active link-1 btn btn-primary">Home</a>
  
</div>


	</div>
</header>










<div class="container-fluid" style="margin-top: 3px;">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-5">
			<nav class="navigation" id="mainNav">
	<h2 class="Contents__link text-align active" style="color: #ffff; text-align:center;">Contents</h2>

				<hr class="new4">
	
			</i>


									



			
			<a href="#1" class="active"><h4 id="linkline" ><p>STEP 1: Sign up for your Account </p></h4></a>

			
					
   				<a href="#2" class="scroll"><h4 id="linkline" ><p>STEP 2 : You will view a box </p></h4></a>	
					
			
			<a href="#3" class="scroll"><h4 id="linkline" ><p>STEP 3 : Click on Exam Button in the sub dropbox  </p></h4></a>		
			
			
			
			<a href="#4" class="scroll"><h4 id="linkline" ><p>STEP 4: Add <strong>"+"</strong> an exam button</p></h4></a>		
			
					
			<a href="#5" class="scroll"><h4 id="linkline"><p>STEP 5 : View the page </p></h4></a>	
					

			<a href="#6" class="scroll anchor"><h4 id="linkline"><p>STEP 6: Fill up the information in the boxes & click on Next.</p></h4></a>

			<a href="#7" class="scroll anchor"><h4 id="linkline"><p>STEP 7: Click on Add Question.</p></h4></a>

			<a href="#8" class="scroll anchor"><h4 id="linkline"><p>STEP 8: Click on "+ Add Question" On the page. <strong>"Add Question"</strong></p></h4></a>

			<a href="#9" class="scroll anchor"><h4 id="linkline"><p>STEP 9: Select the form of Questions.</p></h4></a>

			<a href="#10" class="scroll anchor"><h4 id="linkline"><p>STEP 10: Write the questions & Click on Submit. </p></h4></a>

			
			<a href="#11" class="scroll anchor"><h4 id="linkline"><p>STEP 11: Repeat 8,9,&10 Step view Questions in table. </p></h4></a>

			<a href="" class="scroll anchor"><h4 id="linkline"><p>You are compleated with your added syllabus.</p></h4></a>


</nav><br><br> </div> <div class="col-md-7"> <div class="page-section hero"
id="1"> <p id="cursive">How to add exam</p>
				

			<p id="cursive">Follow the following process to add exam.</p>

			<h3 style="color: red; font-weight: bold;">STEP1 : </h3>  <h4 style="margin-bottom: 0px;"> Sign up for your Account</h4>
			<img src="assets/img/help/signin.png" class="img-responsive">
			<hr style="border-top: 1px; solid #fd610582;">
</div>

<div class="page-section" id="2">
			<h3 style="color: red; font-weight: bold;">STEP 2: </h3><h4>Once you complete the Signing up the Following page will appear. </h4><br> 
			<img src="/assets/img/help/add_exam/ste-p_2.png" class="img-responsive">
			<hr style="border-top: 1px solid #fd610582; margin-top: 75px;
    margin-bottom: 20px;">
			
</div>

<div class="page-section" id="3">
			<h3 style="color: red; font-weight: bold;">STEP3:</h3><br>
			<h4>Click on the <strong>"Exam"</strong> & then on  <strong>"Exam"</strong> in the Sub dropbox.</h4>
			<img src="assets/img/help/add_exam/Step_3_exam.png" class="img-responsive" style="padding-bottom: 60px;">
			
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="4">
			<!-- <h4>(B) Click on Add Exam</h4> -->
			
			<h3 style="color: red; font-weight: bold;">STEP 4 :</h3><br>
			<h4>  Click on <strong>"+ Add an Exam" <strong></h4>
			<img src="assets/img/help/add_exam/Step_3.1_exam.png" class="img-responsive" style="padding-bottom: 60px;">
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="5">
			<h3 style="color: red; font-weight: bold; ">STEP 5 :</h3><br>
			<h4> A new page will appear. </h4>
			
				<img src="assets/img/help/add_exam/step_3.2.png" class="img-responsive">
			</div><br><br><br>
			<hr style="border-top: 1px solid #fd610582;">
<div class="page-section" id="6">
			<h3 style="color: red; font-weight: bold;">STEP 6 :</h3> 
			<h4> On the same page, start filling up all the information in the boxes.</h4>
			<div class="col-md-12">
	<div class="col-md-11"><div class="col-md-1">  i. </div> 	Select the Course.</div>
    <div class="col-md-11"><div class="col-md-1"> ii. </div>  	Select the semester.</div> 
  	<div class="col-md-11"><div class="col-md-1"> iii.</div> 	Select the subjects.</div>
  	<div class="col-md-11"><div class="col-md-1"> iv. </div> 	Select the Exam name.</div>
  	<div class="col-md-11"><div class="col-md-1"> v.  </div> 	Description <span style="font-weight: 400;">(Here you can give a 2-3 line NOTE to students as given in the following image)</span></div>
  	
  			</div>



			<img src="assets/img/help/add_exam/step_4.png" class="img-responsive" style="padding-bottom: 120px;">
			  <p> vi. Start Date <span style="font-weight: 400;">(Schedule the Start date and time for the exam)</span></p>
 	            <p> vii.  End Date <span style="font-weight: 400;">(select the end date and time for the exam)</span></p>
 	
			<img src="assets/img/help/add_exam/step_4.2.png" class="img-responsive" style="padding-bottom: 60px;">


			<p> viii.  Fill the duration for the exam.</p>
        	   <p> ix.  Fill the score for the correct answer.</p>
  	           <p>  x. Fill the score for incorrect answers.</p>
  	           <h4>On the same page click on "Next".</h4>
			<img src="assets/img/help/add_exam/step_4.3.png" class="img-responsive" style="padding-bottom: 60px;">

				
</div>



<div class="page-section" id="7">
			<h3 style="color: red; font-weight: bold;">STEP 7:</h3><br>
			<h4> You will get a New page, Click on Add Questions.</h4>
	          <img src="assets/img/help/add_exam/step_5.png" class="img-responsive" style="padding-bottom: 60px;">

	          

	         
			
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="8">
			<h3 style="color: red; font-weight: bold;">STEP 8:</h3><br>
			<h4> Click on <b>"+ Add Question"</b> on the page. </h4>
	          <img src="assets/img/help/add_exam/step_5.1.png" class="img-responsive" style="padding-bottom: 60px;">
	          <hr style="border-top: 1px solid #fd610582;">
</div>


 

<div class="page-section" id="9">
			<h3 style="color: red; font-weight: bold;">STEP 9:</h3><br>
			<h4>Select the forms of questions you want to have.</h4>

			<img src="assets/img/help/add_exam/step_5.2.png" class="img-responsive" style="padding-bottom: 60px;">   
			
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="10">
			<h3 style="color: red; font-weight: bold;">STEP 10:</h3><br>
			<h4> Write the Questions & Click on <b>"Submit"</b></h4>
			<img src="assets/img/help/add_exam/step_5.3.png" class="img-responsive" style="padding-bottom: 60px;">
			
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="11">
			<h3 style="color: red; font-weight: bold;">STEP 11	:</h3><br>
			<h4> For adding more question repeat <strong>"Step 8,9 & 10 "</strong>. You can view your question on the table.</h4>
			<img src="assets/img/help/add_exam/step_20.png" class="img-responsive" style="padding-bottom: 100px;">
			
		
</div>





	
	</div>
	</div>
</div>

		</div>
		

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>


	$(document).ready(function() {
		$('a[href*=#]').bind('click', function(e) {
				e.preventDefault(); // prevent hard jump, the default behavior

				var target = $(this).attr("href"); // Set the target as variable

				// perform animated scrolling by getting top-position of target-element and set it as scroll target
				$('html, body').stop().animate({
						scrollTop: $(target).offset().top
				}, 600, function() {
						location.hash = target; //attach the hash (#jumptarget) to the pageurl
				});

				return false;
		});
});

$(window).scroll(function() {
		var scrollDistance = $(window).scrollTop();

		// Show/hide menu on scroll
		//if (scrollDistance >= 850) {
		//		$('nav').fadeIn("fast");
		//} else {
		//		$('nav').fadeOut("fast");
		//}
	
		// Assign active class to nav links while scolling
		$('.page-section').each(function(i) {
				if ($(this).position().top <= scrollDistance) {
						$('.navigation a.active').removeClass('active');
						$('.navigation a').eq(i).addClass('active');
				}
		});
}).scroll();



$(document).ready(function() {
    $(document).on("scroll", onScroll);

  function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
        });
    }
})
</script>