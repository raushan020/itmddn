<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>Fee And Account Management System For Students- E-Campus</title>
    <meta name="description" content="E-Campus provides the best Fee And Account Management System for students that includes Management of store inventories,Payroll management and many more.">

    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020|Fee And account Management|Fee And account Management System|Student Fee management|E-Campus Fee And account Management System|Best Fee And account Management System|Top Fee And account Management System">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex, nofollow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="eCampus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire">
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="E-Campus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='E-Campus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='E-Campus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="E-Campus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>

</head>
<<body>
<?php include('header.php'); ?>

    <section class="hero-area heroV4" style="margin-top: 100px;">

        <div class="container">

            <div class="hero-content">

                <div class="row">

                    <div class="col-md-4">

                        <div class="hero-img heroImg-s3">

                            <img src="assets/img/section-img/heroV3-1.png" alt="" class="hm_img6">

                        </div>

                    </div>

                    <div class="col-md-8">

                        <div class="hero-text">

                            <h2>Fee And account Management</h2>

                            <p>It can be quite unmanageable for the administration department of any university to take care of theincoming or outgoing finances. The finances can relate to payment of course fee or examination fee by the students. It would be difficult to track who paid the fee and who is due to pay if it’s all on the paper-based documents. </p>

                            <p>The Fee and Account Management (FAM) Module lets you organize the fee payment structure for different type of students in an orderly manner with no hassle and confusion. Not only it helps with the student payments but also with the salary processing of the professors and the college staff as well. All can be taken care of at a single spot.</p>

                            <div class="hero-btn">

                                <a href="#" class="btn-style-1_b">Request Demo</a>

                                <!-- <a href="#" class="btn-style-1">Start For Free</a> -->

                            </div>

                        </div>

                    </div>

                    

                </div>

            </div>

        </div>

    </section>

    <!-- /Hero Area -->



    <section class="about-area3 section-padding">

        <div class="container">

            <div class="row">

                <div class="section-title">

                    <h3>FAM to your rescue!</h3>

                    <!-- <p>We believe in simplifying things which is the beauty of E-Campus. We have figured the UMS module with the object to keep things free from hassle and chaos.

                    </p> -->

                </div>

            </div>

            <div class="row">

                <div class="col-md-5">

                    <div class="section-text">

                        <p>FAM comes handy in creating vouchers for admissions receipts, fee collection, staff salary payment, inventory sales and others. </p>

                        <p>The options available under this head are highly customizable as per the needs and requirements. </p>

                        <!-- <p>The Admins can view the total no. of students enrolled, no. of professors, total revenue earned and other kinds of statistics on the homepage of the portal itself.</p> -->

                    </div>

                </div>

                <div class="col-md-7">

                    <div class="section-img about3-img">

                        <img src="assets/img/section-img/heroV2-1.png" alt="">

                        <div class="hs2-mini-img">

                            <!-- <img src="assets/img/section-img/heroV2-1_mini2.jpg" alt="" class="a3_1"> -->

                            

                            <!-- <img src="assets/img/section-img/heroV2-1_mini1.jpg" alt="" class="a3_2"> -->

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <section class="about-area3 section-padding" style="background: #f1f4f9;">

        <div class="container">

            <div class="row">

                <div class="section-title">

                    <h3>The features of FAM involve:</h3>

                </div>

                <div class="col-md-6">

                        <div class="au-list-box">

                            <ul class="au-list">

                                <li><h5>Student Fee management</h5></li>

                                <li><h5>Processing of employees’ salaries</h5></li>

                                <li><h5>Management of store inventories</h5></li>

                                <li><h5>Payroll management</h5></li>

                                <li><h5>Visitors management</h5></li>
                                <li><h5>Late fee management</h5></li>
                            </ul>

                    </div>

                </div>

                <div class="col-md-6">
                 <div class="au-list-box">

                            <ul class="au-list">

                                

                                <li><h5>Obtain graphs and pie charts related to:</h5>
                                    <ol  class="au-list1">
                                        <li><h6>Total revenue incurred</h6></li>
                                        <li><h6>Total amount paid</h6></li>
                                        <li><h6>Total amount due</h6></li>
                                        <li><h6>Payment type/modes </h6></li>
                                    </ol>
                                </li>

                                <li><h5>…and many more.</h5>
                                </li>
                            </ul>

                    </div>
                </div>

            </div>

        </div>

    </section>



<!--     <section class="about-area3 section-padding" style="background: #f1f4f9;">

        <div class="container">

            <div class="row">

                <div class="section-title">

                    <h3>Verticals in E-Campus University Management:</h3>

                </div>

                <div class="col-md-6">

                        <div class="au-list-box">

                            <ul class="au-list">

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>    

                            </ul>

                    </div>

                </div>

                <div class="col-md-6">

                        <div class="au-list-box">

                            <ul class="au-list">

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>

                                <li><h5>Manage huge chunks of students as well as professors’ data</h5>

                                    <ul>

                                        <li><h6>Manage huge chunks of students as well as professors’ data</h6></li>

                                    </ul>

                                </li>    

                            </ul>

                    </div>

                </div>

            </div>

        </div>

    </section> -->



    <?php include 'footer.php';?>