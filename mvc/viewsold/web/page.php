<?php
$productinfo = "Pay E-Campus";
$txnid = time();
$surl = $surl;
$furl = $furl;        
$key_id = RAZOR_KEY_ID;
$currency_code = $currency_code;            
// $total = 100.00;
// $amount = 100.00;
$merchant_order_id = rand(10,1000) ;
$card_holder_name = '';
// $email = $siteinfos->email;
// $phone = $siteinfos->phone;
$name = "E-Campus";
$return_url = base_url().'Razorpay/callback';
?>
<!DOCTYPE html>

<html class="white-bg-login" lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <title>Pay Now</title>

    <!-- <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" /> -->

    <!-- bootstrap 3.0.2 -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- font Awesome -->

    <link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet"  type="text/css">

     <link href="<?php echo base_url('assets/etslabs/Mainlogin.css'); ?>" rel="stylesheet"  type="text/css">
     <style type="text/css">
         .wrapper{
            background: none;
         }
     </style>

</head>



<body class="" >
    <div class="wrapper">

        <div class="login-form">
<!-- 
            <center><img src="<?php echo base_url() ?>assets/img/e-campus-logo.png" alt="Logo" class="img-responsive"/ style="height:100px;">
                
            </style></center> -->

            <form method="post" id="razorpay-form" action="<?php echo $return_url; ?>" class="form-data">

                <h2 class="text-center">Pay Now</h2>

           <!-- <?php

            if($form_validation == "No"){

            } else {

                if(count($form_validation)) {

                    echo "<div class=\"alert alert-danger alert-dismissable\">

                        <i class=\"fa fa-ban\"></i>

                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>

                        $form_validation

                    </div>";

                }

            }

            if($this->session->flashdata('reset_success')) {

                $message = $this->session->flashdata('reset_success');

                echo "<div class=\"alert alert-success alert-dismissable\">

                    <i class=\"fa fa-check\"></i>

                    <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>

                    $message

                </div>";

            }

        ?>
 -->
            <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
           <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
           <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
           <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="<?php echo $productinfo; ?>"/>
           <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
           <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
           <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/>
           <!-- <input type="hidden" name="billing_invoiceID" id="billing_invoiceID" value="<?php if($invoice){ echo $billing_invoiceIDs;} ?>"/> -->
            <input type="text" disabled name="payamount" id="amountvalue" placeholder="Payment Amount" value="<?=set_value('username',$_GET['amount'])?>">

                <!-- <div class="positionRelative">

                    <input type="password" name="payment" id="password" placeholder="Enter Password">

                    <div class="positionAbsolutePassword">

                        <img src="<?php echo base_url() ?>uploads/images/eye.png" onclick="passwordShow()">

                    </div>

                </div>
 -->
                <!-- <div class="row">

                    <div class="col-md-6">

                        <label class="container">Remember Me

                            <input type="checkbox" name="remember">

                            <span class="checkmark"></span>

                        </label>

                    </div>

                    <div class="col-md-6 text-right">

                        <a href="<?=base_url('reset/index')?>">Forgot Password?</a> 

                    </div>

                </div> -->

                  <input type="button" class="btn btn-signin" onclick="razorpaySubmit();" value="Proceed to pay" />

            </form>

        </div>

    </div>

    <style type="text/css">

            .positionRelative{

      position:relative;

    }

    .positionAbsolutePassword{

    position:absolute;

    top:0px;

    position: absolute;

    top: 5px;

    right: 23px;

    }

    </style>

    <script type="text/javascript">

  function passwordShow() {

    var x = document.getElementById("password");

    if (x.type === "password") {

        x.type = "text";

    } else {

        x.type = "password";

    }

}

</script>


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var amountPaisa =   document.getElementById('amountvalue').value;
  var amount=amountPaisa*100;
  // alert(amountPaisa);
   var razorpay_options = {
   key: "<?php echo $key_id; ?>",
   amount: amount,
   name: "<?php echo $name; ?>",
   description: "Order # <?php echo $merchant_order_id; ?>",
   netbanking: true,
   currency: "<?php echo $currency_code; ?>",
   prefill: {
     name:"<?php echo $card_holder_name; ?>",
     email: "<?php echo $_GET['email']; ?>",
     contact: "<?php echo $_GET['phone']; ?>"
   },
   notes: {
     soolegal_order_id: "<?php echo $merchant_order_id; ?>",
   },
   handler: function (transaction) {
       document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
       document.getElementById('razorpay-form').submit();
   },
   "modal": {
       "ondismiss": function(){
           location.reload()
       }
   }
 };

 var razorpay_submit_btn, razorpay_instance;

 function razorpaySubmit(el){
   // alert("nc");
   // $('#modal-2').hide();
   if(typeof Razorpay == 'undefined'){
     setTimeout(razorpaySubmit, 200);
     if(!razorpay_submit_btn && el){
       razorpay_submit_btn = el;
       el.disabled = true;
       el.value = 'Please wait...';  
     }
   } else {
     if(!razorpay_instance){
       razorpay_instance = new Razorpay(razorpay_options);
       if(razorpay_submit_btn){
         razorpay_submit_btn.disabled = false;
         razorpay_submit_btn.value = "Pay Now";
       }
     }
     razorpay_instance.open();
   }
 }  
 
</script>

</body>

</html>