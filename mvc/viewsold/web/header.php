<header class="header-area Saased-Header header_2">
        <div class="container-flud" style="margin-right: 40px;
            margin-left: 40px;">
            <div class="menu-area desktop-menu" style="padding: 10px 0;">
                <div class="row">
                    <div class="col-md-2 col-sm col">
                        <div class="logo">
                            <a href="home" title="Strick">
                                <img src="assets/img/e_campus.png" alt="Logo" style="max-width: 300px;height: 60px;">
                            </a>
                        </div>
                    </div>
            <style>
               .images img{
                  margin-right: 30px;
               }
            </style>
                    <div class="col-md-7 col-sm col" style="padding: 5px 0;">
                        <div class="main-menu">
                            <nav>
                                <ul id="mobile_menu" class="mainMenu">
                                    <li class="active"><a href="#">Product Tour</a>
                                    </li>
            
                                    <li><a href="pricing" style="padding-right: 15px;">Pricing</a></li>
                                    <li><a href="about" style="padding-right: 15px;">Company</a></li>
                                    <li><a href="contact">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm col">
                     <div class="main-menu" style="padding: 5px 0;">
                        <nav >
                           <ul id="" class="mainMenu" style="float: right;">
                              <li style="padding-right: 15px;"><a href="<?php echo base_url('signin'); ?>">Sign In</a></li>
                              <li><a href="#" class="btn-style-rau paddd" data-toggle="modal" data-target="#signinmodal" style="font-weight: 600;
    letter-spacing: 1px;">Start For Free</a></li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
                </div>
            </div>
            <div class="mobile-menu-area">
                <div class="responsive-menu-wrap mobile-menu text-right"></div>
            </div>
        </div>
    </header>
    <!-- /Header Area -->


    <!-- Modal for signup login  -->
<div class="modal fade" id="signinmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="main" style="background: #0c92f3 !important; padding: 15px !important;">
        <h3 style=" color: #fff;text-align: center; padding: 5px 0px; letter-spacing: 0px;">Let’s Get Started</h3>
        <!-- <p style=" color: #fff;text-align: center; "><em>Sign Up for FREE</em></p>  -->
        </div>
     
      <div class="col-md-12">
        <div class="contact-formV1" style="margin: 20px auto;">

                        <form method="post" action="<?php echo base_url('signin/newuser'); ?>" id="cf">

                            <div class="form-group">
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name*" required="">
                            
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name*" required="">
                                <input type="hidden" name="timezone" id="timezone" value="">

                            </div>

                            <div class="form-group">

                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail address*" required="">

                            </div>

                            <div class="form-group">

                                <input type="phone" class="form-control" id="phone" name="phone" placeholder="Phone number*" required="">

                            </div>

                            <div class="form-group">

                                <input type="text" class="form-control" id="institutional" name="institutional" placeholder="Institution Name*" required="">

                            </div>
                            <div style="text-align: center;">
                                <input type="submit" id="submit" value="Start For Free" name="submit" class="cf-btn">
                            </div>
                            <p style="text-align: center;margin-top: 10px;"><em>No credit card required</em></p>
                            

                            <div class="cf-msg"></div>

                        </form>

                    </div>
                </div>
    </div>
  </div>
</div>



  <!-- Modal for demo date and time -->
<div class="modal fade" id="demoreq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="main" style="background: #0c92f3 !important; padding: 15px !important;">
        <h3 style=" color: #fff;text-align: center;padding: 5px 0px; letter-spacing: 0px;">Request Demo</h3>
        </div>
      <div class="col-md-12">
        <div class="contact-formV1" style="margin: 20px auto;">

                        <form method="post" action="<?php echo base_url() ?>home/mailer" id="cf">

                            <div class="form-group">

                                <input type="text" class="form-control" id="first_Name" name="firstName" placeholder="Full Name*" required="">

                            </div>

                            <div class="form-group">

                                <input type="email" class="form-control" id="e_mail" name="email" placeholder="E-mail*" required="">

                            </div>

                            <div class="form-group">

                                <input type="phone" class="form-control" id="phone_no" name="phone" placeholder="Contact number*" required="">

                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="institute" name="institutional" placeholder="Institution Name*" required="">

                            </div>

                            <div class="form-group">

                                <input id="filter-date" class="form-control" name="date_time" type="text" placeholder="Schedule Date & Time*" required="">

                            </div>
                            <div style="text-align: center;">
                                <input type="submit" id="submit" value="Submit" name="submit" class="cf-btn">
                            </div>

                            <div class="cf-msg"></div>

                        </form>

                    </div>
                </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var a=Intl.DateTimeFormat().resolvedOptions().timeZone;
// var options = {
//     timeZone: a,
//     year: 'numeric', month: 'numeric', day: 'numeric',
//     hour: 'numeric', minute: 'numeric', second: 'numeric',
// },
//     formatter = new Intl.DateTimeFormat([], options)
//   var b=formatter.format(new Date());
//   var d = new Date();
// var gmtHours = -d.getTimezoneOffset()/60;
document.getElementById("timezone").value = a;        
</script>
<!-- end -->
    
