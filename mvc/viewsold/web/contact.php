<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>e campus | University Management System | LMS | E-Learning | 2020</title>
    <meta name="description" content="e Campus is a Saas based 'Campus Automation Software' empowering colleges, universities & educational institutions to automate and manage their entire day to day operations. It provides powerful digitalised modules like Learning Management System(LMS), Webcam-based examination, Fee Management among others for improved efficiency and productivity.">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="eCampus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="eCampus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='eCampus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='eCampus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="eCampus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>

</head>
<<body>
<?php include('header.php'); ?>

    <section class="container-fluid bg-blue">
        <div class="row">
            <div class="col">
                <h3 class="text-white p-3 mb-0">Contact Us</h3>
            </div>
        </div>
    </section>

    <section id="location">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.5317837700295!2d77.38761751472879!3d28.613819991679254!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cef8d8012cb07%3A0x202c6b9fea477e52!2s108%2C%20B%20Block%20Rd%2C%20G%20Block%2C%20Sector%2065%2C%20Noida%2C%20Uttar%20Pradesh%20201307!5e0!3m2!1sen!2sin!4v1577438798844!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" class="d-block"></iframe>
        <h4 class="text-center font-weight-normal text-white py-4 bg-pink">
            <span>Reach out to us at: </span><a href="tel:+919911331937">support@edgetechnosoft.com</a>
        </h4>
    </section>

    <section class="contact-details">
        <div class="container">
            <div class="row">
                <div class="col col-sm-5">
                    <h4 class="font-weight-normal p-3">Get in touch</h4>
                    <div class="get-in-touch pl-3">

                    <dl class="row no-gutters">
                        <dt class="col-1 col-sm-1 semi-border text-center"><img src="assets/img/partner/contactimg/location.svg" alt="location" width="20" /></dt>
                        <dd class="col-10 offset-1 col-sm-10 offset-sm-1">
                            <p style="color: #000;">e campus<br/>
                                B-108, Sector-63,
                                Noida, <br>
                                Uttar Pradesh, India - 201301</p></dd>

                        <dt class="col-1 col-sm-1 semi-border text-center"><img src="assets/img/partner/contactimg/phone.svg" alt="Mobile" width="20" /></dt>
                        <dd class="col-10 offset-1 col-sm-10 offset-sm-1">
                            <a href="tel:+919911331937" class=""><strong>+91 9911 3319 37</strong></a>
                        </dd>

                        <dt class="col-1 col-sm-1 semi-border text-center"><img src="assets/img/partner/contactimg/mail.svg" alt="Email" width="20" /></dt>
                        <dd class="col-10 offset-1 col-sm-10 offset-sm-1"><a href="#" class=""><strong>support@edgetechnosoft.com</strong></a></dd>

                        <dt class="col-1 col-sm-1 semi-border text-center"><img src="assets/img/partner/contactimg/link.svg" alt="Website" width="20" /></dt>
                        <dd class="col-10 offset-1 col-sm-10 offset-sm-1"><a href="home" class=""><strong>www.e-campus.in</strong></a></dd>
                    </dl>
                    </div>
                </div>
                <div class="col col-sm-7">
                    <div class="contact-form pt-3">
                        <h4 class="font-weight-normal text-pink px-3">We would love to hear from you!</h4><br>
                        <p class="pl-3">Send us a message</p>
                        <div class="form-wrapper p-3 my-4 mx-auto">
                            <form method="post" action="<?php echo base_url() ?>home/mailerforcontact" id="cf">
                                <div class="form-row">
                                    <div class="col">
                                        <div class=" group mb-4 pb-3">      
                                            <input id="fname" name="fname" class="inputMaterial d-block w-100 p-2" type="text" required>
                                            <span class="highlight"></span>
                                            <span class="bar d-block w-100 position-relative"></span>
                                            <label for="fname" class="font-weight-normal position-absolute">First Name *</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5 offset-md-1">
                                        <div class="group mb-4 pb-3">      
                                            <input id="lname" name="lname" class="inputMaterial d-block w-100 p-2" type="text" required>
                                            <span class="highlight"></span>
                                            <span class="bar d-block w-100 position-relative"></span>
                                            <label for="lname" class="font-weight-normal position-absolute">Last Name *</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="group mb-4 pb-3">      
                                            <input id="e_mail" name="e_mail" class="inputMaterial d-block w-100 p-2" type="email" required>
                                            <span class="highlight"></span>
                                            <span class="bar d-block w-100 position-relative"></span>
                                            <label for="email" class="font-weight-normal position-absolute">Business Email *</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="group mb-4 pb-3">      
                                            <input id="mobile" name="mobile" class="inputMaterial d-block w-100 p-2" type="text" required="">
                                            <span class="highlight"></span>
                                            <span class="bar d-block w-100 position-relative"></span>
                                            <label for="phone" class="font-weight-normal position-absolute">Phone *</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="group mb-4 pb-3">      
                                            <input id="insti_tution" name="insti_tution" class="inputMaterial d-block w-100 p-2" type="text" required>
                                            <span class="highlight"></span>
                                            <span class="bar d-block w-100 position-relative"></span>
                                            <label for="institution" class="font-weight-normal position-absolute">Institution *</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col group mb-4 pb-3">      
                                        <input id="requirement" name="requirement" class="inputMaterial d-block w-100 p-2" type="text" required>
                                        <span class="highlight"></span>
                                        <span class="bar d-block w-100 position-relative"></span>
                                        <label for="institution" class="font-weight-normal position-absolute">Requirement *</label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-outline-danger">Talk to our Solutions Expert!</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


 <?php include 'footer.php';?>