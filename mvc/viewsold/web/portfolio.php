<?php include 'header.php';?>
<!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h3>Sean Portfolio</h3>
                    </div>
                </div>
            </div>
        </div>     
    </section>
    <!-- /Breadcrumb Area -->

    <!-- Portflio Area -->
    <section class="portfolio-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group btn-group-toggle shuffleFilter psFilter" data-toggle="buttons">
                        <label class="btn active">
                            <input type="radio" name="shuffle-filter" value="all" checked="checked" />All
                        </label>
                        <label class="btn">
                            <input type="radio" name="shuffle-filter" value="design" />Design
                        </label>
                        <label class="btn">
                            <input type="radio" name="shuffle-filter" value="photography" />Photography
                        </label>
                        <label class="btn">
                            <input type="radio" name="shuffle-filter" value="UIdesign" />UI Design
                        </label>
                        <label class="btn">
                            <input type="radio" name="shuffle-filter" value="UXdesign" />UX Design
                        </label>
                        <label class="btn">
                            <input type="radio" name="shuffle-filter" value="branding" />Branding
                        </label>
                        <label class="btn">
                            <input type="radio" name="shuffle-filter" value="prototype" />Prototype
                        </label>
                                    
                    </div>
                </div>
            </div>
            <div class="portfolio-shuffle">
                <div class="single-shuffle" data-groups='["design"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-1.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                            <a data-fancybox="gallery" href="assets/img/portfolio/p-1.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["UIdesign","branding"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-2.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-2.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["design","UIdesign"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-3.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-3.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["UXdesign"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-4.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-4.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["branding","prototype"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-5.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-5.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["photography","UXdesign"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-6.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-6.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["prototype","photography"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-7.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-7.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["design","UXdesign"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-8.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-8.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="single-shuffle" data-groups='["branding","photography"]'>
                    <div class="ssp-img">
                        <img src="assets/img/portfolio/p-9.jpg" alt="">
                    </div>
                    <div class="ssp-hover">
                        <a data-fancybox="gallery" href="assets/img/portfolio/p-9.jpg"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </div>
                </div>
                
                <div class="col-1 Ssizer-element"></div>
            </div>
        </div>
    </section>
    <!-- /Portflio Area -->

    <!-- Newsletter Area -->
    <section class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="news-letter-box">
                        <form class="nl-form">
                            <input type="email" name="EMAIL" id="email" placeholder="Enter Email To Subscribe">
                            <button type="submit" class="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Newsletter Area -->
<?php include 'footer.php';?>