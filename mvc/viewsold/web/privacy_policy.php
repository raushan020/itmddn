<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>e campus|University Management System|LMS|Smart Classes|E-learning|2020</title>
    <meta name="description" content="E-Campus is a Software-as-a-Service (SaaS) platform that  provides various services to institutions for managing all Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions on the cloud for universities, colleges and educational institutions |2020 ">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="E-Campus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="E-Campus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='E-Campus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='E-Campus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="E-Campus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>

</head>
<<body>
<?php include('header.php'); ?>
<section class="pricing-area" style="margin-top: 89px;background: #0c92f3;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title1">
                        <h3>e campus Privacy Policy
                        </h3>
                        <p>This Privacy Policy describes our responsibility towards privacy on how we collect, use, maintain,share, and disclose your information, including personal as well as other important information, in conjunction with your access to and use of our website, www.e-campus.in (“Website”).</p>

                            <p>When this policy mentions “e campus,” “we,” “us,” or “our”, it refers to Edge Technosoft Pvt. Ltd., the company that is responsible for your information under this Privacy Policy.</p>
                           
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="about-area3 section-padding" style="background: #f1f4f9;">

        <div class="container">

            <div class="row">
                

                        <div class="">
                        	<ol>
    <li><strong>Our Commitment To User(s) Privacy</strong>
        <ol>
            <li>This privacy policy sets out how e campus uses and protects any information that you give e campus when you use this website.<br>
            We provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. The privacy policy is available on our homepage for it easy availability and at every point where personally identifiable information may be requested.<br>
            e campus may change the privacy policy from time to time by updating the page. The users are requested to check the privacy page from time to time to ensure that you are satisfiedwith any of the changes made so forth. This policy is effective from October 1, 2019.
            </li>
        </ol>
    </li>

    <li><strong>The Information We Collect</strong><br>
    	This notice is applicable to all the information or data collected or submitted on the e campus website. On our contact page, you can make an inquiry, request(s), and register to receive information regarding the services and products we specialize in. <br><br>
    	<strong>The types of personal information collected at these pages are:</strong>
        <ol>
            <li>Name</li>
            <li>Company</li>
            <li>Email address</li>
            <li>Phone number</li><br>
            <strong>By providing this information you approve to be contacted by e campus.</strong>
        </ol>

    </li>

    <li><strong>The Way We Use Information</strong><br>
    	We make use of the information providedby about you while sending an inquiry only to respond to that inquiry. We do not share this information with any outside or third parties except to the extent necessary to answer your inquiry(s).<br>

    	We use return email addresses to answer the emails we receive. Such addresses are not used for any other purpose and are not shared with outside orany third parties.<br>

    	You can register with our website if you would like to receive our newsletter as well as updates on our new products and services. Information you submit on our website will not be used for this purpose unless you fill out the subscription form. We may use the information to customize the website according to your interests.<br>

    	Finally, we never make use of or share the personally identifiable information provided to us online in ways unrelated to the ones described above without also providing you an opportunity to opt-out or otherwise prohibit such unrelated uses.
    </li>

    <li><strong>Our Commitment To Security of Information</strong><br>
    	We are pledged to ensuring that your information is safe and secure with us. To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have placedrelevant physical, electronic, and managerial procedures to safeguard and secure the details that we collect online.
    </li>

    <li><strong>Our Cookie Policy</strong><br>
    	A cookie is a small piece of information or data that is sent from a website, whenever an individual accesses a particular website, and stored into an individual’s computer system through the web browser on which that website had been accessed.<br>
    	Cookie asks for permissionto store or remember an individual’s record from their previous visit to the website. Once you agree, the data or information is added in the form of a file in your system.Cookies allow customized web pages or and asks for permission to save site login information for the user so that they do not have to login time and again.<br>
    	Websites may use "cookies" to enhance user experience.You can either accept or decline cookies. Most web browsers automatically accept cookies, but you can alter your browser cookie settings according to your preferences. However, disabling cookies may affect some parts of the Website to not function effectively. 
    </li>

    <li><strong>Links To Other Websites</strong><br>
    	Our website may contain links to other pages in order to enable you to visit other websites of interest easily. However, once you choose or click those links to leave our site, you should note that we do not have any control over the other websites. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. <br>
    	<strong>You should exercise caution and look at the privacy statement applicable to the other website in question.</strong><br><br>

    	We will not sell, distribute or lease your personal information to third parties unless we are permitted to do so by you or are required by law to do so. We may only use your personal data to sendyou promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.

    </li>

    <li><strong>How To Contact Us</strong><br>
    	In case you wish to contact us regarding any questions, inquiries or concerns about these privacy policies, <br>
    	break the ice at: <strong>support@edgetechnosoft.com</strong>

    </li>

    
</ol>


                    </div>

            </div>

        </div>

    </section>


<?php include 'footer.php';?>