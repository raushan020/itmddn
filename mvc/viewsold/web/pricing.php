<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>eCampus|University Management System|LMS|Smart Classes|E-learning|2020</title>
    <meta name="description" content="eCampus is a Software-as-a-Service (SaaS) platform that  provides various services to institutions for managing all Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions on the cloud for universities, colleges and educational institutions |2020 ">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="og:title" property="og:title" content="eCampus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
<meta property="og:title" content="eCampus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />
<meta property='og:title' content='eCampus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='eCampus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<meta property="og:title" content="eCampus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="eCampus"/>
<?php include('head.php'); ?>
</head>
<body>
<?php include('header.php'); ?>
    <section class="pricing-area" style="background: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title" style="max-width: 700px; margin: 120px auto 40px;">
                        <h3>Choose the right plan for your institution</h3>
                        <p>Get started for free, or select a flexible plan based on your institution's needs</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="pricing-content">
                        <div class="single-pricing focused yearly-packege">
                            <div class="spricing-info">
                                <div class="spi-head">
                                    <h4>Basic</h4>
                                    <p>Ideal for 100 Students</p>
                                    <p style="color: #000;font-weight: 600;text-transform: uppercase; font-size: 14px;">FREE FOREVER</p>
                                    <h3><span class="fsize"><i class="fa fa-dollar"></i></span> 0</h3><br><br>
                                </div>
                                <ul class="spi-list">
                                	<li>10 GB Data Storage</li>
                                    <li>100 User Access</li>
                                    <li>Data Management</li>
                                    <li>All User Interface</li>
                                    <li>Email Support</li>
                                    <br>
                                    
                                </ul>
                                <a href="#" class="btn-style-1_c" data-toggle="modal" data-target="#signinmodal">Try Basic</a>
                            </div>
                            <p style="font-size: 12px;">no credit card required</p>
                        </div>
                        <div class="single-pricing focused yearly-packege">
                            <div class="s-pricing-icon">
                                <p>Popular</p>
                            </div>
                             <div class="spricing-info">
                                <div class="spi-head">
                                    <h4>Essential</h4>
                                    <p>Ideal for 500-5000 Students</p>
                                    <p style="color: #000;font-weight: 600; text-transform: uppercase; font-size: 14px;">For small & medium colleges</p>
                                    <h3><span class="fsize"><i class="fa fa-dollar"></i></span> 2.5</h3>
                                    <p style="font-size: 12px;">per user / month (billed annually)</p>
                                </div>
                                <ul class="spi-list">
                                	<li>1 TB Data Storage</li>
                                    <li>5000 User Access</li>
                                    <li style="color: #000;"><strong>All in Basic, Plus</strong></li>
                                    <li> Phone Support</li>
                                    <li>API & PG Integration</li>
                                    <li> Dedicated Account Manager</li>
                                </ul>
                                <a href="#" class="btn-style-1_r" data-toggle="modal" data-target="#signinmodal" style="background-color: #0c92f3;">Try Essential</a>
                                <p style="font-size: 12px;">no credit card required</p>
                            </div>
                                 
                        </div>

                        <div class="single-pricing focused yearly-packege">
                               <div class="spricing-info">
                                <div class="spi-head">
                                    <h4>Pro</h4>
                                    <p>Ideal for 5000+ Students</p>
                                    <p style="color: #000;font-weight: 600; font-size: 14px;">For colleges & universities that need extended support, scalability, performance, and flexibility in their automation process.</p>
                                    
                                </div>
                                <ul class="spi-list">
                                	<li>Upto 5 TB Data Storage</li>
                                    <li>Unlimited User Access</li>
                                    <li style="color: #000;"><strong>All in Essential, Plus</strong></li>
                                    <li>Dedicated Server</li>
                                    <li>Custom Domain</li>
                                   <li>Fully Customizable</li>

                                </ul>
                                <a href="#" class="btn-style-1_c" data-toggle="modal" data-target="#demoreq" style="margin-bottom: 26px;">Contact Sales</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </section>

   <!--  compare table start-->
   <section class="pricing-area" style="background: #163654; margin-top: -120px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title" style="padding: 60px 0 20px; margin: 0;">
                        <!-- <img src="assets/img/e-campus-logo.png" alt="Logo" style="margin-top: -8px;max-width: 300px;height: 60px;"> -->
                        <p style="color: #fff; font-size: 18px;">Discover the differences of each individual plan and learn more about our product.</p>
                    </div>
                </div>
                </div>

  <div id="pakete" style="padding-bottom: 40px;">
<div class="box"><!-- The surrounding box -->

  <!-- The front container -->
  <div class="front">
    <table border="0" class="round">
      <tr >
        <th style="width: 40%;">Features</th>
        <th style="width: 20%;">Basic </th>
        <th style="width: 20%;">Essensial</th>
        <th style="width: 20%;">Pro</th>
      </tr>
      <tr>
        <td style="color: #fff;">Super Admin</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Admin & Users</td>
        <td style="font-size: 16px;height:47px;">Limited</td>
        <td class="faicongreen1" style="font-size: 16px;">Unlimited</td>
        <td class="faicongreen1" style="font-size: 16px;">Unlimited</td>
        
      </tr>
      <tr>
        <td style="color: #fff;">Professor & Students</td>
        <td style="font-size: 16px;">Limited</td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
       
      </tr>
      <tr>
        <td style="color: #fff;">Dashboard</td>
        <td ><i class="fa fa-check"aria-hidden="true"></i></td>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
     
      </tr>
      <tr>
        <td style="color: #fff;">Graphical Presentation</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
    
      </tr>
      <tr>
        <td style="color: #fff;">Data Management</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      
      </tr>
      <tr>
        <td style="color: #fff;">Student Dashboard</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Student ID card</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Student Information</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Student Promotion</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Messaging System</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Attendance Tracking</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Student Performance</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Professors Login</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Time-Table</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Subject Substitution</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Learning Management System</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">E-Books & Digital Library</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Examination Module</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Assignment Module</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Instant Result</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Notice</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Bulk Import</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Accountant Login</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Fee Management</td>
        <td><i class="fa fa-check"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Auto Financial Report</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <td style="color: #fff;">Custom E-mail</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <td style="color: #fff;">Placement Analytics</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <td style="color: #fff;">Customisations</td>
        <td><i class="fa fa-close"aria-hidden="true"></i></td>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
    </table>    
  </div>
<div class="responsive-tables">
  <div class="little1 active">
    <table>
      <tr>
        <th>PLAN</th>
        <th>Basic</th>
      </tr>
      <tr>
        <th>Super Admin</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Admin & Users</th>
        <td class="faicongreen1">Limited</td>
      </tr>
      <tr>
        <th>Professor & Students</th>
        <td class="faicongreen1">Limited</td>
      </tr>
      <tr>
        <th>Dashboard</th>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Graphical Presentation</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Data Management</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Dashboard</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student ID card</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Information</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr><tr>
        <th>Student Promotion</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Messaging system</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Attendance Tracking</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Performance</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Professors login</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Subject Addition</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Time-Table</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Subject Substitution</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Learning Management System</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>E-Books & Digital Library</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Assignments Module</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Instant Result</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Notice</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Bulk Import</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Accountant login</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Fee Management</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Auto Financial Report</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Custom E-mail</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <th>Placement Analytics</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <th>Customisations</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
    </table>
  </div>
  <div class="little2 activetwo">
    <table>
      <tr>
        <th>PLAN</th>
        <th>Essensial</th>
      </tr>
      <tr>
        <th>Super Admin</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Admin & Users</th>
        <td class="faicongreen1">Limited</td>
      </tr>
      <tr>
        <th>Professor & Students</th>
        <td class="faicongreen1">Limited</td>
      </tr>
      <tr>
        <th>Dashboard</th>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Graphical Presentation</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Data Management</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Dashboard</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student ID card</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Information</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr><tr>
        <th>Student Promotion</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Messaging system</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Attendance Tracking</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Performance</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Professors login</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Subject Addition</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Time-Table</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Subject Substitution</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Learning Management System</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>E-Books & Digital Library</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Examination Module</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Assignments Module</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Instant Result</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Notice</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Bulk Import</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Accountant login</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Fee Management</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Auto Financial Report</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Custom E-mail</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <th>Placement Analytics</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <th>Customisations</th>
        <td><i class="fa fa-close faiconred1"aria-hidden="true"></i></td>
      </tr>
    </table>
  </div>
  <div class="little3">
    <table>
      <tr>
        <th>PLAN</th>
        <th>PRO</th>
      </tr>
      <tr>
        <th>Super Admin</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Admin & Users</th>
        <td class="faicongreen1">Limited</td>
      </tr>
      <tr>
        <th>Professor & Students</th>
        <td class="faicongreen1">Limited</td>
      </tr>
      <tr>
        <th>Dashboard</th>
        <td ><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Graphical Presentation</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Data Management</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Dashboard</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student ID card</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Information</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr><tr>
        <th>Student Promotion</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Messaging system</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Attendance Tracking</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Student Performance</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Professors login</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Subject Addition</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Time-Table</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Subject Substitution</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Learning Management System</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>E-Books & Digital Library</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Examination Module</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Assignments Module</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Instant Result</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Notice</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Bulk Import</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Accountant login</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Fee Management</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Auto Financial Report</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
      <tr>
        <th>Custom E-mail</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <th>Placement Analytics</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
       <tr>
        <th>Customisations</th>
        <td><i class="fa fa-check faicongreen1"aria-hidden="true"></i></td>
      </tr>
    </table>
  </div>
  <button class="next-table entypo-right-dir"></button>
  <button class="prev-table entypo-left-dir"></button>

</div>
                                                                            
</div>
  
</div>
</div>
   
</section>

<section class="faq-area">
        <div class="container">
            <div class="row">

                <div class="section-title" style="max-width: 1000px;padding: 15px 0px;">

                    <h3>Frequently Asked Questions</h3>

                </div>
            </div>
            <div class="faq-box">
                <div class="accordion text-center" id="accordion">
                    <div class="row">
                        <div class="col-md-10 offset-1">
                            <div class="card">
                                <div class="card-header" id="headingFive">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                        How does user licensing work?
                                    </button>
                                </div>
                                <div id="collapse5" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>eCampus Basic plan supports up to 250 users in any account. However, when a 251 th user is added, you need to purchase 251 users paid license i.e., your entire institution has to move to a paid plan, to continue using our service. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                        Can I have my own personalized custom plan as per my requirements?
                                    </button>
                                </div>
                                <div id="collapse6" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Yes, we shall be happy to make a custom plan for you if you're a 500+ member institution.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                        Where will my data be stored on the cloud?
                                    </button>
                                </div>
                                <div id="collapse7" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Your data will be kept securely on the cloud hosted by AWS / BigRock, the names which all the leading companies trust. So, keep your worries at bay!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                        Who will upload the information database over the portal in the initial stage?
                                    </button>
                                </div>
                                <div id="collapse8" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The data will be uploaded by you through our bulk uploading option. However, if you need technical assistance in uploading the data, it shall be provided by our team.</p>
                                    </div>
                                </div>
                            </div>
                                 <div class="card">
                                <div class="card-header" id="headingNine">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                        If I chose eCampus, who will be the owner of the data that is on the portal?
                                    </button>
                                </div>
                                <div id="collapse9" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>It's definitely you, as the data belongs to you and it's your property.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        Is there an option to upgrade or downgrade my subscription or plan?
                                    </button>
                                </div>
    
                                <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Yes, you can upgrade or downgrade your subscription plan any time as per your need.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                        Will the portal go blank if my subscription limit exceeds and I have not renewed the plan?
                                    </button>
                                </div>
                                <div id="collapse2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>No, the data shall be there but the login will get frozen or restricted.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <button class="btn accordion-header collapsed waves-effect" type="button" data-toggle="collapse"
                                        data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        What modes of payments are acceptable?
                                    </button>
                                </div>
                                <div id="collapse3" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>We accept payment via Visa, MasterCard and PayPal. We also accept payment via NEFT bank transfers for yearly subscriptions. For more details, please contact <strong>support@edgetechnosoft.com.</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php';?>
<!-- <style>@import url(https://weloveiconfonts.com/api/?family=entypo);</style> -->

    <script>

  var mediaQuery = window.matchMedia('(min-width: 640px)');
  
  
  mediaQuery.addListener(doSomething);
  
  
  function doSomething(mediaQuery) {    
    if (mediaQuery.matches) {
      $('.sep').attr('colspan',5);
    } else {
      $('.sep').attr('colspan',2);
    }
  }
  
  
  doSomething(mediaQuery);
    </script>

    <script>
$(document).ready( function(){

  // The flipping control-center
  // Two functions for every box :(

// 1
  $(".show-me_1").on("click", function() {
    $(".box").addClass("flip1");
  });
  $(".hide-me_1").on("click", function() {
    $(".box").removeClass("flip1");  
  });
// 2
  $(".show-me_2").on("click", function() {
    $(".box").addClass("flip2");
  });
  $(".hide-me_2").on("click", function() {
    $(".box").removeClass("flip2");
  });
// 3
  $(".show-me_3").on("click", function() {
    $(".box").addClass("flip3");
  });
  $(".hide-me_3").on("click", function() {
    $(".box").removeClass("flip3");
  });

  $('[class*="-me_"]').click( function(){
    var boxclass = $('.box').attr('class');
    $('p.boxclass').html(boxclass);
  });

  
// Table Carousel

$('button.next-table').click( function(){
  var currentone = $('.responsive-tables .active');
  var currenttwo = $('.responsive-tables .activetwo'); 
  var next = $('.responsive-tables .activetwo + [class*="little"]');
  var last = $('.responsive-tables .last');
  next.addClass('activetwo');
  currentone.removeClass('active').addClass('last');
  currenttwo.removeClass('activetwo').addClass('active');
  last.removeClass('last');

  var echonext = next.attr('class');
  //alert(echonext);
  

  if( echonext === undefined ){
    $('.little1').addClass('activetwo');
  }

});

$('button.prev-table').click( function(){
  var currentone = $('.responsive-tables .active');
  var currenttwo = $('.responsive-tables .activetwo'); 
  var prev = $('.responsive-tables .last').prev('[class*="little"]');
  var last = $('.responsive-tables .last');
  prev.addClass('last');
  currentone.removeClass('active').addClass('activetwo'); 
  last.removeClass('last').addClass('active');
  currenttwo.removeClass('activetwo');

  //alert(prev.attr('class'));

  if( prev.attr('class') === undefined ){
    $('.responsive-tables .little9').addClass('last');
  }

});



}); // .ready-END
</script>