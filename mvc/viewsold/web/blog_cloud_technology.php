<?php include 'header.php';?>
<!-- Breadcrumb Area -->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h3>Sean Blog Details</h3>
                    </div>
                </div>
            </div>
        </div>     
    </section>
    <!-- /Breadcrumb Area -->

    <!-- Faq Area -->
    <section class="blog-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-blogV4 blog-details">
                        <div class="sblog-img">
                            <img src="assets/img/blog/blog-details-1-2.jpg" alt="">
                        </div>
                        <div class="sb-text">
                            <h4><a href="#">Design is about point view and there should be some sort of
                                    woman or lifestyle attitude in one's head as a designer.</a></h4>
                            <ul class="sb-meta">
                                <li>Analysis</li>
                                <li>18.12.2018</li>
                            </ul>
                            <p>Every few decades, we have an opportunity to make drastic change to the way we lives. We get a chance to
                                design the building blocks of our daily routines, the that will support accompany us for the years. To
                                design is
                                much more than simply to assemble, to order, or even to edit: it is to add value and meaning, to
                                illuminate, to
                                simplify, to clarify, to modify, to dignify, to dramatize, to persuade, and perhaps even to amuse.
                                To design is to transform prose into poetry.</p>
                            <p>Design is a way of life, a point of view. It involves the whole complex of visual communications:
                                talent, creative
                                ability, manual skill, and technical knowledge. Aesthetics and economics, technology and psychology are
                                intrinsically related to the process. There's design, and there's art. Good design is total harmony.
                                There's no
                                better designer than nature - if you look at a branch or a leaf, it's perfect. It's all function.</p>
                
                            <div class="sbt-img">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="assets/img/blog/blog-details-2-2.jpg" alt="">
                                    </div>
                                    <div class="col-md-6">
                                        <img src="assets/img/blog/blog-details-3-2.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <p>Every few decades, we have an opportunity to make drastic change to the way we lives. We get a chance to
                                design the building blocks of our daily routines, the that will support accompany us for the years. To
                                design is
                                much more than simply to assemble, to order, or even to edit: it is to add value and meaning, to
                                illuminate, to
                                simplify, to clarify, to modify, to dignify, to dramatize, to persuade, and perhaps even to amuse.
                                To design is to transform prose into poetry.</p>
                        </div>
                    </div>
                    <!-- Blogitem Comments -->
                    <div class="blogitem-comments">
                        <h5>Comments</h5>
                
                        <div class="tm-comment-wrapper">
                
                            <!-- Comment Single -->
                            <div class="tm-comment">
                                <div class="tm-comment-thumb">
                                    <img src="assets/img/avatar/comment-1.jpg" alt="author image">
                                </div>
                                <div class="tm-comment-content">
                                    <h6 class="tm-comment-authorname">Robison Croso</h6>
                                    <span class="tm-comment-date">11th December, 2018</span>
                
                                    <p>“Design must be functional, and functionality must be translated into visual aesthetics without
                                        any reliance on gimmicks that have to be explained. Functional and function
                                        ality must be translated into visual aesthetics without any reliance on gimmicks that.”</p>
                                    <a href="#" class="tm-comment-replybutton"><i class="fa fa-reply-all" aria-hidden="true"></i>Reply</a>
                                </div>
                            </div>
                            <!--// Comment Single -->
                            <div class="tm-comment tm-comment-replypost">
                                <div class="tm-comment-thumb">
                                    <img src="assets/img/avatar/comment-2.jpg" alt="author image">
                                </div>
                                <div class="tm-comment-content">
                                    <h6 class="tm-comment-authorname">Robison Croso</h6>
                                    <span class="tm-comment-date">11th December, 2018</span>
                
                                    <p>“Design must be functional, and functionality must be translated into visual aesthetics without
                                        any reliance on gimmicks. Functional and functionality must be translated into visual aesthetics without any
                                        reliance that.”</p>
                                    <a href="#" class="tm-comment-replybutton"><i class="fa fa-reply-all" aria-hidden="true"></i>Reply</a>
                                </div>
                            </div>
                
                
                        </div>
                
                    </div>
                    <!--// Blogitem Comments -->
                
                    <!-- Blogitem Commentbox -->
                    <div class="blogitem-commentbox">
                        <h5 class="small-title">Leave a Reply</h5>
                        <form class="tm-commentbox">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="tm-commentbox-singlefield">
                                        <input type="text" id="tm-comment-namefield" class="form-control" placeholder="Your Name Here">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="tm-commentbox-singlefield">
                                        <input type="email" id="tm-comment-email" class="form-control" placeholder="Email Address Here">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="tm-commentbox-singlefield">
                                        <input type="text" id="tm-comment-website" class="form-control" placeholder="Website URL">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="tm-commentbox-singlefield">
                                        <textarea name="tm-comment-textbox" id="tm-comment-textbox" class="form-control" placeholder="Your Text Here"></textarea>
                                    </div>
                                    <div class="tm-commentbox-singlefield bdc-btn-box">
                                        <button type="submit" class="tm-button">Submit <b></b></button>
                                    </div>
                                </div>
                            </div>
                
                        </form>
                    </div>
                    <!--// Blogitem Commentbox -->
                
                </div>
                
            </div>
        </div>
    </section>
    <!-- /Faq Area -->

    <!-- Newsletter Area -->
    <section class="newsletter-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="news-letter-box">
                        <form class="nl-form">
                            <input type="email" name="EMAIL" id="email" placeholder="Enter Email To Subscribe">
                            <button type="submit" class="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Newsletter Area -->
<?php include 'footer.php';?>