<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--- Basic Page Needs  -->
    
    <title>e campus | University Management System | LMS | E-Learning | 2020</title>
    <meta name="description" content="e Campus is a Saas based 'Campus Automation Software' empowering colleges, universities & educational institutions to automate and manage their entire day to day operations. It provides powerful digitalised modules like Learning Management System(LMS), Webcam-based examination, Fee Management among others for improved efficiency and productivity. ">
    <meta name="author" content="Edge Technosoft Pvt. Ltd.">
    <meta name="keywords" content="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="index, follow"> 
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--<img src="https://e-campus.in/assets/img/e_campus.png" alt="E-Campus">-->
 
<meta name="og:title" property="og:title" content="eCampus|University Management System|LMS|Smart Classes|E-learning|2020"> 
<meta name="og:description" property="og:description" content="The University Management System (UMS) helps in maintaining all the data that matters in an educational institution. It can include student database, employee-centric data, and information pertaining to all the parties to the institution.
The Learning Management System (LMS) delivers the course-related material to a student anytime anywhere. It ensures that the students don’t face boredom while studying anymore.
The Webcam Based Assessment System helps the universities organize and conduct assessments on the electronic platforms in an exact manner that they desire"> 
<meta name="og:type" property="og:type" content="Website">    

<!-- Twitter Card data -->
<meta name="twitter:card" value="Student Management System|Learning Management System|Online Assessment System|Attendance Management System|2020">

<!-- Open Graph data for twitter -->
<meta property="og:title" content="eCampus" />
<meta property="og:type" content="E-Learning" />
<meta property="og:url" content="https://twitter.com/ECampus18" />
<meta property="og:image" content="https://pbs.twimg.com/profile_images/1185095035714330624/08AyCI8q_400x400.jpg" />
<meta property="og:description" content="A leap towards University Digitization" />


<!-- linkedin Card data -->

<meta property='og:title' content='eCampus | E-Learning'/>
<meta property='og:image' content='https://media.licdn.com/dms/image/C510BAQGRR4s5DeFfRw/company-logo_200_200/0?e=2159024400&v=beta&t=KQpecsEVraEowYLi7mlXFSZoFPPsbyiMoaNY72hGH7s'/>
<meta property='og:description' content='eCampus is a SaaS platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions for universities, colleges, and educational institutions.'/>
<meta property='og:url' content='https://www.linkedin.com/company/ecampus/' />
<!-- facebook Card data -->
<meta property="og:title" content="eCampus"/>
<meta property="og:url" content="https://www.facebook.com/E-Campus-115587966511317/"/>
<meta property="og:site_name" content="E-Campus"/>
<?php include('head.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

    <section class="innerpage-area" style="background: #f1f4f9;">

        <div class="container">

            <div class="ip-section-content">

                <div class="row">

                    <div class="col-md-6">

                        <div class="section-text more-about-text">

                            <div class="section-title">

                                <h3>About e campus</h3>

                            </div>

                            <p>e campus is a Software-as-a-Service (SaaS) platform that specializes in Database Management, Student Management System, Learning Management System, Examination System Management, Fee and Account Management & other digitized solutions on the cloud for universities, colleges and educational institutions.</p>

                            <p>e campus is the solitary mixture for all your administrative needs built keeping scale and security in mind. We elucidate that the universities provide seamless UI to its users and students with product engineering as the cornerstone.</p>

                            <div class="section-btn">

                                <h5 style="margin-top: 50px">“Our already existing clients can’t help but give three cheers to us and we are certain you will too! ” <i class="fa fa-smile-o" style="font-size:32px;color:red"></i> </h5>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="section-img more-about-img">

                            <img src="assets/img/section-img/aupc-1.jpg" alt="">

                            <img src="assets/img/section-img/aupc-2.jpg" alt="">

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    
    <!-- Team Area -->

    <section class="team-area" style="background: #f1f4f9;">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="section-title">

                        <h3>Meet The Team</h3>

                        <p>Design is a way of life, a point of view. It involves the whole complex of visual

                                communications: talent, creative ability, manual skill.</p>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-4">

                    <div class="single-team">
                        

                        <div class="st-info">
                            <div class="s2t-client" style="padding-bottom: 10px">
                                <img src="assets/img/avatar/testi2-avatar-1.jpg" alt="">         
                            </div>

                            <h4>FAIZ KHAN </h4>
                            <span class="st-d">Founder & CEO</span><hr>

                            <span class="st-d">Tech & Business</span>
                            

                            <p>Software Analyst (technical lead), Magic Software [2008-2013]</p>
                            <!-- <p>8 years of experience in education industry; expertise in client acquisition</p> -->
                            <p>Start-up Experience: Bootstrapped ‘AdEdge’</p>

                            <ul class="sts-list">

                                <li><a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                <li><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                <li><a href="#" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>


                            </ul>

                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="single-team" >

                        <div class="st-info"style="background-color: #5e2ced;">
                            <div class="s2t-client" style="padding-bottom: 10px">
                                <img src="assets/img/team/anjum.png" alt="e campus">         
                            </div>

                            <h4 style="color: #fff;">ANJUM BADAR</h4>

                            <span class="st-d" style="color: #fff;">Co Founder & COO</span><hr>
                            <span class="st-d" style="color: #fff;">Product & Ops</span>

                            <p style="color: #fff;">Operations, Paytm; expertise in client retention and management</p>
                            <p style="color: #fff;">Start up Experience: Jobsterz</p>

                            <ul class="sts-list">

                                <li><a href="https://www.facebook.com/Anjumbadar" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                <li><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                <li><a href="https://www.linkedin.com/in/anjumbadar/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>


                            </ul>

                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <div class="single-team">

                        <div class="st-info">
                            <div class="s2t-client" style="padding-bottom: 10px">
                                <img src="assets/img/team/mayank.png" alt="" style="height: 90px;">         
                            </div>

                            <h4>MAYANK GUPTA</h4>

                            <span class="st-d">Chief Advisor</span><hr>
                            <span class="st-d">Product & Business Vision </span>

                            <p>Industry veteran with 25+ years in education</p>
                            <!-- <p>Serial entrepreneur with exits ($5 M).</p> -->
                            <p>Actively involved in new university acquisition, sales & product development road</p>

                            <ul class="sts-list">

                                <li><a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                <li><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                                <li><a href="#" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- /Team Area -->


<?php include 'footer.php';?>