
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
	

html,
body {
  height: 100%;
  font-family: Montserrat, Helvetica, Arial, sans-serif;
    font-weight: 400;
    font-size: 15px;
    line-height: 1.55;
    color: #595959;

}


}

h1 {

    color: #333333 !important;
  font-size: 30px;
  font-weight: 700;
  padding-bottom: 50px;
}


.page-section {
   padding: 8px;
}

.navigation {
  position: fixed;
  width: 30%;
  margin-left: 2%;
  background-color: #0c92f3;
  color: #fff;
  padding: 18px 18px 18px 18px;
  text-decoration: none;
    /*box-shadow: 16px -14px 6px -13px #111;*/
}
.navigation__link {
  display: block;
  text-decoration: none;
  padding: 0.35em;
  color: #fff;
  /*text-shadow: 2px 2px 4px #000000;*/

}

.navigation__link:hover {
  background-color: rgba(0, 0, 0, 1);
  color:white;
  border-radius: 8px;


}
.navigation__link.active {
  
  background-color: rgba(0, 0, 0, 1);
  border-radius: 8px;
  
}
.left_menu{
	padding-left: 60px;
}
p{
	 margin-top: 30px;
    margin-bottom: 30px;
}
.h4, .h5, .h6, h4, h5, h6, h1, h2{
    margin-top: 30px;
    margin-bottom: 30px;
}



/*.nav_bar{
	padding-top: 20px;
	float:right;
	height: 70px;
	width: 140px;
	background-image: linear-gradient(15deg, #13547a 0%, #80d0c7 100%);
	text-align: center;
	font-weight:bold;
	color:white;
	border-radius: 5px;
}
.nav_bar:hover{
 text-decoration : none;
 font-color:;

}*/

nav {
	
 
 
 /* float: right;*/
  
  box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.5);
}



.link-1 {
 color: black;
 text-decoration: none;
 text-align: center;
  float: right;
  margin: 10 50px;
  width: 80px;
  
  
}
.link-1:hover { 
  text-decoration: none;
  color:black;
}
.a:hover{
	text-decoration:none;
}



body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #ffff;
}

.topnav a {
  
  display: block;
  color: #f2f2f2;
  text-align: center;
  
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  
  color: black;
}

.topnav a {
  margin-top:10px; 
  color: black;
}





@media (min-width:320px)  {  
	.navigation {
   display: none;
  }
 
@media (min-width:480px)  {  
	.navigation {
   display: none;
  }
}
@media (min-width:600px)  { 
	.navigation {
   display: none;
  }
}

@media (min-width:801px)  { 
	.navigation {
   display: none;
  }
 }
@media (min-width:1025px) { 
	.navigation {
   display: block;
  }
 }
@media (min-width:1281px) {
	.navigation {
   display: block;
  }
 }

/*@media (min-width: 768px) {

#mainNav {
 display: block;
  margin: 0 auto; 
}
*/
</style>

<header>
	<div class="container-fluid" style="background: #fff; height: 70px;">
		<div class="row" >
		
		
<!-- <div class="container"id="nav-1">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3">
				<img src="assets/img/e_campus.png" alt="Logo" style="max-width: 500px;height: 60px;      padding-top:6px;">
				</div>
		
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>

			<div class="col-md-3">
				<a class="link-1" href="home">Home</a>
			</div>
				
			</div>
 		</div>
	</div> -->

<div class="container-fluid topnav" id="myTopnav">
	<img src="assets/img/e_campus.png" alt="Logo" style="max-width: 500px;height: 60px; padding-left: 50px; padding-top:6px;">
  <a href="home" class="active link-1 btn btn-primary">Home</a>
  
</div>


	</div>
</header>
<div class="container-fluid" style="margin-top: 3px;">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-5">
			<nav class="navigation" id="mainNav">
	<h3 class="navigation__link text-align active">Contents</h3>
	
			</i><a class="navigation__link a" href="#1">STEP1: How to Add an Exam
			</a>
			<a class="navigation__link a" href="#2">STEP2: The dashboard page will appear on the screen
			</a>
			<a class="navigation__link a" href="#3">STEP3: Click on Exam on the dropdown menu.
			</a>
			<a class="navigation__link left_menu a"   href="#4"><li>3.1 Click on Add Exam </li>
			</a>
				<a class="navigation__link a" href="#5" >STEP4 :Start filling the information</a>
				<a class="navigation__link left_menu a" href="#6"><li>4.1 Schedule Start Date</li> 
				</a>
				<a class="navigation__link left_menu a" href="#7"><li>4.2 Schedule End Date</li>
				</a>
				<a class="navigation__link left_menu a" href="#8"><li>4.3 Fill the duration for exam.</li></a>
			<a class="navigation__link" href="#9" style="text-decoration: none;">STEP5: Click on “ADD QUESTION”</li></a>

			<a class="navigation__link left_menu a" href="#10"><li>5.1 For Single choice question</li></a>

			<a class="navigation__link left_menu a" href="#11"><li>5.2 For adding multiple questions</li></a>
			<a class="navigation__link left_menu a" href="#12"><li>5.3 For adding Short Answer again </li></a>


</nav><br><br>
		</div>
		<div class="col-md-7">
			<div class="page-section hero" id="1">
			<h2>How to Add an Exam</h2>
			<b><h4 >Please follow the below mentioned steps to Add an Exam</h4></b>
			<h3>STEP1:</h3>  <h4 style="margin-bottom: 0px;">Sign-in to your account</h4>
			<img src="assets/img/help/signin.png" class="img-responsive">
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="2">
			<h3>STEP2:</h3><br> <h4>The dashboard page will appear on the screen</h4>
			<img src="assets/img/help/dashappear.PNG" class="img-responsive">
			<hr style="border-top: 1px solid #fd610582; margin-top: 75px;
    margin-bottom: 20px;">
			
</div>

<div class="page-section" id="3">
			<h3>STEP3:</h3><br> <h4>(A) Click on Exam on the dropdown menu. </h4>
			<p>As you <strong>click</strong> on Exam sub drop box will appear and then click on <strong>“Exam”</strong></p>
			<img src="assets/img/help/step3.png" class="img-responsive">
			<p>As you click on exam the below shown box will appear </p>
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="4">
			<h4>(B) Click on Add Exam</h4>
			<img src="assets/img/help/step3b.PNG" class="img-responsive">
			<h4>The below mentioned page will open</h4>
			<img src="assets/img/help/step3_1.png" class="img-responsive">
			<hr style="border-top: 1px solid #fd610582;">
</div>

<div class="page-section" id="5">
			<h3>Start filling the information about the exam on the same page.</h3>
			<h4>As shown in the image:</h4>
			<h4>1.Select the Course.</h4>
				<h4>2.Select the semester.</h4>
				<h4>3.Select the subject.</h4>
				<h4>4.Select the Exam name.</h4>
				<h4>5.DESCRIPTION (Here you can give a 2-3 line NOTE to students as given in the following image).</h4>
				<img src="assets/img/help/4.png" class="img-responsive">
			</div>
<div class="page-section" id="6">
			<h4>6.Start Date (Schedule the Start date and time for the exam).</h4>
			<img src="assets/img/help/6.png" class="img-responsive">
</div>

<div class="page-section" id="7">
	<h4>7.End Date (select the end date and time for exam).</h4>
	<img src="assets/img/help/7.png" class="img-responsive">
				
</div>
<div class="page-section" id="8">
			<h3>Fill the score for Exam</h3>
			<h4>8.Fill the duration for exam. </h4>
			<h4>9.Fill the score for correct answer.</h4>
			<h4>10.Fill the score for incorrect answer.</h4>
			<img src="assets/img/help/8.png" class="img-responsive">

			<p>As you click on NEXT the following page will appear. Here you can see the short information about the Exam.</p>
			<hr style="border-top: 1px solid #fd610582;">
				
</div>

<div class="page-section" id="9">
			<h3>STEP5:</h3> <br> <h4 style="margin-top: 0px;">Click on “ADD QUESTION”</h4>
			<img src="assets/img/help/5.png" class="img-responsive">

			<p>Next page will appear, click on <strong>“ADD QUESTION”</strong></p>
			<img src="assets/img/help/5_1.png" class="img-responsive">
			<h4>After clicking, you will get the below page, as you click on “Select” a box will appear and here you will get various forms of questions which you can give to students in exam.</h4>
			<img src="assets/img/help/5_2.png" class="img-responsive">
				
</div>

<div class="page-section" id="10">
			<h3>1.	For Single choice question</h3>
			<img src="assets/img/help/5_3.png" class="img-responsive">

			<p>Next page will appear, click on <strong>“ADD QUESTION”</strong></p>
			<img src="assets/img/help/5_4.png" class="img-responsive">
			<p>You may add as many as questions you want.</p>

				
</div>

<div class="page-section" id="11">
			<h3>2.	For adding multiple questions</h3>
			<p>click again on “Add Questions” on the same page.</p>
			<img src="assets/img/help/adding_2.jpg" class="img-responsive">

			<p>After filling all the information’s, click on Submit.You will get all the questions on the same page.</p>
			<img src="assets/img/help/Question.jpg" class="img-responsive">
				
</div>

<div class="page-section" id="12">
			<h3>3.For adding Short Answer again click on ADD QUESTION on the same page.</h3>
			<img src="assets/img/help/Question_add.jpg" class="img-responsive">
			<p>All the questions will be added on the same page.</p>
			<img src="assets/img/help/Question_after_add.jpg" class="img-responsive">
				
</div>

<div class="page-section" id="13">
			<h3>4.	Same process will be followed to add long questions.</h3>
			<img src="assets/img/help/Long_ansers.jpg" class="img-responsive">
			<p>Click on Submit. And you can see the information on the same page</p>
			<img src="assets/img/help/creat_exam.jpg" class="img-responsive">
			<p>On the same page you will be able to see all the form of questions.
This is how you create an Exam and add questions to it.
</p>
				
</div>

	
	</div>
	</div>
</div>

		</div>
		

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>


	$(document).ready(function() {
		$('a[href*=#]').bind('click', function(e) {
				e.preventDefault(); // prevent hard jump, the default behavior

				var target = $(this).attr("href"); // Set the target as variable

				// perform animated scrolling by getting top-position of target-element and set it as scroll target
				$('html, body').stop().animate({
						scrollTop: $(target).offset().top
				}, 600, function() {
						location.hash = target; //attach the hash (#jumptarget) to the pageurl
				});

				return false;
		});
});

$(window).scroll(function() {
		var scrollDistance = $(window).scrollTop();

		// Show/hide menu on scroll
		//if (scrollDistance >= 850) {
		//		$('nav').fadeIn("fast");
		//} else {
		//		$('nav').fadeOut("fast");
		//}
	
		// Assign active class to nav links while scolling
		$('.page-section').each(function(i) {
				if ($(this).position().top <= scrollDistance) {
						$('.navigation a.active').removeClass('active');
						$('.navigation a').eq(i).addClass('active');
				}
		});
}).scroll();
</script>