<div class="">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="ti ti-ruler-pencil"></i> <?=$this->lang->line('panel_title')?> </h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('panel_title')?></li>
          </ol>
      </div>
    </div>

        <!-- form start -->

    <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
         <div class="col-md-12 nopading">
              <div class="col-sm-6 nopading">
                <div class="advance-tab boredr_none">
                    <?php 
                    $SpecialUsertype = $this->session->userdata('SpecialUsertype');
                    if($SpecialUsertype!='Btech'){
                   ?>
                    <ul class="nav nav-tabs" role="tablist">
                       <?php 

                  if (empty($this->session->userdata('DraftExams')) && empty($this->session->userdata('TrashExams'))) {
                      $active = "active";
                  }else{
                      $active = "";
                  }
                ?>
                 <!--     <li class="<?php echo $active ?>">
                      <a  aria-expanded="false" onclick="AllExams()">All <span style="font-size: 11px;">(<?php echo $all_count; ?>)</span></a>
                   </li> -->
                   <li class="<?php echo $active ?>">
                      <a aria-expanded="true" onclick="ActiveExams()">Active <span style="font-size: 11px;">(<?php echo $ActiveExams_count; ?>)</span></a>
                   </li>
                   <li <?php if ($this->session->userdata('DraftExams')){ ?> class="active" <?php  } ?>>
                      <a  aria-expanded="false" onclick="DraftExams()">Draft <span style="font-size: 11px;">(<?php echo $DraftExams_count; ?>)</span></a>
                   </li>
                   <li <?php if ($this->session->userdata('TrashExams')){ ?> class="active" <?php  } ?>>
                      <a aria-expanded="false" onclick="TrashExams()">Trash <span style="font-size: 11px;">(<?php echo $TrashExams_count; ?>)</span></a>
                   </li>
                   </ul>
                 <?php } ?>
                </div>
                </div>
                
                <?php 

                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

                ?>
 
                <div class="col-sm-6 nopading">
                   <div class="pull-right">
                      <div class="btn-group">
                        <?php $totalexam=""; if(count($totalexam) >= 0)
                        {
                          if($totalexam < 3)
                          { ?>
                            <a href="<?php echo base_url('exam/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
                         <?php } 
                         else 
                         { ?>
                          <a class="btn-top check-all btn bg-success pull-right" data-toggle="modal" data-target="#myModal"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
                         <?php } 
                        } 
                        else
                        { ?>
                          <a href="<?php echo base_url('exam/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
                       <?php } ?>
                        <a style="cursor:pointer;color: red;" onclick="ResetAllfilter_exam()" class="btn-top check-all1  pull-right"> <span class="glyphicon glyphicon-minus"></span>Reset All Filters</a>
                         
                      </div>
                   </div>
                </div>



         </div>
<!-- Start Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Upgrade Plan</h4>
                    </div>
                    <div class="modal-body">
                      <p>Your current plan has reached maximum student addition limit. To enable more students, upgrade your plan.</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <a href="<?php echo base_url('upgradeplan/index') ?>" ><button type="button" class="btn btn-success" >Upgrade Plan</button></a>
                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- End Model -->
       <div class="clearfix"></div>

          </div>
        <div class="clearfix"></div>  
           
        <div class="col-sm-12">
         <div class="theme_input_blue">
            <?php 
               // code goes here...
               ?>

            <div class="col-sm-3">
               <div class="">
                  <form style="" class="form-horizontal" role="form" method="post">
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Course
                        </label>
                        <div class="">
                           <?php
                             $array = array("0" => "Select Course");

                                        foreach ($classes as $classa) {

                                         $array[$classa->classesID] = $classa->classes;

                                }
                       echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterExamclassesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");
                              
                              ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                     </div>
                  </form>
               </div>
            </div>

            <div class="col-sm-3">
               <div class="">
                  <form style="" class="form-horizontal" role="form" method="post">
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Sub Courses
                        </label>
                        <div class="">
                           <select id="subCourseID" class="form-control">
                              <option value="">Select</option>
                               <?php  foreach ($subCourses as $key => $value) {
                                            if ($value->sub_coursesID==$this->session->userdata('FilterExamsubCourseID')) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                    <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                             
                           </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                     </div>
                  </form>
               </div>
            </div>
            
            <div class="col-sm-3">
               <div class="">
                  <form style="" class="form-horizontal" role="form" method="post">
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Year/Semester
                        </label>
                        <div class="">
                           <select id="yearSemesterID" class="form-control">
                              <option value="">Select</option>
                              <?php 
                                if ($classesRow) {
                                   $looping    =  (int) $classesRow->duration;
                                 if ($classesRow->mode==1) {
                               
                                    for ($i=1; $i <=$looping; $i++) {
                                                if (CallYears($i)==$this->session->userdata('FilterExamyearSemesterID')) {
                                      $select = 'Selected';
                                   }else{
                                   $select = '';
                                   }
                           echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                  }
                                }

                              else{
                                    for ($i=1; $i <=(2*$looping); $i++) {
                                   if (CallSemester($i)==$this->session->userdata('FilterExamyearSemesterID')) {
                                      $select = 'Selected';
                                   }else{
                                   $select = '';
                                   }
                           echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                    }
                                 }
                              }
                               ?>
                              
                           </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                     </div>
                  </form>
               </div>
            </div>
			
			
			<div class="col-sm-3">
               <div class="">
                  <form style="" class="form-horizontal" role="form" method="post">
                     <div class="form-group">
                        <label for="subjectID" class="control-label">
                        Subject
                        </label>
                        <div class="">
							<select name="subjectID" id="subjectID" class="form-control">
								<option value="0">Select Subject</option>
								<?php  
									foreach ($subject as $k => $v) 
									{
										if ($v->subjectID==$this->session->userdata('FilterExamsubjectID')) 
										{
											$selected =  "Selected";
										}
										else
										{
											$selected =  "";
										}
								?>
										<option value="<?php echo $v->subjectID ?>" <?php echo $selected ?>><?php echo $v->subject ?></option>
								<?php 
									} 
								?>
							</select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubject()">Reset This Filter </a></div>
                     </div>
                  </form>
               </div>
            </div>

            <div class="clearfix"></div>
         </div>
      </div>



            
      <div class="clearfix"></div>
                <?php } ?> 

<!-- <?php
      if (1) {
 ?> -->
            <form action="<?php echo base_url() ?>exam/multipleAction" class="pad5" method = "post" onsubmit="return confirm('Do you really want to submit the form?');">
                <div class="action-layout">
                    <ul>
                      <?php 
                        if ($usertype == "Support") {
                         
                        }
                        else{ ?>
                        <li>
                            <label class="nexCheckbox">Check
                                <input type="checkbox" name="select_all" id="select_all">
                                <span class="checkmark checkmark-action-layout"></span>
                            </label>
                        </li>
                        <li>
                            <input type="submit" class="btn btn-success btn-active etsfilertButton disabled"  name="Active" value="Active" disabled>
                        </li>
                        <li>
                            <input type="submit" class="btn btn-info btn-draft etsfilertButton disabled" name="Draft" value="Draft" disabled>
                        </li>
                                            
                        <li>
                            <input type="submit" class="btn btn-danger btn-delete etsfilertButton disabled" name="Delete" value="Delete" disabled>
                        </li>
                      <?php } ?>

                        
                    </ul>
                </div> 

                      <table id="examTables" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2"></th>
                                <th class="col-sm-2">S.no</th>
                                <th class="col-sm-2">Exam Name</th>
                                <th class="col-sm-2">Subject Code</th>
                                <th class="col-sm-2">Start Date(Exam Date Start)</th>
                                <th class="col-sm-2" >End Date(Exam Date Start)</th>
                                <th class="col-sm-2">Subject Name</th>
                                <th class="col-sm-2">Course Name</th>
                                <th class="col-sm-2">Sub Course Name</th>
                                <th class="col-sm-2">Semester/Year</th>
                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                    </table>
</form>


  <form class="form-horizontal" role="form" method="post">

    <?php 
            if(form_error('classesID')) 
              echo "<div class='form-group has-error' >";
            else     
              echo "<div class='form-group' >";
          ?>
            <?php 
                        if ($usertype == "Support") {
                         
                        }
                        else{ ?>
            <div class="col-md-12 col-sm-12">
              <div class="card">
                <h3 style="text-align: center;padding: 10px;">Bulk Exam Upload through <span class="red-color">CSV</span> </h3>
                <div class="card-body">
                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="classesID">Course<span class="red-color">*</span></label>
                    <select name="classesID" required onchange="ajaxGet_subCoursesExcel($(this).val())" id="classesIDExcel" class="form-control" value="<?php echo set_value('classesID'); ?>">
                        <option value="">Select Course</option>
                        <?php foreach($classes as $courses){ ?>
                          <option value="<?php echo $courses->classesID ?>"><?php echo $courses->classes ?> </option>
                    <?php } ?>
                    </select> 
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group" id="subCourseIDExcel">
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group" id="subjectIDcsv">
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="">Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS)</label> 
               <input type="text" name="start_date"  class="form-control datetimepicker_quiz_start" placeholder="Enter Exam Start Date"   required >
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="">End Date (Exam can be attempted before this date. YYYY-MM-DD HH:II:SS)</label> 
               <input type="text" name="end_date" class="form-control datetimepicker_quiz_start" placeholder="Enter Exam End Date"   required >
                    </div>
                  </div>


                <div class="col-md-12">
                    <div class="form-group">
                      
                        <label for="courseName">Change Other Details</label>
                        <input type="radio" name="cod" id="chde1" value="1"><label for="chde1">YES</label>
                        <input type="radio" name="cod" value="0" id="chde2" checked><label for="chde2">NO</label><hr>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      
                        <label for="courseName">Video Required</label>
                         <input type="radio" name="video" id="video1" value="1" checked><label for="video1">YES</label>
                        <input type="radio" name="video" value="0" id="video2"><label for="video2">NO</label><hr>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      
                        <label for="courseName">Please Select <span class="red-color">Exam Type*</span></label>
                        <input type="radio" name="extype" id="extype1" value="1"><label for="extype1">Main</label>
                        <input type="radio" name="extype" value="2" id="extype2" checked><label for="extype2">Assignment</label><hr>
                    </div>
                  </div>

                  <!-- <div class="col-md-12">
                    <div class="form-group">
                      
                        <label for="courseName">Please Select <span class="red-color">Exam Mode*</span></label>
                        <input type="radio" name="ch" id="jnuexam" value="1" checked><label for="jnuexam">Jnu Exam</label>
                        <input type="radio" name="ch" value="2" id="normalexam"><label for="normalexam">Vmsu</label><hr>
                    </div>
                  </div> -->

                  <div class="col-md-12">
                    <div class="form-group">
                      
                        <label for="uploadFile" class="col-md-2 col-form-label">Upload File</label>
                        <div class="file-upload">
                          <input type="file" class="" name = 'csvQuestion' required /><hr>
                        </div>
                        
                    </div>
                  </div>

                  <div class="col-md-12" style="text-align: center; padding: 20px;">
                    <button class="btn btn-success add-btn" type="submit">Submit</button>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
            </div>
          
</div>
</div>
</div>
</div>

     <!--    <?php } else{ ?>
        <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
<p>Maybe your search was too specific, please try searching with another term.</p>
<img src="<?php echo base_url() ?>uploads/images/crying.png">
</div>
         <?php } ?> -->
<style type="text/css">
    .uploadsExcelQuiz{
        margin-top:4%;
    }
   .uploadsExcelQuiz .form-box {
    
    border-radius: 4px;
    width:100%;
    padding: 3% 4%;
  }
  .form-group {
    overflow: hidden;
  }
  .form-btn {
    margin-bottom: 0 !important;
  }
  input {
    width: 100%;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  .submit-btn {
    padding: 6px 30px;
    margin: 0 auto;
    width: auto;
    font-size: 18px;
    display: block;
  }
  #openContainer{
    display:none;
  }
</style>


<script>
	/*$("#yearSemesterID").change(function(e){
		e.preventDefault();
		yearSemesterID = $(this).val();
		classesID = $("#classesID").val();
		subCourseID = $("#subCourseID").val();
		$.ajax({
			type:"POST",
			data:{"yearSemesterID":yearSemesterID,"classesID":classesID,"subCourseID":subCourseID},
			url:"<?php echo base_url(); ?>exam/subject_search",
			success:function(res){
				alert(res);
				//$("#subjectID").html(res);
			}
		});
	});*/
</script>
<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();
         $('#examTables').DataTable().state.clear();

               $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>


<script type="text/javascript">

    $('#subCourseID').change(function() {

        var subCourseID = $(this).val();
         $('#examTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();
         $('#examTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">

    $('#subjectID').change(function() {

        var subjectID = $(this).val();
         $('#examTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "subjectID=" + subjectID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>
<script type="text/javascript">
    
    function ResetCourses(){
      $('#examTables').DataTable().state.clear();


            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
       $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
       $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

	function ResetSubject(){
	   $('#examTables').DataTable().state.clear();
		$.ajax({
			type: 'POST',

			url: "<?=base_url('exam/ResetSubject')?>",

			data:{ResetSubject:'ResetSubject'},

			dataType: "html",

			success: function(data) {
				location.reload();

			}

		});
	}

 function ResetAllfilter_exam(){
   $('#examTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}



</script>
<script type="text/javascript">
    function ajaxGet_subCoursesExcel(id){
    $('.showLoaderSubcour').show();
    $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Get_subCourses')?>",
        data:{"id":id},
        success: function(response) {
            console.log(response);
            $("#subCourseIDExcel").html(response);
            $('.showLoaderSubcour').hide();
        }
            });
}
</script>

<script type="text/javascript">
    $(document).ready(function(){ 
    $("input[name=cod]").change(function() {
        var test = $(this).val();
        if (test==1) {
    $("#openContainer").show();
        }else{
      $("#openContainer").hide();  
        }
    
    }); 
});
</script>

<script type="text/javascript">
  function Getsubjects(){
    var  classesID  = $("#classesIDExcel").val();

    // var  sub_CourseID  = $("#sub_CourseIDExcel").val();"sub_coursesID":sub_CourseID,
    // alert(sub_CourseID);
    var  semester_idSelect  = $("#semester_idSelect").val();
    $.ajax({
      type: "POST",
      url:"<?=base_url('ajaxControllerQuestion/Getsubjects')?>",
      data:{"classesID":classesID,"semester_idSelect":semester_idSelect},
      success: function(response) {
        $("#subjectIDcsv").html(response);
      }
    });
  }
</script>

<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>
<style type="text/css">
	#change_start_date_time{
		width: 124px;
	}
	#change_end_date_time{
		width: 124px;
	}
</style>
