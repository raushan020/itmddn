
<div class="">

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-cogs"></i> Exam Setting </h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">Exam Setting</li>
          </ol>
      </div>
    </div></div>

   <!-- /.box-header -->

    <!-- form start -->

     <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
<form class="" action="<?=base_url('exam/dau_quiz') ?>" method="post">

  <div class="col-md-4">
                    <div class="form-group">
                    <label for="" >Exam Duration (Please Enter Time in min.)</label> 
               <input type='text' class='form-control' id='subjectCodeClear' onBlur='Comman_field_quiz_setting(this,"duration", <?php echo $quiz_settings->id ?> ,$(this).val())' placeholder='Enter Duration' name='duration' value="<?php echo $quiz_settings->duration ?>" >
                    </div>
                  </div>

  <div class="col-md-4">
                    <div class="form-group">
                    <label for="" >Correct Score</label> 
               <input type='text' class='form-control' id='subjectCodeClear' onBlur='Comman_field_quiz_setting(this,"correct_score", <?php echo $quiz_settings->id ?> ,$(this).val())' placeholder='Correct Score' name='subject' value="<?php echo $quiz_settings->correct_score ?>" >
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                    <label for="" >In correct Score</label> 
               <input type='text' class='form-control' id='subjectCodeClear' onBlur='Comman_field_quiz_setting(this,"incorrect_score", <?php echo $quiz_settings->id ?> ,$(this).val())' placeholder='In correct Score' name='subject' value="<?php echo $quiz_settings->incorrect_score ?>" >
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="" >Start Date</label> 
               <input type="text" name="start_date" id="" class="form-control form-control datetimepicker_quiz_start" value="" placeholder="Enter Start Date"  required >
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="" >End Date</label> 
               <input type="text" name="end_date" id="datetimepicker_quiz_end" value="" class="form-control form-control datetimepicker_quiz_start" placeholder="Enter End Date"  required >
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="" style="color: red;">Video Required</label> 
                    <input type="radio" name="video" id="video1" value="1" checked><label for="video1">YES</label>
              <input type="radio" name="video" value="0" id="video2"><label for="video2">NO</label>
                    </div>
                  </div>

             <div class="col-md-12" style="text-align: center; padding: 20px; margin-top: 15px;">
                  <input type="submit" name="submit" value="Update" class="btn btn-success add-btn"  required >
                  </div>
                
                
               
           
           </form>
       
           </div>
    </div>
</div></div>


