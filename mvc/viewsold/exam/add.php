<?php 

  if($this->input->get('exam_for')){
    $exam_for = $this->input->get('exam_for');
  }else{
    $exam_for = 1;
  }
 ?>
<div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-pencil"></i> Add Exam </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("exam/index")?>"><?=$this->lang->line('menu_exam')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_exam')?></li>
            </ol>
        </div>
    </div>

<!-- new -->
<div class="container-fluid">
  <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
<div class="row">
  <form class="form-horizontal" role="form" method="post">

    <?php 
            if(form_error('classesID')) 
              echo "<div class='form-group has-error' >";
            else     
              echo "<div class='form-group' >";
          ?>
            <div class="col-md-12 col-sm-12">
              <div class="card">
                <div class="card-body">
                  <div class="col-md-6">
                    <div class="form-group">
                    <label>Select Exam Type<span class="red-color">*</span></label>
                    <select name="exam_for" required onchange="ajaxGet_unit($(this).val())"  class="form-control" id="exam_for" value="<?php echo set_value('classesID'); ?>">
                        <option value="" selected>Select</option>
                  <option value="1" <?php if($exam_for==1){echo "selected";} ?>>Subject</option>
                  <option value="2" <?php if($exam_for==2){echo "selected";} ?>>Unit</option>
                        </select>
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="classesID">Course<span class="red-color">*</span></label>
                    <select name="classesID" required onchange="ajaxGet_subCourses_exam($(this).val())" id="classesID" class="form-control" value="<?php echo set_value('classesID'); ?>">
                        <option value="">Select Course</option>
                        <?php foreach($classes as $courses){ ?>
                          <option value="<?php echo $courses->classesID ?>"><?php echo $courses->classes ?> </option>
                          <?php } ?>
                        </select>
                    </div>
                    <span class="col-md-6 showLoaderSubcour">
                      <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                    </span>

                    <span class="col-md-6 control-label">
                      <?php echo form_error('classesID'); ?>
                    </span>
                     <div id="subCourseID"></div>

                  
                    
                  </div>
                  <div class="col-md-6" id="yearAndSemester"></div>
                  <div class="col-md-6"  id="subjectID" required></div>
                  <div class="col-md-12" id="unitID"></div>


                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="quiz_name">Exam Name<span class="red-color">*</span></label> 
                    <input type="text"  name="quiz_name" id="quizID" class="form-control" placeholder="Quiz Name"  required autofocus>
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="">Description</label> 
                    <textarea name="description"  class="form-control tinymce_textarea" ></textarea>
                    </div>
                  </div>



                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="">Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS)</label> 
               <input type="text" name="start_date"  class="form-control datetimepicker_quiz_start" placeholder="Enter Exam Start Date"   required >
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                    <label for="">End Date (Exam can be attempted before this date. YYYY-MM-DD HH:II:SS)</label> 
               <input type="text" name="end_date" class="form-control datetimepicker_quiz_start" placeholder="Enter Exam End Date"   required >
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="" >Exam Duration (Please Enter Time in min.)</label> 
               <input type="text" name="duration"  value="<?php echo $quiz_settings->duration ?>" class="form-control" placeholder="Duration (in min.)"  required  >
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                   <label for=""  >Correct Score</label> 
               <input type="text" name="correct_score"  value="<?php echo $quiz_settings->correct_score ?>" class="form-control" placeholder="Correct Score"   required >
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                    <label for=""  >In correct Score</label> 
               <input type="text" name="incorrect_score"  value="<?php echo $quiz_settings->incorrect_score ?>" class="form-control" placeholder="InCorrect Score"  required  >
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                      
                        <label for="courseName">Please Select <span class="red-color">Exam Mode*</span></label>
                        <input type="radio" name="extype" id="extype1" value="1"><label for="extype1">Main</label>
                        <input type="radio" name="extype" value="2" id="extype2" checked><label for="extype2">Assignment</label>
                    </div>
                  </div>

                  <div class="col-md-12" style="text-align: center; padding: 20px;">
                    <button class="btn btn-success add-btn" type="submit">Submit</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        </div>
<!-- end new -->



<script type="text/javascript">

$('#date').datepicker();

</script>

<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesID").val();
       // var  sub_CourseID  = $("#sub_CourseID").val();"sub_coursesID":sub_CourseID,
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('exam/Getsubjects')?>",
        data:{"classesID":classesID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script>
 function ajaxGet_unit(val){
 window.location.href=base_url+"exam/add?exam_for="+val;
 }  

</script>
<script type="text/javascript">
  function AddQuizName(val){
var exam_for =  $('#exam_for').val();

if(exam_for==1){
$("#quizID").val($("#subject_id option:selected").text());
}else{
       $.ajax({
        type: "POST",
        url:"<?=base_url('exam/GetUnits')?>",
        data:{"subjectID":val},
        success: function(response) {
            $("#unitID").html(response);
            //$("#quizID").val($("#unit_id option:selected").text());
        }
    });
  }
   
}
</script>




