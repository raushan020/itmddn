<html xmlns:v="urn:schemas-microsoft-com:vml"
   xmlns:o="urn:schemas-microsoft-com:office:office"
   xmlns:x="urn:schemas-microsoft-com:office:excel"
   xmlns="http://www.w3.org/TR/REC-html40">
   <head>
      <title></title>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 12">
<!-- <link id=Main-File rel=Main-File href="../JNU_TR_Sep-2017.htm"> -->
<link rel=File-List href=filelist.xml>

<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<link rel=Stylesheet href="<?php  echo base_url() ?>assets/inilabs/stylesheet.css">
<style>
<!--table
  {mso-displayed-decimal-separator:"\.";
  mso-displayed-thousand-separator:"\,";}
@page
  {margin:.75in .7in .75in .7in;
  mso-header-margin:.3in;
  mso-footer-margin:.3in;}
-->
</style>

      <link rel="stylesheet" type="text/css" href="stylesheet.css">
      <style>
         table
         {mso-displayed-decimal-separator:"\.";
         mso-displayed-thousand-separator:"\,";}
         @page
         {margin:.75in .7in .75in .7in;
         mso-header-margin:.3in;
         mso-footer-margin:.3in;}
      </style>
   </head>
   <body>
     
        <?php

 $filename = $classesSingle->classes;
   if (isset($sub_courseSingle->sub_course)) {
$filename  .=    "-in".$sub_courseSingle->sub_course;
}
$filename .= "-".str_replace('_', ' ', $examSingle->yearsOrSemester);

 header("Content-Type: application/xls");    
 header("Content-Disposition: attachment; filename=$filename.xls");  
 header("Pragma: no-cache"); 
 header("Expires: 0"); 

     ?>
     <?php if(isset($subjects)){  ?>
     <table border="0" cellpadding="0" cellspacing="0" width="14527" style="border-collapse:
         collapse;table-layout:fixed;width:10900pt">
                    <tr class=xl65 height=34 style='height:25.5pt'>
<td colspan=33 height=34 width=1649 style='height:25.5pt;width:1243pt'
  align=left valign=top><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:1px;margin-top:0px;width:78px;
  height:71px'><img width=78 height=71 src="<?php echo base_url() ?>uploads/images/image002.gif" v:shapes="Picture_x0020_2"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td colspan=33 height=34 class=xl98 width=1649 style='height:25.5pt;
    width:1243pt'>JAIPUR NATIONAL UNIVERSITY , JAIPUR</td>
   </tr>
  </table>
  </span></td>
 </tr>
         <tr class="xl65" height="30" style="height:22.5pt">
            <td colspan="33" height="30" class="xl99" style="height:22.5pt">School of Distance
               Education &amp; Learning
            </td>
            <td colspan="205" class="xl65" style="mso-ignore:colspan"></td>
         </tr>
         <tr class="xl65" height="25" style="height:18.75pt">
            <td colspan="33" height="25" class="xl100" style="height:18.75pt">Result of
               Semester-End-Examination, Dec-2017
            </td>
            <td colspan="205" class="xl65" style="mso-ignore:colspan"></td>
         </tr>
         <tr class="xl65" height="39" style="mso-height-source:userset;height:29.25pt">
            <td colspan="33" height="39" class="xl101" style="height:29.25pt">Name of Programme
               :  <?php echo $classesSingle->classes ?><span style="mso-spacerun:yes">&nbsp; </span>
              <?php
if (isset($sub_courseSingle->sub_course)) {
  echo "with specialization in ";
          echo $sub_courseSingle->sub_course ;} ?>
            </td>
            <td colspan="205" class="xl65" style="mso-ignore:colspan"></td>
         </tr>
         <tr class="xl65" height="27" style="mso-height-source:userset;height:20.25pt">
            <td colspan="33" height="27" class="xl108" style="height:20.25pt">Semester: <?php echo  str_replace('_', ' ', $examSingle->yearsOrSemester) ?></td>
            <td colspan="205" class="xl65" style="mso-ignore:colspan"></td>
         </tr>
         <tr class="xl65" height="52" style="mso-height-source:userset;height:39.0pt">
            <td rowspan="4" height="249" class="xl94" width="34" style="height:186.75pt;
               border-top:none;width:26pt">S.N.</td>
            <td rowspan="4" class="xl94" width="77" style="border-top:none;width:58pt">Roll No.
               / <br>
               ID
            </td>
            <td rowspan="4" class="xl94" width="185" style="border-top:none;width:139pt">Enrolment
               No.
            </td>
            <td rowspan="4" class="xl94" width="147" style="border-top:none;width:110pt">Name
               of the<br>
               Student
            </td>
            <td rowspan="4" class="xl94" width="83" style="border-top:none;width:62pt">Father's
               Name
            </td>
            <td rowspan="4" class="xl94" width="106" style="border-top:none;width:80pt">Mother's
               Name
            </td>
            <td colspan="<?php echo count($subjects)*4; ?>" class="xl95" style="border-left:none">Theory Papers</td>
            <td rowspan="3" class="xl96" width="67" style="border-top:none;width:50pt">Total<br>
               <font class="font10">MM:600</font>
            </td>
            <td rowspan="4" class="xl95" style="border-top:none">Percentage</td>
            <td rowspan="4" class="xl95" style="border-top:none">Remarks</td>
         </tr>
         <tr height="119" style="mso-height-source:userset;height:89.25pt">
         <?php 
                        $code =1;
                        foreach ($subjects as $key => $value) {
                         ?>
            <td colspan="4" height="119" class="xl105" width="140" style="border-right:.5pt solid black;
               height:89.25pt;border-left:none;width:106pt"><?php 
                            $subCode = explode('_', $examSingle->yearsOrSemester);
preg_match_all('!\d+!', $subCode[0], $matches);
                            echo $matches[0][0];
                            echo 0;
                            echo $code++;
                             ?> :<?php echo $value->subject  ?></td>
               <?php } ?>
           
         </tr>
         <tr height="40" style="mso-height-source:userset;height:30.0pt">
              <?php  foreach ($subjects as $key => $value) {
                         ?>
            <td height="40" class="xl67" style="height:30.0pt;border-top:none;border-left:
               none">Int</td>
            <td class="xl67" style="border-top:none;border-left:none">Ext</td>
            <td class="xl67" style="border-top:none;border-left:none">Total</td>
            <td rowspan="2" class="xl102" style="border-top:none">Remark</td>
            <?php } ?>
         </tr>
         <tr height="38" style="height:28.5pt">
                 <?php 
                 foreach ($subjects as $key => $value) {
                ?>
            <td height="38" class="xl68" style="height:28.5pt;border-top:none;border-left:
               none">30</td>
            <td class="xl68" style="border-top:none;border-left:none">70</td>
            <td class="xl69" style="border-top:none;border-left:none">100</td>
  <?php } ?>
            <td class="xl70" width="67" style="border-top:none;border-left:none;width:50pt">Marks
               Obtained
            </td>
           
         </tr>
          <?php 
          $sn =1;
          foreach ($exams as $key => $student) {  ?>
         <tr height="20" style="height:15.0pt">
            <td height="20" class="xl78" align="right" style="height:15.0pt"><?php echo $sn++ ?></td>
            <td class="xl79" align="right" style="border-left:none"><?php echo $student->registration_ID ?></td>
            <td class="xl79" style="border-left:none"><?php echo $student->roll  ?></td>
            <td class="xl79" style="border-left:none"><?php echo $student->name  ?><span style="display:none"></span></td>
            <td class="xl80" width="83" style="border-left:none;width:62pt"><?php echo $student->father_name  ?></td>
            <td class="xl80" width="106" style="border-left:none;width:80pt"><?php echo $student->mother_name  ?></td>
          <?php 
          $this->db->where('examID',$student->examID);
          $query = $this->db->get('mark');
                    $totalMark = 0;
                    $last_review = 0; 
           foreach ($query->result() as $key => $value) {
              // $min=27;
  // $max=30;
  // $intmark = rand($min,$max);
if ($value->mark<28) {
if ($value->mark==0) {
$total_subject_mark  = 0;
}else{
 $total_subject_mark  = 30;
}
  
}
else{
  $total_subject_mark  = $value->mark;
}
  $totalMarkSubject  = $value->intMark+round($total_subject_mark);
  $totalMark += $totalMarkSubject;
  $subjectPEcr =  round($total_subject_mark)*100/70;
                         ?>

            <td class="xl84" style="border-left:none"><?php echo $value->intMark ?></td>
            <td class="xl84" style="border-left:none"><?php echo round($total_subject_mark)  ?></td>
            <td class="xl83" style="border-left:none"><?php echo $totalMarkSubject; ?></td>
            <td class="xl83" style="border-left:none">
                                                <?php 
                                if ($subjectPEcr>=40) {
                                 echo "P";
                                }else{
                                    if ($subjectPEcr==0) {
                                      $last_review += 1;
                                      echo 'NA';
                                    }else{
                                      echo "F";
                                    }
                                    
                                }

                                 ?>
            </td>
   <?php } ?>
            <td class="xl83" style="border-left:none"><?php echo $totalMark; ?> </td>
            <td class="xl86" style="border-left:none"><?php echo round($totalMark/600*100,2) ?>%</td>
            <td class="xl85" style="border-left:none"><?php
                if (round($totalMark/600*100,2)>=35) {
                  if($last_review>0){
                    echo "Ab";
                  }else{
                     echo "PASS";
                  } 
                   
                }else{
                    echo "FAIL";
                }

                 ?></td>
            <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
         </tr>
         <?php } ?>


<tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td colspan="18" height="20" class="xl72" style="height:15.0pt">Abbreviations : P -
  Pass,<span style="mso-spacerun:yes">&nbsp; </span>BP - Back Paper, NA - Not
  Appeared, G - Grace , RL - Result Later,<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;
  </span>Int. - Internal,<span style="mso-spacerun:yes">&nbsp; </span>Ext. -
  External, Ab- Absent.</td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>

 <tr height="20" style="height:15.0pt">
  <td colspan="18" height="20" class="xl72" style="height:15.0pt">Result Analysis : -
  Theory Papers:<span style="mso-spacerun:yes">&nbsp; </span>Minimum Pass % :-<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp; </span>Internal 30 %<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>and<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>External 30 %<span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Aggregate 40 %</td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td colspan="4" class="xl104">Division</td>
  <td colspan="4" class="xl83" style="border-left:none">No. of Students</td>
  <td class="xl75"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td colspan="4" class="xl104">First</td>
  <td colspan="4" class="xl83" style="border-left:none">1</td>
  <td class="xl75"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td colspan="4" class="xl104">Second</td>
  <td colspan="4" class="xl83" style="border-left:none">NIL</td>
  <td class="xl75"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td colspan="4" class="xl104">Pass</td>
  <td colspan="4" class="xl83" style="border-left:none">NIL</td>
  <td class="xl75"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td colspan="4" class="xl104">BP</td>
  <td colspan="4" class="xl83" style="border-left:none">NIL</td>
  <td class="xl75"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td colspan="4" class="xl104">Pass By Grace</td>
  <td colspan="4" class="xl83" style="border-left:none">NIL</td>
  <td class="xl75"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="20" style="height:15.0pt">
  <td height="20" class="xl72" style="height:15.0pt"></td>
  <td colspan="3" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl73" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="21" style="height:15.75pt">
  <td height="21" class="xl72" style="height:15.75pt"></td>
  <td class="xl88" colspan="2" style="mso-ignore:colspan">Prepared by</td>
  <td class="xl88">Checked by</td>
  <td class="xl73"></td>
  <td class="xl89" width="106" style="width:80pt">Verified by</td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td class="xl75"></td>
  <td class="xl87">Verified by</td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl87">Controller of</td>
  <td class="xl74"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl74"></td>
  <td class="xl87">Verified by</td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
 <tr height="21" style="height:15.75pt">
  <td height="21" class="xl72" style="height:15.75pt"></td>
  <td class="xl88" colspan="2" style="mso-ignore:colspan">Assistant Registrar</td>
  <td class="xl88">Dy Director</td>
  <td class="xl73"></td>
  <td class="xl88" colspan="2" style="mso-ignore:colspan">Director, SODEL</td>
  <td class="xl74"></td>
  <td class="xl75"></td>
  <td class="xl87">Dy. COE</td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl87">Examinations</td>
  <td class="xl74"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl74"></td>
  <td class="xl87">Hon'ble Vice Chancellor</td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl74" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td colspan="2" class="xl75" style="mso-ignore:colspan"></td>
  <td class="xl76"></td>
  <td colspan="205" class="xl71" style="mso-ignore:colspan"></td>
 </tr>
</table>
      <?php } ?>
<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>


<script type="text/javascript">

    $('#subCourseID').change(function() {

        var subCourseID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>
<script type="text/javascript">
    
    function ResetCourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){

            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}



</script>

   </body>
</html>