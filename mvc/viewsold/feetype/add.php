

<div class="box">

    <div class="box-header">

        <h3 class="box-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>



        <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("feetype/index")?>"><?=$this->lang->line('menu_feetype')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_feetype')?></li>

        </ol>

    </div><!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">

            <div class="col-sm-8">

<p>Field are required with <span class="red-color">*</span></p>
                <form class="form-horizontal" role="form" method="post">



                    <?php 

                        if(form_error('feetype')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="feetype" class="col-sm-2 control-label">

                            <?=$this->lang->line("feetype_name")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                        <?php 

                        $options = array(
                        'Registration_Fee' => 'Registration Fee',
                        'Re_Registration_Fee' => 'Re-Registration Fee',
                        'Study_Materials' => 'Study Materials',
                        'Lateral_Entry' => 'lateral_entry',
                        'TOC' => 'TOC',
                        'Convocation_Fee' => 'Convocation Fee',
                        'Provisional_Certificate_Fee' => 'Provisional Certificate Fee',
                        'Duplicate_ID' => 'Duplicate ID',
                        'Prospectus' => 'Prospectus',
                        'o' => 'One Time Student',
                        );
                                        echo form_dropdown('feetype', $options, 'y', 'class="form-control"  id ="feetype" onchange = "onChangevalue()" ');
                                                 ?>

            

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('feetype'); ?>

                        </span>

                    </div>
                    
                    <?php 

                        if(form_error('amount')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="amount" class="col-sm-2 control-label">

                           Amount <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            
<div class="input-group">
  <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
  <input class="form-control" placeholder="Amount" value="<?=set_value('amount')?>" required = "required" style="resize:none;" id="amount" name="amount">
  <span class="input-group-addon">.00</span>
</div>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('amount'); ?>

                        </span>

                    </div>

<!--                     <?php 

                        if(form_error('semesterIds')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="semesterIds" class="col-sm-2 control-label">

                           Semester <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

<select multiple="" name="semesterIds[]" class="ui fluid dropdown">
      <?php  for ($i=1; $i <=6; $i++) { ?>
     <option value="<?php echo CallSemester($i) ?>" selected ><?php echo  str_replace('_', ' ', CallSemester($i) ) ?></option>
   <?php  } ?>
</select>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('semesterIds'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('yearIDs')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="yearIDs" class="col-sm-2 control-label">

                           Year <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

<select  multiple="" name="yearIDs[]" class="ui fluid dropdown">
     <?php  for ($i=1; $i <=8; $i++) {  ?>
     <option value="<?php echo CallYears($i) ?>" selected> <?php echo str_replace('_', ' ', CallYears($i) ) ?></option>
<?php    } ?>
</select>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('yearIDs'); ?>

                        </span>

                    </div> -->






                    <?php 

                        if(form_error('note')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-2 control-label">

                            <?=$this->lang->line("feetype_note")?>

                        </label>

                        <div class="col-sm-6">

                            <textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note')?></textarea>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('note'); ?>

                        </span>

                    </div>



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_feetype")?>" >

                        </div>

                    </div>



                </form>



            </div>

        </div>

    </div>

</div>