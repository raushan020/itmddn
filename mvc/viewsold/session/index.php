

<div class="box">

    <div class="box-header">

        <h3 class="box-title"><i class="fa fa-sitemap"></i> <?=$this->lang->line('panel_title')?></h3>



        <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active"><?=$this->lang->line('menu_classes')?></li>

        </ol>

    </div><!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">

            <div class="col-sm-12">



                <?php 

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin") {

                ?>

                    <h5 class="page-header">

                        <a href="<?php echo base_url('classes/add') ?>">

                            <i class="fa fa-plus"></i> 

                            <?=$this->lang->line('add_title')?>

                        </a>

                    </h5>

                <?php } ?>


<div id="messageForEdit"></div>
                <div id="hide-table">


                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                        <thead>

                            <tr>
                            <th>S.N.</th>

                                <th>Session</th>
<!-- 
                                <th class="col-lg-2"><?=$this->lang->line('courses_name')?></th>
                                                                <th class="col-lg-2"><?=$this->lang->line('courses_note')?></th> -->

                                <th ><?=$this->lang->line('action')?></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php

                             if(count($std_session)) {$i = 1; foreach($std_session as $class) { ?>

                                <tr>

                                    <td data-title="<?=$this->lang->line('slno')?>">

                                        <?php echo $i; ?>

                                    </td><td contenteditable="true" onBlur="EditSession(this,'<?php echo $class->session_name ?>','<?php echo $class->sessionID ?>')" onClick="showEdit(this);"><?php echo $class->session_name ?> <i class="fa fa-pencil" aria-hidden="true"></i></td>
                             
                                     <td data-title="<?=$this->lang->line('action')?>">

                                        <?php echo btn_edit('classes/edit/'.$class->    sessionID, $this->lang->line('edit')) ?>

                                        <?php echo btn_delete('classes/delete/'.$class->    sessionID, $this->lang->line('delete')) ?>

                                    </td>

                                </tr>

                            <?php $i++; }} ?>

                        </tbody>

                    </table>

                </div>



            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
    function EditSession(val,session_name,sessionID) {
     var name = val.innerHTML;
$(val).css("background","#FFF url(../images/loaderIcon.gif) no-repeat right");
$.ajax({
    type:'post',
    url:"<?php echo base_url()  ?>session/EditSession",
    data:{sessionID:sessionID,session_name:name},
    success:function(response){

$('#messageForEdit').html(response);
    }

});

    }
</script>
<!-- 
<script>
function EditSession(editableObj,sessionID,session_name) {

    $.ajax({
        url: "<?php echo base_url()  ?>session/EditSession",
        type: "POST",
        data:'sessionID='+sessionID+'&session_name='+session_name.innerHTML+'&session_name='+session_name,
        success: function(data){
            $(editableObj).css("background","#FDFDFD");
        }        
   });
}
</script> -->