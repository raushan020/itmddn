<?php $adminpackage=$this->session->userdata('adminpackage');

if($adminpackage == 1) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     You can add only 50 students<a href="<?php echo base_url('upgradeplan') ?>" class="alert-link"> upgrade now</a>.
   </div>
  
<?php }  ?>
  <div class="">

    <div class="row page-titles">

            <div class="col-md-5 align-self-center">

                <h3 class="text-themecolor"><i class="ti ti-user"></i> <?=$this->lang->line('panel_title')?> </h3>

            </div>

            <div class="col-md-7 align-self-center">

                <ol class="breadcrumb">

                  <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                  <li class="active"><?=$this->lang->line('menu_student')?></li>

                </ol>

            </div>

    </div>

   <!-- /.box-header -->

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         

            <div>

            <div class="card-body">





<!-- new tab bar -->

<div class="col-sm-6 nopading">

<div class="advance-tab boredr_none">

                <ul class="nav nav-tabs" role="tablist">

                  <?php 

                    if (empty($this->session->userdata('DraftStudent')) && empty($this->session->userdata('TrashStudent')) ) {

                        $active = "active";

                    }else{

                        $active = "";

                    }

                  ?>

                  <!-- <li class="<?php echo $active ?>">

                  <a  aria-expanded="false" onclick="AllStudent()">All <span style="font-size: 11px;">(<?php echo $all_count; ?>)</span></a>

               </li> -->

               <li class="<?php echo $active ?>">

                  <a aria-expanded="true" onclick="ActiveStudent()">Active <span style="font-size: 11px;">(<?php echo $ActiveStudent_count; ?>)</span></a>

               </li>

               <li <?php if ($this->session->userdata('DraftStudent')){ ?> class="active" <?php  } ?>>

                  <a  aria-expanded="false" onclick="DraftStudent()">Draft <span style="font-size: 11px;">(<?php echo $DraftStudent_count; ?>)</span></a>

               </li>

              <li <?php if ($this->session->userdata('TrashStudent')){ ?> class="active" <?php  } ?>>
                  <a  aria-expanded="false" onclick="TrashStudent()">Trash <span style="font-size: 11px;">(<?php echo $TrashStudent_count; ?>)</span></a>
                </li>

            </ul>



</div>

</div>

<div class="col-sm-6 nopading">

   <div class="pull-right">

      <div class="btn-group">

            <?php 

            $usertype = $this->session->userdata("usertype");

               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {

               $display =  "block";

               $addmorebutton = "none";

               $Removemorebutton = "inline-block";

               }else{

               $display = "none";

               $addmorebutton = "inline-block";

               $Removemorebutton = "none";

               }

               

               ?> 

               <?php 

               if ($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Superadmin" || $usertype == "Teacher" || $usertype == "Accountant") {
                
                // $totalstudent="";
                
                if($adminpackage == 1)
                {
                  // echo $totalstudent;die;
                  if($totalstudent < 2)
                    { ?>
                        <a href="<?php echo base_url('student/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
              <?php }
                    else
                    { ?>
                     <a class="btn-top check-all btn bg-success pull-right" data-toggle="modal" data-target="#myModal"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
              <?php }
                }
                else
                {
                 // echo "totalstudent";die;
                  ?>
                  <a href="<?php echo base_url('student/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
               <?php }

               ?> 

   

    <?php  } ?>
             <!-- Start Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Upgrade Plan</h4>
                    </div>
                    <div class="modal-body">
                      <p>Your current plan has reached maximum student addition limit. To enable more students, upgrade your plan.</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <a href="<?php echo base_url('upgradeplan/index') ?>" ><button type="button" class="btn btn-success" >Upgrade Plan</button></a>
                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- End Model -->


       <!--  <a href="#" id="morefilterButtinShow" style="display:<?php echo $addmorebutton ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus" ></span> More Filters</a> -->

        <a  onclick="ResetMorefilter()" id="morefilterButtinHide" style="display:<?php echo $Removemorebutton ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-minus"></span> Remove Filters</a>

        <a href="#" onclick="ResetAllfilter_student()" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-minus"></span> Reset All Filters</a>

      </div>

   </div>

</div>`

<div class="clearfix"></div>



         </div>

         <div class="clearfix"></div>

         <div class="col-sm-12">

         <div class="theme_input_blue">

            <?php 

               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {

               $display =  "block";

               $addmorebutton = "none";

               $Removemorebutton = "inline-block";

               }else{

               $display = "none";

               $addmorebutton = "inline-block";

               $Removemorebutton = "none";

               }

               

               ?>



            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post" >

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        <?=$this->lang->line("student_classes")?>

                        </label>

                        <div class="selectdiv">

                           <?php

                              $array = array("0" => $this->lang->line("student_select_class"));

                              

                              foreach ($classes as $classa) {

                              

                                  $array[$classa->classesID] = $classa->classes;

                              

                              }

                              

                              echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('classesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");

                              

                              ?>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>

            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Sub Courses

                        </label>

                        <div class="selectdiv">

                           <select id="subCourseID" class="form-control">

                              <option value="">Select</option>

                              <?php  foreach ($subCourses as $key => $value) {

                                 if ($value->sub_coursesID==$this->session->userdata('subCourseID')) {

                                     $selected =  "Selected";

                                 }else{

                                   $selected =  "";

                                 }

                                 ?>

                              <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>

                              <?php } ?>

                           </select>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>

            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <div>

                        <label for="classesID" class="control-label">

                        Session

                        </label>

                     </div>

                        <?php

                           $arraySessionFilter = explode('-', $this->session->userdata('sessionFilter'));

                           

                            ?>

                        <div class="col-sm-6 nopading">

                           <div class="forpostionReletive">



                            <select class="form-control" id="SessionFrom">

                              <option>From</option>

                              <?php for($i=2015; $i<=2025; $i++){ ?>

                              <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[0]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                            <?php } ?>

                            </select>



                              <!-- <input type="text" name="" value="<?php if(count($arraySessionFilter)==2) {echo $arraySessionFilter[0]; } ?>" id="SessionFrom" class="form-control CalenderYear" placeholder="From">

                              <div class="postionAbsoluter"><i class="fa fa-calendar" aria-hidden="true"></i></div> -->

                           </div>

                        </div>

                        <div class="col-sm-6 nopading">

                           <div class="forpostionReletive">

                              <select class="form-control" id="SessionTo">

                                <option>To</option>

                                <?php for($i=2015; $i<=2025; $i++){ ?>

                                <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[1]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                              <?php } ?>

                              </select>

                              <!-- <input type="text" name="" id="SessionTo" value="<?php if(count($arraySessionFilter)==2) {echo $arraySessionFilter[1]; } ?>" class="form-control CalenderYear" placeholder="To">

                              <div class="postionAbsoluter"><i class="fa fa-calendar" aria-hidden="true"></i></div> -->

                           </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetSesession()" id="ResetSesession">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>





            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Year/Semester

                        </label>

                        <div class="selectdiv">



                        <select name="yos_filter" id="yos_filter" class='form-control'>

                           

                              <option>Year/Semester</option>

                       		<?php 

                       			if ($classesRow) {

	 $looping    =  (int) $classesRow->duration;

   if ($classesRow->mode==1) {

	  for ($i=1; $i <=$looping; $i++) {

	 	 if ($this->session->userdata('yos_filter')==CallYears($i)) {

	 	$select =  "Selected";

	 }else{

	 	$select =  "";

	 }

	 echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

	 

   }

}



else{

	  for ($i=1; $i <=(2*$looping); $i++) {

	if ($this->session->userdata('yos_filter')==CallSemester($i)) {

	 	$select =  "Selected";

	 }else{

	 	$select =  "";

	 }

	 echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

   }

   }

}

?>

                           </select>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="Resetyos()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>





   <!-- <div class="col-sm-6">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="student_position" class="control-label">

                        Student Position

                        </label>

                        <div class="selectdiv">

                          <select name="student_position" id="student_position" class='form-control'>

                           

                              <option>Student Position</option>

                              <option <?php if($this->session->userdata('student_position')=="Passed Out"){echo "selected";}else{echo '';} ?> value="Passed Out">Passed Out</option>

                              <option <?php if($this->session->userdata('student_position')=="2nd Year with back"){echo "selected";}else{echo '';} ?> value="2nd Year with back">2nd Year with back</option>

                              <option <?php if($this->session->userdata('student_position')=="Back Out"){echo "selected";}else{echo '';} ?> value="Back Out">Back Out</option>

                              <option <?php if($this->session->userdata('student_position')=="3rd Year with back"){echo "selected";}else{echo '';} ?> value="3rd Year with back">3rd Year with back</option>

                              <option <?php if($this->session->userdata('student_position')=="Refund"){echo "selected";}else{echo '';} ?> value="Refund">Refund</option>

                                                        </select>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="Resetstudentposition()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div> -->



            <!-- <div class="col-sm-6">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Payment Status

                        </label>

                        <div class="selectdiv">

                          <select name="payment_status" id="payment_status" class='form-control'>

                           

                              <option>Payment Status</option>

                              <option <?php if($this->session->userdata('payment_status')==2){echo "selected";}else{echo '';} ?> value="2">Paid</option>

                              <option <?php if($this->session->userdata('payment_status')==1){echo "selected";}else{echo '';} ?> value="1">Partial Paid</option>

                              <option <?php if($this->session->userdata('payment_status')==3){echo "selected";}else{echo '';} ?> value="3">Not paid</option>

                           </select>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="Resetpaymentstatus()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>
 -->






            <div class="clearfix"></div>

         </div>

      </div>

         <div class="clearfix"></div>

         <!-- col-sm-12 -->

         <div class="col-sm-12" id="MorefilterPanel" style="display:<?php echo $display ?>">

            <div class="theme_input_blue">

            <div class="col-sm-3">

               <div class="selectdiv">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Session Type

                        </label>

                        <div class="">

                           <?php

                              $array = array("0" => "Select Session Type","A"=>"A","C"=>"C");

                              

                              echo form_dropdown("sessionType", $array, set_value("sessionType",$this->session->userdata('sessionType')), "id='sessionType' class='form-control'");

                              

                              ?>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetsessionType()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>

            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Exam Type

                        </label>

                        <div class="selectdiv">

                           <?php

                              $array = array("0" => "Select Exam Type","o"=>"One Time","y"=>"Yearly");

                              

                              echo form_dropdown("examType", $array, set_value("examType",$this->session->userdata('examType')), "id='examType' class='form-control'");

                              

                              ?>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetexamType()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>

            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Education Mode

                        </label>

                        <div class="selectdiv">

                           <?php

                              $array = array("0" => "Select Education Mode","2"=>"Semester","1"=>"Year");

                              

                              echo form_dropdown("education_mode", $array, set_value("education_mode",$this->session->userdata('education_mode')), "id='education_mode' class='form-control'");

                              

                              ?>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="Reseteducation_mode()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>

            <div class="col-sm-3">

               <div class="">

                  <form style="" class="form-horizontal" role="form" method="post">

                     <div class="form-group">

                        <label for="classesID" class="control-label">

                        Counsellor

                        </label>

                        <div class="selectdiv">

                           <?php

                              $array = array("0" => "Select Counsellor");

                              

                              

                              foreach ($counsellor as $counsellors) {

                              

                                  $array[$counsellors->teacherID] = $counsellors->name;

                              

                              }

                              

                              echo form_dropdown("teacherID", $array, set_value("teacherID",$this->session->userdata('teacherID')), "id='teacherID' class='form-control'");

                              

                              ?>

                        </div>

                        <div class="clearfix"></div>

                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetteacherID()">Reset This Filter </a></div>

                     </div>

                  </form>

               </div>

            </div>



            



           





            <div class="clearfix"></div>

         </div>

      </div>

         <div class="clearfix"></div>

         <!-- col-sm-12 dusra -->

<div class="col-sm-12">

<div class="">

   <div class="tab-content">

      <div id="all" class="tab-pane active">



<div id="hide-table">

            <?php 

               if ($students) { ?>
<?php if($usertype=="ClgAdmin" || $usertype == "Admin" || $usertype=="Accountant" ){ ?>

<form action="<?php echo base_url() ?>Student/multipleAction" onsubmit="return confirm('Do you really want to submit the form?');" method = "post">

                <div class="action-layout">

                    <ul>

                        <li>

                            <label class="nexCheckbox">Check

                                <input type="checkbox" name="select_all" id="select_all">

                                <span class="checkmark checkmark-action-layout"></span>

                            </label>

                        </li>
						<?php
							if(empty($this->session->userdata('yos_filter')))
							{
								
							}
							else
							{
						?>
								<li class="active-btn">
									<input type="submit" class="btn btn-warning etsfilertButton disabled" name="Promote" value="Promote" disabled>
								 </li>
						<?php
							}
						?>

                        <li class="active-btn">

                            <input type="submit" class="btn btn-success etsfilertButton disabled"  name="Active" value="Active" disabled>

                        </li>

                        <li class="active-btn">

                            <input type="submit" class="btn btn-info etsfilertButton disabled" name="Draft" value="Draft" disabled>

                        </li>

                        <li class="active-btn">

                            <input type="submit" class="btn btn-danger etsfilertButton disabled" name="Delete" value="Delete" disabled>

                        </li>
                        <li class="active-btn">

                            <!-- <input type="button" class="btn btn-warning etsfilertButton disabled" name="sendmailbyadmin" data-toggle="modal" data-target="#myModal" value="Send Mail" disabled> -->
                             <a href="#modal-2" class="btn btn-warning etsfilertButton disabled" name="sendmailbyadmin" data-toggle="modal" data-target="#modal-2" disabled>Send Mail</a>

                        </li>

                    </ul>

                </div>    

            <table id="example4" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

               <thead>

                  <tr>

                     <th></th>

                     <th >S.no</th>

                     <th><?=$this->lang->line('student_photo')?></th>

                     <th>Student Id</th>

                     <th><?=$this->lang->line('student_roll')?></th>

                     <th><?=$this->lang->line('student_name')?></th>

                     <th><?=$this->lang->line('father_name')?></th>                      

                     <th>Semester/Year</th>

                     <th><?=$this->lang->line("student_classes")?></th>

                     <th><?=$this->lang->line('sub_coursesID')?></th>           

                     

                     <th><?=$this->lang->line('session')?></th>

                     <th><?=$this->lang->line('sessionType')?></th>

                     

                

                     <th><?=$this->lang->line('student_entry_type')?></th>

                     <th><?=$this->lang->line('student_phone')?></th>

                     <th><?=$this->lang->line('student_dob')?></th>

                     <th><?=$this->lang->line('student_sex')?></th>

                     <th><?=$this->lang->line('student_email')?></th>

                     <th><?=$this->lang->line('mother_name')?></th>                     

                     <th><?=$this->lang->line('aadhar')?></th>

                     <th><?=$this->lang->line('nationality')?></th>

                     <th ><?=$this->lang->line('create_date')?></th>

                     <th>Total Amount</th>

                     <th>Paid Amount</th>

                     <th>Payment Status</th>

                     <th><?=$this->lang->line('action')?></th>

                  </tr>

               </thead>
                 
            </table>
            <!-- Modal Start-->
                     <div class="modal modal-box-2 fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog">
                         <div class="modal-content" id="myModalLabel">
                           <div class="modal-header theme-bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                           </div>
                          <div class="modal-body">
                            <h3>Custom <span>Mail</span></h3>
                            <!-- <form name="sentMessage" class="contactForm"> -->
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="subjectname" name="subjectname" placeholder="Enter subject name" >
                                    <p class="help-block text-danger"></p>
                                  </div>
                                  <div class="form-group">
                                    <textarea class="form-control" id="writemail" name="writemail" placeholder="Write Your Message Here"></textarea>  
                                    <p class="help-block text-danger"></p>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                  <div id="success"></div>
                                  <input type="submit" class="btn btn-default etsfilertButton disabled" name="sendmailbyadmin" value="Send Mail">
                                </div>
                              </div>
                            <!-- </form> -->
                          </div>
                        </div>
                      </div>
                    </div>
             <!-- Modal Start-->
                            <!-- <div class="modal fade" id="myModal" role="dialog">
                              <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Custom Mail</h4>
                                  </div>
                                  
                                  <div class="modal-body">
                                    <table>
                                      <tr>
                                        <th>Subject</th>
                                        <td><input type="text" name="subjectname" id="subjectname"></td>
                                      </tr>
                                      <tr>
                                        <th>Write Mail</th>
                                        <td><input type="text" name="writemail" id="writemail"></td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="modal-footer"> -->
                                    <!-- <input type="hidden" name="checked_id[]" value="<?php echo $post->studentID;?>"> -->
                                   <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" class="btn btn-default etsfilertButton disabled" name="sendmailbyadmin" value="Send Mail">
                                  </div>
                                  
                                </div>
                              </div>
                            </div>
                          </div> -->
                          <!-- MOdel End -->
            </form>
            
  <?php } ?>
<!-- <?php if($usertype=="Accountant"){ ?>
            <table id="students_for_ac" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">
               <thead>
                  <tr>
                     <th></th>

                     <th >S.no</th>

                     <th><?=$this->lang->line('student_photo')?></th>

                     <th>Student Id</th>

                     <th><?=$this->lang->line('student_roll')?></th>

                     <th><?=$this->lang->line('student_name')?></th>

                     <th><?=$this->lang->line('father_name')?></th>                      

                     <th>Year</th>

                     <th><?=$this->lang->line("student_classes")?></th>

                     <th><?=$this->lang->line('sub_coursesID')?></th>           

                     

                     <th><?=$this->lang->line('session')?></th>

                     <th><?=$this->lang->line('sessionType')?></th>

                     

                

                     <th><?=$this->lang->line('student_entry_type')?></th>

                     <th><?=$this->lang->line('student_phone')?></th>

                     <th><?=$this->lang->line('student_dob')?></th>

                     <th><?=$this->lang->line('student_sex')?></th>

                     <th><?=$this->lang->line('student_email')?></th>

                     <th><?=$this->lang->line('mother_name')?></th>                     

                     <th><?=$this->lang->line('aadhar')?></th>

                     <th><?=$this->lang->line('nationality')?></th>

                     <th ><?=$this->lang->line('create_date')?></th>

                     <th>Total Amount</th>

                     <th>Paid Amount</th>

                     <th>Payment Status</th>

                     <th><?=$this->lang->line('action')?></th>

                  </tr>

               </thead>

            </table>
          <?php } ?> -->

            <?php } else { ?>

            <div class="Noresuls">

               <h1>Sorry we couldn't find any matches</h1>

               <p>Maybe your search was too specific, please try searching with another term.</p>

               <img src="<?php echo base_url() ?>uploads/images/404.png">

            </div>

            <?php } ?>

         </div>



      </div>

   </div>

   <!-- nav-tabs-custom -->

</div>

   </div>

   <div class="clearfix"></div>

</div>

</div>

</div>

   <!-- Body -->

<!-- col-sm-12 for tab -->

<!-- end table form here -->

<script type="text/javascript">

   $('#classesID').change(function() {
     $('#example4').DataTable().state.clear();
       var classesID = $(this).val();
           $.ajax({
               type: 'POST',
               url: "<?=base_url('student/index')?>",
               data: "classesID=" + classesID,
               dataType: "html",
               success: function(data) {
                    location.reload();
               }
           });
   });
</script>

<script type="text/javascript">

   $('#subCourseID').change(function() {

     $('#example4').DataTable().state.clear();

   

       var subCourseID = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "subCourseID=" + subCourseID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#SessionTo').change(function() {

     $('#example4').DataTable().state.clear();

   

       var SessionTo = $(this).val();

       var SessionFrom  = $('#SessionFrom').val();

   

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data:{SessionTo:SessionTo,SessionFrom:SessionFrom},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#sessionType').change(function() {

    $('#example4').DataTable().state.clear();

       var sessionType = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "sessionType=" + sessionType,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#examType').change(function() {

     $('#example4').DataTable().state.clear();

   

       var examType = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "examType=" + examType,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#education_mode').change(function() {

     $('#example4').DataTable().state.clear();

   

       var education_mode = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "education_mode=" + education_mode,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>



<script type="text/javascript">

   $('#student_position').change(function() {

     $('#example4').DataTable().state.clear();

   

       var student_position = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "student_position=" + student_position,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>



<script type="text/javascript">

   $('#yos_filter').change(function() {

     $('#example4').DataTable().state.clear();

   

       var yos_filter = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "yos_filter=" + yos_filter,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#payment_status').change(function() {

     $('#example4').DataTable().state.clear();

   

       var payment_status = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('student/index')?>",

   

               data: "payment_status=" + payment_status,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">
   $('#teacherID').change(function() {
        $('#example4').DataTable().state.clear();
        var teacherID = $(this).val();
        $.ajax({
           type: 'POST',
           url: "<?=base_url('student/index')?>",
           data: " teacherID=" + teacherID,
           dataType: "html",
           success: function(data) {
                location.reload();
           }
        });
   });
</script>

<script type="text/javascript">
	function ResetSesession(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetSesession')?>",
			data:{ResetSesession:'ResetSesession'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetCourses(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetCourses')?>",
			data:{ResetCourses:'ResetCourses'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetSubcourses(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetSubcourses')?>",
			data:{ResetSubcourses:'ResetSubcourses'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetsessionType(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetsessionType')?>",
			data:{ResetCourses:'ResetCourses'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetexamType(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetexamType')?>",
			data:{ResetCourses:'ResetCourses'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function Reseteducation_mode(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/Reseteducation_mode')?>",
			data:{ResetCourses:'ResetCourses'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetAllfilter_student(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetAllfilter')?>",
			data:{ResetAllfilter:'ResetAllfilter'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetMorefilter(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetMorefilter')?>",
			data:{ResetMorefilter:'ResetMorefilter'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function ResetteacherID(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/ResetteacherID')?>",
			data:{ResetMorefilter:'ResetMorefilter'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function Resetstudentposition(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/Resetstudentposition')?>",
			data:{ResetMorefilter:'ResetMorefilter'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function Resetyos(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/Resetyos')?>",
			data:{ResetMorefilter:'ResetMorefilter'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	function Resetpaymentstatus(){
		$('#example4').DataTable().state.clear();
		$.ajax({
			type: 'POST',
			url: "<?=base_url('student/Resetpaymentstatus')?>",
			data:{ResetMorefilter:'ResetMorefilter'},
			dataType: "html",
			success: function(data) {
				location.reload();
			}
		});
	}
	
    function get_fb()
    {
        $.ajax({
		    type:"POST",
		    url:"<?php echo base_url(); ?>student/update_student_notification",
		    success:function(data){
		      $(".new").css("display","none");
		    }
		}); 
    }
    setTimeout(function(){ get_fb(); },10000);
    
    
    $(document).on('click','.send_sms',function(e){
        e.preventDefault();
        phone = this.id;
        username = $(this).data("id");
        password = $("#passwords_"+phone).val();
        name = $("#student_name_"+phone).val();
        var IndNum = /^[0]?[789]\d{9}$/;
        if(IndNum.test(phone))
        {
            $.ajax({
                type:"POST",
                data:{"phone":phone,"username":username,"password":password,"name":name},
                url:"<?php echo base_url(); ?>student/send_sms",
                success: function(data){
                    alert(data);
                },
                error: function(){
                    console.log("error found");
                }
            });
        }
        else
        {
            alert('Mobile Number Not Valid');
        }
    });
</script>
<script type="text/javascript">
   $("#morefilterButtinShow").click(function(){
   $("#MorefilterPanel").show();
   $("#morefilterButtinShow").hide();
   $("#morefilterButtinHide").show();
   });
   $("#morefilterButtinHide").click(function(){
   $("#MorefilterPanel").hide();
   $("#morefilterButtinShow").show();
   $("#morefilterButtinHide").hide();
   });
</script>