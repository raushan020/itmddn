<style type="text/css">

    #upload_images_mess{

        display:none;

        color:green;

    }

</style>

<input type="hidden" name="studentID" id = "studentID" value="0">
<?php $adminpackage=$this->session->userdata('adminpackage');

if($adminpackage == 1) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     You can add only 50 students, To add unlimitted go for premium modal! <a href="<?php echo base_url('upgradeplan') ?>" class="alert-link">upgrade now</a>.
   </div>
  
<?php }  ?>

<div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="ti ti-user"></i> <?=$this->lang->line('panel_title')?> </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li class="breadcrumb-item"><i class="fa fa-bullseye"></i> <a href="<?=base_url("dashboard/index")?>"><?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="breadcrumb-item"><a href="<?=base_url("student/index")?>"><?=$this->lang->line('menu_student')?></a></li>

                <li class="breadcrumb-item active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('panel_title')?></li> </li>

            </ol>

      	</div>

     </div>

    <!-- form start -->

    <div class="container-fluid">
    	 <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">

            <div class="col-sm-12">

                <form class="form-horizontal" autocomplete="off" role="form" onsubmit="return validateForm()" method="post" enctype="multipart/form-data">
                   <!--  <form class="form-horizontal" autocomplete="off" role="form" novalidate  method="post" enctype="multipart/form-data"> --> <!-- off validation -->

					<div class="persnolInfo">

						<p>Field are required with <span class="red-color">*</span></p>

						<h3  class="border_heading">Personal Information</h3>

						<?php 



							if(form_error('name')) 



								echo "<div class='form-group has-error' >";



							else     



								echo "<div class='form-group' >";



						?>



						<label for="name_id" class="col-sm-2 control-label">



							<?=$this->lang->line("student_name")?><span class="red-color">*</span>



						</label>



						<div class="col-sm-6">



							<input type="text" class="form-control" required placeholder="<?=$this->lang->line("student_name")?>" id="name_id" name="name" value="<?=set_value('name')?>" >



						</div>



						<span class="col-sm-4 control-label">



							<?php echo form_error('name'); ?>



						</span>



					</div>



					<?php 



						if(form_error('father_name'))



							echo "<div class='form-group has-error' >";



						else     



							echo "<div class='form-group' >";



					?>



					<label for="guardiaName" class="col-sm-2 control-label">



						<?=$this->lang->line("father_name")?><span class="red-color">*</span>



					</label>



					<div class="col-sm-6">



						<div class="select2-wrapper">



							<input type="text" class="form-control" required placeholder="<?=$this->lang->line("father_name")?>" id="father_name" name="father_name" value="<?=set_value('father_name')?>" >



						</div>



					</div>



					<span class="col-sm-4 control-label">



						<?php echo form_error('father_name'); ?>



					</span>



				</div>







				<?php 



					if(form_error('mother_name'))



						echo "<div class='form-group has-error' >";



					else     



						echo "<div class='form-group' >";



				?>



				<label for="guardiaName" class="col-sm-2 control-label">



					<?=$this->lang->line("mother_name")?><span class="red-color">*</span>



				</label>



				<div class="col-sm-6">



					<div class="select2-wrapper">



						<input type="text" class="form-control" required placeholder="<?=$this->lang->line("mother_name")?>" id="mother_name" name="mother_name" value="<?=set_value('mother_name')?>" >



					</div>



				</div>



				<span class="col-sm-4 control-label">



					<?php echo form_error('mother_name'); ?>



				</span>



			</div>





			<?php 



				if(form_error('dob')) 



					echo "<div class='form-group has-error' >";



				else     



					echo "<div class='form-group' >";



			?>



							<label for="dob" class="col-sm-2 control-label">



								<?=$this->lang->line("student_dob")?><span class="red-color">*</span>



							</label>



							<div class="col-sm-6">



								<input type="text" class="form-control datepicker_quiz_data" required placeholder="<?=$this->lang->line("student_dob")?>"  name="dob" value="<?=set_value('dob')?>" >



							</div>



							<span class="col-sm-4 control-label">

								<?php echo form_error('dob'); ?>

							</span>

						</div>

							<?php 

								if(form_error('sex')) 

									echo "<div class='form-group has-error' >";

								else 

									echo "<div class='form-group' >";

							?>

							<label for="sex" class="col-sm-2 control-label">

								<?=$this->lang->line("student_sex")?><span class="red-color">*</span>

							</label>

							<div class="col-sm-6">

								<?php 

									echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex"), "id='sex' class='form-control' required"); 

								?>

							</div>

							<span class="col-sm-4 control-label">

								<?php echo form_error('sex'); ?>

							</span>

						</div>

						
					<?php 



						if(form_error('email')) 



							echo "<div class='form-group has-error' >";



						else     



							echo "<div class='form-group' >";



					?>



					<label for="email" class="col-sm-2 control-label">



			        <?=$this->lang->line("student_email")?><span class="red-color">*</span>



					</label>



					<div class="col-sm-6">


                        <!--checkemail function write here:- assets/etslabs/js/etsstudent.js -->
						<input type="email" required class="form-control" onkeyup="checkemail()" placeholder="<?=$this->lang->line("student_email")?>" id="email" name="email" value="<?=set_value('email')?>" >



					</div>



					<span class="col-sm-4 control-label"  id="emailID">



						<?php echo form_error('email'); ?>



					</span>



				</div>







					<?php 



						if(form_error('phone')) 



							echo "<div class='form-group has-error' >";



						else     



							echo "<div class='form-group' >";



					?>



					<label for="phone" class="col-sm-2 control-label">



						<?=$this->lang->line("student_phone")?><span class="red-color">*</span>



					</label>



					<div class="col-sm-6">



						<input type="tel"  class="form-control" required  placeholder="<?=$this->lang->line("student_phone")?>" id="phonecountry" name="phone" value="<?=set_value('phone')?>" >

						<span id="valid-msg" class="hide">✓ Valid</span>

                        <span id="error-msg" class="hide">Invalid number</span>

					</div>



					<span class="col-sm-4 control-label">



						<?php echo form_error('phone'); ?>



					</span>



				</div>





				<?php 



					if(form_error('address')) 



						echo "<div class='form-group has-error' >";



					else     



						echo "<div class='form-group' >";



				?>



				<label for="address" class="col-sm-2 control-label">

					<?=$this->lang->line("student_address")?>

				</label>

				<div class="col-sm-6">

					<input type="text" class="form-control" placeholder="<?=$this->lang->line("student_address")?>" id="address" name="address" value="<?=set_value('address')?>" >

				</div>

				<span class="col-sm-4 control-label">

					<?php echo form_error('address'); ?>

				</span>

			</div>



			<?php 

			if(form_error('pin')) 
				echo "<div class='form-group has-error' >";
			else 
				echo "<div class='form-group' >";
			?>



		<label for="address" class="col-sm-2 control-label">
			<?=$this->lang->line("pin")?>
		</label>

		<div class="col-sm-6">

			<input type="text" class="form-control" placeholder="<?=$this->lang->line("pin")?>" id="pin" name="pin" value="<?=set_value('pin')?>" >

		</div>

		<span class="col-sm-4 control-label">

			<?php echo form_error('pin'); ?>

		</span>

	</div>





	<?php 

		if(form_error('nationality')) 

			echo "<div class='form-group has-error' >";

		else     

			echo "<div class='form-group' >";

	?>



	<label for="address" class="col-sm-2 control-label">

		<?=$this->lang->line("nationality")?>

	</label>

	<div class="col-sm-6">	

		<select class="form-control" placeholder="<?=$this->lang->line("nationality")?>" id="nationality" name="nationality" >

			<option value="">Select</option>

			<?php

			foreach ($country as $key => $value) {?>

			<option value="<?php echo $value->nationality ?>">

				<?php echo $value->nationality ?>

			</option>

			<?php } ?>

		</select>


	</div>



	<span class="col-sm-4 control-label">



		<?php echo form_error('nationality'); ?>



	</span>



</div>







<?php 



	if(form_error('aadhar')) 



		echo "<div class='form-group has-error' >";



	else     



		echo "<div class='form-group'>";



?>



				<label for="address" class="col-sm-2 control-label">

					Aadhaar no. 

				</label>

				<div class="col-sm-6">

					<input type="number" class="form-control" placeholder="<?=$this->lang->line("aadhar")?>" id="aadhar" name="aadhar" value="<?=set_value('aadhar')?>" >

				</div>
				

				<span class="col-sm-4 control-label">

					<?php echo form_error('aadhar'); ?>

				</span>

				</div>


<!-- enrollment -->
				<?php 

				if(form_error('roll')) 

					echo "<div class='form-group has-error' >";

				else     

					echo "<div class='form-group' >";

				?>



				<label for="roll" class="col-sm-2 control-label">



				<?=$this->lang->line("student_roll")?>

				</label>

				<div class="col-sm-6">

				<input type="text" class="form-control" placeholder="<?=$this->lang->line("student_roll")?>" id="roll" name="enrollment" value="<?=set_value('roll')?>" >
				</div>
				<span class="col-sm-4 control-label" id ='rollID'>
				<?php echo form_error('roll'); ?>
				</span>
				</div>

        <!-- roll no -->
        <?php 

        if(form_error('roll')) 

          echo "<div class='form-group has-error' >";

        else     

          echo "<div class='form-group' >";

        ?>

        <label for="roll" class="col-sm-2 control-label">

         Roll No.

        </label>

        <div class="col-sm-6">

        <input type="text" class="form-control" placeholder="Enter Roll no." id="roll_no" name="roll" value="<?=set_value('roll_no')?>" >
        </div>


        <span class="col-sm-4 control-label" id ='rollID'>

        <?php echo form_error('roll_no'); ?>

        </span>


        </div>




				<?php 



				if(isset($image)) 



				echo "<div class='form-group has-error' >";



				else     



				echo "<div class='form-group' >";



				?>



				<label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">



				<?=$this->lang->line("student_photo")?>



				</label>



				<div class="col-sm-4 col-xs-6 col-md-4">



				<input class="form-control"   id="uploadFile" placeholder="Choose File" disabled />  



				</div>







				<div class="col-sm-2 col-xs-6 col-md-2">



				<div class="fileUpload btn btn-success form-control">



				<span class="fa fa-repeat"></span>



				<span><?=$this->lang->line("upload")?></span>



				<input type="file" class="upload" name="upload_image" id="upload_image_student" />



				</div>



				</div>

<div id="uploaded_image_student"></div>

				<span class="col-sm-4 control-label col-xs-6 col-md-4" id = "upload_images_mess">							   

					<i class="fa fa-check" aria-hidden="true"></i> Uploaded

					<?php if(isset($image)) echo $image; ?>



				</span>



				</div>







				<!-- <?php 



				if(form_error('username')) 



				echo "<div class='form-group has-error' >";



				else     



				echo "<div class='form-group' >";



				?>



				<label for="username" class="col-sm-2 control-label">



				<?=$this->lang->line("student_username")?><span class="red-color">*</span>



				</label>



				<div class="col-sm-6">

				<input type="text" class="form-control" required onkeyup="lol_username_ajax()" id="usernameStu"  name="username" value="<?=set_value('username')?>" autocomplete="off" >

				</div>



				<span class="col-sm-4 control-label" id="usernameID">

				<?php echo form_error('username'); ?>

				</span>



				</div>





				<?php 



				if(form_error('password')) 



				echo "<div class='form-group has-error' >";



				else     



				echo "<div class='form-group' >";



				?>



				<label for="password" class="col-sm-2 control-label">



				<?=$this->lang->line("student_password")?><span class="red-color">*</span>



				</label>



				<div class="col-sm-6 positionRelative">



				<input type="password" class="form-control" required id="passwordStu" id="password" name="password" value="<?=set_value('password')?>" >

				<div class="positionAbsolutePassword" autocomplete="off">

				<img src="<?php echo  base_url() ?>uploads/images/eye.png" onclick="passwordShow()">

				</div>



				</div>



				<span class="col-sm-4 control-label">



				<?php echo form_error('password'); ?>



				</span>



				</div> -->





				</div>

				</br>

				<div class="Course Details">

				<h3 class="border_heading">Course Details</h3>





				<?php 



				if(form_error('classesID')) 



				echo "<div class='form-group has-error' >";



				else     



				echo "<div class='form-group' >";



				?>

				<label for="classesID" class="col-sm-2 control-label">Course<span class="red-color">*</span></label>

				<div class="col-sm-6">

				<select name="classesID" required onchange="ajaxGet_subCourses($(this).val())" id="classesID" class="form-control" value="<?php echo set_value('classesID'); ?>">

				<option value="">Select Course</option>

				<?php foreach($classes as $courses){ ?>

				<option value="<?php echo $courses->classesID ?>"><?php echo $courses->classes ?> </option>

				<?php } ?>



				</select>                       

				</div>

				<span class="col-sm-4 showLoaderSubcour">

				<img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">

				</span>

				<span class="col-sm-4 control-label">

				<?php echo form_error('classesID'); ?>

				</span>

				</div>





				<!-- print subcourses here -->

				<div id="subCourseID">



				</div>													

				<div id="ModeType"></div>





				<!--  -->



				<?php 



				if(form_error('sessionFrom') || form_error('sessionTo')) 



				echo "<div class='form-group has-error' >";



				else     



				echo "<div class='form-group' >";



				?>

				<label for="" class="col-sm-2 control-label" >Session<span class="red-color">*</span></label>

				<div class="col-sm-3"> 

				 	<select class="form-control" name="sessionFrom">

	                    <option value="">From</option>

	                    <?php for($i=2010; $i<=2025; $i++){ ?>

	                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>

	                    <?php } ?>

                	</select>

				</div>

				<div class="col-sm-3"> 



					<select class="form-control" name="sessionTo">

                                <option value="">To</option>

                                <?php for($i=2010; $i<=2025; $i++){ ?>

                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>

                              <?php } ?>

                              </select>

				</div>



				<span class="col-sm-4 control-label">

				<?php echo form_error('sessionFrom'); ?>

				<?php echo form_error('sessionTo'); ?>

				</span>

				</div>

				


					</div>

					</br>

					<div ></div>

					<div class='Course Details' id='ApendDataAcadamic'>

					</div>

					</br>











				<div class="Course Details">													
				<div id="ModeType"></div>
					<div ></div>

					<div class='Course Details' id='ApendDataAcadamic'>

					</div>					

					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" id="submit_student" class="btn btn-success add-btn" value="<?=$this->lang->line("add_student")?>" >
					</div>
                </form>
            </div> <!-- col-sm-8 -->
        </div><!-- row -->
    </div>
</div>
</div>
    </div><!-- Body -->
</div>
<style type="text/css">
	.red-color{
		color:red;
	}
	.hide{
		color:red;
	}
</style>
<script type="text/javascript">
    function AddUsernamePass()
	{
		$('.displayNone').show();
		var name_id = $('#name_id').val();
		$.ajax({
			type:'post',
			url:"<?php echo base_url() ?>teacher/AddUsernamePass",
			data:{name_id:name_id},
			success:function(response) {
				var objective = jQuery.parseJSON(response);
				$('#usernameStu').val(objective.username);
				$('#passwordStu').val(objective.password);
				$('.displayNone').hide();
			}
		});
    }
</script>
<div id="uploadimageModalStudent" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_student" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                        <?php if(!isset($image)){ ?>
                          <button class="btn btn-success crop_image_student" id="imgUpdloader" onclick="imgUpdloader($this.val())">Crop & Upload Image</button>
                      	<img style="display: none;" id="imageUploader" src="<?php echo base_url() ?>uploads/images/loading-circle.gif">
                      <?php } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function compare_payment()
{
	var amount =  document.getElementById("amount").value;
	var paidamount = document.getElementById("paidamount").value;
	if(Number(paidamount) > Number(amount))
	{
		alert("Does Not Exceed Admission Fee!");
	}
}
</script>
<script>
	function imgUpdloader(){
		// alert('hrllo');
		$('#imageUploader').show();
		// element.setAttribute("style", "display: none;");

		$('#imageUploader').attr("display", "none");
	}
</script>
<script type="text/javascript">
	function validateForm() {
		aadhar = document.getElementById("aadhar").value;
		var adharcard = /^\d{12}$/;
		if (aadhar != '') {
			 if (!aadhar.match(adharcard))
			 {
				 alert("Invalid Aadhar Number");
				 return false;
			 }
		 }
	}
</script>