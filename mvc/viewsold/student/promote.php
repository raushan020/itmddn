<div class="row page-titles">

	<div class="col-md-5 align-self-center">

		<h3 class="text-themecolor"><i class="fa fa-user"></i> Promote Student</h3>

	</div>

	<div class="col-md-7 align-self-center">

		<ol class="breadcrumb">

			<li class="breadcrumb-item">

				<i class="fa fa-bullseye"></i> 

				<a href="<?=base_url("dashboard/index")?>">

					<?=$this->lang->line('menu_dashboard')?>

				</a>

			</li>

			<li class="breadcrumb-item">

				<a href="<?=base_url("student/index")?>">

					<?=$this->lang->line('menu_student')?>

				</a>

			</li>

			<li class="breadcrumb-item active">Promote Student</li>

		</ol>

	</div>

</div>

<div class="container-fluid">
	<button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>

	<div class="row">

		<div class="">

			<div class="card">

				<div class="card-body">

					<style type="text/css">

						.top_heading{

							text-align:center;

						}

					</style>

					<form action="<?php echo base_url() ?>student/promote_student" method='post'>

						<table class="table">

							<thead>

								<tr>

									<th>Student name</th>

									<th>Father name</th>

									<th>Course name</th>

									<th>Semester</th>

									<th>Promote In</th>

									<th>Action</th>

								</tr>

							</thead>

							<tbody>

								<?php

									foreach ($total_student as $key => $value) 

									{

								?>

										<tr>

											<td><?php echo $value->name ?></td>

											<td><?php echo $value->father_name ?></td>

											<td><?php echo $value->classes ?></td>

											<td><?php echo $value->yearsOrSemester ?></td>

											<td>

												<select name="yos_filter[]" id="yos_filter" class='form-control'>

													<?php 

														if ($classesRow) 

														{

															$looping = (int)$classesRow->duration;

															if ($classesRow->mode==1) 

															{

																for ($i=1; $i <=$looping; $i++) 

																{

																	if ($this->session->userdata('yos_filter')==CallYears($i))

																	{

																		$next =  $i+1;

																	}
                                     			
																	$select = ($next == $i)?"selected":"";



																	echo "<option ".$select." value=".CallYears($i).">".str_replace(CallYears)."</option>";

																}

															}

															else

															{

																for ($i=1; $i <=(2*$looping); $i++) 

																{

																	if ($this->session->userdata('yos_filter')==CallSemester($i))

																	{

																		$next =  $i+1;

																	}

																	$select = ($next == $i)?"selected":"";



																	echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

																}

															} 

														}

													?>

												</select>

											</td>

											<td>

												<label>

													<input type="checkbox" name="checked_id[]" value="<?php echo $value->studentID ?>" checked>

												</label>

											</td>

										</tr>

								<?php 

									} 

								?>

							</tbody>

						</table>

						

						<div class="col-md-12">

							<center><button class="btn btn-success ">Promote</button></center>

						</div>

					</form>

				</div>

			</div>

		</div>

	</div>

</div>