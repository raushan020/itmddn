<!DOCTYPE html>

<html lang="en">

    <head>

      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

      <meta name="description" content="">

      <meta name="author" content="">



      <title><?=$this->lang->line('panel_title')?></title>

      <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" />

      <script>

        var uri =  "<?php echo $this->uri->segment(3) ?>";

        var uri4 =  "<?php echo $this->uri->segment(4) ?>";

        var base_url="<?php echo base_url();?>";



      </script>

      

      <?=link_tag('assets/etslabs/css/croppie.css');?>

      <?=link_tag('assets/etslabs/css/all_icons.css');?>

      <?=link_tag('assets/plugins/bootstrap/css/bootstrap.css');?>        

      <?=link_tag('assets/etslabs/css/style.css');?>   

      <?=link_tag('assets/etslabs/css/skin/default-skin.css');?>     

      <?=link_tag('assets/fonts/font-awesome.css');?>

      <?=link_tag('assets/etslabs/css/styleExam.css');?> 
       
      <?=link_tag('assets/etslabs/css/kavach.min.css');?>

      <?=link_tag('assets/etslabs/css/button-style.css');?>


      <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.3.3/video-js.css" rel="stylesheet">

      <style type="text/css">

        .vjs-default-skin { color: #eb1f1f; }

        .vjs-default-skin .vjs-play-progress,

        .vjs-default-skin .vjs-volume-level { background-color: #284c78 }

        .vjs-default-skin .vjs-control-bar,

        .vjs-default-skin .vjs-big-play-button { background: rgba(34,169,189,0.7) }

        .vjs-default-skin .vjs-slider { background: rgba(34,169,189,0.2333333333333333) }

        .vjs-default-skin .vjs-control-bar { font-size: 86% }

      </style>       
  	  <script src="<?php echo base_url('assets/etslabs/js/exam.js');?>"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>




      <!-- EC2 SERVER JAVASCRIPT -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/jquery/jquery-1.12.0.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/jquery/jquery-ui.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/jquery/jquery.websocket.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/jquery/jquery.json.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/js/utils.js"></script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/Flashphoner.js"></script>
      <script>
          var record = true;
      </script>
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/js/streaming.js"></script>
      <!-- Bootstrap JS -->
      <script type="text/javascript" src="<?php echo base_url()?>assets/livestream/dependencies/swf/swfobject.js"></script>
    </head>

    <body onload="initAPI()">

    <?php

      $URI = "$_SERVER[REQUEST_URI]";

      $uriMain= str_replace('/dev/','',$URI);

    ?>   

    <input type="hidden" name="uri_findOutFrom_urls_jquery" value="<?php echo $uriMain ?>" id="uri_findOutFrom_urls_jquery" >

    <script type="text/javascript">

      var redirecturi =  $('#uri_findOutFrom_urls_jquery').val();

    </script>

<?php $usertype = $this->session->userdata("usertype");

if ( $usertype== "Student") {
$activeClass = "active";
}else 
{
 $activeClass = ""; 
}

 ?>

    <div id="wrapper" class="<?php echo $activeClass ?>">

    <div class="fakeLoader"></div>

    <?php if ($this->session->flashdata('success')): ?>

    <script type="text/javascript">

     swal(" ", "<?=$this->session->flashdata('success') ?>", "success");

       setTimeout( "$('.swal-overlay').hide();", 3000);

    </script>

    <?php endif ?>

    <?php if ($this->session->flashdata('error')): ?>

    <script type="text/javascript">

      swal("<?=$this->session->flashdata('error') ?>");

       setTimeout( "$('.swal-overlay').hide();", 3000);

    </script>

    <?php endif ?>



    <script type="text/javascript">

      $('side-menu').click(function(){
        $( ".tabs" ).tabs( "option", "active", 2 );
      });

    </script>

