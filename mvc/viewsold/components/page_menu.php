<div class="navbar-default sidebar hold-transition skin-blue sidebar-collapse sidebar-mini" id="dsds" role="navigation">
   <div class="sidebar-nav navbar-collapse collapse">
      <?php $usertype = $this->session->userdata("usertype"); ?>
      <ul class="nav" id="side-menu">
         <li class="">
            <?php
               echo anchor('dashboard/index' ,'<i class="fa fa-dashboard
               
               " style="color: black;"></i><span>'.$this->lang->line('menu_dashboard').'</span>',array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Dashboard'));
               
               ?> 
         </li>
         <?php
            if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant" || $usertype == "Support") {
                $adminpackage=$this->session->userdata('adminpackage');

                        if($adminpackage == 1) { 
                            echo '<li class="">';
            
                    echo anchor('student/index', '<i class="fa fa-user"></i><span>'.'&nbsp;'.$this->lang->line('menu_student').'</span>',array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Student'));
                   echo'<b class="badge bg-success menuposition"><a data-toggle="tooltip" title="upgrade now" href="'.base_url().'upgradeplan">Limited</a></b>';
            
                echo '</li>';
            }
            if($adminpackage == 2) { 

                echo '<li class="">';
            
                    echo anchor('student/index', '<i class="fa fa-user"></i><span>'.'&nbsp;'.$this->lang->line('menu_student').'</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Student'));
            
                echo '</li>';
            
            }
        }
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "HOD") {
            
                echo '<li class="">';
            
                    echo anchor('professor/index', '<i class="ti ti-id-badge"></i><span>Professor</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Professor'));
            
                echo '</li>';
                  // data-step="2" data-intro="Click on <b>Department.</b> Add & create as
// many departments.as you require. Eg. 
// Management Department, Commerce
// Department, IT Department, etc." data-position="right"
            
                echo '<li class="">';
            
                    echo anchor('department/index', '<i class="ti ti-id-badge"></i><span>Department</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Department'));
            
                echo '</li>';
            
            }
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Teacher") {
            
    
                echo '<li class="">';
            
            
            
                    echo anchor('classes/index', '<i class="ti ti-book"></i><span>'.$this->lang->line('menu_classes').'</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Course'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Teacher") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('subject/index', '<i class="fa fa-book"></i><span>'.$this->lang->line('menu_subject').'</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
            
            
                echo '</li>';
            
            $adminpackage=$this->session->userdata('adminpackage');

                if($adminpackage == 1) {
                echo '<li>';
                    echo anchor('timetable/index', '<i class="fa fa-table"></i><span>Time Table</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
                echo '</li>';
            }
            
            if($adminpackage == 2) {
                echo '<li>';
                    echo anchor('timetable/index', '<i class="fa fa-table"></i><span>Time Table</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
                echo '</li>';

            }
            }
            
            
            
            ?>
         <?php 
            if($usertype == "Student") {
                echo '<li>';
            
            
            
                    echo anchor('subject/index', '<i class="fa fa-book"></i><span>My '.$this->lang->line('menu_subject').'s</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));

                echo '</li>';
                echo '<li>';
                    echo anchor('subject/attendance', '<i class="fa fa-list"></i><span>Attendance</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Attendance'));
                echo '</li>';
            
                 echo '<li>';
        
                    echo anchor('timetable/index', '<i class="fa fa-table"></i><span>Time Table</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
            
            
                echo '</li>';
            }    
            ?>
         <?php
            if($usertype == "Student") {
                // echo '<li>';
                //     echo anchor('lms', '<i class="fa fa-edit"></i><span>LMS</span>',array('data-toggle'=>'tooltip','title'=>'LMS'));
                // echo '</li>';
            
                echo '<li>';
                    echo anchor('lms/lmsLatest', '<i class="ti ti-blackboard"></i><span>LMS</span>',array('data-toggle'=>'tooltip','title'=>'LMS'));
                echo '</li>';
            }
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {
            ?>
         <li>
            <a data-toggle='tooltip' title='LMS' href="javascript:void(0)"><i class="ti ti-blackboard"></i><span>LMS</span><span class="fa arrow"></span></a>
            <b class="badge bg-success menuposition"><a data-toggle='tooltip' title='upgrade now' href="<?php echo base_url() ?>upgradeplan">Limited</a></b>
            <ul class="nav nav-second-level">
               <!-- <li>
                  <a data-toggle='tooltip' title='Add PDF' href="<?php echo base_url() ?>lms" style="margin-left: 0px;"><i class="fa fa-file-pdf-o"></i>Add PDF</a>
               </li> -->
               <li>
                  <a data-toggle='tooltip' title='Online Reader' href="<?php echo base_url() ?>online_reader" style="margin-left: 0px;"><i class="fa fa-laptop"></i>Online Reader</a>
               </li>
               <li>
                  <a data-toggle='tooltip' title='Videos' href="<?php echo base_url() ?>videos" style="margin-left: 0px;"><i class="fa fa-video-camera" aria-hidden="true"></i>Videos</a>
               </li>
            </ul>
         </li>
         <?php } ?>
         <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") { ?>
            <?php $adminpackage=$this->session->userdata('adminpackage');

if($adminpackage == 1) {  ?>
         <li>

            <a  data-toggle='tooltip' title='Exam' href="javascript:void(0)"><i class="ti ti-ruler-pencil"></i><span><?=$this->lang->line('menu_exam');?></span><span class="fa arrow"></span></a>
                <b class="badge bg-success menuposition"><a data-toggle='tooltip' title='upgrade now' href="<?php echo base_url() ?>upgradeplan">Limited</a></b>
            <ul class="nav nav-second-level">
               <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") { ?>
               <!-- admin -->
               <li>
                  <?php echo anchor('exam/index', '<i class="fa fa-pencil"></i>'.$this->lang->line('menu_exam')); ?>
               </li>
               <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") { ?>
               <li>
                  <?php echo anchor('exam/quizSetting', '<i class="fa fa-cogs"></i>Exam Setting',array('data-toggle'=>'tooltip','title'=>'Exam Setting')); ?>
               </li>
               <li>
                  <?php echo anchor('etsexam/liveVideos', '<i class="fa fa-video-camera"></i>Live Video',array('data-toggle'=>'tooltip','title'=>'Live Video','target'=>'_blank')); ?>
               </li>
               <?php }?>
               <?php  } ?> 
            </ul>
         </li>
     <?php } ?>
         <?php $adminpackage=$this->session->userdata('adminpackage');

if($adminpackage == 2) {  ?>
            <li>
            
            <a  data-toggle='tooltip' title='Exam' href="javascript:void(0)"><i class="ti ti-ruler-pencil"></i><span><?=$this->lang->line('menu_exam');?></span><span class="fa arrow"></span></a>

            <ul class="nav nav-second-level">
               <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") { ?>
               <!-- admin -->
               <li>
                  <?php echo anchor('exam/index', '<i class="fa fa-pencil"></i>'.$this->lang->line('menu_exam')); ?>
               </li>
               <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") { ?>
               <li>
                  <?php echo anchor('exam/quizSetting', '<i class="fa fa-cogs"></i>Exam Setting',array('data-toggle'=>'tooltip','title'=>'Exam Setting')); ?>
               </li>
               <li>
                  <?php echo anchor('etsexam/liveVideos', '<i class="fa fa-video-camera"></i>Live Video',array('data-toggle'=>'tooltip','title'=>'Live Video','target'=>'_blank')); ?>
               </li>
               <?php }?>
               <?php  } ?> 
            </ul>
         </li>
         <?php } ?>
         <?php } ?>
         <!-- <?php
            if($usertype == "Student") {                                
            
                echo '<li>';
            
                 echo anchor('c/m/1', '<i class="fa fa-pencil"></i><span>Exam</span>',array('data-toggle'=>'tooltip','title'=>'Exam'));
            
                echo '</li>';                                    
            
            }
            
            ?> -->
         <?php
            if($usertype == "Student") {
            
                
            
                echo '<li>';
            
            
            
                 echo anchor('c/m/1', '<i class="fa fa-tasks"></i><span>Exam</span>',array('data-toggle'=>'tooltip','title'=>'Exam')); 
            
            
            
                echo '</li>';
            
                
            
            }
            
            
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('mark/index', '<i class="fa fa-check-square-o"></i><span>'.$this->lang->line('menu_mark').'</span>' ,array('data-toggle'=>'tooltip','title'=>'Result'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
  
         <?php if($usertype == 'ClgAdmin' ||  $usertype == "Accountant" || $usertype == "Support" || $usertype == "Teacher") { ?>
         <li>
            <a data-toggle='tooltip' title='Account' href="<?=base_url()?>invoice/index"><i class="fa fa-rupee"></i> <span><?=$this->lang->line('menu_account');?></span></a>
            <!--  <ul class="nav nav-second-level">
               <li>
               
                   <?php echo anchor('invoice/index', '<i class="fa fa-rupee"></i>Fee' ,array('data-toggle'=>'tooltip','title'=>'Fee','class'=>'rupbakwaas')); ?>
               
               </li>
               
               
               </ul> -->
         </li>
         <?php } ?>
         <?php
            if($usertype == "Student") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('invoice/index', '<i class="fa fa-rupee"></i><span>&nbsp;&nbsp;Fee</span>',array('class'=>'rupbakwaas','data-toggle'=>'tooltip','title'=>'invoice'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Student" || $usertype == "Support" || $usertype == "Teacher" || $usertype == 'Accountant') {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('notice/index', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>'.$this->lang->line('menu_notice').'</span>',array('data-toggle'=>'tooltip','title'=>'Notice'));
            
            
            
                echo '</li>';
            }
            ?>
         <?php
            if( $usertype == "" || $usertype == "" ) 
               {  
                   echo '<li>';
                                   echo anchor('notice/add', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>'.$this->lang->line('menu_notice').'</span>',array('data-toggle'=>'tooltip','title'=>'Notice'));
            
            
            
                               echo '</li>';
                           }
            ?>
         <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") { ?>
         <!--      <li>
            <a href="javascript:void(0)"><i class="ti ti-ruler-pencil"></i><?=$this->lang->line('menu_mailandsms');?><span class="fa arrow"></span></a>
            
            <ul class="nav nav-second-level">
            
               
            
                <li>
            
            
            
                    <?php echo anchor('mailandsmstemplate/index', '<i class="fa icon-template"></i><span>'.$this->lang->line('menu_mailandsmstemplate').'</span>'); ?>
            
            
            
                </li>
            
            
            
                <li>
            
            
            
                    <?php echo anchor('mailandsms/index', '<i class="fa icon-mailandsms"></i><span>'.$this->lang->line('menu_mailandsms').'</span>'); ?>
            
            
            
                </li>
            
            
            
                <li>
            
            
            
                    <?php echo anchor('smssettings/index', '<i class="fa fa-wrench"></i><span>'.$this->lang->line('menu_smssettings').'</span>'); ?>
            
            
            
                </li>
            
            </ul>
            
            </li> -->
         <?php } ?>
         <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") { ?>
         <!--           <li>
            <a href="javascript:void(0)"><i class="ti ti-file"></i>Data Management <span class="fa arrow"></span></a>
            
            <ul class="nav nav-second-level">
            
                                                    <li>
            
                    <?php echo anchor('databaseManagement/Courses', '<i class="fa icon-template"></i><span>Copy Course</span>'); ?>
            
                </li>
            
                <li>
            
                    <?php echo anchor('databaseManagement/CourseView', '<i class="fa icon-template"></i><span>Course View</span>'); ?>
            
                </li>
            
            </ul>
            
            </li> -->
         <?php } ?>
         <?php
            if($this->session->userdata("usertype") == "Admin" || $this->session->userdata("usertype") == "superadmin" || $this->session->userdata("usertype") == "ClgAdmin" || $usertype == "Support") {
                $adminpackage=$this->session->userdata('adminpackage');
                if($adminpackage == 1) { 
            
                echo '<li>';
            
                    echo anchor('bulkimport/index', '<i class="fa fa-upload"></i><span>'.$this->lang->line('menu_import').'</span>',array('data-toggle'=>'tooltip','title'=>'Import'));
                    echo'<b class="badge bg-success menuposition"><a data-toggle="tooltip" title="upgrade now" href="'.base_url().'upgradeplan">Limited</a></b>';
            
                echo '</li>';
            }
            if($adminpackage == 2) { 

                echo '<li>';
            
                    echo anchor('bulkimport/index', '<i class="fa fa-upload"></i><span>'.$this->lang->line('menu_import').'</span>' ,array('data-toggle'=>'tooltip','title'=>'Import'));
            
                echo '</li>';
            
            }
                    }
            
            
            ?>
         <?php
            if($usertype == "Professor") {
            
                echo '<li>';
                    echo anchor('viewlecture/index', '<i class="fa fa-pencil-square-o"></i><span>Lecture</span>' ,array('data-toggle'=>'tooltip','title'=>'Lecture'));
                echo '</li>';
            
            
                echo '<li>';
                    echo anchor('viewlecture/attendance?type=filter', '<i class="fa fa-list"></i><span>Mark Attendance</span>' ,array('data-toggle'=>'tooltip','title'=>'Mark Attendance'));
                echo '</li>';
            
                echo '<li>';
                    echo anchor('viewlecture/subjectprogress', '<i class="fa fa-book"></i><span>Subject Progress</span>' ,array('data-toggle'=>'tooltip','title'=>'Subject Progress'));
                echo '</li>';
                echo '<li>';
                    echo anchor('viewlecture/viewattendance', '<i class="fa fa-eye"></i><span>View Attendance</span>' ,array('data-toggle'=>'tooltip','title'=>'View Attendance'));
                echo '</li>';
            
             
          
            
                echo '<li>';
            
            
            
                    echo anchor('notice/index', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>'.$this->lang->line('menu_notice').'</span>',array('data-toggle'=>'tooltip','title'=>'Notice'));
            
            
            
                echo '</li>';
        

               
            
            }
            
            
            
            ?>
         <?php
            if($usertype == 'ClgAdmin' || $usertype == "superadmin") {
                $adminpackage=$this->session->userdata('adminpackage');

                        if($adminpackage == 1) { 
            
            
            
                echo '<li>';
            
            
            
                echo anchor('user/index', '<i class="fa fa-users"></i><span>'.$this->lang->line('menu_user').'</span>',array('data-toggle'=>'tooltip','title'=>'User'));
                echo'<b class="badge bg-success menuposition"><a data-toggle="tooltip" title="upgrade now" href="'.base_url().'upgradeplan">Limited</a></b>';
            
            
            
                echo '</li>';
            }

            if($adminpackage == 2) { 


                echo '<li>';
            
            
            
                echo anchor('user/index', '<i class="fa fa-users"></i><span>'.$this->lang->line('menu_user').'</span>' ,array('data-toggle'=>'tooltip','title'=>'User'));
            
            
            
                echo '</li>';
            
            }
            
            }
            
            
            
            ?>
         <?php
            if($usertype == 'ClgAdmin') {
            
            
            
                echo '<li>';
            
            
            
                 echo anchor('setting/index', '<i class="fa fa-cog" aria-hidden="true"></i><span>Setting</span>' ,array('data-toggle'=>'tooltip','title'=>'Setting'));
            
                
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
                     <?php
            if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == 'Professor' || $usertype == 'Accountant') {
            
            
            
                echo '<li>';
            
            
            
                 echo anchor('customemail/index', '<i class="fa fa-cog" aria-hidden="true"></i><span>Custom Email</span>' ,array('data-toggle'=>'tooltip','title'=>'Custom Email'));
            
                
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>

            <?php
            if($usertype == 'ClgAdmin') {
            
            
            
                echo '<li>';
            
            
            
                 echo anchor('billing/index', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>Billing</span><span class="fa arrow"></span>' ,array('data-toggle'=>'tooltip','title'=>'Billing'));

                 echo '<ul class="nav nav-second-level">
              
               <li>';
                echo anchor("billing/index", "<i class='fa fa-pencil'></i> Plans & Billing",array("data-toggle"=>"tooltip","title"=>"Plans & Billing") ); 
              echo '</li>
               <li>';
                  echo anchor("billing/invoice", "<i class='fa fa-cogs'></i>Invoice",array("data-toggle"=>"tooltip","title"=>"Invoice")); 
               echo '</li>
               
            </ul>';
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
      </ul>
   </div>
   <!-- /.sidebar-collapse -->
</div>
<!-- <script>
   $(document).ready(function(){
   
     $('[data-toggle="tooltip"]').tooltip();   
   });
</script> -->
<!-- <script>
   $('.navbar-nav a').on('click', function () {
   
           if (window.innerWidth <= 768) {
   
               $(".navbar-toggle").click();
   
           }
   
       });
   
   </script> -->