<style type="text/css">
   .card.simple-card .cardheader {
   background-size: cover!important;
   position: relative;
   height: 135px;
   }
   .left{
   text-align: left;
   margin-left: 30px;
   }
   h1 { 
   text-align: center; 
   } 
   .b_center{
   text-align: center;
   }
</style>
<?php $adminpackage=$this->session->userdata('adminpackage');
if($adminpackage == 1) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
     To unlock premium modules <a href="<?php echo base_url('upgradeplan') ?>" class="alert-link">upgrade now</a>
   </div>
  
<?php }  ?>
   <div class="row page-titles dashboardtitle-background">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-dashboard"></i> Dashboard</h3>
   </div>
   <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
         <li class="breadcrumb-item active">Dashboard</li>
      </ol>
   </div>
</div>
   


<div class="container-fluid">
   <?php
      $usertype = $this->session->userdata("usertype");
      if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "HOD") {
      if($usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support") {
      ?>
   <div class="row">
      <div class="col-md-3 col-sm-6">
         <!-- <a href="<?php echo base_url() ?>user/index"> -->
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption success">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-users"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail user-back" id="mydivuser">
                           <h3 class="cl-success"><?php echo $Totaluser+count($student)+count($professors_count) ?></h3>
                           <span>Total Users</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-success">
                           <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         <!-- </a> -->
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa fa-user"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back" id="mydivstu">
                           <h3 class="cl-info"> <?=count($student)?></h3>
                           <span>Total Students</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('professor')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption danger">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-id-badge"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail teacher-back" id="mydivtea">
                           <h3 class="cl-danger"> <?=count($professors_count)?></h3>
                           <span>Total Professors</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back" id="mydivco">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      
   </div>
   <?php } ?>
   <?php if($usertype == "Admin") { ?>
   <div class="row">
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa fa-user"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back" id="mydivstu">
                           <h3 class="cl-info"> <?=count($student)?></h3>
                           <span>Total Students</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('professor')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption danger">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-id-badge"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail teacher-back" id="mydivtea">
                           <h3 class="cl-danger"> <?=count($professors_count)?></h3>
                           <span>Total Professors</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back" id="mydivco">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <?php } ?>
   <!-- /row -->
   <!-- second -->
   <?php if($usertype == "superadmin" || $usertype == "ClgAdmin") { ?>
   <div class="row">
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption" >
                  <div class="col-xs-4 no-pad zoom" >
                     <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <script type="text/javascript">
                           $.ajax({
                           
                           type: 'GET',
                           
                           dataType: "json",
                           
                           url: "<?=base_url('dashboard/paymentscall')?>",
                           
                           dataType: "html",
                           
                           success: function(data) {
                           
                           var response = jQuery.parseJSON(data);
                           
                           $('#fpaid').html(response.fpaid);
                           
                           $('#npaid').html(response.npaid);
                           
                           $('#ppaid').html(response.ppaid);
                           
                           }
                           
                           });
                           
                        </script> 
                        <span>
                        </span>
                        <h3 style="color: #9c1855;"><i class="fa fa-inr" aria-hidden="true"></i> <?php
                           echo number_format($totalPayment->TotalAmount);
                           ?></h3>
                        <span>Total Revenue</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%; background: #9c1855;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption primary">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-money" aria-hidden="true" style="background: #007bff;color: #ffffff"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail paymentpaid-back">
                        <span></span>
                        <h3 style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($totalPayment->Totalpaidamount); ?></h3>
                        <span>Total Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%; background: #007bff;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption success">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-warning" style="background: #434348;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail totalPayment-back">
                        <span></span>
                        <h3 style="color: #434348"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($totalPayment->TotalAmount-$totalPayment->Totalpaidamount); ?></h3>
                        <span>Due Amount</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%; background: #434348" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <!-- /row -->
   <!--Ritiks's Code-->
   <?php if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "HOD") { ?>
   <?php if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin") { ?>
   <div class="row page-titles shortcut-menu">
      <h3>
      Shortcut Menu <i class="fa fa-share-square-o font-icon-shortcut"></i></h3 style="box-shadow: 10px;">
   </div>
   <div class="row">
         <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("classes/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail details-menu addpdf-back">
                           <span><i class="fa fa-plus"></i> Add Course</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("subject/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail details-menu adduser-back">
                           <span><i class="fa fa-plus"></i> Add Subject</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
         </a>
      </div>
       <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("student/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail addnotice-back">
                           <span><i class="fa fa-plus"></i> Add Student</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("exam/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad" >
                        <div class="widget-detail details-menu addcourse-back">
                           <span><i class="fa fa-plus"></i> Add Exam</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("professor/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail addnotice-back">
                           <span><i class="fa fa-plus"></i> Add Professor </span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
          <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("notice/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail addnotice-back">
                           <span><i class="fa fa-plus"></i> Add Notice</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <hr>
</div>
<div class="col-md-6 col-sm-12 col-xs-12">
   <div class="card simple-card" style="height: 430px;">
      <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
      </div>
      <div class="avatar">
         <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
      </div>
      <div class="info1">
         <div class="title">
            <h3><?php
               $name = $this->session->userdata('name');
               
               if(strlen($name) > 11) 
               {
                 echo substr($name, 0,20); 
               } else {
                 echo $name;
               }
               
               ?></h3>
         </div>
      </div>
      <div class="bottom admin_dash">
         <div class="col-md-4 col-sm-6 col-xs-6">
            <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
         </div>
         <div class="col-md-8 col-sm-6 col-xs-6">
            <p class="left"><?php
               $name = $this->session->userdata('name');
               
               {
                 echo $name;
               }
               
               ?></p>
         </div>
      </div>
      <div class="bottom">
         <div class="col-md-4 col-sm-6 col-xs-6">
            <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
         </div>
         <div class="col-md-8 col-sm-6 col-xs-6">
            <p class="left"><?php
               $email = $admindetails->email;
                echo $email; if($admindetails->systemadminactive == 0) { ?>
                  <span>
                     <button class="verifyemailw" onclick="verifyemailbyadmin(<?php echo $admindetails->adminID?>)" style="color: red; background: #fff;"><i class="fa fa-warning" title="verify now"></i></button>
                  </span>
                  <span class="verify" id="verify"></span>
               <?php }
               else{ ?>
                  <span>
                     <button class="verifyemails" style="color: green;background: #fff;"><i class="fa fa-check" title="verified"></i></button>
                  </span>

              <?php } ?></p>
         </div>
         <!-- <div class="col-md-3 col-sm-6 col-xs-6">
            <a href="#" class="left">verify now</a>
         </div> -->
      </div>
      <div class="bottom">
         <div class="col-md-4 col-sm-6 col-xs-6">
            <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
         </div>
         <div class="col-md-8 col-sm-6 col-xs-6">
            <p class="left"> <?php echo $admindetails->phone; if($admindetails->systemadminextra1 == 0) { ?>
               &nbsp;&nbsp;&nbsp;&nbsp;
               <span>
                  <a href="#" style="color: red;"data-toggle="modal" data-target="#verifymob"><i class="fa fa-warning" title="verify now"></i></a>
               </span>
          <?php  }
          else{ ?>
            <span>
                  <a href="#" style="color: green;"><i class="fa fa-warning" title="verify now"></i></a>
               </span>
          <?php } ?></p>
         </div>
        <!--  <div class="col-md-3 col-sm-6 col-xs-6">
            <a href="#" class="left">verify now</a>
         </div> -->
      </div>
      <div class="bottom">
         <div class="col-md-4 col-sm-6 col-xs-6">
            <p class="left"><i class="fa fa-tasks text-maroon-light"></i>&nbsp;Plan</p>
         </div>
         <div class="col-md-8 col-sm-6 col-xs-6">
            <p class="left"><?php $package=$admindetails->adminpackage; if($package == 1){ echo "Basic"; } if($package == 2){ echo "Essential";} if($package == 3){ echo "Pro";} ?></p>
         </div>
         <!-- <div class="col-md-4 col-sm-6 col-xs-6">
            <a href="<?php echo base_url() ?>upgradeplan" class="left">Upgrade now</a>
         </div> -->
      </div>
      <div class="bottom">
         <?php $package=$admindetails->adminpackage;
          if($package == 1)
            { ?>
               <div class="col-md-12 col-sm-6 col-xs-6">
                  <p class="unlock">To unlock premium modules <span><a class="blinking" href="<?php echo base_url() ?>upgradeplan" title="">Upgrade now</a></span></p>
               </div>
           <?php } ?>
         
      </div>
   </div>
</div>
<!-- user profile demo end -->
<div class="container-fluid">
   <div class="row">
      <div class="col-xs-6">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="container"></div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<?php if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "HOD") { ?>
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="table-wrapper">
            <div class="table-title">
               <div class="row">
                  <div class="col-md-5 col-sm-5 col-xs-5">
                     <h4 style="margin-bottom: 10px;color:black;text-align: center;">All Professor Progress</h4>
                     <div id="chart_divbyadmin"></div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-7">
                     <div class="row">
                        <div class="col-md-9 col-sm-9 col-xs-9">
                           <h4 style="margin-bottom: 10px;color:black;text-align: center;">Month Wise Attendance Data</h4>
                        </div>
                        <?php $date=date('m');?>
                        <div class="col-md-3">
                           <select name="year" class="form-control" id="year">
                              <option value="0" <?php if($date==0){ echo "selected";}?>>Select Year</option>
                              <option value="1" <?php if($date==01){ echo "selected";}?>>January</option>
                              <option value="2" <?php if($date==02){ echo "selected";}?>>February</option>
                              <option value="3" <?php if($date==03){ echo "selected";}?>>March</option>
                              <option value="4" <?php if($date==04){ echo "selected";}?>>April</option>
                              <option value="5" <?php if($date==05){ echo "selected";}?>>May</option>
                              <option value="6" <?php if($date==06){ echo "selected";}?>>June</option>
                              <option value="7" <?php if($date==07){ echo "selected";}?>>July</option>
                              <option value="8" <?php if($date==08){ echo "selected";}?>>August</option>
                              <option value="9" <?php if($date==09){ echo "selected";}?>>September</option>
                              <option value="10" <?php if($date==10){ echo "selected";}?>>October</option>
                              <option value="11" <?php if($date==11){ echo "selected";}?>>November</option>
                              <option value="12" <?php if($date==12){ echo "selected";}?>>December</option>
                           </select>
                        </div>
                     </div>
                     <div id="chart_area" style="width: 550px; height: 380px;"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<!-- All Professor Progress start -->
<!-- <script type="text/javascript"> 
   google.charts.load('current', {'packages':['corechart']}); 
   
   google.charts.setOnLoadCallback(drawChart); 
      
   function drawChart() { 
     var jsonData = $.ajax({ 
         url: "<?php echo base_url() . 'dashboard/pie_chart_js_byadmin' ?>", 
         dataType: "json", 
         async: false 
         }).responseText; 
          
     console.log(jsonData);
     var data = new google.visualization.DataTable(jsonData); 
     var options = {
                       title:"All Professor Progress",
                       is3D: true,
                       width: 500,
                       height: 410,
                       tooltip: { text:'value',isHtml: true,trigger:'selection'},
                    
                       legend: {position: 'right'},
                       slices: {
                                   0: { color: 'Red' },
                                   1: { color: 'FFAA1D' },
                                   2: { color: 'darkGreen' }
                               }
   
                    };
   
     var chart = new google.visualization.PieChart(document.getElementById('chart_divbyadmin')); 
     chart.draw(data,options); 
   } 
   
</script> -->
<!-- All Professor Progress end -->
<!-- Course wise Student Attendance Start -->
<!-- <script type="text/javascript">
   google.charts.load('current', {packages: ['corechart', 'bar']});
   google.charts.setOnLoadCallback();
   
   function load_monthwise_data(year, title)
   {
       var month=year;
       if(month==1)
       {
         month="January";
       }
       else if(month==2)
       {
         month="February";
       }
       else if(month==3)
       {
         month="March";
       }
       else if(month==4)
       {
         month="April";
       }
       else if(month==5)
       {
         month="May";
       }
       else if(month==6)
       {
         month="June";
       }
       else if(month==7)
       {
         month="July";
       }
       else if(month==8)
       {
         month="August";
       }
       else if(month==9)
       {
         month="September";
       }
       else if(month==10)
       {
         month="October";
       }
       else if(month==11)
       {
         month="November";
       }
       else
       {
         month="December";
       }
       
       var temp_title = title + ' '+month+'';
       $.ajax({
           url:"<?php echo base_url() . 'dashboard/bar_chart_all_student_attendance'?>",
           method:"POST",
           data:{year:year},
           dataType:"JSON",
           success:function(data)
           {
              
               drawMonthwiseChart(data, temp_title);
     
           }
       });
   }
   
   function drawMonthwiseChart(chart_data, chart_main_title)
   {
       var jsonData = chart_data;
       var data = new google.visualization.DataTable();
       data.addColumn('string', 'Course');
       data.addColumn('number', 'Attendance');
       $.each(jsonData, function(i, jsonData){
           var month = jsonData.month;
           var profit = parseFloat($.trim(jsonData.profit));
           data.addRows([[month, profit]]);
       });
       var options = {
           title:chart_main_title,
           hAxis: {
               title: "Course"
           },
           vAxis: {
               title: 'Attendance'
           }
       };
   
       var chart = new google.visualization.ColumnChart(document.getElementById('chart_area'));
       chart.draw(data, options);
   }
   
</script> -->
<!-- <script>
   $(document).ready(function(){
   
       $('#year').change(function(){
           var year = $(this).val();
           if(year != '')
           {
               load_monthwise_data(year, 'Month Wise Attendance Data For');
           }
       });
   
        var year = $('#year').val();
           if(year != '')
           {
               load_monthwise_data(year, 'Month Wise Attendance Data For');
           }
   
   });
   
</script> -->
<?php } ?>

<?php
   $usertype = $this->session->userdata("usertype");
   if($usertype == "Accountant") {
   ?>
<!-- user profile demo end -->
<div class="col-md-6 col-sm-6 col-xs-12">
   <div class="card simple-card" style="height: 362px;">
      <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
      </div>
      <div class="avatar">
         <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
      </div>
      <div class="info1">
         <div class="title">
            <h3><?php
               $name = $this->session->userdata('name');
               
               if(strlen($name) > 11) 
               {
                 echo substr($name, 0,11). ".."; 
               } else {
                 echo $name;
               }
               
               ?></h3>
         </div>
      </div>
      <div class="bottom admin_dash">
         <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="left"><?php
               $name = $this->session->userdata('name');
               
               if(strlen($name) > 11) 
               {
                 echo substr($name, 0,11). ".."; 
               } else {
                 echo $name;
               }
               
               ?></p>
         </div>
      </div>
      <div class="bottom">
         <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="left"><?php
               $email = $this->session->userdata('email');
               echo $email;
               ?></p>
         </div>
      </div>
      <div class="bottom">
         <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-6">
            <p class="left"><?=$user->phone?></p>
         </div>
      </div>
   </div>
</div>
<!-- user profile demo end --> 
<!-- accountant notice start --> 
<div class="container-fluid">
   <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
         <div class="table-wrapper" style="height: 360px;">
            <div class="table-title theme-bg">
               <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                     <h2 style="margin-bottom: 10px;">Notice</h2>
                  </div>
               </div>
            </div>
            <div class="table-responsive">
               <table class="table table-striped col-md-12">
                  <thead>
                     <tr>
                        <td style="font-weight: 600;">S.No.</td>
                        <td style="font-weight: 600;">Title.</td>
                        <td style="font-weight: 600;">Date</td>
                        <td style="font-weight: 600;">Action</td>
                     </tr>
                  </thead>
                  <tbody>
                     <?php  {$i = 1; 
                        foreach( $notices  as  $notices) { ?>
                     <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo substr($notices['title'], 0,20); ?></td>
                        <td><?php echo date('d-m-Y', strtotime($notices['date'])); ?></td>
                        <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                     </tr>
                     <?php $i++; }} ?>
                  </tbody>
               </table>
            </div>
            <div class="box-main-footer clearfix">
               <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- accountant notice end -->
<?php }?>
<script type="text/javascript">
   $.ajax({
   
      type: 'GET',
   
      dataType: "json",
   
      url: "<?=base_url('dashboard/graphcall')?>",
   
      dataType: "html",
   
      success: function(data) {
   
   var response = jQuery.parseJSON(data);
   
   Highcharts.chart('container', {
   chart: {
   type: 'cylinder',
   options3d: {
   enabled: true,
   alpha: 15,
   beta: 15,
   depth: 50,
   viewDistance: 25
   }
   },
   title: {
   text: 'Earning Graph'
   },
   plotOptions: {
   series: {
   depth: 25,
   colorByPoint: true
   }
   },
   
   xAxis: {
   
    categories: ['2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026']
   
   },
   
   
   series: [{
   data: response.balance,
   name: 'Payment',
   showInLegend: false
   }]
   });
   
   
   }
   
   }); 
   
   $('#plain').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Plain'
   
    }
   
   });
   
   });
   
   
   
   $('#inverted').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: true,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Inverted'
   
    }
   
   });
   
   });
   
   
   
   $('#polar').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: true
   
    },
   
    subtitle: {
   
      text: 'Polar'
   
    }
   
   });
   
   });
   
</script>  
<script type="text/javascript">
   $.ajax({
   
       type: 'GET',
   
       dataType: "json",
   
   
   
       url: "<?=base_url('dashboard/paymentscall')?>",
   
   
   
       dataType: "html",
   
   
   
       success: function(data) {
   
   
   
         var response = jQuery.parseJSON(data);
   
   
   
   
   
         npaid = response.npaid;
   
   
   
         ppaid = response.ppaid;
   
   
   
         fpaid = response.fpaid;
   
   
   
         cash = response.cash;
   
   
   
         cheque = response.cheque;
   
   
   
         paypal = response.paypal;
   
   
   
         stripe = response.stripe;
   
   
   
         PayuMoney = response.PayuMoney;
   
   
   
         OnlinePayment = response.OnlinePayment;
   
   
   
         Bank = response.Bank;
   
   
   Highcharts.chart('pichartPaymentStatus', {
   
   chart: {
   
   plotBackgroundColor: null,
   
   plotBorderWidth: null,
   
   plotShadow: false,
   
   type: 'pie'
   
   },
   
   title: {
   
   text: 'Payment Status Graph'
   
   },
   
   tooltip: {
   
   pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
   pie: {
   
     allowPointSelect: true,
   
     cursor: 'pointer',
   
     dataLabels: {
   
       enabled: true,
   
       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
       style: {
   
         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
       },
   
       connectorColor: 'silver'
   
     }
   
   }
   
   },
   
   series: [{
   
   name: 'Payment',
   
   data: [
   
     { name: 'Partial Paid', y:ppaid},
   
     { name: 'Not paid', y:npaid },
   
     { name: 'Full paid', y:fpaid }
   
   ]
   
   }]
   
   });   
   Highcharts.chart('piecharthichart', {
   chart: {
   type: 'pie',
   options3d: {
   enabled: true,
   alpha: 45,
   beta: 0
   }
   },
   title: {
   text: 'Payment Type Graph'
   },
   tooltip: {
   pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   },
   plotOptions: {
   pie: {
   allowPointSelect: true,
   cursor: 'pointer',
   depth: 35,
   dataLabels: {
     enabled: true,
     format: '{point.name}'
   }
   }
   },
   series: [{
   type: 'pie',
   name: 'Payment',
   data: [{
   
     name: 'Cash',
   
     y: cash,
   
     sliced: true,
   
     selected: true
   
   },{
   
     name: 'Cheque',
   
     y: cheque
   
   }, {
   
     name: 'paypal',
   
     y: paypal
   
   },{
   
     name: 'PayUmoney',
   
     y: PayuMoney
   
   },
   
   {
   
     name: 'Bank',
   
     y: Bank
   
   },
   
   {
   
     name: 'Online Payment',
   
     y: OnlinePayment
   
   },
   
    {
   
     name: 'stripe',
   
     y: stripe
   
   }]
   }]
   });
   
   
   
   
   
   }
   
   }); 
   
</script>
<?php } ?>
<?php
   $usertype = $this->session->userdata("usertype");
   if($usertype == "Professor") {
   ?>
<div class="row">
</div>
<div class="row">
   <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="card simple-card" style="height: 362px;">
         <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
         </div>
         <div class="avatar">
            <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
         </div>
         <div class="info1">
            <div class="title">
               <h3><?php
                  $name = $this->session->userdata('name');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                  } else {
                    echo $name;
                  }
                  
                  ?></h3>
            </div>
         </div>
         <div class="bottom admin_dash">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $name = $this->session->userdata('name');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                  } else {
                    echo $name;
                  }
                  
                  ?></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><span><?=$this->lang->line("profile_email")?></span><?=$professor->email?></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><span><?=$professor->phone?></span></p>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6 col-sm-6 col-xs-12" >
            <div class="table-wrapper" style="height: 360px;">
               <div class="table-title theme-bg">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2 style="margin-bottom: 10px;">Notice</h2>
                     </div>
                  </div>
               </div>
               <div class="table-responsive">
                  <table class="table table-striped col-md-12">
                     <thead>
                        <tr>
                           <td style="font-weight: 600;">S.No.</td>
                           <td style="font-weight: 600;">Title</td>
                           <td style="font-weight: 600;">Date</td>
                           <td style="font-weight: 600;">Action</td>
                        </tr>
                     </thead>
                     <tbody>
                        <?php  {$i = 1; 
                           foreach( $notices  as  $notices) { ?>
                        <tr>
                           <td><?php echo $i; ?></td>
                           <td><?php echo substr($notices['title'], 0,20);?></td>
                           <td><?php echo date('d-m-Y', strtotime($notices['date'])); ?></td>
                           <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                        </tr>
                        <?php $i++; }} ?>
                     </tbody>
                  </table>
               </div>
               <div class="box-main-footer clearfix">
                  <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div id="chart_div" style="width:100%;height: 400px"></div>
         </div>
      </div>
   </div>
   <script type="text/javascript"> 
      google.charts.load('current', {'packages':['corechart']}); 
         
      google.charts.setOnLoadCallback(drawChart); 
         
      function drawChart() { 
        var jsonData = $.ajax({ 
            url: "<?php echo base_url() . 'dashboard/pie_chart_js' ?>", 
            dataType: "json", 
            async: false 
            }).responseText; 
             
        console.log(jsonData);
        var data = new google.visualization.DataTable(jsonData); 
        var options = {
                          title:"All Subject Progress",
                          is3D: true,
                          tooltip: { text: 'value',isHtml: true,trigger:'selection' },
                          slices: {
                                      0: { color: 'Red' },
                                      1: { color: 'Yellow' },
                                      2: { color: 'darkGreen' }
                                  }
      
                       };
      
        var chart = new google.visualization.PieChart(document.getElementById('chart_div')); 
        chart.draw(data,options); 
      } 
      
   </script>
   <!-- All Attendance Progress end -->
   <div class="row">
      <div class="col-sm-4">
         <div class="box-main1 border-yellow">
            <div class="box-main-header">
               <h3>Calender</h3>
            </div>
            <div class="box-main-body">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div id="mycalendars"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="box-main box_rau border-brightyellow">
            <div class="box-main-header">
               <h3>Thought of the Day</h3>
            </div>
            <div class="box-main-body table-display">
               <div class="daily-thought">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                     <div class="col-md-12">
                        <div class="carousel-inner">
                           <div class="item active">
                              <p>“Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.”
                              </p>
                           </div>
                           <div class="item">
                              If you want to be successful, you need to discover something you are actually passionate about.”
                           </div>
                           <div class="item">
                              “Successful and unsuccessful people do not vary greatly in their abilities. They vary in their desires to reach their potential.”
                           </div>
                           <div class="item">
                              “There are no negatives in life, only challenges to overcome that will make you stronger.”
                           </div>
                           <div class="item">
                              “When you think about it, the secret to success in social media isn't any different than the secret to success in life: help others first.”
                           </div>
                           <div class="item">
                              “You see, success isn’t a list of goals to be checked off one after another. It’s not reaching a destination. Success is a journey.”
                           </div>
                           <div class="item">
                              “When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one which has opened for us.”
                           </div>
                           <div class="item">
                              “Solve one problem, and you keep a hundred others away.”
                           </div>
                           <div class="item">
                              “At the end of the day we are accountable to ourselves - our success is a result of what we do.” 
                           </div>
                        </div>
                     </div>
                  </div>
                  <span>
                  <img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/happiness.png" alt="Good Day" />
                  </span>
                  <div>&nbsp;</div>
                  <div>&nbsp;</div>
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="col-md-9">&nbsp;</div>
                           <div class="col-md-3">
                              <a class="carousel-control" href="#myCarousel" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
                              <span class="sr-only">Previous</span>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="col-md-1">&nbsp;</div>
                           <div class="col-md-2">
                              <a class="carousel-control" href="#myCarousel" role="button" data-slide="next" >
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                              </a>
                           </div>
                           <div class="col-md-9">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="box-main box_rau border-blue">
            <div class="box-main-header">
               <h3>Weather</h3>
            </div>
            <div class="box-main-body">
               <div class="daily-weather">
                  <a class="weatherwidget-io" href="https://forecast7.com/en/30d3278d03/dehradun/" data-label_1="DEHRADUN" data-label_2="WEATHER" data-theme="original" >DEHRADUN WEATHER</a>
                  <script>
                     !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                  </script>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <?php
      $usertype = $this->session->userdata("usertype");
      if($usertype == "Teacher") {
      ?>
   <div class="row">
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-graduation-cap"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back">
                           <h3 class="cl-info"> <?=count($student)?></h3>
                           <span>Total <?=$this->lang->line("menu_student")?></span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?php echo base_url() ?>subject/index">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption success">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-users"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail user-back">
                           <h3 class="cl-success"><?php echo count($subject) ?></h3>
                           <span>Total Subjects</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-success">
                           <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <div class="row">
      <div class="col-md-3 col-sm-6 fpaid">
         <a  id = 'fpaidfilter'>
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-check-circle"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail">
                           <script type="text/javascript">
                              $.ajax({
                              
                              type: 'GET',
                              
                              dataType: "json",
                              
                              url: "<?=base_url('dashboard/paymentscall')?>",
                              
                              dataType: "html",
                              
                              success: function(data) {
                              
                              var response = jQuery.parseJSON(data);
                              
                              $('#fpaid').html(response.fpaid);
                              
                              $('#npaid').html(response.npaid);
                              
                              $('#ppaid').html(response.ppaid);
                              
                              }
                              
                              });
                              
                           </script> 
                           <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment_full->total
                              ?>
                           </span>
                           <h3 class="cl-info" id="fpaid"></h3>
                           <span>Full Paid</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6 fpaid">
         <a id = 'ppaidfilter'>
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption danger">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-pie-chart1"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail payment-back">
                           <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment_part->total ?></span>
                           <h3 class="cl-danger" id="ppaid"></h3>
                           <span>Partial Paid</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6 fpaid">
         <a id = 'npaidfilter'>
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-times-circle" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail paymentpaid-back">
                           <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment_not->total ?></span>
                           <h3 class="cl-warning" id="npaid"></h3>
                           <span>Not Paid</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption success">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-line-chart"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail totalPayment-back">
                        <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $duePayment ?></span>
                        <h3 class="cl-success"><i class="fa fa-inr" aria-hidden="true"></i><?php echo comp_numb($duePayment) ?></h3>
                        <span>Due Amount</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor title-font-size">Earning Graph </h3>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="pichartPaymentStatus" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="piecharthichart" style="height: 400px"></div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="row">
      <div class="col-xs-12" style="margin-top:3%;">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="container"></div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
   <script type="text/javascript">
      $.ajax({
      
         type: 'GET',
      
         dataType: "json",
      
         url: "<?=base_url('dashboard/graphcall')?>",
      
         dataType: "html",
      
         success: function(data) {
      
      var response = jQuery.parseJSON(data);
      
      var chart = Highcharts.chart('container', {
      
      title: {
      
       text: 'Earning Graph'
      
      },
      
      subtitle: {
      
       text: 'Plain'
      
      },
      
      
      xAxis: {
      
       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      
      },
      
      series: [{
      
       type: 'column',
      
       colorByPoint: true,
      
       data: response.balance,
      
       showInLegend: false
      
      }]
      
      });
      
      }
      
      }); 
      
      $('#plain').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: false,
      
         polar: false
      
       },
      
       subtitle: {
      
         text: 'Plain'
      
       }
      
      });
      
      });
      
      
      
      $('#inverted').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: true,
      
         polar: false
      
       },
      
       subtitle: {
      
         text: 'Inverted'
      
       }
      
      });
      
      });
      
      
      
      $('#polar').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: false,
      
         polar: true
      
       },
      
       subtitle: {
      
         text: 'Polar'
      
       }
      
      });
      
      });
      
   </script>  
   <script type="text/javascript">
      $.ajax({
      
      
      
          type: 'GET',
      
      
      
          dataType: "json",
      
      
      
          url: "<?=base_url('dashboard/paymentscall')?>",
      
      
      
          dataType: "html",
      
      
      
          success: function(data) {
      
      
      
            var response = jQuery.parseJSON(data);
      
      
            npaid = response.npaid;
      
      
      
            ppaid = response.ppaid;
      
      
      
            fpaid = response.fpaid;
      
      
      
            cash = response.cash;
      
      
      
            cheque = response.cheque;
      
      
      
            paypal = response.paypal;
      
      
      
            stripe = response.stripe;
      
      
      
            PayuMoney = response.PayuMoney;
      
      
      
            OnlinePayment = response.OnlinePayment;
      
      
      
            Bank = response.Bank;
      
      
      Highcharts.chart('pichartPaymentStatus', {
      
      chart: {
      
      plotBackgroundColor: null,
      
      plotBorderWidth: null,
      
      plotShadow: false,
      
      type: 'pie'
      
      },
      
      title: {
      
      text: 'Payment Status Graph'
      
      },
      
      tooltip: {
      
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      
      },
      
      plotOptions: {
      
      pie: {
      
        allowPointSelect: true,
      
        cursor: 'pointer',
      
        dataLabels: {
      
          enabled: true,
      
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
      
          style: {
      
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
      
          },
      
          connectorColor: 'silver'
      
        }
      
      }
      
      },
      
      series: [{
      
      name: 'Payment',
      
      data: [
      
        { name: 'Partial Paid', y:ppaid},
      
        { name: 'Not paid', y:npaid },
      
        { name: 'Full paid', y:fpaid }
      
      ]
      
      }]
      
      });
      
      
      
      Highcharts.chart('piecharthichart', {
      
      chart: {
      
      plotBackgroundColor: null,
      
      plotBorderWidth: null,
      
      plotShadow: false,
      
      type: 'pie'
      
      },
      
      title: {
      
      text: 'Payment Type Graph'
      
      },
      
      tooltip: {
      
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      
      },
      
      plotOptions: {
      
      pie: {
      
        allowPointSelect: true,
      
        cursor: 'pointer',
      
        dataLabels: {
      
          enabled: true,
      
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
      
          style: {
      
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
      
          }
      
        }
      
      }
      
      },
      
      series: [{
      
      name: 'Payment',
      
      colorByPoint: true,
      
      data: [{
      
        name: 'Cash',
      
        y: cash,
      
        sliced: true,
      
        selected: true
      
      },{
      
        name: 'Cheque',
      
        y: cheque
      
      }, {
      
        name: 'paypal',
      
        y: paypal
      
      },{
      
        name: 'PayUmoney',
      
        y: PayuMoney
      
      },
      
      {
      
        name: 'Bank',
      
        y: Bank
      
      },
      
      {
      
        name: 'Online Payment',
      
        y: OnlinePayment
      
      },
      
       {
      
        name: 'stripe',
      
        y: stripe
      
      }]
      
      }]
      
      });
      
      }
      
      }); 
      
   </script>
   <?php } ?>  
   <?php
      $usertype = $this->session->userdata("usertype");
      
      if($usertype == "Student") {
      
      ?>
   <div class="row">
      <div id="rating" style="color: green;font-size: 100%;font-weight: bold;text-align: center;"></div>
      <div class="col-md-3 col-sm-6" >
         <a href="<?=base_url('subject')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail">
                           <h3 class="cl-info"> <?php echo count($subject)+count($optionalsubject) ?></h3>
                           <span>Total subjects</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption danger">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon ti ti-ruler-pencil"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <h3 class="cl-danger">Dec 2019</h3>
                        <span>Exam Date</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-danger">
                        <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('mark/view')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-check-square-o" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail">
                           <h3 class="cl-warning"> 0 </h3>
                           <span>Total Results Out</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('invoice/index')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption success">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-rupee"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail">
                           <h3 class="cl-success"><i class="fa fa-inr" aria-hidden="true">&nbsp;</i><?php echo $totalamountSend->amount; ?></h3>
                           <span>Total Fees</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-success">
                           <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <div class="row">
      <div class="col-md-8">
         <div class="box-main border-red">
            <div class="box-main-header">
               <h3>My Profile</h3>
            </div>
            <div class="box-main-body my-profile">
               <div class="profile-head">
                  <div class="profile-head-content">
                     <img src="<?=base_url("uploads/images/".$this->session->userdata('photo')); ?>" class="img-responsive img-circle"  alt="user" />
                     <p><strong><?=$student->name?></strong></p>
                     <p class="pdt0">
                        <?=$class->classes?>                           
                     </p>
                  </div>
               </div>
               <div class="profile-body fading">
                  <h2><?=$this->lang->line("personal_information")?></h2>
                  <hr/>
                  <table class="pull-left text-left">
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_roll")?></td>
                        <td width="5%">:</td>
                        <td width="30%"><?=$student->roll?></td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_email")?></td>
                        <td width="5%">:</td>
                        <td width="30%"><?=$student->email?></td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_sex")?></td>
                        <td width="5%">:</td>
                        <td width="30%"> <?=$student->sex?></td>
                     </tr>
                  </table>
                  <table class="pull-right text-left width40">
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("father_name")?></td>
                        <td width="5%">:</td>
                        <td width="30%">
                           <?php if ($student->father_name!='') {
                              echo $student->father_name;
                              
                              }else{
                              
                                echo "--";
                              
                              }
                              
                              
                              
                               ?>
                        </td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("mother_name")?></td>
                        <td width="5%">:</td>
                        <td width="30%">
                           <?php if ($student->mother_name!='') {
                              echo $student->mother_name;
                              
                              }else{
                              
                                echo "--";
                              
                              }
                              
                              
                              
                               ?>
                        </td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_phone")?></td>
                        <td width="5%">:</td>
                        <td width="30%">
                           <?php if ($student->phone!='') {
                              echo $student->phone;
                              
                              }else{
                              
                                echo "--";
                              
                              }
                              
                              
                              
                               ?>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="box-main-footer clearfix">
               <a href="<?php echo base_url() ?>profile/index" onload="myfunction()" class="btn btn-sm btn-view pull-right" data-toggle="popover">View Profile</a>
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="box-main border-yellow mrgtop">
            <div class="box-main-header">
               <h3>Learning Management System</h3>
            </div>
            <div class="box-main-body">
               <div class="digital-library">
                  <a href="<?php echo base_url() ?>lms/lmsLatest" target="_blank">Visit LMS</a>

                  <video style="" width="100%" height="100%" controls="">
                     <source src="<?php echo base_url() ?>uploads/videos/lmsdemo.mp4" type="video/mp4">
                  </video>
                  <!-- <video style="" width="100%" height="100%" controls="">
                     <source src="" type="video/mp4">
                     <source src="" type="video/ogg">
                  </video> -->
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row page-titles">
      <div class="col-md-12 align-self-center">
         <h3 class="text-themecolor title-font-size">Recent Notice </h3>
      </div>
   </div>
   <?php 
      if ( $usertype == "Student") { ?>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="table-responsive">
               <div class="table-wrapper ">
                  <table class="table table-striped table-hover">
                     <thead style="background: #3c8dbc;">
                        <tr style="color: #fff;">
                           <td style="font-weight: 600;">S.N.</td>
                           <td style="font-weight: 600;">Title</td>
                           <td style="font-weight: 600;">Date</td>
                           <td style="font-weight: 600;">Type</td>
                           <td style="font-weight: 600;">Action</td>
                        </tr>
                     </thead>
                     <tbody>
                        <?php  {$i = 1; 
                           foreach( $NoticeD  as  $notices) { ?>
                        <tr>
                           <td><?php echo $i; ?></td>
                           <td><?php echo substr($notices['title'], 0,20); ?></td>
                           <td><?php echo date('d-m-Y', strtotime($notices['date'])); ?></td>
                           <td><?php echo $notices['name']; ?></td>
                           <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                        </tr>
                        <?php $i++; }} ?>
                     </tbody>
                  </table>
                  <div class="box-main-footer clearfix">
                     <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <div class="row">
      <div class="col-sm-4">
         <div class="box-main1 border-yellow">
            <div class="box-main-header">
               <h3>Calender</h3>
            </div>
            <div class="box-main-body">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div id="mycalendars"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="box-main box_rau border-brightyellow">
            <div class="box-main-header">
               <h3>Thought of the Day</h3>
            </div>
            <div class="box-main-body table-display">
               <div class="daily-thought">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                     <div class="col-md-12">
                        <div class="carousel-inner">
                           <div class="item active">
                              <p>“Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.”
                              </p>
                           </div>
                           <div class="item">
                              If you want to be successful, you need to discover something you are actually passionate about.”
                           </div>
                           <div class="item">
                              “Successful and unsuccessful people do not vary greatly in their abilities. They vary in their desires to reach their potential.”
                           </div>
                           <div class="item">
                              “There are no negatives in life, only challenges to overcome that will make you stronger.”
                           </div>
                           <div class="item">
                              “When you think about it, the secret to success in social media isn't any different than the secret to success in life: help others first.”
                           </div>
                           <div class="item">
                              “You see, success isn’t a list of goals to be checked off one after another. It’s not reaching a destination. Success is a journey.”
                           </div>
                           <div class="item">
                              “When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one which has opened for us.”
                           </div>
                           <div class="item">
                              “Solve one problem, and you keep a hundred others away.”
                           </div>
                           <div class="item">
                              “At the end of the day we are accountable to ourselves - our success is a result of what we do.” 
                           </div>
                        </div>
                     </div>
                  </div>
                  <span>
                  <img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/happiness.png" alt="Good Day" />
                  </span>
                  <div>&nbsp;</div>
                  <div>&nbsp;</div>
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="col-md-9">&nbsp;</div>
                           <div class="col-md-3">
                              <a class="carousel-control" href="#myCarousel" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
                              <span class="sr-only">Previous</span>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="col-md-1">&nbsp;</div>
                           <div class="col-md-2">
                              <a class="carousel-control" href="#myCarousel" role="button" data-slide="next" >
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                              </a>
                           </div>
                           <div class="col-md-9">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="box-main box_rau border-blue">
            <div class="box-main-header">
               <h3>Weather</h3>
            </div>
            <div class="box-main-body">
               <div class="daily-weather">
                  <a class="weatherwidget-io" href="https://forecast7.com/en/30d3278d03/dehradun/" data-label_1="DEHRADUN" data-label_2="WEATHER" data-theme="original" >DEHRADUN WEATHER</a>
                  <script>
                     !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                  </script>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="myChartmodal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
         </div>
         <div class="modal-body">
            <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   Highcharts.chart('container', {
   
   chart: {
   
     plotBackgroundColor: null,
   
     plotBorderWidth: null,
   
     plotShadow: false,
   
     type: 'pie'
   
   },
   
   title: {
   
     text: ''
   
   },
   
   tooltip: {
   
     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
     pie: {
   
       allowPointSelect: true,
   
       cursor: 'pointer',
   
       dataLabels: {
   
         enabled: true,
   
         format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
         style: {
   
           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
         }
   
       }
   
     }
   
   },
   
   series: [{
   
     name: 'Brands',
   
     colorByPoint: true,
   
     data: [{
   
       name: '1st Sem',
   
       y: 41.41,
   
       sliced: true,
   
       selected: true
   
     }, {
   
       name: '2nd Sem',
   
       y: 11.84
   
     }, {
   
       name: '3rd Sem',
   
       y: 10.85
   
     }, {
   
       name: '4th Sem',
   
       y: 10.67
   
     }, {
   
       name: '5th Sem',
   
       y: 9.18
   
     }, {
   
       name: '6th Sem',
   
       y: 18.64
   
     }]
   
   }]
   
   });
   
</script>
<script type="text/javascript">
   Highcharts.chart('container3', {
   
   chart: {
   
     plotBackgroundColor: null,
   
     plotBorderWidth: null,
   
     plotShadow: false,
   
     type: 'pie'
   
   },
   
   title: {
   
     text: ''
   
   },
   
   tooltip: {
   
     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
     pie: {
   
       allowPointSelect: true,
   
       cursor: 'pointer',
   
       dataLabels: {
   
         enabled: true,
   
         format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
         style: {
   
           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
         }
   
       }
   
     }
   
   },
   
   });
   
</script>
<script type="text/javascript">
   $.ajax({
   
      type: 'GET',
   
      dataType: "json",
   
      url: "<?=base_url('dashboard/graphcall')?>",
   
      dataType: "html",
   
      success: function(data) {
   
   var response = jQuery.parseJSON(data);
   
   var chart = Highcharts.chart('container', {
   
   title: {
   
    text: 'Earning Graph'
   
   },
   
   subtitle: {
   
    text: 'Plain'
   
   },
   
   
   xAxis: {
   
    categories: ['1st Semester', '2nd Semester', '3rd Semester', '4th Semester', '5th Semester', '6th Semester', '7th Semester', '8th Semester']
   
   },
   
   series: [{
   
    type: 'column',
   
    colorByPoint: true,
   
    data: response.balance,
   
    showInLegend: false
   
   }]
   
   });
   
   }
   
   }); 
   
   $('#plain').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Plain'
   
    }
   
   });
   
   });
   
   
   
   $('#inverted').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: true,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Inverted'
   
    }
   
   });
   
   });
   
   
   
   $('#polar').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: true
   
    },
   
    subtitle: {
   
      text: 'Polar'
   
    }
   
   });
   
   });
   
</script> 
</div>
<?php } ?>
</div>

<style type="text/css">
   .fpaid{
   cursor:pointer;
   }
</style>
<style type="text/css">
   .font-icon-shortcut{
   color: #e20b0b;
   }
   .title-font-size{
   font-size: 16px;
   }
   .widget-shortcut-height{
   height: 50px;
   }
   * {
   box-sizing: border-box;
   }
   .zoom {
   transition: transform .2s; 
   }
   .zoom:hover {
   -ms-transform: scale(1.5); 
   -webkit-transform: scale(1.5);
   transform: scale(1.2); 
   }
   .thoughts{
   margin-top: 25px;
   cursor: pointer;
   }
</style>
</style>
<script></script>
<script>
   setInterval(function(){
      $("#mydivstu").load(location.href + " #mydivstu");
      $("#mydivtea").load(location.href + " #mydivtea");
      $("#mydivco").load(location.href + " #mydivco");
      $("#mydivuser").load(location.href + " #mydivuser");
   },5000);
</script>
<script>
   $('#weatherfunctionSession').click(function() {  
   
     var weatherfunctionSession = $(this).val();   
   
       $.ajax({  
   
         type: 'POST',   
   
         url: "<?=base_url('dashboard/index')?>",   
   
         data: "weatherfunctionSession=" + weatherfunctionSession,   
    
         dataType: "html",   
   
         success: function(data) {   
   
         }   
   
       });   
   
     });
   
</script>
<script>
   $(document).ready(function(){
      ('.')
       var cData = JSON.parse(`<?php echo $chart_data; ?>`);
       var data = {
        interactivityEnabled: false,
           labels: cData.label,
           datasets: [{
               label: "Users Count",
               data: cData.data,
               backgroundColor: [
                   "#FF0000",
                   "#FFFF00",
                   "#008000",
               ],
               borderColor: [
                   "#FF0000",
                   "#FFFF00",
                   "#008000",
               ],
               borderWidth: [1, 1, 1]
           }]
       };
       
       var options = {
           responsive: true,
           title: {
               display: true,
               position: "top",
               text: "All Subject Progress",
               fontSize: 18,
               fontColor: "#111"
           },
           legend: {
               display: true,
               position: "bottom"
           }
       };
       
       var canvas = document.getElementById("pie-chart");
       var ctx = canvas.getContext("2d");
       var myNewChart = new Chart(ctx, {
           type: 'pie',
           data: data
       });
       
       
       canvas.onclick = function(evt) {
           var activePoints = myNewChart.getElementsAtEvent(evt);
           if (activePoints[0]) {
               var chartData = activePoints[0]['_chart'].config.data;
               var idx = activePoints[0]['_index'];
               
               var label = chartData.labels[idx];
               var value = chartData.datasets[0].data[idx];
               
               var url = "label=" + label;
               console.log(url);
               alert(url);
           }
       };
       
   });
</script>
<script type="text/javascript">
   function verifyemailbyadmin(adminID)
   {
      $.ajax({  
   
         type: 'POST',   
   
         url: "<?=base_url('customemail/verifyemailsuperadmin')?>",   
   
         data: "adminID=" + adminID,   
    
         dataType: "html",   
   
         success: function(data)
         {  
             $('.verify').html("success! please check you email"); 
             $('.verify').css('color','red');
             $(".verify").fadeOut(30000);
   
         }   
   
       }); 
   }
</script>