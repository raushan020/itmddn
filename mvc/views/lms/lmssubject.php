  <style type="text/css">
    .panel-body .tag-new {
      position: relative;
      right: 0px;
     top: 0;
      font-size: 10px;
      font-weight: 700;
      color: #fff;
      background: #77c13a;
      line-height: 1;
      text-transform: uppercase;
      padding: 5px 10px 3px;
      
      width: 42px;
         
    }
    @-webkit-keyframes blinker {
  from {opacity: 1.0;}
  to {opacity: 0.0;}
}
.blink{
  text-decoration: blink;
  -webkit-animation-name: blinker;
  -webkit-animation-duration: 0.9s;
  -webkit-animation-iteration-count:infinite;
  -webkit-animation-timing-function:ease-in-out;
  -webkit-animation-direction: alternate;
}
.page-section{
  padding: 0px 0px 40px 0px;
}
.cow{
  position: relative;
    bottom: 19px;
    z-index: 11;
    background: black;
    color: #fff;
    padding-left: 5px;
    width: 80px;
    text-transform: uppercase;
    font-size: xx-small;
}

.paper-shadow .panel-bodyText{

  color: #f2f2f2;
  
}



</style>
  <!-- 2nd choice -->

  <div class="container">
       


    <div class="page-section">

      <div class="row">

        

         <div class="">

              <select class="flex-c-m stext-106 cl6 size-104 bor4 pointer hov-btn3 trans-04 m-r-8 m-tb-4 js-show-filter pull-right" style="margin-right: 30px; height: 30px;" onchange='GetEntryTypeData($(this).val())' name='semesterId' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>

                <option selected><?=$student->yearsOrSemester?></option>

                  <!-- <?php



                    if ($classesRow) {

                      $looping    =  (int) $classesRow->duration;

                      if ($classesRow->mode==1) {

                        for ($i=1; $i <=$looping; $i++) {

                          if (CallYears($i)==$this->session->userdata('FilterlmsyearSemesterID')) {

                            $select = 'Selected';

                          }else{

                            $select = '';

                          }

                          echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                        }

                    }else{

                      for ($i=1; $i <=(2*$looping); $i++) {

                        if (CallSemester($i)==$this->session->userdata('FilterlmsyearSemesterID')) {

                          $select = 'Selected';

                        }else{

                          $select = '';

                        }

                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                      }

                    }

                  }

                ?> -->



               </select>

                          <div class="clearfix"></div>

                          <div class="clearfix"></div>


<!-- 
                                <div class="forReset pull-right" style="margin-right:32px;"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div> -->



           </div>

           <div class="col-md-4"></div>

           <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">

          <div class="col-md-9">

          <div class="row" data-toggle="isotope">

              

            <?php     

              if(count($subjects)) { 

              foreach($subjects as $key => $subject) {  

            ?>



            <div class="item col-xs-12 col-sm-6 col-lg-4">

              <div class="panel panel-default paper-shadow" data-z="0.5">



                <div class="panel-heading">

                  <div class="media media-clearfix-xs-min v-middle">

                  <div class="media-body text-caption text-light">

                       <?=$key+1?> of <?=count($subjects)?>

                    </div>

                    <div class="media-right">
                    
                      <div class="progress progress-mini width-50_1 margin-none">

                        <div class="progress-bar progress-bar-grey-600" role="progressbar" aria-valuenow="<?php 
 echo $subject['percentage']; ?>" aria-valuemin="0" aria-valuemax="80">

                        </div>

                      </div>

                    </div>

                  </div>

                </div>



                <div class="cover overlay cover-image-full hover">

                  <span class="img icon-block height-100 bg-default"></span>

                  <a class="padding-none overlay overlay-full icon-block bg-default">

                    <span class="v-center">

                      <i class="fa fa-book"></i>

                    </span>

                  </a>



                  <a class="overlay overlay-full overlay-hover overlay-bg-white">

                    <span class="v-center">

                      <button type="button" class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></button>

                    </span>

                  </a>



                </div>
<div class="cow"><?php echo str_replace('_',' ',$subject['yearsOrSemester']); ?></div>


                <div class="panel-body panel-bodyText" style="height:100px">

                  <h6 class="text-headline margin-v-0-10" style="font-weight: 700;"><?php echo $subject['subject']; ?></h6>



                </div>

                <hr class="margin-none" />

                <div class="panel-body">


<?php 
$this->db->where('subjectID',$subject['subjectID']);
$rowData = $this->db->get('epub')->result();
  if($rowData) { 
?>

<a style="width: 100%;background: limegreen;color: white;" class="btn btn-white btn-flat paper-shadow relative background_lms_button" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmscourse').'/'.$subject['subjectID']; ?>">Read </a>
<?php }else{?>
  
  <a style="width: 100%;background: #ec7373;color: white;" class="btn btn-white btn-flat paper-shadow relative background_lms_danger" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmscourse').'/'.$subject['subjectID']; ?>">Read </a>
  <?php }?>
             
                  
                 <!--  <a style="width: 100%;" class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmscourse').'/'.$subject['subjectID']; ?>">Read </a> -->
<?php if ($subject['percentage']==0) { ?>
  

                  <span class="pull-right"><div class="tag-new">New</div></span>
<?php } ?>
   </div>



              </div>

            </div>

            

            

            <?php }} ?>

            

            

          </div>
          </div>
          
            

             <?php $this->load->view("components/page_lmssidebar"); ?>
         





          <br/>

          <br/>



        </div>





    





    <script type="text/javascript">

    $('#yearSemesterID').change(function() {

      // alert(yearSemesterID);

      // $('#subjectsTablesPdf').DataTable().state.clear();

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('lms/lmssubject')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });



</script>





<script type="text/javascript">

        function ResetSemesterYear(){

            $.ajax({



                type: 'POST',

                url: "<?=base_url('lms/ResetSemesterYear')?>",



                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {



                    location.reload();

                }

            });

          }

</script>

