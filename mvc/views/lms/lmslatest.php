  <style type="text/css">
  	.height100{
  		height :20px;
  	}
	.height500{
		height :320px;
	}
  .paper-shadowRahul:hover{
    color:red;
    box-shadow: 0px 1px 8px 3px #ccc;
    /*text-align: center;*/
  }
  .text-danger {
	  color:red!important;
	  font-weight:bold!important;
  }
  .text-warning {
	  color:orange!important;
	  font-weight:bold!important;
  }
  .text-success {
	  color:green!important;
	  font-weight:bold!important;
  }
  .paper-shadowRahul:hover .panel-bodyText h6{
    color:#00f3ff;
    /*box-shadow: 0px 1px 8px 3px #ccc;*/
    
  }
  .paper-shadowRahul:hover .panel-bodyText{
    text-align: center;
    background: cadetblue;
  }

  .service-single{
    margin-top: 20px;
  padding-bottom: 10px;
  border: 1px solid #e8e8e8;
  box-shadow: 0 0 10px #e8e8e8;
}
.service-single .box-top{
  background: #2196f3;
  padding: 20px;
  color: #fff;
}
.service-single .box-top i{
  font-size: 50px;
      padding: 0px 0px 20px;
}
.service-single .box-top h2{
  font-size: 16px;
  text-transform: uppercase;
  margin-bottom: 45px;
}
.service-single .box-down{
  max-width: 350px;
  min-height: 140px;
  margin: 0 auto;
  border-radius: 100%;
  background: #fff;
  margin-top: -50px;
  padding: 50px 25px 10px 25px;
}
.service-single .btn-holly{
    background: #fff;
  border:1px solid #437094;
  border-radius: 20px;
  color: #437094;
  padding: 10px;
  width: -webkit-fill-available;
}
.service-single .btn-holly:hover{
    
    border: 1px dashed #437094;;
    color: red;

}
.service-single .box-down p{
  min-height: 40px;
}
.width-100_1{
  width: 50% !important;
}
.box-down .tag-new {
      position: absolute;
      right: 11px;
	  top: 29px;
      font-size: 10px;
      font-weight: 700;
      color: #fff;
      background: #77c13a;
      line-height: 1;
      text-transform: uppercase;
      padding: 5px 10px 3px;
      
      width: 42px;
         
    }
	table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}
  </style>

  <div class="container">



    <div class="page-section">

      <div class="row">



        <div class="col-md-12">



          <div class="panel panel-default">

            <div class="media v-middle">

              <div class="media-left">

                <div class="bg-green-400 text-white">

                  <div class="panel-body">

                    <!-- <i class="fa fa-magic fa-fw fa-2x"></i> -->
                    <img src="<?php echo base_url() ?>assets/img/book_lms.png" alt="LMS">
                    <!-- <i class="fas fa-cog"></i> -->

                  </div>

                </div>

              </div>

              <div class="media-body" style="font-size: 16px;color: #000;font-weight: 600;">

                <span class="text-body-2" >Welcome Aboard.</span> Begin your exciting journey of learning.

              </div>

              <!-- <div class="media-right media-padding">

                <a class="btn btn-white paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmssubject');?>">

                Start now

            </a>

              </div> -->

            </div>

          </div>



          <div class="row" data-toggle="isotope">
			<div class="co-md-12">
			
			

          	<!-- =========================================================================================== -->

            <!-- <div class="col-xs-12 col-lg-9"> -->

              <!-- <div class="panel panel-default paper-shadow" data-z="0.5"> -->

                <!-- <div class="panel-heading"> -->

                  <!-- <h4 class="text-headline margin-none">Your Recent Subjects</h4> -->

                  <!-- <p class="text-subhead text-light">View a list of all recent read subjects</p> -->

                <!-- </div> -->

                <!-- <ul class="list-group"> -->

<?php
$usertype = $this->session->userdata("usertype");
if($usertype == "Teacher")
{

}
else{
$counter = 0;
foreach ($subjects_recent_three as $key => $value) {
  if($value['percentage']!=0){
   $counter +=1;
   if($counter<4){  
  ?>
                  <!-- <li class="list-group-item media v-middle"> -->

                    <!-- <div class="media-body"> -->

                      <!-- <a href="<?php echo base_url() ?>lms/lmscourse/<?php echo $value['subjectID'] ?>" class="text-subhead list-group-link"><?php echo $value['subject']; ?></a> -->

                    <!-- </div> -->

                    <!-- <div class="media-right"> -->
<!-- <span class="pull-right"><?php echo round($value['percentage'],2)." "."%"; ?> </span> -->
                      <!-- <div class="progress progress-mini width-100 margin-none">
							
                        <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="<?php echo $value['percentage']; ?>" aria-valuemin="0" aria-valuemax="100">

                        </div>

                      </div> -->

                    <!-- </div> -->

                  <!-- </li> -->


                <?php } }  }  if($counter==0) {  ?>
   <!-- <li class="list-group-item media v-middle">
  There are no enrolled recent subjects. To enrol & begin reading any e-book, go to <a href="<?php echo base_url() ?>lms/lmssubject">Subjects</a>
   </li> -->
                <?php } ?>
                <!-- </ul> -->

                <!-- <div class="panel-footer text-right"> -->

                  <?php if($counter==0) { ?>
                  <!-- <a class="btn btn-white paper-shadow relative" disabled="disabled" data-toggle="modal" data-target="#myModal"> View all</a> -->
                  <?php }else{ ?>
                    <!-- <a class="btn btn-white paper-shadow relative" data-toggle="modal" data-target="#myModal"> View all</a> -->
                  <?php } } ?>

                <!-- </div> -->

              <!-- </div> -->

            <!-- </div> -->

            <!-- side bar php -->
            <?php $this->load->view("components/page_lmssidebar"); ?>
            <div class="col-xs-12 col-lg-9">

              <div class="panel panel-default paper-shadow" data-z="0.5">

                <div class="panel-heading">

                  <h4 class="text-headline margin-none"><strong>SUBJECTS</strong></h4>

                  <p class="text-subhead text-light" style="color: #000;"><strong>List of all subjects</strong></p>

                </div>

                <div class="height500">
                  
          <?php     
// print_r($subjects_three);
// exit();
              if(($subjects_three)) { 

              foreach($subjects_three as $key => $subject_teen) {  

            ?>

              <div class="col-xs-12 col-lg-4">
                <div class="service-single text-center wow fadeInDown" data-wow-delay="0.2s">
                  <div class="progress progress-mini width-100_1 margin-none" style="background: #d0c5c5">

                        <div class="progress-bar progress-bar-grey-600" role="progressbar" aria-valuenow="<?php 
 echo $subject_teen['percentage']; ?>" aria-valuemin="0" aria-valuemax="80">

                        </div>

                      </div>
                  <div class="box-top" style="margin-top: 3px;">
                    
                    
                      <i class="fa fa-graduation-cap"></i>
                    <h2></h2>
                  </div>

                    
                      

                    
                  <div class="box-down">

                    <p><?php echo $subject_teen['subject']; ?></p>
                    <div class="smooth">
                       <a href="<?php echo base_url('lms/lmscourse').'/'.$subject_teen['subjectID'].'/'. $this->input->get("classes");  ?>" class="btn btn-holly">Read</a>
                    </div>
                    <?php if ($subject_teen['percentage']==0) { ?>
                    <span class="pull-right"><div class="tag-new">New</div></span>
                    <?php } ?>
                  </div>
                </div>
              </div>

            <!-- <div class="col-lg-4">

              <div class="panel panel-default paper-shadow paper-shadowRahul" data-z="0.5" style="margin-top: 10px;">



                <div class="panel-heading">

                  <div class="media media-clearfix-xs-min v-middle">

                  <div class="media-body text-caption text-light">

                      Subject <?=$key+1?> of <?=count($subject_teen)?>

                    </div>

                    <div class="media-right">
                    
                      <div class="progress progress-mini width-50_1 margin-none">

                        <div class="progress-bar progress-bar-grey-600" role="progressbar" aria-valuenow="<?php 
 echo $subject_teen['percentage']; ?>" aria-valuemin="0" aria-valuemax="80">

                        </div>

                      </div>

                    </div>

                  </div>

                </div>



                <div class="height100"> 

                </div> -

                <div class="panel-body panel-bodyText" style="height:100px">

                  <h6 class="text-headline margin-v-0-10"><?php echo $subject_teen['subject']; ?></h6>

                </div>

                <hr class="margin-none" />

                <div class="panel-body">



                  <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmscourse').'/'.$subject_teen['subjectID']; ?>">Go to course</a>



                </div>



              </div>

            </div>  -->          

            

            <?php }} ?>

                </div>

                <div class="panel-footer text-right">

                  <a href="<?php echo base_url('lms/lmssubject'); ?>" class="btn btn-success paper-shadow relative" style="color: #000; background-color:#66bb6a;"> All Subject</a>

                </div>

              </div>

            </div>
            </div>
<!-- modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">All Subject</h4>
        </div>
        <div class="modal-body">
                        <ul class="list-group">

<?php
foreach ($subjects_recent_three as $key => $value) {
  if($value['percentage']!=0){
 ?>
                  <li class="list-group-item media v-middle">

                    <div class="media-body">

                      <a href="<?php echo base_url() ?>lms/lmscourse/<?php echo $value['subjectID'] ?>/<?php echo $this->input->get('classes') ?>" class="text-subhead list-group-link"><?php echo $value['subject']; ?></a>

                    </div>

                    <div class="media-right">
						<span class="pull-right"><?php echo round($value['percentage'],2)." "."%"; ?> </span>
                      <div class="progress progress-mini width-100 margin-none">

                        <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="<?php echo $value['percentage']; ?>" aria-valuemin="0" aria-valuemax="100">

                        </div>

                      </div>

                    </div>

                  </li>
                <?php }  }?>
                </ul>  


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

            
<?php 
 if($usertype != "Teacher")
                        { ?>



<div class="clearfix"></div>
            

            <div class="col-xs-12 col-lg-12">
              <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                  <h4 class="text-headline margin-none" style="color: #000;font-weight: 700;">ASSIGNMENT</h4><br>
                  <p class="text-subhead text-light" style="color: #000;">Your recent performance</p>
                </div>
		<table>
			<thead>
				<th>Quiz Name</th>
				<th>Unit</th>
				<th>Semester</th>
				<th>Score</th>
				<th>Grade</th>
			</thead>
			<tbody>
              <?php
              $usertype = $this->session->userdata("usertype");
if($usertype == "Teacher")
{

}
else{
					$counter_assign =0;
					foreach ($assignment_recent_three as $key => $value) 
					{
						$counter_assign +=1;
						if($counter_assign<5)
						{
               ?>
							<tr>
								<td><?php echo $value->quiz_name; ?></td>
								<td>
									<?php
										$unit = $this->db->select('unit_name')->from('units')->where('unitID',$value->unitID)->get()->result_array();
										foreach($unit as $unit)
										{
											echo $unit['unit_name'];
										}
									?>
								</td>
								<td>
									<?php echo $value->yearsOrSemester; ?></td> 
								</td>
								<td>
									<?php 
										$qid = explode(",",$value->qids);
										$score = 0;
										$outoff = 0;
										foreach($qid as $qid)
										{
											$query = $this->db->select('score')->from('ets_options')->where('qid',$qid)->get()->result_array();
											foreach($query as $rows)
											{
												$outoff += $rows['score'];
											}
										}
										
										$sql = $this->db->select('score_u')->from('ets_answers')->where('rid',$value->rid)->get()->result_array();
										foreach($sql as $row)
										{
											$score += $row['score_u'];
										}
										echo $score."/".$outoff;
									?>
								</td>
								<td>
									<?php
										if($outoff!=0)
										{
											$grade = ($score/$outoff)*100;
											if( $grade >= 0 && $grade < 40 ) {
												echo '<span class="text-danger">Bad</span>';
											}
											else if( $grade > 39 && $grade < 75 ) {
												echo '<span class="text-warning">Average</span>';
											}
											else if($grade > 74 && $grade <= 100 ) {
												echo '<span class="text-success">Good</span>';
											}
										}
										else
										{
											//no comment please
										}
									?>
								</td>
							<tr>
						
                <?php 	} 
					} 
				?>
                </tbody>
			</table>

                <div class="panel-footer">
                  <?php  if($counter_assign==0){ ?>

                  <a href="#" disabled="disabled" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php }else{ ?>
                  <a href="#" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php } } ?>

                </div>

              </div>

            </div>



<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">All Assignment</h4>
        </div>
        <div class="modal-body">
			<table>
				<thead>
					<th>Quiz Name</th>
					<th>Unit</th>
					<th>Semester</th>
					<th>Score</th>
					<th>Grade</th>
				</thead>
				<tbody>
					<?php
						foreach ($assignment_recent_three as $key => $value) 
						{
					?>
						<tr>
							<td><?php echo $value->quiz_name; ?></td>
							<td>
								<?php
									$unit = $this->db->select('unit_name')->from('units')->where('unitID',$value->unitID)->get()->result_array();
									foreach($unit as $unit)
									{
										echo $unit['unit_name'];
									}
								?>
							</td>
							<td>
								<?php echo $value->yearsOrSemester; ?></td> 
							</td>
							<td>
								<?php 
									$qid = explode(",",$value->qids);
									$score = 0;
									$outoff = 0;
									foreach($qid as $qid)
									{
										$query = $this->db->select('score')->from('ets_options')->where('qid',$qid)->get()->result_array();
										foreach($query as $rows)
										{
											$outoff += $rows['score'];
										}
									}
									
									$sql = $this->db->select('score_u')->from('ets_answers')->where('rid',$value->rid)->get()->result_array();
									foreach($sql as $row)
									{
										$score += $row['score_u'];
									}
									echo round($score)."/".round($outoff);
								?>
							</td>
							<td>
								<?php
									if($outoff!=0)
									{
										$grade = ($score/$outoff)*100;
										if( $grade >= 0 && $grade < 40 ) {
											echo '<span class="text-danger">Bad</span>';
										}
										else if( $grade > 39 && $grade < 75 ) {
											echo '<span class="text-warning">Average</span>';
										}
										else if($grade > 74 && $grade <= 100 ) {
											echo '<span class="text-success">Good</span>';
										}
									}
									else
									{
										//no comment please
									}
								?>
							</td>
						<tr>
										
					<?php   
						}
					?>
				</tbody>
			</table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php } ?>


            <!-- <div class="item col-xs-12 col-lg-6">

              <div class="panel-heading list-group-item">

                  <h4 class="text-headline margin-none">Forum Activity</h4>

                  <p class="text-subhead text-light">Latest forum topics & comments</p>

                </div>

              <ul class="list-group relative paper-shadow" data-hover-z="0.5" data-animated>

                <li class="list-group-item paper-shadow">

                  <div class="media v-middle">

                    <div class="media-left">

                      <a href="#">

                        <img src="images/people/110/guy-3.jpg" alt="person" class="img-circle width-40" />

                      </a>

                    </div>

                    <div class="media-body">

                      <a href="" class="text-subhead link-text-color">Can someone help me with AngularJS?</a>

                      <div class="text-light">

                        Topic: <a href="#">AngularJS</a> &nbsp; By: <a href="#">Adrian Demian</a>

                      </div>

                    </div>

                    <div class="media-right">

                      <div class="width-60 text-right">

                        <span class="text-caption text-light">1 hr ago</span>

                      </div>

                    </div>

                  </div>

                </li>

                <li class="list-group-item paper-shadow">

                  <div class="media v-middle">

                    <div class="media-left">

                      <a href="#">

                        <img src="images/people/110/guy-6.jpg" alt="person" class="img-circle width-40" />

                      </a>

                    </div>

                    <div class="media-body">

                      <a href="#" class="text-subhead link-text-color">Can someone help me with AngularJS?</a>

                      <div class="text-light">

                        Topic: <a href="website-forum-category.html">AngularJS</a> &nbsp; By: <a href="#">Adrian Demian</a>

                      </div>

                    </div>

                    <div class="media-right">

                      <div class="width-60 text-right">

                        <span class="text-caption text-light">2 hrs ago</span>

                      </div>

                    </div>

                  </div>

                </li>

                <li class="list-group-item paper-shadow">

                  <div class="media v-middle">

                    <div class="media-left">

                      <a href="#">

                        <img src="images/people/110/guy-5.jpg" alt="person" class="img-circle width-40" />

                      </a>

                    </div>

                    <div class="media-body">

                      <a href="#" class="text-subhead link-text-color">Can someone help me with AngularJS?</a>

                      <div class="text-light">

                        Topic: <a href="#">AngularJS</a> &nbsp; By: <a href="#">Adrian Demian</a>

                      </div>

                    </div>

                    <div class="media-right">

                      <div class="width-60 text-right">

                        <span class="text-caption text-light">3 hr ago</span>

                      </div>

                    </div>

                  </div>

                </li>

                <li class="list-group-item paper-shadow">

                  <div class="media v-middle">

                    <div class="media-left">

                      <a href="#">

                        <img src="images/people/110/guy-4.jpg" alt="person" class="img-circle width-40" />

                      </a>

                    </div>

                    <div class="media-body">

                      <a href="website-forum-thread.html" class="text-subhead link-text-color">Can someone help me with AngularJS?</a>

                      <div class="text-light">

                        Topic: <a href="#">AngularJS</a> &nbsp; By: <a href="#">Adrian Demian</a>

                      </div>

                    </div>

                    <div class="media-right">

                      <div class="width-60 text-right">

                        <span class="text-caption text-light">4 hr ago</span>

                      </div>

                    </div>

                  </div>

                </li>

              </ul>

            </div> -->

          </div>

          <br/>

          <br/>

        </div>

		<!-- Course And Subject Filter -->     

	<!-- 	<?php 		



			if(count($subjects)) { 



            foreach($subjects as $key => $subject) {                 



        ?>



    <div class="col-lg-4 col-md-6">

      <div class="categorie-item">

        <div class="ci-thumb set-bg" data-setbg="<?php echo base_url('assets/lms/images/place1-full.jpg')?>" style="background-image: url('<?php echo base_url('assets/lms/images/place1-full.jpg')?>')"> 

          

        </div>

        <div class="ci-text">

          <h5><?php echo $subject['subject_code']; ?></h5>

          <span><?php echo $subject['subject']; ?></span>

        </div>

      </div>

    </div>



<?php }} ?> -->

  </div>

  <!-- 2nd choice -->

    <style type="text/css">

      .paper-shadow .panel-bodyText{

        color: #f2f2f2;

      }
    </style>