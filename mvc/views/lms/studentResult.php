<style>
	@media print {
		body * {
			visibility: hidden;
		}
		#printableArea, #printableArea * {
			visibility: visible;
		}
		#printableArea {
			left: 0;
			top: 0;
		}
	}
</style>
<div class="col-lg-12">
	<div class="col-lg-2">
		<p>Total Question: [ <?php echo count($result); ?> ]</p>
	</div>
	<div class="col-lg-2">
		<!--<p>Scored Obtain: [ 6 ]</p>-->
	</div>
	<div class="col-lg-8">
		<a href="javascript:void(0)" class="btn btn-primary" id="print">Print</a>
	</div>
</div>
<div class="col-lg-12">
	<hr/>
</div>
<div id="printableArea">
<?php
	foreach($result as $fetch)
	{
		foreach($fetch as $row)
		{
			//echo json_encode($row);
?>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="wrapped">
							<ul>
								<li><p><?php echo $row['question']; ?></p></li>
								<?php
									foreach($row['object'] as $option)
									{
								?>
										<li>
											<?php 
												if($option->student_selectanswer == $option->oid && ($option->score=='1' || $option->score=='0.5'))
												{
													echo "<b style='color: #06d206;'>".$option->q_option." <i class='fa fa-check'></i></b>";
												}
												else if($option->student_selectanswer == $option->oid && $option->score=='0')
												{
													echo "<b style='color: red;'>".$option->q_option." <i class='fa fa-remove'></i></b>";
												}
												else
												{
													echo $option->q_option;
												}
											?>
										</li>
								<?php
									}
								?>
							</ul>
							<div id="yourAns">
								Correct Answer: <span>
												<?php
													foreach($row['object'] as $option)
													{ 
														if(($option->score=='1' || $option->score=='0.5') && $option->q_option!='')
														{
															echo "<b style='color: blue;'>[ ".$option->q_option." ]</b>";
														}
													}
												?>
											</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr/>
<?php			
		}
	}
?>
</div>
<script>
	$("#print").click(function () {
		$("#printableArea").show();
		window.print();
	});
</script>