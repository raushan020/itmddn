
<table class="table table-responsive table-striped table-bordered table-hover" id="timeTable">
	<thead>
		<tr role="row">
			<th>Course</th>
            <?php if($timing)
            {
                foreach ($timing as $key => $value)
                { ?>
                    <th><?php echo $value->start_time."-".$value->end_time ?></th>
               <?php }
            }
            else
            {
                echo "Time Not Assign";
            } ?>
		</tr>
	</thead>
	<tbody>
	    <?php
        	foreach ($timetable as $key1 => $value) 
        	{
                // print_r($value);
                // exit();
        		foreach ($value as $key2 => $values) 
        		{
        		    $course_id = $values['classesID'];
                    $semester = $values['yearsOrSemester'];
        ?>
        			<tr class="odd">
                        <td><?=$values['classes'] ?><br><?=$values['yearsOrSemester'] ?></td>
                        <?php foreach ($timing as $key => $value) { ?>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data($course_id,$semester,$value->start_time,$value->end_time);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                       <?php } ?>
                    </tr>
        <?php
        		}
        	}
        ?>
	</tbody>
</table>