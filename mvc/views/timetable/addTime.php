<style>
    .attachment-block {
        border-radius: 0px;
        width: 225px;
        margin: 0px;
    }
    td,th{
        border: 2px solid gray;
    }
    #trs {
        background:#e8eef1;
    }
    .dataTables_info {
        display:none;   
    }
    .selectBox {
      position: relative;
    }
    
    .selectBox select {
        width:100%;
        color: #000;
        font-size: 13px;
        height: 36px;
        box-shadow: none;
        border-radius: 2px;
        border: 1px solid #e8eef1;
        font-weight: 600;
    }
    
    .overSelect {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
    }

    #checkboxes {
        display: none;
        border: 1px #0064f9 solid;
        line-height: 2em;
    }
    
    #checkboxes label {
        display: block;
        padding: 0px 10px;
        margin-bottom: 0px;
    }
    
    #checkboxes label:hover {
        background-color: #1e90ff;
        color:#fff;
    }
    #green {
        color:green;
    }
    #red {
        color:red;
    }
    #actions {
        text-align: right;
    }
</style>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"><i class="fa fa-table"></i> Time Table</h3>
    </div>  
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("timetable/index")?>"><i class="fa fa-bullseye"></i> Time Table</a></li>
            <li class="active">Add Lecture Timing</li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <a href="<?=base_url('timetable/index')?>" class="btn btn-link" style="margin-bottom: 10px;"> <i class="fa fa-angle-double-left"></i> BACK</a>
    <div class="row">
        <div class="card">
            <div class="card-body">
                
                <!----------------------------------Form Start------------------------------------>
                    <form method="post">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="start_time" class="control-label">Start Time</label>
                                <input type="time" class="form-control" name="start_time" id="start_time" value="">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="end_time" class="control-label">End Time</label>
                                <input type="time" class="form-control" name="end_time" id="end_time" value="">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                       <!--  <div class="col-sm-2"> -->
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-8 ">
                                    <center>
                                    <input type="submit" class="btn btn-success add-btn update-btn" name="submit" id="submit"/> 
                                    </center>         
                                </div>
                                <div class="clearfix"></div>
                            </div>
                      <!--   </div> -->
                    </form>
                <!--------------------------------End Form Start---------------------------------->
                
                <!----------------------------------Time Table------------------------------------>
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <div id="views">
                                <table class="table table-responsive table-striped table-bordered table-hover">
                                    <thead>
                                        <tr role="row">
                                            <th>
                                                Start Time 
                                            </th>
                                            <th>
                                                End Time
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(count($timing) > 0) 
                                            {
                                                foreach ($timing as $key => $value) {
                                        ?>
                                                    <tr>
                                                        <td><?php echo $value->start_time; ?></td>
                                                        <td><?php echo $value->end_time; ?></td>
                                                        <td>
                                                            <a href="<?=base_url('timetable/deleteTiming?q='.$value->timeID.''); ?>" class="btn btn-danger btn-xs mrg for_margR trash" data-placement="top" data-toggle="tooltip" aria-hidden="true" title="Delete" onclick="return confirm('Do you Really want to Delete This?')"><i class="fa fa-trash-o"></i></a>
                                                        </td>
                                                    </tr>
                                        <?php
                                                }
                                            }else{
                                        ?>
                                                <p>No Lecture Time Found</p>
                                        <?php        
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <!--------------------------------End Time Table---------------------------------->
                
            </div>
        </div>
    </div>
</div>