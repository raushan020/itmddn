
<table class="table table-responsive table-striped table-bordered table-hover" id="timeTable">
	<thead>
		<tr role="row">
			<th>
				Course 
			</th>
			<th>
				9:15 - 10:00AM
			</th>
			<th>
				10:15 - 11:00AM
			</th>
			<th>
				11:15 - 12:00PM
			</th>
            <th>
                12:00 - 12:45PM
            </th>
			<th>
				12:15 - 1:00PM
			</th>
			<th>
				1:00 - 2:00PM
			</th>
            <th>
                1:00 - 1:45PM
            </th>
			<th>
				1:15 -  2:00PM
			</th>
            <th>
                2:00 - 2:45PM
            </th>
			<th>
				2:15 - 3:00PM
			</th>
			<th>
				3:15 - 4:00PM
			</th>
			<!-- <th>
				3:30 - 4:15PM (P9)
			</th> -->
			<th>
				4:15 - 5:00pm
			</th>
		</tr>
	</thead>
	<tbody>
	    <?php
        	foreach ($timetable as $key1 => $value) 
        	{
        		foreach ($value as $key2 => $values) 
        		{
        		    $course_id = $values['classesID'];
                    $semester = $values['yearsOrSemester'];
        ?>
        			<tr class="odd">
                        <td><?=$values['classes'] ?><br><?=$values['yearsOrSemester'] ?></td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data($course_id,$semester);
                                    // print_r($sql);die;
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data2($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                       
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data3($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                       
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data11($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data4($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data5($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data12($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data6($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data13($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data7($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data8($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data9($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td>
                        <!-- <td>
                            <table>
                                <tr>
                                <?php
                                    $sql = $this->student_m->get_week_data10($course_id,$semester);
                                    foreach($sql as $row)
                                    {
                                        $professor = $this->db->select('name,shortCodeofProfessor')->from('professor')->where('professorID',$row->professor_id)->get()->result();
                                        $subject = $this->db->select('subject')->from('subject')->where('subjectID',$row->subject_id)->get()->result();
                                        
                                        if(empty($subject) || empty($professor))
                                        {
                                            
                                        }
                                        else
                                        {
                                            echo "<td>
                                                    <div id='actions'>
                                                        <a href='".base_url()."timetable/edit_professor_classes/".$row->id."' id='green'><i class='fa fa-edit'></i></a> 
                                                        <a href='".base_url()."timetable/delete_professor_classes/".$row->id."' id='red'><i class='fa fa-trash'></i></a>
                                                    </div>
                                                        <strong>".$row->days."</strong><br>".$subject[0]->subject."<br>".$professor[0]->name."
                                                  </td>";
                                        }
                                    }
                                ?>
                                <tr>
                            </table>
                        </td> -->
                    </tr>
        <?php
        		}
        	}
        ?>
	</tbody>
</table>