
<div class="box">

    <div class="box-header">

        <h3 class="box-title"><i class="fa fa-sitemap"></i> <?=$this->lang->line('panel_title')?></h3>



       

        <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("session/index")?>"></i> <?=$this->lang->line('menu_session')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_courses')?></li>

        </ol>

    </div><!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">

            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post">



                    <?php 

                        if(form_error('session')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="" class="col-sm-2 control-label">

                            <?=$this->lang->line("session_from")?>

                        </label>

                        <div class="col-sm-2">
                                        <select class="form-control" name="year[]" id="year" value="<?php echo set_value('year'); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2017;$i>=1947;$i--){ ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                        </div>
                        <label for="" class="col-sm-2 control-label">

                            <?=$this->lang->line("session_to")?>

                        </label>
                        <div class="col-sm-2">
                                        <select class="form-control" name="year[]" id="year" value="<?php echo set_value('year'); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2018;$i>=1947;$i--){ ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                        </div>

                            <!-- <input type="text" class="form-control" placeholder="<?=$this->lang->line("session_name")?>" id="session" name="session" value="<?=set_value('session')?>" > -->

                        

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('session'); ?>

                        </span>
                    </div>

                    <?php 

                        if(form_error('session')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="" class="col-sm-2 control-label">

                            <?=$this->lang->line("calender_from")?>

                        </label>

                        <div class="col-sm-2">
                                        <select class="form-control" name="year[]" id="year" value="<?php echo set_value('year'); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2018;$i>=1947;$i--){ ?>
                                                <option value="C-<?php echo $i; ?>">C-<?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                        </div>
                        <label for="" class="col-sm-2 control-label">

                            <?=$this->lang->line("calender_to")?>

                        </label>
                        <div class="col-sm-2">
                                        <select class="form-control" name="year[]" id="year" value="<?php echo set_value('year'); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2018;$i>=1947;$i--){ ?>
                                                <option value="C-<?php echo $i; ?>">C-<?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                        </div>

                            <!-- <input type="text" class="form-control" placeholder="<?=$this->lang->line("session_name")?>" id="session" name="session" value="<?=set_value('session')?>" > -->

                        

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('session'); ?>

                        </span>
                    </div>

                    <?php 

                        if(form_error('session')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="" class="col-sm-2 control-label">

                            <?=$this->lang->line("academic_from")?>

                        </label>

                        <div class="col-sm-2">
                                        <select class="form-control" name="year[]" id="year" value="<?php echo set_value('year'); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2018;$i>=1947;$i--){ ?>
                                                <option value="A-<?php echo $i; ?>">A-<?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                        </div>
                        <label for="" class="col-sm-2 control-label">

                            <?=$this->lang->line("academic_to")?>

                        </label>
                        <div class="col-sm-2">
                                        <select class="form-control" name="year[]" id="year" value="<?php echo set_value('year'); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2018;$i>=1947;$i--){ ?>
                                                <option value="A-<?php echo $i; ?>">A-<?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                        </div>

                            <!-- <input type="text" class="form-control" placeholder="<?=$this->lang->line("session_name")?>" id="session" name="session" value="<?=set_value('session')?>" > -->

                        

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('session'); ?>

                        </span>
                    </div>

                    



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_session")?>" >

                        </div>

                    </div>



                </form>





            </div>

        </div>

    </div>

</div>

