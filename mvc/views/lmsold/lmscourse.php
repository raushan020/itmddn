  <div class="container">

    <div class="page-section">
    	<div class="row">
        <div class="col-md-12">
    	     <div class="panel-footer" style="text-align: center;">

                 <h4 class="theight">

                 	<?php                 				
             			
              			echo $subject->subject;
                			
                 	  ?>
               	  	
                	  </h4>

                 <div>

                   <p class="theight"><strong style="color: #333">Course Status:</strong> In Progress</p>

                 </div>

                 <p class="theight" style="color: green; text-transform: uppercase;">You are enrolled in this course</p>

                 <div class=""></div> 
          </div>
      </div>
  </div>

      <div class="row">
        <div class="col-md-12">



          <!-- <div class="panel panel-default">

            <div class="media v-middle">

              <div class="media-left">

                <div class="bg-green-400 text-white">

                  <div class="panel-body">

                    <i class="fa fa-credit-card fa-fw fa-2x"></i>

                  </div>

                </div>

              </div>

              <div class="media-body">

                Your LMS is Here,  <span class="text-body-2">Start Learning</span>

              </div>

              <div class="media-right media-padding">

                <a class="btn btn-white paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmssubject');?>">

                Start now

            </a>

              </div>

            </div>

          </div> -->



          <div class="row" data-toggle="isotope">

            <div class="item col-xs-12 col-lg-9">

              <div class="tabbable tabs-icons tabs-primary">

            <!-- Tabs -->
            <ul class="nav nav-tabs" style="background: whitesmoke;">
            <li class="active"><a href="#color-home-2" data-toggle="tab"><i class="fa fa-address-book"></i> Online Reader</a></li>
              <li><a href="#color-profile-2" data-toggle="tab"><i class="fa fa-play"></i> Watch Videos</a></li>
              <!-- <li><a href="#color-messages-2" data-toggle="tab"><i class="fa fa-sticky-note"></i> Notes</a></li> -->
            </ul>
            <!-- // END Tabs -->

            <!-- Panes -->
            <div class="tab-content" style="background-image: red">
              <div id="color-home-2" class="tab-pane active">
                <div class="table-responsive">
              <table class="table v-middle">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Subject Name</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody id="responsive-table-body">
                  <?php if(count($epub)) {

                   foreach($epub as $key => $value2) { 
  
                  ?>
                  <tr>
                    
                    <td>1.</td>
                    <td><img src="<?php echo base_url() ?>assets/lms/images/ebook.png" alt="">
                      <span><?php                         
                  
                    echo $subject->subject;
                      
                    ?></span>
                    </td>
                    <td style="text-align: center;"><button class="btn btn-primary"><a href="<?php echo base_url() ?>online_reader/reader/<?php echo $subject->subjectID.'/'.$value2->id; ?>" style="color: #fff;">Read E-Book</a></button> </td>
                  </tr>
                  <?php }}
                  else
                    {?>
                      <td><p>There are no recent Ebook to Read</p></td>
                   <?php }?>
                </tbody>
              </table>
            </div> 
              </div>

              <div id="color-profile-2" class="tab-pane">
                <div class="table-responsive">
              <table class="table v-middle">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Subject Name</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody id="responsive-table-body">
                  <?php 
                if(count($videos)) {
                    $count=1;
                 foreach($videos as $key => $value2) { 
                            
                 ?>
                  <tr>
                    
                    <td><?php echo $count ?></td>
                    <td><img src="<?php echo base_url() ?>assets/img/video.png" alt="" >
                      <span><?php                         
                  
                    echo $subject->subject;
                      
                    ?></span>
                      
                    </td>
                    <td class="text-right"><button class="btn btn-primary"><a href="<?php echo base_url() ?>lms/lmsVideos/<?php echo $subject->subjectID.'/'.$value2->videoID; ?>" style="color: #fff;">Watch Video</a></button></td>
                  </tr>
                  <?php $count++;  }
                } else
                    {?>
                      <td><p>There are no recent Video to Watch</p></td>
                   <?php }?>


                </tbody>
              </table>
            </div> 
              </div>


             <!--  <div id="color-messages-2" class="tab-pane">
                <div class="table-responsive">
              <table class="table v-middle">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Notes</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody id="responsive-table-body">
                  <tr>
                    
                    <td>There are no recent Notes to Read</td>
                    <td></td>
                    <td class="text-right">
                    </td>
                  </tr>
                  
                </tbody>
              </table>
            </div> 
              </div> -->
              
            </div>
            <!-- // END Panes -->

          </div>

      </div>

      <?php $this->load->view("components/page_lmssidebar"); ?>
           
        </div>

</div>

<div class="clearfix"></div>
<div class="col-xs-12 col-lg-12">

              <div class="panel panel-default paper-shadow" data-z="0.5">

                <div class="panel-heading">

                  <h4 class="text-headline margin-none" style="color: #000;font-weight: 700;">Assignment</h4>

                  <p class="text-subhead text-light"style="color: #000;">Your recent assignment</p>

                </div>

                <ul class="list-group">

              <?php
              $counter_assign =0;
               foreach ($assignment_recent_three as $key => $value) {
            $counter_assign +=1;
            if($counter_assign<5){
               ?>    
                  <li class="list-group-item media v-middle">

                    <div class="media-body">

                      <h4 class="text-subhead margin-none">

                        <a href="#" class="list-group-link"><?php  echo $value->quiz_name ?></a>

                      </h4>
<!-- 
                      <div class="caption">

                        <span class="text-light">Course:</span>

                        <a href="#">Basics of HTML</a>

                      </div> -->

                    </div>

                    <div class="media-right text-center">

                      <div class="text-display-1"><?php echo $value->score_obtained ?></div>

                      <span class="">Good</span>

                    </div>

                  </li>
                <?php } } ?>
                </ul>

                <div class="panel-footer">
                  <?php  if($counter_assign==0){ ?>

                  <a href="#" disabled="disabled" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php }else{ ?>
                  <a href="#" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php } ?>

                </div>

              </div>

            </div>

<!-- <script>
			var hash = window.location.hash,
				current = 0,
				demos = Array.prototype.slice.call( document.querySelectorAll( '#codrops-demos > a' ) );
			
			if( hash === '' ) hash = '#set-1';
			setDemo( demos[ parseInt( hash.match(/#set-(\d+)/)[1] ) - 1 ] );

			demos.forEach( function( el, i ) {
				el.addEventListener( 'click', function() { setDemo( this ); } );
			} );

			function setDemo( el ) {
				var idx = demos.indexOf( el );
				if( current !== idx ) {
					var currentDemo = demos[ current ];
					currentDemo.className = currentDemo.className.replace(new RegExp("(^|\\s+)" + 'current-demo' + "(\\s+|$)"), ' ');
				}
				current = idx;
				el.className = 'current-demo'; 
			}
		</script> -->