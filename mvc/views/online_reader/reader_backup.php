<div id="title" style="padding-bottom:14%;"></div>
  <div id="viewer" class="spreads"></div>
  <a id="prev" href="#prev" class="arrow1">‹</a>
  <a id="next" href="#next" class="arrow1">›</a>
  <div id="controls">
      <input id="current-percent" size="3" value="0" /> %
  </div>
  <script>

    var controls = document.getElementById("controls");
    var currentPage = document.getElementById("current-percent");
    var slider = document.createElement("input");
    var slide = function(){
        var cfi = book.locations.cfiFromPercentage(slider.value / 100);
        rendition.display(cfi);
    };
    var mouseDown = false;
    var read =  "<?php echo $read  ?>";
    // // Load the opf
    var book = ePub("../../uploads/online_reader/"+read);
//     var totalPages    =  book.location.total;
 //             this.book.pageListReady.then(function (pageList) {
//    alert("_pages" + book.pagination.totalPages);
// });


     this.book.ready.then((book) => {
            return this.book.locations.generate();
        }).then(locations => {
            console.log("Total Pages?: ", locations.length);
        var url = base_url+'online_reader/onlinereader_readingbook/'+uri; 

            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {

  }
};
            xhr.open('POST', url);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("length="+locations.length);
            
        });

// book.generatePagination(width, height).then(function () {
//     console.log("The pagination has been generated");
// });
    var rendition = book.renderTo("viewer", {
      width: "100%",
      height: 600
    });

    var displayed = rendition.display();
    var title = document.getElementById("title");

    var next = document.getElementById("next");
    next.addEventListener("click", function(e){
      rendition.next();
      e.preventDefault();
    }, false);

    var prev = document.getElementById("prev");
    prev.addEventListener("click", function(e){
      rendition.prev();
      e.preventDefault();
    }, false);

    var keyListener = function(e){

      // Left Key
      if ((e.keyCode || e.which) == 37) {
        rendition.prev();
      }

      // Right Key
      if ((e.keyCode || e.which) == 39) {
        rendition.next();
      }

    };

    rendition.on("keyup", keyListener);
    document.addEventListener("keyup", keyListener, false);

    book.ready.then(function(){
      // Load in stored locations from json or local storage
      var key = book.key()+'-locations';
      var stored = localStorage.getItem(key);
      if (stored) {
         return book.locations.load(stored);
      } else {
        // Or generate the locations on the fly
        // Can pass an option number of chars to break sections by
        // default is 150 chars
        return book.locations.generate(1600);
      }
    })
    .then(function(locations){
        controls.style.display = "block";
        slider.setAttribute("type", "range");
        slider.setAttribute("min", 0);
        slider.setAttribute("max", 100);
        // slider.setAttribute("max", book.locations.total+1);
        slider.setAttribute("step", 1);
        slider.setAttribute("value", 0);

        slider.addEventListener("change", slide, false);
        slider.addEventListener("mousedown", function(){
            mouseDown = true;
        }, false);
        slider.addEventListener("mouseup", function(){
            mouseDown = false;
        }, false);


        // Wait for book to be rendered to get current page
        displayed.then(function(){
            // Get the current CFI
            var currentLocation = rendition.currentLocation();
            // Get the Percentage (or location) from that CFI
            var currentPage = book.locations.percentageFromCfi(currentLocation.start.cfi);
            slider.value = currentPage;
            currentPage.value = currentPage;
        });

        controls.appendChild(slider);

        currentPage.addEventListener("change", function(){
          var cfi = book.locations.cfiFromPercentage(currentPage.value/100);
          
          rendition.display(cfi);
        }, false);

        // Listen for location changed event, get percentage from CFI
        rendition.on('relocated', function(location){
  
            var percent = book.locations.percentageFromCfi(location.start.cfi);
            var percentage = Math.floor(percent * 100);
            if(!mouseDown) {
                slider.value = percentage;
            }
                currentPage.value = percentage;
                
 
            
        });

        // Save out the generated locations to JSON
        localStorage.setItem(book.key()+'-locations', book.locations.save());

    });

  </script>
