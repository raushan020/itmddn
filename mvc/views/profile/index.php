<style type="text/css">
   td, th{
   padding: 10px;
   }
   td, th .stable_img img {
   border: 0px solid #ddd;
   }
   .left{
   text-align: left;
   margin-left: 30px;
   }
   /*td{ font-weight: bold;
   }*/
</style>
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-user"></i> Profile</h3>
   </div>
   <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
         <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
         <li class="active">Profile</li>
      </ol>
   </div>
</div>
<!-- /.box-header -->
<div class="container-fluid">
<div class="row">
   <div class="">
      <div class="card">
         <div class="card-body">
            <?php
               $usertype = $this->session->userdata('usertype');
               
               
               
               if($usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {
               ?>
            <!-- user profile demo end -->
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="card simple-card">
                  <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
                  </div>
                  <div class="avatar">
                     <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
                  </div>
                  <div class="info1">
                     <div class="title">
                        <h3><?php
                           $name = $this->session->userdata('name');
                           
                           if(strlen($name) > 11) 
                           {
                             echo substr($name, 0,11). ".."; 
                             if($usertype=="ClgAdmin")
                                    {
                                     ?>
                                             <h6>Super Admin</h6>
                                     <?php
                                    }
                                    else
                                    {
                                     ?>
                                            <h6><?php echo $usertype; ?></h6>
                                     <?php
                                    }
                           } else {
                             echo $name;
                              if($usertype=="ClgAdmin")
                                    {
                                     ?>
                                             <h6>Super Admin</h6>
                                     <?php
                                    }
                                    else
                                    {
                                     ?>
                                            <h6><?php echo $usertype; ?></h6>
                                     <?php
                                    }
                           }
                           
                           ?></h3>
                     </div>
                     <!-- <h3><?=$siteinfos->sname ?></h3> -->
                     <center>
                        <b>
                           <!-- <h3 style="font-size: 20px;"><?=$this->lang->line("personal_information")?></h3> -->
                        </b>
                     </center>
                  </div>
                  <div class="bottom admin_dash">
                     <!-- <div class="col-md-2 col-sm-8"><i class="glyphicon glyphicon-user text-maroon-light"></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User Name</p>
                     </div>
                     <div class="col-md-6 col-sm-8">
                        <p class="left"><?php
                           $name = $this->session->userdata('username');
                           
                           if(strlen($name) > 11) 
                           {
                             echo substr($name, 0,11). ".."; 
                           } else {
                             echo $name;
                           }
                           
                           ?></p>
                     </div>
                  </div>
                  <div class="bottom">
                     <!-- <div class="col-md-2 col-sm-8"><i class="fa fa-envelope text-maroon-light"></i></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;Communication E-mail</p>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><?php echo $email=$this->session->userdata('email'); ?>
                        <a href="#" style="color: red;" data-toggle="modal" data-target="#superadminemailmodal"><i class="fa fa-edit" ></i></a></p>
                     </div>
                  </div>
                  <div class="bottom">
                     <!-- <div class="col-md-2 col-sm-8"><i class="fa fa-phone text-maroon-light"></i></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"></span><?php echo $phone=$this->session->userdata('phone'); ?>&nbsp;&nbsp;<a href="#" style="color: red;" data-toggle="modal" data-target="#superadminphonemodal"><i class="fa fa-edit" ></i></a></p>
                     </div>
                  </div>
                  <div class="bottom">
                     <!-- <div class="col-md-2 col-sm-8"><i class=" fa fa-globe text-maroon-light"></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class=" fa fa-globe text-maroon-light"></i>&nbsp;Address</p>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><?php echo $address=$this->session->userdata('address'); ?></p>
                     </div>
                  </div>
               </div>
            </div>
          <?php } elseif($usertype == "Admin" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner"){ ?>
            <section class="panel">
               <!-- user profile demo end -->
               <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="card simple-card">
                  <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
                  </div>
                  <div class="avatar">
                     <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
                  </div>
                  <div class="info1">
                     <div class="title">
                        <h3><?php
                           $name = $this->session->userdata('name');
                           
                           if(strlen($name) > 11) 
                           {
                             echo substr($name, 0,11). ".."; 
                             if($usertype=="ClgAdmin")
                                    {
                                     ?>
                                             <h6>Super Admin</h6>
                                     <?php
                                    }
                                    else
                                    {
                                     ?>
                                            <h6><?php echo $usertype; ?></h6>
                                     <?php
                                    }
                           } else {
                             echo $name;
                              if($usertype=="ClgAdmin")
                                    {
                                     ?>
                                             <h6>Super Admin</h6>
                                     <?php
                                    }
                                    else
                                    {
                                     ?>
                                            <h6><?php echo $usertype; ?></h6>
                                     <?php
                                    }
                           }
                           
                           ?></h3>
                     </div>
                     <!-- <h3><?=$siteinfos->sname ?></h3> -->
                     <center>
                        <b>
                           <!-- <h3 style="font-size: 20px;"><?=$this->lang->line("personal_information")?></h3> -->
                        </b>
                     </center>
                  </div>
                  <div class="bottom admin_dash">
                     <!-- <div class="col-md-2 col-sm-8"><i class="glyphicon glyphicon-user text-maroon-light"></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User Name</p>
                     </div>
                     <div class="col-md-6 col-sm-8">
                        <p class="left"><?php
                           $name = $this->session->userdata('username');
                           
                           if(strlen($name) > 11) 
                           {
                             echo substr($name, 0,11). ".."; 
                           } else {
                             echo $name;
                           }
                           
                           ?></p>
                     </div>
                  </div>
                  <div class="bottom">
                     <!-- <div class="col-md-2 col-sm-8"><i class="fa fa-envelope text-maroon-light"></i></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;Communication E-mail</p>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><?php echo $email=$this->session->userdata('email'); ?>
                        <a href="#" style="color: red;" data-toggle="modal" data-target="#adminemail"><i class="fa fa-edit" ></i></a></p>
                     </div>
                  </div>
                  <div class="bottom">
                     <!-- <div class="col-md-2 col-sm-8"><i class="fa fa-phone text-maroon-light"></i></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"></span><?php echo $phone=$this->session->userdata('phone'); ?>&nbsp;&nbsp;<a href="#" style="color: red;" data-toggle="modal" data-target="#adminphone"><i class="fa fa-edit" ></i></a></p>
                     </div>
                  </div>
                  <div class="bottom">
                     <!-- <div class="col-md-2 col-sm-8"><i class=" fa fa-globe text-maroon-light"></i></div> -->
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><i class=" fa fa-globe text-maroon-light"></i>&nbsp;Address</p>
                     </div>
                     <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="left"><?php echo $address=$this->session->userdata('address'); ?></p>
                     </div>
                  </div>
               </div>
            </div>              
            </section>
           
            <!-- user profile demo end -->
            <?php }elseif($usertype == "Professor"){ ?>
            <section class="panel">
               <!-- user profile demo end -->
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="card simple-card">
                     <div class="cardheader" style="background:url(../../assets/img/card-bg-3.jpg);">
                     </div>
                     <div class="avatar">
                        <?=img(base_url('uploads/images/'.$professor->photo))?>
                     </div>
                     <div class="info1">
                        <div class="title">
                           <h3><?=$professor->name?></h3>
                        </div>
                        <h4><?=$professor->department_name?></h4>
                        <h4><?=$professor->designation_name?></h4>
                        
                     </div>
                     <div class="bottom admin_dash">
                        <!-- <div class="col-md-6 col-sm-6 col-xs-6"><i class="glyphicon glyphicon-user text-maroon-light"></i></div> -->
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"> <i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$professor->username?></p>
                        </div>
                     </div>
                     <div class="bottom">
                        <!-- <div class="col-md-6 col-sm-6 col-xs-6"><i class="fa fa-envelope text-maroon-light"></i></div> -->
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$professor->email?>&nbsp;&nbsp;<a href="#" style="color: red" data-toggle="modal" data-target="#changeemailbyprofessor"><i class="fa fa-edit" ></i></a></p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class=" fa fa-mars"></i>&nbsp;Gender</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$professor->sex?></p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><span><?=$professor->phone?></span>&nbsp;&nbsp;<a href="#" style="color: red" data-toggle="modal" data-target="#changephonebyprofessor"><i class="fa fa-edit" ></i></a></p>
                        </div>
                     </div>
                  </div>
               </div>              
            </section>
           
            <?php } elseif($usertype == "Librarian" || $usertype == "Accountant" || $usertype == "HOD") { ?>
            <section class="panel">
               <!-- user profile demo end -->
               <div class="row">
                  <div class="col-md-12 col-sm-6">
                     <div class="card simple-card">
                        <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
                        </div>
                        <div class="avatar">
                           <a href="#"><?=img(base_url('uploads/images/'.$user->photo))?></a>
                        </div>
                        <div class="info1">
                           <div class="title">
                              <h3><?=$user->name?></h3>
                           </div>
                           <div><h4><?=$this->lang->line($user->usertype)?><?php
                              ?></h4></div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- user profile demo end -->
               <!-- <div class="profile-view-head">
                  </div> -->
               <div class="panel-body profile-view-dis">
                  <h3 class="border_heading"><?=$this->lang->line("personal_information")?></h3>
                  <div class="row">
                     <div class="bottom admin_dash">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"> <i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$user->name?></p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class="fa fa-birthday-cake text-maroon-light"></i>&nbsp;Date of birth</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=date("d M Y", strtotime($user->dob));?>&nbsp;&nbsp;</p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class="fa fa-gift text-maroon-light"></i>&nbsp;Joining Date</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=date("d M Y", strtotime($user->jod))?></p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class=" fa fa-mars text-maroon-light"></i>&nbsp;Gender</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$user->sex?>&nbsp;&nbsp;</p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$user->email?>&nbsp;&nbsp;<a href="#" style="color: red" data-toggle="modal" data-target="#changeemailbyprofessor"><i class="fa fa-edit" ></i></a></p>
                        </div>
                     </div>
                     <div class="bottom">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                           <p class="left"><?=$user->phone?>&nbsp;&nbsp;<a href="#" style="color: red" data-toggle="modal" data-target="#changephonebyprofessor"><i class="fa fa-edit" ></i></a></p>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <?php } elseif($usertype == "Teacher") { ?>
            <section class="panel">
               <div class="profile-view-head">
                  <a href="#">
                  <?=img(base_url('uploads/images/'.$teacher->photo))?>
                  </a>
                  <h1><?=$teacher->name?></h1>
                  <p><?=$teacher->designation?></p>
               </div>
               <div class="panel-body profile-view-dis">
                  <h1><?=$this->lang->line("personal_information")?></h1>
                  <div class="row">
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($teacher->dob))?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($teacher->jod))?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$teacher->sex?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$teacher->religion?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$teacher->email?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$teacher->phone?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$teacher->address?></p>
                     </div>
                  </div>
               </div>
            </section>
            <?php } elseif($usertype == "Student") { ?>
               <!-- <a href="<?php echo base_url('profile/edit') ?>" class="btn-top check-all btn bg-success pull-right"> Edit Profile</a> -->
            <div id="printablediv">
               <section class="panel">
                  <!-- user profile demo end -->
                  <div class="row">
                     <div class="col-md-12 col-sm-6">
                        <div class="card simple-card">
                           <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
                           </div>
                           <div class="avatar">
                              <a href="#"><?=img(base_url('uploads/images/'.$student->photo))?></a>
                           </div>
                           <div class="info1">
                              <div class="title">
                                 <h3><?=$student->name?> (<?php echo str_replace('_', ' ', $student->yearsOrSemester) ?>)</h3>
                              </div>
                              <div><?=$class->classes?><?php
                                 ?></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- user profile demo end -->
               </section>
            </div>
            <section class="panel">
               <div class="panel-body profile-view-dis">
                  <h3 class="border_heading"><?=$this->lang->line("personal_information")?></h3>
                  <div class="row">
                     <div class="profile-view-tab">
                        <p><span>Student Id </span>: <?=$student->username?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_roll")?> </span>: <?=$student->roll?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_dob")?> </span>: <?=date("d M Y", strtotime($student->dob))?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_sex")?> </span>: <?=$student->sex?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_email")?> </span>: <?=$student->email?>&nbsp;&nbsp;<a href="#" style="color: red" data-toggle="modal" data-target="#studentemail"> <i class="fa fa-edit"></i></a></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_phone")?> </span>: <?=$student->phone?>&nbsp;&nbsp;<a href="#" style="color: red" data-toggle="modal" data-target="#studentphone"> <i class="fa fa-edit"></i></a></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("student_address")?> </span>: <?=$student->address?></p>
                     </div>
                     <!--<div class="profile-view-tab">-->
                     <!--    <p><span>Street:</span>:<?=$student->street?> </p>-->
                     <!--</div>-->
                     <div class="profile-view-tab">
                        <p><span>Pin:</span>: <?=$student->pin ?> </p>
                     </div>
                     <!-- <div class="profile-view-tab">
                        <p><span>Employment Status</span>: <?=$student->employment_status?></p>
                        
                        
                        
                        </div> -->
                     <div class="profile-view-tab">
                        <p><span>Nationality</span>: <?=$student->nationality?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Aadhar</span>: <?=$student->aadhar?></p>
                     </div>
                     <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") { ?>
                     <div class="profile-view-tab">
                        <p><span>Username</span>: <?=$student->username?></p>
                     </div>
                     <?php } ?>
                  </div>
                  <h3 class="border_heading"><?=$this->lang->line("parents_information")?></h3>
                  <div class="row">
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("father_name")?> </span>: <?=$student->father_name?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span><?=$this->lang->line("mother_name")?> </span>: <?=$student->mother_name?></p>
                     </div>
                  </div>
                  <h3 class="border_heading">Course Details</h3>
                  <div class="row">
                     <div class="profile1">
                        <table>
                           <tr>
                              <td class="bold1">Course</td>
                              <td>:</td>
                              <td><?=$class->classes?></td>
                           </tr>
                           <tr>
                              <td class="bold1">Mode</td>
                              <td>:</td>
                              <td><?php if($student->education_mode==1){
                                 echo "Year";
                                 
                                 }else{
                                 
                                 echo "Semester";
                                 
                                 }
                                 
                                 ?></td>
                           </tr>
                           <tr>
                        </table>
                     </div>
                     <div class="profile1">
                        <table>
                           <tr>
                              <td class="bold1">Semester/Year</td>
                              <td>:</td>
                              <td><?= str_replace('_', ' ', $student->yearsOrSemester) ?></td>
                           </tr>
                           <tr>
                              <td class="bold1">Session</td>
                              <td>:</td>
                              <td><?php 
                                 echo $student->session;
                                 
                                 
                                 
                                 ?></td>
                           </tr>
                           <tr>
                        </table>
                     </div>
                  </div>
                   <!-- Modal student EmailID -->
                   <div class="modal modal-box-2 fade" id="studentemail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
             <div class="modal-dialog"style="margin-top: 80px;padding-top: 26px;">
              <form method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                <div class="modal-content" id="myModalLabel">
                   <div class="modal-header theme-bg">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                  <div class="modal-body">
                     <h3>Enter your current <span>E-mail</span></h3>
                     <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" >
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <p><input type="text" class="form-control" name="studentemailid" value="<?=$student->email?>" required placeholder="Enter Your E-mail" ></p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="col-lg-12 text-center">
                              <div id="success"></div>
                              <button type="submit" class="btn modal-btn" >Save changes</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            </form>
         </div>
				<!-- model end -->
				 <!-- Modal student mobile Number-->
             <div class="modal modal-box-2 fade" id="studentphone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
             <div class="modal-dialog"style="margin-top: 80px;padding-top: 26px;">
              <form method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                <div class="modal-content" id="myModalLabel">
                   <div class="modal-header theme-bg">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                  <div class="modal-body">
                     <h3>Enter your current <span>Phone No.</span></h3>
                     <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" >
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <p><input type="text" class="form-control" name="studentmobile" value="<?=$student->phone?>" required placeholder="Enter Your E-mail" ></p>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="col-lg-12 text-center">
                              <div id="success"></div>
                              <button type="submit" class="btn modal-btn" >Save changes</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            </form>
         </div>
				<!-- model end -->
                  <h3 class="border_heading">Academic Details</h3>
                  <div class="row">
                     <table class="table table-striped table-academic">
                        <thead>
                           <tr>
                              <th>Education</th>
                              <th>Year</th>
                              <th>Subject</th>
                              <th>Board</th>
                              <th>Percentage</th>
                              <th>Marksheet</th>
                              <th>Certificate</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($education as $key => $value) {
                              ?> 
                           <tr>
                              <td><?php echo $value->education; ?></td>
                              <td><?php echo $value->year_passing; ?></td>
                              <td><?php echo $value->subject; ?></td>
                              <td><?php echo $value->board_name; ?></td>
                              <td><?php echo $value->percentage; ?> %</td>
                              <td>
                                 <?php  
                                    if ($value->year_passing!='') {
                                    
                                     ?>   
                                 <?php if ($value->marksheet_detail!='')  { ?>
                                 <strong><a href="<?php echo  base_url() ?>uploads/document/marksheet/<?php echo $value->marksheet_detail; ?>" style="color: #3c8dbc;margin-left: 15px" target="_blank">View</a></strong>
                                 <?php } else { ?>
                                 <a disabled>Not Available</a>
                                 <?php } ?>
                                 <?php } ?>
                              </td>
                              <td>
                                 <?php  
                                    if ($value->year_passing!='') {
                                    
                                     ?>
                                 <?php if ($value->certificate_detail!='')  { ?>
                                 <strong><a href="<?php echo  base_url() ?>uploads/document/marksheet/<?php echo $value->certificate_detail; ?>" style="color: #3c8dbc; margin-left: 15px" target="_blank">View</a></strong>
                                 <?php } else{?>
                                 <a >Not Available</a>
                                 <?php } ?>
                                 <?php } ?>
                              </td>
                           </tr>
                           <?php } ?>
                        </tbody>
                     </table>
                  </div>
            </section>
            <?php } elseif($usertype == "Parent") { ?>
              <div id="printablediv">
               <section class="panel">
                  <!-- user profile demo end -->
                  <div class="row">
                     <div class="col-md-12 col-sm-6">
                        <div class="card simple-card">
                           <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
                           </div>
                           <div class="avatar">
                              <a href="#"><?=img(base_url('uploads/images/'.$parentes->photo))?></a>
                           </div>
                           <div class="info1">
                              <div class="title">
                                 <h3><?=$parentes->name?></h3>
                              </div>
                              <div><i class="fa fa-envelope"></i> : <?=$parentes->email?></div>
                              <div><i class="fa fa-phone"></i> : <?=$parentes->phone?></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- user profile demo end -->
               </section>
            </div>
         <!-- <section class="panel">
               <div class="panel-body profile-view-dis">
                  <h3 class="border_heading">Personal Information</h3>
                  <div class="row">
                     <div class="profile-view-tab">
                        <p><span>Student Id </span>: <?=$student->username?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Enrollment No </span>: <?=$student->roll?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Date of Birth </span>: <?=date("d M Y", strtotime($student->dob))?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Gender </span>: <?=$student->sex?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Email </span>: <?=$student->email?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Mobile No. </span>: <?=$student->phone?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Address </span>: ?=$student->address?></p>
                     </div>
                   
                     <div class="profile-view-tab">
                        <p><span>Pin:</span>:  <?=$student->pin ?> </p>
                     </div>
                   
                     <div class="profile-view-tab">
                        <p><span>Nationality</span>: <?=$student->nationality?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Aadhar</span>: <?=$student->aadhar?></p>
                     </div>
                                       </div>
                  <h3 class="border_heading">Parents Information</h3>
                  <div class="row">
                     <div class="profile-view-tab">
                        <p><span>Father Name </span>: <?=$student->father_name?></p>
                     </div>
                     <div class="profile-view-tab">
                        <p><span>Mother Name </span>: <?=$student->mother_name?></p>
                     </div>
                  </div>
                  <h3 class="border_heading">Course Details</h3>
                  <div class="row">
                     <div class="profile1">
                        <table>
                           <tbody><tr>
                              <td class="bold1">Course</td>
                              <td>:</td>
                              <td><?=$class->classes?></td>
                           </tr>
                           <tr>
                              <td class="bold1">Mode</td>
                              <td>:</td>
                              <td><?php if($student->education_mode==1){
                                 echo "Year";
                                 
                                 }else{
                                 
                                 echo "Semester";
                                 
                                 }
                                 
                                 ?></td>
                           </tr>
                           <tr>
                        </tr></tbody></table>
                     </div>
                     <div class="profile1">
                        <table>
                           <tbody><tr>
                              <td class="bold1">Semester/Year</td>
                              <td>:</td>
                              <td><?= str_replace('_', ' ', $student->yearsOrSemester) ?></td>
                           </tr>
                           <tr>
                              <td class="bold1">Session</td>
                              <td>:</td>
                              <td><?php 
                                 echo $student->session;
                                 
                                 
                                 
                                 ?></td>
                           </tr>
                           <tr>
                        </tr>
                      </tbody>
                      </table>
                     </div>
                  </div>

       
      
            </div>
          </section> -->
            <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .boredr_none .nav-tabs{
   border-bottom: 0px solid #e8edef;
   }
   .nopading{
   padding-right:0px;
   padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #2057a8;
   color:#fff;
   }
   .theme_input_blue .form-control::-webkit-input-placeholder {
   color: #fff;
   }
   .theme_input_blue .form-control::-moz-placeholder { 
   color: #fff;
   }
   .theme_input_blue .form-control:-ms-input-placeholder { 
   color: #fff;
   }
   .theme_input_blue .form-control:-moz-placeholder { 
   color: #fff;
   }
   .forpostionReletive {
   position: relative;
   }
   .postionAbsoluter {
   position: absolute;
   top: 8px;
   }
   .action-layout ul li{
   display:inline-block;
   }
</style>


    <div class="modal modal-box-2 fade" id="changeemailbyprofessor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="margin-top: 55px;">
           <div class="modal-content" id="myModalLabel">
             <div class="modal-header theme-bg">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
            <div class="modal-body">
              <h3>Change Your <span>Email Id</span></h3>
              <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" method="post">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Enter Your Email-ID</label>
                      <input type="text" class="form-control" name="emailprofessor" value="<?php echo $this->session->userdata('email') ?>" required placeholder="Enter your Email-ID " >
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="submit" class="btn modal-btn">Update Now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 

      <div class="modal modal-box-2 fade" id="changephonebyprofessor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="margin-top: 55px;">
           <div class="modal-content" id="myModalLabel">
             <div class="modal-header theme-bg">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
            <div class="modal-body">
              <h3>Change Your <span>Mobile Number</span></h3>
              <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" method="post">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Enter Your Mobile Number</label>
                      <input type="text" class="form-control" name="mobilenumber" value="<?php echo $this->session->userdata('phone') ?>" required placeholder="Update your Mobile Number" maxlength="10"  >
                      <p class="help-block text-danger"></p>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="submit" class="btn modal-btn">Update Now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 