<style>
  .AddNewSubjetcsDetails{
    margin-bottom: 10px;
  }
  .margin20{
    margin-bottom: 20px;
  }
</style>
<?php 
              if($this->input->get('type')){
              $type = $_GET['type'];
              }else{
              $type = 'clg';
              }
?>

<div class="">

    <div class="row page-titles">
      <div class="col-md-6 align-self-center">
          <h3 class="text-themecolor"><?=$this->lang->line('panel_title')?>/ Syllabus</h3>
      </div>
      <div class="col-md-6 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("subject/index")?>"><?=$this->lang->line('menu_subject')?></a></li>

            <li class="active">View Syllabus</li>
          </ol>
      </div>          
    </div>

    <!-- /.box-header -->

    <!-- form start -->

     <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

            <div class="col-sm-12">
<?php
$uri = $this->uri->segment(3);
$this->db->where('subjectID',$uri);
$getdata = $this->db->get('subject')->row();
?>

              <center><h4><?php echo $getdata->subject; ?></h4></center>
<style type="text/css">
  .AddNewSyllabus{
    box-shadow: 0px 0px 0px 1px;
    padding: 22px;
  }
  .AddNewSyllabus{
    position:relative;
  }
  .delete_unit{
    position:absolute;
    right:0px;
  }
</style>
<div id="appendUnitDetails"> 
<?php if($subjects){ ?> 
 <?php foreach ($subjects as $key => $value) { ?>
    <div class='AddNewSubjetcsDetails AddNewSyllabus' id="unit_<?php echo $key ?>">
      <!-- use for progres bar -->
      <div class="delete_unit">Progress 10%</div>
      <h3>Unit <?php echo $value['unit_code'] ?></h3>
   <div class='col-sm-6'>
      <div class=''>
        <?php echo $value['unit_name'] ?>
      </div>
   </div>
   <div class="col-sm-12">&nbsp;</div>
 <?php 
 if($value['topic']){
foreach ($value['topic'] as $keys => $values) { ?>
   <div class='col-sm-3'>
      <div class=''>
    <?php echo $values['topic_name']; ?>
      </div>
   </div>
<?php } } ?>   
   <div class='clearfix'></div>
</div>
<?php } ?>
<?php }else{ ?>
<h1>Syllabus not Found</h1>
<?php } ?>

</div>
<style type="text/css">
.pdf_sylnus{
  padding:10px 10px 10px 10px;
}
.biit_dd{
  margin:3%;
}
               </style>
            </div> 

        </div>

    </div>

</div>

</div>

</div>
</div>


<script type="text/javascript">
           
                $('#upload_file_multiple').on('click', function () {
                  var id =  "<?php echo $this->uri->segment(3) ?>";
                    var form_data = new FormData();
                    var ins = document.getElementById('multiFiles').files.length;
                    for (var x = 0; x < ins; x++) {
                        form_data.append("files[]", document.getElementById('multiFiles').files[x]);
                    }
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/UploadFile/"+id, // point to server-side PHP script 
                        dataType: 'text', // what to expect back from the PHP script
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#afx_khjk').html(response); // display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_khjk').html(response); // display error response from the PHP script
                        }
                    });
                });
      
        </script>
        <script type="text/javascript">
           
                function delete_pdf_sylabus(id){
                

                  var confirmation = confirm("are you sure you want to remove the item?");

                  var subjectID =  "<?php echo $this->uri->segment(3) ?>";
                  if(confirmation){
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/delete_pdf_sylabus/"+id, // point to 
                        data:{subjectID:subjectID},
                        type: 'post',
                        success: function (response) {
                          console.log(response);
                            $('#afx_khjk').html(response); // display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_khjk').html(response); // display error response from the PHP script
                        }
                    });
            
            }
          }
      
        </script>
<script type="text/javascript">
  function delete_topic(id,id_forHide){
  $.ajax({
    type: "POST", 
  url:base_url+'syllabus/DeleteTopic',
  data:{id:id},
  success: function(response) {
    console.log(response);
$("#"+id_forHide).hide();

        }

  });
    }
</script>

<script type="text/javascript">
  function delete_unit(id,id_forHide){
  $.ajax({
    type: "POST", 
  url:base_url+'syllabus/DeleteUnit',
  data:{id:id},
  success: function(response) {
    console.log(response);
$("#"+id_forHide).hide();

        }

  });
    }

function unitEdit(name,colum_name,id,val){
 $.ajax({
  type: "POST", 
  url:base_url+'syllabus/unitEdit',
  data:{colum_name:colum_name,id:id,val:val},
  success: function(response) {
    console.log(response);
        }

  });
}

function addTopicsMore(unitID,subjectID,val){
  var topic_name  =$('#'+val).val();  
  $.ajax({
    type: "POST", 
  url:base_url+'AjaxController/addTopicsMore',
  data:{val:topic_name,subjectID:subjectID,unitID:unitID},
  success: function(response) {
$('#appendUnitDetails').html(response);
 location.reload(true); 

        }

  });

}

</script>

<style type="text/css">
button, input, optgroup, select, textarea{    
  
  color: inherit;
}
</style>

<script type="text/javascript"> 
function AddUnit(){

  $.ajax({
    type: "POST", 
  url:base_url+'AjaxController/AddUnit',
  data:$('#form_unit').serialize(),
  success: function(response) {
 location.reload(true);


        }

  });


}

</script>