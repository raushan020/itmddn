
<div class="">

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-cogs"></i> Exam Setting </h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">Exam Setting</li>
          </ol>
      </div>
    </div>

   <!-- /.box-header -->

    <!-- form start -->

     <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
<form class="" action="<?=base_url('exam/dau_quiz') ?>" method="post">
            <div class="col-md-3">
              <h4>Duration</h4>
              <input type='text' class='form-control' id='subjectCodeClear' onBlur='Comman_field_quiz_setting(this,"duration", <?php echo $quiz_settings->id ?> ,$(this).val())' placeholder='Duration' name='duration' value="<?php echo $quiz_settings->duration ?>" >
            </div>
            <div class="col-md-1">  
            </div>
            
            <div class="col-md-3">
              <h4>Correct Score</h4>
              <input type='text' class='form-control' id='subjectCodeClear' onBlur='Comman_field_quiz_setting(this,"correct_score", <?php echo $quiz_settings->id ?> ,$(this).val())' placeholder='Correct Score' name='subject' value="<?php echo $quiz_settings->correct_score ?>" >
            </div>
            <div class="col-md-1">
              </div>

            <div class="col-md-3">
              <h4>Incorrect Score</h4>
              <input type='text' class='form-control' id='subjectCodeClear' onBlur='Comman_field_quiz_setting(this,"incorrect_score", <?php echo $quiz_settings->id ?> ,$(this).val())' placeholder='Incorrect Score' name='subject' value="<?php echo $quiz_settings->incorrect_score ?>" >
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-12"><br></div>
            <div class="col-md-3">
              <h4>Start Date</h4>
              <input type="text" name="start_date" onBlur='Comman_field_quiz_setting(this,"start_date", <?php echo $quiz_settings->id ?> ,$(this).val())' id="" class="form-control form-control datetimepicker_quiz_start" value="<?php echo $quiz_settings->start_date ?>" placeholder="Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )"  required >
            </div>
            <div class="col-md-1">  
            </div>
            <div class="col-md-3">
              <h4>End Date</h4>
              <input type="text" name="end_date" id="datetimepicker_quiz_end" onBlur='Comman_field_quiz_setting(this,"end_date", <?php echo $quiz_settings->id ?> ,$(this).val())' value="<?php echo $quiz_settings->end_date ?>" class="form-control form-control datetimepicker_quiz_start" placeholder="End Date (Exam can be attempted before this date. eg. 2017-12-31 23:59:00 )"  required >
            </div>
            <div class="col-md-1">  
            </div>
            

              <div class="col-md-3">
              <h4>Video Required</h4>
              <input type="radio" name="video" id="video1" value="1" checked><label for="video1">YES</label>
              &nbsp;&nbsp;<input type="radio" name="video" value="0" id="video2"><label for="video2">NO</label>
            </div>
            

                <!-- <label for="courseName" class="col-md-2 col-form-label">Video Required</label>
                <div class="col-md-10">
                  <div class="col-sm-3">
                    <input type="radio" name="video" id="video1" value="1" checked><label for="video1">YES</label>
                  </div>
                  <div class="col-sm-3">
                    <input type="radio" name="video" value="0" id="video2"><label for="video2">NO</label>
                  </div>
                  <div class="clearfix"></div>
                </div> -->
             <div class="col-md-12" style="padding-top: 25px; text-align: center;">
               
                  <input type="submit" name="submit" value="Update Date & Time" class="btn btn-success add-btn"  required >
                
             </div>
                
                
           
           </form>
       
           </div>
    </div>
</div></div></div>


