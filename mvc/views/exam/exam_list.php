<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<div class="container-fluid">
<!-- <script type="text/javascript" >

  window.stop();

</script> -->

<!-- <script>

   history.pushState(null, null, location.href);

   window.onpopstate = function () {

       history.go(1);

   }; 

</script> -->

<script>
   $(document).ready(function() {

       function disableBack() { window.history.forward() }



       window.onload = disableBack();

       window.onpageshow = function(evt) { if (evt.persisted) disableBack() }

   });

</script>

<div class="">
    <div class="row page-titles">
        <div class="col-md-4 col-xs-12 align-self-center">
        <h3 class="text-themecolor" style="font-weight: 500;"></i> <?php 
        if ($this->uri->segment(3)==2) {
        echo "<i class='fa fa-tasks'></i>"."&nbsp;"."Assignment";
        }else{
         echo "<i class='ti ti-ruler-pencil'></i>"."&nbsp;"."Exam schedule";
        }
        ?></h3>
        </div>
        <div class="col-md-4 col-xs-12" style="text-align: center;">
            <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Exam Tutorial</a></div>
        <div class="col-md-4 col-xs-12 align-self-center">
            
            <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>  /
             <?php 

        if ($this->uri->segment(3)==2) {

        echo "<i class='active'></i>"."&nbsp;"."Assignment";

        }else{

         echo "<i class='active'></i>"."&nbsp;"."Exam";

        }



         ?>
        <!-- <li class="active"><?=$this->lang->line('panel_title')?></li> -->

            </ol>

        </div>

    </div>







    <!-- form start -->

<div class="container-fluid">

        <div class="row">

        <div class="card">

            <div class="card-body">



    <div class="box-body">



        <div class="row">





<div class="col-sm-4"><?php
if(isset($_COOKIE['massege_co'])) {
   echo $_COOKIE['massege_co'];
}
?></div>
            </div>

        </div>

                </div>

            <!--  -->

            <div class="col-sm-12">

                <?php 

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Professor" || $usertype == "Parent") {

                ?>

                <div class="col-sm-12">

                    <h5 class="page-header">

                        <ul class="pannleSearchHeading">

                            <li>

                                <a href="<?php echo base_url('exam/add') ?>">

                                    <i class="fa fa-plus"></i> 

                                    <?=$this->lang->line('add_title')?>

                                </a>

                            </li>

                            <li><a style="cursor:pointer;" onclick="ResetAllfilter()">Reset All Filters</a></li>

                        </ul>

                   </h5>

                </div>

                <?php } ?> 

<?php

if ($data_single_student->exam_status==1) {

 ?> 

                <table id="exam_listTables" class="table table-striped table-bordered table-hover dataTable no-footer">

                    <thead>

                        <tr>

                            <th>S.No.</th>

                            <th>Paper Code</th>

                            <th>Exam Name</th>

                            <th>Start Date</th>

                            <th>End Date</th>

                            <th><?=$this->lang->line('action')?></th>

                        </tr>


                    </thead>
                    <tbody>
                      <tr>
                      </tr>
                    </tbody>
                </table>

               
    <?php } else{ ?>
        <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
        <p>Maybe your search was too specific, please try searching with another term.</p>
        <img src="<?php echo base_url() ?>uploads/images/crying.png">
        </div>
         <?php } ?>
            </div> 
        </div>


    </div><!-- Body -->
</div><!-- /.box -->
</div>

   <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <video width="100%"  src="<?php echo base_url('assets/Exam_voiceover.mp4') ?>" controls></video>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

 <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>
  <script>var $jq = jquery.noConflict(true);</script>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/AjaxDatatableStudent.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>