<div class="">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-question"></i> Question</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?>Question</li>
            </ol>
        </div>
    
     </div>

    <!-- form start -->

    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <div class="col-sm-12">
                        <?php 
                            $usertype = $this->session->userdata("usertype");
                            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
                        ?>

                        <div class="col-sm-12">                                       
                            <h5>
                                <ul class="pannleSearchHeading">
                                    <li>
                                        <a class="btn-top check-all btn bg-success pull-right" href="<?php echo base_url('exam/questionadd/'.$this->uri->segment(3)) ?>"><i class="fa fa-plus"></i> 
                                            
                                            Add Question
                                        </a>
                                    </li>                   
                                </ul>
                            </h5>
                        </div>
                        <?php } ?>



                        <div id="hide-table">

                            <table id="questionTables" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-lg-2">S. No.</th>
                                        <th class="col-lg-2">Question Title</th>
                                        <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                    </tr>

                                </thead>

                                <tbody>                              

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
<!-- jquery  -->

<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('classes/index')?>",

            data: "classesID=" + classesID,

            dataType: "html",

            success: function(data) {

                 location.reload();

            }

        });

    });

</script>
<script type="text/javascript">
    function ResetCourses(){
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetCourses')?>",

            data:{ResetCourses:'ResetCourses'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>

<script type="text/javascript">

    $('#durationID').change(function() {

        var durationID = $(this).val();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('classes/index')?>",

            data: "durationID=" + durationID,

            dataType: "html",

            success: function(data) {

                 location.reload();

            }

        });

    });

</script>
<script type="text/javascript">
    function ResetdurationID(){
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetdurationID')?>",

            data:{durationID:'durationID'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>

<script type="text/javascript">
    function ResetAllfilter(){
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetAllfilter')?>",

            data:{durationID:'durationID'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>

