<?php 
$uri =  $this->uri->segment(3);
$uriFor =  $this->uri->segment(4);

 ?>

<style type="text/css">
.login-panel{
  margin-top: -6%;
  box-shadow: 0px 0px 0px 0px rgba(64, 65, 67, 0.04);
}
.login-panel .form-control{
  height: 35px;
}

</style>

  <div class="">
  <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"><i class="fa fa-plus"></i> Add Question </h3>
    </div>
    <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
        <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

        <li><a href="<?=base_url("exam/index")?>"><?=$this->lang->line('menu_exam')?></a></li>

        <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_exam')?></li>
      </ol>
    </div>
  </div>
  <!-- /.box-header -->

    <!-- form start -->

  <div class="container-fluid">
    <a href="<?=base_url("exam/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
     <!-- <a href="<?=base_url("exam/manageQuestion/".$uri)?>"><button class="btn btn-info" style="margin-bottom: 10px;">BACK</button></a> -->

    <div class="row">

      <div class="card">

        <div class="card-body">

          <div class="col-sm-8 col-md-4" style="border-right: 1px solid #efefef;">

            <p>Field are required with <span class="red-color">*</span></p>
            <h3 class="border_heading">Add question information</h3>
            <form method="post" action="<?php echo site_url('exam/questionadd/'.$uri.'/'.$uriFor);?>" enctype="multipart/form-data"> 

              <!-- <div class="col-md-8">   -->              

                <div class="login-panel">
                  <!-- <div class="panel-body">   -->
  
                    <?php 
                    if($this->session->flashdata('message')){
                      echo $this->session->flashdata('message');  
                    }
                    ?>   
    
                    <div class="form-group">      
                      <?php echo $this->lang->line('multiple_choice_single_answer');?>
                    </div>

                    <div class="form-group">   
                      <label><?php echo $this->lang->line('select_category');?></label> 
                      <select class="form-control" name="question_type" onchange="all_type($(this).val())" id="append">
                        <option value="">Select</option>
                        <option value="1" <?php if($uriFor=='1'){echo "selected";} ?>>Single Choice Question</option>
                        <option value="2" <?php if($uriFor=='2'){echo "selected";} ?>>Multiple Choice</option>
                        <option value="3" <?php if($uriFor=='3'){echo "selected";} ?>>Matching Question</option>
                        <option value="4" <?php if($uriFor=='4'){echo "selected";} ?>>Short Answer</option>
                        <option value="5" <?php if($uriFor=='5'){echo "selected";} ?>>long Answer</option>
                      </select>
                    </div>
      
                    <div class="form-group">   
                      <label for="inputEmail">Questions</label> 
                      <b onclick="call_editor()">Add Editor</b>
                      <div class="tests_re">
                        <textarea name="question" class="form-control" ></textarea>
                      </div>
                      <br>
                      <input type="file" class="form-control" name="userfile">
                    </div>
<!-- <script type="text/javascript">
 function call_editor(){
  alert('ju');
                  var sa   = `<textarea name="question"  class="form-control" id="editor"></textarea>`;
                  $('.tests_re').html(sa);
          

 }
</script> -->

                    <div id="all_type">

                      <?php 
                        if ($this->uri->segment(4)==1) { 
                      ?>


                      <div class='form-group'>                       
                        <div class='' id='hereToadd1'>
                          <ul>
                            <li><p>Option 1</p>
                              <input type='radio' id="op1" name='score' value='0' id='multiValue' checked>
                              <label for='op1' id='multiValue'  >Select Correct Option</label>
                              <br><textarea name='option[]' class='form-control' id='multiValue'></textarea><br>
                              <input type="file" id='multiValue' class="form-control" name="image[]">
                            </li>                                      
                            <div class='btn-group'>
                              <button type='button' class='btn btn-primary addValue_1'>+</button>
                            </div>                                      
                          </ul>
                        </div>                         
                      </div>

                      <?php  } elseif($this->uri->segment(4)==2){ ?>

                      <div class='form-group'>                        
                        <div class='' id='hereToadd2'>
                          <ul>
                            <li>
                              <label for='inputEmail' id='multiValue'  >Option 1</label> <br>
                              <input type="checkbox" name="score[]" value="0" checked="">Select Correct Option
                              <br><textarea  name='option[]'  class='form-control' id='multiValue'  ></textarea>
                            </li>
                            <div class='btn-group'>
                              <button type='button' class='btn btn-primary addValue_2'>+</button>
                            </div> 
                              
                          </ul>
                        </div> 
                                                   
                      </div>


          <?php } elseif($this->uri->segment(4)==3){ ?>

          
          <div class='form-group'>            
            <div class='' id='hereToadd3'>
              <ul>
                <li>                 
                  <div class="form-group">  
                  <label for="inputEmail" >Option </label> <br>
                  <input type="text" class="form-control" name="option[]" value=""> = <input type="text" class="form-control" name="option2[]" value=""> 
                  </div>
                </li>
                <li>
                  <div class='btn-group'>
                    <button type='button' class='btn btn-primary addValue_3'>+</button>  
                  </div> 
                </li>
              </ul>                                            
            </div> 
          </div>

            <?php  } elseif($this->uri->segment(4)==4){ ?>
            <div class="form-group">   
              <label for="inputEmail"  >Description</label> 
              <textarea  name="description"  class="form-control"></textarea>
            </div>
            <div class="form-group">   
              <label for="inputEmail"  >Answer in one or two words (comma separated for multiple possibilities.) Not case sensitive</label> <br>
              <input type="text" name="option[]"  class="form-control"  value=""> 
            </div>
            <?php  } elseif($this->uri->segment(4)==5){ ?>
            <div class="form-group">   
              <label for="inputEmail"  >Description</label> 
              <textarea  name="description"  class="form-control"></textarea>
            </div>
            <?php } ?>
          </div>
    
          <input type="hidden" name="quid" value="<?php echo $this->uri->segment(4)?>" >
          <button class="add-btn" type="submit"><!-- <?php echo $this->lang->line('submit');?> --> Submit </button>
            <div>&nbsp;</div>
        </div>
      </div>  
  <!-- </div> -->
<!-- </div> -->
      </form>
      <div class="col-sm-8 col-md-8">
                       <!--  <?php 
                            $usertype = $this->session->userdata("usertype");
                            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
                        ?>

                        <div class="col-sm-12">                                       
                            <h5>
                                <ul class="pannleSearchHeading">
                                    <li>
                                        <a class="btn-top check-all btn bg-success pull-right" href="<?php echo base_url('exam/questionadd/'.$this->uri->segment(3)) ?>"><i class="fa fa-plus"></i> 
                                            
                                            Add Question
                                        </a>
                                    </li>                   
                                </ul>
                            </h5>
                        </div>
                        <?php } ?> -->



                        <div id="hide-table">

                            <table id="questionTables" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-lg-2">S. No.</th>
                                        <th class="col-lg-2">Question Title</th>
                                        <th class="col-lg-2">Correct Answer</th>
                                        <th class="col-lg-2"><?=$this->lang->line('action')?></th>
                                    </tr>

                                </thead>

                                <tbody>                              

                                </tbody>

                            </table>

                        </div>

                    </div>

      </div>    

    </div>

  </div>

</div></div>

<script type="text/javascript">

$('#date').datepicker();

</script>

<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesID").val();
       var  sub_CourseID  = $("#sub_CourseID").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('exam/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>

<script type="text/javascript">
      var count = 1;
      var intcr = 0;
  $(document).on('click','.addValue_1',function(e){
    e.preventDefault();

    count++;
    intcr++;
        // var html =  
        $("<ul><li> <p>Option "+count+"</p>\
          <input type='radio' name='score' value='"+intcr+"' id='opt"+count+"'> \
          <label for='opt"+count+"' id='multiValue[]'>Select Correct Option</label>\
          <br><textarea  name='option[]'  class='form-control' id='multiValue[]'  ></textarea><br><input type='file' id='multiValue' class='form-control' name='image'></li><div class='btn-group'><button type='button' class='btn btn-primary' id='deleteParent_1'>-</button><button type='button' class='btn btn-primary addValue_1'>+</button></div></ul>").appendTo('#hereToadd1');
        // $("#hereToadd").append(html);     
    }); 
    $(document).on('click','#deleteParent_1',function(e){
      count--;
      intcr--;
      e.preventDefault();
      $(this).parent().parent().remove();
      // $(this).parent().parent().parent().remove();
    });

  // 2
  $(document).on('click','.addValue_2',function(e){
    e.preventDefault();   
    count++;
    intcr++;
        // var html =  
      $("<ul><li><label for='inputEmail' id='multiValue[]'> Option" +(count)+ "</label> <br>\
  <input type='checkbox' name='score[]' value='"+(intcr)+"' id='multiValue[]'>Select Correct Option\
   <br><textarea  name='option[]'  class='form-control' id='multiValue[]'  ></textarea></li><div class='btn-group'><button type='button' class='btn btn-primary' id='deleteParent_2'>-</button><button type='button' class='btn btn-primary addValue_2'>+</button></div></ul>").appendTo('#hereToadd2');
  // $("#hereToadd").append(html);     
}); 
    $(document).on('click','#deleteParent_2',function(e){ 
       count--;
       intcr--;
        e.preventDefault();
        $(this).parent().parent().remove();
        // $(this).parent().parent().parent().remove();
    });

// 3
$(document).on('click','.addValue_3',function(e){
    e.preventDefault();
    count++;
// var html =  
var html  = (`<ul><li><div class="form-group">  
<label for="inputEmail"  >Option </label>
<input type="text" name="option[]" class='form-control' value="" /> = <input type="text" class='form-control' name="option2[]" value=""  /> 
</div></li><div class='btn-group'><button type='button' class='btn btn-primary' id='deleteParent_3'>-</button><button type='button' class='btn btn-primary addValue_3'>+</button></div></ul>`);
  $("#hereToadd3").append(html);    
    });
    $(document).on('click','#deleteParent_3',function(e){
      count--;
        e.preventDefault();
        $(this).parent().parent().remove();
        // $(this).parent().parent().parent().remove();
    });
</script>



<script type="text/javascript">

function all_type(val)
{
var count = 1;
count++;
if (val==1) {
 window.location.href = "<?php echo base_url() ?>exam/questionadd/<?php echo $uri ?>/1";
}
else if(val==2){
 window.location.href = "<?php echo base_url() ?>exam/questionadd/<?php echo $uri ?>/2";
}
else if(val==3){
   window.location.href = "<?php echo base_url() ?>exam/questionadd/<?php echo $uri ?>/3";
}
else if(val==4){
 window.location.href = "<?php echo base_url() ?>exam/questionadd/<?php echo $uri ?>/4";
}
else{
 window.location.href = "<?php echo base_url() ?>exam/questionadd/<?php echo $uri ?>/5";
}
}
</script>
<script type="text/javascript">
  $(document).ready(function(){
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")=="green"){
                $(".box").not(".green").hide();
                $(".green").show();
            }
            if($(this).attr("value")=="blue"){
                $(".box").not(".blue").hide();
                $(".blue").show();
            }
        });
    });
    // $(document).on('click','.addValue',function(e){
    //     e.preventDefault();

    // var count = 1;
    
    // count++;
    //     // var html =  
    //     $("<ul><li><label for='inputEmail' id='multiValue[]'> Option +(count)+ </label> <br>\
    //                                                   <input type='radio' name='score' value=' id=multiValue[]'> Select Correct Option\
    //                                                   // <br><textarea  name='option[]'  class='form-control' id='multiValue[]'  ></textarea></li><div class='btn-group'><button type='button' class='btn btn-primary' id='deleteParent'>-</button><button type='button' class='btn btn-primary addValue'>+</button></div></ul>").appendTo('#hereToadd');
    //                                                   // $("#hereToadd").append(html); 

        
    // });
    
    // $(document).on('click','#deleteParent',function(e){
      
    //     e.preventDefault();
    //     $(this).parent().parent().remove();

    //     // $(this).parent().parent().parent().remove();
    // });
</script>


<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('classes/index')?>",

            data: "classesID=" + classesID,

            dataType: "html",

            success: function(data) {

                 location.reload();

            }

        });

    });

</script>
<script type="text/javascript">
    function ResetCourses(){
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetCourses')?>",

            data:{ResetCourses:'ResetCourses'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>

<script type="text/javascript">

    $('#durationID').change(function() {

        var durationID = $(this).val();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('classes/index')?>",

            data: "durationID=" + durationID,

            dataType: "html",

            success: function(data) {

                 location.reload();

            }

        });

    });

</script>
<script type="text/javascript">
    function ResetdurationID(){
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetdurationID')?>",

            data:{durationID:'durationID'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>

<script type="text/javascript">
    function ResetAllfilter(){
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetAllfilter')?>",

            data:{durationID:'durationID'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>
<script type="text/css">
  .box{
padding: 0px;
display: none;
margin-top: 10px;
clear: both;

}
.green{}
.green ul li{
    float: left;
}
.green ul li:nth-child(1){
    width: 93%;
    padding-right: 5px;
}
.green ul li:nth-child(2){
    width: 7%;
}
.green ul li .btn {
    border: 1px solid transparent;
    border-radius: 0 !important;
    cursor: pointer;
    display: inline-block;
    font-size: 20px;
    font-weight: 400;
    line-height: 26px;
    margin-left: 1px !important;
    padding: 5px 12px;
}
</script>
<script>
    initSample();
</script>

