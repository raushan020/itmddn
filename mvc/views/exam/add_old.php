
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("exam/index")?>"><?=$this->lang->line('menu_exam')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_exam')?></li>
            </ol>
        </div>
    </div>



<div class="">

    

    <!-- form start -->

    <div class="container-fluid">
       <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-9">
<p>Field are required with <span class="red-color">*</span></p>
<h3 class="border_heading">Exam Information</h3>
<form class="form-horizontal" role="form" method="post">
   <div class="col-md-11">
      <div class="">
        <div class="panel-body">
          <?php 
            if(form_error('classesID')) 
              echo "<div class='form-group has-error' >";
            else     
              echo "<div class='form-group' >";
          ?>
        </div>

                    <div class="form-group">
                      <label for="classesID" class="">Course<span class="red-color">*</span></label>
                      <div class="">
                        <select name="classesID" required onchange="ajaxGet_subCourses_exam($(this).val())" id="classesID" class="form-control" value="<?php echo set_value('classesID'); ?>">
                        <option value="">Select Course</option>
                        <?php foreach($classes as $courses){ ?>
                          <option value="<?php echo $courses->classesID ?>"><?php echo $courses->classes ?> </option>
                          <?php } ?>
                        </select>                       
                      </div>
                    </div>
                    <span class="col-sm-4 showLoaderSubcour">
                      <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                    </span>
                    <span class="col-sm-4 control-label">
                      <?php echo form_error('classesID'); ?>
                    </span>
                    <div id="subCourseID">
    
                    </div>    
                    <div id="yearAndSemester">
                      
                    </div>

                    <div id="subjectID">
                    
                  </div>
            <div class="form-group">     
               <label for="quiz_name" class="">Exam Name<span class="red-color">*</span></label> 
               <input type="text"  name="quiz_name" id="quizID" class="form-control" placeholder="Quiz Name"  required autofocus>
            </div>
            <div class="form-group">     
               <label for=""  >Description</label> 
               <textarea   name="description"  class="form-control tinymce_textarea" ></textarea>
            </div>
            <div class="form-group">     
               <label for="">Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )</label> 
               <input type="text" name="start_date"  class="form-control datetimepicker_quiz_start" placeholder="Enter Start Date"   required >
            </div>
            <div class="form-group">     
            <label for="">End Date (Exam can be attempted before this date. eg. 2017-12-31 23:59:00 )</label> 
               <input type="text" name="end_date" class="form-control datetimepicker_quiz_start" placeholder="Enter End Date"   required >
            </div>
            <div class="form-group">     
               <label for=""  >Duration (in min.)</label> 
               <input type="text" name="duration"  value="<?php echo $quiz_settings->duration ?>" class="form-control" placeholder="Duration (in min.)"  required  >
            </div>
<!--             <div class="form-group">     
               <label for=""  >Allow Maximum Attempts</label> 
               <input type="text" name="maximum_attempts"  value="10" class="form-control" placeholder="Allow Maximum Attempts"   required >
            </div> -->
    
            <div class="form-group">     
               <label for=""  >Correct Score</label> 
               <input type="text" name="correct_score"  value="<?php echo $quiz_settings->correct_score ?>" class="form-control" placeholder="Correct Score"   required >
            </div>
            <div class="form-group">     
               <label for=""  >In Correct Score</label> 
               <input type="text" name="incorrect_score"  value="<?php echo $quiz_settings->incorrect_score ?>" class="form-control" placeholder="In Correct Score"  required  >
            </div>
            <div class="form-group">
              <label for="courseName" class="col-md-2 col-form-label">Exam Type</label>
              <div class="col-sm-4">
                <input type="radio" name="extype" id="extype1" value="1" checked><label for="extype1">Main</label>
              </div>
              <div class="col-sm-4">
                <input type="radio" name="extype" value="2" id="extype2"><label for="extype2">Assignment</label>
              </div>
              <div class="clearfix"></div>
              
            </div>

            <center><button class="btn btn-success add-btn" type="submit" >Next</button></center>
      
         </div>
      </div>
   </div>
</form>

            </div>    

        </div>

    </div>

</div>



<script type="text/javascript">

$('#date').datepicker();

</script>

<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesID").val();
       var  sub_CourseID  = $("#sub_CourseID").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('exam/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>




