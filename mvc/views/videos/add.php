<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
  <!-- <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
  <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css"> -->
<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i> Add Video </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("videos/index")?>">Video</a></li>

                <li class="active"><?=$this->lang->line('menu_add')?></li>

            </ol>
        </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <a href="<?=base_url("videos")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
      <div class="row">
        <div class="">
          <div class="card">
            <div class="card-body">
              <div class="col-sm-12">
                <form action="<?php echo base_url() ?>videos/UploadVideos" role="form" method="post" enctype="multipart/form-data" class=""> 
                <div class="form-row">
                  <div class="form-group">
                <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Course</label >
                <div class="col-md-6">
                  <select name="classesID" required onchange="ajaxGet_subCourses($(this).val())" id="classesIDForVideo" class="form-control" value="<?php echo set_value('classesID'); ?>" >
                    <option value="">Select Course</option>
                      <?php foreach($classes as $courses){ 
                      if ($courses->classesID==$this->session->userdata('FilterVideoclassesID')) {
                           $selected =  "Selected";
                       }else{
                         $selected =  "";
                       }
                        ?>

                      <option value="<?php echo $courses->classesID ?>" <?php echo $selected ?>><?php echo $courses->classes ?></option>
                    <?php } ?>
                  </select> 
                </div>
              </div>
          </div>
           <!-- <div class="form-row">
            <div class="form-group">
             <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Sub Course</label >
             <div class="col-md-6">
     
                 <select id="subCourseID" class="form-control">
                    <option value="">Select</option>
                    <?php  foreach ($subCourses as $key => $value) {
                       if ($value->sub_coursesID==$this->session->userdata('FilterVideosubCourseID')) {
                           $selected =  "Selected";
                       }else{
                         $selected =  "";
                       }
                       ?><?php echo set_value('subjectvideo'); ?>
                    <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                    <?php } ?>
                 </select>
              </div>
            </div>
        </div> 
 -->
    
          <div class="form-row">
            <div class="form-group">
             <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Year or Semester</label >
             <div class="col-md-6">
                 <select class='form-control' name='yearsOrSemester' onchange='ajaxGet_subCourses($(this).val())' id='yos_filter' value=''> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterVideoyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterVideoyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
              </div>
            </div>
        </div>



        <div class="form-row">
            <div class="form-group">
              <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Subject</label >
              <div class="col-md-6">
                <select class='form-control' name='subjectvideo' onchange='ajaxGet_subject($(this).val())' id='subjectIDforvideo' value=''> 
                  <option>Select</option>      
                  <?php echo $this->session->userdata('FilterVideosubjectID'); ?>
                    <?php  foreach ($subject as $key => $value) {

                       if ($value->subjectID===$this->session->userdata('FilterVideosubjectID')) {
                           $selected =  "";
                       }else{
                         $selected =  "";
                       }
                       ?>
                    <option value="<?php echo $value->subjectID ?>" <?php echo $selected ?>><?php echo $value->subject ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Unit</label>
              <div class="col-md-6">
                <select class='form-control' name='unitvideo' id='unitIDforvideo'> 
                  <option>Select</option>      
                    <?php $this->session->userdata('FilterVideoUnitID'); ?>
                   
                  </select>
                </div>
              </div>
                 <input type="hidden" id="filename_video" name="filename" >
                                <div class='form-inline'>
                                        <label for="course_image" class="col-sm-3 control-label">
                                    Upload Lecture Video 
                                        </label>
                                        <div class="col-sm-7">
                                            <div id="body">
                                                <div id="filelist">Your browser doesn't have Flashs, Silverlight or HTML5 support.</div>
                                                <div id="container">
                                                    <div class="form-group">
                                                        <a id="uploadFile" name="uploadFile" href="javascript:void(0);" class="btn btn-default" style="padding-top: 7px;">Select file</a>
                                                        <a id="upload" href="javascript:void(0);" class="btn btn-primary">Click here to upload file</a>
                                                        <label>Need to convert? <a href="http://www.convertfiles.com/" target="blank" style="border-bottom: 1px solid red;">Click here</a></label>
                                                        <label style="color: red">Max file size: 1GB. Only MP4 supported</label>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="file_ext" name="file_ext" value="">
                                                <div id="console"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- youtube start -->
                                    <div class="form-row">
                                      <div class="form-group">
                                        <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label"></label >
                                        <div class="col-md-6" style="text-align: center;">
                                          --------------------OR--------------------
                                        </div>
                                      </div>
                                    </div> 

                                    <div class="form-row">
                                      <div class="form-group">
                                        <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Youtube Link</label >
                                        <div class="col-md-6">
                                          <input class='form-control' name='youtube_scr' id='youtube_scr' value='' placeholder="Youtube Link">
                                          <label>Got to YouTube. Open the video. Click SHARE. Click COPY. Paste the short URL here.</label>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- youtube end -->
                                    <div class="form-group" style="margin-top: 20px;">
                                      <center><button type="submit" id="upload_videos_button" class="btn btn-success btn-md mrg for_margR">Submit</button></center>
                                    </div>                               

                                
                                </form>

                            </div>

                        </div>

                   </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>

<script type="text/javascript">

jq('#date').datepicker();

jq('#notice').jqte();

</script>
<script type="text/javascript">
    function ajaxGet_subCoursesExcel(id){
      
 $('.showLoaderSubcour').show();
    $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Get_subCourses')?>",
        data:{"id":id},
        success: function(response) {
            console.log(response);
            $("#subCourseIDExcel").html(response);
            $('.showLoaderSubcour').hide();
        }
            });
}
</script>

<script type="text/javascript">
    $(document).ready(function(){ 
    $("input[name=cod]").change(function() {
        var test = $(this).val();
        if (test==1) {
    $("#openContainer").show();
        }else{
      $("#openContainer").hide();  
        }
    
    }); 
});
</script>

<script type="text/javascript">
  function GetEntryTypeData(){
       var  classesID  = $("#classesIDExcel").val();

       var  sub_CourseID  = $("#sub_CourseIDExcel").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>

<script type="text/javascript">
    $('#yearSemesterID').change(function() {
        var yearSemesterID = $(this).val();
        
        $.ajax({
            type: 'POST',
            url: "<?=base_url('notice/index')?>",
            data: "yearSemesterID=" + yearSemesterID,
            dataType: "html",
            success: function(data) {
                 location.reload();
            }
        });
    });
</script>

<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>




<script type="text/javascript">
   $('#classesIDForVideo').change(function() {
       var classesID = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "classesID=" + classesID,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#subCourseID').change(function() {
   
       var subCourseID = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "subCourseID=" + subCourseID,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>

<script type="text/javascript">
   $('#yos_filter').change(function() {
   
       var yearSemesterID = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "yearSemesterID=" + yearSemesterID,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#subjectIDforvideo').change(function() {
   
       var subjectIDforvideo = $(this).val();

    var option_user_selection = $("#subjectIDforvideo option:selected").text();;

   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: {subjectIDforvideo:subjectIDforvideo,option_user_selection:option_user_selection},
   
               dataType: "html",
   
               success: function(data) {
   
                    // location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#professorID').change(function() {
   
       var professorIDvideo = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "professorIDvideo=" + professorIDvideo,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#unitIDforvideo').change(function() {   
       var unitIDforvideo = $(this).val();   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "unitIDforvideo=" + unitIDforvideo,
   
               dataType: "html",
   
               success: function(data) {
   
                    // location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesIDForVideo").val();

       var  sub_CourseID  = $("#subCourseID").val();
       var  semester_idSelect  = $("#yos_filter").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function ajaxGet_subject($a)
  {
    var subjectID=$("#subjectIDforvideo").val();
    $.ajax({
     type: "POST",
     url:"<?=base_url('ajaxControllerQuestion/Getunits')?>",
     data:{"subjectID":subjectID},
     success: function(response)
     {
      // alert(response);
        $("#unitIDforvideo").html(response);
        // alert(response);
     }
    });
  }
</script>
    <script type="text/javascript">
      var  BASE_URL = "<?php echo base_url();?>"
    </script>
    <script src="<?=base_url();?>public/js/plupload/plupload.full.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>public/js/application.js"></script>
 <!--    <script>
        $(document).ready(function(){
            $("#upload_videos_button").click(function(e){
                // e.preventDefault();
                filename = $("#filelist").text();
                $("#filename").val(filename);
            });
        });
    </script> -->
<!-- <script type="text/javascript">
  Dropzone.options.imageUpload = {
        maxFilesize:1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif"
    };
</script> -->