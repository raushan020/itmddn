<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Error Page</title>
	<style>
	.bestOfluck{
		text-align:center;
	}
	</style>
</head>
<body>
<?php 
$type =  $_GET['type'];
 ?>
<?php 
if ($type=='device') { ?>
	<div class="container-fluid" style="text-align:center;">
<h1 class="bestOfluck">Your Camera or Microphone is not working Please enable your camera</h1>
<h3>Follow Step</h3>
<ul>
	<li>
	<h3>1. Click on the lock icon</h3>
	<img src="<?php echo base_url() ?>assets/img/1.PNG">	
	<h3>2. Select allow camera</h3>
	<img src="<?php echo base_url() ?>assets/img/2.png">
     <h3>3. Select allow microphone</h3>
	<img src="<?php echo base_url() ?>assets/img/3.png">	
	<?php  
		$usertype = $this->session->userdata("usertype");
		if ($usertype == 'Professor') {
	?>
	<h3>4. Relaod your Page or Click on link <a href="<?php echo base_url() ?>exam/index" style="color:blue">Go Exam List</a></h3>
	<?php	
		}else{
	?>
		<h3>4. Relaod your Page or Click on link <a href="<?php echo base_url() ?>exam/exam_list/1" style="color:blue">Go Exam List</a></h3>
	<?php } ?>
	</li>
</ul>
</div>

<?php } ?>	


<?php 
if ($type=='Suspended') {
  $this->session->unset_userdata('rid');
 ?>
	<div class="container-fluid" style="text-align:center;">
<h1 class="bestOfluck">Your Exam has been Suspended.</h1>

	
<?php  
		$usertype = $this->session->userdata("usertype");
		if ($usertype == 'Professor') {
	?>
	<h3> <a class="btn btn-primary" href="<?php echo base_url() ?>exam/index"  style="padding: 10px;">Go to Exam List</a></h3>
	<?php	
		}else{
	?>
		<h3> <a class="btn btn-primary" href="<?php echo base_url() ?>exam/exam_list/1"  style="padding: 10px;">Go to Exam List</a></h3>
	<?php } ?>
</div>

<?php } ?>

<?php 
if ($type=='Warning') {
  $this->session->unset_userdata('rid');
 ?>
	<div class="container-fluid" style="text-align:center;">
<h1 class="bestOfluck">Warning </h1>

	<h3>4.Click on link <a href="<?php echo base_url() ?>exam/exam_list/1" style="color:blue">Go Exam List</a></h3>

</div>

<?php } ?>	

</body>
</html>