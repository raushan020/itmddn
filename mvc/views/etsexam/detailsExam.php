<!-- <script>
   $(document).ready(function() {
       function disableBack() { window.history.forward() }

       window.onload = disableBack();
       window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
   });
</script> -->
 <style type="text/css">
 #page-wrapper{
 	margin-top: 25px;
 }
 
 .container{
	box-shadow: 0px 1px 10px 3px #fff;
}
@media (min-width: 768px){
#wrapper.active {
    padding-left: 0px;
}
}

</style>

<!-- <script>
$(document).ready(function(){
    $("#hide").click(function(){
        $("p").hide();
    });
    $("#show").click(function(){
        $("p").show();
    });
});
</script> -->

<!-- for f12 -->
<!-- <script>
$(document).ready(function(){
$("body").bind("contextmenu", function(e) {
    e.preventDefault();
});
document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            return false;
        }
    }
document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            return false;
        }
    }
});
</script> -->

<!-- for right click disable -->
<!-- <script>
	 $(document).ready(function(){
 	$(document).bind("contextmenu",function(e){
   		return false;
  	});
 });

</script> -->

 <!-- for ctrl disable -->

<!-- <script>
jQuery(document).ready(function($){
    $(document).keydown(function(event) { 
        var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
        
        if (event.ctrlKey && (pressedKey == "c" || pressedKey == "u")) {
            return false; 
        }
    });
});
</script> -->

<!-- <script>

$(function(){   
    $(document).keydown(function(objEvent) {        
        if (objEvent.ctrlKey) {          
            if (objEvent.keyCode == 65) {                         

                return false;
            }            
        }        
    });
});  
</script> --> 


 <div class="">


<p><strong style="margin-left: 54px; color: #67757c;font-weight: 600;"><?php echo $quiz['quiz_name'];?></strong></p>

<div class="container-fluid" onkeypress="return disableCtrlKeyCombination(event);" onkeydown="return disableCtrlKeyCombination(event);">
      <div class="row">
         
            <div class="card">
            <div class="card-body"> 
    <!-- instraction start -->
<div class="">  
	<!-- <div class="container"> -->
	<div class="row page-titles" style="padding: 2px;">
	    <div class="col-md-12 align-self-left">
	        <p class="text-themecolor insfont-size" style="font-weight: 600;">General Instructions:</p>
	    </div>

	   <!--  <div class="col-md-6 align-self-right">
	        <p class="text-themecolor insfont-size readCarefully">Please Read The Instructions Carefully:</p>
	    </div> -->
	</div>

<div class="container-fluid instraction-bg">
<p class="instfont" style="color: red">Please Read The Instructions Carefully:</p><br>

<!-- <p class="instfont" style="margin-top: -23px;"><strong style="color: #67757c;">Please Read The Instructions Carefully:</p></strong>  -->
<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i> It is advised to appear for the exam on Google Chrome browser both in laptop & mobile devices.</p>
<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i> There are 2 types of question (a) Multiple choice questions (b) True & False</p>

<p class="instfont"style="color: #67757c;"><i class="fa fa-hand-o-right icons-style"></i>Candidates must complete their online exams within the stipulated time frame as no additional time would be provided. </p>
<p class="instfont"style="color: #67757c;padding-left: 72px;"> The duration of the exam remains unaltered irrespective of the user logging in at correct start time or later during the exam.</p>

<p class="instfont"style="color: #67757c;"><i class="fa fa-hand-o-right icons-style"></i>To change an answer, simply click the desired option button. There will be three buttons on each screen, the use of buttons is given below: </p>

<p class="instfont" style="color: #67757c;"><strong style="padding-left: 72px;">Review question:</strong> Answer the question and mark it for review at a later stage.</p>

<p class="instfont" style="color: #67757c;"><strong style="padding-left: 72px;">Submit & Next:</strong> Save the selected answer and proceed to next question. </p>

<p class="instfont" style="color: #67757c;"><strong style="padding-left: 72px;">Finish Exam Now:</strong> Submit & exit from exam.</p>

<p class="instfont" style="color: #67757c;"><i class="fa fa-hand-o-right icons-style"></i>Do Not CLICK on the 'Finish Exam Now' unless you have completed the exam. In case you click 'Finish Exam Now ' button, you will not be permitted to restart the exam.</p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>The exam will auto-submit at the end of the examination timing. All answers which you have marked till now will be saved in this case.
</p>

<p class="instfont" style="color: #67757c;"><i class="fa fa-hand-o-right icons-style"></i>In case of any issues, please call on 9911331937 / 7015095131 immediately. </p>
		

</div>
</div>
</div>

 <!-- instraction end -->


 <!-- button start -->

 <div class="container_summary1">
		<div class="new-summary">
				<!-- <span class="btn btn-info"></span>Answered -->
				<span class="btn btn-info btn_examsummary btn-answered" style="background: #0fb76b;border: 0;"></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">Answered Question</strong>
		</div>
<div class="new-summary">
		<span class="btn btn-info btn_examsummary btn-marked"><i class="fa fa-bookmark-o" style="margin-top: 5px;"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">Review Later</strong>
</div>

<div class="new-summary">
		<span class="btn btn-info btn_examsummary btn-notanswered" style="background: #da6c6c; border: 0;"></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">Not Answered </strong>
	</div>						

<div class="new-summary">
		<span class="btn btn-info btn_examsummary  btn-notvisited" style="background: #999;border: 0;"><i class="fa fa-eye-slash" style="margin-top: 5px;"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;"> Not Visited Yet</strong>
	</div>
</div>




	<!-- button end-->

	<!-- cheak box start -->
	<!-- <div class="col-md-12">
		<div class="check-style1 col-md-1">
			<label><input class="raushan checkbox" id="show" onchange="TermAndCondition()" type="checkbox"><label>
		</div>
		<div class="col-md-9 text-align">
			The computer provide me is in proper working condition. I have read and understand the instructions given below:
		</div><br><br>

		<div class="col-md-3 term22">

		<p style="color:red;" id = "hide_termsAndCondText">
			Please accept term And conditions
		</p>
		</div>
	</div> -->
	<!-- cheak box end -->
<!-- <script type="text/javascript">
	function TermAndCondition(){
		document.getElementById('start_button'). style.display= "none";
	if (document.getElementById('show').checked) {
		document.getElementById('hide_termsAndCondText').style.display= "none";
		document.getElementById('start_button').style.display= "block";
}
			

		}
	</script> -->


<!-- /.box-header -->
  
<div class="row">
<form method="post" id="quiz_detail" action="<?php echo site_url('etsexam/check_exam/'.$quiz['quid'].'/'.$this->uri->segment(4));?>">	
			<?php 
		if($this->session->flashdata('message')){
			echo $this->session->flashdata('message');	
		}
		?>	

	<?php 

		$a = $quiz['end_date'];
		$b = $quiz['start_date'];
		$diff = $a - $b;
		$hours = floor($diff / (60 * 60));
		
		$min = $diff - $hours * (60 * 60);
		$duration_date = floor( $min / 60 );
		
	 ?>

 <?php 

if(1){
if($quiz['camera_req']==1 && $this->config->item('webcam')==true){
?>
<div style="color:#ff0000;"><?php echo $this->lang->line('camera_instructions');?></div>
<div id="my_photo" style="width:500px;height:500px;background:#212121;padding:2px;border:1px solid #666666;color:red"></div>
<br><br>

		<button class="btn btn-success" type="button" onClick="javascript:capturephoto();"><?php echo $this->lang->line('capture_start_quiz');?></button>

<?php 
}else{
?>
<div class="container">
<div class="row">
	<div class="col-md-12 col-xs-8 col-md-offset-2 col-xs-offset-2">
		<input type="checkbox" id="" name="" value="" required>
		<label> I have read all the general instructions</label>
		<button class="btn btn-success start-button" type="submit" id="start_button">Start Now</button>
</div>
</div>	

 <?php 
}
}else{
	if($quiz['with_login']==0){ 
	?>
	
	<button class="btn btn-success" type="submit"><?php echo $this->lang->line('start_quiz');?></button>
 &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo site_url('quiz/open_quiz/0');?>" ><?php echo $this->lang->line('back');?></a>

	
	<?php 
	}else{
?>
<div class="alert alert-danger"><?php echo str_replace('{base_url}',base_url(),$this->lang->line('login_required'));?></div>
&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo site_url('quiz/open_quiz/0');?>" ><?php echo $this->lang->line('back');?></a>
<?php
	} 
}
?>
</div>
</div>
 
 </div>
    </form>
</div>
</div>
</div>
</div>
</div>
 
 </div>


<div  id="warning_div" style="padding:10px; position:fixed;z-index:100;display:none;width:100%;border-radius:5px;height:200px; border:1px solid #dddddd;left:4px;top:70px;background:#ffffff;">
<center><b> <?php echo $this->lang->line('to_which_position');?></b><br><input type="text" style="width:30px" id="qposition" value=""><br><br>
<a href="javascript:cancelmove();"class="btn btn-danger" style="cursor:pointer;">Cancel</a> &nbsp; &nbsp; &nbsp; &nbsp;
<a href="javascript:movequestion();"class="btn btn-info" style="cursor:pointer;">Move</a>

</center>
</div>

<style type="text/css">
	.panel-body {
    margin-top: -200px;
}
</style>

