    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i>Add Stream</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("live/table")?>">Live</a></li>

                <li class="active">Add Stream</li>

            </ol>
        </div>
    </div>

    <div class="container-fluid">
      <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
          <div class="card">
            <div class="card-body">
              <div class="col-sm-12">
<form method="post">

<!--  <div class="form-group">
     <label>Type</label>
     <select class="form-control" id='unitIDforvideo' name="unitID">
        <option>Type</option>
        <option value="1">General</option>
        <option value="2">For Subject</option>
     </select>
 </div> -->

<!--  <div class="form-group">
     <label>Subject Name</label>
     <select class="form-control" onchange='ajaxGet_subject($(this).val())' required name="subjectID">
        <option>Select Subject</option>
<?php 
foreach ($array_subject as $key => $value) {
 ?>
 <option value="<?php echo $value['subjectID'] ?>"><?php echo $value['subject'] ?></option>
<?php } ?>
     </select>
 </div> -->
  <div class="form-group">
     <label>Stream Name</label>
 <!-- <input class="form-control editor" name="stream_name" id="editorss" required> -->
 <input type="text" class="form-control" name="stream_name" required="required">
 </div>
 <div class="form-group">
     <label>Stream Type</label>
     <select class="form-control" id='unitIDforvideo' name="av">
        <option value="1">Video</option>
        <option value="2">Audio</option>
     </select>
 </div>
          <div class="form-group">
            <input type="submit" class="btn btn-info" name="submit">
          </div>


</form>

</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
  function ajaxGet_subject(subjectID)
  {
 
  // var subjectID=$("#subjectIDforvideo").val();
    $.ajax({
     type: "POST",
     url:"<?=base_url('ajaxControllerQuestion/Getunits')?>",
     data:{"subjectID":subjectID},
     success: function(response)
     {
      // alert(response);
        $("#unitIDforvideo").html(response);
        // alert(response);
     }
    });
  }
</script>
<script>
    initSample();
</script>