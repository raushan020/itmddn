    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i>Edit Stream</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("live/table")?>">Live</a></li>

                <li class="active">Edit Stream</li>

            </ol>
        </div>
    </div>

    <div class="container-fluid">
      <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom:10px;">BACK</button>
      <div class="row">
          <div class="card">
            <div class="card-body">
              <div class="col-sm-12">
<form method="post">

<!--  <div class="form-group">
     <label>Type</label>
     <select class="form-control" id='unitIDforvideo' name="unitID">
        <option>Type</option>
        <option value="1">General</option>
        <option value="2">For Subject</option>
     </select>
 </div> -->

<!--  <div class="form-group">
     <label>Subject Name</label>
     <select class="form-control" onchange='ajaxGet_subject($(this).val())' required name="subjectID">
        <option>Select Subject</option>
<?php 
foreach ($array_subject as $key => $value) {
 ?>
 <option value="<?php echo $value['subjectID'] ?>"><?php echo $value['subject'] ?></option>
<?php } ?>
     </select>
 </div> -->
  <div class="form-group">
     <label>Stream Name</label>
 <!-- <input class="form-control editor" value="<?php echo $edit_data->stream_name ?>" name="stream_name" id="editor" required> -->
 <input type="text" class="form-control" name="stream_name" value="<?php echo $edit_data->stream_name ?>" required>
 </div>
 <div class="form-group">
     <label>Stream Type</label>
     <select class="form-control" id='unitIDforvideo' name="av">
        <option value="1" <?php if($edit_data->av==1){echo "selected";} ?>>Video</option>
        <option value="2" <?php if($edit_data->av==2){echo "selected";} ?>>Audio</option>
     </select>
 </div>

  <div class="form-group">
     <label>Live Status</label>
     <select class="form-control" id='unitIDforvideo' name="live_status">
        <option value="1" <?php if($edit_data->live_status==1){echo "selected";} ?>>Live</option>
        <option value="2" <?php if($edit_data->live_status==2){echo "selected";} ?>>Unpublish</option>
     </select>
 </div>
          <div class="form-group">
            <input type="submit" class="btn btn-info" name="submit">
          </div>


</form>

</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
  function ajaxGet_subject(subjectID)
  {
 
  // var subjectID=$("#subjectIDforvideo").val();
    $.ajax({
     type: "POST",
     url:"<?=base_url('ajaxControllerQuestion/Getunits')?>",
     data:{"subjectID":subjectID},
     success: function(response)
     {
      // alert(response);
        $("#unitIDforvideo").html(response);
        // alert(response);
     }
    });
  }
</script>
<script>
    initSample();
</script>