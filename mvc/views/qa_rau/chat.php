<style type="text/css">
	.single-head {
    min-height: 69px;
    padding-top: 20px;
}
.select-category {
    border-bottom: 1px solid #dadfea;
    margin: 0;
}
.single-head h1 {
    /* display: inline-block; */
    font-size: 22px;
    color: #3498db;
    margin: 0 0 15px 0;
    word-wrap: break-word;
}
.question-main-content {
    padding: 25px 0;
    border-bottom: 1px solid #dadfea;
    margin: 0;
}
.main-content > .row {
    margin: 0;
}
.question-main-content .vote-block ul li {
    display: block;
    margin-bottom: 10px;
}
.question-main-content .vote-block a {
    width: 25px;
    height: 25px;
    border: 1px solid #dadfea;
    display: block;
    text-align: center;
    line-height: 1.6;
    color: #6f7d95;
    margin: 0 auto;
}
.img-circle {
    border-radius: 50%;
}
.question-main-content .vote-block a i.fa-chevron-up {
    position: relative;
    bottom: 2px;
}
.question-main-content .vote-block a i {
    font-size: 9px;
}
.question-main-content .vote-block ul li {
    display: block;
    margin-bottom: 10px;
}
.ask-question, .modal-submit-questions .btn-submit-question, .question-main-content .vote-block span, .contact-block button, #run_setup_pump {
    background: #3498db;
}
.question-main-content .vote-block span {
    color: #fff;
    min-width: 45px;
    height: 23px;
    text-align: center;
    margin: 0 auto;
    display: table;
    padding: 0 10px;
    line-height: 23px;
    font-size: 12px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background-color: #3397db;
}
.question-main-content .top-content ul.question-tags {
    margin-left: 0;
}
ul.question-tags {
    display: inline-block;
    margin-bottom: 10px;
}
ul.question-tags li {
    margin: 10px 8px 10px 0;
    margin-bottom: 10px;
    display: inline-block;
}
.select-grey-bg, .widget-related-tags li a, .q-tag {
    background: #f3f5f9;
}
.main-questions-list_next .q-lbtm-content .question-excerpt, .single-header .vote-block a, .comment-edit a, .comment-time, .q-tag {
    color: #6f7d95;
}
.question-main-content .question-content {
    color: #5f6f81;
}
.question-main-content .question-content {
    margin: 25px 0 20px;
    font-size: 16px;
    color: #5f6f81;
    border-bottom: 1px solid #dadfea;
    padding-bottom: 15px;
}
.question-main-content .question-content > p {
    margin: 10px 0;
}
.question-cat span.user-badge {
    padding: 0 10px;
    color: #fff;
    /* margin-left: 10px; */
    margin-right: 15px;
    line-height: 20px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background-color: #00adef;
}
.question-cat span {
    display: inline-block;
    vertical-align: middle;
}
.question-cat span {
    display: inline-block;
    vertical-align: middle;
}
.question-cat span {
    display: inline-block;
    vertical-align: middle;
}
.question-cat span.author-avatar {
    max-width: 30px;
    margin-right: 8px;
}
.question-cat span {
    display: inline-block;
    vertical-align: middle;
}
.question-cat span.author-avatar img {
    max-width: 100%;
    height: auto;
}
#main_users_list li.user-item .user-avatar img, .author-avatar img, .comment-avatar img {
    border-radius: 50%;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    max-width: 100%;
    height: auto;
}
.main-questions-list_next .q-right-content {
    padding: 0;
    text-align: center;
}
.main-questions-list_next ul.question-statistic {
    text-align: right;
}
.main-questions-list_next ul.question-statistic li {
    display: inline-block;
    text-align: center;
    margin-left: 35px;
}
.main-questions-list_next ul.question-statistic li span {
    width: 46px;
    height: 46px;
    display: block;
    text-align: center;
    line-height: 3.3;
    margin-bottom: 1px;
    border: 1px solid #d9dfe9;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.main-questions-list_next ul.question-statistic li.active span {
    -moz-border-radius: 22px;
    -webkit-border-radius: 22px;
    border-radius: 22px;
    background-color: #1abc9c;
    color: #fff;
    border: none;
}
.main-questions-list_next ul.question-statistic li span {
    width: 46px;
    height: 46px;
    display: block;
    text-align: center;
    line-height: 3.3;
    margin-bottom: 1px;
    border: 1px solid #d9dfe9;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
</style>
          <div class="card">
            <div class="card-body">
<div class="row select-category single-head main-questions-list_next">
<div class="col-md-8">
	<!-- <h1 itemprop="name"><?php echo $qa->title ?></h1> -->
</div>	 
	<div class="col-md-4 col-xs-4 q-right-content">
            <ul class="question-statistic">
                <li>
                <span class="question-views">
                    <?php echo $qa->views ?>                </span>
                    views                </li>
                <li class="active">
                <span itemprop="answerCount" class="question-answers">
                    <?php echo $qa->no_ans ?>                </span>
                    answers                </li>
                <li>
                <span itemprop="upvoteCount" class="question-votes">
                    <?php echo $qa->votes ?>                 </span>
                    votes                </li>
            </ul>
            <div class="pumping">
                            </div>
        </div>

</div>

<div id="question_content" class="row question-main-content question-item">
	<div class="col-md-2 col-xs-2 vote-block">
		<ul>
        <!-- vote up -->
        <li title="" data-original-title="This is useful.">
        	<a data-container="body" onclick="que_upvote('<?php echo $qa->qaID ?>')" data-toggle="popover" data-content="You must have 5 points to vote up." href="javascript:void(0)" data-name="vote_up" class="action vote vote-up img-circle " data-original-title="" title="">
        		<i class="fa fa-chevron-up"></i>
        	</a>
        </li>
        <!--// vote up -->

        <!--vote point -->
        <li data-original-title="" title="">
        	<span itemprop="upvoteCount" id="que_vote_bank" class="vote-count"><?php echo $qa->votes ?></span>
        </li>
        <!--// vote point -->
        <!-- vote down -->
        <li title="" data-original-title="This is not useful">
        	<a data-container="body" data-toggle="popover" onclick="que_downvote('<?php echo $qa->qaID ?>')" data-content="You must have 500 points to vote down." href="javascript:void(0)" data-name="vote_down" class="action vote vote-down img-circle " data-original-title="" title="">
        		<i class="fa fa-chevron-down"></i>
        	</a>
        </li>
        <!--// vote down -->
		       <li class="single-question-pump" data-original-title="" title="">
                  </li>
    </ul>
	</div>
	<div class="col-md-9 col-xs-9 q-right-content">
<!-- 		<div class="top-content">
                                        <ul class="question-tags">
                                                    <li>
                                <a class="q-tag" href="/blog/qa-tag/apple/ ">
                                    apple                    </a>
                            </li>
                                                    <li>
                                <a class="q-tag" href="/blog/qa-tag/apps/ ">
                                    apps     </a>
                            </li>
                                                    <li>
                                <a class="q-tag" href="/blog/qa-tag/business-2/ ">
                                    business                                </a>
                            </li>
                                                    <li>
                                <a class="q-tag" href="/blog/qa-tag/important/ ">
                                    important                                </a>
                            </li>
                                                    <li>
                                <a class="q-tag" href="/blog/qa-tag/marketing-2/ ">
                                    marketing                                </a>
                            </li>
                                            </ul>

                </div> -->
                <div class="clearfix"></div>
                <div class="question-content">
                	<p><span style="color: #333333; font-family: Georgia, Times, 'Times New Roman', serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; text-align: start; text-indent: 0px; float: none;"><?php echo $qa->que ?><span class="Apple-converted-space"><br>
</span></span></p>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 question-cat" data-user="4">
                        <a href="">
                            <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                                <span class="author-avatar">
                                	<?php if($name_of_qa){ ?>
                                    <img itemprop="image" src="<?php echo base_url() ?>uploads/images/<?php echo $name_of_qa->photo ?>" class="avatar" alt="">   
										<?php } ?>
                                                                 </span>
                                <span itemprop="name" class="author-name"><?php
                                if($name_of_qa){
                                		echo $name_of_qa->name;
                                	}else{
                                		echo "Admin";
                                	}
                                 ?></span>
                                	
                                
                            </span>
                        </a>
                        <span title="46" class="user-badge" style="background-color:#04aad4;">Student</span>
                        <span class="question-time" itemprop="dateCreated" datetime="on June 5, 2014">
                           	<?php 
	echo time_elapsed_string($qa->createTime);
		  ?> in                        </span>
                        <span class="question-category">
                        	<?php if($name_of_subject && $name_of_unit): ?>
                                            <span style="background-color:#04aad4;" class="user-badge"><?php echo  $name_of_subject->subject  ?>(<?php echo  $name_of_unit->unit_name  ?>)</span>
                                        <?php endif; ?>
                                        </span>                    </div>
                    <div class="col-md-4 col-xs-4 question-control">
                     
                     <ul class=&quot;socials-share&quot;><li>
                                	<a href=&quot;https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fqaengine.enginethemes.com%2Fblog%2Fquestion%2Fwhat-are-good-ways-to-learn-to-become-the-best-digital-marketer%2F&amp;t=What are good ways to learn to become the best digital marketer?&quot; target=&quot;_blank&quot; class=&quot;btn-fb&quot;><i class=&quot;fa fa-facebook&quot;></i></a>
                                </li>
                                <li>
                                	<a target=&quot;_blank&quot; href=&quot;http://twitter.com/share?text=What are good ways to learn to become the best digital marketer?&amp;url=https%3A%2F%2Fqaengine.enginethemes.com%2Fblog%2Fquestion%2Fwhat-are-good-ways-to-learn-to-become-the-best-digital-marketer%2F&quot; class=&quot;btn-tw&quot;><i class=&quot;fa fa-twitter&quot;></i></a>
                                </li>
                                <li class=&quot;ggplus&quot;>
                                	<a target=&quot;_blank&quot;  href=&quot;https://plus.google.com/share?url=https%3A%2F%2Fqaengine.enginethemes.com%2Fblog%2Fquestion%2Fwhat-are-good-ways-to-learn-to-become-the-best-digital-marketer%2F&quot; class=&quot;btn-gg&quot;><i class=&quot;fa fa-google-plus&quot;></i>
                                	</a>
                                </li>
                            </ul>

                   
                    </div>
                </div>
	</div>
</div>

<style type="text/css">
	.answers-filter {
    margin: 0;
}
.answers-filter, .paginations {
    min-height: 44px;
    border-bottom: 1px solid #dadfea;
}
.main-questions-list .q-ltop-content a.question-title, .answers-filter .answers-count, .form-reply h3 {
    /* color: #444c63; */
}
.answers-filter .answers-count {
    color: #444c63;
    font-size: 14px;
    font-weight: 700;
    display: block;
    margin-top: 12px;
}
.answers-filter .answers-count {
    color: #444c63;
    font-size: 14px;
    font-weight: 700;
    display: block;
    margin-top: 12px;
}
</style>

<div class="sticky-wrapper" style="">
	<div class="row answers-filter" id="answers_filter">
                <div class="max-col-md-8">
                    <div class="col-md-6 col-xs-6">
                    <span class="answers-count"><span itemprop="answerCount" class="number">
                        <?php echo $qa->no_ans ?> </span> Answer(s)                    </span>
                    </div>
             <!--        <div class="col-md-6 col-xs-6 sort-questions">
                        <ul>
                            <li>
                                <a class="active" href="/blog/question/how-to-be-rich/">Votes</a>
                            </li>
                            <li>
                            <a class="" href="/blog/question/how-to-be-rich/?sort=active">Active</a>
                        </li>
                            <li>
                                <a class="" href="/blog/question/how-to-be-rich/?sort=oldest">Oldest</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div></div>
<?php 
	foreach ($ans as $key => $value) {
 ?>
            <div id="question_content" class="row question-main-content question-item">
	<div class="col-md-2 col-xs-2 vote-block">
		<ul>
        <!-- vote up -->
        <li title="" data-original-title="This is useful.">
        	<a data-container="body" data-toggle="popover" onclick="ans_upvote('<?php echo $value['ansID']; ?>')" data-content="You must have 5 points to vote up." href="javascript:void(0)" data-name="vote_up" class="action vote vote-up img-circle " data-original-title="" title="">
        		<i class="fa fa-chevron-up"></i>
        	</a>
        </li>
        <!--// vote up -->

        <!--vote point -->
        <li data-original-title="" title="">
        	<span itemprop="upvoteCount" class="vote-count" id="ans_vote_bank<?php echo $value['ansID']; ?>"><?php echo $value['votes']; ?></span>
        </li>
        <!--// vote point -->
        <!-- vote down -->
        <li title="" data-original-title="This is not useful">
        	<a data-container="body" data-toggle="popover" onclick="ans_downvote('<?php echo $value['ansID']; ?>')" data-content="You must have 500 points to vote down." href="javascript:void(0)" data-name="vote_down" class="action vote vote-down img-circle " data-original-title="" title="">
        		<i class="fa fa-chevron-down"></i>
        	</a>
        </li>
        <!--// vote down -->
		       <li class="single-question-pump" data-original-title="" title="">
                  </li>
    </ul>
	</div>
	<div class="col-md-9 col-xs-9 q-right-content">
                <div class="question-content">
                	<p><span style="color: #333333; font-family: Georgia, Times, 'Times New Roman', serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; text-align: start; text-indent: 0px; float: none;"><?php echo $value['ans'] ?><span class="Apple-converted-space"><br>
</span></span></p>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 question-cat" data-user="4">
                        <a href="#">
                            <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
                                <span class="author-avatar">
                                
                                    <img itemprop="image" src="<?php echo base_url() ?>uploads/images/<?php echo $value['name_photo']['photo'] ?>" class="avatar" alt="">   
									
                                                                 </span>
                                <span itemprop="name" class="author-name"><?php echo $value['name_photo']['name'] ?></span>
                                	
                                
                            </span>
                        </a>
                        <span title="46" class="user-badge" style="background-color:#04aad4;"><?php echo $value['CreatedUsertype'] ?></span>
                        <span class="question-time" itemprop="dateCreated" datetime="on June 5, 2014">
                           	<?php 
	echo time_elapsed_string($value['createTime']);
		  ?>                       </span>
                                           </div>
                    <div class="col-md-4 col-xs-4 question-control">
                     
                     <ul class=&quot;socials-share&quot;><li>
                                	<a href=&quot;https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fqaengine.enginethemes.com%2Fblog%2Fquestion%2Fwhat-are-good-ways-to-learn-to-become-the-best-digital-marketer%2F&amp;t=What are good ways to learn to become the best digital marketer?&quot; target=&quot;_blank&quot; class=&quot;btn-fb&quot;><i class=&quot;fa fa-facebook&quot;></i></a>
                                </li>
                                <li>
                                	<a target=&quot;_blank&quot; href=&quot;http://twitter.com/share?text=What are good ways to learn to become the best digital marketer?&amp;url=https%3A%2F%2Fqaengine.enginethemes.com%2Fblog%2Fquestion%2Fwhat-are-good-ways-to-learn-to-become-the-best-digital-marketer%2F&quot; class=&quot;btn-tw&quot;><i class=&quot;fa fa-twitter&quot;></i></a>
                                </li>
                                <li class=&quot;ggplus&quot;>
                                	<a target=&quot;_blank&quot;  href=&quot;https://plus.google.com/share?url=https%3A%2F%2Fqaengine.enginethemes.com%2Fblog%2Fquestion%2Fwhat-are-good-ways-to-learn-to-become-the-best-digital-marketer%2F&quot; class=&quot;btn-gg&quot;><i class=&quot;fa fa-google-plus&quot;></i>
                                	</a>
                                </li>
                            </ul>

                   
                    </div>
                </div>
	</div>
</div>
<?php } ?>
<style type="text/css">
	.form-reply h3 {
    color: #444c63;
    font-size: 14px;
    font-weight: 700;
    margin-bottom: 20px;
}
.submit-wrapper {
    margin-top: 25px;
}
.submit-wrapper button {
    border: none;
    color: #fff;
    display: block;
    height: 40px;
    width: 100%;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background-color: #3397db;
    font-size: 14px;
    font-weight: 700;
}
</style>
<div class="row form-reply">
                <div class="col-md-12">
                    <h3>Your Answer</h3>
 
                    	<div class="form-group">
                    		<textarea class="form-control" id="text_que"></textarea>
                    	</div>
                        <div class="row submit-wrapper">
                            <div class="col-md-2">
                                <button id="submit_reply" class="btn-submit" onclick="addAnswer()">
                                    Post answer                                </button>
                            </div>
                        </div>
                
                </div>
            </div>

</div>
</div>
<script type="text/javascript">
  function addAnswer(){
       var  ans  = $("#text_que").val();
       var  id  = "<?php echo $this->uri->segment(3) ?>";
     $.ajax({
        type: "POST",
        url:"<?=base_url('qa/addAnswer')?>",
        data:{ans:ans,id:id},
        success: function(response) {
        	location.reload();
            $("#subjectID").html(response);
        }
    });
   }
</script>

<script type="text/javascript">
  function que_upvote(qaID){

     $.ajax({
        type: "POST",
        url:"<?=base_url('qa/que_upvote')?>",
        data:{qaID:qaID,},
        success: function(response) {
        	console.log(response);
            $("#que_vote_bank").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function que_downvote(qaID){

     $.ajax({
        type: "POST",
        url:"<?=base_url('qa/que_downvote')?>",
        data:{qaID:qaID,},
        success: function(response) {
        	console.log(response);
            $("#que_vote_bank").html(response);
        }
    });
   }
</script>

<script type="text/javascript">
  function ans_upvote(qaID){

     $.ajax({
        type: "POST",
        url:"<?=base_url('qa/ans_upvote')?>",
        data:{ansID:qaID,},
        success: function(response) {
            console.log(response);
            $("#ans_vote_bank"+qaID).html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function ans_downvote(qaID){

     $.ajax({
        type: "POST",
        url:"<?=base_url('qa/ans_downvote')?>",
        data:{ansID:qaID,},
        success: function(response) {
            console.log(response);
            $("#ans_vote_bank"+qaID).html(response);
        }
    });
   }
</script>