    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i> Ask Question </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("qa/index")?>">Question</a></li>

                <li class="active">Ask Question</li>

            </ol>
        </div>
    </div>

    <div class="container-fluid">
      <a href="<?=base_url("qa/index/all")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
      <div class="row">
          <div class="card">
            <div class="card-body">
              <div class="col-sm-12">
<form method="post">
 <div class="form-group">
     <label>Subject Name</label>
     <select class="form-control" onchange='ajaxGet_subject($(this).val())' name="subjectID">
        <option>Select Subject</option>
<?php 
foreach ($array_subject as $key => $value) {
 ?>
 <option value="<?php echo $value['subjectID'] ?>"><?php echo $value['subject'] ?></option>
<?php } ?>
     </select>
 </div>
  <div class="form-group">
     <label>Unit Name</label>
     <select class="form-control" id='unitIDforvideo' name="unitID">
        <option>Select Unit</option>
        <option></option>
     </select>
 </div>

   <!-- <div class="form-group">
     <label>Title</label>
     <input type="" class="form-control" name="title" />
 </div> -->

   <div class="form-group">
     <label>Write Your Question</label>
 <textarea class="form-control" name="que"></textarea>
 </div>
          <div class="form-group">
            <input type="submit" class="btn btn-info" name="submit">
          </div>


</form>

</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">
  function ajaxGet_subject(subjectID)
  {
 
  // var subjectID=$("#subjectIDforvideo").val();
    $.ajax({
     type: "POST",
     url:"<?=base_url('ajaxControllerQuestion/Getunits')?>",
     data:{"subjectID":subjectID},
     success: function(response)
     {
      // alert(response);
        $("#unitIDforvideo").html(response);
        // alert(response);
     }
    });
  }
</script>