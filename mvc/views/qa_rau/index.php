<style type="text/css">
  .myNav{
   
    margin-top: 20px;
  }
  .myNav .myNav-item{
    display: inline-block;
    padding: 10px;
    /*margin-left: 10px;*/
  }
 .heading:hover{
    color:  #ff7361;
  }
  .nav-item:active{
background-color: #ff7361; 
  }
  .button_colorw{
    color: #fff;
  }
  .button_colorw1{
    float: right !important;
  }
</style>
<div class="">

    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-question"></i> Ask a Question </h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

          <li class="active"><?=$this->lang->line('menu_classes')?></li>
        </ol>
      </div>
    </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">

            <div class="card-body">
<style type="text/css">
  .ask-question, .contact-block, #run_pump_setup {
    width: 100%;
    height: 45px;
    border: none;
    color: #fff;
    margin-top: 30px;
    margin: 0 auto;
    display: block;
    font-weight: 700;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    background-color: #3397db;
}
</style>
<?php 
$usertype = $this->session->userdata('usertype');
if($usertype=='Student'){
 ?>
     <div class="col-md-12">
      <button class="btn btn-primary button_colorw1"><a href="<?php echo base_url() ?>qa/add"  class="action button_colorw">
                    <i class="fa fa-plus"></i>ASK A QUESTION</a></button>
                    <!-- <a href="<?php echo base_url() ?>qa/add"  class="action ask-question">
                    <i class="fa fa-plus"></i> ASK A QUESTION</a> -->
     </div>         
 <?php } ?>
  
<?php
    $uri = $this->uri->segment(3);
 ?>

  <ul class="nav nav-tabs">
    <li class="nav-item <?php if($uri=='all'){echo "active";} ?>">
      <a class="nav-link "  onclick="changeLink('all')"  href="#home">All Questions</a>
    </li>
<!--     <li class="nav-item <?php if($uri=='mr'){echo "active";} ?>">
      <a class="nav-link" onclick="changeLink('mr')"  href="#">Most Responses</a>
    </li>
    <li class="nav-item <?php if($uri=='ra'){echo "active";} ?>">
      <a class="nav-link" onclick="changeLink('ra')"   href="#">Recently Answered</a>
    </li>
    <li class="nav-item <?php if($uri=='na'){echo "active";} ?>">
      <a class="nav-link"  onclick="changeLink('na')"  href="#">No Answers</a>
    </li> -->

  </ul>
<style type="text/css">
  .question-author img {
    width: 65px;
    height: 65px;
    overflow: hidden;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.question-desc {
    margin-bottom: 20px;
    padding-bottom: 30px;
    line-height: 22px;
    font-size: 13px;
    border-bottom: 1px solid #dedede;
    color: #848991;
}
.myNav li a{
    font-size: 13px;
    color: #2f3239;
}
</style>
  <!-- Tab panes -->
  <div class="tab-content">
    
      <div id="home" class="tab-pane active"><br>
        <?php 
        foreach ($qa as $key => $value) {
         ?>
      <div class="card" style="margin: 18px 10px;">
      <div class="row">
        <div class="col-md-1 question-author">
          <img src="<?php echo base_url("assets/img/forum.png") ?>" style="width: 50px;
    height: 50px;margin: 10px;">
        </div>
        <div class="col-md-11 " style="margin-top: 10px;
}">
        <!-- <a  href="<?php echo base_url() ?>qa/chat/<?php echo  $value->qaID ?>" class=""><h4 class="heading"><?php echo  $value->title ?></h4></a> -->
     
<p class="question-desc">
<span><strong style="color: #000;">Question:-</strong></span> <?php echo  $value->que ?><br><br>
<span><a href="<?php echo base_url() ?>qa/chat/<?php echo  $value->qaID ?>"  class="btn btn-primary">view answers</a></span>
</p>

      <ul class="myNav">
        <li class="myNav-item">
          <a class="myNav-link  " data-toggle="tab" href="#">
              
              <?php 
                if($value->qa_status==1){?>
                    <span style="color:green"><i class="fa fa-check" aria-hidden="true"></i> Solved</span>
                 <?php } else { ?>
                   <span style="color:red"><i class="fa fa-spinner" aria-hidden="true"></i> In Progress</span> 
                  <?php } ?>  
          </a>
        </li>
        <li class="myNav-item">
          <a class="myNav-link" data-toggle="tab" href="#"><span><i class="fa fa-clock-o"></i></span> <?php 
  echo time_elapsed_string($value->createTime);
      ?>                </a>
        </li>
        <li class="myNav-item">
          <a class="myNav-link" data-toggle="tab" href="#"><i class="fa fa-reply"></i> Answer(<?php echo  $value->no_ans ?>)</a>
        </li>
        <li class="myNav-item">
          <a class="myNav-link" data-toggle="tab" href="#"><i class="fa fa-eye"></i> views(<?php echo  $value->views ?>)</a>
        </li>
        <?php
        if($this->session->userdata('loginuserID')==$value->CreatedUserID){
         ?>
        <li class="myNav-item">
          <a class="myNav-link"  href="<?php  echo base_url() ?>qa/edit/<?php echo $value->qaID ?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
        </li>
      <?php } ?>
      </ul>
     </div>
 </div>   
      </div>

<?php } ?>


  </div>



  </div>

  
<div id="pagination">
                       <ul class="tsc_pagination pull-right">

                       <!--Show pagination links -->
                           <div class="col-md-12 text-center">
                               <?php echo $pagination; ?>
                           </div>
                       </ul>
                   </div>

</div>
</div>
</div>
</div>
<script type="text/javascript">
  function changeLink(val){
    
   window.location.href=base_url+"qa/index/"+val;
  }
</script>