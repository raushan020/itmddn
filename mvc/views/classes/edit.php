    <div class="">
       <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="ti ti-book"></i> <?=$this->lang->line('edit')?> <?=$this->lang->line('menu_classes')?></h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                    <li><a href="<?=base_url("classes/index")?>"></i> <?=$this->lang->line('menu_classes')?></a></li>

                    <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_classes')?></li>

                </ol>
            </div>
        </div>


<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <a href="<?=base_url("classes/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

   

            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post">
                    <?php 

                        if(form_error('classes')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        

                        <label for="classes" class="col-sm-4 control-label">

                            <?=$this->lang->line("courses_name")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="classes" name="classes" value="<?=set_value('classes', $classes->classes);?>" >


                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classes'); ?>

                        </span>

                    </div>


                    <?php 

                        if(form_error('shortcode')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="shortcode" class="col-sm-4 control-label">

                           Short Code Of <?=$this->lang->line("courses_name")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" required = "required" placeholder='Short Code Of <?=$this->lang->line("courses_name")?>' id="shortcode" name="shortcode" value="<?=set_value('shortcode',$classes->shortCodeofCourse)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('shortcode'); ?>

                        </span>
                        </div>


                     <?php 

                        if(form_error('education_details')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classes" class="col-sm-4 control-label">

                            Eductions Qualification <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6 ulliclass">
                          <ul>
                          <?php
                      $education_detailsIDs = explode(',', $classes->education_detailsID);

                          foreach ($education_details as $key => $value) {
                                  if ($value->education_detailsID==1) {
                                     $required = 'required';

                                    }else{
                                       $required = ''; 
                                    }
                                ?>
                                <li>
                                    <div class="checkbox">
                                        <label><input type="checkbox" <?php echo $required ?>   <?php if(in_array($value->education_detailsID, $education_detailsIDs)){echo "checked";}else{echo"";} ?> name="education_details[]" value="<?=set_value('education_details',$value->education_detailsID) ?>"><?php echo $value->education ?></label>
                                    </div>
                                </li>
                            <?php } ?>                           
                          </ul>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('education_details'); ?>
                        </span>
                    </div>


                    <?php 

                        if(form_error('isRequired')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>
                        <label for="classes" class="col-sm-4 control-label">
                            Validate Qualification <span class="red-color">*</span>               </label>
                        <div class="col-sm-6 ulliclass">
                         <select name="isRequired" class="form-control">
                             <option value="1" <?php if($classes->isRequired==1){echo "Selected";}?>>Required</option>
                             <option value="0" <?php if($classes->isRequired==0){echo "Selected";}?>>Not Required</option>
                         </select>    
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('education_details'); ?>
                        </span>
                    </div>


                    <?php 

                        if(form_error('course_type')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>
                        <label for="classes" class="col-sm-4 control-label">
                           Cours Type <span class="red-color">*</span>               </label>
                        <div class="col-sm-6 ulliclass">
                         <select name="course_type" class="form-control">
                             <option value="3" <?php if($classes->course_type==3){echo "Selected";}?>>PG</option>
                             <option value="2" <?php if($classes->course_type==2){echo "Selected";}?>>UG</option>
                             <option value="1" <?php if($classes->course_type==1){echo "Selected";}?>>Diploma</option>
                         </select>    
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('course_type'); ?>
                        </span>
                    </div>


<!-- time duration -->
                    <?php 

                        if(form_error('duration')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("duration")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                        <input type="number" class="form-control" min="1" max="10" value="<?=set_value('duration',$classes->duration)?>" required = "required" placeholder="<?=$this->lang->line("duration")?>" style="resize:none;" id="duration" name="duration"></input>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('duration'); ?>

                        </span>

                    </div>


<!-- end time duration -->




<!-- time mode -->

                    <?php 

                        if(form_error('mode')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("mode")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">


                     <select class="form-control" placeholder="<?=$this->lang->line("mode")?>" required = "required" value = "<?=set_value('mode')?>" style="resize:none;" id="mode" name="mode"<?php if($check_classesID>0){echo "disabled";}else{echo "";} ?> >
                     <option value="">Select</option>
                     <option value="2" <?php if ($classes->mode==2) {echo "Selected";} ?>>Semester</option>
                    <option value="1" <?php if ($classes->mode==1) {echo "Selected";} ?>>Yearly</option>
                    </select>
                        </div>
<?php if($check_classesID>0){echo "<div class='clearfix'></div><p style='color:red'; text-align='center;'>You can not change course mode please contact to support team</p>";}else{echo "";} ?>  
                        <span class="col-sm-4 control-label">

                            <?php echo form_error('mode'); ?>

                        </span>

                    </div>


<!-- end time mode -->
<?php 

                        if(form_error('department')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            Department <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">
                            <select class="form-control" required = "required" id="department" name="department">
                                <option value="">Select Department</option>

                                <option <?php if ($classes->departmentID==1){echo "selected";}else{echo "";} ?> value="1">Department of IT</option>
                                <option <?php if ($classes->departmentID==2){echo "selected";}else{echo "";} ?> value="2">Department of Management and Commerce</option>
                                <option <?php if ($classes->departmentID==3){echo "selected";}else{echo "";} ?> value="3">Department of Animation</option>
                                <option <?php if ($classes->departmentID==4){echo "selected";}else{echo "";} ?> value="4">Department of Library Sciences</option>
                                <option <?php if ($classes->departmentID==5){echo "selected";}else{echo "";} ?> value="5">Department of Hotel Management</option>
                                <option <?php if ($classes->departmentID==6){echo "selected";}else{echo "";} ?> value="6">Department of Mass Communication</option>
                                <option <?php if ($classes->departmentID==15){echo "selected";}else{echo "";} ?> value="15">Department of Sports</option>
                            </select>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('department'); ?>

                        </span>

                    </div>


                    <?php 

                        if(form_error('IsSubCourse')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            Is Sub Course <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                     <select class="form-control" required = "required"  id="IsSubCourse" name="IsSubCourse" <?php if($check_classesID_Subcourse>0){echo "disabled";}else{echo "";} ?>>
                     <option >Select</option>
                     <option value="1" <?php if ($classes->IsSubCourse==1) {echo "Selected";} ?>>Yes</option>
                    <option value="0" <?php if ($classes->IsSubCourse==0) {echo "Selected";} ?>>No</option>
                    </select>

                            <?php
                                if ($check_classesID_Subcourse>0) {
                            ?>
                                <input type="hidden" name="IsSubCourse" value="<?php echo $classes->IsSubCourse; ?>">
                         <?php } ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('IsSubCourse'); ?>

                        </span>

                    </div>




<!-- time fee -->

                    <?php 

                        if(form_error('fee')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("fee")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">


                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                          <input class="form-control" placeholder="<?=$this->lang->line("fee")?>" value="<?=set_value('fee',$classes->fee)?>" required = "required" style="resize:none;" id="fee" name="fee">
                          <span class="input-group-addon">.00</span>
                        </div>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('fee'); ?>

                        </span>

                    </div>


<!-- end time fee -->



                    <?php 

                        if(form_error('note')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("courses_note")?>

                        </label>

                        <div class="col-sm-6">

                            <textarea style="resize:none;" class="form-control" id="note" name="note"><?=set_value('note', $classes->note);?></textarea>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('note'); ?>

                        </span>

                    </div>

<!-- <style type="text/css">
 .dadaCla ul li{
display:inline-block;
 }
 .dadaCla ul li a {
    background: #23292f;
    padding: 10px 16px;
    color: #fff;
 }
 .dadaCla ul li a span{
    color: red;
    padding-left: 9px;
 }
</style> -->
<!-- <div class="form-group col-sm-6 dadaCla">
  <ul>
      <li><a href="">BA Hindi<span> <i class="fa fa-times" aria-hidden="true"></i></span></a></li>
  </ul>
</div> -->
<!-- <div class="clearfix"></div>
                <div class="form-group">

                     <label for="classes" class="col-sm-2 control-label">

                Add Sub Courses

                        </label>

                        <div class="col-sm-6">

<div class="input-group">
  <input type="text" class="form-control" placeholder="Sub Courses" aria-describedby="basic-addon2">
  <span class="input-group-addon" id="basic-addon2"><a href="">Add</a></span>
</div>

                        </div>


   
                </div>-->



                    <div class="form-group">

                        <div class="col-sm-offset-8 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("update_course")?>" >

                        </div>

                    </div>



                </form>

            </div>    

        </div>

    </div>

</div>
</div></div></div>
<script type="text/javascript">
    
    $(function()
{
    $(document).ready(function(){
        var buttonadd = '<span><button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span></button></span>';
        var fvrhtmlclone = '<div class="fvrclonned">'+$(".fvrduplicate").html()+buttonadd+'</div>';
        $( ".fvrduplicate" ).html(fvrhtmlclone);
        $( ".fvrduplicate" ).after('<div class="fvrclone"></div>');

        $(document).on('click', '.btn-add', function(e)
        {
            e.preventDefault();
    
            $( ".fvrclone" ).append(fvrhtmlclone);
                  $(this).removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="glyphicon glyphicon-minus"></span>');
            
        }).on('click', '.btn-remove', function(e)
        {
            $(this).parents('.fvrclonned').remove();
    
            e.preventDefault();
            return false;
        });

    });
    
 
// FUNÇÂO DE TESTE    
    

});
</script>
<style type="text/css">
    .fvrclonned
{
    margin-top: 10px;
}

.glyphicon
{
    font-size: 14px;
}
.red-color{
    color:red;
}
</style>