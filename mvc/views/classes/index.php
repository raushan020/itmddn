<div class="">

    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="ti ti-book"></i> <?=$this->lang->line('panel_title')?> </h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

          <li class="active"><?=$this->lang->line('menu_classes')?></li>
        </ol>
      </div>
    </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
<?php $usertype = $this->session->userdata("usertype");

          if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
        ?>  
            <div class="card-body">
<!-- new tab bar -->
   
              <div class="col-sm-6 nopading">
              <div class="advance-tab boredr_none">
        
               <ul class="nav nav-tabs" role="tablist">
	              <?php 
								if (empty($this->session->userdata('DraftClasess')) && empty($this->session->userdata('TrashClasess'))) {
                  
									$active = "active";
								}else{
									$active = "";
								}
								?>
								<!-- <li class="<?php echo $active ?>">
									<a  aria-expanded="false" onclick="AllClasess()">All <span style="font-size: 11px;">(<?php echo $all_count; ?>)</span></a>
								</li> -->
								<li class="<?php echo $active ?>">
									<a class="active-btn" aria-expanded="true" onclick="ActiveClasess()">Active <span style="font-size: 11px;">(<?php echo $ActiveClasess_count; ?>)</span></a>
								</li>
								<li <?php if ($this->session->userdata('DraftClasess')){ ?> class="active" <?php  } ?>>
									<a class="draft-btn" aria-expanded="false" onclick="DraftClasess()">Draft <span style="font-size: 11px;">(<?php echo $DraftClasess_count; ?>)</span></a>
								</li>
								<li <?php if ($this->session->userdata('TrashClasess')){ ?> class="active" <?php  } ?>>
									<a class="delete-btn" aria-expanded="false" onclick="TrashClasess()">Trash <span style="font-size: 11px;">(<?php echo $TrashClasess_count; ?>)</span></a>
								</li>
                </ul>

</div>
</div>
<div class="col-sm-6 nopading">
    <div class="pull-right">
      <div class="btn-group">
        <a href="<?php echo base_url('classes/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
      </div>
   </div>
</div>
<div class="clearfix"></div>


         </div>
         
         <div class="theme_input_blue">
				<div class="col-sm-12">
	                <div class="col-sm-6">
	                    <div class="">
	                        <form style="" class="form-horizontal" role="form" method="post">
	                            <div class="form-group">
	                                <label for="classesID" class="control-label">
	                                    Course
	                                </label>
	                                <div class="">
                                    <?php
                                      $array = array("0" => "Select Course");

                                      foreach ($classes as $classa) {

                                        $array[$classa->classesID] = $classa->classes;

                                      }
                                      echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('classesIDForfilterCourses')), "id='classesID' class='form-control custom-select' onchange ='coursesFilter($(this).val())' ");
                                    ?>
	                                </div>
	                                <div class="clearfix"></div>
	                                <div class="forReset">
	                                	<a style="cursor:pointer;" onclick="ResetCourses()">Reset </a>
	                                </div>
	                            </div>
	                        </form>
	                    </div>
	                </div>

                <div class="col-sm-6">

                    <div class="">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="control-label">

                                 Course Duration

                                </label>

                                <div class="">
                                    <select id="durationID" class="form-control custom-select" onchange="CoursesdurationFilter($(this).val())">
                                    <option value="">Select</option>
                                    <?php for ($i=1; $i <6 ; $i++) { 
                                     ?>
                                   <option value="<?php echo $i ?>" <?php if($this->session->userdata('durationID')==$i){echo "selected";} ?>><?php echo $i ?> Year</option>
                                   <?php } ?>
                                  </select>

                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetdurationID()">Reset </a></div>

                            </div>

                        </form>

                    </div>

                </div>      
          
      </div>
    </div>
         <div class="clearfix"></div>
       <?php } ?>
         <!-- col-sm-12 dusra -->
<div class="col-sm-12">
<div class="nav-tabs-custom">
   <div class="">
      <div id="all" class="tab-pane active">
        <div id="hide-table">
          <?php $usertype = $this->session->userdata("usertype");

          if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
        ?>
           <form action="<?php echo base_url() ?>classes/multipleAction" method = "post"onsubmit="return confirm('Do you really want to submit the form?');">
                <div class="action-layout">
                    <ul>
                        <li>
                            <label class="nexCheckbox">Check
                                <input type="checkbox" name="select_all" id="select_all">
                                <span class="checkmark checkmark-action-layout"></span>
                            </label>
                        </li>
                        <li class="active-btn">
                            <input type="submit" class="btn btn-success btn-active etsfilertButton disabled"  name="Active" value="Active" disabled>
                        </li>
                        <li class="active-btn">
                            <input type="submit" class="btn btn-info btn-draft etsfilertButton disabled" name="Draft" value="Draft" disabled>
                        </li>
                        <li class="active-btn">
                            <input type="submit" class="btn btn-danger btn-delete etsfilertButton disabled" name="Delete" value="Delete" disabled>
                        </li>
                    </ul>
                </div>    

                
                <div id="hide-table">

          			  <table id="courses" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">
          						<thead>

          							<tr>
          								<th><?=$this->lang->line('slno')?></th>
                          <th>S no.</th>
          								<th>Course Code</th>
          								<th><?=$this->lang->line('courses_name')?></th>
          								<th><?=$this->lang->line('duration')?></th>
                          <th>Course Type</th>
          								<th><?=$this->lang->line('mode')?></th>
          								<th><?=$this->lang->line('fee')?> (<i class="fa fa-inr"></i>) </th>
          								<th><?=$this->lang->line('courses_note')?></th>
                          <th>Department Name</th>
          								<th><?=$this->lang->line('action')?></th>
          							</tr>
          						</thead>
          						<tbody>
          						</tbody>
          					</table>
          </div>
        
      </div>
            </form>
          <?php } else { ?>
                <div id="hide-table">
                  <table id="courses_views" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">
                      <thead>
                        <tr>
                          <th>S no.</th>
                          <th>Course Code</th>
                          <th><?=$this->lang->line('courses_name')?></th>
                          <th><?=$this->lang->line('duration')?></th>
                          <th>Course Type</th>
                          <th><?=$this->lang->line('mode')?></th>
                          <th><?=$this->lang->line('fee')?> (<i class="fa fa-inr"></i>) </th>
                          <th><?=$this->lang->line('courses_note')?></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
          </div>
        <?php } ?>
         </div>
       </div>
      </div>
   </div>
   <!-- nav-tabs-custom -->
</div>
   </div>
   
</div>
<div class="clearfix"></div>
   <!-- Body -->
<!-- col-sm-12 for tab -->
<!-- end table form here -->
 <script src="<?php echo base_url('assets/inilabs/etsClasses.js'); ?>" type="text/javascript" charset="utf-8" async defer></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.mycheckbox').each(function(){
                this.checked = true;
                $(".etsfilertButton").removeClass("disabled");
                $(".etsfilertButton").removeAttr("disabled");
            });
        }else{
             $('.mycheckbox').each(function(){
                this.checked = false;
                $(".etsfilertButton").addClass("disabled");
                 $(".etsfilertButton").attr("disabled",true);
            });
        }
    });
    $('.table').on('click', '.mycheckbox', function(){

        if($('.mycheckbox:checked').length == $('.mycheckbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
         if($('.mycheckbox:checked').length >=1){
    $(".etsfilertButton").removeClass("disabled");
    $(".etsfilertButton").removeAttr("disabled");
}else{
$(".etsfilertButton").addClass("disabled");
 $(".etsfilertButton").attr("disabled",true);
}
    });
});
</script>

<script type="text/javascript">
    function ResetCourses(){
      $('#courses').DataTable().state.clear();
        $.ajax({
            type: 'POST',

            url: "<?=base_url('classes/ResetCourses')?>",

            data:{ResetSesession:'ResetSesession'},

            dataType: "html",

            success: function(data) {
                location.reload();

            }

        });
    }
</script>

<style type="text/css">
   .boredr_none .nav-tabs{
    border-bottom: 0px solid #e8edef;
   }
   .nopading{
      padding-right:0px;
      padding-left:0px;
   }
   .theme_input_blue .form-control{
  /* background: #fff;*/
   color:#fff;
   }
  .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #fff;
}
.theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */
  color: #fff;
}
.theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */
  color: #fff;
}
.theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */
  color: #fff;
}
.forpostionReletive {
    position: relative;
}
.postionAbsoluter {
    position: absolute;
    right: 7px;
    top: 8px;
}
.action-layout ul li{
   display:inline-block;
}
</style>