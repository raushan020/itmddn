<div class="row page-titles">

	<div class="col-md-5 align-self-center">

		<h3 class="text-themecolor"><i class="fa fa-user"></i> Promote Student</h3>

	</div>

	<div class="col-md-7 align-self-center">

		<ol class="breadcrumb">

			<li class="breadcrumb-item">

				<i class="fa fa-bullseye"></i> 

				<a href="<?=base_url("dashboard/index")?>">

					<?=$this->lang->line('menu_dashboard')?>

				</a>

			</li>

			<li class="breadcrumb-item">

				<a href="<?=base_url("student/index")?>">

					<?=$this->lang->line('menu_student')?>

				</a>

			</li>

			<li class="breadcrumb-item active">Promote Student</li>

		</ol>

	</div>

</div>

<div class="container-fluid">
	<a href="<?=base_url("student/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>

	<div class="row">

		<div class="">

			<div class="card">

				<div class="card-body">

					<style type="text/css">

						.top_heading{

							text-align:center;

						}

					</style>
					<?php $checkstudentsemester="";$classduration="";$classdurationmode="";$lastsemester=""; ?>
					<form action="<?php echo base_url() ?>student/promote_student" method='post'>

						<table class="table">

							<thead>

								<tr>

									<th>Student name</th>

									<th>Father name</th>

									<th>Course name</th>

									<th>Semester</th>

									<th>Promote In</th>

									<th>Action</th>

								</tr>

							</thead>

							<tbody>

								<?php

									foreach ($total_student as $key => $value) 

									{

								?>

										<tr>
											<?php $checkstudentsemester=$value->yearsOrSemester ?>
											<td><?php echo $value->name ?></td>

											<td><?php echo $value->father_name ?></td>

											<td><?php echo $value->classes ?></td>

											<td><?php echo $value->yearsOrSemester ?></td>

											<?php if ($classesRow->mode==1)
											{
												if($this->session->userdata('yos_filter') != CallYears((int)$classesRow->duration))
												{ ?>
													<td>

												<select name="yos_filter[]" id="yos_filter" class='form-control'>

													<?php 
													$next=0;
														if ($classesRow) 

														{

															$looping = (int)$classesRow->duration;
															$classduration=$looping;
															$classdurationmode=$classesRow->mode;
															if ($classesRow->mode==1) 

															{
																$lastsemester=CallYears($looping);
																for ($i=1; $i <=$looping; $i++) 

																{

																	if ($this->session->userdata('yos_filter')==CallYears($i))

																	{

																		$next =  $i+1;
																		$select = ($next == $i)?"selected":"";
																		echo "<option ".$select." value=".CallYears($next).">".str_replace('_', ' ', CallYears($next))."</option>";

																	}
                                     							}

															}
														}

													?>

												</select>

											</td>
											<?php }
											else
											{ 
												$looping = (int)$classesRow->duration;
												$classduration=$looping;
												$classdurationmode=$classesRow->mode;
												$lastsemester=CallYears($looping);
											?>
												<td><?php echo "Pass Out" ?></td>
											<?php }
																							
											}
											elseif ($classesRow->mode==2)
											{
											  	if ($this->session->userdata('yos_filter') != CallSemester((int)2*$classesRow->duration))
												{ ?>
												<td>

												<select name="yos_filter[]" id="yos_filter" class='form-control'>

													<?php 
													$next=0;
														if ($classesRow) 

														{

															$looping = (int)$classesRow->duration;
															$classduration=$looping;
															$classdurationmode=$classesRow->mode;
															if ($classesRow->mode==2) 

															{
																$lastsemester=CallSemester(2*$looping);
																for ($i=1; $i <=(2*$looping); $i++) 

																{

																	if ($this->session->userdata('yos_filter')==CallSemester($i))

																	{

																		$next =  $i+1;
																		$select = ($next == $i)?"selected":"";
																		echo "<option ".$select." value=".CallSemester($next).">".str_replace('_', ' ', CallSemester($next))."</option>";

																	}																	

																}

															}

														}

													?>

												</select>

											</td>
											<?php }
											else
											{
												$looping = (int)$classesRow->duration;
												$classduration=$looping;
												$classdurationmode=$classesRow->mode;
												$lastsemester=CallSemester(2*$looping);
											?>
												<td><?php echo "Pass Out" ?></td>
											<?php }
											} ?>

											<td>

												<label>

													<input type="checkbox" name="checked_id[]" value="<?php echo $value->studentID ?>" checked>

												</label>

											</td>

										</tr>

								<?php 

									} 

								?>

							</tbody>

						</table>

						

						<div class="col-md-12">

							<?php if($checkstudentsemester == $lastsemester)
							{ ?>
							 	<input type="hidden" name="passoutstudent" value="10">
								<center><button class="btn btn-success ">Pass Out</button></center>
								
							<?php }
							else
							{ ?>
							 	<input type="hidden" name="passoutstudent" value="0">
								<center><button class="btn btn-success">Promote</button></center>
							<?php } ?>

						</div>

					</form>

				</div>

			</div>

		</div>

	</div>

</div>