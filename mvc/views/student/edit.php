<style type="text/css">
    #upload_images_mess{
        display:none;
        color:green;
    }
</style>

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-user"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-dashboard"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("student/index/$set")?>"><?=$this->lang->line('menu_student')?></a></li>

            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('panel_title')?></li>
            </ol>
        </div>
    </div>

    <!-- form start -->

    <div class="container-fluid">
        <a href="<?=base_url("student/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
      <div class="row">
            <div class="card">
            <div class="card-body">

        

            <div class="col-sm-12">
    <p>Field are required with <span class="red-color">*</span></p>

                <form class="form-horizontal" role="form" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">

                   <h3  class="border_heading">Personal Information</h3>

                    <?php 

                        if(form_error('name')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="name_id" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_name")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name_id" required="required" name="name" value="<?=set_value('name', $student->name)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>

                        <?php 

                            if(form_error('father_name'))

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="guardiaName" class="col-sm-2 control-label">

                                <?=$this->lang->line("father_name")?> <span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <div class="select2-wrapper">

                                    <input type="text" class="form-control" required="required" id="father_name" name="father_name" value="<?=set_value('father_name',$student->father_name)?>" >

                                </div>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('father_name'); ?>

                            </span>

                        </div>



            <?php 

                            if(form_error('mother_name'))

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="guardiaName" class="col-sm-2 control-label">

                                <?=$this->lang->line("mother_name")?> <span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <div class="select2-wrapper">

                                    <input type="text" class="form-control" required="required" id="mother_name" name="mother_name" value="<?=set_value('mother_name',$student->mother_name)?>" >

                                </div>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('mother_name'); ?>

                            </span>

                        </div>
                    <?php 

                        if(form_error('dob')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="dob" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_dob")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control datepicker_quiz_data" id="dob" required="required" name="dob" value="<?=set_value('dob', date("d-m-Y", strtotime($student->dob)))?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('dob'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('sex')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_sex")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <?php 

                                echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex", $student->sex), "id='sex' class='form-control' required ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('sex'); ?>

                        </span>



                    </div>


                    <!-- <?php 

                        if(form_error('employment_status')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_employment_status")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <?php 

                                echo form_dropdown("employment_status", array("employed" => "employed", "unemployed" => "unemployed"), set_value("employment_status", $student->employment_status), "id='employment_status' class='form-control' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('employment_status'); ?>

                        </span>



                    </div> -->




                    <?php 

                        if(form_error('email')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_email")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" name="email" value="<?=set_value('email', $student->email)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('phone')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_phone")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" required="required"  id="phonecountry" name="phone" value="<?=set_value('phone', $student->phone)?>" >
                                                            <span id="valid-msg" class="hide">✓ Valid</span>
                                <span id="error-msg" class="hide">Invalid number</span>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('address')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_address")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $student->address)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('address'); ?>

                        </span>

                    </div>

                        

                        <?php 

                            if(form_error('pin')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="address" class="col-sm-2 control-label">

                                <?=$this->lang->line("pin")?>

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="pin" name="pin" value="<?=set_value('pin',$student->pin)?>" >

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('pin'); ?>

                            </span>

                        </div>


                        <?php 

                            if(form_error('nationality')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="address" class="col-sm-2 control-label">

                                <?=$this->lang->line("nationality")?>

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="nationality" name="nationality" value="<?=set_value('nationality',$student->nationality)?>" >

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('nationality'); ?>

                            </span>

                        </div>


                         <?php 

                            if(form_error('aadhar')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                            <label for="address" class="col-sm-2 control-label">

                             Aadhaar no. 

                            </label>

                            <div class="col-sm-6">

                                <input type="text" class="form-control" id="aadhar" name="aadhar" value="<?=set_value('aadhar',$student->aadhar)?>" >

                            </div>

                                          

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('aadhar'); ?>

                            </span>

                        </div>
                        <div class='form-group'>
                            <?php
                                if (isset($error)){
                                    echo $error;
                                }
                            ?>
                            <label for="address" class="col-sm-2 control-label">

                            Upload Aadhaar Card

                            </label>

                            <div class="col-sm-6">
                                <!-- <form method="post" id="upload_form" align="center" enctype="multipart/form-data">   -->
                                    <input type="file" name="aadhar_file" id="aadhar_file" />  
                                    <br />  
                                    <br />  
                                    <input type="button" name="aadhar_upload" id="aadhar_upload" value="Upload" class="btn btn-info" />
                                    <span style="color: red">Use Only jpg | jpeg | png</span>  
                               <!-- </form>   -->
                            </div>
                            <div id="uploaded_image" class="col-sm-4">  
                            </div> 
                        </div>
                        <!-- enrollment -->
                    <?php 

                        if(form_error('roll')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="roll" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_roll")?>
                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="roll" name="enrollment" value="<?=set_value('roll', $student->enrollment)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('roll'); ?>

                        </span>

                    </div>
<!-- roll -->
<!-- 
                    <?php 

                        if(form_error('roll_no')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="roll_no" class="col-sm-2 control-label">

                            Roll No.
                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="roll_no" name="roll" value="<?=set_value('roll_no', $student->roll)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('roll_no'); ?>

                        </span>

                    </div> -->



                    <?php 

                        if(isset($image)) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">

                            <?=$this->lang->line("student_photo")?>

                        </label>

                        <div class="col-sm-4 col-xs-6 col-md-4">

                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  

                        </div>



                        <div class="col-sm-2 col-xs-6 col-md-2">

                            <div class="fileUpload btn btn-success form-control">

                                <span class="fa fa-repeat"></span>

                                <span><?=$this->lang->line("upload")?></span>

                                <input type="file" class="upload" name="upload_image" id="upload_image_student"  />

                            </div>
                            <div id="uploaded_image_student"></div>

                        </div>

                         <span class="col-sm-4 control-label col-xs-6 col-md-4" id = "upload_images_mess">

                           <i class="fa fa-check" aria-hidden="true"></i> Uploaded
                            <?php if(isset($image)) echo $image; ?>

                        </span>

                    </div>


                    <!-- start course details -->

                    <h3  class="border_heading">Course Details</h3>

                    <?php 

                        if(form_error('classesID')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classesID" class="col-sm-2 control-label">

                            <?=$this->lang->line("student_classes")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                           <?php

                                $array = array(0 => $this->lang->line("student_select_class"));

                                foreach ($classes as $classa) {

                                    $array[$classa->classesID] = $classa->classes;

                                }

                                echo form_dropdown("classesID", $array, set_value("classesID", $student->classesID), "id='classesID' class='form-control' onchange='ajaxGet_subCourses($(this).val())'  required ");

                            ?>
                            <input type="hidden" id="studentIdForEDit" value="<?php echo  $this->uri->segment(3) ?>" name="">
                        </div>

                   <span class="col-sm-4 showLoaderSubcour">
                        <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                     </span>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classesID'); ?>

                        </span>

                    </div>
  <div id="subCourseID">
    <?php if($classes_single->IsSubCourse==1): ?>
                 <div class='form-group' >
                        <label for="classesID" class="col-sm-2 control-label">

                         Sub Courses <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                           <?php

                                $array = array(0 =>"Select Sub Courses");


                                foreach ($get_sub_courses as $sub_courses) {

                                    $array[$sub_courses->sub_coursesID] = $sub_courses->sub_course;
                                }
                                echo form_dropdown("sub_coursesID", $array, set_value("sub_coursesID", $student->sub_coursesID), "id='' class='form-control' required");

                            ?>

                        </div>
                    </div>
                <?php endif; ?>
                </div>

                <div id="ModeType">
                    <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Semester/Year<span class="red-color">*</span></label>
                                   
                                    <input type="hidden" value="<?php echo $student->education_mode  ?>" name="education_mode">
                                    <div class="col-sm-6" id="yearID2">
                                   <?php  if ($student->education_mode==1) { ?>
                                 <select class='form-control' name='yearsOrSemester' required="required" onchange='GetEntryTypeData($(this).val())' id='yearIDSelect' value='set_value('yearId')>
                                     <?php  for ($i=1; $i <=$classes_single->duration; $i++) {  ?>
                                     <option value="<?php echo CallYears($i) ?>" <?php if(CallYears($i)==$student->yearsOrSemester){echo "selected";} ?> > <?php echo str_replace('_', ' ', CallYears($i) ) ?></option>
                                <?php    } ?>
                                    </select>
                                <?php  } else { ?>
                                     <select class='form-control' name='yearsOrSemester' required="required" onchange='GetEntryTypeData($(this).val())' id='semester_idSelect' value='<?php echo set_value('semesterId'); ?>'>
                                     <option value=''>Semester</option>";
                                      <?php  for ($i=1; $i <=(2*$classes_single->duration); $i++) { ?>
                                     <option value="<?php echo CallSemester($i) ?>" <?php if(CallSemester($i)==$student->yearsOrSemester){echo "selected";} ?>><?php echo  str_replace('_', ' ', CallSemester($i) ) ?></option>
                                   <?php  } ?>
                                   </select>
                                 <?php } ?>
                                </div>

                                                    
                                   <!--  <span class="col-sm-4 control-label">
                                       </span> -->
                            </div>
                        </div>
                        

                        <?php 

                          $SessionArray = explode('-', $student->session);

                         ?>
                                <div class="form-group">
                                        <label for="" class="col-sm-2 control-label" >Session<span class="red-color">*</span></label>
                                        <div class="col-sm-3"> 
                                            <input type="text" placeholder="From" value="<?php if($student->session) {echo $SessionArray[0];} ?>" name="sessionFrom" class="form-control CalenderYear">
                                        </div>
                                        <div class="col-sm-3"> 
                                            <input type="text" placeholder="To" value="<?php if($student->session){ echo $SessionArray[1]; }?>"  name="sessionTo" class="form-control CalenderYear">
                                        </div>

                                            <span class="col-sm-4 control-label">
                                            </span>
                                     </div>
                                   
<!-- end Course Details --> 
<!-- start Academic Details form here -->
<div class="Course Details">
      <input type="hidden" name="studentID" id = "studentID" value="<?php echo $this->uri->segment(3) ?>">
                <div id="ApendDataAcadamic">  
                        <h3 class="border_heading">Academic Details</h3>
                            <?php  
                            $j = 0;
                            $k =  0; 
                             if ($classes_single->isRequired==1) {
                                        $spanRequired = "<span class='red-color'>*</span>";
                                        $required =  "required";
                                    }else{
                                        $required = "";
                                        $spanRequired = ""; 
                                    }
                                foreach ($academic_details as $key => $value) {
                            ?>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label"><?php echo $value->education; ?> <?php  echo $spanRequired ?></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="year[]" <?php  echo $required  ?> id="year" value="<?php echo set_value('year',$value->year_passing); ?>">
                                             <option value="">Year</option>
                                            <?php
                                            for($i=2025;$i>=1947;$i--){ ?>
                                            <option value="<?php echo $i; ?>" <?php if($i==$value->year_passing){echo "selected";} ?> ><?php echo $i; ?></option>
                                            <?php } ?> 
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="hidden" name="education_detailsID[]" value="<?php echo $value->education_detailsID ?>">
                                        <input type="hidden" name="academic_detailsID[]" value="<?php echo $value->academic_detailsID ?>">
                                        <input type="hidden" name="education_name[]" <?php  echo $required  ?> value="<?php echo $value->education ?>">
                                        <input class="form-control" name="subject[]" <?php  echo $required  ?> id="subject" placeholder="Subject" value="<?php echo set_value('subject',$value->subject); ?>">
                                    </div>
                                    <div class="col-sm-2">
                                        <input class="form-control" name="board[]" <?php  echo $required  ?> id="board" placeholder="Board" value="<?php echo set_value('board',$value->board_name); ?>">
                                    </div>
                                    <div class="col-sm-2">
                                        <input class="form-control" name="percent[]" <?php  echo $required  ?> id="percent" placeholder="%" value="<?php echo set_value('percent',$value->percentage); ?>">
                                    </div>
                                    <span class="col-sm-4 control-label">
                                       </span>
                            <div class="clearfix"></div>

                                    <div class="certifixa">
									   <label class="col-sm-4"><?php echo $value->education; ?> <?php  echo $spanRequired ?> Certificate</label>
									   <div class="col-sm-4">
									   <label>Marksheet</label>
									   <input type="file"  name="marksheet<?php echo $j++ ?>" title="Marksheet upload" ><?php echo $value->marksheet_detail?>
									   </div>
									   <div class="col-sm-4">
									   <label>Certificate</label>
									   <input type="file"  name="certificate<?php echo $k++ ?>" title="Certificate upload"><?php echo $value->certificate_detail?>
									   </div>
									   <div class="clearfix"></div>
									   <div class="clearfix"></div></div>
                            </div>
                            <?php  } ?>
                          

                
                       </div>
                    <!--    </label> -->
                    </div>
           

<!-- end acadmic details form here -->


                   

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("update_student")?>" >

                        </div>

                 



                </form>



            </div> <!-- col-sm-8 -->

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->

<style type="text/css">
.showLoaderSubcour{
        display:none;
    }
        .positionRelative{
      position:relative;
    }
    .positionAbsolutePassword{
    position:absolute;
    top:0px;
    position: absolute;
    top: 5px;
    right: 23px;
    }
    .hide{
        color:red;
    }
    #valid-msg{
        color:green;
    }
    #error-msg{
        color:red;
    }
</style>

<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });



$('#classesID').change(function(event) {

    var classesID = $(this).val();

    if(classesID === '0') {

        $('#classesID').val(0);

    } else {

        $.ajax({

            type: 'POST',

            url: "<?=base_url('student/sectioncall')?>",

            data: "id=" + classesID,

            dataType: "html",

            success: function(data) {

               $('#sectionID').html(data);

            }

        });

    }

});
</script>
<script type="text/javascript">
  
  function getvalCourceMode(sel)
{
// alert(sel.value);
 if (sel==2) {
$("#semester_id").show();
$("#yearID").hide();   
}else{
  $("#semester_id").hide();
  $("#yearID").show();
}

}

  function getvalEntryMode(sel)
{

 if (sel=="first_semester" || sel== "first_year") {
$("#fresher").show();
$('#fresherSelect').removeAttr("disabled",true);
$('#lateralSelect').attr("disabled",true);
$("#lateral").hide(); 
$("#transfer").hide();
$('#transferSelect').attr("disabled",true);
}
 else if(sel=="second_semester" || sel=="third_semester" || sel=="second_year") {
    $("#fresher").hide();
    $('#fresherSelect').attr("disabled",true);
    $("#lateral").show();
    $('#lateralSelect').removeAttr("disabled",true); 
    $("#transfer").hide();
    $('#transferSelect').attr("disabled",true);
}
else{
  $("#fresher").hide();
  $('#transferSelect').attr("disabled",true);
  $("#lateral").hide();
  $('#lateralSelect').attr("disabled",true);
  $("#transfer").show();
  $('#transferSelect').removeAttr("disabled",true); 
}

}
</script>
<script type="text/javascript">
    $(".CalenderYear").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});
</script>
<style type="text/css">
.red-color{
    color:red;
}
    .hide{
        color:red;
    }
</style>
<div id="uploadimageModalStudent" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_student" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                          <button class="btn btn-success crop_image_student">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
//     function validateForm() {
// aadhar = document.getElementById("aadhar").value;

// var adharcard = /^\d{12}$/;
// //var adharsixteendigit = /^\d{16}$/;
// if (aadhar != '') {
//      if (!aadhar.match(adharcard))
//      {
//          alert("Invalid Aadhar Number");
//          return false;
//      }
//  }
 // if (adhar != '')
 // {
 //     if(!adhar.match(adharsixteendigit))
 //     {
 //         alert("Invalid Aadhar Number");
 //         return false;
 //     }
 // }
// }
</script>
<script>  
 $(document).ready(function(){  
      $('#aadhar_upload').on('click', function(e){  
           e.preventDefault();  
           if($('#aadhar_file').val() == '')  
           {  
                alert("Please Select the File");  
           }  
           else  
           {  
              var aadharfile=$('#aadhar_file').val();
              var studentID='<?php echo $student->studentID ?>';
              var fd = new FormData();
              var files = $('#aadhar_file')[0].files[0];
              fd.append('aadhar_file',files);
                $.ajax({  
                     url:"<?php echo base_url(); ?>student/aadharcardupload?studentID="+studentID,   
                     method:"POST",  
                     data:fd,
                     contentType: false,
                     processData: false,  
                     success:function(data)  
                     {  
                          $('#uploaded_image').html(data);  
                     }  
                });  
           }  
      });  
 });  
 </script>
