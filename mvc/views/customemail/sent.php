
<div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-send" aria-hidden="true"></i> Sent Email </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Sent</li>
         </ol>
      </div>
   </div>
<!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

                <div id="hide-table">

                <table id="usersTable" class="table table-striped table-bordered table-hover dataTable no-footer">
                    <thead>
                        <tr>
                          <th class="">#</th>
                            <th class="">Date</th>
                            <th class="">To</th>
                            <th class="">Subject</th>
                            <th class="">Body</th>
                            
                            <!-- <th class="col-lg-2"><?=$this->lang->line('action')?></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($sent)
                        {
    $i = 1;

                            foreach ($sent as $key => $value)
                            { ?>
                                <tr>
                                    <td class=""><?php echo $i++ ?></td>
                                    <td class=""><?php echo date('d-m-Y h:i:s', strtotime($value->sentdate)); ?></td>
                                    <td class=""><?php echo $value->sentto; ?></td>
                                    <td class=""><?php echo $value->subject; ?></td>
                                    <td class=""><?php echo $value->msg; ?></td>                                    
                                    <!-- <td class="col-lg-2"><a href="#modal-2" class="btn" name="sendmailbyadmin" data-toggle="modal" data-target="#modal-2"><i class="fa fa-eye"></i></a></td>
                                </tr> -->
                                <!-- Modal Start-->
                              </tr>
                     
                          <?php  }
                        }  ?>
                    </tbody>
                </table>

                </div>


                </div>
              </div>

            </div> <!-- col-sm-12 -->

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->


