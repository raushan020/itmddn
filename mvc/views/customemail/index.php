<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-envelope" aria-hidden="true"></i> Email </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Email</li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
   <!-- <div class="form-row">
      <div class="form-group">
          <label for="courseName" class="col-md-2 col-form-label">Type</label>
           <div class="col-md-10">
         <div class="col-sm-3">
            <input type="radio" name="cod" id="chde1" value="1"><label for="chde1">Sem</label>
         </div>
         <div class="col-sm-3">
         <input type="radio" name="cod" value="0" id="chde2" checked><label for="chde2">Year</label>
         </div>
         <div class="clearfix"></div>
           </div>
      </div>
             </div> -->
   <!-- form start -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
               <div class="card-body">
                  <div class="col-sm-12">
                     <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                        <?php if(form_error('classesID[]')){ ?>
                        <div class="alert alert-danger">
                           <strong>Danger!</strong><?php echo  form_error('classesID[]'); ?>
                        </div>
                        <?php } ?>
                        
                        <div id="subCourseID">
                        </div>
                        <div id="fetchYearsAndSem"  multiple="multiple">
                        </div>
                        <div id="appendSujectsDetails">
                        </div>
                        <?php 
                           if(form_error('title')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                        <label for="title" class="col-sm-2 control-label">
                         Subject
                        </label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" placeholder="Subject" autocomplete="off" name="title" value="<?=set_value('title')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('title'); ?>
                        </span>
                  </div>
               </div>
               <?php 
                  if(form_error('notice')) 
                  
                      echo "<div class='form-group has-error' >";
                  
                  else     
                  
                      echo "<div class='form-group' >";
                  
                  ?>
               <label for="notice" class="col-sm-2 control-label">
               Description
               </label>
               <div class="col-sm-6">
               <textarea class="form-control" id="editor" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
               </div>
               <span class="col-sm-4 control-label">
               <?php echo form_error('notice'); ?>
               </span>
            </div>
            <?php $usertype = $this->session->userdata("usertype"); ?>
            <?php if($usertype != "ClgAdmin")
            {?>
                <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
                <div class="dsd">
                <label class="nexCheckbox">Chairman
                <input type="checkbox" name="super" value="<?php echo "Super Admin" ?>" >
                <span class="checkmark checkmark-action-layout super"></span>
                </label> 
                <?php foreach ($superadmin as $superadmin)
                { ?>
                  <input type="hidden" name="superadmin[]" value="<?php echo $superadmin;?>">
               <?php }   ?>
                </div>
                </div>
           <?php }?>
           <?php if($usertype != 'Professor')
            { ?>
              <div class="col-sm-12" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
              <div class="dsd">
              <label class="nexCheckbox">Professor
              <input type="checkbox" name="professor" class='checkallprofessor' id='checkallprofessor' onclick="selectAllnoticechecked(this,1)" value="<?php echo "Professor" ?>" >
              <span class="checkmark checkmark-action-layout professor"></span>
              </label>
              <?php $count=count($Professor); foreach($Professor as $Professor)
              {?>
                
                 <input type="checkbox" name="professorid[]" id="fool" class="professorid" value="<?php echo $Professor->professorID; ?>" ><?php echo $Professor->name; ?> &nbsp;&nbsp;
                 <?php if($count == 5 || $count == 10 || $count == 15 || $count == 20 || $count == 25 || $count == 30 || $count == 35 || $count == 40 || $count == 45 || $count == 50 || $count == 55){ ?> </br>  <?php } ?>
             <?php } ?>
              </div>
              </div>
           <?php } ?>
            <?php if($usertype != "Admin")
            {?>
              <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
              <div class="dsd" style="display: flex;">
                
              <label class="nexCheckbox">Admin
                
              <input type="checkbox" name="admin" value="<?php echo "Admin" ?>" >
              <span class="checkmark checkmark-action-layout admin"></span>
              </label> 
              <?php 
                  foreach ($admin as $admins){ 
                ?>

              <?php  
                    if ($admins['userID']==31) {
                        echo '<label class="nexCheckbox">';
                        echo 'Registrar';
                        echo '<input type="checkbox" name="admin" value="Admin" >
              <span class="checkmark checkmark-action-layout admin"></span>
              </label>';
                    }
                    if ($admins['userID']==28) {
                      echo '<label class="nexCheckbox">';
                        echo 'Principal'; 
                        echo '<input type="checkbox" name="admin" value="Admin" >
              <span class="checkmark checkmark-action-layout admin"></span>
              </label>';
                    }
                ?>
              
                
             
                <input type="hidden" name="adminid[]" value="<?php echo $admins['userID'];?>">
             <?php }   ?>
              </div>
              </div>
           <?php } ?>
            <?php if($usertype != "Accountant")
            {?>
              <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
              <div class="dsd">
              <label class="nexCheckbox">Accountant
              <input type="checkbox" name="act" value="<?php echo "Accountant" ?>" >
              <span class="checkmark checkmark-action-layout act"></span>
              </label> 
              <?php foreach ($Accountant as $Accountant)
              { ?>
                <input type="hidden" name="accountant[]" value="<?php echo $Accountant->userID;?>">
             <?php }   ?>
              </div>
              </div>
           <?php } ?>
            
           
       <div class="col-md-12" >
  <center><input type="submit" class="btn btn-success" value="Send Mail" ></center>
            
          </div>                               
         </div>
                                                                                
      </div>
      </form>
   </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">
   jq('#date').datepicker();
   
   jq('#notice').jqte();
   
</script>
<script type="text/javascript">
   function selectAll(){
      var items=document.getElementsByName('classesID[]');
      var items_sem=document.getElementsByClassName('yearOrSemester');
   
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=true;
      }
      for(var i=0; i<items_sem.length; i++){
          if(items_sem[i].type=='checkbox')
              items_sem[i].checked=true;
      }
   }
   
   function UnSelectAll(){
      var items=document.getElementsByName('classesID[]');
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=false;
      }
   }           
</script>
<!-- <script src='//cdn.tinymce.com/4/tinymce.min.js'></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script>
    initSample();
</script>
<!-- <script type="text/javascript">
   tinymce.init({
   
       selector:"textarea",
   
       width:"100%",
   
       height:"200",
   
       relative_urls:false,
   
       remove_script_host:false,
   
       theme: "modern",
   
       plugins: [
   
           "advlist autolink lists link image charmap print preview hr anchor pagebreak",
   
           "searchreplace wordcount visualblocks visualchars code fullscreen",
   
           "insertdatetime media nonbreaking save table contextmenu directionality",
   
           "emoticons template paste textcolor colorpicker textpattern"
       ],
   
       toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager | fontsizeselect",
   
       toolbar2: "print preview media | forecolor backcolor emoticons",
   
       fontsize_formats: "1px 2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px 34px 36px",
   
         style_formats: [
   
               {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
   
               {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
   
               {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
   
               {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
   
               {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
   
               {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
   
               {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
   
               {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
   
               {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
   
               {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
   
               {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
   
               {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
   
               {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
   
           ],
   
       image_advtab: true,
   
       templates: [
   
           {title: 'Test template 1', description: 'Test 1'},
   
           {title: 'Test template 2', description: 'Test 2'}
       ],
   
      external_filemanager_path:"<?php echo base_url() ?>filemanager/",
   
      filemanager_title:"Responsive Filemanager" ,
   
      external_plugins: { "filemanager" : "<?php echo base_url() ?>filemanager/plugin.min.js"}
   
   });
   
</script> -->
<script type="text/javascript">
  // function selectAllnoticechecked(){
  //     var items=document.getElementsByName('professorid[]');
      
   
  //     for(var i=0; i<items.length; i++){
  //         if(items[i].checked==true){
  //             items[i].checked=true;
  //           }
  //           // else{
  //           //   items[i].checked=false;
  //           // }
  //     }
      
  //  }

  //  
     // $(document).ready(function() {

     //      $("#checkallprofessor").click(function() {
     //          $("#fool").prop("checked", true);
     //        });
     //      $("#checkallprofessor").click(function() {
     //          $("#fool").prop("checked", false);
     //        });
     // });
</script>
  <script type="text/javascript">
   //  function selectAllnoticechecked(){
   //       var selectAllCheckbox=document.getElementsByName("professorid");
   //       // alert(selectAllCheckbox);
   //       if(selectAllCheckbox.checked==true){
   //        var checkboxes =  document.getElementsByName("professorid[]");
   //         for(var i=0, n=checkboxes.length;i<n;i++) {
   //          checkboxes[i].checked = true;
   //         }
   //        }else {
   //         var checkboxes =  document.getElementsById("professorid[]");
   //         for(var i=0, n=checkboxes.length;i<n;i++) {
   //          checkboxes[i].checked = false;
   //         }
   //        }
   // }

    function selectAllnoticechecked(val,key){
      // alert(key);
     var items_sem=document.getElementsByClassName('professorid'+key);
   
      if(val.checked){
   
          $('.professorid').each(function(){
              this.checked = true;
          });
      }else{
           $('.professorid').each(function(){
              this.checked = false;
              $(".etsfilertButton").addClass("disabled");
          });
      }                
   }
   
 </script>