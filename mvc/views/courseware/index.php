<style>
  .add_sub_11{
    float: none !important;
  }
  
</style>
<div class="">

    <div class="row page-titles">

      <div class="col-md-5 align-self-center">

         <h3 class="text-themecolor"><i class="fa fa-columns"></i> Courseware </h3>

      </div>

      <div class="col-md-7 align-self-center">

         <ol class="breadcrumb">
           <?php $usertype = $this->session->userdata("usertype");
                                      // print_r($usertype);die; 
                                      if($usertype=="Admin" || $usertype=="ClgAdmin" || $usertype == "HOD")
                                      {?>
                                        <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li><li><a href="<?=base_url("professor/index")?>"></i>professor</a></li>
                                      <?php } else
                                      {?>
                                          <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                                     <?php } ?>

            

            <li class="active">Courseware</li>

         </ol>

      </div>

   </div>

   

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card-body">

                  <div class="clearfix"></div>
            
                  <div class="container-fluid">

      <div class="row">
                          <form style="" class="form-horizontal"  autocomplete="off" role="form">

                     <?php 
                     $getSemester = $this->input->get('yearsOrSemester');
                        $year=$this->input->get('year');
                        $mode=$this->input->get('mode');
                        $type = $this->input->get('type');

                        if($type){

                          $type = $this->input->get('type');

                        }else{

                          $type = "filter";

                        }
                        $date = $this->input->get('date');
                         ?>

                     <?php 

                        if($type=="filter") 

                        {

                         ?>

                     <input type="hidden" name="type" value="<?php echo $type ?>">

                    <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Mode</label>

                              <div class="">

                                 <select id="mode" name="mode" required class="form-control" onchange='GetsemesterAtd($(this).val())'>
                                    <option value="0">Select Mode</option>
                                    <option value="14" <?php if($mode == 14){ echo "selected"; }  ?>>Semester Wise</option>
                                    <option value="13" <?php if($mode == 13){ echo "selected"; }  ?>>Year Wise</option>
                                  </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>
                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Semester/Year</label>

                              <div class="">

                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>

                                    <option>Select</option>

                                    <?php 

                                       if ($classesRow) {

                                       

                                        $looping    =  (int) $classesRow->duration;

                                       

                                       if ($classesRow->mode==1) {

                                       

                                         for ($i=1; $i <=$looping; $i++) {

                                       

                                                if (CallYears($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                       

                                        

                                       

                                       }

                                       

                                       }

                                       

                                       

                                       

                                       else{

                                       

                                         for ($i=1; $i <=(2*$looping); $i++) {

                                       

                                       if (CallSemester($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       

                                       }

                                       

                                       }

                                       

                                       }

                                       

                                       ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Year</label>

                              <div class="">

                                 <select id="year" name="year" required class="form-control">
                                    <option value="0">Select Year</option>
                                    <option value="2019" <?php if($year == 2019){ echo "selected"; }  ?>>2019</option>
                                    <option value="2020" <?php if($year == 2020){ echo "selected"; }  ?>>2020</option>
                                    <option value="2021" <?php if($year == 2021){ echo "selected"; }  ?>>2021</option>
                                    <option value="2022" <?php if($year == 2022){ echo "selected"; }  ?>>2022</option>
                                    <option value="2023" <?php if($year == 2023){ echo "selected"; }  ?>>2023</option>
                                    <option value="2024" <?php if($year == 2024){ echo "selected"; }  ?>>2024</option>
                                    <option value="2025" <?php if($year == 2025){ echo "selected"; }  ?>>2025</option>
                                    <option value="2026" <?php if($year == 2026){ echo "selected"; }  ?>>2026</option>
                                  </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-1">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label"></label>

                              <div class="">

                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 15px;" name="Search">

                              </div>

                           </div>

                        </div>

                     </div>

                     <div class="clearfix"></div>
                   </form>
                     <?php } ?>

         <div class="col-md-12 col-sm-6 col-xs-12">

                  

                     <div class="table-wrapper">

                        <div id="editbankloandetails" title="Edit" style="width:900px;display:none"></div>

                        <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="professorexportexce">

                           <thead>

                              <tr style="font-weight:bold">

                                 <td>S.N.</td>
                                 <td>Course Name</td>
                                 <td>Semester</td>
                                 <td>Subject Name</td>                         
                                 <td>Add</td>                                 
                                 
                                 
                              </tr>

                           </thead>

                           <tbody>

                              <?php count($subject); $count=1; foreach($subject as $subjects => $value)
                              {?>

                                   <tr>

                                    <td><?php echo $count?></td>
                                    <td><?php echo $value['classes']?></td>
                                    <td><?php echo $value['yearsOrSemester']?></td>
                                    <td><?php echo $value['subject']?></td>
                                    <td>
                                      <?php $usertype = $this->session->userdata("usertype");
                                      if($usertype=="Professor")
                                      {?>
                                        <a href="<?php echo base_url("syllabus/add_syllabus/".$value['subjectID'].'?type=unit');?>">
                                        
                                        <button class="btn btn-primary primary_2" type="button" title="Add Syllabus" style="font-size: 14px;">
                                        <i class="fa fa-plus"></i>
                                      </button>
                                      </a>

                                    <?php }?>
                                    </td>                                  

                                   </tr>                      

                              <?php $count++; }?>
                              <?php if($subjectyear != 0)
                              {
                               count($subjectyear); $count=$count; foreach($subjectyear as $subjects => $value)
                              {?>

                                   <tr>

                                    <td><?php echo $count?></td>
                                    <td><?php echo $value['classes']?></td>
                                    <td><?php echo $value['yearsOrSemester']?></td>
                                    <td><?php echo $value['subject']?></td>
                                    <td>

                                      
                                      <?php $usertype = $this->session->userdata("usertype");
                                      // print_r($usertype);die; 
                                      if($usertype=="Professor")
                                      {?>
                                      <a href="<?php echo base_url("syllabus/add_syllabus/".$value['subjectID'].'?type=unit');?>">
                                        
                                        <button class="btn btn-primary primary_2" type="button" title="Add Syllabus" style="font-size: 14px;">
                                        <i class="fa fa-plus"></i>
                                      </button>
                                      </a>

                                    <?php }?>
                                    </td>                                 


                                    

                                   </tr>                      

                              <?php $count++; } }?>

                           </tbody>

                        </table>

                        



                     </div>

                  </div>

         </div>

      </div>

   </div>

                  <script type="text/javascript">

                     function idsd(){

                      var atd = $("#date_atd").val();

                      // var topicid=$("#topic").val();

                           var check = true;

                           $("input:radio").each(function(){

                               var name = $(this).attr("name");

                               if($("input:radio[name="+name+"]:checked").length == 0){

                                   check = false;

                                   $("#"+name).html('Please Select');

                               }

                           });

                           

                           if(check){

                               if(atd=='')

                               {

                                  alert('Please select attendance date.');

                                check = false;

                               }

                               

                               return check;

                           }else{

                               

                             return check;

                           }

                      

                     

                     }

                  </script>

               </div>

               <!-- col-sm-12 -->

            </div>

            <!-- row -->

         </div>

         <!-- Body -->

      </div>

      <!-- /.box -->

   </div>

<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classesID').val();

   

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function OnchageDateAtd(date){

   var course =  "<?php echo $this->input->get('course') ?>";

   var subcourse =  "<?php echo $this->input->get('subcourse') ?>";

   var yearsOrSemester =  "<?php echo $this->input->get('yearsOrSemester') ?>";

   var subject =  "<?php echo $this->input->get('subject') ?>";

   var type =  "<?php echo $this->input->get('type') ?>";

   var url = "<?php echo base_url() ?>viewlecture/attendance?type="+type+"&course="+course+"&subcourse=0&yearsOrSemester="+yearsOrSemester+"&subject="+subject+"&date="+date;

   window.location.replace(url);

   

   }

   

</script>

<script>

   var status = '';

   

   

   

   var id = 0;

   

   

   

   $('.onoffswitch-small-checkbox').click(function() {

   

   

   

       if($(this).prop('checked')) {

   

   

   

           status = 'chacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       } else {

   

   

   

           status = 'unchacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       }

   

   

   

   

   

   

   

       if((status != '' || status != null) && (id !='')) {

   

   

   

           $.ajax({

   

   

   

               type: 'POST',

   

   

   

               url: "<?=base_url('professor/active')?>",

   

   

   

               data: "id=" + id + "&status=" + status,

   

   

   

               dataType: "html",

   

   

   

               success: function(data) {

   

   

   

                   if(data == 'Success') {

   

   

   

                       toastr["success"]("Success")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   } else {

   

   

   

                       toastr["error"]("Error")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   }

   

   

   

               }

           });
       }
   }); 
</script>
<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classID').val();

   // alert(val);

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function Getclass(departmentID){

   

   // alert(departmentID);die;

                  $.ajax({

   

                  url: base_url+'AjaxController/class_get',

   

                  type: 'POST',

   

                  data:{departmentID:departmentID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#classID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

  
<!-- <script type="text/javascript">
    function boxDisable(e) {
        
            if($(this).is(":checked"))
            {
              $('#update').show();
                // alert("Checkbox is checked.");
            }
            else if($(this).is(":not(:checked)"))
            {
                $('#update').hide();
                alert("please select atleast one checkbox!!");
            }
        
    }
</script> -->
<!-- <script type="text/javascript">
  $("#updatedataprogress").submit(function()
  {
    // alert("cdnjd");
    if ($('input:checkbox').filter(':checked').length < 1)
    {
        alert("Check at least one Game!");
        return false;
    }
  });
</script> -->









