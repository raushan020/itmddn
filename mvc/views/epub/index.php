 
     <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-amazon"></i> E-Pub</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active">lms/pdf</li>
            </ol>
        </div>
    </div>
    <!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">

                <div class="">
                    <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin")
                   if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
                   $display =  "block";
                   $addmorebutton = "none";
                   $Removemorebutton = "inline-block";
                   }else{
                   $display = "none";
                   $addmorebutton = "inline-block";
                   $Removemorebutton = "none";
                   }
               
                ?>
                
                <div class="col-sm-4">
                    s
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">

                                    <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'> 
                                    <option>Select</option>      
                                    <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterLmsyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterLmsyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4"> </div>
            </div>
            <div class="clearfix"></div>
               
                <div id="hide-table">

                    <table id="subjectsTablesP" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                        <thead>

                            <tr>

                                <th ><?=$this->lang->line('slno')?></th>

                                <th ><?=$this->lang->line('subject_code')?></th>

                                <th ><?=$this->lang->line('subject_name')?></th>

                                <th >Semester/Year</th>
                                 <th >PDF'S</th>

                                <?php  if($usertype == "Admin") { ?>

                                <th ><?=$this->lang->line('action')?></th>

                                <?php } ?>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if(count($subjects)) {
                                $i = 1;
                                $j = 1;
                             foreach($subjects as $subject) { ?>

                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">

                                        <?php echo $i; ?>
                                  </td>

                                <td data-title="<?=$this->lang->line('subject_code')?>">

                                        <?php echo $subject->subject_code; ?>

                                </td>

                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php echo $subject->subject; ?>
                                    </td>

                                    <td data-title="Semester/Year">


                                    <?php echo str_replace('_', ' ', $subject->yearsOrSemester);?>

                                    </td>
                            <td data-title="PDF'S">

                            <?php 
                        $this->db->where('subjectID',$subject->subjectID);
                        $query   = $this->db->get('pdf_lms');
                        $countpdf  = $query->result();
                        $j = 1;
                        foreach ($countpdf as $key => $value) {
                            ?>
                           <a target="_blank" href="<?php base_url() ?>uploads/online_reader/<?php echo  $value->pdf ?>"><strong style="color:#3c8dbc;">Read</strong> <?php echo "&nbsp;"; ?><img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/pdfstu.png"></a><br> <br>      
                    <?php } ?>
                             </td>

                                    <?php  if($usertype == "Admin") { ?>

                                    <td data-title="<?=$this->lang->line('action')?>">

                                        <?php echo btn_edit('subject/edit/'.$subject->subjectID."/".$set, $this->lang->line('edit')) ?>

                                        <?php echo btn_delete('subject/delete/'.$subject->subjectID."/".$set, $this->lang->line('delete')) ?>

                                    </td>

                                    <?php } ?>

                                </tr>

                            <?php $i++; }} ?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
    
    function pdfCustomize(){

        $.ajax({

            type: 'POST',

            url: "<?=base_url('lms/delete')?>",

            data: "id=" + classesID,

            dataType: "html",

            success: function(data) {

                window.location.href = data;

            }

        });

    }

</script>


<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

        if(classesID == 0) {

            $('#hide-table').hide();

        } else {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/subject_list')?>",

                data: "id=" + classesID,

                dataType: "html",

                success: function(data) {

                    window.location.href = data;

                }

            });

        }

    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();
            $.ajax({

                type: 'POST',

                url: "<?=base_url('lms/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">
        function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('lms/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
</script>
