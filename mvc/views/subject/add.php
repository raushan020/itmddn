<style>
  .AddNewSubjetcsDetails{
    margin-bottom: 10px;
  }
  .margin20{
    margin-bottom: 20px;
  }
</style>

<div class="">

    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-book"></i> <?=$this->lang->line('panel_title')?> / Add Subject, Assign Professor </h3>
        </div>
        <div class="col-md-6 align-self-center">
            <ol class="breadcrumb">
              <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            	<li><a href="<?=base_url("subject/index")?>"><?=$this->lang->line('menu_subject')?></a></li>

            	<li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_subject')?></li>
            </ol>
        </div>          
     </div>

    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <a href="<?=base_url("subject/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

            <div class="col-sm-12">

                <form class="form-horizontal" role="form" method="post">

                    <?php 

                        if(form_error('department')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="department" class="col-sm-2 control-label">

                            Department Name

                        </label>

                        <div class="col-sm-6">

                           <?php

                                $array = array();

                                $array[0] = 'Select Department';

                                foreach ($department as $depart) {

                                  $array[$depart->departmentID] = $depart->department_name;

                                }

                                echo form_dropdown("department", $array, set_value("department"), "id='department' class='form-control'  onchange = 'departmentwiseCourse($(this).val())' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('department'); ?>

                        </span>

                    </div>


                    <!-- <?php 

                        if(form_error('classesID')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classesID" class="col-sm-2 control-label">

                            Course Name

                        </label>

                        <div class="col-sm-6">

                           <?php

                                $array = array();

                                $array[0] = 'Select Course';

                                foreach ($classes as $classa) {

                                  $array[$classa->classesID] = $classa->classes;

                                }

                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'  onchange = 'CourseSDependent($(this).val())' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classesID'); ?>

                        </span>

                    </div> -->

                     <div id="departmentCourse"></div>


                    <div id="subCourseID">
                    </div>      

                    <div id="fetchYearsAndSem"></div>


                    
                    <div id="appendSujectsDetails">
                        
                    </div>
                    <div class="clearfix"></div>


                    <div class="AddNewSubjetcsDetails">
                       <div class="col-sm-2">
                          <div class="">
                             <input type="text" class="form-control" id="subjectName" placeholder="Subject Name" name="subject" value="<?=set_value('subject')?>" required>
                          </div>
                       </div>  
                        <div class="col-sm-2">
                          <div class="">
                             <input type="text" class="form-control" id="shortCodeofSubject" placeholder="Sort Code Subject" name="shortCodeofSubject" value="<?=set_value('shortCodeofSubject')?>" required>
                          </div>
                       </div> 

                       <div class="col-sm-2">
                          <div class="">
                             <input type="text" class="form-control" id="subjectCode" placeholder="Subject Code" name="subject" value="<?=set_value('subject')?>" >
                          </div>
                       </div>
                       <div class="col-sm-2">
                         <div class="">
                           <select class='form-control' required = 'required' value = '<?=set_value('subject_mode')?>' style='resize:none;' id='subjectmode' name='subjectmode'>
                             <option value="">Select Mode</option>
                             <option value="0">Permanent</option>
                             <option value="1">Optional</option>
                           </select>
                         </div>
                       </div>
                      <div class="col-sm-2">
                          <div class="">
                             <select class='form-control' required = 'required' value = '<?=set_value('professor')?>' style='resize:none;' id='professorID' name='professor'>";
                              <option value=''>Select</option>
                             <?php $this->db->select('professorID,name');
                                   $this->db->where('status',1);
                                  $professorData = $this->db->get('professor')->result();?>
                          <?php foreach ($professorData as $key => $value) { ?>
                            
                                  <option value="<?= $value->professorID?>"><?= $value->name?></option>
                          <?php } ?>
                              </select>
                          </div>
                       </div>
                       
                        <div class="col-sm-1">
                          <div class="">
                              <input type="button" class="btn btn-success add-btn update-btn" onclick="AddSubject()" value="<?=$this->lang->line("add_subject")?>" >
                          </div>
                       </div>
                       <div class="clearfix"></div>
                    </div>

                </form>

            </div> 

        </div>

    </div>

</div>

</div>

</div></div>



<style type="text/css">
button, input, optgroup, select, textarea{    
	
	color: inherit;
}
</style>
