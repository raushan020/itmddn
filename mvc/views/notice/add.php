<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_notice')?></li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
   <!-- <div class="form-row">
      <div class="form-group">
          <label for="courseName" class="col-md-2 col-form-label">Type</label>
           <div class="col-md-10">
         <div class="col-sm-3">
            <input type="radio" name="cod" id="chde1" value="1"><label for="chde1">Sem</label>
         </div>
         <div class="col-sm-3">
         <input type="radio" name="cod" value="0" id="chde2" checked><label for="chde2">Year</label>
         </div>
         <div class="clearfix"></div>
           </div>
      </div>
             </div> -->
   <!-- form start -->
   <div class="container-fluid">
    <a href="<?=base_url("notice/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
      <div class="row">
         <div class="">
            <div class="card">
               <div class="card-body">
                  <div class="col-sm-12">
                     <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                        <?php if(form_error('classesID[]')){ ?>
                        <div class="alert alert-danger">
                           <strong>Danger!</strong><?php echo  form_error('classesID[]'); ?>
                        </div>
                        <?php } ?>
                        <!-- <label for="title" class="col-sm-1 control-label">
                           Course
                           
                           </label> -->
                        <!-- <?php 
                           if(form_error('classesID')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                           <label for="classesID" class="col-sm-2 control-label">
                           
                              Course<span style="color: red;">*</span>
                           
                           </label>
                           
                           <div class="col-sm-6" >
                                  
                                                   
                               <?php
                              $array = array();
                              
                              
                              $array[0] = "Select Course";
                              
                              
                              foreach ($classes as $classa) {
                              
                              
                                  $array[$classa->classesID] = $classa->classes;
                              
                              }
                              
                              
                              echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control multipleSelectOption'  onchange = 'CourseSDependent1($(this).val())' value='Select Course' ");
                              
                              ?>
                           
                                  
                           </div> 
                           
                               
                           <span class="col-sm-4 control-label">
                           
                               <?php echo form_error('classesID'); ?>
                           
                           </span>
                           
                           </div> -->
                        <div id="subCourseID">
                        </div>
                        <div id="fetchYearsAndSem"  multiple="multiple">
                        </div>
                        <div id="appendSujectsDetails">
                        </div>
                        <?php 
                         if(form_error('date')) 
                         
                             echo "<div class='form-group has-error' >";
                         
                         else     
                         
                             echo "<div class='form-group' >";
                         
                         ?>
                          <label for="date" class="col-sm-2 control-label">
                          <?=$this->lang->line("notice_date")?>
                          </label>
                          <div class="col-sm-6">
                          <input type="" class="form-control datepicker_quiz_data" disabled="disabled" id="date1" name="date1" placeholder="Date" value="<?php echo date('d-m-Y') ?>" >
                          <input type="hidden" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?php echo date('d-m-Y') ?>" >
                          </div>
                          <span class="col-sm-4 control-label">
                          <?php echo form_error('date'); ?>
                          </span>
                       </div>
                        <?php 
                           if(form_error('title')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                        <label for="title" class="col-sm-2 control-label">
                        Notice Title
                        </label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" placeholder="Enter your  title here" autocomplete="off" name="title" value="<?=set_value('title')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('title'); ?>
                        </span>
                  </div>
                  <?php 
                           if(form_error('notice_type')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                        <label for="notice_type" class="col-sm-2 control-label">
                        Notice Type
                        </label>
                        <div class="col-sm-6">
                           <!-- <input type="text" class="form-control" placeholder="Enter your notice_type here" autocomplete="off" name="notice_type" value="<?=set_value('notice_type')?>" > -->
                           <select name="notice_type" class="form-control" required="required">
                            <option value="">--- Select Notice Type ---</option>
                             <option value="Class Lecture">Class Lecture</option>
                             <option value="Lecture Notes">Lecture Notes</option>
                             <option value="Assignment">Assignment</option>
                             <option value="Monthly Academic Activity">Monthly Academic Activity</option>
                             <option value="Semester Activity">Semester Activity</option>
                             <option value="Others">Others</option>
                           </select>
                         </div>
                         <span class="col-sm-4 control-label">
                        <?php echo form_error('notice_type'); ?>
                        </span>
                        </div>

                  
               <?php 
                  if(form_error('notice')) 
                  
                      echo "<div class='form-group has-error' >";
                  
                  else     
                  
                      echo "<div class='form-group' >";
                  
                  ?>
               <label for="notice" class="col-sm-2 control-label">
               Description
               </label>
               <div class="col-sm-8 col-md-6">
               <textarea class="form-control" id="editor" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
               </div>
               <span class="col-sm-4 control-label">
               <?php echo form_error('notice'); ?>
               </span>
            </div>
            <label for="date" class="col-sm-2 control-label">
            Select Course
            </label>
            <div class="col-sm-6">
            <input type="button" class="btn btn-success" onclick='selectAll()' value="Check All Course"/>
            <input type="button" class="btn btn-success" onclick='selectAllsemester()' value="Uncheck"/>
            </div>
            <?php foreach ($classes as $key => $value) {   ?>          
            <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
            <div class="dsd">
            <label class="nexCheckbox"><?php echo $value->classes ?>
            <input type="checkbox" name="classesID[]" class="classidselect<?php echo $key ?>" onclick="select_course(this,'<?php echo $key ?>')" value="<?php echo $value->classesID ?>" id="select_all" >
            <span class="checkmark checkmark-action-layout"></span>
            </label> 
            <?php 
              $looping    =  (int) $value->duration;
              if ($value->mode==1)
              {
                for ($i=1; $i <=$looping; $i++)
                { ?>
                  <div class="col-sm-1">
                    <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>' name="yearOrSemester<?php echo $value->classesID ?>[]" onclick="selectautoclassid(this,'<?php echo $key ?>')" value="<?php echo CallYears($i); ?>" > <?php echo $i; ?>
                  </div>
              <?php }
              }
              else
              {
                for ($i=1; $i <=(2*$looping); $i++)
                { ?>
                  <div class="col-sm-1">
                    <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>' name="yearOrSemester<?php echo $value->classesID ?>[]" onclick="selectautoclassid(this,'<?php echo $key ?>')" value="<?php echo CallSemester($i); ?>" > <?php echo $i; ?>
                  </div>
              <?php }
              }
            ?>
            
            <!-- <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(2);}else{echo CallSemester(2);} ?>"> 2
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(3);}else{echo CallSemester(3);} ?>"> 3
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(4);}else{echo CallSemester(4);} ?>"> 4
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(5);}else{echo CallSemester(5);} ?>"> 5
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(6);}else{echo CallSemester(6);} ?>"> 6
            </div> -->
            </div>
            </div>
            <br>
            <?php } ?>
            <div>&nbsp;</div>
            <?php 
               if(form_error('document')) 
               
                   echo "<div class='form-group has-error' >";
               
               else     
               
                   echo "<div class='form-group' >";
               
               ?>
            <label for="document" class="col-sm-2 control-label">
            Upload Document
            </label>
            <div class="col-sm-6">
            <input type="file" name="userfile">
            <label style="color: red">Max file size: 10 MB. Only PDF, DOC & PPT supported</label>
            </div>
            <span class="col-sm-4 control-label">
            <?php echo form_error('document'); ?>
            </span>
         </div>
        <!--  <div>
              <label>jpg,png,pdf,docx,doc,xls,xlsx,txt</label>
            </div> -->
         <div class="col-md-12" >
          
            <center><input type="submit" class="btn btn-success" value="Publish" ></center>
                                     
         </div>
       
                                                                                
      </div>
      </form>
   </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">
   jq('#date').datepicker();
   
   jq('#notice').jqte();
   
</script>
<script>
   function mail_send() {
       $.ajax({
   
          type: 'POST',
   
          url: "<?=base_url('notice/send_notice_email')?>",
   
          dataType: "html",
   
          success: function(data) {
   
              location.reload(); 
   
          }
   
      });
   
   }
</script>
<script type="text/javascript">
   $(document).ready(function() {
   $('#classesID').multiselect({
   nonSelectedText: 'Select Course'
   });
   });
</script>
<!-- <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
<link href="./jquery.multiselect.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="./jquery.multiselect.js"></script> -->
<script>
   $(function () {
       $('select[multiple].active.3col').multiselect({
           columns: 3,
           placeholder: 'Select States',
           search: true,
           searchOptions: {
               'default': 'Search States'
           },
           selectAll: true
       });
   
   });
</script>
<script type="text/javascript">
   function selectAll(){
      var items=document.getElementsByName('classesID[]');
      var items_sem=document.getElementsByClassName('yearOrSemester');
   
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=true;
      }
      for(var i=0; i<items_sem.length; i++){
          if(items_sem[i].type=='checkbox')
              items_sem[i].checked=true;
      }
   }
   
   function select_course(val,key){
   
      var items_sem=document.getElementsByClassName('yearOrSemester'+key);
   
      if(val.checked){
   
          $('.yearOrSemester'+key).each(function(){
              this.checked = true;
          });
      }else{
           $('.yearOrSemester'+key).each(function(){
              this.checked = false;
              $(".etsfilertButton").addClass("disabled");
          });
      }                
   
   }
   function selectautoclassid(val,key)
   {
     var items_sem=document.getElementsByClassName('classidselect'+key);
     if(val.checked)
     {
      $('.classidselect'+key).each(function(){
              this.checked = true;
          });
     }
     else
     {
       if($('.yearOrSemester'+key+':checked').length > 0)
       {
          $('.classidselect'+key).each(function(){
              this.checked = true;
          });              
       }
       else
       {
          $('.classidselect'+key).each(function(){
                this.checked = false;
                });
       }
        
     }
   }
   
   function selectAllsemester(){
      var items=document.getElementsByName('classesID[]');
      var items_sem=document.getElementsByClassName('yearOrSemester');
      
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=false;
      }
      for(var i=0; i<items_sem.length; i++){
          if(items_sem[i].type=='checkbox')
              items_sem[i].checked=false;
      }
   }
   
   function UnSelectAll(){
      var items=document.getElementsByName('classesID[]');
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=false;
      }
   }           
</script>
<script>
    initSample();
</script>