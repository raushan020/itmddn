<!-- <?php 
 $usertype = $this->session->userdata("usertype");
if ($usertype == "Admin") {

               ?> 
               <style>
                 a.btn.btn-danger.trash{
                  padding: 8px;
               </style>
             <?php } ?> -->
<div class="">

   <style type="text/css">
       .dataTables_scroll
{
    overflow:auto !important;
}
   </style>

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Notice</li>

            </ol>

        </div>

    </div>



<!-- /.box-header -->



    <!-- form start -->

    <div class="container-fluid">

        <div class="row">

            <div class="">

            <div class="card">

            <div class="card-body">



            <div class="box-body">



                <div class="row">



                    <div class="col-sm-12">

                        <?php



                            $usertype = $this->session->userdata("usertype");



                            if( $usertype == "ClgAdmin" || $usertype == "Admin" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner" || $usertype == 'Professor') {



                        ?>



                        <div class="col-sm-6"></div>

                        <div class="col-sm-6">

                               <div class="pull-right">

                              <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>



                              </div>

                           </div>

                        </div>

                        <div class="clearfix"></div>


        <div class="col-sm-12">



            <div class="theme_input_blue">

            <?php 

               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {

               $display =  "block";

               $addmorebutton = "none";

               $Removemorebutton = "inline-block";

               }else{

               $display = "none";

               $addmorebutton = "inline-block";

               $Removemorebutton = "none";

               }

               

               ?>

                

            </div>

             <div class="col-sm-12">



            <div class="theme_input_blue">

              <div class="col-sm-3">
        <div class="form-group">
          
            <label for="classesID" class="control-label">
                Notice Type
            </label>
            <div class="">
              <select name="notice_type" class="form-control" id="noticetypeFilter" required="required">

                <option value="">--- Select Notice Type ---</option>
                 <option value="Class Lecture" <?php if($this->session->userdata('FilterNoticetype') == 'Class Lecture'){echo 'Selected';}else{echo ' ';} ?>>Class Lecture</option>
                 <option value="Lecture Notes" <?php if($this->session->userdata('FilterNoticetype') == 'Lecture Notes'){echo 'Selected';}else{echo ' ';} ?>>Lecture Notes</option>
                 <option value="Assignment" <?php if($this->session->userdata('FilterNoticetype') == 'Assignment'){echo 'Selected';}else{echo ' ';} ?>>Assignment</option>
                 <option value="Monthly Academic Activity" <?php if($this->session->userdata('FilterNoticetype') == 'Monthly Academic Activity'){echo 'Selected';}else{echo ' ';} ?>>Monthly Academic Activity</option>
                 <option value="Semester Activity" <?php if($this->session->userdata('FilterNoticetype') == 'Semester Activity'){echo 'Selected';}else{echo ' ';} ?>>Semester Activity</option>
                 <option value="Others" <?php if($this->session->userdata('FilterNoticetype') == 'Others'){echo 'Selected';}else{echo ' ';} ?>>Others</option>
               </select>
            </div>
            
            <div class="clearfix"></div>
            <div class="forReset">
              <a style="cursor:pointer;" onclick="ResetNoticetype()">Reset </a>
            </div>
        </div>
      </div>
      <?php if($usertype != 'Professor'){ ?>
      <div class="col-sm-3">
        <div class="form-group">
          <div id="loadingimage" style="display: none" ></div>
            <label for="classesID" class="control-label">
                Professor
            </label>
            <div class="">
              <select name="notice_type" class="form-control" id="noticeprofessorFilter" required="required">

                <option value="">--- Select Professor ---</option>
                <?php 
                  foreach ($professor as $key => $value) {
                    
                  ?>
                 <option value="<?=$value->professorID?>" <?php if($this->session->userdata('Filternoticeprofessor') == $value->professorID){echo 'Selected';}else{echo ' ';} ?>><?=$value->name?></option>
                 <?php } ?>
               </select>
            </div>
            
            <div class="clearfix"></div>
            <div class="forReset">
              <a style="cursor:pointer;" onclick="ResetProfessorFitlter()">Reset </a>
            </div>
        </div>

      </div>
    <?php } ?>
        <div class="col-sm-2">
                      <div class="">
                          <!-- <form style="" class="form-horizontal" role="form" method="post"> -->
                              <div class="form-group">
                                  <label for="classesID" class="control-label">
                                      Notice From
                                  </label>
                                  <div class="">
                                    <input type="text" class="form-control datepicker_quiz_data" id="noticefromFilter" name="noticefrom" value="<?php if($this->session->userdata('Filternoticefroms')){ echo $this->session->userdata('Filternoticefroms'); }?>">
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  <div class="forReset">
                                    <a style="cursor:pointer;" onclick="ResetNoticefrom()">Reset  </a>
                                  </div>
                              </div>
                          <!-- </form> -->
                      </div>
                  </div>

                  <div class="col-sm-2">
                      <div class="">
                          <!-- <form style="" class="form-horizontal" role="form" method="post"> -->
                              <div class="form-group">
                                  <label for="classesID" class="control-label">
                                      Notice To
                                  </label>
                                  <div class="">
                                    <input type="text" class="form-control datepicker_quiz_data" id="noticetoFilter" value="<?php if($this->session->userdata('Filternoticeto')){ echo $this->session->userdata('Filternoticeto'); }?>" name="noticeto">
                                  </div>
                                  
                                  <div class="clearfix"></div>
                                  <div class="forReset">
                                    <!-- <a style="cursor:pointer;" onclick="ResetNoticeto()">Reset </a> -->
                                  </div>
                              </div>
                          <!-- </form> -->
                      </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      
                    <button type="button" id="popupsubmit" style="margin-top: 30px;" class="btn btn-primary"> Search </button>
                  </div>
                  </div>

            </div>
            
            <div class="clearfix"></div>

            <div class="clearfix"></div>

         

            <?php }  ?>                      



                                

                <?php  if( $usertype == "ClgAdmin" || $usertype == "Admin" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner" || $usertype == 'Professor'){?>

<!-- <div class="action-layout">

                    <ul>

                        <li>

                            <label class="nexCheckbox">Check

                                <input type="checkbox" name="select_all" id="select_all">

                                <span class="checkmark checkmark-action-layout"></span>

                            </label>

                        </li>

                        <li class="active-btn">

                            <a class="btn btn-success etsfilertButton"  data-toggle="modal" data-target="#myModal">Send mail</a>

                        </li>

                      

                    </ul>

                </div>   -->

                        <div id="hide-tab">

                          <a href='<?= base_url() ?>notice/generateXls'>Export</a><br><br>

                            <table id = "noticeTables" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>
                                    

                                        <th><?=$this->lang->line('slno')?></th>
                                        <th><?=$this->lang->line('notice_date')?></th>
                                        <th>Classes</th>
                                        <th>Subject</th>
                                        <th>Notice Type</th>
                                        <th>Description</th>
                                        <th>Year/ Semester</th>
                                        <th>Sender</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>







                        </div>

                    <?php  } ?>







         <!--   <?php  if ( $usertype == "Admin") { ?>



                          <div class="col-sm-12">                               

                               <div class="pull-right">

                                 <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                                  </div>                            

                                </div>

                            </div>

                            

                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th ><?=$this->lang->line('slno')?></th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                         <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>

                                      <th class="col-sm-4">Notice</th>

                                       <th class="col-sm-4">Action</th>  

                                          </tr>



                                </thead>                                                      

                                    <tdata>

                                    <?php if(count($notices)) {$i = 1; foreach($notices as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>

                                        <td><?php echo $notices['title']; ?></td>

                                       

                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>

                                        <td><?php echo substr($notices['notice'],0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?></td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div> <?php } ?>

                    

                       



 -->





 <!-- <?php if ( $usertype == "Professor") { ?>



                            <div class="col-sm-12">                               

                               <div class="pull-right">

                                 <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                                  </div>                            

                                </div>

                            </div>

                            

                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th class="col-sm-1"><?=$this->lang->line('slno')?></th>
                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>
                                        <th class="col-sm-2">Subject</th>
                                        <th class="col-sm-4">Description</th>
                                        <th class="col-sm-2">Action</th>                           

                                    </tr>



                                </thead>

                             

                                <tdata>

                                    <?php if(count($notices)) {$i = 1; foreach($notices as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>
                                        <td><?php echo $notices['title']; ?></td>                                   

                                        <td><?php echo substr(strip_tags($notices['notice']),0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?>

                                        	<?php echo btn_delete('notice/delete/'.$notices['noticeID'], $this->lang->line('delete')); ?>
                                        </td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div>  <?php } ?> -->

                    

                     









   <?php  if ( $usertype == "Accountant") { ?>



                            <div class="col-sm-12">                               

                               <div class="pull-right">

                                 <div class="btn-group">

                           <a href="<?php echo base_url('notice/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                                  </div>                            

                                </div>

                            </div>





                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>



                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>                                      



                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>



                                        <th class="col-sm-4">Notice</th>

                                        

                                        <th class="col-sm-4">Action</th>

                                       

                                    </tr>



                                </thead>

                             

                                <tdata>

                                    <?php if(count($notices)) {$i = 1; foreach($notices as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>

                                        <td><?php echo $notices['title']; ?></td>

                                        

                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>

                                        <td><?php echo substr(strip_tags($notices['notice']),0,20); ?></td>

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?></td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div> <?php } ?>

                    

                       





 

 



                        



  <?php  if ( $usertype == "Student" || $usertype == "Parent") { ?>

                            

                            <div id="hide-tab">



                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                                <thead>

                                    <tr>

                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>


                                        <th class="col-sm-2">Subject</th>
                                        <th class="col-sm-4">Description</th>
                                        <th>Notice Type</th>
                                        <th>Sender Name</th>
                                        <th class="col-sm-4">Action</th>

                                       

                                    </tr>



                                </thead>

                             

                                <tdata>

                                    <?php if(count($notice)) {$i = 1; foreach($notice as $notices) { ?>

                                    <tr>

                                        <td><?php echo $i; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>
                                        <td><?php echo $notices['title']; ?></td>
                                        <td><?php echo substr(strip_tags($notices['notice']),0,20); ?></td>
                                         <td><?php echo $notices['notice_type']; ?></td>
                                        <td><?php echo $notices['sendByUSerName']; ?></td>
                                        

                                        

                                        <td><?php echo btn_view('notice/view/'.$notices['noticeID'], $this->lang->line('view')); ?></td>



                                    </tr>

                                        <?php $i++; }} ?>

                                </tdata>



                            </table>



                        </div>

                    

                       <?php } ?>

              











                    </div>

               </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>

<!-- <script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script> -->

<!-- <script type="text/javascript">

  $('#noticeTables').DataTable({

   columnDefs: [ { type: 'date', 'targets': 5] } ],

   order: [[ 5, 'desc' ]],          

});

</script> -->




<script type="text/javascript">

    $('#popupsubmit').click(function() {

        var noticetypeFilter = $('#noticetypeFilter').val();
        var noticefromFilter = $('#noticefromFilter').val();
        var noticetoFilter = $('#noticetoFilter').val();
        var noticeprofessorFilter = $('#noticeprofessorFilter').val();
        // alert(noticefromFilter);
        // var img="<?php echo base_url('uploads/images/loading-circle.gif') ?>";
        // $('#loadingimage').show();
        // $('#loadingimage').html("<img src='"+img+"' >");
        // if (noticetypeFilter) {
          // 
          $.ajax({

            type: 'POST',

            url: "<?=base_url('notice/index')?>",

            data: {noticetypeFilter:noticetypeFilter,noticefromFilter:noticefromFilter,noticetoFilter:noticetoFilter,noticeprofessorFilter:noticeprofessorFilter},

            dataType: "html",

            success: function(data) {
              
                // $('#loadingimage').hide();
                location.reload();
               
             
              
            }
        });
        
        
    });

  </script>

  <script type="text/javascript">

    // $('#noticefromFilter').blur(function() {
// alert(hi);
        // var noticefromFilter = $('#noticefromFilter').val();

        // $.ajax({

        //     type: 'POST',

        //     url: "<?=base_url('notice/index')?>",

        //     data: "noticefromFilter=" + noticefromFilter,

        //     dataType: "html",

        //     success: function(data) {
        //       // location.reload();
        //     }
        // });
    // });

  </script>
<script type="text/javascript">

    // $('#noticefromFilter').blur(function() {
// alert(hi);
        // var noticetoFilter = $('#noticetoFilter').val();

        // $.ajax({

        //     type: 'POST',

        //     url: "<?=base_url('notice/index')?>",

        //     data: "noticetoFilter=" + noticetoFilter,

        //     dataType: "html",

        //     success: function(data) {
        //       // location.reload();
        //     }
        // });
    // });

  </script>

<script type="text/javascript">



    $('#subCourseID').change(function() {

        $('#noticeTables').DataTable().state.clear();



        var subCourseID = $(this).val();



            $.ajax({

                type: 'POST',

                url: "<?=base_url('notice/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }



            });



    });



</script>



<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

        $('#noticeTables').DataTable().state.clear();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('notice/index')?>",

            data: "yearSemesterID=" + yearSemesterID,

            dataType: "html",

            success: function(data) {

                 location.reload();

            }

        });

    });

</script>



<script type="text/javascript">

    function ResetProfessorFitlter(){

      $('#noticeTables').DataTable().state.clear();

          $.ajax({

              type: 'POST',

              url: "<?=base_url('notice/ResetProfessorFitlter')?>",

              data:{ResetProfessorFitlter:'ResetProfessorFitlter'},

              dataType: "html",

              success: function(data) {

                  location.reload();

              }

          });

      }    

    function ResetNoticetype(){

      $('#noticeTables').DataTable().state.clear();

          $.ajax({

              type: 'POST',

              url: "<?=base_url('notice/ResetNoticetype')?>",

              data:{ResetNoticetype:'ResetNoticetype'},

              dataType: "html",

              success: function(data) {

                  location.reload();

              }

          });

      }

      function ResetNoticefrom(){

      $('#noticeTables').DataTable().state.clear();

          $.ajax({

              type: 'POST',

              url: "<?=base_url('notice/ResetNoticefrom')?>",

              data:{ResetNoticefrom:'ResetNoticefrom'},

              dataType: "html",

              success: function(data) {

                  location.reload();

              }

          });

      }

      function ResetNoticeto(){

      $('#noticeTables').DataTable().state.clear();

          $.ajax({

              type: 'POST',

              url: "<?=base_url('notice/ResetNoticeto')?>",

              data:{ResetNoticeto:'ResetNoticeto'},

              dataType: "html",

              success: function(data) {

                  location.reload();

              }

          });

      }


    function ResetSubcourses(){

        $('#noticeTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetSubcourses')?>",



                data:{ResetSubcourses:'ResetSubcourses'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

}

    function ResetSemesterYear(){

        $('#noticeTables').DataTable().state.clear();

            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetSemesterYear')?>",



                data:{ResetSesession:'ResetSesession'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

} 



 function ResetAllfilternotice(){

    $('#noticeTables').DataTable().state.clear();

    

            $.ajax({

                type: 'POST',



                url: "<?=base_url('notice/ResetAllfilter')?>",



                data:{ResetSesession:'ResetSesession'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

}



</script>

