  <style type="text/css">

table{
}
/*LOGO*/
.logo h1{
    margin: 0;
}
.logo h1 a{
    color: #000;
    font-size: 20px;
    font-weight: 700;
    text-transform: uppercase;
    font-family: 'Montserrat', sans-serif;
}


/*HEADING SECTION*/
.heading-section{
}
.heading-section h2{
    color: #000000;
    font-size: 28px;
    margin-top: 0;
    line-height: 1.4;
}
.heading-section .subheading{
    margin-bottom: 20px !important;
    display: inline-block;
    font-size: 13px;
    text-transform: uppercase;
    letter-spacing: 2px;
    color: rgba(0,0,0,.4);
    position: relative;
}
.heading-section .subheading::after{
    position: absolute;
    left: 0;
    right: 0;
    bottom: -10px;
    content: '';
    width: 100%;
    height: 2px;
    background: #f3a333;
    margin: 0 auto;
}

th, td {
    white-space: normal !important;
}
  </style>

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>

                <li class="active"><?=$this->lang->line('menu_view')?></li>
            </ol>
        </div>
    </div>

    

    <!-- <div class="container-fluid">
        <div class="row">
            <div class="">
                <div class="card"> -->
                     
 <?php 

        $usertype = $this->session->userdata("usertype");

        if($usertype == "ClgAdmin" ||  $usertype == "Admin" || $usertype == "Accountant" || $usertype == "Professor") {

    ?>
<div class="container-fluid">
   <a href="<?=base_url("notice/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
     <div class="row">
                        <div class="col-md-12">
                          <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                              <li><h3 style="color: #000000; margin: 10px 0px 5px 15px;"><?php echo $notice->title; ?></h3></li>
                            </ul>
                            <div class="tab-content">
                              <div class="active tab-pane">
                                <!-- Post -->
                                <div class="post" style="padding:20px;">
                                  <style type="text/css">
                                    .post a {
                                      color:#5d42c7;
                                    }
                                  </style>
                                  <script type="text/javascript">
                                    
                                    function myFunction() {
                                      document.getElementsByTagName("a")[0].setAttribute("class", "post"); 
                                    }
                                    </script>
                                    <script> 
                                    document.addEventListener("click", function(e) { 
                                        if (e.target.tagName == "A" && 
                                                !e.target.hasAttribute("target")) 
                                        { 
                                            e.target.setAttribute("target", "_blank"); 
                                        } 
                                    }); 
                                </script>
                                
                                        <div class="user-block">
                                    <img alt="" src="<?=base_url("uploads/images/".$sender->photo);?>">
                                        <h2><span class="username"><?php echo $sender->name;?></span></h2>
                                        <span class="username"><?php echo $sender->usertype;?></span>
                                    <span class="description"><?php echo date("d M Y", strtotime($notice->date)); ?></span>
                                  </div>
                                    <p style="color: #000000;text-align: justify;margin: 10px;"><?php echo $notice->notice;?></p></div>
                              </div>
                               <?php if ($notice->noticeDocument!='') { ?>       
                                   
                               <div class="click">
                                   <?php if(file_exists("uploads/notice/".$notice->noticeDocument))
                                   { ?>
                                    <a target="_blank" href="<?php echo base_url()?>uploads/notice/<?php echo $notice->noticeDocument; ?>" class="btn btn-success">
                                        Click here 
                                    </a>
                                 <?php  }
                                 else
                                 { ?>
                                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#myModal7">
                                        Click here 
                                    </a>
                              <?php } ?> 
                                     to see the attachment
                               </div>
                               <?php } ?>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>  
<?php } ?>
<!-- attachment file -->
      <div class="modal modal-box-1 fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="width: 350px;margin-top: 60px;">
           <div class="modal-content" id="myModalLabel">
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>File Not Exist!</label>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <button type="button" class="btn modal-btn" data-dismiss="modal" style="padding: 6px 16px;min-width: 110px;">Close</button>
                  </div>
                </div>              
            </div>
          </div>
        </div>
      </div>
      <!-- End -->
<!-- end notice student view -->

<!-- email modal starts here -->

<form class="form-horizontal" role="form" action="<?=base_url('notice/send_mail');?>" method="post">

    <div class="modal fade" id="mail">

      <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>

            </div>

            <div class="modal-body">

            

                <?php 

                    if(form_error('to')) 

                        echo "<div class='form-group has-error' >";

                    else     

                        echo "<div class='form-group' >";

                ?>

                    <label for="to" class="col-sm-2 control-label">

                        <?=$this->lang->line("to")?>

                    </label>

                    <div class="col-sm-6">

                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >

                    </div>

                    <span class="col-sm-4 control-label" id="to_error">

                    </span>

                </div>



                <?php 

                    if(form_error('subject')) 

                        echo "<div class='form-group has-error' >";

                    else     

                        echo "<div class='form-group' >";

                ?>

                    <label for="subject" class="col-sm-2 control-label">

                        <?=$this->lang->line("subject")?>

                    </label>

                    <div class="col-sm-6">

                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >

                    </div>

                    <span class="col-sm-4 control-label" id="subject_error">

                    </span>



                </div>



                <?php 

                    if(form_error('message')) 

                        echo "<div class='form-group has-error' >";

                    else     

                        echo "<div class='form-group' >";

                ?>

                    <label for="message" class="col-sm-2 control-label">

                        <?=$this->lang->line("message")?>

                    </label>

                    <div class="col-sm-6">

                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>

                    </div>

                </div>
          
            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>

                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />

            </div>

        </div>

      </div>

    </div>

</form>
<!-- email end here -->

<?php 
 
    $usertype = $this->session->userdata("usertype");

    if($usertype == "Student" ) {

    ?>

     

    <div class="container-fluid">
      <a href="<?=base_url("notice/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
                    <!-- /row -->
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-md-12">
                          <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                              <li><h3 style="color: #000000;margin: 10px 0px 5px 15px;"><?php echo $notice->title; ?></h3></li>
                            </ul>
                            <div class="tab-content">
                              <div class="active tab-pane">
                                <!-- Post -->
                                <div class="post" style="padding:20px;">
                                  <div class="user-block">
                                    <img alt="" src="<?=base_url("uploads/images/".$sender->photo);?>">
                                    <h2><span class="username"><?php echo $sender->name;?></span></h2>
                                    <span class="username"><?php echo $sender->usertype;?></span>
                                    <span class="description"><?php echo date("d M Y", strtotime($notice->date)); ?></span>
                                  </div>
                                  <!-- /.user-block -->
                                  <p style="color: #000000;text-align: justify;margin: 10px;"><?php echo $notice->notice; ?></p>
                                </div>
                                <!-- /.post -->

                                <!-- Post -->
                                <?php if ($notice->noticeDocument!='') { ?>       
                                   
                               <div class="click">
                                    <?php if(file_exists("uploads/notice/".$notice->noticeDocument))
                                   { ?>
                                    <a target="_blank" href="<?php echo base_url()?>uploads/notice/<?php echo $notice->noticeDocument; ?>" class="btn btn-success">
                                        Click here 
                                    </a>
                                 <?php  }
                                 else
                                 { ?>
                                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#myModal7">
                                        Click here 
                                    </a>
                              <?php } ?>  to see the attachment
                               </div>
                               <?php } ?>
                                <!-- /.post -->
                              </div>

                              
                              <!-- /.tab-pane -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                            <!-- /.tab-content -->
                          <!-- </div> -->
                          <!-- /.nav-tabs-custom -->
                       <!--  </div> -->
                        <!-- /.col -->
                   <!--  </div> -->
                    <!-- /row -->
               <!--  </div>   -->

<!-- end notice student view -->
 

<?php } ?>



<!--  start student View notic by sender -->
<script type="text/javascript">
   $('p a').attr("target", "_blank");
</script>
<script language="javascript" type="text/javascript">

    function printDiv(divID) {

        //Get the HTML of div

        var divElements = document.getElementById(divID).innerHTML;

        //Get the HTML of whole page

        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only

        document.body.innerHTML = 

          "<html><head><title></title></head><body>" + 

          divElements + "</body>";

        //Print Page

        window.print();

        //Restore orignal HTML

        document.body.innerHTML = oldPage;

    }

    function closeWindow() {

        location.reload(); 

    }

    function check_email(email) {

        var status = false;     

        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

        if (email.search(emailRegEx) == -1) {

            $("#to_error").html('');

            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');

        } else {

            status = true;

        }

        return status;

    }

    $("#send_pdf").click(function(){

        var to = $('#to').val();

        var subject = $('#subject').val();

        var message = $('#message').val();

        var id = "<?=$notice->noticeID;?>";

        var error = 0;

        if(to == "" || to == null) {

            error++;

            $("#to_error").html("");

            $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');

        } else {

            if(check_email(to) == false) {

                error++

            }

        } 

        if(subject == "" || subject == null) {

            error++;

            $("#subject_error").html("");

            $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');

        } else {

            $("#subject_error").html("");

        }

        if(error == 0) {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('notice/send_mail')?>",

                data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,

                dataType: "html",

                success: function(data) {

                    location.reload();

                }

            });

        }

    });

</script>
