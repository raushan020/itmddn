<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>
                <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_notice')?></li>

            </ol>
        </div>
    </div>

    <!-- /.box-header -->
    <!-- <div class="form-row">
     <div class="form-group">
         <label for="courseName" class="col-md-2 col-form-label">Type</label>
          <div class="col-md-10">
        <div class="col-sm-3">
           <input type="radio" name="cod" id="chde1" value="1"><label for="chde1">Sem</label>
        </div>
        <div class="col-sm-3">
        <input type="radio" name="cod" value="0" id="chde2" checked><label for="chde2">Year</label>
        </div>
        <div class="clearfix"></div>
          </div>
     </div>
            </div> -->


    <!-- form start -->
    <div class="container-fluid">
        <a href="<?=base_url("notice/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body">                  

                        <div class="col-sm-12">

                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                                     <!-- <label for="title" class="col-sm-1 control-label">

                                           Course

                                        </label> -->
                                    
                            <!-- <?php 

                                if(form_error('classesID')) 

                                    echo "<div class='form-group has-error' >";

                                else     

                                    echo "<div class='form-group' >";

                            ?>

                        <label for="classesID" class="col-sm-2 control-label">

                           Course<span style="color: red;">*</span>

                        </label>
  
                        <div class="col-sm-6" >
                               
                                                
                            <?php

                                $array = array();


                                $array[0] = "Select Course";
                              

                                foreach ($classes as $classa) {


                                    $array[$classa->classesID] = $classa->classes;

                                }
                                

                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control multipleSelectOption'  onchange = 'CourseSDependent1($(this).val())' value='Select Course' ");

                            ?>
   
                               
                        </div> 
  
                            
                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classesID'); ?>

                        </span>

                    </div> -->

                    <div id="subCourseID">
                    </div>      

                    <div id="fetchYearsAndSem"  multiple="multiple">
                        
                    </div>

                    <div id="appendSujectsDetails">
                        
                    </div>
                
                                   <?php 

                                        if(form_error('title')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="title" class="col-sm-2 control-label">

                                            Notice Title

                                        </label>

                                        <div class="col-sm-6">

                                            <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title',$notice->title)?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('title'); ?>

                                        </span>

                                    </div>

  
                                    <?php 

                                        if(form_error('date')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="date" class="col-sm-2 control-label">

                                            <?=$this->lang->line("notice_date")?>

                                        </label>

                                        <div class="col-sm-6">

                                            <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date',date('d-m-Y',strtotime($notice->date)))?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('date'); ?>

                                        </span>

                                    </div>

                                    <?php 

                                        if(form_error('notice')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="notice" class="col-sm-2 control-label">

                                            Description

                                        </label>

                                        <div class="col-sm-6">

                                            <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice',$notice->notice)?></textarea>

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('notice'); ?>

                                        </span>

                                    </div>
                                    
                                    <label for="date" class="col-sm-2 control-label">
                                            Select Course
                                        </label>
                                        <div class="col-sm-6">
                                        <input type="button" class="btn btn-success" onclick='selectAll()' value="Check All Course"/>
                                        <input type="button" class="btn btn-success" onclick='selectAllsemester()' value="Uncheck"/>
                                        </div>
                                <?php 

                                   foreach ($classes as $key => $value) { 
                                   $semesterOrYear = array();  
                                   $checked = '';
                                    foreach ($notice_x_classes as $key_x => $value_x) {

                                    if($value_x->classesID==$value->classesID){

                                    $checked =  "checked";  
                                
                                    $semesterOrYear = explode(',' , $value_x->yearsOrSemester); 
                                
                                    }
                                    }


                                ?>          
                                    <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
                                        <div class="dsd">
                                            <label class="nexCheckbox"><?php echo $value->classes ?>
                                                <input type="checkbox" name="classesID[]" onclick="select_course(this,'<?php echo $key ?>')" value="<?php echo $value->classesID ?>" id="select_all" <?php echo $checked; ?>>
                                                <span class="checkmark checkmark-action-layout"></span>
                                            </label> 
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(1);}else{echo CallSemester(1);} ?>" <?php if($value->mode==1){ if(in_array(CallYears(1),$semesterOrYear)){echo 'checked';}}else{if(in_array(CallSemester(1),$semesterOrYear)){echo 'checked';}} ?> > 1
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(2);}else{echo CallSemester(2);} ?>" <?php if($value->mode==1){ if(in_array(CallYears(2),$semesterOrYear)){echo 'checked';}}else{if(in_array(CallSemester(2),$semesterOrYear)){echo 'checked';}} ?>> 2
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(3);}else{echo CallSemester(3);} ?>"
                                                <?php if($value->mode==1){ if(in_array(CallYears(3),$semesterOrYear)){echo 'checked';}}else{if(in_array(CallSemester(3),$semesterOrYear)){echo 'checked';}} ?>
                                                > 3
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(4);}else{echo CallSemester(4);} ?>"
                                                <?php if($value->mode==1){ if(in_array(CallYears(4),$semesterOrYear)){echo 'checked';}}else{if(in_array(CallSemester(4),$semesterOrYear)){echo 'checked';}} ?>
                                                > 4
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(5);}else{echo CallSemester(5);} ?>"
                                                <?php if($value->mode==1){ if(in_array(CallYears(5),$semesterOrYear)){echo 'checked';}}else{if(in_array(CallSemester(5),$semesterOrYear)){echo 'checked';}} ?>
                                                > 5
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(6);}else{echo CallSemester(6);} ?>"
                                                <?php if($value->mode==1){ if(in_array(CallYears(6),$semesterOrYear)){echo 'checked';}}else{if(in_array(CallSemester(6),$semesterOrYear)){echo 'checked';}} ?>
                                                > 6
                                            </div>
                                                
                                        </div>
                                    </div>
                                    <br>

<?php } ?>                          
                                    <div>&nbsp;</div>
                                    <?php 

                                        if(form_error('document')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="document" class="col-sm-2 control-label">

                                            Notice Document

                                        </label>

                                        <div class="col-sm-6">

                                           <input type="file" name="userfile" value="<?php echo $notice->noticeDocument ?>">

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('document'); ?>

                                        </span>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-5 col-sm-2">
                                            <input type="submit" class="btn btn-success pull-right" value="update Notice" >                                      
                                        </div>
                                    </div>                                                                        
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                   </div>
            </div>
        </div>
    </div>
    

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>

<script type="text/javascript">

    jq('#date').datepicker();

    jq('#notice').jqte();

</script>

<script>
function mail_send() {
    $.ajax({

       type: 'POST',

       url: "<?=base_url('notice/send_notice_email')?>",

       dataType: "html",

       success: function(data) {

           location.reload(); 

       }

   });

}
</script>

<script type="text/javascript">
    $(document).ready(function() {
$('#classesID').multiselect({
nonSelectedText: 'Select Course'
});
});
</script>
<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
    <link href="./jquery.multiselect.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="./jquery.multiselect.js"></script>
<script>
    $(function () {
        $('select[multiple].active.3col').multiselect({
            columns: 3,
            placeholder: 'Select States',
            search: true,
            searchOptions: {
                'default': 'Search States'
            },
            selectAll: true
        });

    });
</script>
<script type="text/javascript">
             function selectAll(){
                var items=document.getElementsByName('classesID[]');
                var items_sem=document.getElementsByClassName('yearOrSemester');

                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=true;
                }
                for(var i=0; i<items_sem.length; i++){
                    if(items_sem[i].type=='checkbox')
                        items_sem[i].checked=true;
                }
            }

          function select_course(val,key){
             
                var items_sem=document.getElementsByClassName('yearOrSemester'+key);

        if(val.checked){

            $('.yearOrSemester'+key).each(function(){
                this.checked = true;
            });
        }else{
             $('.yearOrSemester'+key).each(function(){
                this.checked = false;
                $(".etsfilertButton").addClass("disabled");
            });
        }


            }

            function selectAllsemester(){
                var items=document.getElementsByName('classesID[]');
                var items_sem=document.getElementsByClassName('yearOrSemester');
                
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
                for(var i=0; i<items_sem.length; i++){
                    if(items_sem[i].type=='checkbox')
                        items_sem[i].checked=false;
                }
            }
            
            function UnSelectAll(){
                var items=document.getElementsByName('classesID[]');
                for(var i=0; i<items.length; i++){
                    if(items[i].type=='checkbox')
                        items[i].checked=false;
                }
            }           
        </script>
        <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>

<script type="text/javascript">

tinymce.init({

    selector:"textarea",

    width:"700",

    height:"200",

    relative_urls:false,

    remove_script_host:false,

    theme: "modern",

    plugins: [

        "advlist autolink lists link image charmap print preview hr anchor pagebreak",

        "searchreplace wordcount visualblocks visualchars code fullscreen",

        "insertdatetime media nonbreaking save table contextmenu directionality",

        "emoticons template paste textcolor colorpicker textpattern"
    ],

    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager | fontsizeselect",

    toolbar2: "print preview media | forecolor backcolor emoticons",

    fontsize_formats: "1px 2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px 34px 36px",

      style_formats: [

            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},

            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},

            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},

            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},

            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},

            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},

            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},

            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},

            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},

            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},

            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},

            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},

            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}

        ],

    image_advtab: true,

    templates: [

        {title: 'Test template 1', description: 'Test 1'},

        {title: 'Test template 2', description: 'Test 2'}
    ],

   external_filemanager_path:"<?php echo base_url() ?>filemanager/",

   filemanager_title:"Responsive Filemanager" ,

   external_plugins: { "filemanager" : "<?php echo base_url() ?>filemanager/plugin.min.js"}

});

</script>
