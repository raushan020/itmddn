<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Subject_m extends MY_Model {



	protected $_table_name = 'subject';



	protected $_primary_key = 'subjectID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "classesID asc";



	function __construct() {



		parent::__construct();



	}



	function get_join_subject_count($adminID) {



		if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 



		$this->db->select('*');



		$this->db->from('subject');



		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');





		if ($this->session->userdata('FilterSubjectclassesID')) {

		 $this->db->where('subject.classesID', $this->session->userdata('FilterSubjectclassesID'));

		}



		if ($this->session->userdata('FilterSubjectsubCourseID')) {

		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));

		}



		if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}



	     if ($this->session->userdata('ActiveSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('ActiveSubjects'));

		}



	   if ($this->session->userdata('DraftSubjects')) {

		 $this->db->where('subject.status', 0);

		}



	   if ($this->session->userdata('TrashSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('TrashSubjects'));

		}	



		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.adminID', $adminID);



		$query = $this->db->get();



		return $query->num_rows();



	}





	function get_join_subject() {



		$adminID = $this->session->userdata("adminID");

		$this->db->select('subject.*,classes.*,sub_courses.*,subject.status as studentStatus');

		$this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');





		if ($this->session->userdata('FilterSubjectclassesID')) {

		 $this->db->where('subject.classesID', $this->session->userdata('FilterSubjectclassesID'));

		}



		if ($this->session->userdata('FilterSubjectsubCourseID')) {

		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));

		}



		if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}



	   if ($this->session->userdata('ActiveSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('ActiveSubjects'));

		}



	   if ($this->session->userdata('DraftSubjects')) {

		 $this->db->where('subject.status', 0);

		}



	   if ($this->session->userdata('TrashSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('TrashSubjects'));

		}	





		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.adminID', $adminID);





if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('subject.subject_code', 'ASC');  

           } 

           if (isset($_POST["length"])) {

  

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       }



	}



	function make_datatables($adminID){  

           $this->get_join_subject();  

           $query = $this->db->get();  

           return $query->result();  

      } 



	function get_join_where_subject($classesID,$sub_coursesID) {



		$this->db->select('*');



		$this->db->from('subject');



	if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}



		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->order_by('subject.subject_code', 'DESC');  

		$query = $this->db->get();
		return $query->result();

	}



	function get_join_where_subject_count($classesID,$sub_coursesID) {



		$this->db->select('*');



		$this->db->from('subject');



		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);

		$query = $this->db->get();



		return $query->result();



	}



	function get_classes() {



		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');



		$query = $this->db->get();



		return $query->result();



	}



	function get_subject($array=NULL, $signal=FALSE) {



		$query = parent::get($array, $signal);



		return $query;



	}


		function get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID) {


$this->db->where('classesID',$getClasses);
$this->db->where('sub_coursesID',$getSubcourse);
$this->db->where('yearsOrSemester',$getSemester);
$this->db->where('professorID',$loginuserID);
$query = $this->db->get('subject')->result();
		return $query;

	}



	function get_subject_call($id) {



		$query = $this->db->get_where('subject', array('classesID' => $id));



		return $query->result();



	}



	function get_order_by_subject($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}



	function insert_subject($array) {



		$error = parent::insert($array);



		return TRUE;



	}

	function insert_syllabus($array) {

		$this->db->insert('syllabus',$array);
		return TRUE;



	}



	function update_subject($data, $id = NULL) {



		parent::update($data, $id);



		return $id;

	}



	 function GetSubjectByParam($array)

{



	$this->db->where($array);
	$this->db->order_by('subject.subject_code', 'ASC'); 
	$query  = $this->db->get('subject');

	return $query->result();

}



	public function delete_subject($id){



		parent::delete($id);



	}



		function all_count(){

		$this->db->where('status',1);

		$this->db->or_where('status',0);

	$query	= $this->db->get('subject');

	return $query->num_rows();

	}



		function ActiveSubjects_count(){

		 $this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.status',1);

			$query	= $this->db->get();

		return $query->num_rows();

	}



		function DraftSubjects_count(){

		 $this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.status',0);

		$query	= $this->db->get();

		return $query->num_rows();

	}



		function TrashSubjects_count(){

	    $this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.status',2);

		$query	= $this->db->get();

	return $query->num_rows();

	}

}



/* End of file subject_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/subject_m.php */