<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class student_m extends MY_Model {

	protected $_table_name = 'student';

	protected $_primary_key = 'studentID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "roll asc";

	function __construct() {

		parent::__construct();

	}

	function get_username($table, $data=NULL) {



		$query = $this->db->get_where($table, $data);



		return $query->result();



	}

	function get_class($id=NULL) {

		$query = $this->db->get_where('classes', array('classesID' => $id));

		return $query->row();

	}

		function get_educations($id=NULL,$education_detailsID) {

			     $this->db->where_in('education_detailsID',$education_detailsID);

		$query = $this->db->get_where('academic_details', array('studentID' => $id));



		return $query->result();



	}



	function get_subCourses($id=NULL) {



		$query = $this->db->get_where('sub_courses', array('sub_coursesID' => $id));



		return $query->row();



	}







	function get_classes() {

		$this->db->where('status',1);

		$this->db->select('*')->from('classes');



		$query = $this->db->get();



		return $query->result();



	}



	function get_classes_by_superAdmin($adminID) {

        $this->db->where('status',1);

		$this->db->where('adminID',$adminID);

		$this->db->select('*')->from('classes');

		$query = $this->db->get();

		return $query->result();

	}

	function get_parent($id = NULL) {

		$query = $this->db->get_where('parent', array('studentID' => $id));

		return $query->row();

	}

	function get_parent_info($username = NULL) {

		$query = $this->db->get_where('parent', array('username' => $username));

		return $query->row();

	}

	function GetRollbyArray($array=NULL){

		$this->db->where($array);

		$query = $this->db->get('student');

		return $query->num_rows();

	}

	function get_order_by_roll($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}

	public function get_academic_details($id,$education_detailsID){

		$this->db->where_in('education_detailsID',$education_detailsID);

	  	$this->db->where('studentID',$id);

	 	$query   = $this->db->get('academic_details');

	 	return  $query->result();

	}



function get_student($array=NULL, $signal=FALSE) {



           $this->db->where($array);

		   $query  = $this->db->get('student');

		return $query->row();

	}





	function get_student_teacher($array=NULL, $signal=FALSE) {



           $this->db->where($array);

		   $query  = $this->db->get('student');

		return $query->result();

	}







function get_student_by_superAdmin($array=NULL, $signal=FALSE) {



		   $query  = $this->db->get('student');

		   return $query->result();





	}







function get_student_single($array=NULL, $signal=FALSE) {



    

		$query = parent::get($array, $signal);

		return $query;

		

		

	}







function get_student_by_admin($id){
$query	= $this->db->query('SELECT student.studentID,student.status totalP FROM student where  student.status=1');

		return $query->result();

}





function get_session() {

	    $query = $this->db->get('session');

		return $query->result();

	}



	function get_session_id($id) {

				 $this->db->where('sessionID',$id);

		$query = $this->db->get('session');

		return $query->row();

	}



function academic_from() {

		$query = $this->db->get('academic_from');

		return $query->result();

	}

function academic_from_id($id) {

				 $this->db->where('academic_fromID',$id);

		$query = $this->db->get('academic_from');

		return $query->row();

	}





function academic_to() {

		$query = $this->db->get('academic_to');

		return $query->result();

	}



	function academic_to_id($id) {

		$this->db->where('academic_toID',$id);

		$query = $this->db->get('academic_to');

		return $query->row();

	}



function calender_from() {

		$query = $this->db->get('calender_from');

		return $query->result();

	}



	function calender_from_id($id) {

				 $this->db->where('calender_fromID',$id);

		$query = $this->db->get('calender_from');

		return $query->row();

	}



function calender_to() {

		$query = $this->db->get('calender_to');

		return $query->result();

	}

	function calender_to_id($id) {

				$this->db->where('calender_toID',$id);

		$query = $this->db->get('calender_to');

		return $query->row();

	}

function counsellor() {

		$query = $this->db->get('teacher');

		return $query->result();

	}

	function counsellor_id($id) {

				 $this->db->where('teacherID',$id);

		$query = $this->db->get('teacher');

		return $query->row();

	}





  function get_sub_courses($id=NULL) {

	    $this->db->where('classesID',$id);

        $query = $this->db->get('sub_courses');

		return $query->result();

	}







	function get_single_student($array) {

		$query = parent::get_single($array);

		return $query;



	}





function get_acadamic_reqired($id){



	$this->db->where('classesID',$id);

	$query = $this->db->get('classes');

    return $query->row();



}



// function get_std_session($array) {



// 		$query = parent::get_std_session($array);



// 		return $query;



// 	}











	function get_order_by_student($array=NULL) {





		$query = parent::get_order_by($array);



		return $query;



	}







	function get_order_by_student_year($classesID) {



		$query = $this->db->query("SELECT * FROM student WHERE year = (SELECT MIN(year) FROM student) && classesID = $classesID order by roll asc");



		return $query->result();



	}







	function get_order_by_student_single_year($classesID) {



		$query = $this->db->query("SELECT year FROM student WHERE year = (SELECT MIN(year) FROM student) && classesID = $classesID order by roll asc");



		return $query->row();



	}







	function get_order_by_student_single_max_year($classesID) {



		$query = $this->db->query("SELECT year FROM student WHERE year = (SELECT MAX(year) FROM student) && classesID = $classesID order by roll asc");



		return $query->row();



	}







	function get_order_by_studen_with_section_and_classes($classesID) {



		$this->db->select('*');



		$this->db->from('student');



		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');



		$this->db->join('section', 'student.sectionID = section.sectionID', 'LEFT');



		$this->db->where('student.classesID', $classesID);



		$query = $this->db->get();



		return $query->result();



	}







	function get_order_by_studen_with_section($classesID, $sectionID) {



		$this->db->select('*');



		$this->db->from('student');



		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');



		$this->db->join('section', 'student.sectionID = section.sectionID', 'LEFT');



		$this->db->where('student.ClassesID', $classesID);



		$this->db->where('student.sectionID', $sectionID);



		$query = $this->db->get();



		return $query->result();



	}



	function get_order_by_student_by_join_Count($adminID) {

	$this->db->select('student.*,classes.*,sub_courses.*,student.status as studentStatus,student.roll as studentRoll,invoice.*,invoice.status as invoiceStatus');

		$this->db->from('student');

		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');

		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('classes.status', 1);

		$this->db->where('sub_courses.status', 1);

		if ($this->session->userdata('sessionFilter')) {

		 $this->db->where('student.session', $this->session->userdata('sessionFilter'));

		}

		if ($this->session->userdata('classesID')) {

		 	$this->db->where('student.classesID', $this->session->userdata('classesID'));

		}

	   	if ($this->session->userdata('subCourseID')) {

		 	$this->db->where('student.sub_coursesID', $this->session->userdata('subCourseID'));

		}

	   	if ($this->session->userdata('sessionType')) {

		 	$this->db->where('student.sessionType', $this->session->userdata('sessionType'));

		}	    

	    if ($this->session->userdata('education_mode')) {

		 	$this->db->where('student.education_mode', $this->session->userdata('education_mode'));

		}	    

	    if ($this->session->userdata('yos_filter')) {

			$this->db->where('student.yearsOrSemester', $this->session->userdata('yos_filter'));

		}

		if ($this->session->userdata('student_position')) {

		 	$this->db->where('student.yosPosition', $this->session->userdata('student_position'));

		}

         if ($this->session->userdata('yos_filter') and $this->session->userdata('payment_status')) {
           $yearsOrSemester =  return_year($this->session->userdata('yos_filter'));		
         if ($this->session->userdata('payment_status')==1) {
           $where2 = "(invoice.totalfeepaidperyear>(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";

           $where3 = "(EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where2);
           $this->db->where($where3);
       }
       elseif($this->session->userdata('payment_status')==3) {
           $where3 = "(NOT EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where3);
       }
        elseif($this->session->userdata('payment_status')==2) {
            $where2 = "(invoice.totalfeepaidperyear=(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";
           $this->db->where($where2);
       }
   }			

      	if ($this->session->userdata('ActiveStudent')) {

		 $this->db->where('student.status', $this->session->userdata('ActiveStudent'));

		}

	   	if ($this->session->userdata('DraftStudent')) {

		 $this->db->where('student.status', 0);

		}

	   	if ($this->session->userdata('TrashStudent')) {

		 $this->db->where('student.status', $this->session->userdata('TrashStudent'));

		}

		if (empty($this->session->userdata('DraftStudent')) && empty($this->session->userdata('TrashStudent'))) {

           $this->db->where('student.status',1);

        }

        $adminID = $this->session->userdata("adminID");

        $usertype = $this->session->userdata("usertype");

        $loginuserID = $this->session->userdata("loginuserID");

	    $this->db->where('student.adminID', $adminID);
		
		if(isset($_POST["search"]["value"])) {  

 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 

                            1 =>'username',

                            2=> 'roll',

                            3 =>'name',                          

                            4=> 'father_name',

                            5=> 'yearsOrSemester',

                            6=> 'classes',                           

                            7=> 'sub_course',

                            8=> 'session',

                            9=> 'sessionType',

                            10=> 'student_status',

                            11=> 'phone',

                            12=> 'dob',

                            13=> 'sex',

                            14=> 'email',

                            15=> 'mother_name',

                            16=> 'aadhar',

                            17=> 'nationality',

                            18=> 'create_date',

                            19=> 'amount',

                            20=> 'paidamount',

                            // 21=> 'invoiceStatus',

                            21=> 'action',

                        );

           $where = "(student.name LIKE '%$searches%' OR student.username LIKE '%$searches%' OR student.father_name LIKE '%$searches%' OR student.mother_name LIKE '%$searches%' OR student.roll LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR student.session LIKE '%$searches%' OR student.sessionType LIKE '%$searches%' OR student.education_mode LIKE '%$searches%' OR student.phone LIKE '%$searches%' OR student.dob LIKE '%$searches%' OR student.email LIKE '%$searches%' OR student.aadhar LIKE '%$searches%' OR student.nationality LIKE '%$searches%' OR  student.create_date LIKE '%$searches%' OR invoice.amount LIKE '%$searches%' OR invoice.paidamount LIKE '%$searches%' OR invoice.total_install LIKE '%$searches%' OR invoice.c_install LIKE '%$searches%')";

           $this->db->where($where);        

        }

		$query = $this->db->get();

		return  $query->num_rows();

	}

	function get_order_by_student_by_join() {

		$this->db->select('student.*,classes.*,sub_courses.*,student.status as studentStatus,student.roll as studentRoll,invoice.*,invoice.status as invoiceStatus');
		$this->db->from('student');
		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');
		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');
		$this->db->where('classes.status', 1);
		$this->db->where('sub_courses.status', 1);
		if ($this->session->userdata('sessionFilter')) {
		 $this->db->where('student.session', $this->session->userdata('sessionFilter'));
		}
		if ($this->session->userdata('classesID')) {
		 $this->db->where('student.classesID', $this->session->userdata('classesID'));
		}
	   if ($this->session->userdata('subCourseID')) {
		 $this->db->where('student.sub_coursesID', $this->session->userdata('subCourseID'));
		}
	   if ($this->session->userdata('sessionType')) {
		 $this->db->where('student.sessionType', $this->session->userdata('sessionType'));
		}
	    if ($this->session->userdata('education_mode')) {
		 $this->db->where('student.education_mode', $this->session->userdata('education_mode'));
		}
		if ($this->session->userdata('yos_filter')) {

		 $this->db->where('student.yearsOrSemester', $this->session->userdata('yos_filter'));
		}
		if ($this->session->userdata('student_position')) {

		 $this->db->where('student.yosPosition', $this->session->userdata('student_position'));

		}
         if ($this->session->userdata('yos_filter') and $this->session->userdata('payment_status')) {
           $yearsOrSemester =  return_year($this->session->userdata('yos_filter'));		
         if ($this->session->userdata('payment_status')==1) {
           $where2 = "(invoice.totalfeepaidperyear>(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";

           $where3 = "(EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where2);
           $this->db->where($where3);
       }
       elseif($this->session->userdata('payment_status')==3) {
           $where3 = "(NOT EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where3);
       }
        elseif($this->session->userdata('payment_status')==2) {
            $where2 = "(invoice.totalfeepaidperyear=(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";
           $this->db->where($where2);
       }
   }	
      	if ($this->session->userdata('ActiveStudent')) {
		 $this->db->where('student.status', $this->session->userdata('ActiveStudent'));
		}

	   if ($this->session->userdata('DraftStudent')) {

		 $this->db->where('student.status', 0);

		}

	   if ($this->session->userdata('TrashStudent')) {

		 $this->db->where('student.status', $this->session->userdata('TrashStudent'));

		}	

		if (empty($this->session->userdata('DraftStudent')) && empty($this->session->userdata('TrashStudent'))) {

           $this->db->where('student.status',1);

        }
       $adminID = $this->session->userdata("adminID");

       $usertype = $this->session->userdata("usertype");

       $loginuserID = $this->session->userdata("loginuserID");

	   $this->db->where('student.adminID', $adminID);

		if(isset($_POST["search"]["value"])) { 

 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 

                            1 =>'username',

                            2=> 'roll',

                            3 =>'name',                          

                            4=> 'father_name',

                            5=> 'yearsOrSemester',

                            6=> 'classes',                           

                            7=> 'sub_course',

                            8=> 'session',

                            9=> 'sessionType',

                            10=> 'student_status',

                            11=> 'phone',

                            12=> 'dob',

                            13=> 'sex',

                            14=> 'email',

                            15=> 'mother_name',

                            16=> 'aadhar',

                            17=> 'nationality',

                            18=> 'create_date',

                            19=> 'amount',

                            20=> 'paidamount',

                            // 21=> 'invoiceStatus',

                            21=> 'action',

                        );

           $where = "(student.name LIKE '%$searches%' OR student.username LIKE '%$searches%' OR student.father_name LIKE '%$searches%' OR student.mother_name LIKE '%$searches%' OR student.roll LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR student.session LIKE '%$searches%' OR student.sessionType LIKE '%$searches%' OR student.education_mode LIKE '%$searches%' OR student.phone LIKE '%$searches%' OR student.dob LIKE '%$searches%' OR student.email LIKE '%$searches%' OR student.aadhar LIKE '%$searches%' OR student.nationality LIKE '%$searches%' OR  student.create_date LIKE '%$searches%' OR invoice.amount LIKE '%$searches%' OR invoice.paidamount LIKE '%$searches%' OR invoice.total_install LIKE '%$searches%' OR invoice.c_install LIKE '%$searches%')";
  


         $this->db->where($where);

        

           } 

           if(isset($_POST["order"])) {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else {  

                $this->db->order_by('student.studentID', 'DESC');  

           } 



           if(isset($_POST["length"]) && $_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

            } 

		}



		function make_datatables($adminID){  
           $this->get_order_by_student_by_join();  
           $query = $this->db->get();  
           return $query->result();  
      }  

      function get_filtered_data(){ 
           $this->get_order_by_student_by_join(); 
           $query = $this->db->get();  
           return $query->num_rows();  
      }       





		function get_student_by_join_superAdmin() {



		$this->db->select('student.*,classes.*,sub_courses.*');



		$this->db->from('student');



		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');



		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');



		if ($this->session->userdata('sessionFilter')) {

		 $this->db->where('student.session', $this->session->userdata('sessionFilter'));

		}



		if ($this->session->userdata('classesID')) {

		 $this->db->where('student.classesID', $this->session->userdata('classesID'));

		}



	   if ($this->session->userdata('subCourseID')) {

		 $this->db->where('student.sub_coursesID', $this->session->userdata('subCourseID'));

		}

	   if ($this->session->userdata('sessionType')) {

		 $this->db->where('student.sessionType', $this->session->userdata('sessionType'));

		}



	   if ($this->session->userdata('examType')) {

		 $this->db->where('student.examType', $this->session->userdata('examType'));

		}



	    if ($this->session->userdata('education_mode')) {

		 $this->db->where('student.education_mode', $this->session->userdata('education_mode'));

		}

	    



		$query = $this->db->get();

		return $query->result();



	}

	function get_order_by_student_with_courses_by_join($id,$adminID) {

		$this->db->select('*');

		$this->db->from('student');

		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');

		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

		$this->db->where('student.adminID', $adminID);

		$this->db->where('student.classesID', $id);

		$query = $this->db->get();

		return $query->result();



	}





	function insert_student($array,$file_name_renameForDocs) {



		$error = parent::insert($array);

        $year  = $this->input->post('year');

        $subject  = $this->input->post('subject');

        $board  = $this->input->post('board');

        $percent  = $this->input->post('percent');

        $education =  $this->input->post('education_name');

        $education_detailsID =  $this->input->post('education_detailsID');

      

	for ($i=0; $i <count($this->input->post('education_name')); $i++) { 

		$certificate = 'certificate'.$i;

		$filename =  'marksheet'.$i;

		if($_FILES[$filename]['error']==0) {

			$file_name = $_FILES[$filename]['name'];

            $explode = explode('.', $file_name);

            if(count($explode) >= 2) {

	            $new_file = $file_name_renameForDocs.'.'.$explode[1];

				$config['upload_path'] = "./uploads/document/marksheet";

				$config['allowed_types'] = "gif|jpg|png|pdf";

				$config['file_name'] = $new_file;

				$config['max_size'] = '1024';

				$config['max_width'] = '3000';

				$config['max_height'] = '3000';

				$array['photo'] = $new_file;

				$this->load->library('upload', $config);

				$this->upload->do_upload($filename);

					

				$docsArray = array("upload_data" => $this->upload->data());

			    $docsData   =	$docsArray['upload_data']['file_name'];



			}

		}else{

			$docsData = '';

		}



		if($_FILES[$certificate]['error']==0) {

			$file_name2 = $_FILES[$certificate]['name'];

            $explode2 = explode('.', $file_name2);

            if(count($explode2) >= 2) {

	            $new_file = $file_name_renameForDocs.'.'.$explode2[1];

				$config['upload_path'] = "./uploads/document/marksheet";

				$config['allowed_types'] = "gif|jpg|png|pdf";

				$config['file_name'] = $new_file;

				$config['max_size'] = '1024';

				$config['max_width'] = '3000';

				$config['max_height'] = '3000';

				$array['photo'] = $new_file;

				$this->load->library('upload', $config);

				$this->upload->do_upload($certificate);	

			   $certificateArray = array("upload_data" => $this->upload->data());

			$certificate = $certificateArray['upload_data']['file_name'];

			} 

		}else{

			$certificate = '';

		}

		

		$data  = 

		array(

		'studentID'=>$error,

		'education' =>$education[$i],

		'year_passing' =>$year[$i],

		'education_detailsID' =>$education_detailsID[$i],

		'subject' =>$subject[$i],

		'board_name' => $board[$i], 

		'percentage' => $percent[$i],

		'certificate_detail' => $certificate,

		'marksheet_detail' => $docsData





	);



		$this->db->insert('academic_details',$data);

	}

		return $error;



	}





	function update_student($data, $id = NULL, $file_name_renameForDocs) {

		

		parent::update($data, $id);



        $year  = $this->input->post('year');

        $subject  = $this->input->post('subject');

        $board  = $this->input->post('board');

        $percent  = $this->input->post('percent');

        $education =  $this->input->post('education_name');

        $academic_detailsID =  $this->input->post('academic_detailsID');

        $education_detailsID =  $this->input->post('education_detailsID');

	for ($i=0; $i <count($this->input->post('education_name')); $i++) {

		$certificate = 'certificate'.$i;

		$filename =  'marksheet'.$i;

		if($_FILES[$filename]['error']==0) {

			$file_name = $_FILES[$filename]['name'];

            $explode = explode('.', $file_name);

            if(count($explode) >= 2) {

	            $new_file = $file_name_renameForDocs.'.'.$explode[1];

				$config['upload_path'] = "./uploads/document/marksheet";

				$config['allowed_types'] = "gif|jpg|png|pdf";

				$config['file_name'] = $new_file;

				$config['max_size'] = '1024';

				$config['max_width'] = '3000';

				$config['max_height'] = '3000';

				$array['photo'] = $new_file;

				$this->load->library('upload', $config);

				$this->upload->do_upload($filename);

					

				$docsArray = array("upload_data" => $this->upload->data());

				$marksheet_detail  = $docsArray['upload_data']['file_name'];

			} 

		}else{

			$this->db->where('education_detailsID',$education_detailsID[$i]);

			$this->db->where('studentID',$id);

		    $qss  =	$this->db->get('academic_details');

		    $marksheetData = $qss->row(); 

		    $marksheet_detail  = $marksheetData->marksheet_detail;

		}

		if($_FILES[$certificate]['error']==0) {

			$file_name2 = $_FILES[$certificate]['name'];

            $explode2 = explode('.', $file_name2);

            if(count($explode2) >= 2) {

	            $new_file = $file_name_renameForDocs.'.'.$explode2[1];

				$config['upload_path'] = "./uploads/document/marksheet";

				$config['allowed_types'] = "gif|jpg|png|pdf";

				$config['file_name'] = $new_file;

				$config['max_size'] = '1024';

				$config['max_width'] = '3000';

				$config['max_height'] = '3000';

				$array['photo'] = $new_file;

				$this->load->library('upload', $config);

				$this->upload->do_upload($certificate);

					

					$certificateArray = array("upload_data" => $this->upload->data());

				    $certificate_detail  = $certificateArray['upload_data']['file_name'];

			}

		}else{

			$this->db->where('education_detailsID',$education_detailsID[$i]);

			$this->db->where('studentID',$id);

		    $qss  =	$this->db->get('academic_details');

		    $certificateData = $qss->row(); 



		    $certificate_detail  = $certificateData->certificate_detail;

		}



		$data  = 

		array(

		'studentID'=>$id,

		'education' =>$education[$i],

		'year_passing' =>$year[$i],	

		'subject' =>$subject[$i],

		'education_detailsID' =>$education_detailsID[$i],

		'board_name' => $board[$i], 

		'percentage' => $percent[$i],

	    'certificate_detail' =>$certificate_detail ,

		'marksheet_detail' => $marksheet_detail

	);

		$this->db->where('studentID',$id);

		$this->db->where('education_detailsID',$education_detailsID[$i]);

		$this->db->update('academic_details',$data);

	}

		return $id;



	}





		function insert_student_byCSV($array,$Education) {

	  $error = parent::insert($array);



	  foreach ($Education as $key => $value){ 

if (count($value)==4) {

 $year_passing1 = str_replace('Y', ' ' , $value[0]);

 $year_passing =  str_replace('-', ' ' , $year_passing1);

}else{

 $year_passing = '';	

}

if (count($value)==4) {

 $subjects1 = str_replace('S', ' ' , $value[1]);

 $subjects =  str_replace('-', ' ' , $subjects1);

}else{

 $subjects = '';	

}

if (count($value)==4) {

 $board1 = str_replace('B', ' ' , $value[2]);

 $board =  str_replace('-', ' ' , $board1);

}else{

 $board = '';	

}

if (count($value)==4) {

 $percent1 = str_replace('P', ' ' , $value[3]);

 $percent =  str_replace('-', ' ' , $percent1);

}else{

 $percent = '';	

}



if ($key=='tenth') {

 $educationName = '10th Class';

 $education_detailsID = 1;

}

 elseif ($key=='twelth') {

 $educationName = '12th Class';

 $education_detailsID = 2;

}

 elseif ($key=='Graduation') {

 $educationName = 'Graduation';

 $education_detailsID = 3;

}elseif ($key=='PostGraduation') {

 $educationName = 'Post Graduation';

 $education_detailsID = 4;

}elseif ($key=='Other') {

 $educationName = 'Other';

 $education_detailsID = 5;

}





$data  = array(

		'studentID'=>$error,

		'education'=>$educationName,

		'year_passing'=>$year_passing,

		'education_detailsID'=>$education_detailsID,

		'subject'=>$subjects,

		'board_name'=>$board, 

		'percentage' =>$percent

	);

		$this->db->insert('academic_details',$data);

	}

		return $error;



	}

			function update_student_byCSV($array,$Education,$studentID) {

	  parent::update($array, $studentID);

	  foreach ($Education as $key => $value){ 

if (count($value)==4) {

 $year_passing1 = str_replace('Y', ' ' , $value[0]);

 $year_passing =  str_replace('-', ' ' , $year_passing1);

}else{

 $year_passing = '';	

}

if (count($value)==4) {

 $subjects1 = str_replace('S', ' ' , $value[1]);

 $subjects =  str_replace('-', ' ' , $subjects1);

}else{

 $subjects = '';	

}

if (count($value)==4) {

 $board1 = str_replace('B', ' ' , $value[2]);

 $board =  str_replace('-', ' ' , $board1);

}else{

 $board = '';	

}

if (count($value)==4) {

 $percent1 = str_replace('P', ' ' , $value[3]);

 $percent =  str_replace('-', ' ' , $percent1);

}else{

 $percent = '';	

}



if ($key=='tenth') {

 $educationName = '10th Class';

 $education_detailsID = 1;

}

 elseif ($key=='twelth') {

 $educationName = '12th Class';

 $education_detailsID = 2;

}

 elseif ($key=='Graduation') {

 $educationName = 'Graduation';

 $education_detailsID = 3;

}elseif ($key=='PostGraduation') {

 $educationName = 'Post Graduation';

 $education_detailsID = 4;

}elseif ($key=='Other') {

 $educationName = 'Other';

 $education_detailsID = 5;

}





$data  = array(

		'education'=>$educationName,

		'year_passing'=>$year_passing,

		'education_detailsID'=>$education_detailsID,

		'subject'=>$subjects,

		'board_name'=>$board, 

		'percentage' =>$percent

	);

		$this->db->where('studentID',$studentID);

		$error = $this->db->update('academic_details',$data);

	}

		return $error;



	}









	function insert_parent($array) {



		$this->db->insert('parent', $array);



		return TRUE;



	}









	function update_student_classes($data, $array = NULL) {



		$this->db->set($data);



		$this->db->where($array);



		$this->db->update($this->_table_name);



	}







	function delete_student($id){



		parent::delete($id);



	}







	function delete_parent($id){



		$this->db->delete('parent', array('studentID' => $id));



	}





		function get_counsellor(){



		 $query = $this->db->get('teacher');

		 return  $query->result();



	}



    function get_counsellor_byAdmin($adminID){

                  $this->db->where('adminID',$adminID);

		 $query = $this->db->get('teacher');

		 return  $query->result();

	}







	function hash($string) {



		return parent::hash($string);



	}



		function all_count(){

		$this->db->where('status',1);

		$this->db->or_where('status',0);

	$query	= $this->db->get('student');

	return $query->num_rows();

	}



		function ActiveStudent_count(){

		$this->db->from('student');

		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');

		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

	   	$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('classes.status', 1);

		$this->db->where('sub_courses.status', 1);

		$this->db->where('student.status',1);

	   $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");

		

		$query	= $this->db->get();

	return $query->num_rows();





	}



		function DraftStudent_count(){

		$this->db->from('student');

		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');

		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

	   	$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('classes.status', 1);

		$this->db->where('sub_courses.status', 1);

		$this->db->where('student.status',0);

	    $adminID = $this->session->userdata("adminID");
        $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");


	    


		$query	= $this->db->get();

	return $query->num_rows();

	}



		function TrashStudent_count(){

			$this->db->from('student');

		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');

		$this->db->join('sub_courses','student.sub_coursesID = sub_courses.sub_coursesID','LEFT');

	   	$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('classes.status', 1);

		$this->db->where('sub_courses.status', 1);

		$this->db->where('student.status',2);

       $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");

	

		$query	= $this->db->get();

	return $query->num_rows();

	}







	/* infinite code starts here */



}







/* End of file student_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/student_m.php */