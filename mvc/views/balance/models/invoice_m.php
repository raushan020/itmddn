<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');







class Invoice_m extends MY_Model {







	protected $_table_name = 'invoice';



	protected $_primary_key = 'invoiceID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "invoiceID desc";



	







	function __construct() {



		parent::__construct();



	}







	function get_join_where_classes($studentID, $classesID) {



		$this->db->select('*');



		$this->db->from('invoice');



		$this->db->join('classes', 'classes.classesID = invoice.classesID', 'LEFT');



		$this->db->where("invoice.classesID", $classesID);



		$this->db->where("invoice.studentID", $studentID);



		$query = $this->db->get();



		return $query->result();



	}











	function get_invoice($array=NULL, $signal=FALSE) {



		$query = parent::get($array, $signal);



		return $query;



	}



		function totalPayment() {

		$query	= $this->db->query('SELECT SUM(paymentamount) total FROM payment ');

	    	

	    return $query->row();

	

	}







		function duePayment() {

	   $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");

	if ($usertype=='Teacher') {

 $query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.paidamount) totalP FROM invoice inner join student on student.studentID = invoice.studentID where  student.status=1 and student.counsellor=$loginuserID");

		// $query	= $this->db->query('SELECT SUM(paidamount) totalP FROM invoice ');


 $queryTotal	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.amount) totalA FROM invoice inner join student on student.studentID = invoice.studentID where student.status=1 and student.counsellor=$loginuserID");


		}else{

		
 $query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.paidamount) totalP FROM invoice inner join student on student.studentID = invoice.studentID where  student.status=1");

		// $query	= $this->db->query('SELECT SUM(paidamount) totalP FROM invoice ');


 $queryTotal	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.amount) totalA FROM invoice inner join student on student.studentID = invoice.studentID where student.status=1");
		
		}



			return $queryTotal->row()->totalA-$query->row()->totalP;

	}



			function totalPayment_full() {



	   $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");


	if ($usertype=='Teacher') {

	  $query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.amount) total FROM invoice inner join student on student.studentID = invoice.studentID where invoice.status=2 and student.status=1 and student.counsellor=$loginuserID");


		}else{
		
		$query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.amount) total FROM invoice inner join student on student.studentID = invoice.studentID where invoice.status=2 and student.status=1");
		
		}



	    return $query->row();

		



	}



			function totalPayment_part() {

	   $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");


		// $query	= $this->db->query('SELECT SUM(paidamount) total FROM invoice where status=1');

	    


	if ($usertype=='Teacher') {

	$query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.paidamount) total FROM invoice inner join student on student.studentID = invoice.studentID where invoice.status=1 and student.status=1 and student.counsellor=$loginuserID");


		}else{
				$query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.paidamount) total FROM invoice inner join student on student.studentID = invoice.studentID where invoice.status=1 and student.status=1");
		}


	    return $query->row();

		



	}



			function totalPayment_not() {


       $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");




	// $query	= $this->db->query('SELECT SUM(amount) total FROM invoice where status=0');

	    	if ($usertype=='Teacher') {

          $query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.amount) total FROM invoice inner join student on student.studentID = invoice.studentID where invoice.status=0 and student.status=1 and student.status=$loginuserID");

		}else{
	    $query	= $this->db->query("SELECT student.studentID,student.status,invoice.status, SUM(invoice.amount) total FROM invoice inner join student on student.studentID = invoice.studentID where invoice.status=0 and student.status=1");
		}





	    return $query->row();

		



	}







		function get_invoice_byAdmin($id,$adminID) {

			     $this->db->where('invoiceID',$id);

			     $this->db->where('adminID',$adminID);

	    $query = $this->db->get('invoice');

		return $query->row();



	}



		function get_invoice_byAdminID($adminID) {

	   $usertype = $this->session->userdata("usertype");

       $loginuserID = $this->session->userdata("loginuserID");

		$this->db->select('invoice.*, student.studentID');
		$this->db->from('invoice');
		$this->db->join('student','invoice.studentID=student.studentID','INNER');
	    $this->db->where('invoice.adminID',$adminID);
			    	if ($usertype=='Teacher') {

		 $this->db->where('student.counsellor', $loginuserID);

		}

			    $this->db->where('student.status',1);
	    $query = $this->db->get();

		return $query->result();



	}



	function get_invoice_for_students($id,$studentID) {

       

        $this->db->where('invoiceID',$id);

        $this->db->where('studentID',$studentID);

	    $query = $this->db->get('invoice');



        return $query->row();



	}







	function get_order_by_invoice($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}







	function get_single_invoice($array=NULL) {



		$query = parent::get_single($array);



		return $query;



	}







	function insert_invoice($array) {



		$error = parent::insert($array);



		return $error;



	}







	function update_invoice($data, $id = NULL) {



	$error =	parent::update($data, $id);



		return $error;



	}



		function update_invoice_byCsv($data, $studentID) {

    $this->db->where('studentID',$studentID);

   $error = $this->db->update('invoice', $data);



		return $error;



	}

function get_order_by_student_by_join_Count($adminID) {

	$this->db->select('student.*,classes.*,sub_courses.*,student.status as studentStatus,student.roll as studentRoll,invoice.*,invoice.status as invoiceStatus');

		$this->db->from('student');

		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');

		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');

		$this->db->where('classes.status', 1);

		$this->db->where('sub_courses.status', 1);

		if ($this->session->userdata('sessionFilter_invoice')) {

		 $this->db->where('student.session', $this->session->userdata('sessionFilter_invoice'));

		}

		if ($this->session->userdata('classesID_invoice')) {

		 	$this->db->where('student.classesID', $this->session->userdata('classesID_invoice'));

		}

	   	if ($this->session->userdata('subCourseID_invoice')) {

		 	$this->db->where('student.sub_coursesID', $this->session->userdata('subCourseID_invoice'));

		}	    

	    if ($this->session->userdata('yos_filter_invoice')) {

			$this->db->where('student.yearID', $this->session->userdata('yos_filter_invoice'));

		}



         if ($this->session->userdata('yos_filter_invoice') and $this->session->userdata('payment_status_invoice')) {
           $yearsOrSemester =  return_year($this->session->userdata('yos_filter_invoice'));		
         if ($this->session->userdata('payment_status_invoice')==1) {
           $where2 = "(invoice.totalfeepaidperyear>(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";

           $where3 = "(EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where2);
           $this->db->where($where3);
       }
       elseif($this->session->userdata('payment_status_invoice')==3) {
           $where3 = "(NOT EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where3);
       }
        elseif($this->session->userdata('payment_status_invoice')==2) {
            $where2 = "(invoice.totalfeepaidperyear=(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";
           $this->db->where($where2);
       }
   }			


         $this->db->where('student.status',1);

        $adminID = $this->session->userdata("adminID");

        $usertype = $this->session->userdata("usertype");

        $loginuserID = $this->session->userdata("loginuserID");

	    $this->db->where('student.adminID', $adminID);
		
		if(isset($_POST["search"]["value"])) {  

 			$searches  =  $_POST["search"]["value"];
			$order_column = array( 

                            0 =>'sn', 

                            1=> 'roll',

                            2 =>'name',                          

                            3=> 'amount',

                            4=> 'paidamount',

                            5=> 'dueamount',

                            6=> 'action',

                        );

           
           $where = "(student.name LIKE '%$searches%' OR student.roll LIKE '%$searches%' OR invoice.amount LIKE '%$searches%' OR invoice.paidamount LIKE '%$searches%')";
  

           $this->db->where($where);        

        }

		$query = $this->db->get();

		return  $query->num_rows();

	}

	function get_order_by_student_by_join() {

		$this->db->select('student.*,classes.*,sub_courses.*,student.status as studentStatus,student.roll as studentRoll,invoice.*,invoice.status as invoiceStatus');
		$this->db->from('student');
		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');
		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');
		$this->db->where('classes.status', 1);
		$this->db->where('sub_courses.status', 1);
		if ($this->session->userdata('sessionFilter_invoice')) {
		 $this->db->where('student.session', $this->session->userdata('sessionFilter_invoice'));
		}
		if ($this->session->userdata('classesID_invoice')) {
		 $this->db->where('student.classesID', $this->session->userdata('classesID_invoice'));
		}
	   
		if ($this->session->userdata('yos_filter_invoice')) {

		 $this->db->where('student.yearID', $this->session->userdata('yos_filter_invoice'));
		}
	
         if ($this->session->userdata('yos_filter_invoice') and $this->session->userdata('payment_status_invoice')) {
           $yearsOrSemester =  return_year($this->session->userdata('yos_filter_invoice'));		
         if ($this->session->userdata('payment_status_invoice')==1) {
           $where2 = "(invoice.totalfeepaidperyear>(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";

           $where3 = "(EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where2);
           $this->db->where($where3);
       }
       elseif($this->session->userdata('payment_status_invoice')==3) {
           $where3 = "(NOT EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where3);
       }
        elseif($this->session->userdata('payment_status_invoice')==2) {
            $where2 = "(invoice.totalfeepaidperyear=(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";
           $this->db->where($where2);
       }
   }	
      	

	$this->db->where('student.status',1);

       $adminID = $this->session->userdata("adminID");

       $usertype = $this->session->userdata("usertype");

       $loginuserID = $this->session->userdata("loginuserID");

	   $this->db->where('student.adminID', $adminID);

		if(isset($_POST["search"]["value"])) { 

 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 

                            1=> 'roll',

                            2 =>'name',                          

                            3=> 'amount',

                            4=> 'paidamount',

                            5=> 'dueamount',

                            6=> 'action',

                        );

           $where = "(student.name LIKE '%$searches%' OR student.roll LIKE '%$searches%' OR invoice.amount LIKE '%$searches%' OR invoice.paidamount LIKE '%$searches%')";
  


         $this->db->where($where);

        

           } 

           if(isset($_POST["order"])) {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else {  

                $this->db->order_by('student.studentID', 'DESC');  

           } 



           if(isset($_POST["length"]) && $_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

            } 

		}



		function make_datatables($adminID){  
           $this->get_order_by_student_by_join();  
           $query = $this->db->get(); 

           return $query->result();  
      }  

	
		


function total_revenue() {

	if ($this->session->userdata('yos_filter_invoice')) {
		$this->db->select('SUM(totalfeepaidperyear) as amount');
	}else{
		$this->db->select_sum('amount');
	}
		$this->db->from('student');
		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');
		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');
		$this->db->where('classes.status', 1);
		$this->db->where('sub_courses.status', 1);

		if ($this->session->userdata('sessionFilter_invoice')) {
		 $this->db->where('student.session', $this->session->userdata('sessionFilter_invoice'));
		}
		if ($this->session->userdata('classesID_invoice')) {
		 $this->db->where('student.classesID', $this->session->userdata('classesID_invoice'));
		}
		if ($this->session->userdata('yos_filter_invoice')) {

		 $this->db->where('student.yearID', $this->session->userdata('yos_filter_invoice'));
		}

         if ($this->session->userdata('yos_filter_invoice') and $this->session->userdata('payment_status_invoice')) {
           $yearsOrSemester =  return_year($this->session->userdata('yos_filter_invoice'));		
         if ($this->session->userdata('payment_status_invoice')==1) {
           $where2 = "(invoice.totalfeepaidperyear>(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";

           $where3 = "(EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where2);
           $this->db->where($where3);
       }
       elseif($this->session->userdata('payment_status_invoice')==3) {
           $where3 = "(NOT EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where3);
       }
        elseif($this->session->userdata('payment_status_invoice')==2) {
            $where2 = "(invoice.totalfeepaidperyear=(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";
           $this->db->where($where2);
       }
   }	
         
       $this->db->where('student.status',1);
       $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");
	   $this->db->where('student.adminID', $adminID);


	  return $this->db->get()->row();


		}

function total_due() {

		$this->db->select('SUM(amount)-SUM(paidamount) as due');
		$this->db->from('student');
		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');
		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');
		$this->db->where('classes.status', 1);
		$this->db->where('sub_courses.status', 1);

		if ($this->session->userdata('sessionFilter_invoice')) {
		 $this->db->where('student.session', $this->session->userdata('sessionFilter_invoice'));
		}
		if ($this->session->userdata('classesID_invoice')) {
		 $this->db->where('student.classesID', $this->session->userdata('classesID_invoice'));
		}
		if ($this->session->userdata('yos_filter_invoice')) {

		 $this->db->where('student.yearID', $this->session->userdata('yos_filter_invoice'));
		}

         
       $this->db->where('student.status',1);
       $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");
	   $this->db->where('student.adminID', $adminID);

	  return $this->db->get()->row();


			}

function total_paid() {
	if ($this->session->userdata('yos_filter_invoice')) {
		$yearsOrSemester = 	$this->session->userdata('yos_filter_invoice');
    $this->db->select('SUM(payment.paymentamount) as paid');
	}else{
	$this->db->select('SUM(paidamount) as paid');		
	}
		$this->db->from('student');
		$this->db->join('classes', 'student.classesID = classes.classesID', 'LEFT');
		$this->db->join('sub_courses', 'student.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');
		$this->db->join('invoice', 'student.studentID = invoice.studentID', 'LEFT');
		if ($this->session->userdata('yos_filter_invoice')) {
		$this->db->join("payment", "payment.invoiceID = invoice.invoiceID", "LEFT");
	}
		$this->db->where('classes.status', 1);
		$this->db->where('sub_courses.status', 1);

		if ($this->session->userdata('sessionFilter_invoice')) {
		 $this->db->where('student.session', $this->session->userdata('sessionFilter_invoice'));
		}
		if ($this->session->userdata('classesID_invoice')) {
		 $this->db->where('student.classesID', $this->session->userdata('classesID_invoice'));
		}
		if ($this->session->userdata('yos_filter_invoice')) {
		 $this->db->where('student.yearID', $this->session->userdata('yos_filter_invoice'));
		}

         if ($this->session->userdata('yos_filter_invoice') and $this->session->userdata('payment_status_invoice')) {
           $yearsOrSemester =  return_year($this->session->userdata('yos_filter_invoice'));		
         if ($this->session->userdata('payment_status_invoice')==1) {
           $where2 = "(invoice.totalfeepaidperyear>(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";

           $where3 = "(EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where2);
           $this->db->where($where3);
       }
       elseif($this->session->userdata('payment_status_invoice')==3) {
           $where3 = "(NOT EXISTS (SELECT SUM(paymentamount)  from payment where yearsOrSemester =  '$yearsOrSemester' and paymentamount !=0 and payment.invoiceID = invoice.invoiceID group by payment.invoiceID))";
           $this->db->where($where3);
       }
        elseif($this->session->userdata('payment_status_invoice')==2) {
            $where2 = "(invoice.totalfeepaidperyear=(SELECT SUM(paymentamount) from payment where yearsOrSemester =  '$yearsOrSemester' and payment.invoiceID = invoice.invoiceID group by payment.invoiceID) )";
           $this->db->where($where2);
       }
   }	
         
       $this->db->where('student.status',1);
       $adminID = $this->session->userdata("adminID");
       $usertype = $this->session->userdata("usertype");
       $loginuserID = $this->session->userdata("loginuserID");
	   $this->db->where('student.adminID', $adminID);


	 
	  
if ($this->session->userdata('yos_filter_invoice')) {
	  $this->db->group_by('payment.invoiceID');
	return   $this->db->get()->result();
}else{
	return $this->db->get()->row();
}	   
	  

		}

	public function delete_invoice($id){



		parent::delete($id);



	}


function total_revenue_dashboard(){
 return	$this->db->query("SELECT SUM(amount) as amount from invoice ")->row();
}


function total_paid_dashboard(){
 return	$this->db->query("SELECT SUM(paidamount) as paidamount  from invoice ")->row();
}



}







/* End of file invoice_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/invoice_m.php */