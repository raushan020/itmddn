<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Mark_m extends MY_Model {



	protected $_table_name = 'mark';

	protected $_primary_key = 'markID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "subject asc";



	function __construct() {

		parent::__construct();

	}



	function get_mark($examID) {

		
		  $this->db->where('examID',$examID);
          $query = $this->db->get('mark');
		  return $query->result();

	}



	function get_order_by_mark($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}



	function insert_mark($array) {

		$error = parent::insert($array);

		return TRUE;

	}



	function update_mark($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}



	function update_mark_classes($array, $id) {

		$this->db->update($this->_table_name, $array, $id);

		return $id;

	}



	public function delete_mark($id){

		parent::delete($id);

	}



	function sum_student_subject_mark($studentID, $classesID, $subjectID) {

		$array = array(

			"studentID" => $studentID,

			"classesID" => $classesID,

			"subjectID" => $subjectID

		);

		$this->db->select_sum('mark');

		$this->db->where($array);

		$query = $this->db->get('mark');

		return $query->row();

	}



	function count_subject_mark($studentID, $classesID, $subjectID) {

		$query = "SELECT COUNT(*) as 'total_semester' FROM mark WHERE studentID = $studentID && classesID = $classesID && subjectID = $subjectID && (mark != '' || mark <= 0 || mark >0)";

	    $query = $this->db->query($query);

	    $result = $query->row();

	    return $result;

	}


	function get_join_result_count($adminID) {

		if(isset($_POST["search"]["value"]))  
        {  
 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 
                            1 =>'studentID',
                            2 =>'subject_code',
                            3 =>'subject',
                            4 =>'classes',
                            5 =>'sub_course',
                            6 =>'yearsOrSemester',
                            7 =>'action'
                        );
           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 

		$this->db->select('*');

		$this->db->from('ets_result');

		$this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

		$this->db->join('student', 'student.studentID = ets_result.uid', 'LEFT');

		$this->db->join('classes', 'classes.classesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

		$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');

		


		if ($this->session->userdata('FilterSubjectclassesID')) {
		 $this->db->where('ets_quiz.classesID', $this->session->userdata('FilterSubjectclassesID'));
		}

		if ($this->session->userdata('FilterSubjectsubCourseID')) {
		 $this->db->where('ets_quiz.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));
		}

		if ($this->session->userdata('FilterSubjectyearSemesterID')) {
		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));
		}
	     	

		
		$query = $this->db->get();

		return $query->num_rows();

	}


	function get_join_result() {

		$adminID = $this->session->userdata("adminID");
		
		$this->db->select('*');

		$this->db->from('ets_result');

		$this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

		$this->db->join('student', 'student.studentID = ets_result.uid', 'LEFT');

		$this->db->join('classes', 'classes.classesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

		$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');
		
		if ($this->session->userdata('FilterMarkclassesID')) {
		 $this->db->where('ets_quiz.classesID', $this->session->userdata('FilterMarkclassesID'));
		}

		if ($this->session->userdata('FilterMarksubCourseID')) {
		 $this->db->where('ets_quiz.sub_coursesID', $this->session->userdata('FilterMarksubCourseID'));
		}

		if ($this->session->userdata('FilterMarkyearSemesterID')) {
		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterMarkyearSemesterID'));
		}


		// $this->db->where('classes.status',1);
		// $this->db->where('sub_courses.status',1);
		// $this->db->where('subject.adminID', $adminID);



		if(isset($_POST["search"]["value"]))  
        {  
 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 
                            1 =>'studentID',
                            2 =>'subject_code',
                            3 =>'subject',
                            4 =>'classes',
                            5 =>'sub_course',
                            6 =>'yearsOrSemester',
                            7 =>'action'
                        );
           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 

           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
           		$this->db->order_by('ets_result.rid', 'DESC'); 
               
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 

            // $this->db->group_by('ets_result.uid', 'DESC');  
       }

	}

	function make_datatables($adminID){  
		$this->get_join_result();             
           $query = $this->db->get();  
           return $query->result();  
      } 


	function get_join_mark($adminID) {		

		// $this->db->join('subject', 'subject.subjectID = mark.subjectID', 'INNER');

		$this->db->select('*');
		// $this->db->where();

		$this->db->from('ets_result');

	    // $this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

	 //    $this->db->join('ets_quiz', 'ets_quiz.classesID = ets_result.classesID', 'LEFT');

		// $this->db->join('ets_quiz', 'ets_quiz.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

		

		$query = $this->db->get();

	    $result = $query->row();

	    return $result;

	}	



	function get_order_by_mark_with_subject($classes,$year) {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->join('mark', 'subject.subjectID = mark.subjectID', 'LEFT');

		$this->db->join('exam', 'exam.examID = mark.examID');

		$this->db->where('mark.classesID', $classes);

		$this->db->where('mark.year', $year);

		$query = $this->db->get();

		return $query->result();

	}



	function get_order_by_mark_with_highest_mark($classID,$studentID) {

		$this->db->select('M.markID,M.examID, M.exam, M.subjectID, M.subject, M.studentID, M.classesID,  M.mark, M.year, (

		SELECT Max( mark.mark )

		FROM mark

		WHERE mark.subjectID = M.subjectID

		AND mark.examID = M.examID

		) highestmark');

		$this->db->from('exam E');

		$this->db->join('mark M', 'M.examID = E.examID', 'LEFT');

		$this->db->join('subject S', 'M.subjectID = S.subjectID');

		$this->db->where('M.classesID', $classID);

		$this->db->where('M.studentID', $studentID);

		$query = $this->db->get();

		return $query->result();

	}

}



/* End of file mark_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/mark_m.php */

