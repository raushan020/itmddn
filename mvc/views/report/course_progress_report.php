

<div class="">

   <div class="row page-titles">

      <div class="col-md-5 align-self-center">

         <h3 class="text-themecolor"><i class="fa fa-eye"></i> Course Progress Report </h3>

      </div>

      <div class="col-md-7 align-self-center">

         <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">Course Progress Report</li>

         </ol>

      </div>

   </div>

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card-body">

               <div class="col-sm-12">

                  <form style="" class="form-horizontal"  autocomplete="off" role="form">

                     <?php 

                        // $getClasses = $this->input->get('course');
                        $getProfessor=$this->input->get('Professor');
                        $getSemester = $this->input->get('yearsOrSemester');
                        $getSubcourse = $this->input->get('subcourse');
                        $reporttype = $this->input->get('reporttype');
                        $getSubject = $this->input->get('subject');
                        $year=$this->input->get('year');
                        $month=$this->input->get('month');
                        $percentage=$this->input->get('percentage');

                        $type = $this->input->get('type');

                        if($type){

                          $type = $this->input->get('type');

                        }else{

                          $type = "filter";

                        }
                        $date = $this->input->get('date');
                      ?>

                     <?php 

                        if($type=="filter") 

                        {

                         ?>

                     <input type="hidden" name="type" value="<?php echo $type ?>">

                      <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Professor

                              </label>

                              <div class="">

                                 <select id="Professor" name="Professor" required class="form-control" onchange='reportbyprofessor($(this).val())'>

                                  <option>Select</option>
                                    <?php  foreach ($Professor as $Professor)
                                    {
                                       if ($Professor->professorID==$getProfessor)
                                       {
                                        $selected =  "Selected";
                                       }
                                       else
                                       {
                                        $selected =  "";
                                       }
                                    ?>
                                    <option value="<?php echo $Professor->professorID ?>" <?php echo $selected ?>><?php echo $Professor->name ?></option>

                                    <?php } ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="reporttype" class="control-label">Report Type</label>

                              <div class="">

                                 <select class='form-control' name='reporttype' id='reporttype' required onchange='GetsemesterAtd($(this).val())'>

                                    <option>Select</option>                            
                                    <!-- <option value="1" <?php if($reporttype == 1){ echo "selected"; }  ?>>Month</option> -->
                                    <option value="14" <?php if($reporttype == 14){ echo "selected"; }  ?>>Semester Wise</option>
                                    <option value="13" <?php if($reporttype == 13){ echo "selected"; }  ?>>Year Wise</option>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div> 
                     <div class="col-sm-2" id="monthselect">

                        <div class="">

                           <div class="form-group">

                              <label for="month" class="control-label">Select Month</label>

                              <div class="">

                                 <select class='form-control' name='month' id='month' required >

                                    <option>Select</option>                            
                                    <option value="1" <?php if($month == 1){ echo "selected"; }  ?>>Jan</option>
                                    <option value="2" <?php if($month == 2){ echo "selected"; }  ?>>Feb</option>
                                    <option value="3" <?php if($month == 3){ echo "selected"; }  ?>>Mar</option>
                                    <option value="4" <?php if($month == 4){ echo "selected"; }  ?>>Apr</option>
                                    <option value="5" <?php if($month == 5){ echo "selected"; }  ?>>May</option>
                                    <option value="6" <?php if($month == 6){ echo "selected"; }  ?>>Jun</option>
                                    <option value="7" <?php if($month == 7){ echo "selected"; }  ?>>Jul</option>
                                    <option value="8" <?php if($month == 8){ echo "selected"; }  ?>>Aug</option>
                                    <option value="9" <?php if($month == 9){ echo "selected"; }  ?>>Sep</option>
                                    <option value="10" <?php if($month == 10){ echo "selected"; }  ?>>Oct</option>
                                    <option value="11" <?php if($month == 11){ echo "selected"; }  ?>>Nov</option>
                                    <option value="12" <?php if($month == 12){ echo "selected"; }  ?>>Dec</option>
                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div> 
                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Semester/Year</label>

                              <div class="">

                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>

                                    <option>Select</option>

                                    <?php 

                                       if ($reporttype != 1) {

                                       

                                        $looping    =  (int) $classesRow->duration;

                                       

                                       if ($classesRow->mode==1) {

                                       

                                         for ($i=1; $i <=$looping; $i++) {

                                                if (CallYears($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                       

                                        

                                       

                                       }

                                       

                                       }

                                       

                                       

                                       

                                       else{

                                       

                                         for ($i=1; $i <=(2*$looping); $i++) {

                                       

                                       if (CallSemester($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       

                                       }

                                       

                                       }

                                       

                                       }
                                       else
                                       {
                                          $loopingyear    =  (int) $classesRowyear->duration;
                                          $looping    =  (int) $classesRow->duration;
                                          if ($classesRowyear->mode==1)
                                          { 
                                            for ($i=1; $i <=$loopingyear; $i++)
                                            {
                                              if (CallYears($i)==$getSemester)
                                              {
                                                $select = 'Selected';
                                              }
                                              else
                                              {
                                                $select = '';
                                              }
                                              echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                            }
                                          }
                                          if($classesRow->mode==2)
                                          {
                                            for ($i=1; $i <=(2*$looping); $i++)
                                            {
                                              if (CallSemester($i)==$getSemester)
                                              {
                                                $select = 'Selected';
                                              }
                                              else
                                              {
                                                $select = '';
                                              }
                                              echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                            }
                                          }
                                       }

                                       

                                       ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>                   

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Year</label>

                              <div class="">

                                 <select id="year" name="year" required class="form-control">
                                    <option>Select Year</option>
                                    <option value="2019" <?php if($year == 2019){ echo "selected"; }  ?>>2019</option>
                                    <option value="2020" <?php if($year == 2020){ echo "selected"; }  ?>>2020</option>
                                    <option value="2021" <?php if($year == 2021){ echo "selected"; }  ?>>2021</option>
                                    <option value="2022" <?php if($year == 2022){ echo "selected"; }  ?>>2022</option>
                                    <option value="2023" <?php if($year == 2023){ echo "selected"; }  ?>>2023</option>
                                    <option value="2024" <?php if($year == 2024){ echo "selected"; }  ?>>2024</option>
                                    <option value="2025" <?php if($year == 2025){ echo "selected"; }  ?>>2025</option>
                                    <option value="2026" <?php if($year == 2026){ echo "selected"; }  ?>>2026</option>
                                  </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>
                     
                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label"></label>

                              <div class="">

                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 15px;" name="Search">

                              </div>

                           </div>

                        </div>

                     </div>

                     <div class="clearfix"></div>

                     <?php } ?>
                    
                     

                     

                  </form>

                  <div class="col-sm-6">

                  </div>

                  <?php if($subject != 0)
                  { ?>

                  <div class="clearfix"></div>

                  <div class="table table-responsive table-wrapper">

                     <div class="col-sm-12">

                        <div id="hide-table">              

                           <div class="col-sm-4"></div>

                              <div class="col-sm-4">

                                 <div class="">

                                 </div>

                              </div>

                              <div class="col-sm-4"></div>

                              <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="professorexportdebarredlist">

                                 <thead>
<!-- <input type="checkbox" name="" onclick="selectAllsendmail()"> -->
                                    <tr>
                                       <!-- <th class="col-sm-1">Check</th> -->
                                       <td>S.N.</td>
                                       <td>Course Name</td>
                                       <td>Semester</td>
                                       <td>Subject Name</td>

                                       <td>Subject Progress</td>                                  

                                    </tr>

                                 </thead>

                                 <tbody>

                                    <?php
                                     count($subject); $count=1;
                                    foreach($subject as $subjects => $value)
                                    { ?>

                                   <tr>

                                    <td><?php echo $count?></td>
                                    <td><?php echo $value['classes']?></td>
                                    <td><?php echo $value['yearsOrSemester']?></td>
                                    <td><?php echo $value['subject']?></td>

                                    <td>

                                      <?php 

                                      foreach ($value['object'] as $key )
                                      { 
                                        $a=$key->topicid;

                                        $b=$key->totalunit;

                                        if($a && $b)

                                        {

                                          $topicconut=explode(",",$a);

                                          $avg=(count($topicconut)*100)/$b;

                                          $unitavg=round($avg,0);



                                          ?>

                                          
                                            <?php
                                            if($unitavg)
                                            {
                                               echo $unitavg.'%';
                                            }
                                            else
                                            {
                                              echo "0%";
                                            }         
                                        

                                       }
                                       else
                                       {
                                        echo "0%";
                                       }
                                    } ?> 

                                    </td>
                                  </tr>
                                    <?php $count++; } ?>
                                 </tbody>

                              </table>

                        </div>

                     </div>

                  </div>  
                  <?php }
                  else
                    { ?>
                      <div class="Noresuls">

                              <h1>Sorry we couldn't find any matches</h1>

                              <p>Maybe your search was too specific, please try searching with another term.</p>

                              <img src="https://www.itmddn.online/uploads/images/crying.png">

                           </div>
                <?php }?>        

               </div>

               <!-- col-sm-12 -->

            </div>

            <!-- row -->

         </div>

         <!-- Body -->

      </div>

      <!-- /.box -->

   </div>
<script type="text/javascript">
  function reportbyprofessor(val)
  {
    var Professor=val;

        $.ajax({

   

         url: base_url+'AjaxController/Professorprogressreport',

   

         type: 'POST',

   

          data:{Professor:Professor},

   

         success: function(data){

   

         $('#reporttype').html(data);

   

         }

   

     });
  }
</script>
<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classID').val();

   // alert(val);

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   
      if(classesID != 1)
      {
           $('#monthselect').hide();

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 
      }
      else
      {
        $('#monthselect').show();
         $.ajax({

   

                  url: base_url+'AjaxController/semester_getandYear',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              });
      }


   

   

    }

</script>

<!-- <script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script> -->

<script type="text/javascript">

   function Getclass(departmentID){

   

   // alert(departmentID);die;

                  $.ajax({

   

                  url: base_url+'AjaxController/class_get',

   

                  type: 'POST',

   

                  data:{departmentID:departmentID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#classID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>
<script type="text/javascript">
  
  function checkboxvalue(check,studentID)
  {
    
    
    if(check.checked==true)
    { 
      arlene=studentID;
    }
    var studentID=arlene;
    // alert(studentID);
    $('#sendmail').click(function()
      {
        
        $('#sendmailbyuser').click(function()
          {
            var subjectname=$('#subjectname').val();
            var writemail=$('#writemail').val();
            $.ajax({

   

                  url: base_url+'customemail/sendmailbyprofessor',

   

                  type: 'POST',

   

                  data:{subjectname:subjectname,writemail:writemail,studentID:studentID},

   

                  success: function(data)
                  { 

                    // console.log(data);
                    
                    location.reload(true);
                  }

            });
          });
        
        
      });       
  }   
</script>
<script type="text/javascript">
  function selectAllsendmail(){

      var items=document.getElementsByClassName('getstudentid');
// alert(items);
      for(var i=0; i<items.length; i++){

          if(items[i].type=='checkbox')

              items[i].checked=true;

      }

      

  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    var reporttype="<?php echo $this->input->get('reporttype') ?>";
    if(reporttype==1)
    {
      $('#monthselect').show();
    }
    else
    {
      $('#monthselect').hide();
    }
    
  });
</script>


