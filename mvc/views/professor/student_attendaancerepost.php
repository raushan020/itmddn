

<div class="">

   <div class="row page-titles">

      <div class="col-md-5 align-self-center">

         <h3 class="text-themecolor"><i class="fa fa-eye"></i> Student Attendance Report</h3>

      </div>

      <div class="col-md-7 align-self-center">

         <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">Student Attendance Report</li>

         </ol>

      </div>

   </div>

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card-body">

               <div class="col-sm-12">

                  <form style="" class="form-horizontal"  autocomplete="off" role="form">

                     <?php 

                        $getClasses = $this->input->get('course');

                        $getdepartment=$this->input->get('department');

                        

                        

                        $getSubcourse = $this->input->get('subcourse');

                        

                        

                        

                        $getSemester = $this->input->get('yearsOrSemester');

                        

                        

                        

                        $getSubject = $this->input->get('subject');

                        $year=$this->input->get('year');

                        $type = $this->input->get('type');

                        if($type){

                          $type = $this->input->get('type');

                        }else{

                          $type = "filter";

                        }

                        

                        

                        $date = $this->input->get('date');

                        

                        

                        

                                           ?>

                     <?php 

                        if($type=="filter") 

                        {

                         ?>

                     <input type="hidden" name="type" value="<?php echo $type ?>">

                     <div class="col-sm-3">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Department

                              </label>

                              <div class="">

                                 <?php

                                    $array = array("0" => "Select Department");

                                    

                                    foreach ($department as $department) {

                                    

                                        $array[$department->departmentID] = $department->department_name;

                                    

                                    }

                                    

                                    echo form_dropdown("department", $array, set_value("departmentID",$getdepartment), " onchange = 'Getclass($(this).val())' required class='form-control'");

                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    ?>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <!-- popup attendance -->
                     <div class="modal modal-box-2" id="viewprogress" hidden="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><img src="<?php echo base_url('uploads/default/loading.gif');?>" style="width: 40%;height: 40%">Loading....</div>
                     <div class="modal modal-box-2" id="viewattendance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                     </div>
                     <!--  -->

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Course

                              </label>

                              <div class="">

                                 <select id="classID" name="course" required class="form-control" onchange='GetsemesterAtd($(this).val())'>

                                  <option>Select</option>

                                    <?php  foreach ($classes as $classes)

                                    {

                                       if ($classes->classesID==$getClasses)

                                       {

                                       

                                           $selected =  "Selected";

                                       

                                       }

                                       else

                                       {

                                       

                                         $selected =  "";

                                       

                                       }

                                       

                                       ?>

                                    <option value="<?php echo $classes->classesID ?>" <?php echo $selected ?>><?php echo $classes->classes ?></option>

                                    <?php } ?>

                                 </select>

                              </div>

                              <!-- <div class="">

                                 <?php

                                    $array = array("0" => "Select Course");

                                    

                                    foreach ($classes as $classa) {

                                    

                                        $array[$classa->classesID] = $classa->classes;

                                    

                                    }

                                    

                                    // echo form_dropdown("course", $array, set_value("classesID",$getClasses), "id='classesID' onchange = 'GetsemesterAtd($(this).val())' required class='form-control'");

                                    

                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    

                                    

                                    

                                    ?>

                              </div> -->

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Semester/Year</label>

                              <div class="">

                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>

                                    <option>Select</option>

                                    <?php 

                                       if ($classesRow) {

                                       

                                        $looping    =  (int) $classesRow->duration;

                                       

                                       if ($classesRow->mode==1) {

                                       

                                         for ($i=1; $i <=$looping; $i++) {

                                       

                                                if (CallYears($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                       

                                        

                                       

                                       }

                                       

                                       }

                                       

                                       

                                       

                                       else{

                                       

                                         for ($i=1; $i <=(2*$looping); $i++) {

                                       

                                       if (CallSemester($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       

                                       }

                                       

                                       }

                                       

                                       }

                                       

                                       ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Subject</label>

                              <div class="">

                                 <select id="subjectID" name="subject" required class="form-control">
                                  <option>Select</option>
                                    <?php foreach ($subjects as $key => $value) {

                                       if ($value->subjectID==$getSubject) {

                                       

                                           $selected =  "Selected";

                                       

                                       }else{

                                       

                                         $selected =  "";

                                       

                                       }

                                 

                                       ?>

                                    <option value="<?php echo $value->subjectID ?>" <?php echo $selected ?>><?php echo $value->subject ?></option>

                                    <?php } ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Report</label>

                              <div class="">

                                 <select id="year" name="year" required class="form-control" required>
                                    <option>Select One</option>
                                    <option value="1" <?php if($year == 1){ echo "selected"; }  ?>>Week</option>
                                    <option value="2" <?php if($year == 2){ echo "selected"; }  ?>>This Month</option>
                                    <option value="3" <?php if($year == 3){ echo "selected"; }  ?>>Last 3 Month</option>
                                    <option value="4" <?php if($year == 4){ echo "selected"; }  ?>>Last 6 Month</option>
                                    <option value="5" <?php if($year == 5){ echo "selected"; }  ?>>Last Year</option>
                                  </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-1">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label"></label>

                              <div class="">

                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 15px;" name="Search">

                              </div>

                           </div>

                        </div>

                     </div>

                     <div class="clearfix"></div>

                     <?php } ?>
                     <!-- <div class="col-sm-2">
                      <div class="modal modal-box">
                        <a href="#modal-2" class="btn-modal" id="sendmail" data-toggle="modal" data-target="#modal-2">Send Mail</a>
                     </div>
                     </div> -->
                     <!-- Modal Start-->
                     <!-- <div class="modal modal-box-2 fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog">
                         <div class="modal-content" id="myModalLabel">
                           <div class="modal-header theme-bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                           </div>
                          <div class="modal-body">
                            <h3>Custom <span>Mail</span></h3> -->
                            <!-- <form name="sentMessage" class="contactForm"> -->
                              <!-- <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="subjectname" placeholder="Enter subject name" >
                                    <p class="help-block text-danger"></p>
                                  </div>
                                  <div class="form-group">
                                    <textarea class="form-control" id="writemail" placeholder="Write Your Message Here"></textarea>  
                                    <p class="help-block text-danger"></p>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                  <div id="success"></div>
                                  <button type="button" class="btn modal-btn" id="sendmailbyuser">Send</button>
                                </div>
                              </div> -->
                            <!-- </form> -->
                         <!--  </div>
                        </div>
                      </div>
                    </div> -->
                    <!-- MOdel End -->
                     <div class="col-sm-10" style="text-align:center; margin-top: 20px;">

                        <?php if($subject_name){  ?> 

                        <h4>Subject Name- <span><?php echo $subject_name->subject; ?></span></h4>

                        <?php } ?>

                     </div>

                     

                  </form>

                  <div class="col-sm-6">

                  </div>

                  <div class="col-sm-6">

                     <?php 

                        $usertype = $this->session->userdata("usertype");

                        

                        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "HOD"){

                        

                        ?>

                     <div class="pull-right">

                        <div class="btn-group">

                           <a href="<?php echo base_url('professor/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                        </div>

                     </div>

                     <?php  }  ?>

                  </div>

                  <div class="clearfix"></div>

                  <div class="table table-responsive">

                     <div class="col-sm-12">

                        <div id="hide-table">

                           <?php  if($getStudent){ ?>                 

                           <form method="post" autocomplete="off" onsubmit="return idsd()">

                              <input type="hidden" name="classesID" value="<?php echo $getClasses ?>">

                              <input type="hidden" name="sub_coursesID" value="0">

                              <input type="hidden" name="yearsOrSemester" value="<?php echo $getSemester ?>">

                              <input type="hidden" name="subjectID" id="subjectID1" value="<?php echo $getSubject ?>">

                              <input type="hidden" name="type" value="<?php echo $type ?>">

                              <input type="hidden" name="date" value="<?php echo $date ?>" id ="date_atd">

                              <div class="col-sm-4"></div>

                              <div class="col-sm-4">

                                 <div class="">

                                 </div>

                              </div>

                              <div class="col-sm-4"></div>

                              <input type="hidden" name="subjectID" value="<?php echo $_GET['subject'] ?>">

                              <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="professorexportattendancereport">

                                 <thead>
<!-- <input type="checkbox" name="" onclick="selectAllsendmail()"> -->
                                    <tr>
                                       <!-- <th>Check &nbsp;&nbsp;</th> -->
                                       <th ><?=$this->lang->line('slno')?></th>
                                       <th >Name</th>
                                       <th >Roll No.</th>

                                      <?php 
                                      if($dateshow==7)
                                      {
                                        $now = new DateTime( "7 days ago", new DateTimeZone('asia/kolkata'));
                                        $interval = new DateInterval( 'P1D'); // 1 Day interval
                                        $period = new DatePeriod( $now, $interval, 7); // 7 Days 
                                      }
                                      else if($dateshow==30)
                                      {
                                        $todaydate=date('d');
                                        $todaydate=$todaydate-1;
                                        $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
                                        $interval = new DateInterval( 'P1D'); // 1 Day interval
                                        $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
                                        
                                      }
                                      else if($dateshow==90)
                                      {
                                        $todaydate=date('d');
                                        $todaydate=$todaydate-1;
                                        $todaydate=$todaydate+60;
                                        $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
                                        $interval = new DateInterval( 'P1D'); // 1 Day interval
                                        $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
                                      }
                                      else if($dateshow==180)
                                      {
                                        $todaydate=date('d');
                                        $todaydate=$todaydate-1;
                                        $todaydate=$todaydate+150;
                                        $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
                                        $interval = new DateInterval( 'P1D'); // 1 Day interval
                                        $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
                                      }
                                      else
                                      {
                                        $year=date('Y');
                                        $lastyear=$year-1;
                                        $this->load->library('calendar');
                                        $days=0; 
                                        for($month=1;$month<=12;$month++)
                                        { 
                                          $totalday=$this->calendar->get_total_days($month,$lastyear);
                                          $days=$days+$totalday;
                                        }     
                                        $todaydate= $days;    
                                        $now = new DateTime( "$todaydate days ago", new DateTimeZone('asia/kolkata'));
                                        $interval = new DateInterval( 'P1D'); // 1 Day interval
                                        $period = new DatePeriod( $now, $interval, $todaydate); // 7 Days 
                                      }
                                      foreach( $period as $day)
                                      {
                                          $key = $day->format( 'M d y'); ?>
                                          <th><?=$key?></th>
                                <?php }
                                      // for($i=1;$i<=$dateshow;$i++)
                                      // { 
                                        ?>
                                          <!-- <th><?=$i?></th> -->
                                <?php
                                 // }
                                 ?>
                                       <!-- <th class="col-sm-2">Attendance Progress</th> -->
                                       <th>Total Attendance</th>
                                       <th>Total Present</th>
                                       <th>Total Absent</th>
                                    </tr>

                                 </thead>

                                 <tbody>

                                    <?php $pr=0; $ab=0; $ho=0;$prs=0;$abs=0;
                                    $count1=1;
                                       foreach ($getStudent as $key => $value) {

                                       

                                        ?>

                                    <tr>
                                       <!-- <td><input type="checkbox" name="getstudentid" class="getstudentid" id="getstudentid<?php echo $value['studentID'] ?>" value="<?php echo $value['studentID'] ?>" onclick="checkboxvalue(this,<?php echo $value['studentID'] ?>)"></td>  -->
                                       <td ><?php echo $count1; ?></td>
<!-- <?php echo $key+1; ?> -->
                                       <input type="hidden" name="studentID[]" value="<?php echo $value['studentID'] ?>">
                                       <td ><?php echo $value['name']; ?></td>
                                       <td ><?php echo $value['roll']; ?></td>
                                      
                                        <?php $a=count($value['object']);
                                        $count=0;
                                        // print_r($getdateweek);die;
                                        for($d=0;$d<count($getdateweek);$d++)
                                        {
                                          foreach ($value['object'] as $key)
                                          {
                                            $present=$key->atd;
                                            if($getdateweek[$d]==$key->atd_date)
                                            {
                                              if($present==1)
                                              { 
                                                $pr=1;
                                              }
                                              else
                                              { 
                                                if($present==0)
                                                { 
                                                  $ab=1;
                                                }
                                                else
                                                { 
                                                  $ho=1;
                                                } 
                                              }
                                            }                                            
                                          }
                                          if($pr==1)
                                          { $pr=0; $prs=$prs+1; ?>
                                            <td >P</td>
                                    <?php }
                                          else if($ab==1)
                                          { $ab=0; $abs=$abs+1; ?>
                                            <td >A</td>
                                    <?php }
                                          else if($ho==1)
                                          { $ho=0; ?>
                                            <td >H</td>
                                    <?php }
                                          else
                                          { ?>
                                            <td >-</td>
                                    <?php }
                                        }                                        
                                        ?>

                                       
                                       <td >
                                         <?php echo $prs+$abs; ?>
                                       </td>
                                       <td >                                    
                                        <?php echo $prs; $prs=0;?>
                                       </td>
                                       <td >
                                         <?php echo $abs; $abs=0;?>
                                       </td>


                                    </tr>

                                    <?php $count1++; } ?>

                                 </tbody>

                              </table>

                           </form>

                           <?php } else{ ?>

                           <div class="Noresuls">

                              <h1>Sorry we couldn't find any matches</h1>

                              <p>Maybe your search was too specific, please try searching with another term.</p>

                              <img src="https://www.itmddn.online/uploads/images/crying.png">

                           </div>

                           <?php } ?>

                        </div>

                     </div>

                  </div>

           <style type="text/css">
                .nav-tabs-custom {
    margin-bottom: 20px;
    background: #fff;
    
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
}
/*@media only screen and (min-width: 786px) {
  .nav-tabs-custom {
   overflow: auto;
  }
}*/

.nav-tabs-custom > .nav-tabs {
    background-color: #e5e5e5;
    margin: 0;
    border-bottom-color: #e5e5e5;

}

.nav-tabs-custom > .nav-tabs > li.active > a {
    border-top: 0;
    border-left-color: #09A3A3;
    border-right-color: #09A3A3;
}
.nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a {
    background-color: #e5e5e5;
    border: 0px;
    color: #000000db;
}
.nav-tabs-custom > .nav-tabs > li > a, .nav-tabs-custom > .nav-tabs > li > a:hover {
    background: transparent;
    margin: 0;
}
.nav-tabs-custom > .tab-content {
    background: #fff;
    padding: 10px;
}
.attendance_table tr th {
    text-align: center;
    border: 0.1px solid #ddd;
    padding: 4px 5px;
    color: #232a2f !important;
}
.attendance_table tr td {
    text-align: center;
    border: 0.1px solid #ddd;
    padding: 4px 5px;
    color: #232a2f !important;
}
.ecampus-bg-success {
    background: #5eb171!important;
    color: #232a2f !important;
}
.ecampus-bg-danger {
    background: #e8737e!important;
    color: #232a2f !important;
}
.ecampus-bg-info {
    background: #5fbfce!important;
    color: #232a2f !important;
}
.ecampus-bg-secondary {
    background: #8cb0d0!important;
    color: #232a2f !important;
}
.ecampus-bg-primary {
    background: #6a94c1!important;
    color: #232a2f !important;
}
.totalattendanceCount{
  text-align: center;
}
.wwww{
    font-weight: 600;
}
            </style>


                  <script type="text/javascript">

                     function idsd(){

                      var atd = $("#date_atd").val();

                      // var topicid=$("#topic").val();

                           var check = true;

                           $("input:radio").each(function(){

                               var name = $(this).attr("name");

                               if($("input:radio[name="+name+"]:checked").length == 0){



                                   check = false;

                                   $("#"+name).html('Please Select');

                                   

                               }

                           });

                           

                           if(check){

                               if(atd=='')

                               {

                                  alert('Please select attendance date.');

                                check = false;

                                // $('#myModal').modal('hide');

                               }



                               return check;

                           }else{

                                

                               if(check==false){

                               

                            

                            $('#myModalLabel').html("Please Check Attendance!!");

                                return check;

                               }

                             return check;

                           }

                      

                     

                     }

                  </script>

               </div>

               <!-- col-sm-12 -->

            </div>

            <!-- row -->

         </div>

         <!-- Body -->

      </div>

      <!-- /.box -->

   </div>

<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classID').val();

   // alert(val);

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function Getclass(departmentID){

   

   // alert(departmentID);die;

                  $.ajax({

   

                  url: base_url+'AjaxController/class_get',

   

                  type: 'POST',

   

                  data:{departmentID:departmentID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#classID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function OnchageDateAtd(date){

   var course =  "<?php echo $this->input->get('course') ?>";

   var subcourse =  "<?php echo $this->input->get('subcourse') ?>";

   var yearsOrSemester =  "<?php echo $this->input->get('yearsOrSemester') ?>";

   var subject =  "<?php echo $this->input->get('subject') ?>";

   var type =  "<?php echo $this->input->get('type') ?>";

   var url = "<?php echo base_url() ?>viewlecture/attendance?type="+type+"&course="+course+"&subcourse=0&yearsOrSemester="+yearsOrSemester+"&subject="+subject+"&date="+date;

   window.location.replace(url);

   

   }

   

</script>

<script>

   var status = '';

   

   

   

   var id = 0;

   

   

   

   $('.onoffswitch-small-checkbox').click(function() {

   

   

   

       if($(this).prop('checked')) {

   

   

   

           status = 'chacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       } else {

   

   

   

           status = 'unchacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       }

   

   

   

   

   

   

   

       if((status != '' || status != null) && (id !='')) {

   

   

   

           $.ajax({

   

   

   

               type: 'POST',

   

   

   

               url: "<?=base_url('professor/active')?>",

   

   

   

               data: "id=" + id + "&status=" + status,

   

   

   

               dataType: "html",

   

   

   

               success: function(data) {

   

   

   

                   if(data == 'Success') {

   

   

   

                       toastr["success"]("Success")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   } else {

   

   

   

                       toastr["error"]("Error")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   }

   

   

   

               }

   

   

   

           });

   

   

   

       }

   

   

   

   }); 

   

   

   

</script>





<!-- <script>

// Get the modal

var modal = document.getElementById("myModal");



// Get the button that opens the modal

var btn = document.getElementById("myBtn");



// Get the <span> element that closes the modal

var span = document.getElementsByClassName("close");



// When the user clicks the button, open the modal 

btn.onclick = function() {

  modal.style.display = "block";

}



// When the user clicks on <span> (x), close the modal

span.onclick = function() {

  modal.style.display = "none";

}



// When the user clicks anywhere outside of the modal, close it

window.onclick = function(event) {

  if (event.target == modal) {

    modal.style.display = "none";

  }

}

</script> -->

<!-- <script type="text/javascript">

  $('#myBtn').click(function()

    {

       var subjectid=$('#subjectID').val();

       // alert(subjectid);

       



       if($('.present').is(":checked"))

       {

           $.ajax({

   

                  url: base_url+'AjaxController/getcompletunitbysubject',

   

                  type: 'POST',

   

                  data:{subjectid:subjectid},

   

                  success: function(data)

                  {

                    if(data)

                    {

                      $('#myModal').html(data);

                    }

                    

   

                  }

   

              }); 

       }

       else

       {

        

          var data="Please take Attendance!!";

          alert(data);

          location.reload();

      

       }

    });

</script> -->

<script type="text/javascript">

  function selectAllsemester(){



      var items=document.getElementsByClassName('present');

      for(var i=0; i<items.length; i++){

          if(items[i].type=='radio')

              items[i].checked=true;

      }

      

  }

  

  function UnSelectAll(){

      var items=document.getElementsByClassName('radio1');

      for(var i=0; i<items.length; i++){

          if(items[i].type=='radio')

              items[i].checked=false;

      }

  }

</script>
<script type="text/javascript">
  
  function checkboxvalue(check,studentID)
  {
    
    
    if(check.checked==true)
    { 
      arlene=studentID;
    }
    var studentID=arlene;
    // alert(studentID);
    $('#sendmail').click(function()
      {
        
        $('#sendmailbyuser').click(function()
          {
            var subjectname=$('#subjectname').val();
            var writemail=$('#writemail').val();
            $.ajax({

   

                  url: base_url+'customemail/sendmailbyprofessor',

   

                  type: 'POST',

   

                  data:{subjectname:subjectname,writemail:writemail,studentID:studentID},

   

                  success: function(data)
                  { 

                    // console.log(data);
                    
                    location.reload(true);
                  }

            });
          });
        
        
      });       
  }   
</script>
<script type="text/javascript">
  function selectAllsendmail(){

      var items=document.getElementsByClassName('getstudentid');
// alert(items);
      for(var i=0; i<items.length; i++){

          if(items[i].type=='checkbox')

              items[i].checked=true;

      }

      

  }
</script>
<!-- <script type="text/javascript">
  function lookUp(studentID)
  {
    var subject="<?php echo $this->input->get('subject') ?>";
    var year="<?php echo $this->input->get('year') ?>";
    $.ajax({
            url: base_url+'AjaxController/attendanceview_byprofessor',
            type: 'POST',
            data:{yearsOrSemester:yearsOrSemester,subject:subject,year:year,studentID:studentID},
            success: function(data)
            { 
              $("#viewattendance").html(data);            
            }

          });
  }
</script> -->


