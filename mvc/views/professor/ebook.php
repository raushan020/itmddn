
<style>
  .modal.modal-box{
    text-align: right!important;
  }
</style>
<div class="">

   <div class="row page-titles">

      <div class="col-md-5 align-self-center">

         <h3 class="text-themecolor"><i class="fa fa-book"></i> View Ebook </h3>

      </div>

      <div class="col-md-7 align-self-center">

         <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">View Ebook</li>

         </ol>

      </div>

   </div>

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card-body">

               <div class="col-sm-12">

                  <form style="" class="form-horizontal"  autocomplete="off" role="form">

                     <?php 

                        $getClasses = $this->input->get('course');

                        $getdepartment=$this->input->get('department');

                        

                        

                        $getSubcourse = $this->input->get('subcourse');

                        

                        

                        

                        $getSemester = $this->input->get('yearsOrSemester');

                        

                        

                        

                        $getSubject = $this->input->get('subject');

                        $year=$this->input->get('year');

                        $type = $this->input->get('type');

                        if($type){

                          $type = $this->input->get('type');

                        }else{

                          $type = "filter";

                        }

                        

                        

                        $date = $this->input->get('date');

                        

                        

                        

                                           ?>

                     <?php 

                        if($type=="filter") 

                        {

                         ?>

                     <input type="hidden" name="type" value="<?php echo $type ?>">

                     <div class="col-sm-3">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Department

                              </label>

                              <div class="">

                                 <?php

                                    $array = array("0" => "Select Department");

                                    

                                    foreach ($department as $department) {

                                    

                                        $array[$department->departmentID] = $department->department_name;

                                    

                                    }

                                    

                                    echo form_dropdown("department", $array, set_value("departmentID",$getdepartment), " onchange = 'Getclass($(this).val())' required class='form-control'");

                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    ?>

                              </div>
                              <script type="text/javascript">
                              function Getclass(departmentID)
                              {
                                $.ajax({
                                  url: base_url+'AjaxController/class_get',
                                  type: 'POST',
                                  data:{departmentID:departmentID},
                                  success: function(data)
                                  {
                                    console.log(data);
                                    $('#classID').html(data);
                                  }
                                }); 
                              }
                              </script>
                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <!-- popup attendance -->
                     <!-- <div class="modal modal-box-2" id="viewprogress" hidden="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><img src="<?php echo base_url('uploads/default/loading.gif');?>" style="width: 40%;height: 40%">Loading....</div> -->
                     <div class="modal modal-box-2" id="viewattendance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                     </div>
                     <!--  -->

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Course

                              </label>

                              <div class="">

                                 <select id="classID" name="course" required class="form-control" onchange='GetsemesterAtd($(this).val())'>

                                  <option>Select</option>

                                    <?php  foreach ($classes as $classes)

                                    {

                                       if ($classes->classesID==$getClasses)

                                       {

                                       

                                           $selected =  "Selected";

                                       

                                       }

                                       else

                                       {

                                       

                                         $selected =  "";

                                       

                                       }

                                       

                                       ?>

                                    <option value="<?php echo $classes->classesID ?>" <?php echo $selected ?>><?php echo $classes->classes ?></option>

                                    <?php } ?>

                                 </select>

                              </div>

                              <!-- <div class="">

                                 <?php

                                    $array = array("0" => "Select Course");

                                    

                                    foreach ($classes as $classa) {

                                    

                                        $array[$classa->classesID] = $classa->classes;

                                    

                                    }

                                    

                                    // echo form_dropdown("course", $array, set_value("classesID",$getClasses), "id='classesID' onchange = 'GetsemesterAtd($(this).val())' required class='form-control'");

                                    

                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    

                                    

                                    

                                    ?>

                              </div> -->

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Semester/Year</label>

                              <div class="">

                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>

                                    <option>Select</option>

                                    <?php 

                                       if ($classesRow) {

                                       

                                        $looping    =  (int) $classesRow->duration;

                                       

                                       if ($classesRow->mode==1) {

                                       

                                         for ($i=1; $i <=$looping; $i++) {

                                       

                                                if (CallYears($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                       

                                        

                                       

                                       }

                                       

                                       }

                                       

                                       

                                       

                                       else{

                                       

                                         for ($i=1; $i <=(2*$looping); $i++) {

                                       

                                       if (CallSemester($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       

                                       }

                                       

                                       }

                                       

                                       }

                                       

                                       ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-1">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label"></label>

                              <div class="">

                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 25px;" name="Search">

                              </div>

                           </div>

                        </div>

                     </div>

                     <div class="clearfix"></div>

                     <?php } ?>
                   </form>

                  <div class="col-sm-6">

                  </div>

                  <div class="col-sm-6">

                     <?php 

                        $usertype = $this->session->userdata("usertype");

                        

                        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "HOD"){

                        

                        ?>

                     <?php  }  ?>

                  </div>

                  <div class="clearfix"></div>
                  <div class="table-wrapper">

                  <div class="table table-responsive">

                     <div class="col-sm-12">

                        <div id="hide-table">

                                          

                           <form method="post" autocomplete="off" onsubmit="return idsd()">

                              <input type="hidden" name="classesID" value="<?php echo $getClasses ?>">

                              <input type="hidden" name="sub_coursesID" value="0">

                              <input type="hidden" name="yearsOrSemester" value="<?php echo $getSemester ?>">

                              <input type="hidden" name="subjectID" id="subjectID1" value="<?php echo $getSubject ?>">

                              <input type="hidden" name="type" value="<?php echo $type ?>">

                              <input type="hidden" name="date" value="<?php echo $date ?>" id ="date_atd">

                              <div class="col-sm-4"></div>

                              <div class="col-sm-4">

                                 <div class="">

                                 </div>

                              </div>

                              <div class="col-sm-4"></div>

                              <!-- <input type="hidden" name="subjectID" value="<?php echo $_GET['subject'] ?>"> -->

                              <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="professorebookdownload">

                                 <thead>
<!-- <input type="checkbox" name="" onclick="selectAllsendmail()"> -->
                                    <tr>
                                       <th class="col-sm-2">Sl_No</th>

                                       <th class="col-sm-2">Subject</th>

                                       <th class="col-sm-2">Ebook Name</th>

                                       <th class="col-sm-2">View</th>

                                    </tr>

                                 </thead>

                                 <tbody>

                                    <?php
                                    $count1=1; 
                                       foreach ($getStudent as $key => $value) {
                                        // print_r($value);
                                        // exit();
                                        ?>
                                        <?php 
                                      // $pdf=file_exists('uploads/pdf/'.$value->subjectID.".pdf");
                                      if($value->subjectID)
                                      { ?>
                                    <tr>                                      
                                       <td class="col-sm-2"><?php echo $count1; ?></td>
                                       <td class="col-sm-2"><?php echo $value->subject; ?></td>

                                       <td class="col-sm-2"><?php echo $value->subjectID.".pdf"; ?></td>

                                      

                                       <td class="col-sm-2">
                                         <?php
                                       if($value->pdf!=''){
                                        ?>
                                            <a href="<?php echo base_url('uploads/pdf/'.$value->pdf) ?>" target="_blank" class="btn btn-success view btn-xs mrg for_margR" title="view PDF" style="float: left;"><i class="fa fa-eye"></i></a> 
                                          <?php } else { ?>  
                                            <a href="<?php echo base_url('uploads/pdf/'.$value->subjectID.".pdf") ?>" target="_blank" class="btn btn-success view btn-xs mrg for_margR" title="view PDF" style="float: left;"><i class="fa fa-eye"></i></a>                                                                     
                                          <?php } ?>   
                                          <a href="<?php echo base_url('uploads/pdf/'.$value->subjectID.".pdf") ?>" class="btn btn-success view btn-xs mrg for_margR" download title="Download" style="float: left;"><i class="fa fa-download"></i></a>                                          

                                       </td>
                                    
                                       
                                       

                                    </tr>
                                    <?php $count1++;  } ?>
                                    <?php  } ?>

                                 </tbody>

                              </table>

                           </form>

                           

                        </div>

                     </div>

                  </div>
                  </div>

           <style type="text/css">
                .nav-tabs-custom {
    margin-bottom: 20px;
    background: #fff;
    
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
}
/*@media only screen and (min-width: 786px) {
  .nav-tabs-custom {
   overflow: auto;
  }
}*/

.nav-tabs-custom > .nav-tabs {
    background-color: #e5e5e5;
    margin: 0;
    border-bottom-color: #e5e5e5;

}

.nav-tabs-custom > .nav-tabs > li.active > a {
    border-top: 0;
    border-left-color: #09A3A3;
    border-right-color: #09A3A3;
}
.nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a {
    background-color: #e5e5e5;
    border: 0px;
    color: #000000db;
}
.nav-tabs-custom > .nav-tabs > li > a, .nav-tabs-custom > .nav-tabs > li > a:hover {
    background: transparent;
    margin: 0;
}
.nav-tabs-custom > .tab-content {
    background: #fff;
    padding: 10px;
}
.attendance_table tr th {
    text-align: center;
    border: 0.1px solid #ddd;
    padding: 4px 5px;
    color: #232a2f !important;
}
.attendance_table tr td {
    text-align: center;
    border: 0.1px solid #ddd;
    padding: 4px 5px;
    color: #232a2f !important;
}
.ecampus-bg-success {
    background: #5eb171!important;
    color: #232a2f !important;
}

.ecampus-bg-danger {
    background: #e8737e!important;
    color: #232a2f !important;
}
.ecampus-bg-info {
    background: #5fbfce!important;
    color: #232a2f !important;
}
.ecampus-bg-secondary {
    background: #8cb0d0!important;
    color: #232a2f !important;
}
.ecampus-bg-primary {
    background: #6a94c1!important;
    color: #232a2f !important;
}
.totalattendanceCount{
  text-align: center;
}
.wwww{
    font-weight: 600;
}
            </style>


                  <script type="text/javascript">

                     function idsd(){

                      var atd = $("#date_atd").val();

                      // var topicid=$("#topic").val();

                           var check = true;

                           $("input:radio").each(function(){

                               var name = $(this).attr("name");

                               if($("input:radio[name="+name+"]:checked").length == 0){



                                   check = false;

                                   $("#"+name).html('Please Select');

                                   

                               }

                           });

                           

                           if(check){

                               if(atd=='')

                               {

                                  alert('Please select attendance date.');

                                check = false;

                                // $('#myModal').modal('hide');

                               }



                               return check;

                           }else{

                                

                               if(check==false){

                               

                            

                            $('#myModalLabel').html("Please Check Attendance!!");

                                return check;

                               }

                             return check;

                           }

                      

                     

                     }

                  </script>

               </div>

               <!-- col-sm-12 -->

            </div>

            <!-- row -->

         </div>

         <!-- Body -->

      </div>

      <!-- /.box -->

   </div>

<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classID').val();

   // alert(val);

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>



<script type="text/javascript">

   function OnchageDateAtd(date){

   var course =  "<?php echo $this->input->get('course') ?>";

   var subcourse =  "<?php echo $this->input->get('subcourse') ?>";

   var yearsOrSemester =  "<?php echo $this->input->get('yearsOrSemester') ?>";

   var subject =  "<?php echo $this->input->get('subject') ?>";

   var type =  "<?php echo $this->input->get('type') ?>";

   var url = "<?php echo base_url() ?>viewlecture/attendance?type="+type+"&course="+course+"&subcourse=0&yearsOrSemester="+yearsOrSemester+"&subject="+subject+"&date="+date;

   window.location.replace(url);

   

   }

   

</script>

<script>

   var status = '';

   

   

   

   var id = 0;

   

   

   

   $('.onoffswitch-small-checkbox').click(function() {

   

   

   

       if($(this).prop('checked')) {

   

   

   

           status = 'chacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       } else {

   

   

   

           status = 'unchacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       }

   

   

   

   

   

   

   

       if((status != '' || status != null) && (id !='')) {

   

   

   

           $.ajax({

   

   

   

               type: 'POST',

   

   

   

               url: "<?=base_url('professor/active')?>",

   

   

   

               data: "id=" + id + "&status=" + status,

   

   

   

               dataType: "html",

   

   

   

               success: function(data) {

   

   

   

                   if(data == 'Success') {

   

   

   

                       toastr["success"]("Success")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   } else {

   

   

   

                       toastr["error"]("Error")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   }

   

   

   

               }

   

   

   

           });

   

   

   

       }

   

   

   

   }); 

   

   

   

</script>

<script type="text/javascript">

  function selectAllsemester(){



      var items=document.getElementsByClassName('present');

      for(var i=0; i<items.length; i++){

          if(items[i].type=='radio')

              items[i].checked=true;

      }

      

  }

  

  function UnSelectAll(){

      var items=document.getElementsByClassName('radio1');

      for(var i=0; i<items.length; i++){

          if(items[i].type=='radio')

              items[i].checked=false;

      }

  }

</script>
<script type="text/javascript">
  
  function checkboxvalue(check,studentID)
  {
    
    
    if(check.checked==true)
    { 
      arlene=studentID;
    }
    var studentID=arlene;
    // alert(studentID);
    $('#sendmail').click(function()
      {
        
        $('#sendmailbyuser').click(function()
          {
            var subjectname=$('#subjectname').val();
            var writemail=$('#writemail').val();
            $.ajax({

   

                  url: base_url+'customemail/sendmailbyprofessor',

   

                  type: 'POST',

   

                  data:{subjectname:subjectname,writemail:writemail,studentID:studentID},

   

                  success: function(data)
                  { 

                    // console.log(data);
                    
                    location.reload(true);
                  }

            });
          });
        
        
      });       
  }   
</script>


