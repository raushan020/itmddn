
<style>
  .modal.modal-box{
    text-align: right!important;
  }
</style>
<div class="">

   <div class="row page-titles">

      <div class="col-md-5 align-self-center">

         <h3 class="text-themecolor"><i class="fa fa-eye"></i> View Attendance </h3>

      </div>

      <div class="col-md-7 align-self-center">

         <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">View Attendance</li>

         </ol>

      </div>

   </div>

   <!-- form start -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card-body">

               <div class="col-sm-12">

                  <form style="" class="form-horizontal"  autocomplete="off" role="form">

                     <?php 

                        $getClasses = $this->input->get('course');

                        $getdepartment=$this->input->get('department');

                        

                        

                        $getSubcourse = $this->input->get('subcourse');

                        

                        

                        

                        $getSemester = $this->input->get('yearsOrSemester');

                        

                        

                        

                        $getSubject = $this->input->get('subject');

                        $year=$this->input->get('year');

                        $type = $this->input->get('type');

                        if($type){

                          $type = $this->input->get('type');

                        }else{

                          $type = "filter";

                        }

                        

                        

                        $date = $this->input->get('date');

                        

                        

                        

                                           ?>

                     <?php 

                        if($type=="filter") 

                        {

                         ?>

                     <input type="hidden" name="type" value="<?php echo $type ?>">

                     <div class="col-sm-3">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Department

                              </label>

                              <div class="">

                                 <?php

                                    $array = array("0" => "Select Department");


                                    foreach ($department as $department) {

                                    

                                        $array[$department->departmentID] = $department->department_name;

                                    

                                    }

                                    

                                    echo form_dropdown("department", $array, set_value("departmentID",$getdepartment), " onchange = 'Getclass($(this).val())' required class='form-control'");

                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    ?>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <!-- popup attendance -->
                     <!-- <div class="modal modal-box-2" id="viewprogress" hidden="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><img src="<?php echo base_url('uploads/default/loading.gif');?>" style="width: 40%;height: 40%">Loading....</div> -->
                     <div class="modal modal-box-2" id="viewattendance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                     </div>
                     <!--  -->

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">

                              Course

                              </label>

                              <div class="">

                                 <select id="classID" name="course" required class="form-control" onchange='GetsemesterAtd($(this).val())'>

                                  <option>Select</option>

                                    <?php  foreach ($classes as $classes)

                                    {

                                       if ($classes->classesID==$getClasses)

                                       {

                                       

                                           $selected =  "Selected";

                                       

                                       }

                                       else

                                       {

                                       

                                         $selected =  "";

                                       

                                       }

                                       

                                       ?>

                                    <option value="<?php echo $classes->classesID ?>" <?php echo $selected ?>><?php echo $classes->classes ?></option>

                                    <?php } ?>

                                 </select>

                              </div>

                              <!-- <div class="">

                                 <?php

                                    $array = array("0" => "Select Course");

                                    

                                    foreach ($classes as $classa) {

                                    

                                        $array[$classa->classesID] = $classa->classes;

                                    

                                    }

                                    

                                    // echo form_dropdown("course", $array, set_value("classesID",$getClasses), "id='classesID' onchange = 'GetsemesterAtd($(this).val())' required class='form-control'");

                                    

                                    //onchange='ajaxGet_subCourses($(this).val())'

                                    

                                    

                                    

                                    ?>

                              </div> -->

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                      

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Semester/Year</label>

                              <div class="">

                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>

                                    <option>Select</option>

                                    <?php 

                                       if ($classesRow) {

                                       

                                        $looping    =  (int) $classesRow->duration;

                                       

                                       if ($classesRow->mode==1) {

                                       

                                         for ($i=1; $i <=$looping; $i++) {

                                       

                                                if (CallYears($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                       

                                        

                                       

                                       }

                                       

                                       }

                                       

                                       

                                       

                                       else{

                                       

                                         for ($i=1; $i <=(2*$looping); $i++) {

                                       

                                       if (CallSemester($i)==$getSemester) {

                                       

                                           $select = 'Selected';

                                       

                                        }else{

                                       

                                        $select = '';

                                       

                                        }

                                       

                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       

                                       }

                                       

                                       }

                                       

                                       }

                                       

                                       ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Subject</label>

                              <div class="">

                                 <select id="subjectID" name="subject" required class="form-control">

                                    <?php  foreach ($subjects as $key => $value) {

                                       if ($value->subjectID==$getSubject) {                                       

                                           $selected =  "Selected";                                       

                                       }else{                                      

                                         $selected =  "";                                       

                                       }                                       

                                       ?>

                                    <option value="<?php echo $value->subjectID ?>" <?php echo $selected ?>><?php echo $value->subject ?></option>

                                    <?php } ?>

                                 </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-2">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label">Academic Session</label>

                              <div class="">

                                 <select id="year" name="year" required class="form-control">
                                    <option>Select Session</option>                                    
                                    <?php foreach ($academicyears as $key => $value)
                                    { ?>
                                      <option value="<?php echo $value->academic_session ?>" <?php if($value->academic_session == $this->session->userdata('sessionyear')){ echo "selected"; }  ?>><?php echo $value->academic_session ?></option>
                                   <?php } ?>
                                  </select>

                              </div>

                              <div class="clearfix"></div>

                           </div>

                        </div>

                     </div>

                     <div class="col-sm-1">

                        <div class="">

                           <div class="form-group">

                              <label for="classesID" class="control-label"></label>

                              <div class="">

                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 25px;" name="Search">

                              </div>

                           </div>

                        </div>

                     </div>

                     <div class="clearfix"></div>

                     
                     <?php } ?>
                     
                     
                     <div class="col-md-10" style="text-align:center;">

                        <?php if($subject_name){  ?> 

                        <h4>Subject Name- <span><?php echo $subject_name->subject; ?></span></h4>

                        <?php } ?>

                     </div>
                     <!-- <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>viewlecture/multipleAction"> -->
                     <div class="col-md-2">
                     <!--  <?php 
                        if($_GET){
                      ?> -->
                                           <!-- <?php   
                        }else{
                      ?> -->
                      <!-- <a name="sendmailbyadmin" disabled class="btn btn-success" >Send Notice</a> -->
                      <!-- <?php    
                        }
                      ?> -->
                       
                        
                     
                     </div>
                     

                     

                  </form>
                  <!-- Modal Start-->
                     <div class="modal modal-box-2 fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog" style="margin-top: 55px;">
                         <div class="modal-content" id="myModalLabel">
                           <div class="modal-header theme-bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                           </div>
                          <div class="modal-body">
                            <h3>Custom <span>Mail</span></h3>
                            <form id="fileinfo" enctype="multipart/form-data" action="<?php echo base_url('customemail/sendmailbyprofessor') ?>" method="post" name="fileinfo">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="hidden" id="checked_id" name="checked_id[]" value="">
                                    <label for="date" class="col-sm-4 control-label">
                                    Notice Date
                                    </label>
                                    <div class="col-sm-6">
                                      <input type="" class="form-control datepicker_quiz_data" disabled="disabled" id="date1" name="date1" placeholder="Date" value="<?php echo date('d-m-Y') ?>" >
                                      <input type="hidden" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?php echo date('d-m-Y') ?>" >
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="title" class="col-sm-4 control-label">
                                    Notice Title
                                    </label>
                                    <div class="col-sm-6">
                                       <input type="text" class="form-control" placeholder="Enter your  title here" autocomplete="off" name="title" id="title">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="notice_type" class="col-sm-4 control-label">
                                    Notice Type
                                    </label>
                                    <div class="col-sm-6">
                                        <select name="notice_type" class="form-control" required="required">
                                          <option value="">--- Select Notice Type ---</option>
                                           <option value="Class Lecture">Class Lecture</option>
                                           <option value="Lecture Notes">Lecture Notes</option>
                                           <option value="Assignment">Assignment</option>
                                           <option value="Monthly Academic Activity">Monthly Academic Activity</option>
                                           <option value="Semester Activity">Semester Activity</option>
                                           <option value="Others">Others</option>
                                         </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="notice" class="col-sm-4 control-label">
                                    Description
                                    </label>
                                    <div class="col-sm-8 col-md-6">
                                    <textarea class="form-control noticeeditor" id="editor" placeholder="Desription" name="notice" ></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="document" class="col-sm-4 control-label">
                                    Upload Document
                                    </label>
                                    <div class="col-sm-6">
                                    <input type="file" name="userfile" id="userfile">
                                    <label style="color: red">Max file size: 10 MB. Only PDF, DOC & PPT supported</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                  <div id="success"></div>
                                  <button type="submit" class="btn modal-btn" id="sendmailbyuser">Send</button>
                                </div>
                                <input type="hidden" name="type" value="<?php echo $this->input->get('type') ?>">
                                <input type="hidden" name="department" value="<?php echo $this->input->get('department') ?>">
                                <input type="hidden" name="course" value="<?php echo $this->input->get('course') ?>">
                                <input type="hidden" name="yearsOrSemester" value="<?php echo $this->input->get('yearsOrSemester') ?>">
                                <input type="hidden" name="subject" value="<?php echo $this->input->get('subject') ?>">
                                <input type="hidden" name="year" value="<?php echo $this->input->get('year') ?>">
                                <input type="hidden" name="submits" value="<?php echo $this->input->get('submit') ?>">
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- MOdel End -->

                  <div class="col-sm-6">

                  </div>

                  <div class="col-sm-6">

                     <?php 

                        $usertype = $this->session->userdata("usertype");

                        

                        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "HOD"){

                        

                        ?>

                     <div class="pull-right">

                        <div class="btn-group">

                           <a href="<?php echo base_url('professor/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>

                        </div>

                     </div>

                     <?php  }  ?>

                  </div>

                  <div class="clearfix"></div>
                  <div class="table-wrapper">

                  <div class="table table-responsive">

                     <div class="col-sm-12">

                        <div id="hide-table">

                           <?php  if($getStudent){ ?>                 

                           <form method="post" autocomplete="off" onsubmit="return idsd()">

                              <input type="hidden" name="classesID" value="<?php echo $getClasses ?>">

                              <input type="hidden" name="sub_coursesID" value="0">

                              <input type="hidden" name="yearsOrSemester" value="<?php echo $getSemester ?>">

                              <input type="hidden" name="subjectID" id="subjectID1" value="<?php echo $getSubject ?>">

                              <input type="hidden" name="type" value="<?php echo $type ?>">

                              <input type="hidden" name="date" value="<?php echo $date ?>" id ="date_atd">

                              <div class="col-sm-4"></div>

                              <div class="col-sm-4">

                                 <div class="">

                                 </div>

                              </div>

                              <div class="col-sm-4"></div>

                              <input type="hidden" name="subjectID" value="<?php echo $_GET['subject'] ?>">
  <div class="admail" style="margin-bottom:10px">            
 <a href="#modal-2" name="sendmailbyadmin" class="btn btn-success etsfilertButton_attend disabled" id="sendmail" style="float:none;margin-left:0px" data-toggle="modal" data-target="#modal-2">Send Notice</a>
</div>
                              <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="professorexportattendance">

                                 <thead>
<!-- <input type="checkbox" name="" onclick="selectAllsendmail()"> -->
                                    <tr>
                                       <th>Check &nbsp;&nbsp;</th>
                                       <th class="col-sm-2">Sl_No</th>

                                       <th class="col-sm-2">Roll No.</th>

                                       <th class="col-sm-2">Name</th>

                                       <th class="col-sm-2">Attendance Progress</th>

                                       <th class="col-sm-2">View</th>

                                    </tr>

                                 </thead>

                                 <tbody>

                                    <?php
                                    $count1=1;
                                       foreach ($getStudent as $key => $value) {

                                       

                                        ?>

                                    <tr>
                                       <td><input type="checkbox" name="getstudentid" class="getstudentid mycheckbox_attend" id="getstudentid<?php echo $value['studentID'] ?>" value="<?php echo $value['studentID'] ?>" onclick="checkboxvalue(this,<?php echo $value['studentID'] ?>)"></td>
                                       <td class="col-sm-2"><?php echo $count1; ?></td>
<!-- <?php echo $key+1; ?> -->
                                       <input type="hidden" name="studentID[]" value="<?php echo $value['studentID'] ?>">

                                       <td class="col-sm-2"><?php echo $value['roll']; ?></td>

                                       <td class="col-sm-2"><?php echo $value['name']; ?></td>

                                       <td class="col-sm-2">

                                        <?php $a=count($value['object']);

                                        // print_r($a);die;

                                        $count=0;

                                        foreach ($value['object'] as $key )

                                        {

                                          // $b=array();



                                          $present1=0;

                                            $present=$key->atd;

                                          

                                            if($present==1)

                                            {

                                              $count=$count+1;

                                            }

                                        }

                                        // print_r($count);

                                       

                                        if($count >=1)

                                        {

                                           $avg=($count*100)/$a;

                                           $unitavg=round($avg,0);

                                          ?>

                                          <div class="progress">



                                            <?php if($unitavg >= 0 && $unitavg <= 50)

                                            {?>

                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $unitavg.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);background-color: red;">

                                           <?php }

                                           else if($unitavg >= 51 && $unitavg <= 75)

                                            {?>

                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $unitavg.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);background-color: yellow;">

                                            <?php }

                                           else if($unitavg >= 76)

                                            {?>

                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $unitavg.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);background-color: green;">

                                            <?php } ?>

                                          </div>

                                          <div class="progress-value" style=" position: absolute;padding: 3px 0px 0px 65px; color: #000;"><?php echo $unitavg.'%';?></div>

                                          </div>   

                                          <span style="color:red;font-weight: bold;"><?php echo $count."/".$a;?>(Total Attendance)</span>

                                            

                                       <?php }

                                       else



                                         {?>



                                           <div class="progress">



                                              <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:0%; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);">                 



                                              </div>



                                              <div class="progress-value" style=" position: absolute;padding: 3px 0px 0px 65px;">0%</div>



                                           </div>

                                        <?php } ?>

                                       </td>

                                       <td class="col-sm-2">
                                          <a href="javascript:void(0)" onclick="lookUp(<?php echo $value['studentID'] ?>)" data-toggle="modal" data-target="#viewattendance" class="btn btn-success view btn-xs mrg for_margR" title="view Attendance" style="float: left;"><i class="fa fa-eye"></i></a> 

                                       </td>

                                    </tr>

                                    <?php $count1++; } ?>

                                 </tbody>

                              </table>

                           </form>

                           <?php } else{ ?>

                           <div class="Noresuls">

                              <h1>Sorry we couldn't find any matches</h1>

                              <p>Maybe your search was too specific, please try searching with another term.</p>

                              <img src="https://www.itmddn.online/uploads/images/crying.png">

                           </div>

                           <?php } ?>

                        </div>

                     </div>

                  </div>
                  </div>

           <style type="text/css">
                .nav-tabs-custom {
    margin-bottom: 20px;
    background: #fff;
    
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
}
/*@media only screen and (min-width: 786px) {
  .nav-tabs-custom {
   overflow: auto;
  }
}*/

.nav-tabs-custom > .nav-tabs {
    background-color: #e5e5e5;
    margin: 0;
    border-bottom-color: #e5e5e5;

}

.nav-tabs-custom > .nav-tabs > li.active > a {
    border-top: 0;
    border-left-color: #09A3A3;
    border-right-color: #09A3A3;
}
.nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a {
    background-color: #e5e5e5;
    border: 0px;
    color: #000000db;
}
.nav-tabs-custom > .nav-tabs > li > a, .nav-tabs-custom > .nav-tabs > li > a:hover {
    background: transparent;
    margin: 0;
}
.nav-tabs-custom > .tab-content {
    background: #fff;
    padding: 10px;
}
.attendance_table tr th {
    text-align: center;
    border: 0.1px solid #ddd;
    padding: 4px 5px;
    color: #232a2f !important;
}
.attendance_table tr td {
    text-align: center;
    border: 0.1px solid #ddd;
    padding: 4px 5px;
    color: #232a2f !important;
}
.ecampus-bg-success {
    background: #5eb171!important;
    color: #232a2f !important;
}

.ecampus-bg-danger {
    background: #e8737e!important;
    color: #232a2f !important;
}
.ecampus-bg-info {
    background: #5fbfce!important;
    color: #232a2f !important;
}
.ecampus-bg-secondary {
    background: #8cb0d0!important;
    color: #232a2f !important;
}
.ecampus-bg-primary {
    background: #6a94c1!important;
    color: #232a2f !important;
}
.totalattendanceCount{
  text-align: center;
}
.wwww{
    font-weight: 600;
}
            </style>


                  <script type="text/javascript">

                     function idsd(){

                      var atd = $("#date_atd").val();

                      // var topicid=$("#topic").val();

                           var check = true;

                           $("input:radio").each(function(){

                               var name = $(this).attr("name");

                               if($("input:radio[name="+name+"]:checked").length == 0){



                                   check = false;

                                   $("#"+name).html('Please Select');

                                   

                               }

                           });

                           

                           if(check){

                               if(atd=='')

                               {

                                  alert('Please select attendance date.');

                                check = false;

                                // $('#myModal').modal('hide');

                               }



                               return check;

                           }else{

                                

                               if(check==false){

                               

                            

                            $('#myModalLabel').html("Please Check Attendance!!");

                                return check;

                               }

                             return check;

                           }

                      

                     

                     }

                  </script>

               </div>

               <!-- col-sm-12 -->

            </div>

            <!-- row -->

         </div>

         <!-- Body -->

      </div>

      <!-- /.box -->

   </div>

<script type="text/javascript">

   function GetsubjectAtd(val){

    var classesID =  $('#classID').val();

         $.ajax({

   

         url: base_url+'AjaxController/subject_get',

   

         type: 'POST',

   

          data:{val:val,classesID:classesID},

   

         success: function(data){

   

         $('#subjectID').html(data);

   

         }

   

     }); 

   

   

   

   }

   

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function GetsemesterAtd(classesID){

   

   

                  $.ajax({

   

                  url: base_url+'AjaxController/semester_get',

   

                  type: 'POST',

   

                  data:{classesID:classesID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#yearSemesterID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function Getclass(departmentID){

   

   // alert(departmentID);die;

                  $.ajax({

   

                  url: base_url+'AjaxController/class_get',

   

                  type: 'POST',

   

                  data:{departmentID:departmentID},

   

                  success: function(data){

   

                    console.log(data);

   

                  $('#classID').html(data);

   

                  }

   

              }); 

   

   

    }

</script>

<script type="text/javascript">

   function OnchageDateAtd(date){

   var course =  "<?php echo $this->input->get('course') ?>";

   var subcourse =  "<?php echo $this->input->get('subcourse') ?>";

   var yearsOrSemester =  "<?php echo $this->input->get('yearsOrSemester') ?>";

   var subject =  "<?php echo $this->input->get('subject') ?>";

   var type =  "<?php echo $this->input->get('type') ?>";

   var url = "<?php echo base_url() ?>viewlecture/attendance?type="+type+"&course="+course+"&subcourse=0&yearsOrSemester="+yearsOrSemester+"&subject="+subject+"&date="+date;

   window.location.replace(url);

   

   }

   

</script>

<script>

   var status = '';

   

   

   

   var id = 0;

   

   

   

   $('.onoffswitch-small-checkbox').click(function() {

   

   

   

       if($(this).prop('checked')) {

   

   

   

           status = 'chacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       } else {

   

   

   

           status = 'unchacked';

   

   

   

           id = $(this).parent().attr("id");

   

   

   

       }

   

   

   

   

   

   

   

       if((status != '' || status != null) && (id !='')) {

   

   

   

           $.ajax({

   

   

   

               type: 'POST',

   

   

   

               url: "<?=base_url('professor/active')?>",

   

   

   

               data: "id=" + id + "&status=" + status,

   

   

   

               dataType: "html",

   

   

   

               success: function(data) {

   

   

   

                   if(data == 'Success') {

   

   

   

                       toastr["success"]("Success")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   } else {

   

   

   

                       toastr["error"]("Error")

   

   

   

                       toastr.options = {

   

   

   

                         "closeButton": true,

   

   

   

                         "debug": false,

   

   

   

                         "newestOnTop": false,

   

   

   

                         "progressBar": false,

   

   

   

                         "positionClass": "toast-top-right",

   

   

   

                         "preventDuplicates": false,

   

   

   

                         "onclick": null,

   

   

   

                         "showDuration": "500",

   

   

   

                         "hideDuration": "500",

   

   

   

                         "timeOut": "5000",

   

   

   

                         "extendedTimeOut": "1000",

   

   

   

                         "showEasing": "swing",

   

   

   

                         "hideEasing": "linear",

   

   

   

                         "showMethod": "fadeIn",

   

   

   

                         "hideMethod": "fadeOut"

   

   

   

                       }

   

   

   

                   }

   

   

   

               }

   

   

   

           });

   

   

   

       }

   

   

   

   }); 

   

   

   

</script>





<!-- <script>

// Get the modal

var modal = document.getElementById("myModal");



// Get the button that opens the modal

var btn = document.getElementById("myBtn");



// Get the <span> element that closes the modal

var span = document.getElementsByClassName("close");



// When the user clicks the button, open the modal 

btn.onclick = function() {

  modal.style.display = "block";

}



// When the user clicks on <span> (x), close the modal

span.onclick = function() {

  modal.style.display = "none";

}



// When the user clicks anywhere outside of the modal, close it

window.onclick = function(event) {

  if (event.target == modal) {

    modal.style.display = "none";

  }

}

</script> -->

<!-- <script type="text/javascript">

  $('#myBtn').click(function()

    {

       var subjectid=$('#subjectID').val();

       // alert(subjectid);

       



       if($('.present').is(":checked"))

       {

           $.ajax({

   

                  url: base_url+'AjaxController/getcompletunitbysubject',

   

                  type: 'POST',

   

                  data:{subjectid:subjectid},

   

                  success: function(data)

                  {

                    if(data)

                    {

                      $('#myModal').html(data);

                    }

                    

   

                  }

   

              }); 

       }

       else

       {

        

          var data="Please take Attendance!!";

          alert(data);

          location.reload();

      

       }

    });

</script> -->

<script type="text/javascript">

  function selectAllsemester(){



      var items=document.getElementsByClassName('present');

      for(var i=0; i<items.length; i++){

          if(items[i].type=='radio')

              items[i].checked=true;

      }

      

  }

  

  function UnSelectAll(){

      var items=document.getElementsByClassName('radio1');

      for(var i=0; i<items.length; i++){

          if(items[i].type=='radio')

              items[i].checked=false;

      }

  }

</script>
<script type="text/javascript">
  function selectAllsendmail(){

      var items=document.getElementsByClassName('getstudentid');
// alert(items);
      for(var i=0; i<items.length; i++){

          if(items[i].type=='checkbox')

              items[i].checked=true;

      }

      

  }
</script>
<script type="text/javascript">
  function lookUp(studentID)
  {
    // $("#viewprogress").show();
    var yearsOrSemester="<?php echo $this->input->get('yearsOrSemester') ?>";
    var subject="<?php echo $this->input->get('subject') ?>";
    var year="<?php echo $this->input->get('year') ?>";
    $.ajax({
            url: base_url+'AjaxController/attendanceview_byprofessor',
            type: 'POST',
            data:{yearsOrSemester:yearsOrSemester,subject:subject,year:year,studentID:studentID},
            success: function(data)
            { 
              // $("#viewprogress").hide();
              $("#viewattendance").html(data);
              
              
            }

          });
  }
</script>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script>
    initSample();
</script>
<script type="text/javascript">
  
  function checkboxvalue(check,studentID)
  {
      $('#sendmail').click(function()
      {  

          var g=($('.getstudentid:checked').map(function() {
              return this.value;
          }).get().join(', '));
          $('#checked_id').val(g);

        });
          
  }   
</script>

<script type="text/javascript">
 
  
  $(document).ready(function(){
  if($(":checkbox").is(':checked') == true){
    $('#sendmail').show();
  }
});
</script>