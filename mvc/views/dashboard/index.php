<style type="text/css">
   .card.simple-card .cardheader {
   background-size: cover!important;
   position: relative;
   height: 135px;
   }
   .left{
   text-align: left;
   margin-left: 18px;
   }
   h1 { 
    text-align: center; 
} 

.b_center{
  text-align: center;
}
/*.table-responsive{
   overflow: inherit !important;
}*/

.modall {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 100; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-contentt {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.closee {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.closee:hover,
.closee:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
<script type="text/javascript" src="<?php echo base_url('assets/piechart/chart.js');?>"></script> 

<div class="row page-titles dashboardtitle-background">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-dashboard"></i> Dashboard</h3>
   </div>
   <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
         <li class="breadcrumb-item active">Dashboard</li>
      </ol>
   </div>
</div>
<div class="container-fluid">
   <!-- /row 1-->
   <?php
      $usertype = $this->session->userdata("usertype");
      if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "HOD") {
      if($usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support") {
      ?>
      <div class="col-md-12 col-sm-12">
                     <div class="widget list-widget" style="border: 2px solid #007bff;">
                        <div class="row mrg-0">
                           <div class="todo-list todo-list-hover todo-list-divided">
                              <div class="todo todo-default" href="#">
                    

                                <span class="badge badge-contributer bage-primary" style="background: #636337;
    color: #fff;"><span>Plan :</span> 

                                 
                           
                               <!-- <span> Basic</span> -->
                        
                           
                              <!-- <span> Essential</span> -->
                          <span> Pro</span>
                         

 </span>
                                <span class="badge badge-contributer bage-primary" style="background: #0fb76b;
    color: #fff;"><span>Activated On :</span> 01-07-2020
</span>
                                <span class="badge badge-contributer bage-success" style="background: #e20b0b;
    color: #fff;"><span>Expiring On:</span> :30-06-2021
</span>
    <span class="badge badge-contributer bage-primary" style="background: #0fb76b;
    color: #fff;"><span>Type :</span> Annual</span>
    <span class="badge badge-contributer" style="color: #000;background: ##007bff"><span>Status:</span> Active</span>

                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

<!-- <div id="forallmodal" class="modall">
     <div class="modal-contentt">
       <span class="closee">&times;</span>
        <div class="modal-header" style="background: #3c8dbc;">
          <h4 style="color: #fff;text-align: center;font-weight: 500;font-family: sans-serif;">REVISED Examination Notification</h4>
        </div>
         <div class="modal-body">
            <p><i class="fa fa-hand-o-right"></i> The general instructions to the students have already been pushed to their dashboard.</p>
               <p><i class="fa fa-hand-o-right"></i> A copy of the same will be mailed to all faculty members/admin.</p>
            <p><i class="fa fa-hand-o-right"></i> 2 New Tabs of 'Exam' and 'Result' have been added to faculty/ admin panel.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> You may check your exam papers, questions by selecting the course and semester filters. Everything will already be uploaded & you can just cross-check.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> Exam Setting. Various exam settings are managed from here. </p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Result></strong> You may check individual student result or download result in bulk after selecting appropriate filter and clicking on 'Download Excel'.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Webcam & Microphone Disabled:</strong> Due to slow internet across mobile and wifi network reported by many students, we've disabled webcam & microphone as it requires fast internet connection.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Lockdown Browser Functionality Relaxation:</strong> Few features like exam suspension upon receiving a call on mobile, browser tab minimize, etc will no longer suspend the exam.</p>
        </div>
       
     </div>
   </div> -->
<script>
var modal = document.getElementById("forallmodal");
var span = document.getElementsByClassName("closee")[0];
window.onload = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
   <div class="row">
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa fa-user"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back" id="mydivstu">
                           <?php
                              
                              ?>
                           <h3 class="cl-info"> <?=$student->counter?></h3>
                           <span>Total Students</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('professor')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption danger">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-id-badge"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail teacher-back" id="mydivtea">
                           <h3 class="cl-danger"> <?=count($professors_count)?></h3>
                           <span>Total Professors</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back" id="mydivco">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-3 col-sm-6">
         <a href="<?php echo base_url() ?>user/index">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption success">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-users"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail user-back" id="mydivuser">
                           <h3 class="cl-success"><?php echo $Totaluser ?></h3>
                           <span>Total Users</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-success">
                           <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <?php } ?>

   <?php if($usertype == "Admin") { ?>

 <!-- <div id="foradminmodal" class="modall">
     <div class="modal-contentt">
       <span class="closee">&times;</span>
        <div class="modal-header" style="background: #3c8dbc;">
          <h4 style="color: #fff;text-align: center;font-weight: 500;font-family: sans-serif;">REVISED Examination Notification</h4>
        </div>
         <div class="modal-body">
            <p><i class="fa fa-hand-o-right"></i> The general instructions to the students have already been pushed to their dashboard.</p>
               <p><i class="fa fa-hand-o-right"></i> A copy of the same will be mailed to all faculty members/admin.</p>
            <p><i class="fa fa-hand-o-right"></i> 2 New Tabs of 'Exam' and 'Result' have been added to faculty/ admin panel.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> You may check your exam papers, questions by selecting the course and semester filters. Everything will already be uploaded & you can just cross-check.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> Exam Setting. Various exam settings are managed from here. </p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Result></strong> You may check individual student result or download result in bulk after selecting appropriate filter and clicking on 'Download Excel'.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Webcam & Microphone Disabled:</strong> Due to slow internet across mobile and wifi network reported by many students, we've disabled webcam & microphone as it requires fast internet connection.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Lockdown Browser Functionality Relaxation:</strong> Few features like exam suspension upon receiving a call on mobile, browser tab minimize, etc will no longer suspend the exam.</p>
        </div>
       
     </div>
   </div> -->
<script>
var modal = document.getElementById("foradminmodal");
var span = document.getElementsByClassName("closee")[0];
window.onload = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>   
   <div class="row">
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa fa-user"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back" id="mydivstu">
                           <h3 class="cl-info"> <?=$student->counter?></h3>
                           <span>Total Students</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('professor')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption danger">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-id-badge"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail teacher-back" id="mydivtea">
                           <h3 class="cl-danger"> <?=count($professors_count)?></h3>
                           <span>Total Professors</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-danger">
                           <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back" id="mydivco">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      
   </div>
<?php } ?>

   <?php if($usertype == "superadmin" || $usertype == "ClgAdmin") { ?>
   <div class="row">
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption" >
                  <div class="col-xs-4 no-pad zoom" >
                     <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                  
                        <span>
                        </span>
                        <h3 style="color: #9c1855;"><i class="fa fa-inr" aria-hidden="true"></i> <?php
                           echo number_format($totalPayment->TotalAmount);
                           ?></h3>
                        <span>Total Revenue</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%; background: #9c1855;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption primary">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-money" aria-hidden="true" style="background: #007bff;color: #ffffff"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail paymentpaid-back">
                        <span></span>
                        <h3 style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($totalPayment->Totalpaidamount); ?></h3>
                        <span>Total Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%; background: #007bff;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption success">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-warning" style="background: #434348;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail totalPayment-back">
                        <span></span>
                        <h3 style="color: #434348"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($totalPayment->TotalAmount-$totalPayment->Totalpaidamount); ?></h3>
                        <span>Due Amount</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%; background: #434348" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php } ?>

   <?php if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "HOD") { ?>
   <?php if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin") { ?>
   <div class="row page-titles shortcut-menu">
      <h3>
      Shortcut Menu <i class="fa fa-share-square-o font-icon-shortcut"></i></h3 style="box-shadow: 10px;">
    </div>
   <div class="row">
      <div class="col-md-2 col-sm-6 fpaid">
         <a id = 'ppaidfilter' href="<?=base_url("student/add")?>">
            <div class="button">
               <div class="row">
                  <div class="widget-caption b_center">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail addnotice-back">
                           <span><i class="fa fa-plus"></i> Add Student</span>
                        </div>
                     </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-danger">
                        <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
                     </div>
                  </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-2 col-sm-6 fpaid">
      
      <a id = 'ppaidfilter' href="<?=base_url("subject/add")?>">
      
      <div class="button">
      
      <div class="row">
      <div class="widget-caption b_center">
      <div class="col-xs-12 no-pad">
        <div class="widget-detail details-menu adduser-back">
          <span><i class="fa fa-plus"></i> Add Subject</span>
        </div>
      </div>

      <div class="col-xs-12">
      
        <div class="widget-line bg-danger">
      
          <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
      
        </div>
      
      </div>
      
       <div class="clearfix"></div>
      
      </div>
      
      </div>
      
      </div>
      
      </a>
      
      </div>
      
      
      
      <div class="col-md-2 col-sm-6 fpaid">
      
      <a id = 'ppaidfilter' href="<?=base_url("exam/add")?>">
      
      <div class="button">
      
      <div class="row">
      
      <div class="widget-caption b_center">
      
      <div class="col-xs-12 no-pad" >
      
        <div class="widget-detail details-menu addcourse-back">
      
          <span><i class="fa fa-plus"></i> Add Exam</span>
      
        </div>
      
      </div>
      <div class="col-xs-12">
      
        <div class="widget-line bg-danger">
      
          <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
      
        </div>
      
      </div>
      
      </div>
      
      </div>
      
      </div>
      
      </a>
      
      </div>
      
      <div class="col-md-2 col-sm-6 fpaid">
      
      <a id = 'ppaidfilter' href="<?=base_url("classes/add")?>">
      
      <div class="button">
      
      <div class="row">
      
      <div class="widget-caption b_center">
      
      <div class="col-xs-12 no-pad">
      
        <div class="widget-detail details-menu addpdf-back">
      
          <span><i class="fa fa-plus"></i> Add Course</span>
      
        </div>
      
      </div>
      <div class="col-xs-12">
      
        <div class="widget-line bg-danger">
      
          <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
      
        </div>
      
      </div>
      
      </div>
      
      </div>
      

      </div>
      
      </a>
      
      </div>

      <div class="col-md-2 col-sm-6 fpaid">
      
      <a id = 'ppaidfilter' href="<?=base_url("notice/add")?>">
      
      <div class="button">
      
      <div class="row">
      
      <div class="widget-caption b_center">
      
      <div class="col-xs-12 no-pad">
      
        <div class="widget-detail addnotice-back">
      
          <span><i class="fa fa-plus"></i> Add Notice</span>
      
        </div>
      
      </div>
      <div class="col-xs-12">
      
        <div class="widget-line bg-danger">
      
          <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
      
        </div>
      
      </div>
      
      </div>
      
      </div>
      
      </div>
      
      </a>
      
      </div>

      <div class="col-md-2 col-sm-6 fpaid">
      
      <a id = 'ppaidfilter' href="<?=base_url("professor/add")?>">
      
      <div class="button">
      
      <div class="row">
      
      <div class="widget-caption b_center">
      
      <div class="col-xs-12 no-pad">
      
        <div class="widget-detail addnotice-back">
      
          <span><i class="fa fa-plus"></i> Add Professor </span>
      
        </div>
      
      </div>
      
      <div class="col-xs-12">
      
        <div class="widget-line bg-danger">
      
          <span style="width:82%;" class="bg-danger widget-horigental-line"></span>
      
        </div>
      
      </div>
      
      </div>
      
      </div>
      
      </div>
      
      </a>
      
      </div>
      
      </div>
      <hr>
   </div>

<?php if($usertype == "superadmin" || $usertype == "ClgAdmin") { ?>
   <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="card simple-card" style="height: 370px;">
         <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
         </div>
         <div class="avatar">
            <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
         </div>
         <div class="info1">
            <div class="title">
               <h3><?php
                  $name = $this->session->userdata('name');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                    if($usertype=="ClgAdmin")
                    {
                     ?>
                                    <h5>Super Admin</h5>

                               <?php
                    }
                    else
                    {
                     ?>
                                    <h5><?php echo $usertype; ?></h5>

                               <?php
                    }
                    
                  } else {
                    echo $name;
                    if($usertype=="ClgAdmin")
                    {
                     ?>
                                    <h5>Super Admin</h5>

                               <?php
                    }
                    else
                    {
                     ?>
                                    <h5><?php echo $usertype; ?></h5>

                               <?php
                    }
                  }
                  
                  ?></h3>
            </div>
         </div>
         <div class="bottom admin_dash">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $name = $this->session->userdata('username');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                  } else {
                    echo $name;
                  }
                  
                  ?></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-envelope text-maroon-light"></i> Communication E-mail</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $email = $this->session->userdata('email');
                   echo $email; ?>&nbsp;&nbsp;<a href="#" style="color: red;" data-toggle="modal" data-target="#superadminemailmodal"><i class="fa fa-edit" ></i></a></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-phone text-maroon-light"></i> Phone</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"> <?php echo $phone = $this->session->userdata('phone'); ?>&nbsp;&nbsp;<a href="#" style="color: red;" data-toggle="modal" data-target="#superadminphonemodal"><i class="fa fa-edit" ></i></a></p>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>

<?php if($usertype == "Admin") { ?>
   <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="card simple-card" style="height: 370px;">
         <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
         </div>
         <div class="avatar">
            <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
         </div>
         <div class="info1">
            <div class="title">
               <h3><?php
                  $name = $this->session->userdata('name');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                    if($usertype=="ClgAdmin")
                    {
                     ?>
                                    <h5>Super Admin</h5>

                               <?php
                    }
                    else
                    {
                     ?>
                                    <h5><?php echo $usertype; ?></h5>

                               <?php
                    }
                    
                  } else {
                    echo $name;
                    if($usertype=="ClgAdmin")
                    {
                     ?>
                                    <h5>Super Admin</h5>

                               <?php
                    }
                    else
                    {
                     ?>
                                    <h5><?php echo $usertype; ?></h5>

                               <?php
                    }
                  }
                  
                  ?></h3>
            </div>
         </div>
         <div class="bottom admin_dash">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $name = $this->session->userdata('username');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                  } else {
                    echo $name;
                  }
                  
                  ?></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-envelope text-maroon-light"></i> Communication E-mail</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"><?php
               $loginuserID=$this->session->userdata('loginuserID');
                  $email = $this->session->userdata('email');
                   echo $email; ?>&nbsp;&nbsp;
                  <?php if($this->session->userdata('verifyemail') != 0)
                  { ?>
                     <span><i class="fa fa-check" title="verify now" style="color: green"></i></span>
         <?php   }
                  else
                  { ?>
                     <div class="verify"></div>
                  
                   
                        <span><button class="verifyemailw" onclick="verifyemailbyadmin(<?php echo $loginuserID ?>)" style="color: red;float: right;margin-top: -35px;"><i class="fa fa-warning" title="verify now"></i></button></span></p>
               <?php   } ?>                 
                        
                        
                     
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-phone text-maroon-light"></i> Phone</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"> <?php echo $phone = $this->session->userdata('phone'); ?>&nbsp;&nbsp;
               <?php if($this->session->userdata('verifymobileno') != 0)
               { ?>
                  <span style="color: green;"><i class="fa fa-check" title="verify now"></i>
               </span>
           <?php    }
               else
               { ?>
                  <span>
                  <a href="#" onclick="sendotp()" style="color: red;"data-toggle="modal" data-target="#verifymob"><i class="fa fa-warning" title="verify now"></i></a>
               </span></p>
           <?php    } ?>
               
            
            </div>
         </div>
      </div>
   </div>
   <?php } ?> 
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="table-wrapper" style="height: 370px;">
               <div class="table-title theme-bg">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2 style="margin-bottom: 10px;">Recent Notice</h2>
                     </div>
                  </div>
               </div>
               <div class="table-responsive">
                  <table class="table table-striped col-md-12">
                     <thead>
                        <tr>
                           <td style="font-weight: 600;">S.No.</td>
                           <td style="font-weight: 600;">Date</td>
                           <td style="font-weight: 600;">Title</td>
                           <td style="font-weight: 600;">Sender</td>                           
                           <td style="font-weight: 600;">Action</td>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if($notices)  {$i = 1; 
                           foreach($notices  as  $notices) { ?>
                        <tr>
                           <td><?php echo $i; ?></td>
                           <td><?php echo date('d-m-Y', strtotime($notices['date']  )); ?></td>
                           <td><?php echo substr($notices['title'], 0,20); ?></td>
                           <?php if($notices['usertype']=="Admin")
                           { ?>
                            <td><?php echo $notices['name']; ?></td> 
                          <?php }
                          else
                          { ?>
                            <td><?php echo $notices['professorname']; ?></td> 
                        <?php } ?>
                                                     
                           <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                        </tr>
                        <?php $i++; }}
                        else
                        { ?>
                          <tr><td colspan="5" style="text-align: center;color: red">No Recent Notice</td></tr>
                      <?php } ?>
                     </tbody>
                  </table>
                  <div class="box-main-footer clearfix">
                     <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php } ?>
<?php if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "HOD") { ?>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="table-wrapper">
               <div class="table-title">
                  <div class="row">
                     <!-- <div class="col-md-5 col-sm-5 col-xs-5">
                        <h4 style="margin-bottom: 10px;color:black;text-align: center;">All Professor Progress</h4>
                        <div id="chart_divbyadmin"></div>
                     </div> -->
                     <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="row">
                        <div class="col-md-9 col-sm-9 col-xs-9">
                        <h4 style="margin-bottom: 10px;color:black;text-align: center;">Month Wise Attendance Data</h4>
                        </div>
                        <?php (int)$date=date('m');?>
                        <div class="col-md-3">
                        <select name="year" class="form-control" id="year">
                           <option value="0" <?php if($date==0){ echo "selected";}?>>Select Year</option>
                            <option value="1" <?php if($date==1){ echo "selected";}?>>January</option>
                            <option value="2" <?php if($date==2){ echo "selected";}?>>February</option>
                            <option value="3" <?php if($date==3){ echo "selected";}?>>March</option>
                            <option value="4" <?php if($date==4){ echo "selected";}?>>April</option>
                            <option value="5" <?php if($date==5){ echo "selected";}?>>May</option>
                            <option value="6" <?php if($date==6){ echo "selected";}?>>June</option>
                            <option value="7" <?php if($date==7){ echo "selected";}?>>July</option>
                            <option value="8" <?php if($date==8){ echo "selected";}?>>August</option>
                            <option value="9" <?php if($date==9){ echo "selected";}?>>September</option>
                            <option value="10" <?php if($date==10){ echo "selected";}?>>October</option>
                            <option value="11" <?php if($date==11){ echo "selected";}?>>November</option>
                            <option value="12" <?php if($date==12){ echo "selected";}?>>December</option>
                        </select>
                        </div>
                      </div>
                        <div id="chart_area" style="width: 950px; height: 380px;"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
<?php } ?>
    <!-- <script type="text/javascript"> 
    google.charts.load('current', {'packages':['corechart']}); 
    google.charts.setOnLoadCallback(drawChart); 
       
    function drawChart() { 
      var jsonData = $.ajax({ 
          url: "<?php echo base_url() . 'dashboard/pie_chart_js_byadmin' ?>", 
          dataType: "json", 
          async: false 
          }).responseText;  
      console.log(jsonData);
      var data = new google.visualization.DataTable(jsonData); 
      var options = {
                        title:"All Professor Progress",
                        is3D: true,
                        width: 500,
                        height: 410,
                        tooltip: { text:'value',isHtml: true,trigger:'selection'},
                        legend: {position: 'right'},
                        slices: {
                                    0: { color: 'Red' },
                                    1: { color: 'FFAA1D' },
                                    2: { color: 'darkGreen' }
                                }

                     };
      var chart = new google.visualization.PieChart(document.getElementById('chart_divbyadmin')); 
      chart.draw(data,options); 
    } 
 
    </script> -->
    <script type="text/javascript">
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback();

function load_monthwise_data(year, title)
{
    var month=year;
    if(month==1)
    {
      month="January";
    }
    else if(month==2)
    {
      month="February";
    }
    else if(month==3)
    {
      month="March";
    }
    else if(month==4)
    {
      month="April";
    }
    else if(month==5)
    {
      month="May";
    }
    else if(month==6)
    {
      month="June";
    }
    else if(month==7)
    {
      month="July";
    }
    else if(month==8)
    {
      month="August";
    }
    else if(month==9)
    {
      month="September";
    }
    else if(month==10)
    {
      month="October";
    }
    else if(month==11)
    {
      month="November";
    }
    else
    {
      month="December";
    }
    
    var temp_title = title + ' '+month+'';
    $.ajax({
        url:"<?php echo base_url() . 'dashboard/bar_chart_all_student_attendance'?>",
        method:"POST",
        data:{year:year},
        dataType:"JSON",
        success:function(data)
        {
            drawMonthwiseChart(data, temp_title);
        }
    });
}

function drawMonthwiseChart(chart_data, chart_main_title)
{
    var jsonData = chart_data;
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Course');
    data.addColumn('number', 'Attendance');
    $.each(jsonData, function(i, jsonData){
        var month = jsonData.month;
        var profit = parseFloat($.trim(jsonData.profit));
        data.addRows([[month, profit]]);
    });
    var options = {
        title:chart_main_title,
        hAxis: {
            title: "Course"
        },
        vAxis: {
            title: 'Attendance'
        }
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart_area'));
    chart.draw(data, options);
}

</script>
<script>
    
$(document).ready(function(){

    $('#year').change(function(){
        var year = $(this).val();
        if(year != '')
        {
            load_monthwise_data(year, 'Month Wise Attendance Data For');
        }
    });

     var year = $('#year').val();
        if(year != '')
        {
            load_monthwise_data(year, 'Month Wise Attendance Data For');
        }

});

</script>
    <?php } ?>
   <?php
      $usertype = $this->session->userdata("usertype");
      $loginuserID=$this->session->userdata('loginuserID');
      if($usertype == "Accountant") {
      ?>

<!-- <div id="foraccountantmodal" class="modall">
     <div class="modal-contentt">
       <span class="closee">&times;</span>
        <div class="modal-header" style="background: #3c8dbc;">
          <h4 style="color: #fff;text-align: center;font-weight: 500;font-family: sans-serif;">REVISED Examination Notification</h4>
        </div>
         <div class="modal-body">
            <p><i class="fa fa-hand-o-right"></i> The general instructions to the students have already been pushed to their dashboard.</p>
               <p><i class="fa fa-hand-o-right"></i> A copy of the same will be mailed to all faculty members/admin.</p>
            <p><i class="fa fa-hand-o-right"></i> 2 New Tabs of 'Exam' and 'Result' have been added to faculty/ admin panel.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> You may check your exam papers, questions by selecting the course and semester filters. Everything will already be uploaded & you can just cross-check.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> Exam Setting. Various exam settings are managed from here. </p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Result></strong> You may check individual student result or download result in bulk after selecting appropriate filter and clicking on 'Download Excel'.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Webcam & Microphone Disabled:</strong> Due to slow internet across mobile and wifi network reported by many students, we've disabled webcam & microphone as it requires fast internet connection.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Lockdown Browser Functionality Relaxation:</strong> Few features like exam suspension upon receiving a call on mobile, browser tab minimize, etc will no longer suspend the exam.</p>
        </div>
       
     </div>
   </div> -->
<script>
var modal = document.getElementById("foraccountantmodal");
var span = document.getElementsByClassName("closee")[0];
window.onload = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>


   <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="card simple-card" style="height: 362px;">
         <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
         </div>
         <div class="avatar">
            <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
         </div>
         <div class="info1">
            <div class="title">
               <h3><?php
                  $name = $this->session->userdata('name');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                    ?>
                                    <h6><?php echo $usertype; ?></h6>

                               <?php
                  } else {
                    echo $name;
                    ?>
                                    <h6><?php echo $usertype; ?></h6>

                               <?php
                  }
                  
                  ?></h3>
            </div>
         </div>
         <div class="bottom admin_dash">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $name = $this->session->userdata('username');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                  } else {
                    echo $name;
                  }
                  
                  ?></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-envelope text-maroon-light"></i> E-mail</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $email = $this->session->userdata('email');
                  echo $email;
                  ?>
                   <?php if($this->session->userdata('verifyemail') != 0)
                  { ?>
                     <i class="fa fa-check" title="verify now" style="color: green"></i>
                        
               <?php   }
               else
               { ?>
                  <div class="verify"></div>
                  <span><button class="verifyemailw" onclick="verifyemailbyadmin(<?php echo $loginuserID ?>)" style="color: red;float: right;margin-top: -35px;"><i class="fa fa-warning" title="verify now"></i></button></span></p>
             <?php  }?>        
                        
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
               <p class="left"><?=$user->phone?>
               <?php if($this->session->userdata('verifyemail') != 0)
               { ?>
                  <span>
                  <a href="#" style="color: green;"><i class="fa fa-check" title="verify now"></i></a>
               </span>
         <?php      }
               else
               { ?>
                  <span>
                  <a href="#" onclick="sendotp()" style="color: red;"data-toggle="modal" data-target="#verifymob"><i class="fa fa-warning" title="verify now"></i></a>
                  </span></p>
            <?php   }
               ?>
               
            
            </div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="table-wrapper" style="height: 360px;">
               <div class="table-title theme-bg">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2 style="margin-bottom: 10px;">Notice</h2>
                     </div>
                  </div>
               </div>
               <div class="table-responsive">
                  <table class="table table-striped col-md-12">
                     <thead>
                        <tr>
                           <td style="font-weight: 600;">S.No.</td>
                           <td style="font-weight: 600;">Title.</td>
                           <td style="font-weight: 600;">Date</td>
                           <td style="font-weight: 600;">Action</td>
                        </tr>
                     </thead>
                     <tbody>
                        <?php  {$i = 1; 
                           foreach( $notices  as  $notices) { ?>
                        <tr>
                           <td><?php echo $i; ?></td>
                           <td><?php echo substr($notices['title'], 0,20); ?></td>
                           <td><?php echo date('d-m-Y', strtotime($notices['date'])); ?></td>
                           <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                        </tr>
                        <?php $i++; }} ?>
                     </tbody>
                  </table>
               </div>
               <div class="box-main-footer clearfix">
                  <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php }?>
   <?php if($usertype == "superadmin" || $usertype == "ClgAdmin") { ?>
   <!-- <div class="row">
   </div> -->
   <div class="row">
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="pichartPaymentStatus" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="piecharthichart" style="height:400px"></div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="row">
      <div class="col-xs-12" style="margin-top:3%;">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="container"></div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
<?php } ?>
   <div class="row" style="margin-top:3%;">
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="professor_gender" style="height: 400px"></div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-body">
               <div id="student_gender" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
   <script type="text/javascript">
      $.ajax({
      
      
      
         type: 'GET',
      
      
      
         dataType: "json",
      
      
      
         url: "<?=base_url('dashboard/graphcall')?>",
      
      
      
         dataType: "html",
      
      
      
         success: function(data) {
      
      var response = jQuery.parseJSON(data);
      
      Highcharts.chart('container', {
      chart: {
      type: 'cylinder',
      options3d: {
      enabled: true,
      alpha: 15,
      beta: 15,
      depth: 50,
      viewDistance: 25
      }
      },
      title: {
      text: 'Earning Graph'
      },
      plotOptions: {
      series: {
      depth: 25,
      colorByPoint: true
      }
      },
      
      xAxis: {
      
       categories: ['2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026']
      
      },
      
      
      series: [{
      data: response.balance,
      name: 'Payment',
      showInLegend: false
      }]
      });
      
      
      }
      
      }); 
      
      $('#plain').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: false,
      
         polar: false
      
       },
      
       subtitle: {
      
         text: 'Plain'
      
       }
      
      });
      
      });
      
      
      
      $('#inverted').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: true,
      
         polar: false
      
       },
      
       subtitle: {
      
         text: 'Inverted'
      
       }
      
      });
      
      });
      
      
      
      $('#polar').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: false,
      
         polar: true
      
       },
      
       subtitle: {
      
         text: 'Polar'
      
       }
      
      });
      
      });
      
      
      
   </script>  
   <script type="text/javascript">
      $.ajax({
      
          type: 'GET',
      
          dataType: "json",
      
      
      
          url: "<?=base_url('dashboard/paymentscall')?>",
      
      
      
          dataType: "html",
      
      
      
          success: function(data) {
      
      
      
            var response = jQuery.parseJSON(data);
      
      
      
      
      
            npaid = response.npaid;
      
      
      
            ppaid = response.ppaid;
      
      
      
            fpaid = response.fpaid;
      
      
      
            cash = response.cash;
      
      
      
            cheque = response.cheque;
      
      
      
            paypal = response.paypal;
      
      
      
            stripe = response.stripe;
      
      
      
            PayuMoney = response.PayuMoney;
      
      
      
            OnlinePayment = response.OnlinePayment;
      
      
      
            Bank = response.Bank;
      
      Highcharts.chart('pichartPaymentStatus', {
      
      chart: {
      
      plotBackgroundColor: null,
      
      plotBorderWidth: null,
      
      plotShadow: false,
      
      type: 'pie'
      
      },
      
      title: {
      
      text: 'Payment Status Graph'
      
      },
      
      tooltip: {
      
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      
      },
      
      plotOptions: {
      
      pie: {
      
        allowPointSelect: true,
      
        cursor: 'pointer',
      
        dataLabels: {
      
          enabled: true,
      
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
      
          style: {
      
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
      
          },
      
          connectorColor: 'silver'
      
        }
      
      }
      
      },
      
      series: [{
      
      name: 'Payment',
      
      data: [
      
        { name: 'Partial Paid', y:ppaid},
      
        { name: 'Not paid', y:npaid },
      
        { name: 'Full paid', y:fpaid }
      
      ]
      
      }]
      
      });
      // payment type   
      Highcharts.chart('piecharthichart', {
      chart: {
      type: 'pie',
      options3d: {
      enabled: true,
      alpha: 45,
      beta: 0
      }
      },
      title: {
      text: 'Payment Type Graph'
      },
      tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
      pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      depth: 35,
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      }
      }
      },
      series: [{
      type: 'pie',
      name: 'Payment',
      data: [{
      
        name: 'Cash',
      
        y: cash,
      
        sliced: true,
      
        selected: true
      
      },{
      
        name: 'Cheque',
      
        y: cheque
      
      }, {
      
        name: 'paypal',
      
        y: paypal
      
      },{
      
        name: 'PayUmoney',
      
        y: PayuMoney
      
      },
      
      {
      
        name: 'Bank',
      
        y: Bank
      
      },
      
      {
      
        name: 'Online Payment',
      
        y: OnlinePayment
      
      },
      
       {
      
        name: 'stripe',
      
        y: stripe
      
      }]
      }]
      });
      
      
      
      
      
      }
      
      }); 
      
   </script>
   <script type="text/javascript">
      $.ajax({
         type: 'GET',
      
         dataType: "json",
      
         url: "<?=base_url('dashboard/professor_gender_count')?>",
      
         dataType: "html",
         success: function(data) {
      var response = jQuery.parseJSON(data);
      Male = response.male;
      Female = response.female;
      NotAssigned = response.na;
      
      Highcharts.chart('professor_gender', {
      chart: {
      type: 'pie',
      options3d: {
      enabled: true,
      alpha: 45
      }
      },
      title: {
      text: 'Professors'
      },
      subtitle: {
      text: '% of Male & Female'
      },
      plotOptions: {
      pie: {
      innerSize: 100,
      depth: 45
      }
      },
      series: [{
      name: 'Professors',
      data: [
      {
        name: 'Male',
        y: Male
      },
      {
        name: 'Female',
        y: Female
      },
      {
        name: 'Not Assigned',
        y: NotAssigned
      }]
      }]
      });
      
          }
      
      });
   </script>
   <script type="text/javascript">
      $.ajax({
         type: 'GET',
      
         dataType: "json",
      
         url: "<?=base_url('dashboard/student_gender_count')?>",
      
         dataType: "html",
         success: function(data) {
      var response = jQuery.parseJSON(data);
      Male = response.male;
      Female = response.female;
      NotAssigned = response.na;
      
      Highcharts.chart('student_gender', {
      chart: {
      type: 'pie',
      options3d: {
      enabled: true,
      alpha: 45
      }
      },
      title: {
      text: 'Students'
      },
      subtitle: {
      text: '% of Male & Female'
      },
      plotOptions: {
      pie: {
      innerSize: 100,
      depth: 45
      }
      },
      series: [{
      name: 'Students',
      data: [
      {
        name: 'Male',
        y: Male
      },
      {
        name: 'Female',
        y: Female
      },
      {
        name: 'Not Assigned',
        y: NotAssigned
      }]
      }]
      });
      
          }
      
      });
   </script>
   <?php } ?>

   <?php if($usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") { ?>
   <div class="col-md-6 col-sm-12 col-xs-12">
      <div class="card simple-card" style="height: 370px;">
         <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
         </div>
         <div class="avatar">
            <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
         </div>
         <div class="info1">
            <div class="title">
               <h3><?php
                  $name = $this->session->userdata('name');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                    
                    
                  } else {
                    echo $name;
                    
                  }
                  
                  ?></h3>
            </div>
         </div>
         <div class="bottom admin_dash">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"><?php
                  $name = $this->session->userdata('username');
                  
                  if(strlen($name) > 11) 
                  {
                    echo substr($name, 0,11). ".."; 
                  } else {
                    echo $name;
                  }
                  
                  ?></p>
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-envelope text-maroon-light"></i> Communication E-mail</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"><?php
               $loginuserID=$this->session->userdata('loginuserID');
                  $email = $this->session->userdata('email');
                   echo $email; ?>&nbsp;&nbsp;
                  <?php if($this->session->userdata('verifyemail') != 0)
                  { ?>
                     <span><i class="fa fa-check" title="verify now" style="color: green"></i></span>
         <?php   }
                  else
                  { ?>
                     <div class="verify"></div>
                  
                   
                        <span><button class="verifyemailw" onclick="verifyemailbyadmin(<?php echo $loginuserID ?>)" style="color: red;float: right;margin-top: -35px;"><i class="fa fa-warning" title="verify now"></i></button></span></p>
               <?php   } ?>                 
                        
                        
                     
            </div>
         </div>
         <div class="bottom">
            <div class="col-md-5 col-sm-6 col-xs-6">
               <p class="left"><i class="fa fa-phone text-maroon-light"></i> Phone</p>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
               <p class="left"> <?php echo $phone = $this->session->userdata('phone'); ?>&nbsp;&nbsp;
               <?php if($this->session->userdata('verifymobileno') != 0)
               { ?>
                  <span style="color: green;"><i class="fa fa-check" title="verify now"></i>
               </span>
           <?php    }
               else
               { ?>
                  <span>
                  <a href="#" onclick="sendotp()" style="color: red;"data-toggle="modal" data-target="#verifymob"><i class="fa fa-warning" title="verify now"></i></a>
               </span></p>
           <?php    } ?>
               
            
            </div>
         </div>
      </div>
   </div>
   <?php } ?> 





   <?php
      $usertype = $this->session->userdata("usertype");
      if($usertype == "Professor") {
      ?>
      <!-- <div id="forprofessmodal" class="modall">
     <div class="modal-contentt">
       <span class="closee">&times;</span>
        <div class="modal-header" style="background: #3c8dbc;">
          <h4 style="color: #fff;text-align: center;font-weight: 500;font-family: sans-serif;">REVISED Examination Notification</h4>
        </div>
         <div class="modal-body">
            <p><i class="fa fa-hand-o-right"></i> The general instructions to the students have already been pushed to their dashboard.</p>
               <p><i class="fa fa-hand-o-right"></i> A copy of the same will be mailed to all faculty members/admin.</p>
            <p><i class="fa fa-hand-o-right"></i> 2 New Tabs of 'Exam' and 'Result' have been added to faculty/ admin panel.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> You may check your exam papers, questions by selecting the course and semester filters. Everything will already be uploaded & you can just cross-check.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Exam></strong> Exam Setting. Various exam settings are managed from here. </p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Result></strong> You may check individual student result or download result in bulk after selecting appropriate filter and clicking on 'Download Excel'.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Webcam & Microphone Disabled:</strong> Due to slow internet across mobile and wifi network reported by many students, we've disabled webcam & microphone as it requires fast internet connection.</p>
            <p><i class="fa fa-hand-o-right"></i> <strong>Lockdown Browser Functionality Relaxation:</strong> Few features like exam suspension upon receiving a call on mobile, browser tab minimize, etc will no longer suspend the exam.</p>
        </div>
       
     </div>
   </div> -->
<script>
var modal = document.getElementById("forprofessmodal");
var span = document.getElementsByClassName("closee")[0];
window.onload = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>   
   <!-- <div class="row">
   </div> -->
   <div class="row">
   <div class="col-md-12 col-sm-6 col-xs-12">
         <div class="table-wrapper" style="height: 360px;">
            <div class="table-title theme-bg">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <h2 style="margin-bottom: 10px;">Today's Lecture</h2>
                        </div>
                     </div>
                  </div>
                  <div class="table-responsive">
                     <table class="table table-striped col-md-6 col-sm-6 col-xs-12">
                        <thead>
                           <tr>
                              <td style="font-weight: 600;">S.No.</td>
                              <td style="font-weight: 600;">Semester</td>
                              <td style="font-weight: 600;">Subject Name</td>
                              <td style="font-weight: 600;">Week Day</td>
                              <td style="font-weight: 600;text-align: center;">Time</td>
                              <!-- <td style="font-weight: 600;text-align: center;">Live Classes</td> -->
                              <td style="font-weight: 600;text-align: center;">Action</td>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
                     if(empty($professorLecture))
                     {
                        echo $this->session->flashdata('msg');
                     }
                     else
                     {
                        $currentdate=date('m');
                        $currentyear=date('Y');
                        $count=1;
                        $records=0;
                        // print_r($professorLecture);die;
                        foreach ($professorLecture as $key => $value)
                        {     
                            $days=explode(",", $value->days);
                            $today=date('D');
                            $todaydate=date('Y-m-d');
                            for($g=0;$g<count($days);$g++)
                            {
                              if($days[$g]==$today)
                              {
                                 $records++;
                                 if($currentdate)
                                 { 
                                    ?>
                                    <tr>
                                       <td class="col-sm-1"><?php echo $count++; ?></td>
                                       <td class="col-sm-2"><?php echo $value->semester; ?></td>
                                       <td class="col-sm-2"><?php echo $value->subject; ?></td>
                                       <td class="col-sm-1"><?php echo date("l"); ?></td>
                                       <td class="col-sm-1"><p style="background: #3c8dbc;color: #fff;font-weight: 600;border-radius: 45px;padding:5px;"><?php echo $value->times; ?></p></td>
                                    <?php $markattendance=$this->db->select('atd_date')->where('atd_date',$todaydate)->where('year_mode !=',5)->where('subjectID',$value->subject_id)->get('attendance_student')->row();
                                    if($markattendance)
                                    { ?>
                                       <td class="col-sm-2">
                                             <a href="<?php echo base_url()?>viewlecture/attendance?type=wfilter&course=<?php echo $value->course_id; ?>&subcourse=&yearsOrSemester=<?php echo $value->semester; ?>&subject=<?php echo $value->subject_id; ?>&date=<?php echo date('d-m-Y') ?>" class="btn btn-success " data-placement="top" data-toggle="tooltip" title="Update Attendance">Already Marked</a>
                                          </td>
                                <?php }
                                    else
                                    { ?>
                                       <td class="col-sm-2">
                                             <a href="<?php echo base_url()?>viewlecture/attendance?type=wfilter&course=<?php echo $value->course_id; ?>&subcourse=&yearsOrSemester=<?php echo $value->semester; ?>&subject=<?php echo $value->subject_id; ?>&date=<?php echo date('d-m-Y') ?>" class="btn btn-warning btn-xs mrg for_margR" data-placement="top" data-toggle="tooltip" title="Mark Attendance">Mark Attendance</a>
                                          </td>
                                <?php } ?>
                                    </tr>
                               <?php 
                                 }                                 
                              }
                            }
                                                         
                           
                  $count; }
                      if($records == 0)
                      {
                        ?>
                        <h4 style="color: red;text-align: center;margin-top: 50px;">
                        <?php echo "No Lecture available for today"; ?></h4>

                        
                     <?php }
                     }

                  ?>      
                        </tbody>
                     </table>
                  </div>
         </div>
      </div>
  </div>
   <div class="row" style="margin-top: 20px;">
      <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
         <div class="card simple-card" style="height: 382px;">
            <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
            </div>
            <div class="avatar">
               <img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>">
            </div>
            <div class="info1">
               <div class="title">
                  <h3><?php
                     $name = $this->session->userdata('name');
                     
                     if(strlen($name) > 11) 
                     {
                       echo substr($name, 0,11). ".."; 
                       ?>
                                    <h5 style="color: #000;"><?php echo $usertype; ?></h5>

                               <?php
                     } else {
                       echo $name;
                       ?>
                                    <h5 style="color: #000;"><?php echo $usertype; ?></h5>

                               <?php
                     }
                     
                     ?></h3>
               </div>
            </div>
            <div class="bottom admin_dash">
               <div class="col-md-4 col-sm-6 col-xs-6">
                  <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
               </div>
               <div class="col-md-8 col-sm-6 col-xs-6">
                  <p class="left"><?php
                     $name = $this->session->userdata('username');
                     
                     if(strlen($name) > 11) 
                     {
                       echo substr($name, 0,11). ".."; 
                     } else {
                       echo $name;
                     }
                     
                     ?></p>
               </div>
            </div>
            <div class="bottom">
               <div class="col-md-4 col-sm-6 col-xs-6">
                  <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
               </div>
               <div class="col-md-7 col-sm-6 col-xs-6">
                  <p class="left"><span><?=$this->lang->line("profile_email")?></span><?=$professor->email?>
                  <?php if($professor->verifyemail != 0)
                  { ?>
                     <span><i class="fa fa-check" title="verify now" style="color: green"></i></span>
                        
                <?php  }
                else
                { ?>
                  <span><button class="verifyemailw" onclick="verifyemailbyadmin(<?php echo $professor->professorID ?>)" style="color: red;float: right;margin-right: 50px;"><i class="fa fa-warning" title="verify now"></i></button></span>
              <?php  } ?>          
                  <div class="verify"></div>      
                </p>
               </div>
            </div>
            <div class="bottom">
               <div class="col-md-4 col-sm-6 col-xs-6">
                  <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
               </div>
               <div class="col-md-8 col-sm-6 col-xs-6">
                  <p class="left"><span><?=$professor->phone?></span>
                     <?php if($professor->verifymobileno != 0)
                     { ?>
                        <span>
                           <a href="#" style="color: green;"><i class="fa fa-check" title="verify now"></i></a>
                         </span>
                  <?php   }
                     else
                     { ?>
                        <span>
                           <a href="#" onclick="sendotp()" style="color: red;"data-toggle="modal" data-target="#verifymob"><i class="fa fa-warning" title="verify now"></i></a>
                         </span>
                 <?php } ?>
                  
            
                  </p>
                  
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
               <div class="table-wrapper" style="height: 362px;">
                  <div class="table-title theme-bg">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <h2 style="margin-bottom: 10px;">Today's Attendance</h2>
                        </div>
                     </div>
                  </div>
                   <?php if($todaystudentattendance)
                    { ?>
                  <div class="table-responsive">
                     <table class="table table-striped col-md-12">
                        <thead>
                           <tr>
                              <td style="font-weight: 600;">S.No.</td>
                              <td style="font-weight: 600;">Subject</td>
                              <td style="font-weight: 600;">Total Student</td>
                              <td style="font-weight: 600;">Total Persent</td>
                              <td style="font-weight: 600;">Total Absent</td>
                           </tr>
                        </thead>
                        <tbody>
                          <?php
                              $count=1;
                              $totalpersent=0;
                              foreach ($todaystudentattendance as $key => $value)
                              { ?>
                                 <tr>
                                    <td><?php echo $count; ?></td>
                                     <td><?php echo $value['subject']; ?></td>
                                     <?php $totalstudent=count($value['object']) ?>
                                     <td><?php echo $totalstudent; ?></td>
                                    <?php foreach ($value['object'] as $key)
                                    { 
                                       $present=$key->atd;
                                       if($present==1)
                                       {
                                          $totalpersent=$totalpersent+1;
                                       }
                                          
                                      } ?>
                                      <td><?php echo $totalpersent;?></td>
                                     <td><?php echo $totalstudent-$totalpersent; $totalpersent=0;?></td>
                                    </tr>
               <?php $count++; }
                           
                            ?>                  
                        </tbody>
                     </table>
                  </div>
                  <?php   }
                           else
                           { ?>
                              
                                 
                                 <h4 style="color: red;text-align: center;margin-top: 50px;"><?php echo "No attendance available for today"; ?></h4>
                              
                              
                              
                       <?php } ?>
               </div>
            </div>
      <!-- <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="card simple-card" style="height: 362px;">
               <div id="chart_div" style="width:100%;height: 362px"></div>
               
            </div>
          </div> -->
            
   </div>
      <!-- user profile demo end -->
      <!-- professor notice start --> 
         <!-- <div class="row">
            
         </div> -->
<div class="clearfix"></div>
      <div class="row" style="margin-top: 20px;">
         <div class="hide_1">
            <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
            <div class="card simple-card" style="height: 382px;">
               <div id="chart_div" style="width:100%;height: 382px"></div>
               
            </div>
          </div>
         </div>
         
      <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
               <div class="table-wrapper" style="height: 382px;">
                  <div class="table-title theme-bg">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <h2 style="margin-bottom: 10px;">Notice</h2>
                        </div>
                     </div>
                  </div>
                  <div class="table-responsive">
                     <table class="table table-striped col-md-12">
                        <thead>
                           <tr>
                              <!-- <td style="font-weight: 600;">S.No.</td> -->
                              <td style="font-weight: 600;">Date</td>
                              <td style="font-weight: 600;">Title</td>                              
                              <td style="font-weight: 600;">Action</td>
                           </tr>
                        </thead>
                        <tbody>
                           <?php  {$i = 1; 
                              foreach( $notices  as  $notices) { ?>
                           <tr>
                              <!-- <td><?php echo $i; ?></td> -->
                              <td><?php echo date('d-m-Y', strtotime($notices['date'])); ?></td>
                              <td><?php echo substr($notices['title'], 0,20);?></td>                              
                              <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                           </tr>
                           <?php $i++; }} ?>
                        </tbody>
                     </table>
                  </div>
                  <div class="box-main-footer clearfix">
                     <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
                  </div>
               </div>
            </div>

 
         </div>

         <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12" style="margin-top: 30px;">
               <div class="table-wrapper" style="height: 382px;">
                  <div class="table-title theme-bg">
                     <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                           <h2 style="margin-bottom: 10px;">Assigned Subjects</h2>
                        </div>
                     </div>
                  </div>
                  <div class="table-responsive">
                     <table class="table table-striped col-md-12">
                        <thead>
                           <tr>
                              <td style="font-weight: 600;">S.No.</td>
                              <td style="font-weight: 600;">Course Name</td>
                              <td style="font-weight: 600;">Semester</td>
                              <td style="font-weight: 600;">Subject Name</td>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $subjectcount=0; if($assignedsubjectsemester)
                           {
                        $sno=1; foreach ($assignedsubjectsemester as $key => $value)
                              { $subjectcount++; ?>
                                 <tr>
                                    <td><?php echo $sno; ?></td>
                                    <td><?php echo $value->classes; ?></td>
                                    <td><?php echo $value->yearsOrSemester; ?></td>
                                    <td><?php echo substr($value->subject, 0,20); ?>...</td>
                                  </tr>
                 <?php $sno++;   }
                        }
                        if($assignedsubjectyear)
                        {
                        $sno=$sno; $subjectcount++; foreach ($assignedsubjectyear as $key => $value)
                              { ?>
                                 <tr>
                                    <td><?php echo $sno; ?></td>
                                    <td><?php echo $value->classes; ?></td>
                                    <td><?php echo $value->yearsOrSemester; ?></td>
                                    <td><?php echo substr($value->subject, 0,20); ?>...</td>
                                  </tr>
                 <?php $sno++;   }
                        }
                        if($subjectcount==0)
                        {
                           echo "No Subject Assigned";
                        } ?>                   
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            
         </div>
    <script type="text/javascript"> 
    google.charts.load('current', {'packages':['corechart']}); 
    google.charts.setOnLoadCallback(drawChart);
       
    function drawChart() { 
      var jsonData = $.ajax({ 
          url: "<?php echo base_url() . 'dashboard/pie_chart_js' ?>", 
          dataType: "json", 
          async: false 
          }).responseText; 
      console.log(jsonData);
      var data = new google.visualization.DataTable(jsonData);
      var options = {
                        title:"All Subject Progress",
                        is3D: true,
                        tooltip: { text: 'value',isHtml: true,trigger:'selection' },
                        slices: {
                                    0: { color: 'Red' },
                                    1: { color: 'Yellow' },
                                    2: { color: 'darkGreen' }
                                }

                     };
      var chart = new google.visualization.PieChart(document.getElementById('chart_div')); 
      chart.draw(data,options); 
    } 
 
    </script>
    
    <!-- All Attendance Progress end -->
<div class="row" style="margin-top: 20px;">
   <div class="col-sm-4">
      <div class="box-main1 border-yellow">
         <div class="box-main-header">
            <h3>Calender</h3>
         </div>
         <div class="box-main-body">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div id="mycalendars"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="box-main box_rau border-brightyellow">
         <div class="box-main-header">
            <h3>Thought of the Day</h3>
         </div>
         <div class="box-main-body table-display">
            <div class="daily-thought">
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <div class="col-md-12">
                     <div class="carousel-inner">
                        <div class="item active">
                           <p>“Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.”
                           </p>
                        </div>
                        <div class="item">
                           If you want to be successful, you need to discover something you are actually passionate about.”
                        </div>
                        <div class="item">
                           “Successful and unsuccessful people do not vary greatly in their abilities. They vary in their desires to reach their potential.”
                        </div>
                        <div class="item">
                           “There are no negatives in life, only challenges to overcome that will make you stronger.”
                        </div>
                        <div class="item">
                           “When you think about it, the secret to success in social media isn't any different than the secret to success in life: help others first.”
                        </div>
                        <div class="item">
                           “You see, success isn’t a list of goals to be checked off one after another. It’s not reaching a destination. Success is a journey.”
                        </div>
                        <div class="item">
                           “When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one which has opened for us.”
                        </div>
                        <div class="item">
                           “Solve one problem, and you keep a hundred others away.”
                        </div>
                        <div class="item">
                           “At the end of the day we are accountable to ourselves - our success is a result of what we do.” 
                        </div>
                     </div>
                  </div>
               </div>
               <span>
               <img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/happiness.png" alt="Good Day" />
               </span>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="col-md-9">&nbsp;</div>
                        <div class="col-md-3">
                           <a class="carousel-control" href="#myCarousel" role="button" data-slide="prev">
                           <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
                           <span class="sr-only">Previous</span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-md-2">
                           <a class="carousel-control" href="#myCarousel" role="button" data-slide="next" >
                           <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                        <div class="col-md-9">&nbsp;</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="box-main box_rau border-blue">
         <div class="box-main-header">
            <h3>Weather</h3>
         </div>
         <div class="box-main-body">
            <div class="daily-weather">
               <a class="weatherwidget-io" href="https://forecast7.com/en/30d3278d03/dehradun/" data-label_1="DEHRADUN" data-label_2="WEATHER" data-theme="original" >DEHRADUN WEATHER</a>
               <script>
                  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
               </script>
            </div>
         </div>
      </div>
   </div>
   
</div>

<?php } ?>
<!-- End Professor Dashboard -->

<!-- teachers dashbord -->
<?php
   $usertype = $this->session->userdata("usertype");
   if($usertype == "Teacher") {
   ?>
<div class="row">
   <div class="col-md-4 col-sm-6">
      <a href="<?=base_url('student')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-graduation-cap"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail student-back">
                        <h3 class="cl-info"> <?=$student->counter?></h3>
                        <span>Total <?=$this->lang->line("menu_student")?></span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-4 col-sm-6">
      <a href="<?=base_url('classes')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-book" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail course-back">
                        <h3 class="cl-warning"><?=count($courses)?></h3>
                        <span>Total Courses</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-4 col-sm-6">
      <a href="<?php echo base_url() ?>subject/index">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption success">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-users"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail user-back">
                        <h3 class="cl-success"><?php echo count($subject) ?></h3>
                        <span>Total Subjects</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
</div>
<!-- /row -->
<!-- second -->
<div class="row">
   <div class="col-md-3 col-sm-6 fpaid">
      <a  id = 'fpaidfilter'>
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-check-circle"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <script type="text/javascript">
                           $.ajax({
                           
                           type: 'GET',
                           
                           dataType: "json",
                           
                           url: "<?=base_url('dashboard/paymentscall')?>",
                           
                           dataType: "html",
                           
                           success: function(data) {
                           
                           var response = jQuery.parseJSON(data);
                           
                           $('#fpaid').html(response.fpaid);
                           
                           $('#npaid').html(response.npaid);
                           
                           $('#ppaid').html(response.ppaid);
                           
                           }
                           
                           });
                           
                        </script> 
                        <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment_full->total
                           ?>
                        </span>
                        <h3 class="cl-info" id="fpaid"></h3>
                        <span>Full Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-3 col-sm-6 fpaid">
      <a id = 'ppaidfilter'>
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption danger">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-pie-chart1"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail payment-back">
                        <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment_part->total ?></span>
                        <h3 class="cl-danger" id="ppaid"></h3>
                        <span>Partial Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-danger">
                        <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-3 col-sm-6 fpaid">
      <a id = 'npaidfilter'>
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-times-circle" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail paymentpaid-back">
                        <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totalPayment_not->total ?></span>
                        <h3 class="cl-warning" id="npaid"></h3>
                        <span>Not Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-3 col-sm-6">
      <div class="widget smart-standard-widget">
         <div class="row">
            <div class="widget-caption success">
               <div class="col-xs-4 no-pad zoom">
                  <i class="icon fa fa-line-chart"></i>
               </div>
               <div class="col-xs-8 no-pad">
                  <div class="widget-detail totalPayment-back">
                     <span><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $duePayment ?></span>
                     <h3 class="cl-success"><i class="fa fa-inr" aria-hidden="true"></i><?php echo comp_numb($duePayment) ?></h3>
                     <span>Due Amount</span>
                  </div>
               </div>
               <div class="col-xs-12">
                  <div class="widget-line bg-success">
                     <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor title-font-size">Earning Graph </h3>
   </div>
</div>
<div class="row">
   <div class="col-md-6 col-sm-12">
      <div class="card" style="margin-left: 0px;">
         <div class="card-body">
            <div id="pichartPaymentStatus" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
         </div>
      </div>
   </div>
   <div class="col-md-6 col-sm-12">
      <div class="card" style="margin-left: 0px;">
         <div class="card-body">
            <div id="piecharthichart" style="height: 400px"></div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
</div>
<div class="row">
   <div class="col-xs-12" style="margin-top:3%;">
      <div class="card" style="margin-left: 0px;">
         <div class="card-body">
            <div id="container"></div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
</div>
<script type="text/javascript">
   $.ajax({
   
      type: 'GET',
   
      dataType: "json",
   
      url: "<?=base_url('dashboard/graphcall')?>",
   
      dataType: "html",
   
      success: function(data) {
   
   var response = jQuery.parseJSON(data);
   
   var chart = Highcharts.chart('container', {
   
   title: {
   
    text: 'Earning Graph'
   
   },
   
   subtitle: {
   
    text: 'Plain'
   
   },
   
   
   xAxis: {
   
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
   
   },
   
   series: [{
   
    type: 'column',
   
    colorByPoint: true,
   
    data: response.balance,
   
    showInLegend: false
   
   }]
   
   });
   
   }
   
   }); 
   
   $('#plain').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Plain'
   
    }
   
   });
   
   });
   
   
   
   $('#inverted').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: true,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Inverted'
   
    }
   
   });
   
   });
   
   
   
   $('#polar').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: true
   
    },
   
    subtitle: {
   
      text: 'Polar'
   
    }
   
   });
   
   });
   
</script>  
<script type="text/javascript">
   $.ajax({
   
   
   
       type: 'GET',
   
   
   
       dataType: "json",
   
   
   
       url: "<?=base_url('dashboard/paymentscall')?>",
   
   
   
       dataType: "html",
   
   
   
       success: function(data) {
   
   
   
         var response = jQuery.parseJSON(data);
   
   
   
   
   
         npaid = response.npaid;
   
   
   
         ppaid = response.ppaid;
   
   
   
         fpaid = response.fpaid;
   
   
   
         cash = response.cash;
   
   
   
         cheque = response.cheque;
   
   
   
         paypal = response.paypal;
   
   
   
         stripe = response.stripe;
   
   
   
         PayuMoney = response.PayuMoney;
   
   
   
         OnlinePayment = response.OnlinePayment;
   
   
   
         Bank = response.Bank;
   
   
   
   //payment status
   
   // Build the chart
   
   Highcharts.chart('pichartPaymentStatus', {
   
   chart: {
   
   plotBackgroundColor: null,
   
   plotBorderWidth: null,
   
   plotShadow: false,
   
   type: 'pie'
   
   },
   
   title: {
   
   text: 'Payment Status Graph'
   
   },
   
   tooltip: {
   
   pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
   pie: {
   
     allowPointSelect: true,
   
     cursor: 'pointer',
   
     dataLabels: {
   
       enabled: true,
   
       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
       style: {
   
         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
       },
   
       connectorColor: 'silver'
   
     }
   
   }
   
   },
   
   series: [{
   
   name: 'Payment',
   
   data: [
   
     { name: 'Partial Paid', y:ppaid},
   
     { name: 'Not paid', y:npaid },
   
     { name: 'Full paid', y:fpaid }
   
   ]
   
   }]
   
   });
   
   
   
   // payment type
   
   
   
   Highcharts.chart('piecharthichart', {
   
   chart: {
   
   plotBackgroundColor: null,
   
   plotBorderWidth: null,
   
   plotShadow: false,
   
   type: 'pie'
   
   },
   
   title: {
   
   text: 'Payment Type Graph'
   
   },
   
   tooltip: {
   
   pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
   pie: {
   
     allowPointSelect: true,
   
     cursor: 'pointer',
   
     dataLabels: {
   
       enabled: true,
   
       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
       style: {
   
         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
       }
   
     }
   
   }
   
   },
   
   series: [{
   
   name: 'Payment',
   
   colorByPoint: true,
   
   data: [{
   
     name: 'Cash',
   
     y: cash,
   
     sliced: true,
   
     selected: true
   
   },{
   
     name: 'Cheque',
   
     y: cheque
   
   }, {
   
     name: 'paypal',
   
     y: paypal
   
   },{
   
     name: 'PayUmoney',
   
     y: PayuMoney
   
   },
   
   {
   
     name: 'Bank',
   
     y: Bank
   
   },
   
   {
   
     name: 'Online Payment',
   
     y: OnlinePayment
   
   },
   
    {
   
     name: 'stripe',
   
     y: stripe
   
   }]
   
   }]
   
   });
   
   }
   
   }); 
   
</script>
<?php } ?>
<!-- end teacher dashbord -->
<!-- student dashbord -->  
<?php
   $usertype = $this->session->userdata("usertype");
   
   if($usertype == "Student" || $usertype == "Parent") {
   
   ?>

   <!-- <div id="studentmodal" class="modall">
     <div class="modal-contentt">
       <span class="closee">&times;</span>
        <div class="modal-header" style="background: #3c8dbc;">
          <h4 style="color: #fff;text-align: center;font-weight: 500;font-family: sans-serif;">REVISED Examination Notification</h4>
        </div>
        <div class="modal-body">
          <p><i class="fa fa-hand-o-right"></i> <strong>Please click on Support button on right side of your dashboard. Click on 'Chat' and keep this tab open to clarify any technical issues during the examination.</strong></p>
          <strong><p> <i class="fa fa-hand-o-right"></i> Please write down these numbers before starting any exam. You may call/ WhatsApp on 9911331937 / 8802037139 in case of facing any issues while writing the exam.</p></strong>
            <p><i class="fa fa-hand-o-right"></i> It is advised to appear for the exam on Google Chrome browser both in laptop & mobile devices. In mobile, please keep chat option in another tab.</p>
            <p><i class="fa fa-hand-o-right"></i> Do not refresh the browser after starting the exam. All marked questions will be lost if you have not clicked on 'Finish Exam Now'.</p>
            <p><i class="fa fa-hand-o-right"></i> There are 2 types of question (a) Multiple choice questions (b) True & False</p>
            <p><i class="fa fa-hand-o-right"></i> You must complete the online exam within the stipulated time frame as no additional time would be provided.</p>
            <p><i class="fa fa-hand-o-right"></i> The duration of the exam remains unaltered irrespective of the user logging in at correct start time or later during the exam.</p>
            <p><i class="fa fa-hand-o-right"></i> To change an answer, simply click the desired option button. There will be three buttons on each screen, the use of buttons is given below:</p>
            <p><strong >Review question:</strong> Answer the question and mark it for review at a later stage.</p>

          <p><strong>Submit & Next:</strong> Save the selected answer and proceed to next question. </p>

          <p><strong>Finish Exam Now:</strong> Submit & exit from exam.</p>
          <p><i class="fa fa-hand-o-right"></i> Do Not CLICK on the 'Finish Exam Now' unless you have completed the exam. In case you click 'Finish Exam Now ' button, you will not be permitted to restart the exam.</p>
          <p><i class="fa fa-hand-o-right"></i> The exam will auto-submit at the end of the examination timing. All answers which you have marked till now will be saved in this case.</p>
        </div>
       
     </div>
   </div> -->
<div class="row">
   <div id="rating" style="color: green;font-size: 100%;font-weight: bold;text-align: center;"></div>
   <div class="col-md-3 col-sm-6" >
      <a href="<?=base_url('subject')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-book" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <h3 class="cl-info"> <?=count($subject)?></h3>
                        <span>Total subjects</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-3 col-sm-6">
      <!-- <a href="<?=base_url('exam/exam_list/1')?>"> -->
      <div class="widget smart-standard-widget">
         <div class="row">
            <div class="widget-caption danger">
               <div class="col-xs-4 no-pad zoom">
                  <i class="icon ti ti-ruler-pencil"></i>
               </div>
               <div class="col-xs-8 no-pad">
                  <div class="widget-detail">
                     <!-- <h3 class="cl-danger"><?php echo $Examtime ?></h3> -->
                     <h3 class="cl-danger">External Exam</h3>
                     <span>Exam Date</span>
                  </div>
               </div>
               <div class="col-xs-12">
                  <div class="widget-line bg-danger">
                     <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--  </a> -->
   </div>
   <div class="col-md-3 col-sm-6">
      <a href="#">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-check-square-o" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <!--  <h3 class="cl-warning"><?=$exams ?></h3> -->
                        <h3 class="cl-warning"> 0 </h3>
                        <span>Total Results Out</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-3 col-sm-6">
      <a href="<?=base_url('invoice/index')?>">
         <div class="widget smart-standard-widget">
            <!-- <div class="row" data-step="3" data-intro="Ok, wasn't that fun?" data-position='bottom'> -->
            <div class="row">
               <div class="widget-caption success">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-rupee"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <h3 class="cl-success"><i class="fa fa-inr" aria-hidden="true">&nbsp;</i><?php echo $class->fee; ?></h3>
                        <span>Course fee per annum</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
</div>
<!-- /row -->
<!-- show student progress in % Start -->
<!-- <div class="row page-titles">
   <div class="col-md-12" style="margin-top:10px;">
               
               <?php $bar=0;
               $photo=$student->photo;
               // print_r($student->photo);die;
               if($photo != "defualt.png")
               {
                  $bar=25;
               }
               if($student->verifymobileno != 0)
               {
                  $bar=$bar+25;
               }
               if($student->verifyemail != 0)
               {
                  $bar=$bar+25;
               }
               if($totalsubject)
               {
                  $video=0;$epub=0;
                  foreach ($totalcoursestatus as $key => $value)
                  {
                     if($value->type=="videos")
                     {
                        if($video==0)
                        {
                           $video=$video+1;
                        }
                     }
                     if ($value->type=="epub")
                     {
                        if($epub==0)
                        {
                           $video=$video+1;
                        }
                     }
                  }
                  if ($video != 0 || $epub != 0)
                  {
                     if($totalsubject->totalsubject != 0)
                     {
                        $progress=(($video+$epub)*100)/$totalsubject->totalsubject;
                        $unitavg=round($progress,0);
                        if($unitavg >= 25)
                        {
                           $bar=$bar+25;
                        }
                     }
                     
                  }
                  
               }
               ?>
               <?php 
                     if ($bar == 0)
                     { ?>
                        <h4 style="color: #d9534f;text-align: center;">oops! Your profile is only 0% complete
                           <img src="<?php echo base_url() ?>/assets/img/sad.png">

                        </h4>
                  <?php   }
                  elseif ($bar <= 25)
                     { ?>
                        <h4 style="color: #d9534f;text-align: center;">oops! Your profile is only 25% complete
                           <img src="<?php echo base_url() ?>/assets/img/sad.png">

                        </h4>
                  <?php   }
                     elseif ($bar >= 26 && $bar <= 50)
                     { ?>
                        <h4 style="color: #f0ad4e;text-align: center;">Good! Your profile is 50% complete Now
                        <img src="<?php echo base_url() ?>/assets/img/sad.png"></h4>
                 <?php    }
                     elseif ($bar >= 51 && $bar <= 75)
                     { ?> 
                        <h4 style="color: #337ab7;text-align: center;">Great! Your profile is 75% complete Now
                        <img src="<?php echo base_url() ?>/assets/img/happy.png"></h4>
                 <?php    }
                     else
                     { ?>
                        <h4 style="color: #5cb85c;text-align: center;">Wahoo! Your profile is 100% completed
                           <img src="<?php echo base_url() ?>/assets/img/smiley.png">
                        </h4>
                 <?php    }

                  ?>
               
               <div class="progress">

                  <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:<?php echo $bar.'%';?>; box-shadow:-1px 10px 10px rgba(92, 190, 220, 0.5);">                     
                  </div>
                  <div class="progress-value" style=" position: absolute;color: #fff;margin-left: 500px;color: #000;"><?php echo $bar.'%';?></div>
               </div>
            </div>
</div> -->
<!-- show student progress in % end -->
<div class="row page-titles">
   <div class="col-md-12 align-self-center">
      <h3 class="text-themecolor title-font-size">Today's Lecture</h3>
   </div>
</div>
<div class="row page-titles" id="autometic">
   <div class="col-md-12" style="margin-top:10px;">
      <div class="table-responsive">
        <table class="table table-responsive table-striped table-bordered table-hover" >

          <thead>

            <tr>

           

            <th>Subject</th>

            <th>Subject Code</th>

            <th>Professor Name</th>

            <th>Day</th>

            <th>Time</th>
            <!-- <th>Live Classes</th> -->
            <!-- <th></th> -->
            </tr>

          </thead>

          <tbody>
         
         <?php $time= date('h:i');
         $count=1;
          foreach ($subjects as $key1 => $values) 
          {
          $days=explode(",", $values['days']);
          $today=date('D');
          $todaydate=date('Y-m-d');
          for($g=0;$g<count($days);$g++)
          {
            if($days[$g]==$today)
            {
               ?>
                  <tr>
                     <td><?php echo $values['subject'];?></td>
                     <td><?php echo $values['subject_code'];?></td>
                     <td><?php echo $values['name'];?></td>
                     <td><?php echo date("l"); ?></td>
                     <td><?php echo $studenttime=$values['times']; ?></td>
                     <!-- <td>
                        <?php $date=date('Y-m-d'); $checkliveclass=$this->db->select('live_status,status,streamID,av,channel_name,classtiming')->where(array('courseID' => $values['course_id'],'yearsOrSemester'=>$values['semester'],'subjectID'=>$values['subjectID'],'createdID'=>$values['professorID'],'date'=>$date))->get('live_broadcast')->row();
                        if($checkliveclass)
                        { 
                          if($checkliveclass->status==1)
                          { ?>
                             <a href="<?php echo base_url('live/audience/'.$checkliveclass->streamID.'/'.$checkliveclass->av.'/'.$checkliveclass->classtiming.'?channel='.$checkliveclass->channel_name) ?>" class="btn btn-success btn-xs mrg for_margR" style="text-align: center;"> Join Live Class</a>
                        <?php  }
                        else
                        { ?>
                          <a href="#" class="btn btn-danger btn-xs mrg for_margR" style="text-align: center;">Live Class Over</a>
                      <?php  }
                          ?>
                        
                          
                      <?php  }
                        else
                        { ?>
                          <a href="" class="btn btn-primary btn-xs mrg for_margR" style="text-align: center;">No Live Class</a>
                      <?php  } ?>                       
                     </td> -->
                <?php  $variable = substr($studenttime, 0, strpos($studenttime, "-")); 
                $endTime = strtotime("+15 minutes", strtotime($variable));
                $starttime=strtotime("+16 minutes", strtotime($time));
                $lasttime= date('h:i', $starttime);
                $checktime= date('h:i', $endTime);
                if($lasttime >= $checktime && $checktime >= $time)
                {
                  if($values['onlinecheck'])
                  {
                     if($values['onlinecheck']->activestatus==1)
                     { ?>
                       <!-- <td><button type="button" disabled="disabled" class="btn btn-success">Attending</button></td> -->
                   <?php }
                   else
                   { ?>
                     <!-- <td><button type="button" disabled="disabled" class="btn btn-info">Attend</button></td> -->
                 <?php }
                  }
                  else
                  { ?>
                     <!-- <td><button type="button" class="btn btn-info" id="attend" onclick="checkavtive('<?php echo $checktime; ?>','<?php echo $loginuserID = $this->session->userdata("loginuserID"); ?>','<?php echo $values['subjectID'] ?>','<?php echo $values['professorID'] ?>','<?php echo $values['semester'] ?>','<?php echo $values['course_id'] ?>')">Attend</button></td> -->
                <?php }                   
               } 
                else
                { ?>
                  <!-- <td><button type="button" disabled="disabled" class="btn btn-info">Attend</button></td> -->
                   
                  
             <?php } ?> 
             <?php  $variable1 = substr($studenttime, strpos($studenttime, "-") + 1);  ?>                   
                  <input type="hidden" name="logintime" id="logintime<?php echo $count;?>" value="<?php echo $variable2=date('h:i', strtotime($variable1)); ?>">
                  <input type="hidden" name="slottime" id="slottime" value="<?php echo $count ?>">
                  <input type="hidden" name="studenttime" id="studenttime" value="<?php echo $time ?>">
                  </tr>
  <?php
         } } $count++; }
            foreach($student_subject as $key1 => $values)
            {
          $days=explode(",", $values['days']);
          $today=date('D');
          $todaydate=date('Y-m-d');
          for($g=0;$g<count($days);$g++)
          {
            if($days[$g]==$today)
            {
         ?>
               <tr>
                  <td><?php echo $values['subject'];?></td>
                  <td><?php echo $values['subject_code'];?></td>
                  <td><?php echo $values['name'];?></td>
                  <td class="col-sm-2"><?php echo date("l"); ?></td>
                  <td><?php echo $studenttime=$values['times'];?></td>
                  <!-- <td>
                        <?php $date=date('Y-m-d'); $checkliveclass=$this->db->select('live_status,status,streamID,av,channel_name,classtiming')->where(array('courseID' => $values['course_id'],'yearsOrSemester'=>$values['semester'],'subjectID'=>$values['subjectID'],'createdID'=>$values['professorID'],'date'=>$date))->get('live_broadcast')->row();
                        if($checkliveclass)
                        { 
                          if($checkliveclass->status==1)
                          { ?>
                             <a href="<?php echo base_url('live/audience/'.$checkliveclass->streamID.'/'.$checkliveclass->av.'/'.$checkliveclass->classtiming.'?channel='.$checkliveclass->channel_name) ?>" class="btn btn-success btn-xs mrg for_margR" style="text-align: center;"> Join Live Class</a>
                        <?php  }
                        else
                        { ?>
                          <a href="#" class="btn btn-danger btn-xs mrg for_margR" style="text-align: center;">Live Class Over</a>
                      <?php  }
                          ?>
                        
                          
                      <?php  }
                        else
                        { ?>
                          <<a href="" class="btn btn-primary btn-xs mrg for_margR" style="text-align: center;">No Live Class</a>
                      <?php  } ?>                       
                  </td> -->
            <?php  $variable = substr($studenttime, 0, strpos($studenttime, "-")); 
                $endTime = strtotime("+15 minutes", strtotime($variable));
                $starttime=strtotime("+16 minutes", strtotime($time));
                $lasttime= date('h:i', $starttime);
                $checktime= date('h:i', $endTime);
                if($lasttime >= $checktime && $checktime >= $time)
                { 
                  if($values['optionalsubjects_onlinecheck'])
                  {
                     if($values['optionalsubjects_onlinecheck']->activestatus==1)
                     { ?>
                       <!-- <td><button type="button" disabled="disabled" class="btn btn-success">Attending</button></td> -->
                   <?php }
                   else
                   { ?>
                     <!-- <td><button type="button" disabled="disabled" class="btn btn-info">Attend</button></td> -->
                 <?php }
                  }
                  else
                  { ?>
                     <!-- <td><button type="button" class="btn btn-info" id="attend" onclick="checkavtive('<?php echo $checktime; ?>','<?php echo $loginuserID = $this->session->userdata("loginuserID"); ?>','<?php echo $values['subjectID'] ?>','<?php echo $values['professorID'] ?>','<?php echo $values['semester'] ?>','<?php echo $values['course_id'] ?>')">Attend</button></td> -->
                <?php } 
               }  
                else
                { ?>
                  <td><button type="button" disabled="disabled" class="btn btn-info">Attend</button></td>
             <?php } ?> 
             <?php  $variable1 = substr($studenttime, strpos($studenttime, "-") + 1);  ?>                   
                  <input type="hidden" name="logintime" id="logintime<?php echo $count;?>" value="<?php echo $variable2=date('h:i', strtotime($variable1)); ?>">
                  <input type="hidden" name="slottime" id="slottime" value="<?php echo $count ?>">
                  <input type="hidden" name="studenttime" id="studenttime" value="<?php echo $time ?>">
               </tr>
         <?php
            } } $count++; }
         ?>
          </tbody>

        </table>

          </div>
   </div>
</div>
<script>

   var modal = document.getElementById("studentmodal");
var span = document.getElementsByClassName("closee")[0];
window.onload = function() {
  modal.style.display = "block";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<script type="text/javascript">
  function checkavtive(checktime,studentID,subjectID,professorID,yearsOrSemester,course_id)
  {
   var systemtime='<?php echo $time ?>';
   if(checktime >= systemtime)
   {
      $.ajax({

            type: "POST",
            data:{"checktime":checktime,"studentID":studentID,"subjectID":subjectID,"professorID":professorID,"yearsOrSemester":yearsOrSemester,"course_id":course_id},
            url:"<?php echo base_url(); ?>viewlecture/requesttoattendclass",
            success: function(data)
            {
                $("#attend").attr("disabled","disabled");  
                alert("Online Lecture attending confirmation will be sent.");
                // $("#attend1").show();           

            }

        });
   }
   else
   {
      alert("Your Time Is Over");
      location.reload();

   }
    
  }
</script>
<!-- row 2 student -->
<div class="row">
   <div class="col-md-10 col-md-offset-1 ">
      <div class="box-main border-red">
         <div class="box-main-header">
            
<?php
  if ( $usertype == "Parent") { ?>
  <h3>Student Profile</h3>
   <?php } else if( $usertype == "Student"){ ?>

       <h3>My Profile</h3>
   <?php  } ?>
         </div>
         <div class="box-main-body my-profile">
            <div class="profile-head">
               <div class="profile-head-content">
                 <?php if($this->session->userdata('photo')){ ?>
                            <img src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>" class="img-responsive img-circle"  alt="user" />
                      <?php  }
                      else
                      { ?>
                        <img src="<?=base_url("uploads/images/defualt.png");?>" class="img-responsive img-circle"  alt="user" />
                    <?php  } ?>
                  <p><strong><?=$student->name?></strong></p>
                  <p class="pdt0">
                     <?=$class->classes?>                           
                  </p>
                  <span class="bold_td">Course duration : </span><span class="bold_td"><?=$class->duration?>yr</span>
               </div>
            </div>
            <div class="profile-body fading">
               <h2><?=$this->lang->line("personal_information")?></h2>
               <hr/>
                    <div class="col-md-3 bold_td"><?=$this->lang->line("student_roll")?>:</div>
                     <div class="col-md-3"><?=$student->roll?></div>

                     <div class="col-md-1 bold_td"><?=$this->lang->line("student_email")?>:</div>
                     <div class="col-md-5"><?=$student->email?>
                     <?php if($student->verifyemail==1)
                     { ?><i class="fa fa-check" title="verify now" style="color: green"></i>
                   <?php   }
                     else
                     { ?>
                        <div class="verify"></div>
                        <button class="verifyemailw" onclick="verifyemailbyadmin(<?php echo $student->studentID?>)" style="color: red;float: right;margin-top: -20px"><i class="fa fa-warning" title="verify now"></i></button>
                        
                     
                   <?php  } ?>
                         
                     </div>

                     <div class="col-md-3 bold_td"><?=$this->lang->line("student_sex")?>:</div>
                     <div class="col-md-3"><?=$student->sex?></div>
                  
                     <div class="col-md-3 bold_td"><?=$this->lang->line("father_name")?>:</div>
                     <div class="col-md-3">
                        <?php if ($student->father_name!='') {
                           echo $student->father_name;
                           
                           }else{
                           
                             echo "--";
                           
                           }
                           
                           
                           
                            ?>
                     </div>

                     <div class="col-md-3 bold_td"><?=$this->lang->line("mother_name")?>:</div>
                     <div class="col-md-3"><?php if ($student->mother_name!='') {
                           echo $student->mother_name;
                           
                           }else{
                             echo "--";
                           }
                            ?></div>
                     
                     <div class="col-md-3 bold_td"><?=$this->lang->line("student_phone")?>:</div>
                     <div class="col-md-3">
                        <?php if ($student->phone!='') {
                           echo $student->phone;

                           if ($student->verifymobileno==1)
                              { ?>
                                 <span><i class="fa fa-check" title="verified" style="color: green"></i></span>
                              
                         <?php  }
                         else
                         { ?>
                            <a href="#" onclick="sendotp()"  style="color: red;" data-toggle="modal" data-target="#verifymob"><i class="fa fa-warning" title="verify now"></i></a>
                       <?php  }
                      

                           
                           }else{   
                           
                             echo "--";
                           
                           }
                           
                           
                           
                            ?>
                     </div>
            </div>
            
         </div>
         <!-- <div class="row">
           
         </div> -->
         <div class="box-main-footer clearfix">
            <a href="<?php echo base_url() ?>profile/index" onload="myfunction()" class="btn btn-sm btn-view pull-right" data-toggle="popover">View Profile</a>
         </div>
      </div>
   </div>
   <!-- <div class="col-md-4">
      <div class="box-main border-yellow mrgtop">
         <div class="box-main-header">
            <h3>Learning Management System</h3>
         </div>
         <div class="box-main-body">
            <div class="digital-library">
               
                <a href="<?php echo base_url() ?>lms/lmsLatest" target="_blank">Visit LMS</a>
                  <video  width="335" height="240" controls="" > -->
                     <!-- <source src="<?php echo base_url() ?>uploads/videos/lmsdemo.mp4" type="video/mp4"> -->
                  <!-- </video>
               
            </div>
         </div>
      </div>
   </div> -->
</div>
<!-- /row 2 student -->
<!-- raushan -->
<div class="row page-titles">
   <div class="col-md-12 align-self-center">
      <h3 class="text-themecolor title-font-size">Recent Notice </h3>
   </div>
</div>
 <?php 
   if ( $usertype == "Student") { ?>
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="table-responsive">
            <div class="table-wrapper ">
               <table class="table table-striped table-hover">
                  <thead style="background: #3c8dbc;">
                     <tr style="color: #fff;">
                        <td style="font-weight: 600;">S.N.</td>
                        <td style="font-weight: 600;">Date</td>
                        <td style="font-weight: 600;">Title</td>                        
                        <td style="font-weight: 600;">Sender Name</td>
                        <td style="font-weight: 600;">Action</td>
                     </tr>
                  </thead>
                  <tbody>
                     <?php  {$i = 1; 
                        foreach( $NoticeD  as  $notices) { ?>
                     <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo date('d-m-Y', strtotime($notices['date'])); ?></td>
                        <td><?php echo substr($notices['title'], 0,20); ?></td>                        
                        <td><?php echo $notices['sendByUSerName']; ?></td>
                        <td><?php echo btn_viewfornotice('notice/view/'.$notices['noticeID'] , $this->lang->line('view')); ?></td>
                     </tr>
                     <?php $i++; }} ?>
                  </tbody>
               </table>
               <div class="box-main-footer clearfix">
                  <a href="<?php echo base_url() ?>notice/index" class="btn btn-info pull-right" data-toggle="popover">View All</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<div class="row">
   <div class="col-sm-4">
      <div class="box-main1 border-yellow">
         <div class="box-main-header">
            <h3>Calender</h3>
         </div>
         <div class="box-main-body">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div id="mycalendars"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="box-main box_rau border-brightyellow">
         <div class="box-main-header">
            <h3>Thought of the Day</h3>
         </div>
         <div class="box-main-body table-display">
            <div class="daily-thought">
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <div class="col-md-12">
                     <div class="carousel-inner">
                        <div class="item active">
                           <p>“Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.”
                           </p>
                        </div>
                        <div class="item">
                           If you want to be successful, you need to discover something you are actually passionate about.”
                        </div>
                        <div class="item">
                           “Successful and unsuccessful people do not vary greatly in their abilities. They vary in their desires to reach their potential.”
                        </div>
                        <div class="item">
                           “There are no negatives in life, only challenges to overcome that will make you stronger.”
                        </div>
                        <div class="item">
                           “When you think about it, the secret to success in social media isn't any different than the secret to success in life: help others first.”
                        </div>
                        <div class="item">
                           “You see, success isn’t a list of goals to be checked off one after another. It’s not reaching a destination. Success is a journey.”
                        </div>
                        <div class="item">
                           “When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one which has opened for us.”
                        </div>
                        <div class="item">
                           “Solve one problem, and you keep a hundred others away.”
                        </div>
                        <div class="item">
                           “At the end of the day we are accountable to ourselves - our success is a result of what we do.” 
                        </div>
                     </div>
                  </div>
               </div>
               <span>
               <img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/happiness.png" alt="Good Day" />
               </span>
               <div>&nbsp;</div>
               <div>&nbsp;</div>
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="col-md-9">&nbsp;</div>
                        <div class="col-md-3">
                           <a class="carousel-control" href="#myCarousel" role="button" data-slide="prev">
                           <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
                           <span class="sr-only">Previous</span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-md-2">
                           <a class="carousel-control" href="#myCarousel" role="button" data-slide="next" >
                           <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                        <div class="col-md-9">&nbsp;</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="box-main box_rau border-blue">
         <div class="box-main-header">
            <h3>Weather</h3>
         </div>
         <div class="box-main-body">
            <div class="daily-weather">
               <a class="weatherwidget-io" href="https://forecast7.com/en/30d3278d03/dehradun/" data-label_1="DEHRADUN" data-label_2="WEATHER" data-theme="original" >DEHRADUN WEATHER</a>
               <script>
                  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
               </script>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>


<div id="myChartmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo base_url('assets/fullcalendar/fullcalendar.min.js'); ?>"></script>

<script type="text/javascript">
   Highcharts.chart('container', {
   
   chart: {
   
     plotBackgroundColor: null,
   
     plotBorderWidth: null,
   
     plotShadow: false,
   
     type: 'pie'
   
   },
   
   title: {
   
     text: ''
   
   },
   
   tooltip: {
   
     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
     pie: {
   
       allowPointSelect: true,
   
       cursor: 'pointer',
   
       dataLabels: {
   
         enabled: true,
   
         format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
         style: {
   
           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
         }
   
       }
   
     }
   
   },
   
   series: [{
   
     name: 'Brands',
   
     colorByPoint: true,
   
     data: [{
   
       name: '1st Sem',
   
       y: 41.41,
   
       sliced: true,
   
       selected: true
   
     }, {
   
       name: '2nd Sem',
   
       y: 11.84
   
     }, {
   
       name: '3rd Sem',
   
       y: 10.85
   
     }, {
   
       name: '4th Sem',
   
       y: 10.67
   
     }, {
   
       name: '5th Sem',
   
       y: 9.18
   
     }, {
   
       name: '6th Sem',
   
       y: 18.64
   
     }]
   
   }]
   
   });
   
</script>
<script type="text/javascript">
   Highcharts.chart('container3', {
   
   chart: {
   
     plotBackgroundColor: null,
   
     plotBorderWidth: null,
   
     plotShadow: false,
   
     type: 'pie'
   
   },
   
   title: {
   
     text: ''
   
   },
   
   tooltip: {
   
     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
     pie: {
   
       allowPointSelect: true,
   
       cursor: 'pointer',
   
       dataLabels: {
   
         enabled: true,
   
         format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
         style: {
   
           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
         }
   
       }
   
     }
   
   },
   
   });
   
</script>
<script type="text/javascript">
   $.ajax({
   
      type: 'GET',
   
      dataType: "json",
   
      url: "<?=base_url('dashboard/graphcall')?>",
   
      dataType: "html",
   
      success: function(data) {
   
   var response = jQuery.parseJSON(data);
   
   var chart = Highcharts.chart('container', {
   
   title: {
   
    text: 'Earning Graph'
   
   },
   
   subtitle: {
   
    text: 'Plain'
   
   },
   
   
   xAxis: {
   
    categories: ['1st Semester', '2nd Semester', '3rd Semester', '4th Semester', '5th Semester', '6th Semester', '7th Semester', '8th Semester']
   
   },
   
   series: [{
   
    type: 'column',
   
    colorByPoint: true,
   
    data: response.balance,
   
    showInLegend: false
   
   }]
   
   });
   
   }
   
   }); 
   
   $('#plain').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Plain'
   
    }
   
   });
   
   });
   
   
   
   $('#inverted').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: true,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Inverted'
   
    }
   
   });
   
   });
   
   
   
   $('#polar').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: true
   
    },
   
    subtitle: {
   
      text: 'Polar'
   
    }
   
   });
   
   });
   
</script> 
</div>
<?php } ?>
</div>
<style type="text/css">
   .fpaid{
   cursor:pointer;
   }
</style>
<style type="text/css">
   .font-icon-shortcut{
   color: #e20b0b;
   }
   .title-font-size{
   font-size: 16px;
   }
   .widget-shortcut-height{
   height: 50px;
   }
   * {
   box-sizing: border-box;
   }
   .zoom {
   transition: transform .2s; 
   }
   .zoom:hover {
   -ms-transform: scale(1.5); 
   -webkit-transform: scale(1.5);
   transform: scale(1.2); 
   }
   .thoughts{
   margin-top: 25px;
   cursor: pointer;
   /*position: fixed;*/
   }
</style>
</style>
<script></script>

<script>
   $('#weatherfunctionSession').click(function() {  
   
     var weatherfunctionSession = $(this).val();   
   
       $.ajax({  
   
         type: 'POST',   
   
         url: "<?=base_url('dashboard/index')?>",   
   
         data: "weatherfunctionSession=" + weatherfunctionSession,   
    
         dataType: "html",   
   
         success: function(data) {   
   
           // location.reload();  
   
         }   
   
       });   
   
     });
   
</script>

<script>
    $(document).ready(function(){
        //get the pie chart canvas
        var cData = JSON.parse(`<?php echo $chart_data; ?>`);
        var data = {
         interactivityEnabled: false,
            labels: cData.label,
            datasets: [{
                label: "Users Count",
                data: cData.data,
                backgroundColor: [
                    "#FF0000",
                    "#FFFF00",
                    "#008000",
                ],
                borderColor: [
                    "#FF0000",
                    "#FFFF00",
                    "#008000",
                ],
                borderWidth: [1, 1, 1]
            }]
        };
        
        var options = {
            responsive: true,
            title: {
                display: true,
                position: "top",
                text: "All Subject Progress",
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: true,
                position: "bottom"
                // labels: {
                //   fontColor: "#333",
                //   fontSize: 16
                // }
            }
        };
        
        var canvas = document.getElementById("pie-chart");
        var ctx = canvas.getContext("2d");
        var myNewChart = new Chart(ctx, {
            type: 'pie',
            data: data
        });
        
        
        canvas.onclick = function(evt) {
            var activePoints = myNewChart.getElementsAtEvent(evt);
            if (activePoints[0]) {
                var chartData = activePoints[0]['_chart'].config.data;
                var idx = activePoints[0]['_index'];
                
                var label = chartData.labels[idx];
                var value = chartData.datasets[0].data[idx];
                
                var url = "label=" + label;
                console.log(url);
                alert(url);
                //$("#myChartmodal").modal("show");
            }
        };
        
    });
</script>