<div class="">
   
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i> Video </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active">Video</li>
            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
            <div class="card">
            <div>
            <div class="">

            <div class="box-body">

                <div class="row">

                    <div class="col-sm-12">
                        <?php
                            $uri = $this->uri->segment(3);
                            $usertype = $this->session->userdata("usertype");
                            $SpecialUsertype = $this->session->userdata('SpecialUsertype');
                            if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == 'Support' || $usertype == 'Academic' || $SpecialUsertype=='Btech') {

                        ?>

                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                               <div class="pull-right">
                              <div class="btn-group">
                           <a href="<?php echo base_url('videos/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span>Add a video</a>

                              </div>
                           </div>
                        </div>
                        <div class="clearfix"></div>



        <div class="col-sm-12">

            <div class="theme_input_blue">
            <?php 
               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
               $display =  "block";
               $addmorebutton = "none";
               $Removemorebutton = "inline-block";
               }else{
               $display = "none";
               $addmorebutton = "inline-block";
               $Removemorebutton = "none";
               }
               
               ?>
                
                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterVideoclassesID')), "id='classesID' onchange='CourseSDependent($(this).val())' class='form-control'");
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label"> Sub Courses </label>

                                <div class="">
                                    <select id="subCourseID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                        if ($value->sub_coursesID==$this->session->userdata('FilterNoticesubCourseID')) {
                                            $selected =  "Selected";
                                        }else{
                                          $selected =  "";
                                        }
                                        ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?>                                            
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='yearsOrSemester' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('yearsOrSemester'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterVideoyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterVideoyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
         
            <?php }  ?>                      

                                
                <?php  if( $usertype == "ClgAdmin" || $usertype == "Academic"  || $usertype == "Admin" || $SpecialUsertype=='Btech'){?>
 
                        <!-- <div id="hide-tab">

                            <table id = "noticeTables" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>

                                        <th class="col-sm-4">Course</th>

                                        <th class="col-sm-4">Sub Course</th>

                                        <th class="col-sm-4">Year / Semester</th>

                                        <th class="col-sm-4">Notice</th>
                                        
                                        <th class="col-sm-4">Action</th>
                                       
                                    </tr>

                                </thead>
                                

                            </table>



                        </div> -->
                       <div class="container-fluid">
                    <!-- /row -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                                <div class="table-responsive"> 
                                    <div class="table-wrapper" style="padding: none !important;">
                                        <table class="table table-striped table-hover">
                                            <?php if(count($subjects))
                                            {
                                                $i = 1;
                                                $j = 1;?>
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1">S.No</th>
                                                    <th class="col-md-5">Subject Name</th>
                                                    <th class="col-md-6"> Subject's Video</th>
                                                   <!--  <th></th>
                                                    <th></th>
                                                    <th ></th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                
                                                foreach($subjects as $key2 => $subject){ ?> 
                                                    <tr>
                                                        <td class="col-md-1"><?php echo $i; ?></td>
                                                        <td class="col-md-5"><?php echo $subject['subject']; ?></td>
                                                        <?php if($subject['videoKey'])
                                                        {?>
                                                           
                                                        <td class="col-md-6">
                                                            <?php $count=1;
                                                        foreach ($subject['videoKey'] as $key => $value1) { 
                                                         
                                                         ?>
                                                            
                                                             <a href="#" class="js-video-button<?php echo $key2.$key;?>" id="<?php echo $value1['videoID'];?>" data-channel="video" data-video-url="<?php echo base_url() ?>uploads/videos/<?php echo $value1['src'];  ?>">
                                                                <img src="<?php echo base_url() ?>assets/img/video.png" alt="<?php echo $subject['subject'];?>" style="width: 50px; height: 50px;" data-toggle="tooltip" data-placement="top" title="<?php echo $subject['subject'];?>">
                                                            </a>
                                                            <a href="javascript:void(0)" style="color: red" class="videodelete" id="<?php echo $value1['videoID'];?>" data-toggle="tooltip" data-placement="top" title="Want To delete? <?php echo $subject['subject'];?>">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                            <script>
                                                                $(".js-video-button<?php echo $key2.$key;?>").modalVideo({
                                                                    youtube:{
                                                                        controls:0,
                                                                        nocookie: true
                                                                    }
                                                                });
                                                            </script>
                                                            <?php if($count==5 || $count==10 || $count==15)
                                                            { ?>

                                                                <BR>
                                                           <?php } ?>
                                                           <!-- <br> -->
                                                           <?php $count++; } }
                                                         ?>
                                                        </td>
                                                        

                                                        
                                                        <!-- <td>hello</td> -->
                                                    </tr>
                                                    <?php $i++; }
                                                
                                              

                                            } else
                                            { ?>
                                                <!-- <tr>
                                                    <td>
                                                         <p>No data Found</p>
                                                    </td>
                                                </tr> -->
                                                <div class="Noresuls">

                                               <h1>Sorry we couldn't find any matches</h1>

                                               <p>Maybe your search was too specific, please try searching with another term.</p>

                                               <img src="<?php echo base_url() ?>uploads/images/crying.png">

                                            </div>
                                             
                                           <?php }
                                            ?>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                               
                        </div>
                        <!-- ./Basic Table -->
                        </div>
                    </div>
                    <!-- /row -->
                </div>  
                       
                    <div id="pagination">
                        <ul class="tsc_pagination pull-right">

                        <!--Show pagination links -->
                            <div class="col-md-12 text-center">
                                <?php echo $pagination; ?>
                            </div>
                        </ul>
                    </div>
                </div>



                    <?php  } ?>
                        <?php 

                            if ( $usertype == "Student") { ?>
                            
                            <div id="hide-tab">

                            <table id = "noticeTableStudent" class="table table-striped table-bordered table-hover dataTable no-footer">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_title')?></th>

                                        <th class="col-sm-2"><?=$this->lang->line('notice_date')?></th>

                                        <th class="col-sm-4">Notice</th>
                                        
                                        <th class="col-sm-4">Action</th>                                      
                                    </tr>

                                </thead>
                             
                                <tdata>
                                    <?php if(count($notice)) {$i = 1; foreach($notice as $notices) { ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $notices->title; ?></td>
                                        <td><?php echo $notices->date; ?></td>
                                        <td><?php echo substr($notices->notice,0,20); ?></td>
                                        <td><?php echo btn_view('notice/view/'.$notices->noticeID, $this->lang->line('view')); ?></td>
                                    </tr>
                                        <?php $i++; }} ?>
                                </tdata>

                            </table>

                        </div>
                    
                       <?php } ?>
              
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        // alert(classesID);
            $.ajax({
                type: 'POST',
                url: "<?=base_url('videos/index')?>",
                data: "classesID=" + classesID,
                dataType: "html",
                success: function(data) {
                    // alert(data);
                     location.reload();
                }
            });
    });

</script>

<script type="text/javascript">

    $('#subCourseID').change(function() {
        $('#noticeTables').DataTable().state.clear();

        var subCourseID = $(this).val();

            $.ajax({
                type: 'POST',
                url: "<?=base_url('notice/index')?>",
                data: "subCourseID=" + subCourseID,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }

            });

    });

</script>

<script type="text/javascript">
    $('#yearSemesterID').change(function() {
        var yearSemesterID = $(this).val();
        $('#noticeTables').DataTable().state.clear();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('videos/index')?>",
            data: "yearSemesterID=" + yearSemesterID,
            dataType: "html",
            success: function(data) {
                 location.reload();
            }
        });
    });
    $(".videodelete").click(function(e){
        if(!confirm('Are you sure?')){
            e.preventDefault();
            return false;
        }
        else
        {
            ID = this.id;
            $.ajax({
                type: "POST",
                data: {'ID':ID},
                url: '<?php echo base_url(); ?>videos/deletevideos',
                success:function(data){
                    location.reload();
                }
            });
        }
    });
</script>

<script type="text/javascript">
    
    function ResetCourses(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('videos/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
        $('#noticeTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('videos/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){
    $('#noticeTables').DataTable().state.clear();

            $.ajax({
                type: 'POST',

                url: "<?=base_url('notice/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

</script>



