<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
  <!-- <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script> -->
  <!-- <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css"> -->
<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-video-camera"></i> Add Video </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("videos/index")?>">Video</a></li>

                <li class="active">Add Video </li>

            </ol>
        </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
        <div class="">
          <div class="card">
            <div class="card-body">
              <div class="col-sm-12">
                <div class="form-row">
                  <div class="form-group">
                <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Course</label >
                <div class="col-md-6">
                  <select name="classesID" required onchange="ajaxGet_subCourses($(this).val())" id="classesIDForVideo" class="form-control" value="<?php echo set_value('classesID'); ?>" >
                    <option value="">Select Course</option>
                      <?php foreach($classes as $courses){ 
                      if ($courses->classesID==$this->session->userdata('FilterVideoclassesID')) {
                           $selected =  "Selected";
                       }else{
                         $selected =  "";
                       }
                        ?>

                      <option value="<?php echo $courses->classesID ?>" <?php echo $selected ?>><?php echo $courses->classes ?></option>
                    <?php } ?>
                  </select> 
                </div>
              </div>
          </div>
          <!-- <div class="form-row">
            <div class="form-group">
             <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Sub Course</label >
             <div class="col-md-6">
     
                 <select id="subCourseID" class="form-control">
                    <option value="">Select</option>
                    <?php  foreach ($subCourses as $key => $value) {
                       if ($value->sub_coursesID==$this->session->userdata('FilterVideosubCourseID')) {
                           $selected =  "Selected";
                       }else{
                         $selected =  "";
                       }
                       ?><?php echo set_value('subjectvideo'); ?>
                    <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                    <?php } ?>
                 </select>
              </div>
            </div>
        </div> -->

    
          <div class="form-row">
            <div class="form-group">
             <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Year or Semester</label >
             <div class="col-md-6">
                 <select class='form-control' name='yearsOrSemester' onchange='ajaxGet_subCourses($(this).val())' id='yos_filter' value=''> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterVideoyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterVideoyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
              </div>
            </div>
        </div>



        <div class="form-row">
            <div class="form-group">
              <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Subject</label >
              <div class="col-md-6">
                <select class='form-control' name='subjectvideo' id='subjectIDforvideo' value=''> 
                  <option>Select</option>      
                  <?php echo $this->session->userdata('FilterVideosubjectID'); ?>
                    <?php  foreach ($subject as $key => $value) {

                       if ($value->subjectID===$this->session->userdata('FilterVideosubjectID')) {
                           $selected =  "";
                       }else{
                         $selected =  "";
                       }
                       ?>
                    <option value="<?php echo $value->subjectID ?>" <?php echo $selected ?>><?php echo $value->subject ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <!-- <div class="form-group">
              <label for="courseName" style="margin-top: 6px" class="col-md-2 col-form-label">Unit</label>
              <div class="col-md-6">
                <select class='form-control' name='unitvideo' id='unitIDforvideo'> 
                  <option>Select</option>      
                    <?php $this->session->userdata('FilterVideoUnitID'); ?>
                   
                  </select>
                </div>
              </div> -->
            


                                  <!--  <?php 

                                        if(form_error('title')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="title" class="col-sm-2 control-label">

                                            <?=$this->lang->line("notice_title")?>

                                        </label>

                                        <div class="col-sm-6">

                                            
                                            <input type="" class="form-control" id="title1" name="title" value="<?=set_value('title')?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('title'); ?>

                                        </span>

                                    </div> -->



                       <!--          <?php 
                                    if(form_error('type_notice')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="title" class="col-sm-2 control-label">

                                        Show Video For

                                        </label>

                                        <div class="col-sm-9">

                                    <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type1" value="course" checked=""><label for="notice_type1">Only Course</label>
                                    </div>
                                        <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type2" value="sub_course" checked=""><label for="notice_type2">Only Sub Course</label>
                                    </div>
                                        <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type3" value="with_year_semester" checked=""><label for="notice_type3">With Year/Semester</label>
                                    </div>
                                          <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type4" value="only_year_semester" checked=""><label for="notice_type4">Only Year/Semester</label>
                                    </div>

                                        </div>

                                        <span class="col-sm-1 control-label">

                                            <?php echo form_error('type_notice'); ?>

                                        </span>

                                    </div> -->
  
                                   

                                    <?php 

                                        if(form_error('notice')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>



                    


                              
<!-- <input id="thefiles" type="file" style="display:none;" name="files" class="ff_fileupload_hidden" accept=".jpg, .png, image/jpeg, image/png,.mp4,video/mp4" multiple> -->
        <!--  <div class="form-group">
              <label for="courseName" style="margin-top: 12px" class="col-md-2 col-form-label">Upload Video</label>
              <div class="col-md-6">
                 <input type="file" name="files"/>
                 <input type="submit" class="upload-image" value="upload" />
              </div>
         </div> -->
        </div>
        <form action="<?php echo base_url() ?>videos/UploadVideos" role="form" method="post" enctype="multipart/form-data" class="dropzone" id="image-upload">
          
    


                                    <div class="form-group">

                                        <div class="col-sm-offset-2 col-sm-8">

                                            <!-- <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" > -->

                                        </div>

                                    </div>

                                </form>
                                <form action="<?php echo base_url('videos');?>" method="post" style="text-align: center;padding: 15px">
                                <button type="submit" class="btn btn-success btn-md mrg for_margR">Submit</button>
                                </form>

                            </div>

                        </div>

                   </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>

<script type="text/javascript">

jq('#date').datepicker();

jq('#notice').jqte();

</script>
<script type="text/javascript">
    function ajaxGet_subCoursesExcel(id){
      
 $('.showLoaderSubcour').show();
    $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Get_subCourses')?>",
        data:{"id":id},
        success: function(response) {
            console.log(response);
            $("#subCourseIDExcel").html(response);
            $('.showLoaderSubcour').hide();
        }
            });
}
</script>

<script type="text/javascript">
    $(document).ready(function(){ 
    $("input[name=cod]").change(function() {
        var test = $(this).val();
        if (test==1) {
    $("#openContainer").show();
        }else{
      $("#openContainer").hide();  
        }
    
    }); 
});
</script>

<script type="text/javascript">
  function GetEntryTypeData(){
       var  classesID  = $("#classesIDExcel").val();

       var  sub_CourseID  = $("#sub_CourseIDExcel").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>

<script type="text/javascript">
    $('#yearSemesterID').change(function() {
        var yearSemesterID = $(this).val();
        
        $.ajax({
            type: 'POST',
            url: "<?=base_url('notice/index')?>",
            data: "yearSemesterID=" + yearSemesterID,
            dataType: "html",
            success: function(data) {
                 location.reload();
            }
        });
    });
</script>

<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>




<script type="text/javascript">
   $('#classesIDForVideo').change(function() {
       var classesID = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "classesID=" + classesID,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#subCourseID').change(function() {
   
       var subCourseID = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "subCourseID=" + subCourseID,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>

<script type="text/javascript">
   $('#yos_filter').change(function() {
   
       var yearSemesterID = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "yearSemesterID=" + yearSemesterID,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#subjectIDforvideo').change(function() {
   
       var subjectIDforvideo = $(this).val();

    var option_user_selection = $("#subjectIDforvideo option:selected").text();;

   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: {subjectIDforvideo:subjectIDforvideo,option_user_selection:option_user_selection},
   
               dataType: "html",
   
               success: function(data) {
   
                    // location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#professorID').change(function() {
   
       var professorIDvideo = $(this).val();
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "professorIDvideo=" + professorIDvideo,
   
               dataType: "html",
   
               success: function(data) {
   
                    location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
   $('#unitIDforvideo').change(function() {   
       var unitIDforvideo = $(this).val();   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('videos/index')?>",
   
               data: "unitIDforvideo=" + unitIDforvideo,
   
               dataType: "html",
   
               success: function(data) {
   
                    // location.reload();
   
               }
   
           });
   
   });
   
</script>
<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesIDForVideo").val();

       var  sub_CourseID  = $("#subCourseID").val();
       var  semester_idSelect  = $("#yos_filter").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function ajaxGet_subject($a)
  {
    var subjectID=$("#subjectIDforvideo").val();
    $.ajax({
     type: "POST",
     url:"<?=base_url('ajaxControllerQuestion/Getunits')?>",
     data:{"subjectID":subjectID},
     success: function(response)
     {
      // alert(response);
        $("#unitIDforvideo").html(response);
        // alert(response);
     }
    });
  }
</script>
<!-- <script type="text/javascript">
  Dropzone.options.imageUpload = {
        maxFilesize:1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif"
    };
</script> -->