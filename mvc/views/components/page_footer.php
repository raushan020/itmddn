<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
    <!-- jQUERY -->
    <script  src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>

    <!-- BOOTSTRAP 4 -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- chat script start  -->
<script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-app.js"></script>   
<script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-database.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/etslabs/css/chatstyle.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/etslabs/css/chatnormalize.css');?>">
<link href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyA0LKosISwOf8j38IgviFxCDPqeVgHBK1k",
    authDomain: "chatapp-1d351.firebaseapp.com",
    databaseURL: "https://chatapp-1d351.firebaseio.com",
    projectId: "chatapp-1d351",
    storageBucket: "chatapp-1d351.appspot.com",
    messagingSenderId: "209743411223",
    appId: "1:209743411223:web:1467455c1a561fdb250452"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  firebase.database().ref("messages").on("child_removed", function (snapshot) {
    document.getElementById("message-" + snapshot.key).innerHTML = "This message has been deleted";
  });

  function deleteMessage(self) {
    var messageId = self.getAttribute("data-id");
    firebase.database().ref("messages").child(messageId).remove();
  }


  function sendMessage(ids) {
    var message = document.getElementById("message").value;
    var myName="<?php echo $this->session->userdata('name') ?>";
    var sid='<?php echo $this->session->userdata('loginuserID') ?>';
    var suertype='<?php echo $this->session->userdata('usertype') ?>';
    firebase.database().ref("messages").push().set({
      "message": message,
      "sender": myName,
      "rid":ids,
      "sid":sid,
      "suertype":suertype
    });
    return false;
  }
</script>

<style>
  figure.avatar {
    bottom: 0px !important;
  }
  .btn-delete {
    background: red;
    color: white;
    border: none;
    /*margin-left: 10px;*/
    border-radius: 5px;
  }
</style>
<!-- chat script end -->
    <script>
        function getScreen() {
            var caption = $('#caption-input').val();
            $("#caption-text").html(caption);
            $("#panel").show();
            html2canvas(document.body, {
                dpi: 192,
                onrendered: function(canvas) {
                    $("#blank").attr('href', canvas.toDataURL("<?php echo base_url() ?>images/png"));
                    $("#blank").attr('download', caption + '.png');
                    $("#blank")[0].click();
                }
            });
        }
    </script>

<script type="text/javascript">
    $(document).ready(function() {
      $(".add-more").click(function(){ 
        var html = $(".copy").html();
        $(".after-add-more").after(html);
      });
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
      $(".add-topic").click(function(){    
          var html = $(".copy-topic").html();
          $(".after-add-topic").after(html);
      });

      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
</script>

    <style>
        /*.bg {
            background-image: url("");
            height: 100%;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }*/
        .caption {
            font-family: 'Times New Roman';
            text-transform: uppercase;
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            color: #000;
        }
        .caption span.border {
            background-color: #111;
            color: #fff;
            padding: 18px;
            font-size: 25px;
        }
    </style>

<style type="text/css">

  #demo {

    -webkit-transition: width 1s ease;

    -moz-transition: width 1s ease;

    -o-transition: width 1s ease;

    transition: width 1s ease;

    display: inline-block;

    overflow: hidden;

    white-space: nowrap;

    background: yellow;

    vertical-align: middle;

    line-height: 30px;

    height: 30px;

    position:fixed;

    bottom:72px;

    left:50px;

    width: 0px;

    z-index:909090900;

}

#demo.in {

    width: 220px;

}

.feedback_cornae{

    z-index: 999999;

    position: fixed;

    top: 395px;

    right: -68px;



        -webkit-transform: rotate(90deg); /* Safari and Chrome */

    -moz-transform: rotate(90deg);   /* Firefox */

    -ms-transform: rotate(90deg);   /* IE 9 */

    -o-transform: rotate(90deg);   /* Opera */

    transform: rotate(90deg);

   

}

#feedback_button{

  width: 165px;

 /* width: 10ox;*/

}

.form-feedback{

  padding: 0px;

}
</style>
<style type="text/css">
  #form
{
  position: fixed;
  bottom: 0px;
  right:0%;
  width:20%;
  border:2px solid green;
  padding:0px;
  background: #fff;
}
#form p
{
  margin:0px;
  background-color:#283e4a;
 /* text-align:center;*/
  color:#CED8F6;
  padding:0px;
  font-size:16px;
  /*font-style:oblique;*/
  cursor:pointer;
  width: 100%;
}
.d1 ul li{
  border-bottom: 1px solid #efefef;
    padding: 10px 0px 0px 5px;
    font-size: 1.4rem;
    color: #283e4a !important;

}
.d1 span{
  padding: 21px;
  color: green;
  font-size: 10px;
}
.chatbox-holder {
  position: fixed;
  right: 0;
  bottom: 0;
  display: flex;
}

.chatbox {
  width: 300px;
  height: 320px;
  margin: 0 310px 0 0;
  position: relative;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, .2);
  display: flex;
  flex-flow: column;
  border-radius: 10px 10px 0 0;
  background: white;
  bottom: 0;
  transition: .1s ease-out;
}

.chatbox-top {
  position: relative;
  display: flex;
  padding: 10px 0;
  border-radius: 10px 10px 0 0;
  background: rgba(0, 0, 0, .05);
}

.chatbox-icons {
  padding: 0 10px 0 0;
  display: flex;
  position: relative;
}

.chatbox-icons .fa {
  background: rgba(220, 0, 0, .6);
  padding: 3px 5px;
  margin: 0 0 0 3px;
  color: white;
  border-radius: 0 5px 0 5px;
  transition: 0.3s;
}

.chatbox-icons .fa:hover {
  border-radius: 5px 0 5px 0;
  background: rgba(220, 0, 0, 1);
}

.chatbox-icons a, .chatbox-icons a:link, .chatbox-icons a:visited {
  color: white;
}

.chat-partner-name, .chat-group-name {
  flex: 1;
  padding: 0 0 0 58px;
  font-size: 15px;
  font-weight: bold;
  color: #30649c;
  text-shadow: 1px 1px 0 white;
  transition: .1s ease-out;
}

.status {
  width: 12px;
  height: 12px;
  border-radius: 50%;
  display: inline-block;
  box-shadow: inset 0 0 3px 0 rgba(0, 0, 0, 0.2);
  border: 1px solid rgba(0, 0, 0, 0.15);
  background: #cacaca;
  margin: 0 3px 0 0;
}

.online {
  background: #b7fb00;
}

.away {
  background: #ffae00;
}

.donot-disturb {
  background: #ff4343;
}

.chatbox-avatar {
  width: 40px;
  height: 40px;
  overflow: hidden;
  border-radius: 50%;
  background: white;
  padding: 3px;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, .2);
  position: absolute;
  transition: .1s ease-out;
  bottom: 0;
  left: 6px;
}

.chatbox-avatar img {
  width: 100%;
  height: 100%;
  border-radius: 50%;
}

.chat-messages {
  border-top: 1px solid rgba(0, 0, 0, .05);
  padding: 10px;
  overflow: auto;
  display: flex;
  flex-flow: row wrap;
  align-content: flex-start;
  flex: 1;
}

.message-box-holder {
  width: 100%;
  margin: 0 0 15px;
  display: flex;
  flex-flow: column;
  align-items: flex-end;
}

.message-sender {
  font-size: 12px;
  margin: 0 0 15px;
  color: #30649c;
  align-self: flex-start;
}

.message-sender a, .message-sender a:link, .message-sender a:visited, .chat-partner-name a, .chat-partner-name a:link, .chat-partner-name a:visited {
  color: #30649c;
  text-decoration: none;
}

.message-box {
  padding: 6px 10px;
  border-radius: 0 6px 0 6px;
  position: relative;
  background: rgba(100, 170, 0, .1);
  border: 2px solid rgba(100, 170, 0, .1);
  color: #6c6c6c;
  font-size: 12px;
}

.message-box:after {
  content: "";
  position: absolute;
  border: 10px solid transparent;
  border-top: 10px solid rgba(100, 170, 0, .2);
  border-right: none;
  bottom: -22px;
  right: 10px;
}

.message-partner {
  background: rgba(0, 114, 135, .1);
  border: 2px solid rgba(0, 114, 135, .1);
  align-self: flex-start;
  margin: 0 0 15px;
}

.message-partner:after {
  right: auto;
  bottom: auto;
  top: -22px;
  left: 9px;
  border: 10px solid transparent;
  border-bottom: 10px solid rgba(0, 114, 135, .2);
  border-left: none;
}

.chat-input-holder {
  display: flex;
  border-top: 1px solid rgba(0, 0, 0, .1);
}

.chat-input {
  resize: none;
  padding: 5px 10px;
  height: 40px;
  font-family: 'Lato', sans-serif;
  font-size: 14px;
  color: #999999;
  flex: 1;
  border: none;
  background: rgba(0, 0, 0, .05);
   border-bottom: 1px solid rgba(0, 0, 0, .05);
}

.chat-input:focus, .message-send:focus {
  outline: none;
}

.message-send::-moz-focus-inner {
  border:0;
  padding:0;
}

.message-send {
  -webkit-appearance: none;
  background: #9cc900;
  background: -moz-linear-gradient(180deg, #00d8ff, #00b5d6);
  background: -webkit-linear-gradient(180deg, #00d8ff, #00b5d6);
  background: -o-linear-gradient(180deg, #00d8ff, #00b5d6);
  background: -ms-linear-gradient(180deg, #00d8ff, #00b5d6);
  background: linear-gradient(180deg, #00d8ff, #00b5d6);
  color: white;
  font-size: 12px;
  padding: 0 15px;
  border: none;
  text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.3);
}

.attachment-panel {
  padding: 3px 10px;
  text-align: right;
}

.attachment-panel a, .attachment-panel a:link, .attachment-panel a:visited {
  margin: 0 0 0 7px;
  text-decoration: none;
  color: rgba(0, 0, 0, 0.5);
}

.chatbox-min {
  bottom: -362px;
}

.chatbox-min .chatbox-avatar {
  width: 60px;
  height: 60px;
}

.chatbox-min .chat-partner-name, .chatbox-min .chat-group-name {
  padding: 0 0 0 75px;
}

.settings-popup {
  background: white;
  border-radius: 20px/10px;
  box-shadow: 0 3px 5px 0 rgba(0, 0, 0, .2);
  font-size: 13px;
  opacity: 0;
  padding: 10px 0;
  position: absolute;
  right: 0;
  text-align: left;
  top: 33px;
  transition: .15s;
  transform: scale(1, 0);
  transform-origin: 50% 0;
  width: 120px;
  z-index: 2;
  border-top: 1px solid rgba(0, 0, 0, .2);
  border-bottom: 2px solid rgba(0, 0, 0, .3);
}

.settings-popup:after, .settings-popup:before {
  border: 7px solid transparent;
  border-bottom: 7px solid white;
  border-top: none; 
  content: "";
  position: absolute;
  left: 45px;
  top: -10px;
  border-top: 3px solid rgba(0, 0, 0, .2);
}

.settings-popup:before {
  border-bottom: 7px solid rgba(0, 0, 0, .25);
  top: -11px;
}

.settings-popup:after {
  border-top-color: transparent;
}

#chkSettings {
  display: none;
}

#chkSettings:checked + .settings-popup {
  opacity: 1;
  transform: scale(1, 1);
}

.settings-popup ul li a, .settings-popup ul li a:link, .settings-popup ul li a:visited {
  color: #999;
  text-decoration: none;
  display: block;
  padding: 5px 10px;
}

.settings-popup ul li a:hover {
  background: rgba(0, 0, 0, .05);
}</style>
  </style>



<?php $usertype = $this->session->userdata("usertype");

if ( $usertype== "Student") {

?><div class="container" id='container'>

  <div id="feedback">

    <div id="feedback-form" style='display:none;' class="col-xs-4 col-md-4 panel panel-default">

      <form method="post" class="form panel-body" role="form" enctype="multipart/form-data">

        <div class="form-group">

          <select class="form-control" name="category" id="category">
            <option value="">Select category</option>
            <option value="Classroom Related">Classroom Related</option>
            <option value="Professor Related">Professor Related</option>
            <option value="Exam Related">Exam Related</option>
            <option value="Document Related">Document Related</option>
            <option value="Paper Related">Paper Related</option>
            <option value="Subject Related">Subject Related</option>
            <option value="Other">Other</option>
          </select>

        </div>

        <div class="form-group">

          <textarea class="form-control" name="message_feedback" id="message_feedback" required placeholder="Please write your feedback here..." rows="5" value="<?=set_value('message_feedback')?>" required="required"></textarea>

        </div>

        <div class="form-group">

          <input class="form-control" name="email" id="email_feedback"  autofocus placeholder="Enter your E-mail" type="email" /><br>
          <button class="btn btn-primary pull-left" style="width: 56px;" id="" onclick="feedbackSubmit()" type="submit">Send</button>

          <div>&nbsp;</div>
           

       </div>
      </form>

    </div>
    <div id="feedback-tab"><img class="feedback-tab" src="<?php echo base_url() ?>assets/img/feedback.PNG"></div>


  </div>

  </div>

<?php 

}

?>

<footer class="footer"> Powered by : <a href="https://e-campus.in/" target="_blank">e-Campus (Edge Technosoft Private Limited)</a> 
<!-- 
</div>
</footer> -->
<?php if($usertype=="Admins"  || $usertype=="Students" ) { ?>
<div id="form">
    <p id="feedback_button"><img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>" style="height: 25px;border-radius: 50%;
    border: 1px solid #fff;margin: 10px 6px 13px 15px;"> Messaging</p>
    <div class="d1">
        <input type="text" name="search_text" id="search_text" placeholder="Search message" class="form-control" / style="margin-bottom: -14px;">
   <br />
   <div id="result"></div>
   <?php if($showalldatarequest)
   { ?>
    
      <h5>you have one request <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h5>
      <ul>
        <?php foreach ($showalldatarequest as $key => $value)
        { ?>
          <a href="#" onclick="chatmsg('<?php echo $value->status; ?>','<?php echo $value->SenderID; ?>','<?php echo $value->senderusertype; ?>')"><li><?php echo $value->sendername; ?> <span><i class="fa fa-circle"></i></span>
          </li>

      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if($showalldatarequestaccept || $showalldatasenderaccept){ ?>
    <div class="d1" style="height: 278px;
    overflow-y: scroll;">
      <ul>
   <?php if($showalldatarequestaccept)
   { 
    foreach ($showalldatarequestaccept as $key => $value)
    { ?>
      <a href="#" onclick="chatmsg('<?php echo $value->status; ?>','<?php echo $value->SenderID; ?>','<?php echo $value->senderusertype; ?>','<?php echo $value->sendername; ?>')"><li><img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>" style="height:20px;border-radius: 50%;
    border: 1px solid #000;"> <?php echo $value->sendername; ?> <span><i class="fa fa-circle"></i></span><br>
        <span style="color: #283e4a;">last chat message here</span>
      </li>
        
      </a>
    <?php } 
   } ?>
  <?php if($showalldatasenderaccept)
  {
    foreach ($showalldatasenderaccept as $key => $value)
    { ?>
      <a href="#" onclick="chatmsg('<?php echo $value->status; ?>','<?php echo $value->requestID; ?>','<?php echo $value->requestusertype; ?>','<?php echo $value->rquestname; ?>')">

        <li><img alt="" src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>" style="height:20px;border-radius: 50%;
    border: 1px solid #000;"> <?php echo $value->rquestname; ?> <span><i class="fa fa-circle"></i></span><br>
        <span style="color: #283e4a;">last chat message here</span>
      </li>
        </a>
      </a>
    <?php  }
  } ?> 
  </ul>
    </div>
  <?php } ?>
  <!-- <div class="d1">
    <ul>
        <a href="#" onclick="chatmsg()"><li>Raushan kumar <span><i class="fa fa-circle"></i></span></li></a>
    </ul>
  </div> -->
      
  
  </div>

  <div class="chatbox-holder chatboxid">
  <div class="chatbox">
    <div class="chatbox-top">
      <div class="chatbox-avatar">
        <a target=""><img src="<?=base_url("uploads/images/".$this->session->userdata('photo'));?>"></a>
      </div>
      <div class="chat-partner-name">
        <span class="status online"></span>
        <a target="_blank" href="#"><label id="chatname"></label></a>
      </div>
      <div class="chatbox-icons">
        <a href="javascript:void(0);"><i class="fa fa-minus"></i></a>
        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>       
      </div>      
    </div>
    
   
       <!-- <div class="chat-messages">
       <div class="message-box-holder">
        <div class="message-box">
          Hello
        </div>
      </div>
      
      <div class="message-box-holder">
       
        <div class="message-box message-partner">
          Hi.
        </div>
      </div>
    </div> -->
    <!-- <div class="chat-input-holder">
      <textarea class="chat-input"></textarea>
      <input type="submit" value="Send" class="message-send" />
    </div> -->
    <!-- <div class="attachment-panel">
      <a href="#" class="fa fa-thumbs-up"></a>
      <a href="#" class="fa fa-camera"></a>
      <a href="#" class="fa fa-image"></a>
    </div> -->
    
 <div class="chat-messages">
  <div class="message-box-holder">
    <div class="messages-content"></div>
    <!-- <div class="messages-content message-partner"></div> -->
  </div>
</div>
<div class="chat-input-holder">
    <textarea type="text" class="message-input chat-input" id="message" placeholder="Type message..."></textarea>
    <input type="hidden" name="ids" id="ids">
    <input type="hidden" name="rname">
    <input type="submit" class="message-submit message-send" value="Send" />
  </div>
<div class="attachment-panel">
      <a href="#" class="fa fa-thumbs-up"></a>
      <a href="#" class="fa fa-camera"></a>
      <a href="#" class="fa fa-image"></a>
    </div>
    <!-- <div class="attachment-panel">
      <a href="#" class="fa fa-thumbs-up"></a>
      <a href="#" class="fa fa-camera"></a>
      <a href="#" class="fa fa-video-camera"></a>
      <a href="#" class="fa fa-image"></a>
      <a href="#" class="fa fa-paperclip"></a>
      <a href="#" class="fa fa-link"></a>
      <a href="#" class="fa fa-trash-o"></a>
      <a href="#" class="fa fa-search"></a>
    </div> -->
  </div>
<?php } ?>
</div>
</footer>


</div>  
<!-- chat script start -->
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.3/jquery.mCustomScrollbar.concat.min.js'></script> -->

        <!-- <script src="js/index.js?v=<?= time(); ?>"></script> -->
<script type="text/javascript">

function updateScrollbar() {
  $messages.update();
}

function setDate(){
  d = new Date()
  if (m != d.getMinutes()) {
    m = d.getMinutes();
    $('<div class="timestamp">' + d.getHours() + ':' + m + '</div>').appendTo($('.messages-content'));
  }
}

function insertMessage(ids) {
  msg = $('.message-input').val();
  if ($.trim(msg) == '') {
    return false;
  }

  sendMessage(ids);
}

$('.message-submit').click(function() {
  var ids=$('#ids').val();
  insertMessage(ids);
});

$(window).on('keydown', function(e) {
  if (e.which == 13) {
    var ids=$('#ids').val();
    insertMessage(ids);
    return false;
  }
});
</script>
<!-- chat script end -->
<style type="text/css">
.chatboxid{
	display:none;
}	
</style>
<script type="text/javascript">

$(document).ready(function(){
  $('.d1').slideUp('fast');
  $("#feedback_button").click(function(){
    form();
  });
});
  
function form()
{
  $(".d1").slideToggle('fast');
}
</script>
<script type="text/javascript">
  function chatmsg(status,id,usertype,name)
  {
    $('.messages-content').empty();
    $('.chatboxid').show();
    $('#chatname').html(name);
    $('#ids').val(id);
     if(status == 0)
     {
        $('<div class="chat-messages message-personal"><div class="message-box message-partner"><div id="message"><button class="btn-delete" id="'+usertype+'" data-id="'+id+'" onclick="acceptrequest(this);">Accept Request</button></div></div></div>').appendTo($('.messages-content')).addClass('new');
     }
     else
     {
      var $messages = $('.messages-content'),
      d, h, m,
      i = 0;

      var myName = "<?php echo $this->session->userdata('name') ?>";
      var senderid='<?php echo $this->session->userdata('loginuserID') ?>';
        // myName = 'sudhakar';
        // alert(myName);
        // $messages.mCustomScrollbar();
      // alert(id);
        firebase.database().ref("messages").on("child_added", function (snapshot) {
          if (snapshot.val().sid == senderid && snapshot.val().rid == id)
          {
            $('<div class="message-box-holder"><div class="message-box message-partner"><div id="message-' + snapshot.key + '">' + snapshot.val().message + '<span style="float:right;"><button class="btn-delete" data-id="' + snapshot.key + '" onclick="deleteMessage(this);"><i class="fa fa-trash-o"></i></button></span></div></div></div>').appendTo($('.messages-content')).addClass('new');
            $('.message-input').val(null);
          }
          else
          {
            if (snapshot.val().rid == senderid && snapshot.val().sid == id)
            {
              $('<div class="message-box-holder message-box"><div id="message-' + snapshot.key + '">'+ snapshot.val().message + '</div></div>').appendTo($('.messages-content')).addClass('new');
            }
          }
          
          setDate();
          updateScrollbar();
        });
       }
  }
</script>
<script>
$(function(){
  $('.fa-minus').click(function(){ 
  	$(this).closest('.chatbox').toggleClass('chatbox-min');
  });
  $('.fa-close').click(function(){

    $('.chatboxid').hide();
  });
});</script>

<script type="text/javascript">
  $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search.length == 3)
  {
   load_data(search);
  }
  
 });

  function load_data(query)
 {
  $.ajax({
   url:"<?php echo base_url(); ?>message/fetchsearchforrequest",
   method:"POST",
   data:{query:query},
   success:function(data){
    $('#result').html(data);
   }
  })
 }

 function requestsendss(countvalue)
 {
   var request=$('#requeststudentid'+countvalue).val();
   var name=$('#requestname'+countvalue).val();
   var requestusertype=$('#requestusertype'+countvalue).val();
   var sender='<?php echo $this->session->userdata('loginuserID') ?>';
   var senderusertype='<?php echo $this->session->userdata('usertype') ?>';
   var sendername='<?php echo $this->session->userdata('name') ?>';
   $.ajax({
   url:"<?php echo base_url(); ?>message/requestsendformsender",
   method:"POST",
   data:{request:request,sender:sender,requestusertype:requestusertype,senderusertype:senderusertype,name:name,sendername:sendername},
   success:function(data){
    alert("Your Request Sent");
   }
  })
 }

 function acceptrequest(self)
  {
    var Id = self.getAttribute("data-id");
    var usertype=self.getAttribute("id");
    $.ajax({
     url:"<?php echo base_url(); ?>message/acceptrequestforchat",
     method:"POST",
     data:{Id:Id,usertype:usertype},
     success:function(data){
      $('#message-personal').fadeOut(1000);
      chatmsg();
     }
    })
  }
</script>

    <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>

       <!-- Bootstrap Core JavaScript -->     

    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>



    <script src="<?php echo base_url('assets/etslabs/js/intro.js');?>"></script>



    <script type="text/javascript">

      var jq = $.noConflict();

    </script>



    <script src="<?php echo base_url('assets/etslabs/js/pignose.calendar.full.min.js');?>"></script>

<!-- Student Dashboard calender -->

<!-- <script>



html2canvas(document.html).then(function(canvas) {

    document.body.appendChild(canvas);

});



html2canvas(document.querySelector("#capture")).then(canvas => {

    document.body.appendChild(canvas)

});

</script> -->

<script type="text/javascript">
  const $canvas = $('body');
const $eyes = $('.eye');
const $rateInputs = $('.rate-input');


function vendorize(key, value) {
  const vendors = ['webkit', 'moz', 'ms', 'o', ''];
  var result = {};

  vendors.map(vendor => {
    const vKey = vendor ? '-' + vendor + '-' + key : key;

    result[vKey] = value;
  });

  return result;
}

//https://github.com/jfmdev/jqEye/blob/master/Source/jqeye.js
function circle_position(x, y, r) {
  // Circle: x^2 + y^2 = r^2
  var res = { x: x, y: y };
  if (x * x + y * y > r * r) {
    if (x !== 0) {
      var m = y / x;
      res.x = Math.sqrt(r * r / (m * m + 1));
      res.x = x > 0 ? res.x : -res.x;
      res.y = Math.abs(m * res.x);
      res.y = y > 0 ? res.y : -res.y;
    } else {
      res.y = y > 0 ? r : -r;
    }
  }
  return res;
};

function findCenter(coords, sizeX, sizeY) {
  return {
    x: coords.left + sizeX / 2,
    y: coords.top + sizeY / 2 };

}


function deltaVal(val, targetVal) {
  const delta = Math.min(100.0, ts - prevTs);
  const P = 0.001 * delta;

  return val + P * (targetVal - val);
}


function changeEyesPosition(px, py) {
  function changePosition() {
    const $t = $(this);
    const $pupil = $t.find('.pupil');
    const t_w = $t.width();
    const t_h = $t.height();
    const t_o = $t.offset();
    const t_p = $t.position();
    const abs_center = findCenter(t_o, t_w, t_h);
    const pos_x = px - abs_center.x + $(window).scrollLeft();
    const pos_y = py - abs_center.y + $(window).scrollTop();
    const cir = circle_position(pos_x, pos_y, t_w / 20);
    const styles = vendorize('transform', 'translateX(' + cir.x + 'px) translateY(' + cir.y + 'px)');

    $pupil.css(styles);
  }

  $eyes.each(changePosition);
}

function handleMouseMove(e) {
  const px = e.pageX,
  py = e.pageY;

  changeEyesPosition(px, py);
}

$canvas.on('mousemove', handleMouseMove);


function getFace($element) {
  return $element.parent('.face-wrapper').find('.face');
}


function handleFaceHover($face) {
  const $hint = $('.faces-hint');
  const hintText = $face.attr('data-hint') || $hint.attr('data-default-hint');
  $hint.text(hintText);
}


function handleFacesHover(e) {
  const $face = getFace($(e.target));

  handleFaceHover($face);
}

$('.feedback-faces').on('mousemove', handleFacesHover);



function handleFeedbackTitleHover(e) {
  const isHover = e.type === 'mouseenter';
  $(this).parent().toggleClass('title-hovered', isHover);
}

$('.feedback-title').on('mouseenter mouseleave', handleFeedbackTitleHover);


function handleFeedbackToggle() {
  const $this = $(this),
  $parent = $this.parent();

  $parent.toggleClass('at-bottom');

  $parent.find('.face-wrapper').each(function (index) {
    setTimeout(function (face) {
      face.toggleClass('slide-out-y-alt', $parent.hasClass('at-bottom'));
    }, (index - 1) * 40, $(this));
  });
}
$('.feedback-title').on('click', handleFeedbackToggle);



function handleRateInputChange() {
  const rating = parseInt($(this).val());

  getFace($rateInputs).addClass('grayscale');
  getFace($(this)).removeClass('grayscale');
  postRating(rating);
}

$rateInputs.on('change', handleRateInputChange);



//Firebase stuff

function setCounter(stats) {
  const $counters = $('.face-counter');

  function setTitle($counter, size) {
    var titleType = '',
    titlePrefix = '';
    if (size === 0) {
      titleType = 'none';
    } else if (size === 1) {
      titleType = 'one';
    } else {
      titleType = 'many';
      titlePrefix = `${size} `;
    }

    $counter.attr({
      'title': titlePrefix + $counter.attr(`data-title-${titleType}`) });

  }

  $counters.each(index => {
    const $counter = $counters.eq(index),
    size = stats[index] || 0;

    $counter.text(size);
    setTitle($counter, size);
    $counter.removeClass('invisible');
  });

}


function getTotalRating() {
  var stats = {};
  firebase.database().ref('votes').limitToLast(1000).once('value', snapshot => {
    snapshot.forEach(snap => {
      const val = snap.val();
      var voteStat = stats[val.vote];

      voteStat = voteStat ? voteStat + 1 : 1;
      stats[val.vote] = voteStat;

    });
    setCounter(stats);
  });
}


function postRating(rating) {
  const currentUser = firebase.auth().currentUser;

  if (currentUser) {
    const uid = currentUser.uid;
    const data = {
      vote: rating,
      time: new Date().getTime() };


    firebase.database().ref(`votes/${uid}`).set(data).then(getTotalRating);
  }
}


function loginFB() {
  console.log('login');
  firebase.auth().signInAnonymously().then(user => {
    console.log(firebase.auth().currentUser.uid);
  }).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    if (errorCode === 'auth/operation-not-allowed') {
      alert('You must enable Anonymous auth in the Firebase Console.');
    } else {
      console.error(error);
    }
  });
}

function initFB() {
  var config = {
    apiKey: "AIzaSyA7-zbUFMXGItgDwVyfS0IVlqjCytQxQ8k",
    authDomain: "greatest-ever.firebaseapp.com",
    databaseURL: "https://greatest-ever.firebaseio.com",
    projectId: "greatest-ever",
    storageBucket: "greatest-ever.appspot.com",
    messagingSenderId: "784422044422" };

  firebase.initializeApp(config);

  if (!firebase.auth().currentUser) {
    loginFB();
  }

}

initFB();
</script>

 <script>

  function screenshot(){

    html2canvas(document.body).then(function(canvas) {

      document.body.appendChild(canvas);

   });

  }

  </script>



<script>

  html2canvas(document.getElementById('container')).then(function(canvas) {

    document.getElementById("image").src= canvas.toDataURL();

    var="copyContext.drawImage(img, 0, 0);"

});

</script>

 <!--  <script>

    window.html2canvas([$('body')[0]], {

              onrendered: function(canvas) {

                var extra_canvas = document.createElement("canvas");

                extra_canvas.setAttribute('width',70);

                extra_canvas.setAttribute('height',70);

                var ctx = extra_canvas.getContext('2d');

                ctx.drawImage(canvas,0,0,canvas.width, canvas.height,0,0,70,70);

                var dataURL = extra_canvas.toDataURL();

                var img = $(document.createElement('img'));

                img.attr('src', dataURL);

                // insert the thumbnail at the top of the page

                $('body').prepend(img);

              },

            });



  </script> -->

<!-- feedback script -->

  <?php if ( $usertype== "Student") { ?>

    <script type="text/javascript">    

        $(document).ready(function () {

            $('#test').feedBackBox();

        });

    </script>

  <?php } ?>

    <!-- calender script start -->

<script type="text/javascript">

    jq(function() {

    jq('#mycalendars').pignoseCalendar();

});

</script>

<!-- end Student Dashboard calender -->

<!-- for exam use  -->

<script type="text/javascript">

   jq(document).ready(function(){ 

    jq("input[name=cod]").change(function() {

        var test = jq(this).val();

        if (test==1) {

    jq("#openContainer").show();

        }else{

      jq("#openContainer").hide();  

        }
    }); 

});

</script>



<!-- main calender  use in exam and add student--> 

<script src="<?php echo base_url() ?>assets/inilabs/EtDesigns/js/jquery.datetimepicker.full.min.js"></script>

<script>

  jq(document).ready(function () {

      'use strict';

      jq('.datetimepicker_quiz_start').datetimepicker();

  });

</script>

<script>

  jq(document).ready(function () {

  'use strict';

  jq('.datetimepicker_quiz_data').datetimepicker();

  });

</script>

<script type="text/javascript">

  function subject_date(){

  jq('.datetimepicker_quiz_data').datetimepicker();



  }

</script>

<!-- main calender--> 

<script src="<?php echo base_url() ?>assets/etslabs/js/croppie.js"></script>

<script>  

jq(document).ready(function(){

  var updloadImageByadmin = $('#updloadImageByadmin').val();

  if (updloadImageByadmin=='undefined') {

    var segment = '';

  }else{

     var segment = updloadImageByadmin;

  }

  $image_crop = jq('#image_demo').croppie({

    enableExif: true,

    viewport: {

      width:200,

      height:200,

      type:'square' //circle

    },

    boundary:{

      width:300,

      height:300

    }

  });

  jq('#upload_image').on('change', function(){

    var reader = new FileReader();

    reader.onload = function (event) {

      $image_crop.croppie('bind', {

        url: event.target.result

      }).then(function(){

       

      });

    }

    reader.readAsDataURL(this.files[0]);

    jq('#uploadimageModal').modal('show');

  });

  jq('.crop_image').click(function(event){

    $image_crop.croppie('result', {

      type: 'canvas',

      size: 'viewport'

    }).then(function(response){

      $.ajax({

        url:"<?php echo base_url() ?>profile/uploadPic/"+segment,

        type: "POST",

        data:{"image": response},

        success:function(data)

        {

          console.log(data);

          jq('#uploadimageModal').modal('hide');

          jq('#uploaded_image').html(data);

        }

      });

    })

  });

});  

</script>

<script>  

jq(document).ready(function(){

  var updloadImageByadmin = $('#updloadImageByadmin').val();

  if (updloadImageByadmin=='undefined') {

    var segment = '';

  }else{

     var segment = updloadImageByadmin;

  }

  $image_crop_student = jq('#image_student').croppie({

    enableExif: true,

    viewport: {

      width:200,

      height:200,

      type:'square' //circle

    },

    boundary:{

      width:300,

      height:300

    }

  });



  jq('#upload_image_student').on('change', function(){

    var reader = new FileReader();

    reader.onload = function (event) {

      $image_crop_student.croppie('bind', {

        url: event.target.result

      }).then(function(){

       

      });

    }

    reader.readAsDataURL(this.files[0]);

    jq('#uploadimageModalStudent').modal('show');

    jq('#imageUploader').hide();

  });



  jq('.crop_image_student').click(function(event){

     jq('#imageUploader').show();

    $image_crop_student.croppie('result', {

      type: 'canvas',

      size: 'viewport'

    }).then(function(response){

      $.ajax({

        url:"<?php echo base_url() ?>student/uploadPicByCrop/"+segment,

        type: "POST",

        data:{"image": response},

        success:function(data)

        {

          console.log(data);

          jq('#uploadimageModalStudent').modal('hide');

         jq('#uploaded_image_student').html(data);

         jq('#upload_images_mess').show();

        }

      });

    })

  });



});  

</script> 



<script type="text/javascript">

  jq("[data-toggle='toggle']").click(function() {

    var selector = $(this).data("target");

    jq(selector).toggleClass('in');

});

</script>

<script>  

jq(document).ready(function(){

  $image_crop2 = jq('#image_demo_setting').croppie({

    enableExif: true,

    viewport: {

      width:150,

      height:50,

      type:'square' //circle

    },

    boundary:{

      width:300,

      height:300

    }

  });


  jq('#upload_image_setting').on('change', function(){

    var reader = new FileReader();

    reader.onload = function (event) {

      $image_crop2.croppie('bind', {

        url: event.target.result

      }).then(function(){
       

      });

    }

    reader.readAsDataURL(this.files[0]);

    jq('#uploadimageModal_setting').modal('show');

  });

  jq('.crop_image_setting').click(function(event){

    $image_crop2.croppie('result', {

      type: 'canvas',

      size: 'viewport'

    }).then(function(response){

      $.ajax({

        url:"<?php echo base_url() ?>setting/uploadPic",

        type: "POST",

        data:{"image": response},

        success:function(data)

        {

          jq('#uploadimageModal_setting').modal('hide');

          jq('#uploaded_image_setting').html(data);

        }

      });

    })

  });

});  

</script>

<script type="text/javascript">
  
  function weather() {

  var location = document.getElementById("location");
  var apiKey = 'f536d4c3330c0a1391370d1443cee848'; // PLEASE SIGN UP FOR YOUR OWN API KEY
  var url = 'https://api.forecast.io/forecast/';

  navigator.geolocation.getCurrentPosition(success, error);

  function success(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;

    location.innerHTML = 'Latitude is ' + latitude + '° <br> Longitude is ' + longitude + '°';
alert(location.innerHTML);
     jq.getJSON(url + apiKey + "/" + latitude + "," + longitude + "?callback=?", function(data) {
      jq('#temp').html(data.currently.temperature + '° F');
      jq('#minutely').html(data.minutely.summary);
    });
  }

  function error() {
    location.innerHTML = "Unable to retrieve your location";
  }

  location.innerHTML = "Locating...";
}

weather();
</script>

  <script type="text/javascript">
    jq(document).ready(function () {
      'use strict';
      jq('.datepicker_quiz_data').datetimepicker({
        timepicker:false,
        format:'d-m-Y',
        formatDate:'Y/m/d',
      });
      
      jq('#startDate').datetimepicker({
        minDate: 0,
        multidate: true,
        timepicker:false,
        format:'d-m-Y',
        formatDate:'Y/m/d',
      });
      jq('#endDate').datetimepicker({
        minDate: 0,
        multidate: true,
        timepicker:false,
        format:'d-m-Y',
        formatDate:'Y/m/d',
      });
    });
  </script>

    <!-- Metis Menu Plugin JavaScript -->

    <script src="<?php echo base_url() ?>assets/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->

    <script src="<?php echo base_url() ?>assets/etslabs/js/kavach.min.js"></script>
    <script>
    $("[data-toggle='tooltip']").tooltip();
    </script>

<!-- <script>

    jq(".CalenderYear" ).datepicker({

    format: "yyyy",

    viewMode: "years", 

    minViewMode: "years"

    });

      jq('#dob').datepicker({ startView: 2 });

  </script> -->

  <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>
  <script>var $jq = jquery.noConflict(true);</script>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js'); ?>"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
  <script src="<?php echo base_url() ?>assets/etslabs/js/etsStudent.js"></script>
  <script src="<?php echo base_url() ?>assets/etslabs/js/studentFilter.js?updated=1234567890"></script>
  <script src="<?php echo base_url() ?>assets/etslabs/js/etsExam.js"></script>
  <script src="<?php echo base_url() ?>assets/etslabs/js/etsClasses.js"></script>
  <script src="<?php echo base_url() ?>assets/etslabs/js/etsSubject.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/AjaxDatatable.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/AjaxDatatableStudent.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {

  $('#subjectsTablesP').DataTable( {

        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

        buttons: [
        ]

    } );

} );

  </script>



  <script type="text/javascript">

    function passwordShow() {

        var x = document.getElementById("passwordStu");

        if (x.type === "password") {

            x.type = "text";

        } else {

            x.type = "password";

        }

    }

    </script>

    <script type="text/javascript">

      jq(".CalenderYear").datepicker({

        format: "yyyy",

        viewMode: "years", 

        minViewMode: "years"

    });

    </script>

    <script type="text/javascript">

      function  Set_sub_coureses_for_filter(subCourseID){

    alert(subCourseID);

    $.ajax({

    type:'POST',

    url:"<?php echo base_url() ?>student/set_session_of_subCourse",

    data:{subCourseID:subCourseID},

    success:function(){

    }

    }); 

      }

    </script>

  <script src="<?php echo base_url('assets/inilabs/intlTelInput.js') ?>"></script>

  <script>

    var telInput = $("#phonecountry"),

  errorMsg = $("#error-msg"),

  validMsg = $("#valid-msg");

    $("#phonecountry").intlTelInput({

      // allowDropdown: false,

      autoHideDialCode: false,

      autoPlaceholder: "off",

      dropdownContainer: "body",

      excludeCountries: ["us"],

      formatOnDisplay: false,

      geoIpLookup: function(callback) {

        $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {

          var countryCode = (resp && resp.country) ? resp.country : "";

          callback(countryCode);

        });

      },

      hiddenInput: "full_number",

      initialCountry: "auto",

      nationalMode: false,

      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],

      placeholderNumberType: "MOBILE",

      preferredCountries: ['cn', 'jp'],

      separateDialCode: true,

      utilsScript:"<?php echo base_url() ?>assets/inilabs/utils.js"

    });

//     var reset = function() {

//   telInput.removeClass("error");

//   errorMsg.addClass("hide");

//   validMsg.addClass("hide");

// };

// on blur: validate

telInput.blur(function() {

//   reset();

//   if ($.trim(telInput.val())) {

//     if (telInput.intlTelInput("isValidNumber")) {

//       validMsg.removeClass("hide");

//     } else {

//       telInput.addClass("error");

//       errorMsg.removeClass("hide");

//     }

//   }

// });

// on keyup / change flag: reset

telInput.on("keyup change", reset);

  </script>

<!-- EtDesigns -->
<script src="<?php echo base_url() ?>assets/timepicker/timepicker.js"></script>

   <script type="text/javascript" src="https://cdn.rawgit.com/monkeecreate/jquery.simpleWeather/master/jquery.simpleWeather.min.js"></script> 

  <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/weather.js"></script>

  <script type="text/javascript">

    function  CourseSDependent(id){ 

     $('.showLoaderSubcour').show();

    $.ajax({

        type: "POST",

        url:"<?=base_url('student/Get_subCourses')?>",

        data:{"id":id},

        success: function(response) {

            $("#subCourseID").html(response);

            $('.showLoaderSubcour').hide();
            $("#appendSujectsDetails").html('');

        }

            });

    

        $.ajax({

        type: "POST",

        url:"<?=base_url('AjaxController/fetch_Semester')?>",

        data:{"id":id},

        success: function(response) {

            $("#fetchYearsAndSem").html(response);

        }

            });

  }

</script>

<script type="text/javascript">

    function  CourseSDependentwithtimetable(id){ 

     $('.showLoaderSubcour').show();

    $.ajax({

        type: "POST",

        url:"<?=base_url('student/Get_subCourses')?>",

        data:{"id":id},

        success: function(response) {

            $("#subCourseID").html(response);

            $('.showLoaderSubcour').hide();
            $("#appendSujectsDetails").html('');

        }

            });

    

        $.ajax({

        type: "POST",

        url:"<?=base_url('AjaxController/fetch_Semesterwithtimetable')?>",

        data:{"id":id},

        success: function(response) {

            $("#fetchYearsAndSem").html(response);

        }

            });

  }

</script>

<script type="text/javascript">

    function  departmentwiseCourse(id){ 
     

     $('.showLoaderSubcour').show();    

      $.ajax({

      type: "POST",

      url:"<?=base_url('AjaxController/fetch_course')?>",

      data:{"id":id},

      success: function(response) {

        $("#departmentCourse").html(response);
        $("#yearSemesterID").html('');
        $("#appendSujectsDetails").html('');

        // $("#fetchYearsAndSem").css("display", "none");

      }

    });

  }

</script>
<script>
  function departmentwiseCoursewithTimetable(id){ 
     

     $('.showLoaderSubcour').show();    

      $.ajax({

      type: "POST",

      url:"<?=base_url('AjaxController/fetch_coursewithtimetable')?>",

      data:{"id":id},

      success: function(response) {

        $("#departmentCourse").html(response);
        $("#yearSemesterID").html('');
        $("#appendSujectsDetails").html('');

        // $("#fetchYearsAndSem").css("display", "none");

      }

    });

  }

</script>

<script>

function feedbackSubmit(){

  var  message = $('#message_feedback').val();
  var  email = $('#email_feedback').val();
  var  category = $('#category').val();
  

  if(message==''){

     swal(" ", "Please enter your query", "warning");

       setTimeout( "$('.swal-overlay').hide();", 3000);

  }else{

  $.ajax({

type:'POST',

url:"<?php echo base_url() ?>dashboard/feedbackMessage",

data:{val:message,email:email,category:category},

success:function(){
    jq('#myModal').modal('hide');

  jq('#message_feedback').val('');

     swal(" ", "Thank you, we will contact you soon", "success");

    setTimeout( "$('.swal-overlay').hide();", 3000);

}

}); 

}

}

</script>

<script type="text/javascript">

  function  CourseSDependent1(id){ 

     $('.showLoaderSubcour').show();

    $.ajax({

        type: "POST",

        url:"<?=base_url('student/Get_subCourses')?>",

        data:{"id":id},

        success: function(response) {

            $("#subCourseID").html(response);

            $('.showLoaderSubcour').hide();

        }

            });

        $.ajax({

        type: "POST",

        url:"<?=base_url('AjaxController/fetch_noticefilter')?>",

        data:{"id":id},

        success: function(response) {

            $("#fetchYearsAndSem").html(response);

        }

            });

  }

</script>

 <script type="text/javascript">

       jq(document).ready(function(){

            jq('ul li').click(function(){

            jq('li').removeClass("active");

            jq(this).addClass("active");

        });

        });

        </script>

       <script>

$(document).ready(function(){

  $('ul li').click(function(){

    $('li').removeClass("active");

    $(this).addClass("active");

});

});

</script>

<!-- <script>

  function oncheck(){

    alert('hh');

  $(document).ready(function(){

      $(".checkbox").click(function(){

          $("#start_button").show();

      });

      $("#oncheck").click(function(){

          $("#start_button").hide();

      });

  });

}

</script> -->

<script type="text/javascript">

  function oncheck(){

  onchange="document.getElementById('$checkbox').disabled = !this.checked;"

}

</script>

<!-- <script type="text/javascript">

  $('input[type="checkbox"]').change(function(event) {

      // State has changed to checked/unchecked.

      alert('gg');

});

</script> -->

<script type="text/javascript">

   $('#fpaidfilter').click(function() {

    $('#example4').DataTable().state.clear();

       var payment_status = 2;

           $.ajax({

               type: 'POST',   

               url: "<?=base_url('student/index')?>",

               data: "payment_status=" + payment_status,

               dataType: "html",

               success: function(data) {

                  window.location.href=base_url+"student"; 

               }   

           });

   });

     $('#ppaidfilter').click(function() {

     $('#example4').DataTable().state.clear();

       var payment_status = 1;
  
           $.ajax({ 

               type: 'POST',

               url: "<?=base_url('student/index')?>",

               data: "payment_status=" + payment_status,

               dataType: "html",

               success: function(data) {   

                   window.location.href=base_url+"student";

               }

           });
   });

     $('#npaidfilter').click(function() {

      $('#example4').DataTable().state.clear();

       var payment_status = 3;

           $.ajax({
   
               type: 'POST',

               url: "<?=base_url('student/index')?>",

               data: "payment_status=" + payment_status,

               dataType: "html",

               success: function(data) {   

                   window.location.href=base_url+"student";

               }

   

           });

   

   });

</script>  

        <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/fancy-file-uploader/jquery.ui.widget.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/fancy-file-uploader/jquery.fileupload.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/fancy-file-uploader/jquery.iframe-transport.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/fancy-file-uploader/jquery.fancy-fileupload.js"></script>

        <script type="text/javascript">

$(function() {

    $('#thefiles').FancyFileUpload({

        params : {

            action : 'fileuploader',

            bhikhari :Math.floor((Math.random() * 5000) + 2000),

            profid :$('#professorID').val(),

        },

        maxfilesize:100000000000

    });

});

</script> 

<style type="text/css">

          /* Ensure that the demo table scrolls */

    th, td { white-space: nowrap; }

    div.dataTables_wrapper {

        width: 100%;

        margin: 0 auto;

    }

    div.ColVis {

        float: left;

    }

  </style>




  <script type="text/javascript">
  function myFunction()
  {
    
        var = $angry('#angry')val();
       

         $.ajax(
          {
            type:"POST",
            data:{"#angry":$angry},
            url: "<?php echo base_url('notice/ratingmail');?>",
            success: function(result)
            {
              $("#rating").fadeIn();
              $("#rating").html(result);
              $("#rating").fadeOut(5000);
            }
          });
      // });
  }
</script>

<script type="text/javascript">
  function myFunction1()
  {
      var angry=$('#sad').val();
       

  
         $.ajax(
          {
            type:"POST",
            data:{"angry":angry},
            url: "<?php echo base_url('notice/ratingmail');?>",
            success: function(result)
            {
              
              $("#rating").fadeIn();
              $("#rating").html(result);
              $("#rating").fadeOut(5000);
            }
          });
      // });
  }
</script>

<script type="text/javascript">
  function myFunction2()
  {
     
        var angry=$('#love').val();

    
         $.ajax(
          {
            type:"POST",
            data:{"angry":angry},
            url: "<?php echo base_url('notice/ratingmail');?>",
            success: function(result)
            {
              $("#rating").fadeIn();
              $("#rating").html(result);
              $("#rating").fadeOut(5000);
            }
          });
      // });
  }
</script>
<script type="text/javascript">
  function myFunction3()
  {
     
        var angry=$('#wow').val();
      
         $.ajax(
          {
            type:"POST",
            data:{"angry":angry},
            url: "<?php echo base_url('notice/ratingmail');?>",
            success: function(result)
            {
              $("#rating").fadeIn();
              $("#rating").html(result);
              $("#rating").fadeOut(5000);
            }
          });
      // });
  }
</script>
<script type="text/javascript">
  function myFunction4()
  {

         var angry=$('.like').val();
        
         $.ajax(
          {
            type:"POST",
            data:{"angry":angry},
            url: "<?php echo base_url('notice/ratingmail');?>",
            success: function(result)
            {
              $("#rating").fadeIn();
              $("#rating").html(result);
              $("#rating").fadeOut(5000);
            }
          });
      // });
  }
</script>
<script type="text/javascript">
    function goBack() {
  window.history.back();
  console.log('We are in previous page');
}


  </script>
<div class="modal modal-box-2 fade" id="superadminemailmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="margin-top: 55px">
           <div class="modal-content" id="myModalLabel">
             <div class="modal-header theme-bg">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
            <div class="modal-body">
              <h3>Request to change your <span> E-mail ID</span></h3>
              <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;">
                <div class="row">
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#requestopt">Request Now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
          <!-- after request click popup -->
          <div class="modal modal-box-2 fade" id="requestopt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             <div class="modal-dialog" style="margin-top: 55px">
                <div class="modal-content" id="myModalLabel">
                   <div class="modal-header theme-bg">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                  <div class="modal-body">
                     <h3>Enter New <span>E-mail</span></h3>
                     <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" action="<?php echo base_url('dashboard/requestbysuperadmin') ?>" method="POST">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="newemail" required placeholder="Enter New E-mail" >
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="col-lg-12 text-center">
                              <div id="success"></div>
                              <button type="submit" class="btn modal-btn" >Request Now</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div> 
          <!--  -->
<div class="modal modal-box-2 fade" id="superadminphonemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="margin-top: 55px">
           <div class="modal-content" id="myModalLabel">
             <div class="modal-header theme-bg">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
            <div class="modal-body">
              <h3>Request to change your <span> Mobile No.</span></h3>
              <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;">
                <div class="row">
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="button" class="btn modal-btn" data-toggle="modal" data-target="#requestmobile">Request Now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal modal-box-2 fade" id="requestmobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="margin-top: 55px">
           <div class="modal-content" id="myModalLabel">
             <div class="modal-header theme-bg">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
            <div class="modal-body">
              <h3>Enter New <span>Mobile Number</span></h3>
              <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" action="<?php echo base_url('dashboard/requestbysuperadmin') ?>" method="POST">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" class="form-control" name="requestmobilenumber" required placeholder="Enter New Mobile No." >
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="submit" class="btn modal-btn">Request Now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 

<!-- for admin -->
 <!-- after request click popup -->
          <div class="modal modal-box-2 fade" id="adminemail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             <div class="modal-dialog" style="margin-top: 55px">
                <div class="modal-content" id="myModalLabel">
                   <div class="modal-header theme-bg">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                  <div class="modal-body">
                     <h3>Change Your <span>E-mail</span></h3>
                     <form name="sentMessage" class="contactForm" style="padding: 1em 2em 0em 2em;" method="post" action="<?php echo base_url('profile/index') ?>">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="emailprofessor" value="<?php echo $this->session->userdata('email') ?>" required placeholder="Enter Your E-mail" >
                              </div>
                           </div>
                           <div class="clearfix"></div>
                           <div class="col-lg-12 text-center">
                              <div id="success"></div>
                              <button type="submit" class="btn modal-btn" >Update Now</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div> 
          <!--  -->
<div class="modal modal-box-2 fade" id="adminphone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="margin-top: 55px">
           <div class="modal-content" id="myModalLabel">
             <div class="modal-header theme-bg">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
            <div class="modal-body">
              <h3>Change Your <span>Mobile Number</span></h3>
              <form name="sentMessage" method="post" class="contactForm" style="padding: 1em 2em 0em 2em;" action="<?php echo base_url('profile/index') ?>">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" class="form-control" name="mobilenumber" value="<?php echo $this->session->userdata('phone') ?>" required placeholder="Enter your mobile No." maxlength="10">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <div id="success"></div>
                    <button type="submit" class="btn modal-btn">Update Now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 