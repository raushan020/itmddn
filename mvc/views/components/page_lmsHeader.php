<!DOCTYPE html>

<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">



<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="">

  <meta name="author" content="">

  <title>LMS - Learning Management System</title>

 


<?=link_tag('assets/lms/css/vendor/material-design-iconic-font.min.css');?>
<?=link_tag('assets/lms/css/vendor/util.css');?>
<?=link_tag('assets/lms/css/vendor/main.css');?>

<?=link_tag('assets/lms/css/app/material-icons.css');?>

<?=link_tag('assets/lms/css/app/app_course.css');?>
<?=link_tag('assets/lms/css/vendor/all.css');?>

<!-- <?=link_tag('assets/lms/css/default.css');?> -->

<?=link_tag('assets/lms/css/component.css');?>

<?=link_tag('assets/lms/css/app/app.css');?>

<?=link_tag('assets/lms/css/style.css');?>

<script src="assets/lms/css/app.js"></script>
<script src="<?php echo base_url('assets/etslabs/js/jquery-modal-video.min.js');?>"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet"> -->
  <!-- <link href="https://vjs.zencdn.net/7.6.6/video-js.css" rel="stylesheet" />

  <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script> -->





</head>



<div class="parallax overflow-hidden bg-blue-400 page-section third">

    <div class="container parallax-layer" data-opacity="true">

      <div class="media v-middle">
<?php $usertype = $this->session->userdata('usertype');
      if($usertype != "Parent"){     ?>
        <div class="media-left text-center">

          <a href="<?php echo base_url() ?>profile/index">

            <img src="<?=base_url("uploads/images/".$this->session->userdata('photo')); ?>" alt="people" class="img-circle width-80" />

          </a>

        </div>

        <div class="media-body">

          <h1 class="text-white text-display-1 margin-v-0" style="font-variant-caps: all-petite-caps;"><?=$this->session->userdata('name');?> </h1>

          <p class="text-subhead" style="color: #fff;">

            <?php echo  $data_single_class->classes ?><br>

                              <?php



                                if (!empty($data_single_sub_courses)) {?>



                                    (<?php echo $data_single_sub_courses->sub_course ?>)



                                    <?php } ?>

          </p>

        </div>
      <?php } ?>
        <div class="media-right">
          <?php if ($this->uri->segment(2)!='lmssubject') { ?>
            <span class="label bg-blue-500"><a class="link-white" href="<?php echo base_url() ?>lms/lmssubject">Home</a></span>
          
          <?php }else{ ?>
            <span class="label bg-blue-500"><a class="link-white" href="<?php echo base_url() ?>dashboard/index">Dashboard</a></span>
          <?php } ?>
        </div>

      </div>

    </div>

  </div>

  <div class="container">
  	<div class="media v-middle" style="margin-top: 10px;">
      <?php 
      if ($this->uri->segment(2)!='lmssubject') {
      
      
      ?>
  	<button id="back" class="asd_ss" onclick="goBack()">BACK</button>
  <?php }?>
    <!-- <button id="forward" class="asd_ss" onclick="goForward()">FORWARD</button> -->
    <!-- <a href="<?php echo base_url() ?>lms/lmsLatest" class="asd_ss_fd"><i class="fa fa-home" aria-hidden="true"></i></a> -->
    </div>
  </div>

  <style type="text/css">
  	.asd_ss{
  		    border: none;
    background: #3c8dbc;
    color: #fff;
    /* padding: 1px; */
        width: 60px;
    height: 25px;
    border-radius: 2px;
  	}
  	.asd_ss_fd{
  		font-size: 18px;
    padding: 0px;
    background: none;
    color: #000;
  	}
  </style>
  <script type="text/javascript">
  	function goBack() {
	window.history.back();
	console.log('We are in previous page');
}

function goForward() {
	window.history.forward();
	console.log('We are in next page');
}
  </script>

