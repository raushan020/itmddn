<style>
    .menuposition {
       position: absolute;
    right: 28px;
    background: #ff9800;
}
</style>
<div class="navbar-default sidebar hold-transition skin-blue sidebar-collapse sidebar-mini" id="dsds" role="navigation" >
   <div class="sidebar-nav navbar-collapse collapse">
      <?php $usertype = $this->session->userdata("usertype");
          $adminID = $this->session->userdata('adminID');
       ?>
      <ul class="nav" id="side-menu" >
         <li class="">
            <?php
               echo anchor('dashboard/index' ,'<i class="fa fa-dashboard
               
               " style="color: black;"></i><span>'.$this->lang->line('menu_dashboard').'</span>',array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Dashboard'));
               
               ?> 
         </li>
         <?php
            if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "Accountant" || $usertype == "Support" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
            
                echo '<li class="">';
            
                    echo anchor('student/index', '<i class="fa fa-user"></i><span>'.'&nbsp;'.$this->lang->line('menu_student').'</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Student'));
            
                echo '</li>';
            
            }
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support" || $usertype == "HOD") {
            
                echo '<li class="">';
            
                    echo anchor('professor/index', '<i class="ti ti-id-badge"></i><span>Professor</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Professor'));
            
                echo '</li>';
            
            }
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support" || $usertype == "Teacher") {
            
    
                echo '<li class="">';
            
            
            
                    echo anchor('classes/index', '<i class="ti ti-book"></i><span>'.$this->lang->line('menu_classes').'</span>' ,array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Course'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support" || $usertype == "Teacher") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('subject/index', '<i class="fa fa-book"></i><span>'.$this->lang->line('menu_subject').'</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
            
            
                echo '</li>';
            
            
                echo '<li>';
            
            
            
                    echo anchor('timetable/index', '<i class="fa fa-table"></i><span>Time Table</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
                echo '</li>';
            
            }
            
            
            
            ?>
         <?php 
            if($usertype == "Student" || $usertype == "Parent") {

              if($usertype == "Student"){
                echo '<li>';
            
            
            
                    echo anchor('subject/index', '<i class="fa fa-book"></i><span>My '.$this->lang->line('menu_subject').'s</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));

                echo '</li>';
                
              }
                echo '<li>';
                    echo anchor('subject/attendance', '<i class="fa fa-list"></i><span>Attendance</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Attendance'));
                echo '</li>';
              if($usertype == "Student"){
                 echo '<li>';
        
                    echo anchor('timetable/index', '<i class="fa fa-table"></i><span>Time Table</span>',array('data-toggle'=>'tooltip','data-placemen'=>'right','title'=>'Subject'));
            
            
            
                echo '</li>';
              }
            }    
            ?>
         <?php
            if($usertype == "Student") {
               
            
                echo '<li>';
                    echo anchor('lms/lmssubject', '<i class="ti ti-blackboard"></i><span>LMS</span>',array('data-toggle'=>'tooltip','title'=>'LMS'));
                echo '</li>';
            }
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support" || $usertype=='Professor') {
            ?>
            <!-- <li>
          <?php echo anchor('live/table', '<i class="fa fa-laptop"></i><span>Live Class</span>' ,array('data-toggle'=>'tooltip','title'=>'Live Class')); ?>
         </li> -->
         <li>
            <a data-toggle='tooltip' title='LMS' href="javascript:void(0)"><i class="ti ti-blackboard"></i><span>LMS</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
               
               <?php if($usertype!='Professors') { ?>
               <li>
                  <a data-toggle='tooltip' title='Online Reader' href="<?php echo base_url() ?>online_reader" style="margin-left: 0px;"><i class="fa fa-laptop"></i>Online Reader</a>
               </li>
           <?php }?>
               <li>
                  <a data-toggle='tooltip' title='Videos' href="<?php echo base_url() ?>videos" style="margin-left: 0px;"><i class="fa fa-video-camera" aria-hidden="true"></i>Videos</a>
               </li>
               <li>
                  <a data-toggle='tooltip' title='Notes' href="<?php echo base_url() ?>lms" style="margin-left: 0px;"><i class="fa fa-file-pdf-o"></i>Miscellaneous File</a>
               </li>
               <?php if($usertype == "Admin" || $usertype == "ClgAdmin"){ ?>
               
            <?php }
            else
              { ?>
                <li>
                  <a data-toggle='tooltip' title='Courseware' href="<?php echo base_url() ?>courseware" style="margin-left: 0px;"><i class="fa fa-columns" aria-hidden="true"></i>Courseware</a>
               </li>
             <?php }?>
            </ul>
         </li>
         
         <?php } ?>
         <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner" || $usertype == "Professor") { ?>
         <li>
            <a  data-toggle='tooltip' title='Exam' href="javascript:void(0)"><i class="ti ti-ruler-pencil"></i><span><?=$this->lang->line('menu_exam');?></span><span class="badge bg-success menuposition" style="background: #ff9800;">New</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
               <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner" || $usertype == "Professor") { ?>
               <!-- admin -->
               <li>
                  <?php echo anchor('exam/index', '<i class="fa fa-pencil"></i>'.$this->lang->line('menu_exam')); ?>
               </li>
               <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner" || $usertype == "Professors") { ?>
               <li>
                  <?php echo anchor('exam/quizSetting', '<i class="fa fa-cogs"></i>Exam Setting',array('data-toggle'=>'tooltip','title'=>'Exam Setting')); ?>
               </li>
               
                <!-- <li>
                  <?php echo anchor('mark/suspend_exam_list', '<i class="fa fa-cogs"></i>Suspend Exam',array('data-toggle'=>'tooltip','title'=>'Suspend Exam')); ?>
               </li>  -->
               <?php }?>
               <li>
                  <?php echo anchor('etsexam/liveVideos', '<i class="fa fa-video-camera"></i>Live Video',array('data-toggle'=>'tooltip','title'=>'Live Video','target'=>'_blank')); ?>
               </li>
               <?php  } ?> 
            </ul>
         </li>
         <?php } ?>
         <!-- || $usertype == "Parent" -->
          <?php
            if($usertype == "Student") {                                
            
                echo '<li>';
            
                 echo anchor('c/m/1', '<i class="fa fa-pencil"></i><span>Exam</span>',array('data-toggle'=>'tooltip','title'=>'Exam','target'=>'_blank'));
            
                echo '</li>';   
                                                 
            
            }
            
            ?> 
         <!-- <?php
            if($usertype == "Student") {
            
                
            
                echo '<li>';
            
            
            
                 echo anchor('c/m/2', '<i class="fa fa-tasks"></i><span>Assignment</span>',array('data-toggle'=>'tooltip','title'=>'Assignment')); 
            
            
            
                echo '</li>';
            
                
            
            }
            
            
            
            ?> -->
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'Professor') {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('mark/index', '<i class="fa fa-sticky-note-o"></i><span>'.$this->lang->line('menu_mark').'</span>' ,array('data-toggle'=>'tooltip','title'=>'Result'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
                     <?php 
                     if($usertype == "Student" || $usertype == "Professor" || $usertype =="Admin" || $usertype == "ClgAdmin" || $usertype == "Parent")
                     {
            if($usertype == "Student" || $usertype == "Professor") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('qa/index/all', '<i class="fa fa-check-square-o"></i><span>Q & A</span>' ,array('data-toggle'=>'tooltip','title'=>'QA'));
            
            
            
                echo '</li>';
            
            
            
            }
            if($usertype == "Admin" || $usertype == "ClgAdmin") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('qa/viewqa', '<i class="fa fa-check-square-o"></i><span>Question & Answer</span>' ,array('data-toggle'=>'tooltip','title'=>'qa'));
            
            
            
                echo '</li>';
            
            
            
            }
          }
            
            
            
            ?>
         <!-- <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('timetable/index', '<i class="fa fa-table"></i><span>Time Table</span>' ,array('data-toggle'=>'tooltip','title'=>'Time Table'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?> -->
<!-          <?php
            if($usertype == "Student" || $usertype == "Parent") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('/mark/result/', '<i class="fa fa-flask"></i><span>'.$this->lang->line('menu_mark').'</span>');
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?> 
         <?php if($usertype == 'ClgAdmin' ||  $usertype == "Accountant" || $usertype == "Support" || $usertype == "Teacher") { ?>
         <li>
            <a data-toggle='tooltip' title='Account' href="<?=base_url()?>invoice/index"><i class="fa fa-rupee"></i> <span><?=$this->lang->line('menu_account');?></span></a>
            <!--  <ul class="nav nav-second-level">
               <li>
               
                   <?php echo anchor('invoice/index', '<i class="fa fa-rupee"></i>Fee' ,array('data-toggle'=>'tooltip','title'=>'Fee','class'=>'rupbakwaas')); ?>
               
               </li>
               
               
               </ul> -->
         </li>
         <?php } ?>

         <?php 
if($usertype == 'ClgAdmin' || $usertype == 'superadmin' || $usertype == 'Accountant') { ?>

 <li>

                                <a data-toggle='tooltip' title='LMS' href="javascript:void(0)"><i class="fa fa-credit-card"></i><span>Billing</span><span class="badge bg-success menuposition" style="background: #ff9800;">New</span><span class="fa arrow"></span></a>
                              <!--   <b class="badge bg-success menuposition">New</b> -->
                                <ul class="nav nav-second-level">
                                <li>
                                    <a data-toggle='tooltip' title='<?php $this->lang->line('menu_teacher') ?>' href="<?php echo base_url() ?>invoice/clgPayinvoice?panel=<?php echo $this->input->get('panel') ?>" style="margin-left: 0px;"><i class="fa fa-rupee"></i>Pay Now</a>
                                </li>
                                <li>
                                    <a data-toggle='tooltip' title='Passbook' href="<?php echo base_url() ?>invoice/clgBilling?panel=<?php echo $this->input->get('panel') ?>" style="margin-left: 0px;"><i class="fa fa-table"></i>Account Statement</a>
                                </li>
                                <li>
                                    <a data-toggle='tooltip' title='Payment Reciept' href="<?php echo base_url() ?>invoice/payment_reciept?panel=<?php echo $this->input->get('panel') ?>" style="margin-left: 0px;"><i class="fa fa-money"></i> Payment History</a>
                                </li>
                                <li>
                                    <a data-toggle='tooltip' title='Passbook' href="<?php echo base_url() ?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>&type=pds" style="margin-left: 0px;"><i class="fa fa-address-card-o"></i>Billing Profile</a>
                                </li>
                                </ul>

                            </li>
                           <?php  } ?>

         <?php
            if($usertype == "Student" || $usertype == "Parent") {           
            
            
                echo '<li>';
            
                    echo anchor('invoice/index', '<i class="fa fa-rupee"></i><span>&nbsp;&nbsp;Fee</span>',array('class'=>'rupbakwaas','data-toggle'=>'tooltip','title'=>'invoice'));
            
                echo '</li>';
            
            
            }
            
            
            
            ?>
         <?php
            if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Student" || $usertype == "Support" || $usertype == "Teacher" || $usertype == 'Accountant' || $usertype == "Parent" || $usertype == "Senior_Examiner" || $usertype == "Assistant_Examiner") {
            
            
            
                echo '<li>';
            
            
            
                    echo anchor('notice/index', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>'.$this->lang->line('menu_notice').'</span>',array('data-toggle'=>'tooltip','title'=>'Notice'));
            
            
            
                echo '</li>';
            }
            ?>
         <?php
            if( $usertype == "" || $usertype == "" ) 
               {  
                   echo '<li>';
                                   echo anchor('notice/add', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>'.$this->lang->line('menu_notice').'</span>',array('data-toggle'=>'tooltip','title'=>'Notice'));
            
            
            
                               echo '</li>';
                           }
            ?>
         <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") { ?>
         <!--      <li>
            <a href="javascript:void(0)"><i class="ti ti-ruler-pencil"></i><?=$this->lang->line('menu_mailandsms');?><span class="fa arrow"></span></a>
            
            <ul class="nav nav-second-level">
            
               
            
                <li>
            
            
            
                    <?php echo anchor('mailandsmstemplate/index', '<i class="fa icon-template"></i><span>'.$this->lang->line('menu_mailandsmstemplate').'</span>'); ?>
            
            
            
                </li>
            
            
            
                <li>
            
            
            
                    <?php echo anchor('mailandsms/index', '<i class="fa icon-mailandsms"></i><span>'.$this->lang->line('menu_mailandsms').'</span>'); ?>
            
            
            
                </li>
            
            
            
                <li>
            
            
            
                    <?php echo anchor('smssettings/index', '<i class="fa fa-wrench"></i><span>'.$this->lang->line('menu_smssettings').'</span>'); ?>
            
            
            
                </li>
            
            </ul>
            
            </li> -->
         <?php } ?>
         <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") { ?>
         <!--           <li>
            <a href="javascript:void(0)"><i class="ti ti-file"></i>Data Management <span class="fa arrow"></span></a>
            
            <ul class="nav nav-second-level">
            
                                                    <li>
            
                    <?php echo anchor('databaseManagement/Courses', '<i class="fa icon-template"></i><span>Copy Course</span>'); ?>
            
                </li>
            
                <li>
            
                    <?php echo anchor('databaseManagement/CourseView', '<i class="fa icon-template"></i><span>Course View</span>'); ?>
            
                </li>
            
            </ul>
            
            </li> -->
         <?php } ?>
         <?php
            if($this->session->userdata("usertype") == "Admin" || $this->session->userdata("usertype") == "ClgAdmin" || $usertype == "Support") {
            
             echo '<li>';
                    echo anchor('viewlecture/ebook', '<i class="fa fa-book"></i><span>View Ebook</span><span class="badge bg-success menuposition" style="background: #ff9800;">New</span>' ,array('data-toggle'=>'tooltip','title'=>'View Ebook'));
                echo '</li>';
            
                echo '<li>';
            
            
            
                    echo anchor('bulkimport/index', '<i class="fa fa-upload"></i><span>'.$this->lang->line('menu_import').'</span>' ,array('data-toggle'=>'tooltip','title'=>'Import'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
         <?php
            if($usertype == "Professor") {
            
                echo '<li>';
                    echo anchor('viewlecture/index', '<i class="fa fa-pencil-square-o"></i><span>Lecture</span>' ,array('data-toggle'=>'tooltip','title'=>'Lecture'));
                echo '</li>';
                
              echo "<li>";
               echo'<a data-toggle="tooltip" title="Attendance" href="javascript:void(0)"><i class="fa fa-list"></i><span>Attendance</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">';
                     echo '<li>';
                       echo anchor('viewlecture/attendance?type=filter', '<i class="fa fa-list"></i><span>Mark Attendance</span>' ,array('data-toggle'=>'tooltip','title'=>'Mark Attendance'));
                     echo '</li>';
                     echo '<li>';
                      echo anchor('viewlecture/viewattendance', '<i class="fa fa-eye"></i><span>View Attendance</span>' ,array('data-toggle'=>'tooltip','title'=>'View Attendance'));
                     echo '</li>';
                echo '</ul>';
              echo "</li>";  
            
                echo '<li>';
                    echo anchor('viewlecture/subjectprogress', '<i class="fa fa-book"></i><span>Subject Progress</span>' ,array('data-toggle'=>'tooltip','title'=>'Subject Progress'));
                echo '</li>';
                
                echo '<li>';
                    echo anchor('viewlecture/ebook', '<i class="fa fa-book"></i><span>View Ebook</span><span class="badge bg-success menuposition" style="background: #ff9800;">New</span>' ,array('data-toggle'=>'tooltip','title'=>'View Ebook'));
                echo '</li>';
            
             
          
            
                echo '<li>';
            
            
            
                    echo anchor('notice/index', '<i class="fa fa-bell-o" aria-hidden="true"></i><span>'.$this->lang->line('menu_notice').'</span>',array('data-toggle'=>'tooltip','title'=>'Notice'));
            
            
            
                echo '</li>';

             echo   "<li>
            <a data-toggle='tooltip' title='Report' href='javascript:void(0)''><i class='fa fa-clipboard'></i><span>Report</span><span class='badge bg-success menuposition' style='background: #ff9800;'>New</span><span class='fa arrow'></span></a>
            <ul class='nav nav-second-level'>
               <li>
                  <a data-toggle='tooltip' title='Student Attendance' href='".base_url('professor/student_attendaancerepost')."' style='margin-left: 0px;''><i class='fa fa-eye'></i>Student Attendance</a>
               </li>
               <li>
                  <a data-toggle='tooltip' title='Debarred List' href='".base_url('professor/debarred_list')."' style='margin-left: 0px;''><i class='fa fa-align-left'></i>Debarred List</a>
               </li>               
            </ul>
         </li>";
        

               
            
            }
            
            
            
            ?>
         <?php
            if($usertype == 'ClgAdmin') {
            
            
            
                echo '<li>';
            
            
            
                echo anchor('user/index', '<i class="fa fa-users"></i><span>'.$this->lang->line('menu_user').'</span>' ,array('data-toggle'=>'tooltip','title'=>'User'));
            
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
         <?php
            if($usertype == 'ClgAdmin') {
            
            
            
                echo '<li>';
            
            
            
                 echo anchor('setting/index', '<i class="fa fa-cog" aria-hidden="true"></i><span>College Setting</span>' ,array('data-toggle'=>'tooltip','title'=>'Setting'));
            
                
            
            
                echo '</li>';
            
            
            
            }
            
            
            
            ?>
                     <?php
            if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == 'Professor' || $usertype == 'Accountant') { ?>
            
            <li>
            <a data-toggle='tooltip' title='Email' href="javascript:void(0)"><i class="fa fa-envelope"></i><span>Email</span><span class="badge bg-success menuposition" style="background: #ff9800;">New</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
               <!-- <li>
                  <a data-toggle='tooltip' title='Add PDF' href="<?php echo base_url() ?>lms" style="margin-left: 0px;"><i class="fa fa-file-pdf-o"></i>Add PDF</a>
               </li> -->
               <li>
                  <a data-toggle='tooltip' title='Compose' href="<?php echo base_url() ?>customemail/index" style="margin-left: 0px;"><i class="fa fa-th"></i>Compose</a>
               </li>
               <li>
                  <a data-toggle='tooltip' title='Sent' href="<?php echo base_url() ?>customemail/sent" style="margin-left: 0px;"><i class="fa fa-send" aria-hidden="true"></i>Sent</a>
               </li>
            </ul>
         </li>
          <?php  }
            
            ?>
 <!--            <?php
    if($usertype == 'Professor') { ?>

 <li>

                                <a data-toggle='tooltip' title='Billing' href="javascript:void(0)"><i class="ti ti-id-badge"></i><span>Help</span><span class="fa arrow"></span></a>

                                <ul class="nav nav-second-level">
                                <li>
                                    <a data-toggle='tooltip' title='Pay Now' href="<?php echo base_url() ?>invoice/clgPayinvoice" style="margin-left: 0px;"><i class="ti ti-id-badge"></i>feedback</a>
                                </li>
                                </ul>
                                <li>
                                    <a data-toggle='tooltip' title='Account Statement' href="<?php echo base_url() ?>invoice/clgBilling" style="margin-left: 0px;"><i class="fa fa-table"></i>logout</a>
                                </li>
                            </li>
                           <?php  } ?> -->


             <?php
            if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == "HOD") { ?>
            
            <li>
            <a data-toggle='tooltip' title='Report' href="javascript:void(0)"><i class="fa fa-clipboard"></i><span>Report</span><span class="badge bg-success menuposition" style="background: #ff9800;">New</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
               <li>
                  <a data-toggle='tooltip' title='course_progress_report' href="<?php echo base_url() ?>report/course_progress_report" style="margin-left: 0px;"><i class="fa fa-eye"></i>Course Progress</a>
               </li>
               <!-- <li>
                  <a data-toggle='tooltip' title='professor_progress' href="<?php echo base_url() ?>report/professor_progress" style="margin-left: 0px;"><i class="fa fa-send" aria-hidden="true"></i>Professor Progress</a>
               </li> -->
            </ul>
         </li>
            
                
    
          <?php  }
            
            ?>
            
           

      </ul>
   </div>
   <!-- /.sidebar-collapse -->
</div>

  