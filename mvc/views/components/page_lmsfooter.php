 <footer class="footer">
    <strong>EdgeTechnosoft</strong> &copy; Copyright 2013-2020
  </footer>

  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#42a5f5"
        }
      }
    };
  </script>

  
  <script src="<?php echo base_url('assets/lms/js/vendor/all.js');?>"></script>
  
  <script src="<?php echo base_url('assets/lms/js/app/app.js'); ?>"></script>
  
  <script type="text/javascript">

    $('#yearSemesterID').change(function() {
     
        var yearSemesterID = $(this).val();

        $.ajax({

            type: 'POST',

            url: "<?=base_url('lms/lmsLatest')?>",

            data: "yearSemesterID=" + yearSemesterID,

            dataType: "html",

            success: function(data) {

                location.reload();

            }

        });

    });

</script>

</body>

</html>