<!DOCTYPE html>

<html>

    <head>

        <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

        <meta name="description" content="">

        <meta name="author" content="">



        <title><?=$this->lang->line('panel_title')?></title>

        <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" />
        </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125810435-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-125810435-1');
</script><script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','../../../../../../../www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027142-1', 'w3layouts.com');
 ga('send', 'pageview');
</script> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-157727560-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-157727560-1');
</script>



        <script>

          var uri =  "<?php echo $this->uri->segment(3) ?>";

          var uri4 =  "<?php echo $this->uri->segment(4) ?>";

          var base_url="<?php echo base_url();?>";

        </script>        

        <?=link_tag('assets/etslabs/css/pignose.calendar.min.css');?>        

        <?=link_tag('assets/etslabs/css/croppie.css');?>

        <!-- Font CSS -->

        <?=link_tag('assets/etslabs/css/all_icons.css');?>

        <?=link_tag('assets/plugins/bootstrap/css/bootstrap.css');?>        

        <?=link_tag('assets/etslabs/css/style.css');?>        

         <?=link_tag('assets/etslabs/css/feedback.css');?>       

        <!-- MetisMenu CSS -->        

        <?=link_tag('assets/plugins/metisMenu/metisMenu.min.css');?>

        <!-- Custom CSS -->   

     <?=link_tag('assets/etslabs/css/weather.css');?>

        <!-- Change Color CSS -->         

        <?=link_tag('assets/etslabs/css/skin/default-skin.css');?>      

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">  

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">            

        <?=link_tag('assets/etslabs/css/intlTelInput.css');?>

        <?=link_tag('assets/fonts/font-awesome.css');?>

        <?=link_tag('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css');?>       

        <?=link_tag('assets/etslabs/css/styleExam.css');?> 

        <?=link_tag('assets/etslabs/css/modal-video.min.css');?> 

        <?=link_tag('assets/etslabs/css/login.css');?> 

        <?=link_tag('assets/etslabs/css/introjs.css');?>

        <?=link_tag('assets/timepicker/timepicker.css');?>

        

        <?=link_tag('assets/etslabs/css/examples.css');?>          

        <?=link_tag('assets/etslabs/css/kavach.min.css');?>

        <?=link_tag('assets/inilabs/EtDesigns/css/jquery.datetimepicker.min.css');?>

        <?=link_tag('assets/etslabs/css/button-style.css');?>
        <!-- <?=link_tag('assets/plugins/select2/select2.css');?> -->
        <!-- <script src="http://vjs.zencdn.net/4.2/video.js"></script> -->

        <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.3.3/video-js.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/calendar/css/style.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/calendar/font-awesome/css/font-awesome.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/calendar/plugins/fullcalendar/fullcalendar.css'; ?>"> 

    <style type="text/css">

  .vjs-default-skin { color: #eb1f1f; }

  .vjs-default-skin .vjs-play-progress,

  .vjs-default-skin .vjs-volume-level { background-color: #284c78 }

  .vjs-default-skin .vjs-control-bar,

  .vjs-default-skin .vjs-big-play-button { background: rgba(34,169,189,0.7) }

  .vjs-default-skin .vjs-slider { background: rgba(34,169,189,0.2333333333333333) }

  .vjs-default-skin .vjs-control-bar { font-size: 86% }

</style>
<!-- <script src="<?php echo base_url()?>assets/plugins/select2/select2.full.min.js"></script>  -->
        <script src="<?php echo base_url() ?>assets/webcamjs/MediaStreamRecorder.min.js"></script>

    	  <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>  

        <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>

        <script src="<?php echo base_url('assets/etslabs/js/html2canvas.min.js');?>"></script>
        <!--  -->
        <script src="<?php echo base_url('assets/etslabs/js/basic.js');?>"></script>        

        <script src="<?php echo base_url('assets/etslabs/js/jquery-modal-video.min.js');?>"></script>   

        <script src="https://code.highcharts.com/highcharts.js"></script>

         <script src="<?php echo base_url('ckeditor/ckeditor.js');?>"></script> 
         <script src="<?php echo base_url('ckeditor/samples/js/sample.js');?>"></script> 
        <script src="https://code.highcharts.com/highcharts-3d.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/cylinder.js"></script>


        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <script src="https://code.highcharts.com/modules/export-data.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

        <script src="<?php echo base_url('assets/etslabs/js/feedback.js');?>"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/epubjs/dist/epub.min.js"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/annotator/1.2.9/annotator.min.js"></script> 
        
        <!-- <script src="<?php echo base_url()?>assets/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>  -->  

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/fancy-file-uploader/fancy_fileupload.css" type="text/css" media="all" /> 

        <style type="text/css">

          .ff_fileupload_hidden { display: none; }

        </style>



    </head>

    <body onload="javascript:introJs().start();" id="screenshot_id">

    <?php
      // $URI = "$_SERVER[REQUEST_URI]";
      $uriMain= $this->uri->segment(1).'/'.$this->uri->segment(2); 
    ?>   

    <input type="hidden" name="uri_findOutFrom_urls_jquery" value="<?php echo $uriMain ?>" id="uri_findOutFrom_urls_jquery" >

    <script type="text/javascript">

      var redirecturi =  $('#uri_findOutFrom_urls_jquery').val();

    </script>

<?php $usertype = $this->session->userdata("usertype");

// if ($usertype== "Student") {
// $activeClass = "";
// }else 
// {
//  $activeClass = ""; 
// }

 ?>

    <div id="wrapper" class="">

    <div class="fakeLoader"></div>

    <?php if ($this->session->flashdata('success')): ?>

    <script type="text/javascript">

     swal(" ", "<?=$this->session->flashdata('success') ?>", "success");

       setTimeout( "$('.swal-overlay').hide();", 3000);

    </script>

    <?php endif ?>

    <?php if ($this->session->flashdata('error')): ?>

    <script type="text/javascript">

      swal("<?=$this->session->flashdata('error') ?>");

       setTimeout( "$('.swal-overlay').hide();", 10000);
       $('.swal-text').css('color','red');

    </script>

    <?php endif ?>



    <script type="text/javascript">

      $('side-menu').click(function(){
        $( ".tabs" ).tabs( "option", "active", 2 );
      });

    </script>
    <script type="text/javascript">
      // $(document).ready(function(){
        function sendotp()
        {
          var mobile="<?php echo $this->session->userdata('phone') ?>";
          // alert(mobile);
           $.ajax({
            url: base_url+'AjaxController/sendotpbyanyuser',
            type: 'POST',
            data:{mobile:mobile},
            success: function(data)
            { 
              
              $("#otp").attr("value", data);
              
              
            }

          });
        }
        // $('#sendotp').click(function(){
          
        // });
      // });
    </script>

<!-- verifymobile -->
      <div class="modal modal-box-1 fade" id="verifymob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="width: 350px;margin-top: 60px;">
           <div class="modal-content" id="myModalLabel">
            <div class="modal-body">

              <form id="contactForm" action="<?php echo base_url('dashboard/verifymob'); ?>" method="post">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="number" name="otpuser" class="form-control" placeholder="Enter Your OTP" required>
                      <input type="hidden" name="otp" id="otp">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-lg-12 text-center">
                    <button type="submit" class="btn modal-btn" style="padding: 6px 16px;min-width: 110px;">Verify now</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End -->
      <!-- help -->
      <div class="modal modal-box-1 fade" id="help" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog" style="width: 350px;margin-top: 60px;">
           <div class="modal-content" id="myModalLabel">
                <h4 style="background: #546e7a;height: 50px;color: #fff;padding: 15px;">Send feedback</h4>
            <div class="modal-body" style="padding: 0px !important;">
                <form id="" action="<?php echo base_url('customemail/sendfeedbacktohelp') ?>" method="post">  
                    <div class="form-group" style="margin-bottom: 0px !important;" >
                      <input type="text" class="form-control" name="writecomment" placeholder="Describe your issue or share your ideas" style="padding-bottom: 120px;padding-top: 10px;" required>
                    </div>
                    <div class="form-group" style="background: #fafafa;height: 40px;margin-bottom: 0px !important;">
                      <label style="padding: 10px;color: #000;">
                    <input type="checkbox" class="flat-red" checked name="hidescreenshot" id="hidescreenshot" value="1">
                    Include screenshot
                  </label>
                    </div>
                    <div class="form-group" style="border: 1px solid #e8eef1;">
                      <img src='<?php echo base_url("uploads/images/loading-circle.gif") ?>' width='32px' height='32px' id="loader" style="display: none">
                      <img src="" id="shot" style="height: 150px;width: 100%;" style="display: none">
                      <input type="hidden" name="imagename" id="imagename" value="">
                      <p style="text-align: center;" id="loadertaking">Taking Screenshot...</p>
                    </div>
                    <div class="form-group">
                      <p style="padding: 5px;"> We will use the information that you give us to help address technical issues and to improve our services, subject to our Privacy Policy and Terms of Service.</p>
                      <div class="col-lg-12" style="text-align: right;">

        			<button type="button" class="btn btn-default" data-dismiss="modal" id="deletescreenshot">Close</button>
                    <button type="submit" class="btn btn-default" >Send now</button>
                  </div>
                    </div>
                  
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End -->

        <!-- Modal -->
<div id="studentpanelvideo" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 55px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #283e4a;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: #fff;">How to use Panel</h4>
      </div>
      <div class="modal-body">
        <video width="100%" height="auto" controls>
  <source src="<?php echo base_url() ?>assets/etslabs/attendancevido.MP4" type="video/mp4">
  <source src="<?php echo base_url() ?>assets/etslabs/attendancevido.MP4" type="video/ogg">
  Your browser does not support the video tag.
</video>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
    $(document).ready(function(){
        $('#hidescreenshot').click(function(){
            if($(this).prop("checked") == true){
                $("#shot").show();
                $('#hidescreenshot').attr("value",1);
            }
            else if($(this).prop("checked") == false){
                $("#shot").hide();
                $('#hidescreenshot').attr("value",0);
            }
        });
    });
</script>
<script type="text/javascript">
   function verifyemailbyadmin(adminID)
   {
      $.ajax({  
   
         type: 'POST',   
   
         url: "<?=base_url('customemail/verifyemailsuperadmin')?>",   
   
         data: "adminID=" + adminID,   
    
         dataType: "html",   
   
         success: function(data)
         {  
             $('.verify').html("Check your email for verification link"); 
             $('.verify').css('color','green');
             $(".verify").fadeOut(10000);
   
         }   
   
       }); 
   }
</script>
<script type="text/javascript">
  function Screenshot()
  {
     $("#loader").show();
     $("#loadertaking").show();
     $("#shot").hide();
    // $("#shot").removeData(bs.model);
    html2canvas(document.getElementById("screenshot_id")).then(function(canvas)
    {                  // document.getElementById("short").appendChild(canvas);
                    var base64URL = canvas.toDataURL();
                    // var base64URL = canvas.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream');
      $.ajax({
      type:"POST",
      data:{"base64URL":base64URL},
      url:"<?php echo base_url(); ?>student/send_feedbackbyscreenshot",
      success: function(data)
      {
        $("#loader").hide();
        $("#loadertaking").hide();
        $("#shot").attr("src", data);
        $("#shot").show();
        $("#imagename").attr("value", data);

      // alert(data);
      // Set the image's source.

      },
      error: function()
      {
        console.log("error found");
      }
      });
          
    });
  }
</script>
<script type="text/javascript">
  $('#deletescreenshot').click(function()
    {
       var image=$('#imagename').val();
       var base = new String(image).substring(image.lastIndexOf('/') + 1); 
       $.ajax({
      type:"POST",
      data:{"base":base},
      url:"<?php echo base_url(); ?>student/send_feedbackbyscreenshotdeletescreenshot",
      success: function(data)
      {
        

      // alert(data);
      // Set the image's source.

      }
      });
       
    });
</script>
