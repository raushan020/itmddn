
<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-upload"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
               <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active"><a href="<?=base_url("bulkimport/index")?>"><?=$this->lang->line('menu_import')?></a></li>
            </ol>
        </div>
    </div>

    <!-- /.box-header -->
    <!-- form start -->
     <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-12">
                <?php if($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <h4><strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>.</h4>
                </div>
            <?php endif ?>

                <!-- <form action="<?=base_url('bulkimport/teacher_bulkimport');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            Add Counseller
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control teacher"  id="uploadFile" placeholder="Choose File" disabled />
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload teacherUpload import-btn" name="csvFile" data-show-upload="false"
                                       data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div> -->
                        <!-- <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="#"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                             <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_teacher.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a> 
                        </div> -->
                    <!-- </div>
                </form> -->
                <!-- <form enctype="multipart/form-data" style="" action="<?=base_url('bulkimport/parent_bulkimport');?>" class="form-horizontal" role="form" method="post">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("bulkimport_parent")?>
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control parent" id="uploadFile" placeholder="Choose File" disabled />
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload parentUpload" name="csvParent" />
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>

                        <div class="col-md-1 rep-mar">
                            <a class="btn btn-info" href="<?=base_url('assets/csv/sample_parent.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div>
                    </div>
                </form>

                <form enctype="multipart/form-data" style="" action="<?=base_url('bulkimport/user_bulkimport');?>" class="form-horizontal" role="form" method="post">
                    <div class="form-group">
                        <label for="csvUser" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("bulkimport_user")?>
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="upload parentUpload form-control" id="uploadFile" placeholder="Choose File" disabled />
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload userUpload" name="csvUser" />
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>

                        <div class="col-md-1 rep-mar">
                            <a class="btn btn-info" href="<?=base_url('assets/csv/sample_user.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div>
                    </div>
                </form> -->

                
                <form action="<?=base_url('bulkimport/student_bulkimport');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("bulkimport_student")?>
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control student" id="uploadFile" placeholder="Choose File" disabled />
                        </div>
                        
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload studentUpload import-btn" name="csvStudent" data-show-upload="false"
                                       data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>
                        <!-- <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_student.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div> -->
                    </div>
                </form>

                <form action="<?=base_url('bulkimport/updateStudent_EnrollmentDirect');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            Update Enrollment
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control student" id="uploadFile" placeholder="Choose File" disabled />
                        </div>
                        
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload studentUpload import-btn" name="csvStudent" data-show-upload="false"
                                       data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div>
                        <!-- <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_student.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div> -->
                    </div>
                </form>

                <!-- <form action="<?=base_url('bulkimport/subject_bulkimport');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                           Add Subject
                        </label>
                        <div class="col-sm-3 col-xs-4 col-md-3">
                            <input class="form-control subject" id="uploadFile" placeholder="Choose File" disabled />
                        </div>
                        
                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload subjectUpload import-btn" name="csvSubject" data-show-upload="false" data-show-preview="false" required="required"/>
                            </div>
                        </div>

                        <div class="col-md-1 rep-mar">
                            <input type="submit" class="btn btn-success import-btn" value="<?=$this->lang->line("bulkimport_submit")?>" >
                        </div> -->
                       <!--  <div class="col-md-1 rep-mar">
                            <a class="btn btn-info download-sample" href="<?=base_url('assets/csv/sample_subject.csv')?>"><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                        </div> -->
                   <!--  </div>
                </form> -->



                



            </div>
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
</div></div>
<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
$('.parentUpload').on('change', function() {
  $('.parent').val($(this).val());
});
$('.userUpload').on('change', function() {
  $('.user').val($(this).val());
});
$('.bookUpload').on('change', function() {
  $('.bookImport').val($(this).val());
});
$('.studentUpload').on('change', function() {
  $('.student').val($(this).val());
});


$('.teacherUpload').on('change', function() {
  $('.teacher').val($(this).val());
});

$('.subjectUpload').on('change', function() {
  $('.subject').val($(this).val());
});
</script>

<!-- <script type="text/javascript">
    $(document).ready(function () {
        $("body").on("contextmenu",function(e){
            alert("right click functionality is disabled for this page.");
            return false;
        });        
 });
 </script> -->