<style>
   .btn-green {
  background: #53af58;
  border: 1px solid #fff;
  border-radius: 100%;
  /*box-shadow: 0 -2px 0 3px #0b9512 inset, 0 5px 5px rgba(0, 7, 1, 0.17), 0 15px rgba(255, 255, 255, 0.25) inset;*/
  /*cursor: pointer;*/
  display: inline-block;
  height: 15px;
  width: 15px;
  margin: -2px 5px;
}

</style>
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-list"></i> Attendance </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Attendance</li>
         </ol>
      </div>
   </div>
   <!-- form start -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card-body">
               <div class="col-sm-12">
                  <form style="" class="form-horizontal"  autocomplete="off" role="form">
                     <?php 
                        $getClasses = $this->input->get('course');
                        
                        $getdepartment=$this->input->get('department');
                        
                        $getSubcourse = $this->input->get('subcourse');
                        
                        
                        
                        $getSemester = $this->input->get('yearsOrSemester');
                        
                        
                        
                        $getSubject = $this->input->get('subject');
                        
                        $type = $this->input->get('type');
                        if($type){
                          $type = $this->input->get('type');
                        }else{
                          $type = "filter";
                        }
                        
                        
                        $date = $this->input->get('date');
                        
                        
                        
                                           ?>
                     <?php 
                        if($type=="filter") 
                        {
                         ?>
                     <input type="hidden" name="type" value="<?php echo $type ?>">
                     <div class="col-sm-3">
                        <div class="">
                           <div class="form-group">
                              <label for="departmentID" class="control-label">
                              Department
                              </label>
                              <div class="">
                                 <?php
                                    $array = array("0" => "Select Department");
                                    
                                    foreach ($department as $department) {
                                    
                                        $array[$department->departmentID] = $department->department_name;
                                    
                                    }
                                    
                                    echo form_dropdown("department", $array, set_value("departmentID",$getdepartment), " onchange = 'Getclass($(this).val())' required class='form-control'");
                                    
                                    //onchange='ajaxGet_subCourses($(this).val())'
                                    
                                    
                                    
                                    ?>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <div class="form-group">
                              <label for="classesID" class="control-label">
                              Course
                              </label>
                              <div class="">
                                 <select id="classID" name="course" required class="form-control" onchange='GetsemesterAtd($(this).val())'>
                                  <option>Select</option>
                                    <?php  foreach ($classes as $classes)
                                    {
                                       if ($classes->classesID==$getClasses)
                                       {
                                       
                                           $selected =  "Selected";
                                       
                                       }
                                       else
                                       {
                                       
                                         $selected =  "";
                                       
                                       }
                                       
                                       ?>
                                    <option value="<?php echo $classes->classesID ?>" <?php echo $selected ?>>
                                      <?php echo $classes->classes ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                              <!-- <div class="">
                                 <?php
                                    $array = array("0" => "Select Course");
                                    
                                    foreach ($classes as $classa) {
                                    
                                        $array[$classa->classesID] = $classa->classes;
                                    
                                    }
                                    
                                    // echo form_dropdown("course", $array, set_value("classesID",$getClasses), "id='classesID' onchange = 'GetsemesterAtd($(this).val())' required class='form-control'");
                                    
                                    //onchange='ajaxGet_subCourses($(this).val())'
                                    
                                    
                                    
                                    ?>
                              </div> -->
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>

                     

                     <div class="col-sm-2">
                        <div class="">
                           <div class="form-group">
                              <label for="classesID" class="control-label">Semester/Year</label>
                              <div class="">
                                 <select class='form-control' name='yearsOrSemester' onchange='GetsubjectAtd($(this).val())' id='yearSemesterID' required value='<?php echo set_value('semesterId'); ?>'>
                                    <option>Select</option>
                                    <?php 
                                       if ($classesRow) {
                                       
                                        $looping    =  (int) $classesRow->duration;
                                       
                                       if ($classesRow->mode==1) {
                                       
                                         for ($i=1; $i <=$looping; $i++) {
                                       
                                                if (CallYears($i)==$getSemester) {
                                       
                                           $select = 'Selected';
                                       
                                        }else{
                                       
                                        $select = '';
                                       
                                        }
                                       
                                        echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                       
                                        
                                       
                                       }
                                       
                                       }
                                       
                                       
                                       
                                       else{
                                       
                                         for ($i=1; $i <=(2*$looping); $i++) {
                                       
                                       if (CallSemester($i)==$getSemester) {
                                       
                                           $select = 'Selected';
                                       
                                        }else{
                                       
                                        $select = '';
                                       
                                        }
                                       
                                        echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       
                                       }
                                       
                                       }
                                       
                                       }
                                       
                                       ?>
                                 </select>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="">
                           <div class="form-group">
                              <label for="classesID" class="control-label">Subject</label>
                              <div class="">
                                 <select id="subjectID" name="subject" required class="form-control">
                                    <?php  foreach ($subjects as $key => $value) {
                                       if ($value->subjectID==$getSubject) {
                                       
                                           $selected =  "Selected";
                                       
                                       }else{
                                       
                                         $selected =  "";
                                       
                                       }
                                       
                                       ?>
                                    <option value="<?php echo $value->subjectID ?>" <?php echo $selected ?>><?php echo $value->subject ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="">
                           <div class="form-group">
                              <div class="">
                                 <input type="submit" value="Search" name="submit" class="btn btn-success" style="margin-top: 35px;" name="Search">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <?php } ?>
                     <div class="col-sm-12" style="text-align:center; margin-top: 20px;">
                        <?php if($subject_name){  ?> 
                        <h4>Subject Name- <span><?php echo $subject_name->subject; ?></span></h4>
                        <?php } ?>
                     </div>
                     <div class="col-sm-12">
                        <div class="col-sm-2"><label for="classesID" class="control-label">Attendance Date:</label></div>
                           <div class="col-sm-2" >
                              <div class="form-group">
                              <input type="text" placeholder="Select Date" onchange="OnchageDateAtd($(this).val())"  class="form-control datepicker_quiz_data" name="date" value="<?php echo $date ?>">
                              </div>
                              
                           </div>
                           <div class="col-sm-8"><a class="btn-top check-all btn bg-success " onclick="selectAllsemester()"> Check All Present</a><a class="btn-top check-all btn bg-success " onclick="selectallabsent()"> Check All Absent</a><a class="btn-top check-all btn bg-success " onclick="UnSelectAll()"> Uncheck </a></div>
                        </div>
                           
                        <!-- </div> -->

                        <!-- Start Search Div -->
                        <!-- <div class="col-sm-12">
                          <div class="col-sm-5"></div> -->
                          <!-- <div class="col-sm-2"><label class="control-label pull-right">Search:</label></div> -->
                           <!-- <div class="col-sm-5" > -->
                              <!-- <div class="form-group">
                              <input type="text" placeholder="Select Date" onchange="OnchageDateAtd($(this).val())"  class="form-control datepicker_quiz_data" name="date" value="<?php echo $date ?>">
                              </div> -->
                              
                           <!-- </div> -->
                           <!-- <div class="col-sm-5 pull-right">
                              <input type="search" name="searchName">
                           </div>
                        </div> -->
                        <!-- End Search Div -->
                     </div>
                  </form>
                  <div class="col-sm-6">
                  </div>
                  <div class="col-sm-6">
                     <?php 
                        $usertype = $this->session->userdata("usertype");
                        
                        if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"){
                        
                        ?>
                     <div class="pull-right">
                        <div class="btn-group">
                           <a href="<?php echo base_url('professor/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span><?=$this->lang->line('add_title')?></a>
                        </div>
                     </div>
                     <?php  }  ?>
                  </div>
                  <div class="clearfix"></div>
                  <div class="table table-responsive">
                     <div class="col-sm-12">
                        <div id="hide-table">
                           <?php  if($getStudent){ ?>                 
                           <form method="post" autocomplete="off" onsubmit="return idsd()">
                              <input type="hidden" name="classesID" value="<?php echo $getClasses ?>">
                              <input type="hidden" name="sub_coursesID" value="0">
                              <input type="hidden" name="yearsOrSemester" value="<?php echo $getSemester ?>">
                              <input type="hidden" name="subjectID" id="subjectID1" value="<?php echo $getSubject ?>">
                              <input type="hidden" name="type" value="<?php echo $type ?>">
                              <input type="hidden" name="date" value="<?php echo $date ?>" id ="date_atd">
                              <div class="col-sm-4"></div>
                              <div class="col-sm-4">
                                 <div class="">
                                 </div>
                              </div>
                              <div class="col-sm-4"></div>
                              <input type="hidden" name="subjectID" value="<?php echo $_GET['subject'] ?>">
                             <div class="table-wrapper"> 
                              <table class="table table-striped table-bordered table-hover dataTable no-footer table-responsive" id="studentattendanceTableforProfessor">
                                 <thead>
                                    <tr>
                                       <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                       <th class="col-sm-2">Roll No.</th>
                                       <th class="col-sm-2">Name</th>
                                       <?php if($usertype == "Admin"){ ?>
                                       <th class="col-sm-2"><?=$this->lang->line('professor_status')?></th>
                                       <?php } ?>
                                       <th class="col-sm-2">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php if(count($getStudent))
                                    {
                                      // print_r($getStudent);die;
                                       $count=1;
                                       foreach ($getStudent as $key => $value) {
                                       
                                        ?>
                                    <tr>
                                       <td class="col-sm-2"><?php echo $key+1;  ?></td>
                                       <input type="hidden" name="keyIDs[]" value="<?php echo $key ?>">
                                       <input type="hidden" name="studentID[]" value="<?php echo $value['studentID'] ?>">
                                       <td class="col-sm-2"><?php echo $value['roll']; ?></td>
                                       <td class="col-sm-2"><?php 
                                       if($value['professorcheckstudentonline'])
                                       {
                                          if($value['professorcheckstudentonline']->activestatus==1)
                                         { ?>
                                          <span class="btn-green"></span>
                                         <?php } 
                                       }
                                        echo $value['name']; ?></td> 
                                       <!-- <td class="col-sm-2">
                                        <input type="radio" id="" name="student" value="student" disabled>
                                        <label class="radio" disabled><?php echo $value->name; ?></label>
                                        </td -->
                                       <?php if($usertype == "Admin"){ ?>
                                       <td class="col-sm-2"><?=$this->lang->line('professor_status')?></td>
                                       <?php } ?>
                                       <td class="col-sm-2">
                                          <?php if ($count_students_atd) { ?>
                                          <input type="hidden" name="atdID[]" value="<?php echo $value['atdID'] ?>">
                                          <?php } ?>
                                          <div class="custom-control custom-radio">
                                             <input type="radio" class="custom-control-input radio1 present"  id="present<?php echo $key; ?>" value = "1" <?php if($count_students_atd){if($value['atd']=='1'){echo "checked";}} ?> name="atd<?php echo $key; ?>">
                                             <label class="custom-control-label" for="present<?php echo $key; ?>">P</label>
                                             <input type="radio"  class="custom-control-input radio1 absent" id="absent<?php echo $key; ?>" value = "0" <?php if($count_students_atd){if($value['atd']=='0'){echo "checked";}} ?> name="atd<?php echo $key; ?>" >
                                             <label class="custom-control-label" for="absent<?php echo $key; ?>">A</label>
                                             <span id="atd<?php echo $key; ?>" style ="color:red;"></span>

                                             <a href="#" style="color: red; margin: -9px;" data-toggle="modal" data-target="#Modals<?php echo $count; ?>" ><button class="btn btn-danger" title="Remove Student"><i class="fa fa-remove"></i></button></a>
                                             <!-- <a href="" style="color: red"></a> -->
                                             <div class="modal" id="Modals<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                               <div class="modal-dialog modal-sm" style="margin-top: 55px;">
                                                <div class="modal-content" style="width: 500px;">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h6 class="modal-title"><?php if($subject_name){  ?> 
                                                      <span><?php echo $subject_name->subject; ?></span>
                                                      <?php } ?>
                                                    </h6>
                                                  </div>
                                                  <div class="modal-body">
                                                    <input type="hidden" name="removestudentid" id="removestudentid" value="<?php echo $value['studentID'] ?>">
                                                    <h5>Are You Sure to remove the Student permamantly from the department?</h5></div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <button type="button" id="removestudent" class="btn btn-success" data-dismiss="modal" onclick="validate(this)" value="<?php echo $value['studentID'] ?>">Yes</button>
                                                  </div>
                                                </div>
                                               </div>
                                              </div>
                                          </div>
                                       </td>
                                    </tr>
                                    <?php $count++; } }?>
                                 </tbody>
                                 <script type="text/javascript">
                                  function validate(a)
                                  {
                                      var studentID= a.value;
                                    
                                        $.ajax({
                                                type: "POST",
                                                  url: '<?php echo base_url(); ?>AjaxController/Removestudentformcource',
                                                  dataType: "json",
                                                    data: {'studentID':studentID},
                                                  success:function(data)
                                                  {
                                                    alert("Stduent is Remove Successfully");
                                                    location.reload();
                                                  }
                                              });
                                  }
                                 </script>
                              </table>
                             </div>
                              <?php  if ($count_students_atd) {  ?>
                                <div class="col-md-6" >
                                  <div style="color: red;">Attendance has already been Marked.
                                  </div>
                                  <div style="color: darkgreen; float: left;padding: 5px;">Total  =  
                                    <span id="totalstudentdata"></span>,
                                  </div>
                                  <div style="color: darkgreen; float: left;padding: 5px;"> Present   =  
                                    <span id="presentdata"></span>,
                                  </div>
                                  <div style="color: darkgreen; float: left;padding: 5px;"> Absent   =  
                                    <span id="absentdata"></span>
                                  </div>
                                </div>
                              <div class="col-sm-12" style="margin-top: 20px;">

                         <center><input type="submit" name="submit" value="Update" class="btn btn-success"></center>
                                 
                              </div>
                              
                              <?php } else{ ?>
                              <div class="">
                                 <div class="pdf_form">
                                  <center><a href="javascript:void(0)" onclick="lookUp()" >
                                  <button type="button" class="btn btn-success btn-xs mrg for_margR studentattendanceSystem" data-toggle="modal" data-target="#myModal" style="margin: 15px;">Save & Proceed</button></a></center>
                                    <!-- <button id="myBtn" class="btn-success" type="button" style="float: right;margin: 15px;"></button> -->
                                    <!-- <div id="myModal" class="modal"> -->
                                      <!-- <span class="close">&times;</span> -->
                                      <!-- <div class="modal-content">    -->
                                      <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                      </div>
                                      <script type="application/javascript">

                                      function lookUp() {
                                       var subjectid=$('#subjectID1').val();
                                       var date='<?php echo $this->input->get('date')?>';
                                       var yearsOrSemester='<?php echo $this->input->get('yearsOrSemester')?>';
                                     // alert(subjectid);
                                       $.post('<?php echo base_url();?>AjaxController/getcompletunitbysubject',

                                      {

                                      subjectid: subjectid,date:date,yearsOrSemester:yearsOrSemester
                                      }, function (data)

                                      {

                                        $("#myModal").html(data);

                                      });

                                      }

                                    </script>
                                    </div>
                                    
                                 </div>
                              </div>
                              <?php } ?>
                           </form>
                           <?php } else{ ?>
                           <div class="Noresuls">
                              <h1>Sorry we couldn't find any matches</h1>
                              <p>Maybe your search was too specific, please try searching with another term.</p>
                              <img src="https://www.itmddn.online/uploads/images/crying.png">
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                  </div>
                  <script type="text/javascript">
                     function idsd(){
                      var atd = $("#date_atd").val();
                      // var topicid=$("#topic").val();
                           var check = true;
                           $("input:radio").each(function(){
                               var name = $(this).attr("name");
                               if($("input:radio[name="+name+"]:checked").length == 0){

                                   check = false;
                                   $("#"+name).html('Please Select');
                                   
                               }
                           });
                           
                           if(check){
                               if(atd=='')
                               {
                                  alert('Please select attendance date.');
                                check = false;
                                // $('#myModal').modal('hide');
                               }

                               return check;
                           }else{
                                
                               if(check==false){
                               
                            
                            $('#myModalLabel').html("Please Check Attendance!!");
                                return check;
                               }
                             return check;
                           }
                      
                     
                     }
                  </script>
               </div>
               <!-- col-sm-12 -->
            </div>
            <!-- row -->
         </div>
         <!-- Body -->
      </div>
      <!-- /.box -->
   </div>
<script type="text/javascript">
   function GetsubjectAtd(val){
    var classesID =  $('#classID').val();
   
         $.ajax({
   
         url: base_url+'AjaxController/subject_get',
   
         type: 'POST',
   
          data:{val:val,classesID:classesID},
   
         success: function(data){
   
         $('#subjectID').html(data);
   
         }
   
     }); 
   
   
   
   }
   
</script>
<script type="text/javascript">
   function GetsemesterAtd(classesID){
   
   
                  $.ajax({
   
                  url: base_url+'AjaxController/semester_get',
   
                  type: 'POST',
   
                  data:{classesID:classesID},
   
                  success: function(data){
   
                    console.log(data);
   
                  $('#yearSemesterID').html(data);
   
                  }
   
              }); 
   
   
    }
</script>
<script type="text/javascript">
   function GetsemesterAtd(classesID){
   
   
                  $.ajax({
   
                  url: base_url+'AjaxController/semester_get',
   
                  type: 'POST',
   
                  data:{classesID:classesID},
   
                  success: function(data){
   
                    console.log(data);
   
                  $('#yearSemesterID').html(data);
   
                  }
   
              }); 
   
   
    }
</script>
<script type="text/javascript">
   function Getclass(departmentID){
   
   // alert(departmentID);die;
                  $.ajax({
   
                  url: base_url+'AjaxController/class_get',
   
                  type: 'POST',
   
                  data:{departmentID:departmentID},
   
                  success: function(data){
   
                    console.log(data);
   
                  $('#classID').html(data);
   
                  }
   
              }); 
   
   
    }
</script>
<script type="text/javascript">
   function OnchageDateAtd(date){
    var department="<?php echo $this->input->get('department') ?>";
   var course =  "<?php echo $this->input->get('course') ?>";
   var subcourse =  "<?php echo $this->input->get('subcourse') ?>";
   var yearsOrSemester =  "<?php echo $this->input->get('yearsOrSemester') ?>";
   var subject =  "<?php echo $this->input->get('subject') ?>";
   var type =  "<?php echo $this->input->get('type') ?>";
   var url = "<?php echo base_url() ?>viewlecture/attendance?type="+type+"&department="+department+"&course="+course+"&subcourse=0&yearsOrSemester="+yearsOrSemester+"&subject="+subject+"&date="+date;
   window.location.replace(url);
   
   }
   
</script>
<script>
   var status = '';
   
   
   
   var id = 0;
   
   
   
   $('.onoffswitch-small-checkbox').click(function() {
   
   
   
       if($(this).prop('checked')) {
   
   
   
           status = 'chacked';
   
   
   
           id = $(this).parent().attr("id");
   
   
   
       } else {
   
   
   
           status = 'unchacked';
   
   
   
           id = $(this).parent().attr("id");
   
   
   
       }
   
   
   
   
   
   
   
       if((status != '' || status != null) && (id !='')) {
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('professor/active')?>",
   
   
   
               data: "id=" + id + "&status=" + status,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                   if(data == 'Success') {
   
   
   
                       toastr["success"]("Success")
   
   
   
                       toastr.options = {
   
   
   
                         "closeButton": true,
   
   
   
                         "debug": false,
   
   
   
                         "newestOnTop": false,
   
   
   
                         "progressBar": false,
   
   
   
                         "positionClass": "toast-top-right",
   
   
   
                         "preventDuplicates": false,
   
   
   
                         "onclick": null,
   
   
   
                         "showDuration": "500",
   
   
   
                         "hideDuration": "500",
   
   
   
                         "timeOut": "5000",
   
   
   
                         "extendedTimeOut": "1000",
   
   
   
                         "showEasing": "swing",
   
   
   
                         "hideEasing": "linear",
   
   
   
                         "showMethod": "fadeIn",
   
   
   
                         "hideMethod": "fadeOut"
   
   
   
                       }
   
   
   
                   } else {
   
   
   
                       toastr["error"]("Error")
   
   
   
                       toastr.options = {
   
   
   
                         "closeButton": true,
   
   
   
                         "debug": false,
   
   
   
                         "newestOnTop": false,
   
   
   
                         "progressBar": false,
   
   
   
                         "positionClass": "toast-top-right",
   
   
   
                         "preventDuplicates": false,
   
   
   
                         "onclick": null,
   
   
   
                         "showDuration": "500",
   
   
   
                         "hideDuration": "500",
   
   
   
                         "timeOut": "5000",
   
   
   
                         "extendedTimeOut": "1000",
   
   
   
                         "showEasing": "swing",
   
   
   
                         "hideEasing": "linear",
   
   
   
                         "showMethod": "fadeIn",
   
   
   
                         "hideMethod": "fadeOut"
   
   
   
                       }
   
   
   
                   }
   
   
   
               }
   
   
   
           });
   
   
   
       }
   
   
   
   }); 
   
   
   
</script>


<!-- <script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close");

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script> -->
<!-- <script type="text/javascript">
  $('#myBtn').click(function()
    {
       var subjectid=$('#subjectID').val();
       // alert(subjectid);
       

       if($('.present').is(":checked"))
       {
           $.ajax({
   
                  url: base_url+'AjaxController/getcompletunitbysubject',
   
                  type: 'POST',
   
                  data:{subjectid:subjectid},
   
                  success: function(data)
                  {
                    if(data)
                    {
                      $('#myModal').html(data);
                    }
                    
   
                  }
   
              }); 
       }
       else
       {
        
          var data="Please take Attendance!!";
          alert(data);
          location.reload();
      
       }
    });
</script> -->
<script type="text/javascript">
  function selectAllsemester(){

      var items=document.getElementsByClassName('present');
      for(var i=0; i<items.length; i++){
          if(items[i].type=='radio')
              items[i].checked=true;
      }
     
      
  }
  
  function UnSelectAll(){
      var items=document.getElementsByClassName('radio1');

      for(var i=0; i<items.length; i++){
          if(items[i].type=='radio')
              items[i].checked=false;
      }
      
  }

  function selectallabsent()
  {
    var items=document.getElementsByClassName('absent');
    // var itemC = items.length;
      for(var i=0; i<items.length; i++){
          if(items[i].type=='radio')
              items[i].checked=true;
            // var itemC = items.length;
      }
      
  }


</script>
 <script>
    $(document).ready(function() {

        var items1= $('.absent').val();
        var items2= $('.present').val();
        var a=0;
        var p=0;
        var items=document.getElementsByClassName('absent');

      for(var i=0; i<items.length; i++){
        if(items[i].type=='radio')
        {

          if(items[i].checked)
          {
           
            a++;
          }else{
            p++;
          }

        }
              
        
      }
      
        document.getElementById("totalstudentdata").innerHTML = items.length;
        document.getElementById("absentdata").innerHTML = a;
        document.getElementById("presentdata").innerHTML = p;
     
    });
  </script>







