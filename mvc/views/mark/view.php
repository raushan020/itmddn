
    <style type="text/css">
        * {box-sizing: border-box;}
        .result {
            border: 1px solid #333;
            max-width: 1140px;  
            display: block;
            margin: 0 auto;
        }
        .result .result-header{
            background:#00dcff1a;
            padding: 10px;
            border-top: 10px solid #01e001b0;
            margin: 0;
        }
        .result .result-header table tr td {
            padding: 2px 10px;
            text-align: left;
        } 
        .result .main-result {
            margin: 10px;
            border: 1px solid #333;
            padding: 20px 10px;
        }
        .result .main-result table tr th {
            vertical-align: top;
            padding-bottom: 5px;
            padding-left: 10px;
        }
        .result .main-result table tr td {
            padding: 5px 10px;
            text-align: left;
        }
        .result .result-footer {
            background: #00dcff1a;
            padding: 30px 10px;
            margin: 0;
            font-size: 15px;
        }
        .allSignleFilter{
            width:50%;
            margin:auto;
        }
    </style>
</head>
<body>
    <?php
// header("Content-Type: application/xls");    
// header("Content-Disposition: attachment; filename=filename.xls");  
// header("Pragma: no-cache"); 
// header("Expires: 0");

     ?>
     <div class="">

      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-check-square-o"></i> Result</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active"> Result</li>
          </ol>
        </div>
      </div>

        <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">
                  <div class="allSignleFilter">
                    <div class="col-sm-12 theme_input_blue">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="">
                                Semester/Year
                                </label>
                                <div class="">
                                
                                <?php 
    if ($classes) {
     $looping    =  (int) $classes->duration;
   if ($classes->mode==1) {
     echo"<select class='form-control' name='yearID' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='set_value('yearId');'>
     <option value = 'all'>Select</option>";
      for ($i=1; $i <=$looping; $i++) {
     if (CallYears($i)==$this->session->userdata('FilterMarkyearSemesterID')) {
        $select = 'Selected';
     }else{
     $select = '';
     }
     echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
     
   }
   echo  "</select>";
}

else{
    
     echo"<select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'><option value = 'all'>Select</option>";
      for ($i=1; $i <=(2*$looping); $i++) {

     if (CallSemester($i)==$this->session->userdata('FilterMarkyearSemesterID')) {
        $select = 'Selected';
     }else{
     $select = '';
     }

     echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
   }
   echo  "</select>";
   }
}

                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <div class="clearfix"></div>



<!-- new -->
    <?php 
 if ($this->session->userdata('FilterMarkyearSemesterID')) {
    if ($exams) {
  ?>
    <div class="result">
        <div class="row result-header">
            <div class="col-md-7">
                <table>
                    <tr>
                        <td><strong>Name of Programme</strong></td>
                        <td>:</td>
                        <td><?php echo $classes->classes ?>
                        <br>
                          <?php
if (isset($sub_course->sub_course)) {
  echo "with";
         echo $sub_course->sub_course ;} ?></td>
                    </tr>
                    <tr>
                        <td><strong>Examination</strong></td>
                        <td>:</td>
                        <td>Dec-2017</td>
                    </tr>
                    <tr>
                        <td><strong>Student Name</strong></td>
                        <td>:</td>
                        <td><?php echo $student->username ?></td>
                    </tr>
                    <tr>
                        <td><strong>Father Name</strong></td>
                        <td>:</td>
                        <td><?php echo $student->father_name  ?></td>
                    </tr>
                    <tr>
                        <td><strong>Mother Name</strong></td>
                        <td>:</td>
                        <td><?php echo $student->mother_name  ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-5">
                <table>
                    <tr>
                        <td><strong>Semester</strong></td>
                        <td>:</td>
                        <td><?php echo  str_replace('_', ' ', $exams->yearsOrSemester) ?></td>
                    </tr>
                    <tr>
                        <td><strong>Enrollment No.</strong></td>
                        <td>:</td>
                        <td><?php echo $student->roll  ?></td>
                    </tr>
                    <tr>
                        <td><strong>Roll No.</strong></td>
                        <td>:</td>
                        <td><?php echo $student->username ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row main-result">
            <div class="col-md-12">
                <table>
                    <tr>
                        <th class="text-uppercase col-md-4">Paper Code Title</th>
                        <th class="text-uppercase col-md-2">Internal Assesment <br/><span class="text-capitalize">MM/Ob. Marks</span></th>
                        <th class="text-uppercase col-md-2">External Assesment <br/><span class="text-capitalize">MM/Ob. Marks</span></th>
                        <th class="text-uppercase col-md-2">Total <br/><span class="text-capitalize">MM/Ob. Marks</span></th>
                        <th class="text-uppercase col-md-2">Result</th>
                    </tr>
                     <?php 
                     $totalMark = 0;
                     $code =1;
                     $last_review = 0; 
                     $totalMM = 0;
                        foreach ($marks as $key => $value) {
                        $totalMM += 1;
                         $afterLa  = $totalMM*100;
                         ?>
                    <tr>
                        <td class="col-md-4">
    <?php 
    $subCode = explode('_', $exams->yearsOrSemester);
preg_match_all('!\d+!', $subCode[0], $matches);
                            echo $matches[0][0];
                            echo 0;
                            echo $code++;
                             ?>
                            :<?php echo $value->subject  ?>
                        </td>
                        <?php 
if ($value->mark<28) {
if ($value->mark==0) {
$total_subject_mark  = 0;
}else{
 $total_subject_mark  = 30;
}
}
else{
  $total_subject_mark  = $value->mark;
}                        
$totalMarkSubject  = $value->intMark+round($total_subject_mark);
  $totalMark += $totalMarkSubject;
  $subjectPEcr =  round($total_subject_mark)*100/70;


                         ?>
                        <td class="col-md-2">30/<?php echo $value->intMark ?></td>
                        <td class="col-md-2">70/<?php echo round($total_subject_mark)  ?></td>
                        <td class="col-md-2"><?php echo $totalMarkSubject; ?></td>
                        <td class="col-md-2">
                                <?php 
                                if ($subjectPEcr>=40) {
                                 echo "P";
                                }else{
                                    if ($subjectPEcr==0) {
                                      $last_review += 1;
                                      echo 'NA';
                                    }else{
                                      echo "F";
                                    }
                                    
                                }

                                 ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div class="row result-footer">
            <div class="col-md-12">
                <div class="col-md-4">
                    <p><strong>Max. Marks</strong></p>
                    <p><?php echo $afterLa ?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>Marks Obtained</strong></p>
                    <p><?php echo $totalMark; ?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>Result</strong></p>
                    <p><?php
                if ($totalMark/$afterLa*100>=35) {
                    echo "PASS";
                }else{
                    echo "FAIL";
                }

                 ?></p>
                </div>
            </div>
            <div class="col-md-12">
                <p><strong>Notes &amp; Explanation:</strong></p>
                <ol>
                    <li>In case of any mistake being detected in the preparation of the marks statement at any stage of when it is brougt to the notice of the concerned authority, the university shall have the right gto make the necessary correction.</li>
                    <li>Abbreviations: MM = Maximum Marks, MO = Marks Obtained, BP = Back Paper, AA=Absent.</li>
                    <li>This is a computer generated Provisional Marks Statement and hence does not require signature for verification refer to original marks statement.</li>
                    <li>In case of any error in this statement of marks, it should be reported within 15 days.</li>
                </ol>
            </div>
        </div>
    </div>
</div>
</div>
    <?php }  else { ?>
        <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
<p>Maybe your search was too specific, please try searching with another term.</p>
<img src="<?php echo base_url() ?>uploads/images/crying.png">
</div>
  <?php } } ?>
</body>
</html>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();
            $.ajax({

                type: 'POST',

                url: "<?=base_url('mark/view')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">
        function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('mark/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
</script>

 <script src="<?php echo base_url('assets/inilabs/table2excel.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "Table.xls"
                });
            });
        });
    </script>