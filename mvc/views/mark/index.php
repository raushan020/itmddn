  <div class="">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-book"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active"><?=$this->lang->line('menu_subject')?></li>
            </ol>
        </div>
    </div>
   <!-- /.box-header -->

    <!-- form start -->
   <div class="container-fluid">
      <div class="row">         
        <div class="card">
            <div class="card-body">    

    <div class="box-body">
     
        <div class="row">
            <div class="col-sm-12">




                <div class="theme_input_blue">
            <?php 
            $usertype = $this->session->userdata("usertype");

                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin")
               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
               $display =  "block";
               $addmorebutton = "none";
               $Removemorebutton = "inline-block";
               }else{
               $display = "none";
               $addmorebutton = "inline-block";
               $Removemorebutton = "none";
               }
               
               ?>
                
 


                <div class="col-sm-4">
                    
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    
                </div>
            </div>
            <div class="clearfix"></div>
            



                <?php

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin") {
?>
              <div class="col-sm-4 list-group for-formGroup ">

                    <!-- <div class="list-group-item list-group-item-warning">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="col-sm-3 control-label">

                                Semester/Year

                                </label>

                                <div class="col-sm-9">
                                       
                                <?php 

    if ($classesRow) {
     $looping    =  (int) $classesRow->duration;
   if ($classesRow->mode==1) {
     echo"<select class='form-control' name='yearID' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='set_value('yearId');'>
     <option value = 'all'>All</option>";
      for ($i=1; $i <=$looping; $i++) {
     if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
        $select = 'Selected';
     }else{
     $select = '';
     }
     echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
     
   }
   echo  "</select>";
}

else{
    
     echo"<select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'><option value = 'all'>All</option>";
      for ($i=1; $i <=(2*$looping); $i++) {

     if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
        $select = 'Selected';
     }else{
     $select = '';
     }

     echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
   }
   echo  "</select>";
   }
}

                                ?>

                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div> -->

                </div>
                <div class="clearfix"></div>

                <?php } ?>
                <div id="hide-table">

                    <table id="resultShow" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">

                        <thead>

                            <tr>

                                <th><?=$this->lang->line('slno')?></th>

                                <th>Student Name</th>
                                <th>Semester/Year</th>

                                <?php  if($usertype == "Admin") { ?>

                                <th><?=$this->lang->line('action')?></th>

                                <?php } ?>

                            </tr>

                        </thead>



                        <tbody>

                            <?php if(count($subjects)) {$i = 1; foreach($subjects as $subject) { ?>

                                <tr>

                                    <td data-title="<?=$this->lang->line('slno')?>">

                                        <?php echo $i; ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('subject_code')?>">

                                        <?php echo $subject->subject_code; ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php echo $subject->subject; ?>

                                    </td>

                                    <td data-title="Semester/Year">

                                    <?php echo $subject->yearsOrSemester; ?>

                                    </td>

                                    <?php  if($usertype == "Admin") { ?>

                                    <td data-title="<?=$this->lang->line('action')?>">

                                        

                                    </td>

                                    <?php } ?>

                                </tr>

                            <?php $i++; }} ?>

                        </tbody>

                    </table>

                </div>



            </div>

        </div>

    </div>

</div>
</div>
</div>
</div>
</div>



<!-- <iframe id='fp_embed_player' src='https://ec2-13-233-113-212.ap-south-1.compute.amazonaws.com:8888/embed_player?urlServer=wss://ec2-13-233-113-212.ap-south-1.compute.amazonaws.com:8443&streamName=stream&mediaProviders=WebRTC,Flash,MSE,WSPlayer' marginwidth='0' marginheight='0' frameborder='0' width='100%' height='100%' scrolling='no' allowfullscreen='allowfullscreen'></iframe> -->



<script type="text/javascript">

    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('subject/subject_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });

</script>
<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">
    function ResetSemesterYear(){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('subject/ResetSemesterYear')?>",
            data:{ResetSesession:'ResetSesession'},
            dataType: "html",
            success: function(data) {
                location.reload();
            }
        });
    }
</script>
