		<style>
	.table>thead:first-child>tr:first-child>th{
		border-top: 1px solid #000; 
	}
	.table>thead>tr>th{
		border: 1px solid #000;
	}
	.table>tbody>tr>td{
		border: 1px solid #000;
	}
</style>
<div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-sticky-note-o"></i> Result</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("mark/index")?>">result</a></li>
            <?php $usertype = $this->session->userdata("usertype");
			if($usertype=="Student")
			{

			}
			else
			{ ?>
				<li class="active"><?=$this->lang->line('menu_add')?> marksheet</li>
		<?php } ?>
            
         </ol>
      </div>
   </div>
		<div class="container-fluid">
			<?php $usertype = $this->session->userdata("usertype");
			if($usertype=="Student")
			{

			}
			else
			{ ?>
				<a href="<?=base_url("mark/index")?>"><button class="btn btn-link" style="margin-bottom: 10px;"><i class="fa fa-angle-double-left"></i>  BACK</button></a>
		<?php } ?>
			
			<div class="row">
				<div class="">
					<div class="card" style="color: #000;">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-1 col-xs-1">
										<img src="<?php echo base_url() ?>uploads/images/ITM LOGO1.png">
									</div>
									<div class="col-md-11 col-xs-11">
										<h4 style="text-align: center;font-weight: 600">Institute of Technology & Management</h4>
										<p style="text-align: center;">Affiliated to HNB Garhwal Central University, Srinagar Garhwal</p>
										<p style="text-align: center;">& Sri Dev Suman Uttarakhand Vishwavidhalaya, Tehri Garhwal</p>
										<h4 style="text-align: center;font-weight: 600;">MARKS STATEMENT</h4>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-6 col-xs-6" style="padding: 5px;"><span style="font-weight: 600;">Course:</span> <span><?php echo $studentData->classes; ?></span>
									</div>
									<div class="col-md-6 col-xs-6" style="padding: 5px;"><span style="font-weight: 600;">Roll No:</span> <span><?php echo $studentData->roll; ?></span>
									</div>

									<div class="col-md-6 col-xs-6" style="padding: 5px;"><span style="font-weight: 600;">Semester/Year:</span> <span><?php echo $yearsOrSemester; ?></span>
									</div>
									<div class="col-md-6 col-xs-6" style="padding: 5px;"><span style="font-weight: 600;">Session:</span> <span><?php echo $studentData->session; ?></span>
									</div>

									<div class="col-md-12 col-sm-12" style="padding: 5px;"><span style="font-weight: 600;">Name:</span> <span><?php echo $studentData->name; ?></span>
									</div>

									<div class="col-md-12 col-sm-12" style="padding: 5px;"><span style="font-weight: 600;">Father name:</span> <span><?php echo $studentData->father_name; ?></span>
									</div>

									<div class="col-md-12" style="padding: 5px;"><span style="font-weight: 600;">Mother name:</span> <span><?php echo $studentData->mother_name; ?></span>
									</div>
									
								</div>
							</div>
							<div class="table-responsive"> 
								<div class="table-responsive"> 
									<div class="table-responsive"> 
										<table class="table">
											<thead>
												<tr>
													<th style="font-weight: 600;">S.No.</th>
													<th style="font-weight: 600;">Paper Code</th>
													<th style="font-weight: 600;">Paper Name</th>
													<th style="font-weight: 600;">Full Marks</th>
													<th style="font-weight: 600;">Marks Obtained</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$mark_ob_total = 0; 
												$mark_total = 0; 
													foreach ($resultData as $key => $value) {
													$mark_ob_total += $value['ob'];
													$mark_total += $value['r_noqu'];
												?>
													<tr>
														<td><?=$key+1?></td>
														<td><?php echo $value['subject_code'] ?></td>
														<td><?php echo $value['subject'] ?></td>
														<td><?php echo $value['r_noqu'] ?></td>
														<td><?php echo $value['ob'] ?></td>
													</tr>

												<?php		
													}
												?>												
											</tbody>
										</table>
									</div>
								</div>
								</table>
								<div class="col-md-12">
						

									<div class="col-md-12">
									<div class="col-md-4 col-xs-4" style="padding: 5px;"><span style="font-weight: 600;">Total Aggregate:</span> <span><?php echo $mark_total ?></span>
									</div>
									<div class="col-md-3 col-xs-3" style="padding: 5px;"><span style="font-weight: 600;"></span> <span></span>
									</div>
									<div class="col-md-4 col-xs-5" style="padding: 5px;"><span style="font-weight: 600;">Aggregate Earned :</span> <span><?php echo $mark_ob_total ?></span>
									</div>
							
									</div>
									<div class="col-md-12">
									<div class="col-md-4 col-xs-4" style="padding: 5px;"><span style="font-weight: 600;">Date of Issue:</span> <span>26-02-2021</span>
									</div>
									<div class="col-md-3 col-xs-3" style="padding: 5px;"><span style="font-weight: 600;"></span> <span></span>
									</div>
									<div class="col-md-4 col-xs-5" style="padding: 5px;"><span style="font-weight: 600;">Percentage :</span> <span><?php 
										$cool = ($mark_ob_total*100)/$mark_total;
									echo number_format($cool, 2); ?></span>

				
									
								</div>
								</div>
							</div>

						</div>

					</div>
					<!-- ./Striped Table -->

				</div>
			</div>
			<!-- /row -->