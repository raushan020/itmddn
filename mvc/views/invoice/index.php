<!-- <style>
   .action-layout ul li {
   width: 275px;
   }
</style> -->
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-rupee"></i> Fee </h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active">Fee</li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                     <?php
                        $usertype = $this->session->userdata("usertype");
                        
                        if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Teacher") {
                        
                        ?>
                        <div class="col-xs-12">
                           <div class="pull-right">
                              <div class  = "form-group">

                                 <select class="form-control" id="chenge_fee">
                                    <option value="">Select Fee Type</option>
                                  <option value="1" <?php if($this->session->userdata('chenge_fee')==1){echo "Selected";} ?>>Previus Due Fees</option>
                                  <option value="2" <?php if($this->session->userdata('chenge_fee')==2){echo "Selected";} ?>>Current Year Fees</option>
                                 </select>

                              </div>
                           </div>
                        </div>
                     <div class="col-md-4 col-sm-6">
                        <a>
                           <div class="widget smart-standard-widget">
                              <div class="row">
                                 <div class="widget-caption info">
                                    <div class="col-xs-4 no-pad zoom">
                                       <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                                    </div>
                                    <div class="col-xs-8 no-pad">
                                       <div class="widget-detail">
                                          <!-- <span><i class="fa fa-inr" aria-hidden="true"></i> </span> -->
                                          <h3 style="color: #9c1855"><i class="fa fa-inr" aria-hidden="true"></i> <?php 
                                             if ($total_revenue) {
                                               echo number_format($total_revenue->TotalAmount);
                                             }else{
                                              echo 0;
                                             }
                                              ?></h3>
                                          <span>Total Revenue</span>
                                       </div>
                                    </div>
                                    <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:60%; background: #9c1855" class="widget-horigental-line"></span>
                     </div>
                  </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-md-4 col-sm-6">
                        <a >
                           <div class="widget smart-standard-widget">
                              <div class="row">
                                 <div class="widget-caption info">
                                    <div class="col-xs-4 no-pad zoom">
                                       <i class="icon fa fa-money" style="background: #007bff;color: #ffffff"></i>
                                    </div>
                                    <div class="col-xs-8 no-pad">
                                       <div class="widget-detail">
                                          <span></span>
                                          <h3 style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i><?php
                                             if ($total_revenue) {
                                               echo number_format($total_revenue->Totalpaidamount);
                                             }else{
                                              echo 0;
                                             }
                                              ?></h3>
                                          <span>Total Paid</span>
                                       </div>
                                    </div>
                                    <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%; background: #007bff;" class="widget-horigental-line"></span>
                     </div>
                  </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-md-4 col-sm-6">
                        <a>
                           <div class="widget smart-standard-widget">
                              <div class="row">
                                 <div class="widget-caption info">
                                    <div class="col-xs-4 no-pad zoom">
                                       <i class="icon fa fa-warning" style="background: #434348;"></i>
                                    </div>
                                    <div class="col-xs-8 no-pad">
                                       <div class="widget-detail">
                                          <span></span>
                                          <h3 style="color: #434348;"><i class="fa fa-inr" aria-hidden="true"></i>  <?php 
                                             if ($total_revenue) {
                                             echo number_format($total_revenue->TotalAmount-$total_revenue->Totalpaidamount);
                                             }else{
                                             echo 0;
                                             }
                                             ?></h3>
                                          <span>Total due</span>
                                       </div>
                                    </div>
                                    <div class="col-xs-12">
                                       <div class="widget-line bg-success">
                        <span style="width:48%; background: #434348;" class="widget-horigental-line"></span>
                     </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-sm-6 nopading"></div>
                     <div class="col-sm-6 nopading">
                        <div class="pull-right">
                           <div class="btn-group">
                              <a href="#" onclick="ResetAllfilter_student()" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-minus"></span> Reset All Filters</a>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-sm-3">
                        <div class="">
                           <form style="" class="form-horizontal" role="form" method="post" >
                              <div class="form-group">
                                 <label for="classesID" class="control-label">
                                 Courses
                                 </label>
                                 <div class="selectdiv">
                                    <?php
                                       $array = array("0" => 'Courses');
                                       
                                       
                                       
                                       foreach ($classes as $classa) {
                                           $array[$classa->classesID] = $classa->classes;
                                       }
                                       
                                       
                                       
                                       echo form_dropdown("classesID", $array, set_value("classesID_invoice",$this->session->userdata('classesID_invoice')), "id='classesID' class='form-control'");
                                       
                                       
                                       
                                       ?>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <form style="" class="form-horizontal" role="form" method="post">
                              <div class="form-group">
                                 <div>
                                    <label for="classesID" class="control-label">
                                    Session
                                    </label>
                                 </div>
                                 <?php
                                    $arraySessionFilter = explode('-', $this->session->userdata('sessionFilter_invoice'));
                                     ?>
                                 <div class="col-sm-6 nopading">
                                    <div class="forpostionReletive">
                                       <select class="form-control" id="SessionFrom">
                                          <option>From</option>
                                          <?php for($i=2015; $i<=2025; $i++){ ?>
                                          <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[0]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                          <?php } ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-sm-6 nopading">
                                    <div class="forpostionReletive">
                                       <select class="form-control" id="SessionTo">
                                          <option>To</option>
                                          <?php for($i=2015; $i<=2025; $i++){ ?>
                                          <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[1]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                          <?php } ?>
                                       </select>
                                       <!-- <input type="text" name="" id="SessionTo" value="<?php if(count($arraySessionFilter)==2) {echo $arraySessionFilter[1]; } ?>" class="form-control CalenderYear" placeholder="To">
                                          <div class="postionAbsoluter"><i class="fa fa-calendar" aria-hidden="true"></i></div> -->
                                    </div>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="forReset"><a style="cursor:pointer;" onclick="ResetSesession()" id="ResetSesession">Reset This Filter </a></div>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <form style="" class="form-horizontal" role="form" method="post">
                              <div class="form-group">
                                 <label for="classesID" class="control-label">
                                 Year/Semester
                                 </label>
                                 <div class="selectdiv">
                                    <select name="yos_filter" id="yos_filter" class='form-control'>
                                       <option>Year/Semester</option>
                                       <option value="1st_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="1st_Year") {echo "selected";}else{echo "";}?>>1st Year</option>
                                       <option value="2nd_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="2nd_Year") {echo "selected";}else{echo "";}?>>2nd Year</option>
                                       <option value="3rd_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="3rd_Year") {echo "selected";}else{echo "";}?>>3rd Year</option>
                                       <option value="4th_Year" <?php if ($this->session->userdata('yos_filter_invoice')=="4th_Year") {echo "selected";}else{echo "";}?>>4th Year</option>
                                    </select>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="forReset" ><a style="cursor:pointer;" onclick="Resetyos()">Reset This Filter </a></div>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="">
                           <form style="" class="form-horizontal" role="form" method="post">
                              <div class="form-group">
                                 <label for="classesID" class="control-label">
                                 Payment Status
                                 </label>
                                 <div class="selectdiv">
                                    <select name="payment_status" id="payment_status" class='form-control'>
                                       <option>Payment Status</option>
                                       <option <?php if($this->session->userdata('payment_status_invoice')==2){echo "selected";}else{echo '';} ?> value="2">Paid</option>
                                       <option <?php if($this->session->userdata('payment_status_invoice')==1){echo "selected";}else{echo '';} ?> value="1">Partial Paid</option>
                                       <option <?php if($this->session->userdata('payment_status_invoice')==3){echo "selected";}else{echo '';} ?> value="3">Not paid</option>
                                    </select>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="forReset" ><a style="cursor:pointer;" onclick="Resetpaymentstatus()">Reset This Filter </a></div>
                              </div>
                           </form>
                        </div>
                     </div>
                     <form action="<?php echo base_url() ?>invoice/mail" method = "post">
                     <div class="action-layout">
                        <ul>
                           <li>
                              <label class="nexCheckbox">Check
                              <input type="checkbox" name="select_all" id="select_all">
                              <span class="checkmark checkmark-action-layout"></span>
                              </label>
                           </li>
                           <li style=" width: 275px;">
                              <a class="btn btn-success etsfilertButton" id="send_mail">Send Fee Weekly Report to Superadmin</a>
                           </li>
                           <li>
                              <a href="#modal-2" class="btn btn-warning etsfilertButton disabled" name="sendmailbyadmin" data-toggle="modal" data-target="#modal-2" disabled>Send Mail</a>
                               <!-- <input type="button" class="btn btn-warning etsfilertButton disabled" name="sendmailbyadmin" data-toggle="modal" data-target="#myModal1" value="Send Mail" disabled> -->
                           </li>
                        </ul>
                     </div>
                     <div class="clearfix"></div>
                     <div class="" style="">
                        <!-- Button to Open the Modal -->
                        <!-- The Modal --> 
                     </div>
                     <table id="invoice_data_table" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">
                        <thead>
                           <tr>
                              <!--  <th >S.no</th> -->
                              <th>#</th>
                              <th>S.no</th>
                              <th>Student's Name / Username</th>
                              <th>Year</th>
                              <th>Total Amount</th>
                              <th>Paid Amount</th>
                              <th>Due Amount</th>
                              <th><?=$this->lang->line('action')?></th>
                           </tr>
                        </thead>
                     </table>
                      <!-- Modal Start-->
                            <!-- Modal Start-->
                     <div class="modal modal-box-2 fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                       <div class="modal-dialog">
                         <div class="modal-content" id="myModalLabel">
                           <div class="modal-header theme-bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                           </div>
                          <div class="modal-body">
                            <h3>Custom <span>Mail</span></h3>
                            <!-- <form name="sentMessage" class="contactForm"> -->
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="subjectname" name="subjectname" placeholder="Enter subject name" >
                                    <p class="help-block text-danger"></p>
                                  </div>
                                  <div class="form-group">
                                    <textarea class="form-control" id="writemail" name="writemail" placeholder="Write Your Message Here"></textarea>  
                                    <p class="help-block text-danger"></p>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                  <div id="success"></div>
                                  <input type="submit" class="btn btn-default etsfilertButton disabled" name="sendmailbyadmin" value="Send Mail">
                                </div>
                              </div>
                            <!-- </form> -->
                          </div>
                        </div>
                      </div>
                    </div>
                          <!-- MOdel End -->
                     </form>
                     <?php } ?>
                     <?php if($usertype=='Student' || $usertype == "Parent"){  ?>
                        <div class="col-md-12" style="text-align: center;padding-bottom: 15px;">
                           <button class="btn btn-warning" data-toggle="modal" data-target="#reminderbystudent">Your Fee is not updated! Send Reminder
                              <span class="email-notify" style="padding: 0px 4px;background: green"> New</span></button>
                        </div>
                        
                        
                        <div class="modal modal-box-1 fade" id="reminderbystudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                           <div class="modal-dialog" style="width: 350px;margin-top: 60px;">
                             <div class="modal-content" id="myModalLabel">
                              <div class="modal-body">

                                <form id="contactForm" action="<?php echo base_url('customemail/reminderbystudent'); ?>" method="post">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <p>Is Your Fee Updated Yes/No ?</p>
                                      </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-12 text-center">
                                      <button type="button" class="btn btn-info" data-dismiss="modal" style="padding: 6px 16px;min-width: 110px;">Yes</button>
                                      <button type="submit" class="btn btn-success" style="padding: 6px 16px;min-width: 110px;">No</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>

                     <div class="table-wrapper" id="hide-table">
                        <table id="noticeAdmin" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">
                           <thead>
                              <tr>
                                 <th><?=$this->lang->line('slno')?></th>
                                 <th>Year</th>
                                 <th><?=$this->lang->line('invoice_due')?></th>
                                 <th><?=$this->lang->line('invoice_status')?></th>
                                 <th><?=$this->lang->line('action')?></th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php  if(count($invoices)) {$i = 1; foreach($invoices as $invoice) { ?>
                              <tr>
                                 <td data-title="<?=$this->lang->line('slno')?>">
                                    <?php echo $i; ?>
                                 </td>
                                 <td><?php echo str_replace('_', " ", $invoice->yearsOrSemester)  ?></td>
                                 <td data-title="<?=$this->lang->line('invoice_due')?>">
                                    <?php echo $siteinfos->currency_symbol. ($invoice->amount - $invoice->paidamount); ?>
                                 </td>
                                 <td data-title="<?=$this->lang->line('invoice_status')?>">
                                    <?php 
                                       $status = $invoice->status;
                                       
                                       $setstatus = '';
                                       
                                       if($status == 3) {
                                       
                                           $status = $this->lang->line('invoice_notpaid');
                                       
                                       } elseif($status == 1) {
                                       
                                           $status = $this->lang->line('invoice_partially_paid');
                                       
                                       } elseif($status == 2) {
                                       
                                           $status = $this->lang->line('invoice_fully_paid');
                                       
                                       }
                                       
                                       
                                       
                                       echo "<button class='btn btn-success btn-xs disabled'>".$status."</button>";
                                       
                                       
                                       
                                       ?>
                                 </td>
                                 <td data-title="<?=$this->lang->line('action')?>">
                                    <?php echo btn_view('invoice/view/?type=clg&year='.$invoice->yearsOrSemester, $this->lang->line('view')) ?>
                                    <?php if($usertype == "Admin" || $usertype == "Accountant") { ?>
                                    <?php echo btn_edit('invoice/edit/'.$invoice->invoiceID, $this->lang->line('edit')) ?>
                                    <?php echo btn_delete('invoice/delete/'.$invoice->invoiceID, $this->lang->line('delete'))?>
                                    <?php } ?>
                                 </td>
                              </tr>
                              <?php $i++; }} ?>
                           </tbody>
                        </table>
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>



<script type="text/javascript">
   // function submit_mail() 
   //  {
   //   document.getElementById("form_id").submit();// Form submission
   //  }
   
</script>
<script type="text/javascript">
   $('#classesID').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
       var classesID = $(this).val();
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "classesID_invoice=" + classesID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#subCourseID').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var subCourseID = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "subCourseID_invoice=" + subCourseID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#SessionTo').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var SessionTo = $(this).val();
   
       var SessionFrom  = $('#SessionFrom').val();
   
   
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data:{SessionTo_invoice:SessionTo,SessionFrom_invoice:SessionFrom},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#sessionType').change(function() {
   
    $('#invoice_data_table').DataTable().state.clear();
   
       var sessionType = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "sessionType=" + sessionType,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#examType').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var examType = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "examType=" + examType,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#education_mode').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var education_mode = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "education_mode=" + education_mode,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#student_position').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var student_position = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "student_position=" + student_position,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#yos_filter').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var yos_filter = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "yos_filter_invoice=" + yos_filter,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>

<script type="text/javascript">
   $('#chenge_fee').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();

       var chenge_fee = $(this).val();   
           $.ajax({
               type: 'POST',
               url: "<?=base_url('invoice/index')?>",
   
               data: "chenge_fee=" + chenge_fee,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#payment_status').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var payment_status = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: "payment_status_invoice=" + payment_status,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   $('#teacherID').change(function() {
   
     $('#invoice_data_table').DataTable().state.clear();
   
   
   
       var teacherID = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/index')?>",
   
   
   
               data: " teacherID=" + teacherID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
   function ResetSesession(){
   
     $('#invoice_data_table').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/ResetSesession')?>",
   
   
   
               data:{ResetSesession:'ResetSesession'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   
   
   function ResetCourses(){
   
     $('#invoice_data_table').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/ResetCourses')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }   
   
   function ResetsessionType(){
   
     $('#invoice_data_table').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/ResetsessionType')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   function ResetAllfilter_student(){
   
     $('#invoice_data_table').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/ResetAllfilter')?>",
   
   
   
               data:{ResetAllfilter:'ResetAllfilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   function Resetyos(){
   
     $('#invoice_data_table').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/Resetyos')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
   
   
   
   
   
   function Resetpaymentstatus(){
   
     $('#invoice_data_table').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('invoice/Resetpaymentstatus')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
<script type="text/javascript">
   $('#classesID').change(function() {
   
       var classesID = $(this).val();
   
       if(classesID == 0) {
   
           $('#hide-table').hide();
   
       } else {
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('setfee/setfee_list')?>",
   
               data: "id=" + classesID,
   
               dataType: "html",
   
               success: function(data) {
   
                   window.location.href = data;
   
               }
   
           });
   
       }
   
   });
   
</script>
<script type="text/javascript">
   $(document).ready(function () {
       $("#select_all").click(function () {
           $(".checkBoxClass").attr('check', this.checked);
       });
       
       $("#send_mail").click(function(e) {
           e.preventDefault();
           var date = new Date();
           day = date.getDay();
           if(day == 1)
           {
               $.ajax({
                    type:"POST",
                    url:"<?php echo base_url(); ?>invoice/send_mail_account",
                    success:function(data){
                        alert(data);
                    }
               });
           }
           else
           {
               alert("You have to send only monday to monday report");
           }
       });
   });
   
</script>