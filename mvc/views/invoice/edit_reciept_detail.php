    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-money"> </i> Edit Reciept </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-bullseye"></i> <a href="<?=base_url("dashboard/index")?>"><?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="breadcrumb-item"><a href="<?=base_url()?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>"> Billing</a></li>
                <li class="breadcrumb-item active">Edit Reciept</li> 
            </ol>
        </div>
     </div>

    <!-- form start -->
    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body">
                        <div class="col-sm-12">
                        <!--  <form class="form-horizontal" autocomplete="off" role="form" name="myForm" method="post" enctype="multipart/form-data" onsubmit="return validateForm()"> -->

                        <form class="form-horizontal" autocomplete="off" role="form" novalidate name="myForm" method="post" enctype="multipart/form-data" >

                            <div class="persnolInfo">

                                <p>Field are required with <span class="red-color">*</span></p>
                                
                               

                                <h3  class="border_heading">Edit Reciept</h3>

                                <?php 
                                    if(form_error('invoiceDate')) 
                                    echo "<div class='form-group has-error' >";
                                    else 
                                    echo "<div class='form-group' >";
                                ?>



                                <label for="name_id" class="col-sm-2 control-label">
                                    Payment Date<span class="red-color">*</span>
                                </label>

                                <div class="col-sm-6">
                                    <input type="text" class="form-control datepicker_quiz_data" required placeholder="Invoice Date"  name="invoiceDate" value="<?=set_value('invoiceDate',$receiptDetail->invoiceDate)?>" >
                                </div>

                                <span class="col-sm-4 control-label">
                                    <?php echo form_error('invoiceDate'); ?>
                                </span>

                            </div>

                            <?php 
                                if(form_error('invoiceNumber'))
                                echo "<div class='form-group has-error' >";
                                else     
                                echo "<div class='form-group' >";
                            ?>

                            <label for="payment_mode" class="col-sm-2 control-label">
                                Payment Mode<span class="red-color">*</span>
                            </label>

                            <div class="col-sm-6">
                                <?php 
                                    $array = $array = array('0' => $this->lang->line("invoice_select_paymentmethod"));
                                    $array['Bank'] = 'Bank Transffer';
                                    $array['Cheque'] = 'Cheque Mode';
                                    $array['onlinePayment'] = 'Online Payment';
                                    $array['Draft'] = 'DD';
                                    $array['Cash'] = 'Cash Mode';
                                    
                                    echo form_dropdown("payment_mode", $array, set_value("payment_mode",$receiptDetail->payment_mode), "id='payment_mode' onchange='changeInvoice($(this).val())' class='form-control' required"); 

                                ?>
                            </div>

                            <span class="col-sm-4 control-label">
                                <?php echo form_error('invoiceNumber'); ?>
                            </span>

                        </div>

                        <div id="custome_paymentType"> </div>

                            <?php 



                if(form_error('dob')) 



                    echo "<div class='form-group has-error' >";



                else     



                    echo "<div class='form-group' >";



            ?>



                            <label for="paidAmount" class="col-sm-2 control-label">



                                Paid Amount<span class="red-color">*</span>



                            </label>



                            <div class="col-sm-6">



                                <input type="text" class="form-control" required placeholder="Paid Amount"  name="paidAmount" value="<?=set_value('paidAmount',$receiptDetail->paidAmount)?>" >



                            </div>



                            <span class="col-sm-4 control-label">

                                <?php echo form_error('paidAmount'); ?>

                            </span>

                        </div>




                            <?php 

                                if(form_error('sex')) 

                                    echo "<div class='form-group has-error' >";

                                else 

                                    echo "<div class='form-group' >";

                            ?>

                            <label for="status" class="col-sm-2 control-label">

                                Status<span class="red-color">*</span>

                            </label>

                            <div class="col-sm-6">

                                <?php 

                                    echo form_dropdown("status", array('0' => 'Not Paid','1' => 'Paid'), set_value("status",$receiptDetail->status), "id='status' class='form-control' required"); 

                                ?>

                            </div>

                            <span class="col-sm-4 control-label">

                                <?php echo form_error('status'); ?>

                            </span>

                        </div>

                        
                    <?php 
                        if(form_error('email')) 
                            echo "<div class='form-group has-error' >";
                        else 
                            echo "<div class='form-group' >";
                    ?>

                    <label for="email" class="col-sm-2 control-label">
                        Attatchment<span class="red-color">*</span>
                    </label>

                    <div class="col-sm-6">
                        <!-- onkeyup="checkemail()" -->
                        <input type="file"  class="form-control" name="userfile" >   
                    </div>

                    <span class="col-sm-4 control-label"  id="emailID">
                        <?php echo form_error('email'); ?>
                    </span>

                </div>
              

                    <div class="col-sm-offset-2 ">
                        <input type="submit" class="btn btn-success add-btn" value="Add Invoice" >
                    </div>                        

                </form>

            </div> <!-- col-sm-8 -->

        </div><!-- row -->

    </div>

</div>

</div>

    </div><!-- Body -->

</div>


<script type="text/javascript">
    function changeInvoice(val){

if(val=='Cheque'){

var chagesIputs =  `<div class='form-group' >
                
                        <label for="amount" class="col-sm-2 control-label">
                            Cheque Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="cnumber" placeholder="Enter Cheque Number..." value="" >
                        </div>
                    
                    </div>

   
                        <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            Cheque Name
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="cname" placeholder="Enter Cheque Name..." value="" >
                        </div>
                      
                    </div>
                           <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            Account Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="acnumber" id="dob" placeholder="Enter Account Number..." value="" >
                        </div>
                    
                    </div>

                    </div>`;

$('#custome_paymentType').html(chagesIputs);
    }
else if(val =='Bank'){
        var chagesIputs = `<div class="form-group">
                        <label class="col-sm-2 control-label">
                            Bank Name
                        </label>
                        <div class="col-sm-6">                         
                        <select name="bank" class="form-control">
                           <option selected="" class="plch" value="" disabled="" >Select Bank</option>
                           <option value="SBIN" >SBI</option>
                           <option value="HDFC" data-reactid="$HDFC1">HDFC Bank</option>
                           <option value="ICIC" data-reactid="$ICIC2">ICICI Bank</option>
                           <option value="AXIS" data-reactid="$AXIS3">AXIS Bank</option>
                           <option value="CITA" data-reactid="$CITA4">CITI Bank</option>
                           <option value="ALLB" data-reactid="$ALLB5">Allahabad Bank</option>
                           <option value="ANDB" data-reactid="$ANDB6">Andhra Bank</option>
                           <option value="BAHK" data-reactid="$BAHK7">Bank of Bahrain and Kuwait</option>
                           <option value="BOBC" data-reactid="$BOBC8">Bank of Baroda - Coporate</option>
                           <option value="BOBR" data-reactid="$BOBR9">Bank of Baroda - Retail</option>
                           <option value="BOI" data-reactid="$BOI10">Bank of India</option>
                           <option value="MAHB" data-reactid="$MAHB11">Bank of Maharastra</option>
                           <option value="CANB" data-reactid="$CANB12">Canara Bank</option>
                           <option value="CSYB" data-reactid="$CSYB13">Catholic Syrian Bank</option>
                           <option value="CITU" data-reactid="$CITU14">City Union Bank</option>
                           <option value="CORB" data-reactid="$CORB15">Corporation Bank</option>
                           <option value="DENB" data-reactid="$DENB16">Dena bank</option>
                           <option value="DEUB" data-reactid="$DEUB17">Deutche Bank</option>
                           <option value="DECB" data-reactid="$DECB18">Development Credit Bank</option>
                           <option value="FEDB" data-reactid="$FEDB19">Federal Bank</option>
                           <option value="IDFC" data-reactid="$IDFC20">IDFC Bank</option>
                           <option value="INDB" data-reactid="$INDB21">Indian Bank</option>
                           <option value="IOB" data-reactid="$IOB22">Indian Overseas Bank</option>
                           <option value="INIB" data-reactid="$INIB23">IndusInd Bank</option>
                           <option value="JNKB" data-reactid="$JNKB24">Jammu and Kashmir Bank</option>
                           <option value="KRTB" data-reactid="$KRTB25">Karnataka Bank Ltd</option>
                           <option value="KARB" data-reactid="$KARB26">Karur Vyasa Bank</option>
                           <option value="KOBK" data-reactid="$KOBK27">KOTAK Bank</option>
                           <option value="LXCB" data-reactid="$LXCB28">Laxmi Vilas Bank - Corporate Net Banking</option>
                           <option value="LXRB" data-reactid="$LXRB29">Laxmi Vilas Bank - Retail Net Banking</option>
                           <option value="ORTB" data-reactid="$ORTB30">Oriental Bank of Commerce</option>
                           <option value="PJSB" data-reactid="$PJSB31">Punjab and Sind Bank</option>
                           <option value="PJRB" data-reactid="$PJRB32">Punjab National Bank</option>
                           <option value="PJRB" data-reactid="$PJRB33">Punjab National Bank - Retail Banking</option>
                           <option value="RATB" data-reactid="$RATB34">RBL Bank Limited</option>
                           <option value="SHVB" data-reactid="$SHVB35">Shamrao Vittal Cooperative Bank</option>
                           <option value="SINB" data-reactid="$SINB36">South Indian Bank</option>
                           <option value="STCB" data-reactid="$STCB37">Standard Chartered Bank</option>
                           <option value="SBJB" data-reactid="$SBJB38">State Bank of Bikaner and Jaipur</option>
                           <option value="SHYB" data-reactid="$SHYB39">State Bank of Hyderabad</option>
                           <option value="SMYB" data-reactid="$SMYB40">State Bank of Mysore</option>
                           <option value="SPTB" data-reactid="$SPTB41">State Bank of Patiala</option>
                           <option value="STVB" data-reactid="$STVB42">State Bank of Travancore</option>
                           <option value="SYNB" data-reactid="$SYNB43">Syndicate Bank</option>
                           <option value="TMEB" data-reactid="$TMEB44">Tamilnadu Mercentile Bank</option>
                           <option value="UCOB" data-reactid="$UCOB45">UCO Bank</option>
                           <option value="UBI" data-reactid="$UBI46">Union Bank of India</option>
                           <option value="UNIB" data-reactid="$UNIB47">United Bank of India</option>
                           <option value="VIJB" data-reactid="$VIJB48">Vijaya Bank</option>
                           <option value="YEBK" data-reactid="$YEBK49">YES Bank</option>
                        </select>
                        </div>
                        </div>

                        <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            Payee Name
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="payname" value="" >
                        </div>
                    
                    </div>
                    <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                           Beneficiary Account Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ben_accNumber" value="" >
                        </div>
                    
                    </div>
                    <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                           Reference Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="refNumber" value="" >
                        </div>
                    
                    </div>`;

$('#custome_paymentType').html(chagesIputs);

    }

    else if( val == 'Draft' ){
      var chagesIputs = `<div class='form-group' >
                
                        <label for="amount" class="col-sm-3 control-label">
                            DD Name
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter DD Name..." name="ddname" value="" >
                        </div>
                    
                    </div>

                     <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            DD Number
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter DD Number..." name="DDnumber" value="" >
                        </div>
                    
                    </div>
                    <div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            DD Date
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter DD Date..." name="dddate" value="" >
                        </div>
                    
                    </div>`;
       $('#custome_paymentType').html(chagesIputs);             
    }

    else if( val == 'Cash' ){
      var chagesIputs = `<div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            Paid To
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter Paid To..." name="paidto" value="" >
                        </div>
                    
                    </div>`;
      $('#custome_paymentType').html(chagesIputs);
    }

    else if( val == 'onlinePayment' ){
      var chagesIputs = `<div class='form-group' >
                        <label for="amount" class="col-sm-2 control-label">
                            Transaction ID
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Enter Transaction ID..." name="transactionid" value="" >
                        </div>
                    
                    </div>`;
      $('#custome_paymentType').html(chagesIputs);
    }

}

</script>