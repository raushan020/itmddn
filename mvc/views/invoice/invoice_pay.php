<?php
$productinfo = "Pay E-Campus";
$txnid = time();
$surl = $surl;
$furl = $furl;        
$key_id = RAZOR_KEY_ID;
$currency_code = $currency_code;            
// $total = 100.00;
// $amount = 100.00;
$merchant_order_id = rand(10,1000) ;
$card_holder_name = '';
$email = $siteinfos->email;
$phone = $siteinfos->phone;
$name = "E-Campus";
$return_url = base_url().'Razorpay/callback';
?>
<div class="row">
   <div class="col-lg-12">
       <?php if(!empty($this->session->flashdata('msg'))){ ?>
           <div class="alert alert-success">
               <?php echo $this->session->flashdata('msg'); ?>
           </div>        
       <?php } ?>
       <?php if(validation_errors()) { ?>
         <div class="alert alert-danger">
           <?php echo validation_errors(); ?>
         </div>
       <?php } ?>
   </div>
</div>


<style>
  .payclr{
    color: #000;
  }
</style>
<div class="">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"> Pay Now </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Fee</li>

            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

    <div class="box-body">

        <div class="row">

           <div class="">
  <div class="row">
    <div class="col-md-8">
  <!-- <h4>Pay Now</h4>
  <br> -->
  <!-- Nav tabs -->
 

  <!-- Tab panes -->
  <div class="tab-content">
    
   


        
              <div class="form-group payclr">
         <?php
          if ($invoice) {
            $mainInvoice = $invoice->invoiceAmount;
            $invoicePaidamount = $invoice->paidAmount;
          }else{
            $mainInvoice = 0.00;
            $invoicePaidamount = 0;
          }
          if ($advancePayment) {
              $totaladvanceAmount = $advancePayment->advanceAmount;
          }else{
              $totaladvanceAmount = 0;
          }
          if ($invoice) {
          	$billing_invoiceIDs = $invoice->billing_invoiceID;
          }
         ?>
          <label for="exampleInputEmail1" class="col-md-4">Latest Bill Amount</label>
          <div class="col-md-1">:</div>
          <div class="col-md-7">
            <p><i class="fa fa-inr"></i> <?=number_format($mainInvoice)?></p>
          <!-- <input type="text" name="payment" class="form-control" value="<?=$mainInvoice?>" placeholder="payment" readonly> -->
         </div>
        </div>
              <div class="form-group payclr">
         
          <label for="exampleInputEmail1" class="col-md-4">Advanced Amount</label>
          <div class="col-md-1">:</div>
          <div class="col-md-7">
            <p><i class="fa fa-inr"></i> <?=$totaladvanceAmount?></p>
          <!-- <input type="text" name="payment" class="form-control" value="<?=$totaladvanceAmount?>" placeholder="payment" readonly> -->
         </div>
        </div>
        <div class="form-group payclr">
         
          <label for="exampleInputEmail1" class="col-md-4">Outstanding Bill</label>
          <div class="col-md-1">:</div>
          <div class="col-md-7">
            <p><i class="fa fa-inr"></i> <?=number_format($totaladvanceAmount+$mainInvoice-$invoice->paidAmount)?><span style="font-size: font-size: 11px;">  Your payment may take up to 3 days to reflect.(Please ignore if already paid.)
            </span></p>
          <!-- <input type="text" name="payment" class="form-control" value="<?=$totaladvanceAmount+$mainInvoice-$totaladvanceAmount?>" placeholder="payment" readonly> -->
         </div>
        </div>

        <div class="form-group payclr">
         <form name="razorpay-form" id="razorpay-form" action="<?php echo $return_url; ?>" method="POST">
			 <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
			 <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
			 <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
			 <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="<?php echo $productinfo; ?>"/>
			 <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
			 <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
			 <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/>
			 <input type="hidden" name="billing_invoiceID" id="billing_invoiceID" value="<?php if($invoice){ echo $billing_invoiceIDs;} ?>"/>
			 <!-- <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/> -->
			 <!-- <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/> -->
			
          <label for="exampleInputEmail1" class="col-md-4">Payment Amount</label>
          <div class="col-md-1">:</div>
          <div class="col-md-4">
          <input type="number" name="merchant_total" id="amount" class="form-control" value="<?php echo ($totaladvanceAmount+$mainInvoice-$invoice->paidAmount)?>" placeholder="payment">
         </div>
         <?php if ($invoice) {         	
	          $disabled = ''; 
	      }else{
	      	$disabled = 'disabled';
	      }
          ?>
         <div class="col-md-3">
           <!-- <button type="button" class="btn btn-primary" <?=$disabled?> onclick="razorpaySubmit(this);" style="padding: 5px;">Proceed to Pay</button> -->

           <a target="_blank" href="https://e-campus.in/page?amount=<?=($totaladvanceAmount+$mainInvoice-$invoicePaidamount)?>&advanced=<?=$totaladvanceAmount?>&totalinvoice=<?=$mainInvoice?>&name=<?=$name?>&email=<?=$email?>&phone=<?=$phone?>" class="btn btn-primary" style="padding: 5px;">Proceed to Pay</a>
         </div>
         </form>
        </div>


       <!--  <div class="form-group payclr col-md-offset-5">
        <button type="submit" class="btn btn-primary" disabled>Proceed to pay</button>
      </div> -->
      


      </div>

        
        

    </div>
    
    



    


  </div>
</div>
</div>
</div>

        </div>

    </div>
</div>
</div>
</div>
</div>
</div>


</div>



<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
	var amountPaisa =$('#amount').val();
	var amount=amountPaisa*100;
	// alert(amountPaisa);
   var razorpay_options = {
   key: "<?php echo $key_id; ?>",
   amount: amount,
   name: "<?php echo $name; ?>",
   description: "Order # <?php echo $merchant_order_id; ?>",
   netbanking: true,
   currency: "<?php echo $currency_code; ?>",
   prefill: {
     name:"<?php echo $card_holder_name; ?>",
     email: "<?php echo $email; ?>",
     contact: "<?php echo $phone; ?>"
   },
   notes: {
     soolegal_order_id: "<?php echo $merchant_order_id; ?>",
   },
   handler: function (transaction) {
       document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
       document.getElementById('razorpay-form').submit();
   },
   "modal": {
       "ondismiss": function(){
           location.reload()
       }
   }
 };

 var razorpay_submit_btn, razorpay_instance;

 function razorpaySubmit(el){
   // alert("nc");
   // $('#modal-2').hide();
   if(typeof Razorpay == 'undefined'){
     setTimeout(razorpaySubmit, 200);
     if(!razorpay_submit_btn && el){
       razorpay_submit_btn = el;
       el.disabled = true;
       el.value = 'Please wait...';  
     }
   } else {
     if(!razorpay_instance){
       razorpay_instance = new Razorpay(razorpay_options);
       if(razorpay_submit_btn){
         razorpay_submit_btn.disabled = false;
         razorpay_submit_btn.value = "Pay Now";
       }
     }
     razorpay_instance.open();
   }
 }  
 
</script>