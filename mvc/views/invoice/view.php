<?php
          if($this->input->get('year')){
              $year = $_GET['year'];
              }else{
              $year = '1st_year';
              }
              if($this->input->get('type')){
              $feeType = $_GET['type'];
              }else{
              $feeType = 'clg';
              }
              if($this->input->get('month')){
              $month = $_GET['month'];
              }else{
              $month = date('m');
              }
              if($this->input->get('monthYear')){
              $monthYear = $_GET['monthYear'];
              }else{
              $monthYear = date('Y');
              }

  if($feeType=='hstl'){
                   $feePaidPerYear = 0;
   foreach ($paymentDetails as $key => $value) {
    $feePaidPerYear += $value->paymentamount;
   }
              }
   ?>
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-rupee"></i> Fee</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("invoice/index")?>">Fee</a></li>
            <li class="active"><?=$this->lang->line('view')?></li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
    <div class="container-fluid">
       <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
         <div class="">
            <div class="card">
               <div class="card-body">
                  <?php 
                     if(($invoice)) {
                     
                         $usertype = $this->session->userdata("usertype");
                     
                         if($usertype == "Admin" || $usertype == "Accountant" || $usertype == 'Student' || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Teacher" || $usertype == "Parent") {

                    
                     ?>
                     <div class="row">
                       <ul class="nav nav-tabs">

  <li  <?php if($feeType=='clg'){ echo "class='active'"; } ?> ><a href="<?php echo base_url() ?>invoice/view/<?php echo $this->uri->segment(3) ?>?type=clg&year=<?php echo $year ?>" style="padding: 10px;">Total Fees</a></li>
  <li <?php if($feeType=='hstl'){ echo "class='active'"; } ?>><a href="<?php echo base_url() ?>invoice/view/<?php echo $this->uri->segment(3) ?>?type=hstl&month=<?php echo date('m') ?>&monthYear=<?php echo date('Y') ?>" style="padding: 10px;">Hostel Fee</a></li>
</ul>
                     </div>
                  <div class="well">
                     <div class="row">
                        <div class="col-sm-6 fees-button">
                           <button class="btn btn-success btn-xs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                           <!-- <?php
                              echo btn_add_pdf('invoice/print_preview/'.$invoice->invoiceID, $this->lang->line('pdf_preview')) 
                              
                              ?> -->
<?php 
      if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Teacher") { ?>
 
                           <?php
                              if ($feeType=='hstl') {
                              if($invoice->hostelFee != $feePaidPerYear) {
                              echo btn_payment('invoice/payment/'.$invoice->invoiceID.'?type=hstl&month='.$month.'&monthYear'.$monthYear, ' Update '.$this->lang->line('payment')); 
                              }
                              }else{
                              if($invoice->status!=2) {
                                // if(CallSemesterwitSemester($student->yearsOrSemester)==$year)
                                // {
                                   echo btn_payment('invoice/payment/'.$invoice->invoiceID.'?type=clg&year='.$year, ' Update '.$this->lang->line('payment'));
                                // }
                              }
                               }
                               ?>
                           <?php
                         }
                              ?> 

                        </div>
                        <div class="col-sm-6 year-filter">
                          <?php if($feeType=='clg'){ ?>
                           <div class="col-sm-3">Select Year</div>
                           <div class="col-sm-9">
                           <!--  <?php
                            $looping    =  (int) $classes->duration;
                                    
                                    
                                          for ($i=1; $i <=$looping; $i++) {

                                                 if (CallYears($i)==$year) {

                                           echo "<label>".CallYears($i)."</label>";

                                         }

                                         }
                            ?> -->
                              <select class='form-control' name='semesterId' onchange='invoice_filter_url($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>
                                 <?php 
                                    $looping    =  (int) $classes->duration;
                                    
                                          for ($i=1; $i <=$looping; $i++) {

                                                 if (CallYears($i)==$year) {

                                            $select = 'Selected';

                                         }else{

                                         $select = '';

                                         }
                                           
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         

                                       }
                                    
                                    
                                     ?>
                              </select>
                           </div>
                        </div>
                      <?php } else { ?>

<?php
$monthArray = range(1, 12);
?>
<form action="<?php echo base_url() ?>invoice/view/<?php echo $this->uri->segment(3) ?>">
<div class="col-sm-4">
<input type="hidden" name="type" value="hstl" />
<select name="month" class="form-control">
    <option value="">Select Month</option>
    <?php
    foreach ($monthArray as $months) {
        // padding the month with extra zero
        $monthPadding = str_pad($months, 2, "0", STR_PAD_LEFT);
          if($monthPadding==$month)
{
  $select = "selected";
}else {
  $select = "";
}
        $fdate = date("F", strtotime("2015-$monthPadding-01"));
        echo '<option value="'.$monthPadding.'" '.$select.'>'.$fdate.'</option>';
    }
    ?>
</select>
</div>
<?php
$starting_year  = 2000;
$ending_year    = 2030;
for($starting_year; $starting_year <= $ending_year; $starting_year++) {
  if($starting_year==$monthYear)
{
  $select = "selected";
}else {
  $select = "";
}

    $years[] = '<option value="'.$starting_year.'"  '.$select.' >'.$starting_year.'</option>';
}

?>
<div class="col-sm-4">
<select class="form-control" name="monthYear">
    <?php echo implode("\n\r", $years);  ?>
</select>
</div>
<div class="col-sm-4">
  <input type="Submit" class="btn btn-success btn-xs" name="search" value="Search">
</div> 
</form>
                      <?php } ?>
                     </div>
                  </div>
<?php if($this->session->flashdata('success_invoice')): ?>
 <div id=""><div class="alert alert-success">
  <strong>Success!</strong> <?php echo $this->session->flashdata('success_invoice'); ?>
</div></div>
<?php endif; ?>
                  <div id="printablediv">
                     <section class="content invoice" >
                        <!-- title row -->
                        <div class="row">
                           <div class="col-xs-12">
                              <h2 class="page-header">
                                 <?php
                                    if($siteinfos->photo) {
                                    
                                        $array = array(
                                    
                                            // "src" => base_url('uploads/images/'.$siteinfos->photo),
                                             "src" => base_url('uploads/images/1561796140.png'),
                                    
                                            // 'width' => '25px',
                                    
                                            'height' => '55px',
                                    
                                            'class' => 'img-circle'
                                    
                                        );
                                    
                                        echo img($array);
                                    
                                    } 
                                    
                                    ?>
                                 <?php  echo $siteinfos->sname; ?>
                                 <!-- <small class="pull-right"><?=$this->lang->line('invoice_create_date').' : '.date('d M Y')?></small> -->
                              </h2>
                           </div>
                           <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                           <div class="col-sm-4 invoice-col">
                              <?php  echo $this->lang->line("invoice_from"); ?>
                              <address>
                                 <strong><?=$siteinfos->sname?></strong><br>
                                 <?=$siteinfos->address?><br>
                                 <?="<b>".$this->lang->line("invoice_phone"). " : "."</b>". $siteinfos->phone?><br>
                                 <?="<b>".$this->lang->line("invoice_email"). " : "."</b>". $siteinfos->email?><br>
                              </address>
                           </div>
                           <!-- /.col -->
                           <div class="col-sm-4 invoice-col">
                              <?php if($student == "") { ?>    
							   <!-- count -->
                              <?=$this->lang->line("invoice_to")?>
                              <address>
                                 <?=$this->lang->line("invoice_sremove")?>
                              </address>
                              <?php } else { ?>
                              <?=$this->lang->line("invoice_to")?>
                              <address>
                                 <strong><?=$student->name?></strong><br>
                                 <strong>Batch :</strong>&nbsp<?php echo $student->session?><br>
                                 <strong>Enrollment No :</strong>&nbsp<?php echo $student->roll?><br>
                                 <strong>Department :</strong>&nbsp<?php if($departmentData){echo $departmentData->department_name;}else{echo "";}?><br>
                                 <strong>Course :</strong>&nbsp<?php echo $classes->classes?><br>
                                 <?="<strong>".$this->lang->line("invoice_email"). " : "."</strong>". $student->email?><br>
                                 <strong>Mobile No. :</strong>&nbsp<?php echo $student->phone?><br>
                              </address>
                              <?php } ?>
                           </div>
                           <!-- /.col -->
                           <div class="col-sm-4 invoice-col">
                              <b>L/F No :</b>&nbsp<?=$invoice->lf_no ?><br>
                              <?php
                                 if($invoice->paiddate) {
                                     echo "<strong>".$this->lang->line("invoice_pdate")." :  "."</strong>". date("d M Y", strtotime($invoice->paiddate)).'<br/>';
                                 }
                                 ?>
                              <?php 
                              if ($feeType=='hstl') {
                              if($feePaidPerYear) {
                                $due = $invoice->hostelFee-$feePaidPerYear;
                                if($due==0){
                                  $status_hostel = $this->lang->line('invoice_fully_paid');  
                                 }
                                 if($due==$feePaidPerYear){
                                   $status_hostel = $this->lang->line('invoice_notpaid');
                                 }
                                 if($due != $invoice->hostelFee and $invoice->hostelFee>$feePaidPerYear){
                                   $status_hostel = $this->lang->line('invoice_partially_paid');
                                 }
                                  }else {
                                  $status_hostel = $this->lang->line('invoice_notpaid');
                                 } 

                                 } 
                                 if ($feeType=='clg'){
                                 if($invoice->status==2){
                                     $status = $this->lang->line('invoice_fully_paid');  
                                 }
                                 if($invoice->status==3){
                                   $status = $this->lang->line('invoice_notpaid');
                                 }
                                if($invoice->status==1){
                                   $status = $this->lang->line('invoice_partially_paid');
                                 } 
                                 }
                                
                           if ($feeType=='hstl') {
                           echo "<strong>".$this->lang->line('invoice_status'). " : "."</strong>"."<button id = 'StatusAfterAjax' class='btn btn-success btn-xs'>".$status_hostel."</button>";
                               }else{
                          echo "<strong>".$this->lang->line('invoice_status'). " : "."</strong>"."<button id = 'StatusAfterAjax' class='btn btn-success btn-xs'>".$status."</button>";     
                               }
                                 
                                 ?>
                    
                           </div>
                           <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- Table row -->
                        <br />
                      <?php if($feeType=='clg'){ ?>
                          <?php if($usertype=='ClgAdmin' || $usertype=='Admin' || $usertype=='Accountant'){ ?>
                        <div class="row">
                           <div class="col-xs-12" id="hide-table">
                              <h3>Fee Structure</h3>
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Total Fee</th>
                                       <th>Overall Discount</th>
                                       <th></th>
                                       <!-- <th>SWF Per Year</th> -->
                                       <th>Overall Fee</th>
                                       <th></th>
                                       <th></th>
                                       <th></th>
                                       <th></th>
                                       <th></th>
                                       <th>Overall Fee Per Year</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr id="for_write_tr_fee_str">
                                       <td> 
                                          <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                            <input type="number" name="tuitionFee" class="form-control" value="<?php echo ($invoice->tuitionFee)  ?>" onBlur= "inline_edit_invoice_fee_structure(this, 'tuitionFee', '<?php echo $invoice->invoiceID ?>','<?php echo $classes->duration ?>','<?php echo $invoice->studentID ?>' ,$(this).val())">
                                          </div>
                                       </td>
                                       <td> 
                                        <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                          <input type="number" name="discount" class="form-control" value="<?php echo ($invoice->discount)  ?>" onBlur= "inline_edit_invoice_fee_structure(this, 'discount', '<?php echo $invoice->invoiceID ?>','<?php echo $classes->duration ?>', '<?php echo $invoice->studentID ?>' ,$(this).val())">
                                        </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
<!--                                   <td> 
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                        <input type="number" name="welfareFund" class="form-control" value="<?php echo ($invoice->welfareFund)  ?>" onBlur= "inline_edit_invoice_fee_structure(this, 'welfareFund', '<?php echo $invoice->invoiceID ?>','<?php echo $classes->duration ?>', $(this).val())">
                                      </div>
                                      </td> -->
                                       <td>
                                        <div class="input-group">
                                            
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo ($invoice->total_amount) ?>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">      
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                       <td>
                                        <div class="input-group">
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->totalfeepaidperyear)  ?>
                                        </div>
                                      </td>
                                      <td><button type="button" onclick="Edit_fee_struc_row_back()" class="btn btn-success btn-xs mrg for_margR">Back</button></td>
                                    </tr>
                                    <tr id="for_read_tr_fee_str">
                                      <td>
                                      <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->tuitionFee)  ?>
                                       </td>
                                       <td> 
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->discount)  ?>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                       <!-- <td> 
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                        <input type="number" name="welfareFund" class="form-control" value="<?php echo ($invoice->welfareFund)  ?>" onBlur= "inline_edit_invoice_fee_structure(this, 'welfareFund', '<?php echo $invoice->invoiceID ?>','<?php echo $classes->duration ?>', $(this).val())">
                                      </div>
                                      </td> -->
                                       <td>
                                        <div class="input-group">
                                            
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->total_amount) ?>
                                      </div>
                                      </td>
                                      <!-- rau -->
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <td>
                                        <div class="input-group">
                                            
                                        <i class="" aria-hidden="true"></i>
                                      </div>
                                      </td>
                                      <!-- rau end -->

                                       <td>
                                        <div class="input-group">
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->totalfeepaidperyear)  ?>
                                        </div>
                                      </td>
                                      <td><button type="button" onclick="Edit_fee_struc_row()" class="btn btn-success btn-xs mrg for_margR">Edit</button></td>
                                    </tr>
                                
                                 </tbody>
                              </table>
                           </div>
                        </div>
                      <?php } ?>
                       <?php } else { ?>
                            <div class="row">
                           <div class="col-xs-12" id="hide-table">
                              <h3>Fee Structure</h3>
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Hostel Fee/Month</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php if($usertype=='ClgAdmin' || $usertype=='Admin' || $usertype=='Accountant'){ ?>
                                    <tr id="for_write_tr_fee_str">
                                       <td> 
                                        <div class="input-group">
                                          <span class="input-group-addon s"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                          <input type="number" name="hostelFee" class="form-control" value="<?php echo number_format($invoice->hostelFee)  ?>" onBlur= "inline_edit_invoice_fee_structure_forHostel(this, 'hostelFee', '<?php echo $invoice->invoiceID ?>','<?php echo $classes->classesID ?>', $(this).val())">
                                          </div>
                                        </td>
                                        <td><button type="button" onclick="Edit_fee_struc_row_back()" class="btn btn-success btn-xs mrg for_margR">Save</button></td>
                                    </tr>
                                    <tr id="for_read_tr_fee_str">
                                      <td><?php echo number_format($invoice->hostelFee)  ?></td>
                                      <td><button type="button" onclick="Edit_fee_struc_row()" class="btn btn-success btn-xs mrg for_margR">Edit</button></td>
                                    </tr>
                                <?php } elseif($usertype=='Student') {   ?>
                                    <tr>
                                      <td><i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->hostelFee)  ?></td>
                                    </tr>
                                <?php } ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                       <?php } ?> 
                  <!-- paid fee --> 
                          <?php if ($usertype == 'ClgAdmin' || $usertype == "Teacher" || $usertype == "Accountant") { ?>
                        <div class="row">
                           <div class="col-xs-12" id="hide-table">
                              <h3>Paid Fee</h3>
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Installment</th>
                                       <th>Year</th>
                                       <th>Fee Date</th>
                                       <th>Receipt No.</th>
                                       <th>Receipt image</th>
                                       <th>Paid Fee</th>
                                       <th>Payment Type</th>
                                      <?php if($usertype!='Student') { ?>
                                       <th>Action</th>
                                      <?php } ?>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php

                                       foreach ($paymentDetails as $key => $value) { ?>
                                    <tr class="displayNone" id="for_write_tr_fee_paid_<?php echo $key ?>">
                                       <td>Installment <?php echo  $key+1 ?></td>
                                       <td><?php echo str_replace('_', " ", $value->yearsOrSemester); ?></td>
                                       <td> <input type="text" name="payment_date" class="form-control datetimepicker_quiz_data" value="<?php echo $value->paymentdate  ?>" onBlur= "inline_edit_invoice(this, 'paymentdate', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())"></td>
                                       <td><input type="" name="receipt" class="form-control" value="<?php echo $value->receipt  ?>" onBlur= "inline_edit_invoice(this, 'receipt', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())">
                                       </td>
                                       <td>
                                        <?php 
                                        if ($value->receipt_src == '') { ?>
                                             no image
                                        <?php  } else { ?>
                                          <a href="<?php echo base_url() ?>uploads/receipt/<?php echo $value->receipt_src ?>" target =_blank >View</a>   
                                      <?php  } ?>
                                       </td>
                                       <td>
                                          <input type="number" name="paymentamount" class="form-control payment_amout_count" value="<?php echo ($value->paymentamount)  ?>" id ="langda_aam<?php echo $key  ?>" onBlur= "inline_edit_invoice(this, 'paymentamount', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())">
                                       </td>
                                       <td>
                                          <div>
                                             <select class="form-control" onchange="changeInvoicepaymenttype(this, '<?php echo $value->paymentID ?>', $(this).val()) ">
                                                <option value="" >invoice select paymentmethod</option>
                                                <option value="Cash" <?php if($value->paymenttype=='Cash'){echo "Selected";} ?>>Cash Mode</option>
                                                <option value="Cheque" <?php if($value->paymenttype=='Cheque'){echo "Selected";} ?>>Cheque Mode</option>
                                                <option value="Bank" <?php if($value->paymenttype=='Bank'){echo "Selected";} ?>>Bank Transfer</option>
                                                <option value="Draft" <?php if($value->paymenttype=='Draft'){echo "Selected";} ?>>DD</option>
                                                <option value="PayuMoney" <?php if($value->paymenttype=='PayuMoney'){echo "Selected";} ?>>PayU Money</option>
                                                <option value="onlinePayment" <?php if($value->paymenttype=='onlinePayment'){echo "Selected";} ?>>Online Payment</option>
                                             </select>
                                          </div>
                                       </td>
                                    <td>
                                    <a href="<?php echo base_url() ?>invoice/PaymentDelete/<?php echo $value->paymentID ?>/<?php echo $year ?>/<?php echo $feeType ?>/<?php echo $month ?>/<?php echo $monthYear ?>" onclick="return confirm('Do you Really want to Delete This?')" class="btn btn-success btn-xs mrg for_margR">Remove</a>
                                    <a  data-target="#addBookDialog" data-toggle="modal" data-id="<?php echo $value->paymentID ?>" title="Add this item" class="btn btn-success btn-xs mrg for_margR open-AddBookDialog">Add Receipt</a>
                                     <a   class="btn btn-success btn-xs mrg for_margR open-AddBookDialog" onclick='Edit_fee_paid_row_back("<?php echo $key ?>")'>Save</a>
                                  </td>
                                    </tr>
                                    <tr id="for_read_tr_fee_paid_<?php echo $key; ?>">
                                       <td>Installment <?php echo  $key+1 ?></td>
                                       <td><?php echo str_replace('_', " ", $value->yearsOrSemester); ?></td>
                                       <td><?php echo $value->paymentdate  ?></td>
                                       <td><?php echo $value->receipt  ?></td>
                                       <td>
                                        <?php 
                                        if ($value->receipt_src=='') { ?>
                                             no image
                                        <?php  } else { ?>
                                          <a href="<?php echo base_url() ?>uploads/receipt/<?php echo $value->receipt_src ?>" target =_blank >View</a>   
                                      <?php  } ?>
                                       </td>
                                       <td>
                                        <?php echo ($value->paymentamount)  ?>
                                       </td>
                                       <td>
                                      <?php echo $value->paymenttype; ?>
                                       </td>
                                    <!-- <td> -->
                                      <td>
                                        <button type="button" onclick='Edit_fee_paid_row("<?php echo $key; ?>")' class="btn btn-success btn-xs mrg for_margR">Edit</button>
                                     </td>
                                    </tr>
                                    <?php } ?>           
                                 </tbody>
                              </table>
 <?php } if($usertype=='Student'){  ?>
                      <div class="row">
                           <div class="col-xs-12" id="hide-table">
                          <?php if ($feeType=='clg') { ?>
                            <h3>Paid Fee</h3>
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Installment</th>
                                       <th>Year</th>
                                       <th>Fee Date</th>
                                       <th>Receipt No.</th>
                                       <th>Receipt image</th>
                                       <th>Paid Fee</th>
                                       <th>Payment Type</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php

                                       foreach ($paymentDetails as $key => $value) { ?>
                                    <tr class="displayNone" id="for_write_tr_fee_paid_<?php echo $key ?>">
                                       <td>Installment <?php echo  $key+1 ?></td>
                                       <td><?php echo str_replace('_', " ", $value->yearsOrSemester); ?></td>
                                       <td> <input type="text" name="payment_date" class="form-control datetimepicker_quiz_data" value="<?php echo $value->paymentdate  ?>" onBlur= "inline_edit_invoice(this, 'paymentdate', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())"></td>
                                       <td><input type="" name="receipt" class="form-control" value="<?php echo $value->receipt  ?>" onBlur= "inline_edit_invoice(this, 'receipt', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())">
                                       </td>
                                       <td>
                                        <?php 
                                        if ($value->receipt_src == '') { ?>
                                             no image
                                        <?php  } else { ?>
                                          <a href="<?php echo base_url() ?>uploads/receipt/<?php echo $value->receipt_src ?>" target =_blank >View</a>   
                                      <?php  } ?>
                                       </td>
                                       <td>
                                          <input type="number" name="paymentamount" class="form-control payment_amout_count" value="<?php echo ($value->paymentamount)  ?>" id ="langda_aam<?php echo $key  ?>" onBlur= "inline_edit_invoice(this, 'paymentamount', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())">
                                       </td>
                                       <td>
                                          <div>
                                             <select class="form-control" onchange="changeInvoicepaymenttype(this, '<?php echo $value->paymentID ?>', $(this).val()) ">
                                                <option value="" >invoice select paymentmethod</option>
                                                <option value="Cash" <?php if($value->paymenttype=='Cash'){echo "Selected";} ?>>Cash Mode</option>
                                                <option value="Cheque" <?php if($value->paymenttype=='Cheque'){echo "Selected";} ?>>Cheque Mode</option>
                                                <option value="Bank" <?php if($value->paymenttype=='Bank'){echo "Selected";} ?>>Bank Transfer</option>
                                                <option value="Draft" <?php if($value->paymenttype=='Draft'){echo "Selected";} ?>>DD</option>
                                                <option value="PayuMoney" <?php if($value->paymenttype=='PayuMoney'){echo "Selected";} ?>>PayU Money</option>
                                                <option value="onlinePayment" <?php if($value->paymenttype=='onlinePayment'){echo "Selected";} ?>>Online Payment</option>
                                             </select>
                                          </div>
                                       </td>
                                    <td>
                                    <a href="<?php echo base_url() ?>invoice/PaymentDelete/<?php echo $value->paymentID ?>/<?php echo $year ?>/<?php echo $feeType ?>/<?php echo $month ?>/<?php echo $monthYear ?>" onclick="return confirm('Do you Really want to Delete This?')" class="btn btn-success btn-xs mrg for_margR">Remove</a>
                                    <a  data-target="#addBookDialog" data-toggle="modal" data-id="<?php echo $value->paymentID ?>" title="Add this item" class="btn btn-success btn-xs mrg for_margR open-AddBookDialog">Add Receipt</a>
                                     <a   class="btn btn-success btn-xs mrg for_margR open-AddBookDialog" onclick='Edit_fee_paid_row_back("<?php echo $key ?>")'>Save</a>
                                  </td>
                                    </tr>
                                    <tr id="for_read_tr_fee_paid_<?php echo $key; ?>">
                                       <td>Installment <?php echo  $key+1 ?></td>
                                       <td><?php echo str_replace('_', " ", $value->yearsOrSemester); ?></td>
                                       <td><?php echo $value->paymentdate  ?></td>
                                       <td><?php echo $value->receipt  ?></td>
                                       <td>
                                        <?php 
                                        if ($value->receipt_src=='') { ?>
                                             no image
                                        <?php  } else { ?>
                                          <a href="<?php echo base_url() ?>uploads/receipt/<?php echo $value->receipt_src ?>" target =_blank >View</a>   
                                      <?php  } ?>
                                       </td>
                                       <td>
                                        <?php echo ($value->paymentamount)  ?>
                                       </td>
                                       <td>
                                      <?php echo $value->paymenttype; ?>
                                       </td>
                                    <!-- <td> -->
                                                     
                                    </tr>
                                    <?php } ?>           
                                 </tbody>
                              </table>
                              <table class="table table-striped due-amount">
                                 <thead>
                                    <tr>
                                    
                                       <th>Total Outstanding Fees</th>
                                       <th>Action</th>
                                    </tr>
                                    
                                 </thead>
                                 <tbody>
                                  <tr>
                                    
                                    <td>
                                     <?php 
                          
                                          $due =  round($invoice->amount-$invoice->paidamount);
                                        echo number_format($due);
                                     
                                      ?> 
                                    </td>
                                    <td><input type="button" class="btn-success" name="" value="Pay Fee" style="font-size: 15px;height: 40px;width:70px; border-radius: 4px"></td>
                                  </tr>
                                 </tbody>
                              </table>
                            <?php } ?>
                           </div>
                        </div>
                      <?php  } ?>
                           </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18">
                          </div>
                           <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18" style="margin-left: 330px;">
                              <p class="lead"><?=$this->lang->line('invoice_amount')?></p>
                              <div class="table-responsive">
<!--                                  <table class="table">
                                    <tr>
                                       <th class="col-sm-8 col-xs-8">Total Fee</th>
                                       <td class="col-sm-4 col-xs-4"><i class="fa fa-inr" aria-hidden="true"></i> 
                                        <?php 
                                        if($feeType=='clg'){
                                          if ($year=='all') {
                                          echo number_format($invoice->amount);
                                          }else{
                                          echo number_format($invoice->totalfeepaidperyear); 
                                          }
                                        }else{
                                          echo number_format($invoice->hostelFee);
                                        }

                                           ?></td>
                                    </tr>
                                 </table> -->
<!--                                  <table class="table">
                                    <tr>
                                       <th class="col-sm-8 col-xs-8"><?=$this->lang->line('invoice_made');?></th>
                                       <td class="col-sm-4 col-xs-4" id="paid_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> 
                                          <?php 
                      
                                             echo  number_format($feePaidPerYear);
                                                 

                                              ?>
                                       </td>
                                    </tr>
                                 </table> -->
                                 <table class="table">
                                    <tr>
                                      <th>Previous Due</th>
                                      <th>Overall Fee Per Year</th>
                                       <th class="col-sm-8 col-xs-8">Current Year Due</th>
                                       <th>Total Outstanding Fees</th>
                                     </tr>
                                      <?php
                                      if($feeType=='clg') {
                                      $due =  floor($invoice->amount-$invoice->outstanding_fees-$invoice->paidamount);
                                      }else {
                                        $due =  $invoice->hostelFee-$feePaidPerYear;
                                      }
                                      ?>
                                      <tr>
                                      <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo round($invoice->outstanding_fees);?></td>
                                       <td><i class="fa fa-inr" aria-hidden="true"></i><?php echo number_format($invoice->totalfeepaidperyear)  ?></td>
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> <?php if($due > 0)
                                       {
                                          echo number_format($due);
                                       }
                                       else
                                       {
                                         echo number_format(0);
                                       }  ?></td>
                                       <td>
                                         <?php echo round($invoice->amount-$invoice->paidamount); ?>
                                       </td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                           <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <div id = "alertAfterinvoiceAjax">
                        </div>
                     </section>
                     <!-- /.content -->
                  </div>

      </div>
   </div>
</div>
</div>
</div>

                              <script>
                                 function inline_edit_invoice(val,column,invoiceID,paymentID,editValue){
                                 
                                var feeType = "<?php echo $feeType ?>" 

                                 swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                            
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                 
                                       var count_payment_box = $(".payment_amout_count").length;
                                 
                                 var countPayment = 0;
                                 
                                     for (i = 0; i < count_payment_box; i++) {
                                 
                                 countPayment +=  parseInt($("#langda_aam"+i+" ").val());
                                 
                                 }
                                 
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"paymentID":paymentID,"countPayment":countPayment,"feeType":feeType},
                                 
                                            success: function(response) {
                              
                                                if (response=='Exceede') {
                                                    var alertd = `<div class='alert alert-danger' role='alert>This is a danger alert—check it out!</div>`;
                                                location.reload(true);
                                                }else if(response=='Empty'){
                                 
                                                 location.reload(true);
                                 
                                                }
                                 
                                                else{
                                
                                 
                                                location.reload(true);
                                 
                                                }
                                 
                                 
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 location.reload(true);
                                 swal("Your imaginary file is safe!");
                                 }
                                 });
                                      
                                 
                                 
                                 
                                      }
                                 
                                    
                              </script>
                              <script>
                                 function inline_edit_invoice_fee_structure(val,column,invoiceID,duration,studentID,editValue){
                                    swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice_fee_structure')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"duration":duration,"studentID":studentID},
                                 
                                            success: function(response) {
                                            // console.log(response);
                                                 location.reload(true);
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 swal("Your imaginary file is safe!");
                                 location.reload(true);
                                 }
                                 });
                                 
                                   
                                 
                                 
                                      }
                                 
                                    
                              </script>
                          <script>
                                 function inline_edit_invoice_fee_structure_forHostel(val,column,invoiceID,duration,editValue){
                                    
                                      var month  = "<?php echo $month  ?>";
                                  var monthYear  = "<?php echo $monthYear  ?>";
                                 
                                 
                                    swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice_fee_structure_forHostel')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"duration":duration,"month":month,"monthYear":monthYear},
                                 
                                            success: function(response) {
                                 
                                                 // location.reload(true);
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 swal("Your imaginary file is safe!");
                                 location.reload(true);
                                 }
                                 });
                                 
                                   
                                 
                                 
                                      }
                                 
            
                              </script>
                              <script>
                                 function inline_edit_invoice_amount(val,column,invoiceID,paymentID,editValue){
                                 
                                    // confirm('Are You Sure ???');
                                 
                                 
                                    swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice_amount')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue},
                                 
                                            success: function(response) {
                                 
                                                 location.reload(true);
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 swal("Your imaginary file is safe!");
                                 location.reload(true);
                                 }
                                 });
                                 
                                   
                                 
                                 
                                      }
                                 
                                    
                              </script>
                              <script type="text/javascript">
                                 function changeInvoicepaymenttype(val,paymentID,editValue){
                                 
                                     
                                 
                                 swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                         $.ajax({
                                 
                                             type: "POST",
                                 
                                             url:"<?=base_url('AjaxController/changeInvoicepaymenttype   ')?>",
                                 
                                             data:{"paymentID":paymentID,"editValue":editValue},
                                 
                                             success: function(response) {
                                 
                                                  location.reload(true);
                                 
                                              $(val).css("background","#FDFDFD");
                                 
                                             }
                                 
                                         });
                                 
                                 swal("Payment has been changed", {
                                   icon: "success",
                                 });
                                 } else {
                                 location.reload(true);
                                 swal("Your imaginary file is safe!");
                                 }
                                 });
                                 
                                 }
                                 
                              </script>
<!-- email end here -->
<script language="javascript" type="text/javascript">
   function printDiv(divID) {
   
       //Get the HTML of div
   
       var divElements = document.getElementById(divID).innerHTML;
   
       //Get the HTML of whole page
   
       var oldPage = document.body.innerHTML;
   
   
   
       //Reset the page's HTML with div's HTML only
   
       document.body.innerHTML = 
   
         "<html><head><title></title></head><body>" + 
   
         divElements + "</body>";
   
   
   
       //Print Page
   
       window.print();
   
   
   
       //Restore orignal HTML
   
       document.body.innerHTML = oldPage;
   
   }
   
   function closeWindow() {
   
       location.reload(); 
   
   }
   
   
   function invoice_filter_url(val){
   
   var invoiceID = "<?php echo $this->uri->segment(3) ?>";
   
   window.location.href=base_url+"invoice/view/"+invoiceID+"?type=clg&year="+val;
   
   }
   
   
   function check_email(email) {
   
       var status = false;     
   
       var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
   
       if (email.search(emailRegEx) == -1) {
   
           $("#to_error").html('');
   
           $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
   
       } else {
   
           status = true;
   
       }
   
       return status;
   
   }
   
   
   
   
   
   $("#send_pdf").click(function(){
   
       var to = $('#to').val();
   
       var subject = $('#subject').val();
   
       var message = $('#message').val();
   
       var id = "<?=$invoice->invoiceID;?>";
   
       var error = 0;
   
   
   
       if(to == "" || to == null) {
   
           error++;
   
           $("#to_error").html("");
   
           $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
   
       } else {
   
           if(check_email(to) == false) {
   
               error++
   
           }
   
       } 
   
   
   
       if(subject == "" || subject == null) {
   
           error++;
   
           $("#subject_error").html("");
   
           $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
   
       } else {
   
           $("#subject_error").html("");
   
       }
   
   
   
       if(error == 0) {
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('invoice/send_mail')?>",
   
               data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,
   
               dataType: "html",
   
               success: function(data) {
   
                   location.reload();
   
               }
   
           });
   
       }
   
   });
   
   
   
</script>
<?php }} ?>
<style type="text/css">
   .boredr_none .nav-tabs{
   border-bottom: 0px solid #e8edef;
   }
   .nopading{
   padding-right:0px;
   padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #2057a8;
   color:#fff;
   }
   .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
   color: #fff;
   }
   .theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */
   color: #fff;
   }
   .theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */
   color: #fff;
   }
   .theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */
   color: #fff;
   }
   .forpostionReletive {
   position: relative;
   }
   .postionAbsoluter {
   position: absolute;
   right: 7px;
   top: 8px;
   }
   .action-layout ul li{
   display:inline-block;
   }
</style>



  <!-- Modal -->
  <div class="modal fade" id="addBookDialog" role="dialog" style="margin-top: 50px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Receipt</h4>
        </div>
        <div class="modal-body">
          <form  action="<?php echo base_url() ?>invoice/addReceipt" method="post" enctype="multipart/form-data">
    <label>Upload Receipt</label>
    <input type='file' name='userfile' />
    <input type="hidden" name="invoiceID" value="<?php echo $invoice->invoiceID ?>">
    <input type="hidden" name="year" value="<?php echo $year ?>">
    <input type="hidden" name="paymentID" id="bookId" value="" >
    <input type="hidden" name="feeType"  value="<?php echo $feeType ?>" >
    <input type="hidden" name="month"  value="<?php echo $month ?>" >
    <input type="hidden" name="monthYear"  value="<?php echo $monthYear ?>" >
    <input type="submit" name="submit" value="Submit" class="btn btn-success" style="margin-top: 15px;">
    </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <script type="text/javascript">
      $(document).on("click", ".open-AddBookDialog", function () {
     var myBookId = $(this).data('id');
     $(".modal-body #bookId").val( myBookId );
     // As pointed out in comments, 
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
  </script>
  <style type="text/css">
  #for_write_tr_fee_str{
    display:none;
  }
</style>
<script type="text/javascript">
  function Edit_fee_struc_row(){
    $('#for_read_tr_fee_str').hide();
    $('#for_write_tr_fee_str').show();

  }
    function Edit_fee_struc_row_back(){
    $('#for_read_tr_fee_str').show();
    $('#for_write_tr_fee_str').hide();

  }
</script>
<style type="text/css">
  .displayNone{
    display:none;
  }
</style>
<script type="text/javascript">
  function Edit_fee_paid_row(val){
    $('#for_read_tr_fee_paid_'+val).hide();
    $('#for_write_tr_fee_paid_'+val).show();

  }
    function Edit_fee_paid_row_back(val){
    $('#for_read_tr_fee_paid_'+val).show();
    $('#for_write_tr_fee_paid_'+val).hide();

  }
</script>