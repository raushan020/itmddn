<head>
   <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,700italic,900);
      body { font-family: 'Roboto', Arial, sans-serif !important; }
      a[href^="tel"]{
      color:inherit;
      text-decoration:none;
      outline:0;
      }
      a:hover, a:active, a:focus{
      outline:0;
      }
      a:visited{
      color:#FFF;
      }
      span.MsoHyperlink {
      mso-style-priority:99;
      color:inherit;
      }
      span.MsoHyperlinkFollowed {
      mso-style-priority:99;
      color:inherit;
      }
   </style>
</head>
<body style="margin: 0; padding: 0;background-color:#EEEEEE;">
   <div style="display:none;font-size:1px;color:#333333;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
      Questions? Call us any time 24/7 at 1-800-672-4399 or simply reply to this email | Chewy.com
   </div>
   <table cellspacing="0" style="margin:0 auto; width:100%; border-collapse:collapse; background-color:#EEEEEE; font-family:'Roboto', Arial !important">
      <tbody>
         <tr>
            <td align="center" style="padding:20px 23px 0 23px" >
               <table width="600" style="background-color:#FFF; margin:0 auto; border-radius:5px">
                  <tbody>
                     <tr>
                        <td align="center">
                           <table width="500" style="margin:0 auto">
                              <tbody>
                                 <tr>
                                    <td align="center" style="padding:40px 0 35px 0"><a href="#" target="_blank" style="color:#128ced; text-decoration:none;outline:0;"><img alt="" src="https://www.chewy.com/static/cms/lp/email/logo.png" border="0"></a>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td align="center" style="font-family:'Roboto', Arial !important">
                                       <h2 style="margin:0; font-weight:bold; font-size:40px; color:#444; text-align:center; font-family:'Roboto', Arial !important">
                                          Payment Details
                                       </h2>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td align="center" style="padding:0 0 15px 0; font-family:'Roboto', Arial !important">
                                       <p style="text-align: center;">
                                          <img src="#" border="0">
                                       </p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td align="center" style="padding:0 0 20px 0; font-family:'Roboto', Arial !important">
                                       <p style="margin:10px 0; font-size:16px; color:#000; line-height:12px; font-family:'Roboto', Arial !important">
                                       </p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>

                                      <table width="100%" style="border-collapse:collapse">
                                          <tbody>
                                             <tr>
                                                <td style="vertical-align:top; padding:18px 18px 8px 23px; font-family:'Roboto', Arial !important">
                                                   <p style="font-size:16px; color:#333333; text-transform:uppercase; font-weight:900; margin:0; font-family:'Roboto', Arial !important">
                                                      <?=$this->lang->line("invoice_invoice").$invoice->invoiceID?>
                                                   </p>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td style="vertical-align:top; padding:0 18px 18px 23px; font-family:'Roboto', Arial !important">
                                                   <table width="100%" style="border-collapse:collapse">
                                                      <tbody>
                                                         <tr>
                                                            <td style="font-family:'Roboto', Arial !important">
                                                               <p style="font-size:16px; color:#000; margin:0 0 5px 0; font-family:'Roboto', Arial !important">
                                                                  <?=$student->name?>
                                                                  <?php
                                                                     if($invoice->paiddate) {
                                                                         echo "<b>".$this->lang->line("invoice_pdate")." :  </b>". date("d M Y", strtotime($invoice->paiddate)).'<br/>';
                                                                     }
                                                                     ?>
                                                                  <?php 
                                                                     $status = $invoice->status;
                                                                     $setstatus = '';
                                                                     if($status == 0) {
                                                                         $status = $this->lang->line('invoice_notpaid');
                                                                     } elseif($status == 1) {
                                                                         $status = $this->lang->line('invoice_partially_paid');
                                                                     } elseif($status == 2) {
                                                                         $status = $this->lang->line('invoice_fully_paid');
                                                                     }
                                                                     
                                                                     echo $this->lang->line('invoice_status'). " : ". "<button class='btn btn-success btn-xs'>".$status."</button>";
                                                                     ?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                        </td>
                     </tr>
                     <tr>
                        <td align="center" cellspacing="0" style="padding:0 0 30px 0; vertical-align:middle">
                           <table width="550" style="border-collapse:collapse; background-color:#FaFaFa; margin:0 auto; border:1px solid #E5E5E5">
                              <tbody>
                                 <tr>
                                    <td width="276" style="vertical-align:top; border-right:1px solid #E5E5E5">
                                       <table style="width:100%; border-collapse:collapse">
                                          <tbody>
                                             <tr>
                                                <td style="vertical-align:top; padding:18px 18px 8px 23px; font-family:'Roboto', Arial !important">
                                                   <p style="font-size:16px; color:#333333; text-transform:uppercase; font-weight:900; margin:0; font-family:'Roboto', Arial !important">
                                                      <?php  echo $this->lang->line("invoice_from"); ?>
                                                   </p>
                                                </td>
                                             </tr>
                                             <tr style="">
                                                <td style="vertical-align:top; padding:0 18px 18px 23px">
                                                   <table width="100%" style="border-collapse:collapse">
                                                      <tbody>
                                                         <tr>
                                                            <td align="left" style="font-family:'Roboto', Arial !important">
                                                               <p style="font-size:16px; color:#000; margin:0 0 5px 0; font-family:'Roboto', Arial !important">
                                                                  <?=$siteinfos->sname?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td align="left" style="font-family:'Roboto', Arial !important">
                                                               <p style="font-size:16px; color:#000; margin:0 0 5px 0; font-family:'Roboto', Arial !important">
                                                                  <?=$siteinfos->address?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td align="left" style="font-family:'Roboto', Arial !important">
                                                               <p style="font-size:16px; color:#000; margin:0 0 10px 0; font-family:'Roboto', Arial !important">
                                                                  <?=$this->lang->line("invoice_phone"). " : ". $siteinfos->phone?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td align="left" style="font-family:'Roboto', Arial !important;" colspan="2">
                                                               <p style="font-size:16px; color:#bc0101; margin:0;padding:0; font-weight:bold; font-family:'Roboto', Arial !important">
                                                                  <?=$this->lang->line("invoice_email"). " : ". $siteinfos->email?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                    <td style="vertical-align:top">
                                       <table width="100%" style="border-collapse:collapse">
                                          <tbody>
                                             <tr>
                                                <td style="vertical-align:top; padding:18px 18px 8px 23px; font-family:'Roboto', Arial !important">
                                                   <p style="font-size:16px; color:#333333; text-transform:uppercase; font-weight:900; margin:0; font-family:'Roboto', Arial !important">
                                       to
                                                   </p>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td style="vertical-align:top; padding:0 18px 18px 23px; font-family:'Roboto', Arial !important">
                                                   <table width="100%" style="border-collapse:collapse">
                                                      <tbody>
                                                         <tr>
                                                            <td style="font-family:'Roboto', Arial !important">
                                                               <p style="font-size:16px; color:#000; margin:0 0 5px 0; font-family:'Roboto', Arial !important">
                                                                Name:  <?=$student->name?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td style="font-family:'Roboto', Arial !important">
                                                               <p style="font-size:16px; color:#000; margin:0 0 5px 0; font-family:'Roboto', Arial !important">
                                                                Roll No.:  <?=$student->roll?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td style="font-family:'Roboto', Arial !important;">
                                                               <p style="font-size:16px; color:#000; margin:0;padding:0; font-family:'Roboto', Arial !important">
                                                              Course:  <?= $classes->classes?>
                                                               </p>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td style="font-family:'Roboto', Arial !important;">
                                                               <p style="font-size:16px; color:#000; margin:0;padding:0; font-family:'Roboto', Arial !important">
                                                              
                                                               </p>
                                                            </td>
                                                         </tr>
                                                                                                            <tr>
                                                            <td style="font-family:'Roboto', Arial !important;">
                                                               <p style="font-size:16px; color:#000; margin:0;padding:0; font-family:'Roboto', Arial !important">
                                                           
                                                               </p>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td align="center" cellspacing="0" style="padding:0; vertical-align:middle">
                           <table width="550" style="border-collapse:collapse; background-color:#FaFaFa; margin:0 auto; border-bottom:1px solid #E5E5E5">
                              <tbody>
                                 <tr>
                                    <td align="left" style="padding:15px 0 15px 15px; font-family:'Roboto', Arial !important" width="117">
                                       <p style="font-size:16px; text-transform:uppercase; color:#333333; margin:0; font-weight:900; font-family:'Roboto', Arial !important; ">
                                          Fee Applied
                                       </p>
                                    </td>
                                    <td width="80" align="right" style="font-family:'Roboto', Arial !important;padding-right:10px;">
                                       <p style="margin:0; font-size:14px; color:#333333;padding:0;font-family:'Roboto', Arial !important;text-align:right;">
                                          Amount
                                       </p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td style=" font-family:'Roboto', Arial !important;padding:0;" align="center">
                           <table width="550" style="border-collapse:collapse;margin: 0 auto;border-bottom: 1px solid #EBEBEB">
                              <tbody>
                                 <tr>
                                    <td width="270" style="vertical-align:middle; padding:0 0 0 10px; font-family:'Roboto', Arial !important;">
                                       <p style="font-size:16px; margin:0; color:#000; line-height:20px; font-family:'Roboto', Arial !important">
                                          Course Fee
                                       </p>
                                    </td>
                                    <td align="center" width="80" style="font-family:'Roboto', Arial !important;padding:0 10px 0 0;">
                                       <p style="font-size:18px; color:#bc0101; margin:0; font-family:'Roboto', Arial !important;text-align:center;font-weight:bold;text-align: right;">
                                          Rs.<?php echo $classes->fee; ?>
                                       </p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <?php 
                        foreach ($feeApplied as $key => $value) { ?>
                     <tr>
                        <td style=" font-family:'Roboto', Arial !important;padding:0;" align="center">
                           <table width="550" style="border-collapse:collapse;margin: 0 auto;border-bottom: 1px solid #EBEBEB">
                              <tbody>
                                 <tr>
                                    <td width="270" style="vertical-align:middle; padding:0 0 0 10px; font-family:'Roboto', Arial !important;">
                                       <p style="font-size:16px; margin:0; color:#000; line-height:20px; font-family:'Roboto', Arial !important">
                                          <?php echo $value->feeName ?>
                                       </p>
                                    </td>
                                    <td align="center" width="80" style="font-family:'Roboto', Arial !important;padding:0 10px 0 0;">
                                       <p style="font-size:18px; color:#bc0101; margin:0; font-family:'Roboto', Arial !important;text-align:center;font-weight:bold;text-align: right;">
                                         Rs.<?php echo $value->amount; ?>
                                       </p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <?php } ?>
                     <tr>
                        <td align="center" style="padding-top:24px; padding-bottom:20px">
                           <table width="520" style="border-collapse:collapse">
                              <tbody>
                                 <tr>
                                    <td align="right" width="425" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; font-weight:900; margin:0; font-family:'Roboto', Arial !important">
                                          <?=$this->lang->line('invoice_subtotal')?>
                                       </p>
                                    </td>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#bc0101; font-weight:bold; margin:0; font-family:'Roboto', Arial !important">
                                         Rs.<?=$invoice->amount?>
                                       </p>
                                    </td>
                                 </tr>
                                 <?php if(empty($invoice->paidamount) && $invoice->paidamount == 0) { ?>
                                 <tr>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important">
                                          <?=$this->lang->line('invoice_total')." (".$siteinfos->currency_code.")";?>
                                       </p>
                                    </td>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important;color:#bc0101;font-weight:bold;">
                                        Rs.<?=$siteinfos->currency_symbol." ".$invoice->amount?>
                                       </p>
                                    </td>
                                 </tr>
                                 <?php } else { if($invoice->amount == $invoice->paidamount && $invoice->status == 2) { ?>
                                 <tr>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important">
                                          <?=$this->lang->line('invoice_total')." (".$siteinfos->currency_code.")";?>
                                       </p>
                                    </td>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important;color:#bc0101;font-weight:bold;">
                                          Rs.<?=$siteinfos->currency_symbol." ".$invoice->amount?>
                                       </p>
                                    </td>
                                 </tr>
                                 <?php } elseif($invoice->amount > $invoice->paidamount && $invoice->status == 1) { ?>
                                 <tr>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important">
                                          <?=$this->lang->line('invoice_made');?>
                                       </p>
                                    </td>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important;color:#bc0101;font-weight:bold;">
                                          Rs.<?=$invoice->paidamount?>
                                       </p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <?php $due = $invoice->amount-$invoice->paidamount; ?>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important">
                                          <?=$this->lang->line('invoice_due')." (".$siteinfos->currency_code.")";?>
                                       </p>
                                    </td>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important;color:#bc0101;font-weight:bold;">
                                          Rs.<?= $due?>
                                       </p>
                                    </td>
                                 </tr>
                                 <?php } else { ?>
                                 <tr>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important">
                                          <?=$this->lang->line('invoice_due')." (".$siteinfos->currency_code.")";?>
                                       </p>
                                    </td>
                                    <td align="right" style="padding-bottom:15px; font-family:'Roboto', Arial !important">
                                       <p style="font-size:18px; color:#000; margin:0; font-family:'Roboto', Arial !important;color:#bc0101;font-weight:bold;">
                                          Rs.<?= $due?>
                                       </p>
                                    </td>
                                 </tr>
                                 <?php } }  ?>
                          
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding-top:20px">
               <table width="604" style="border-collapse:collapse;background-color:#FFF; font-family:'Roboto', Arial !important; border-radius:5px">
                  <tbody>
                     <tr>
                        <td colspan="4" style="vertical-align:middle;background-color: #128ced;border-radius: 5px 5px 0 0;">
                           <table style="background-color:#128ced; width:100%; border-radius:5px 5px 0 0; border-collapse:collapse">
                              <tbody>
                                 <tr>
                                    <td align="center" style="vertical-align:middle; padding:22px 0; font-family:'Roboto', Arial !important">
                                       <p style="color:#FFF; font-size:18px; margin:0; font-family:'Roboto', Arial !important">
                                          Call us at <a href="tel:xxxxxxxxx" target="_blank" style="text-decoration:none; color:#FFF; font-weight:bold;outline:0;">XXXXXXXXXXXX</a>
                                          or reply to this email
                                       </p>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding-top:29px; padding-bottom:50px">
               <table style="width:100%">
                  <tbody>
                     <tr>
                        <td align="center" style="font-family:'Roboto', Arial !important">
                           <a href="#" target="_blank" style="color:#128ced; text-decoration:none;outline:0;">Edge Technosoft</a>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
</body>