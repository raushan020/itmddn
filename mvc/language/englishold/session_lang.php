<?php



/* List Language  */

$lang['panel_title'] = "Session";

$lang['add_title'] = "Add a Session";

$lang['slno'] = "#";

$lang['session_from'] = "Session From";
$lang['session_to'] = "Session To";

$lang['calender_from'] = "Calender From";
$lang['calender_to'] = "Calender To";

$lang['academic_from'] = "Academic From";
$lang['academic_to'] = "Academic To";

$lang['courses_numeric'] = "Course Numeric";

$lang['teacher_name'] = "Teacher Name";

$lang['courses_note'] = "Note";

$lang['action'] = "Action";



$lang['courses_select_teacher'] = "Select Teacher";



$lang['view'] = 'View';

$lang['edit'] = 'Edit';

$lang['delete'] = 'Delete';



/* Add Language */



$lang['add_session'] = 'Add Session';

$lang['update_session'] = 'Update session';