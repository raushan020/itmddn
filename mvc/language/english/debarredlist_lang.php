<?php



/* List Language  */

$lang['panel_title'] = "Student_Debarred_List";

$lang['add_title'] = "Add a Professor";

$lang['slno'] = "#";

$lang['professor_photo'] = "Photo";

$lang['pofessor_name'] = "Name"; 

$lang['professor_designation'] = 'Designation';

$lang['professor_email'] = "Email";

$lang['professor_dob'] = "Date of Birth";

$lang['professor_sex'] = "Gender";

$lang['professor_sex_male'] = "Male";

$lang['professor_sex_female'] = "Female";

// $lang['Professor_religion'] = "Religion";

$lang['professor_phone'] = "Mobile No.";

$lang['professor_address'] = "Address";

$lang['professor_jod'] = "Joining Date";

$lang['professor_username'] = "Username";

$lang['professor_password'] = "Password";

$lang['professor_status'] = "Status";





$lang['action'] = "Action";

$lang['view'] = 'View';

$lang['edit'] = 'Edit';

$lang['delete'] = 'Delete';

$lang['print'] = "Print";

$lang['pdf_preview'] = "PDF Preview";

$lang['idcard'] = "ID Card";

$lang['mail'] = "Send Pdf to Mail";



/* Add Language */

$lang['personal_information'] = "Personal Information";

$lang['add_professor'] = 'Add Professor';

$lang['update_professor'] = 'Update Professor';



$lang['to'] = 'To';

$lang['subject'] = 'Subject';

$lang['message'] = 'Message';

$lang['send'] = 'Send';

$lang['mail_to'] = "The To field is required.";

$lang['mail_valid'] = "The To field must contain a valid email address.";

$lang['mail_subject'] = "The Subject field is required.";

$lang['mail_success'] = 'Email send successfully!';

$lang['mail_error'] = 'oops! Email not send!';