<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	// print_r("language");die;
	function __construct() {

		parent::__construct();
		
		$this->load->model('systemadmin_m');
		
		$this->load->model('admin_m');

		$this->load->model("dashboard_m");

		$this->load->model("automation_shudulu_m");

		$this->load->model("automation_rec_m");

		$this->load->model("setting_m");

		$this->load->model("notice_m");

		$this->load->model("etsexam_m");

		$this->load->model("user_m");

		$this->load->model("student_m");

		$this->load->model("classes_m");

		$this->load->model("teacher_m");

		$this->load->model("invoice_gen_m");

		// $this->load->model("parentes_m");

		$this->load->model("sattendance_m");

		$this->load->model("subject_m");

		$this->load->model("feetype_m");

		$this->load->model("invoice_m");

		$this->load->model("exam_m");

		$this->load->model("expense_m");

		$this->load->model("payment_m");

		$this->load->model("lmember_m");

		$this->load->model("book_m");

		$this->load->model("issue_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_m");

		$this->load->model('hmember_m');

		$this->load->model('tmember_m');

		$this->load->model('professor_m');

		$this->load->model('event_m');

		$this->load->model('holiday_m');

		$this->load->model('visitorinfo_m');

		$language = $this->session->userdata('lang');
		$this->lang->load('dashboard', $language);

		/* Automation Start */

		$cnt = 0;

		$date = date('Y-m-d');

		$day = date('d');

		$month = date('m');

		$year = date('Y');	

		$setting = $this->setting_m->get_setting(array('adminID'=>$this->session->userdata("adminID")));

		if($day >= $setting->automation) {

			$automation_shudulus = $this->automation_shudulu_m->get_automation_shudulu();

			if(count($automation_shudulus)) {

				foreach ($automation_shudulus as $automation_shudulu) {

					if($automation_shudulu->month == $month && $automation_shudulu->year == $year) {

						$cnt = 1;

					}

				}

				if($cnt === 0) {

					$alltotalamount = 0;

					$alltotalamounttransport = 0;

					$alltotalamounthostel = 0;

					/* Library Start */

					$student_librarys = $this->student_m->get_order_by_student(array('library' => 1));

					foreach ($student_librarys as $student_library) {

						$db_lmember = $this->lmember_m->get_single_lmember(array('studentID' => $student_library->studentID));

						if(count($db_lmember)) {

							if($db_lmember->lbalance > 0) {

								$automation_rec_library = $this->automation_rec_m->get_order_by_automation_rec(array(

									'studentID' => $student_library->studentID,

									'month' => $month,

									'year' => $year,

									'nofmodule' => 5427279

								));

								if(!count($automation_rec_library)) {

									$alltotalamount = ($student_library->totalamount)+($db_lmember->lbalance);

									$array_library = array(

										'totalamount' => $alltotalamount

									);

									$dbclasses = $this->classes_m->get_classes($student_library->classesID);

									$array_library_invoice = array(

										'classesID' => $student_library->classesID,

										'classes' => $dbclasses->classes,

										'studentID' => $student_library->studentID,

										'student' => $student_library->name,

										'roll' => $student_library->roll,

										'feetype' => $this->lang->line('dashboard_libraryfee'),

										'amount' => $db_lmember->lbalance,

										'status' => 0,

										'date' => $date,

										'year' => $year

									);

									$this->invoice_m->insert_invoice($array_library_invoice);

									$this->student_m->update_student($array_library, $student_library->studentID);

									$this->automation_rec_m->insert_automation_rec(array(

										'studentID' => $student_library->studentID,

										'date' => $date,

										'day' => $day,

										'month' => $month,

										'year' => $year,

										'nofmodule' => 5427279

									));

									$alltotalamount = 0;

								}

							}

						}

					}

					/* Library Close */

					/* Transport Start */

					$student_transports = $this->student_m->get_order_by_student(array('transport' => 1));

					foreach ($student_transports as $student_transport) {

						$db_tmember = $this->tmember_m->get_single_tmember(array('studentID' => $student_transport->studentID));

						if(count($db_tmember)) {

							if($db_tmember->tbalance > 0) {

								$automation_rec_transport = $this->automation_rec_m->get_order_by_automation_rec(array(

									'studentID' => $student_transport->studentID,

									'month' => $month,

									'year' => $year,

									'nofmodule' => 872677678

								));

								if(!count($automation_rec_transport)) {

									$alltotalamounttransport = ($student_transport->totalamount)+($db_tmember->tbalance);

									$array_transport = array(

										'totalamount' => $alltotalamounttransport

									);

									$dbclasses = $this->classes_m->get_classes($student_transport->classesID);

									$array_transport_invoice = array(

										'classesID' => $student_transport->classesID,

										'classes' => $dbclasses->classes,

										'studentID' => $student_transport->studentID,

										'student' => $student_transport->name,

										'roll' => $student_transport->roll,

										'feetype' => $this->lang->line('dashboard_transportfee'),

										'amount' => $db_tmember->tbalance,

										'status' => 0,

										'date' => $date,

										'year' => $year

									);

									$this->invoice_m->insert_invoice($array_transport_invoice);

									$this->student_m->update_student($array_transport, $student_transport->studentID);

									$this->automation_rec_m->insert_automation_rec(array(

										'studentID' => $student_transport->studentID,

										'date' => $date,

										'day' => $day,

										'month' => $month,

										'year' => $year,

										'nofmodule' => 872677678

									));

									$alltotalamounttransport = 0;

								}

							}

						}

					}


					/* Transport Close */

					/* Hostel Start */

					$student_hostels = $this->student_m->get_order_by_student(array('hostel' => 1));

					foreach ($student_hostels as $student_hostel) {

						$db_hmember = $this->hmember_m->get_single_hmember(array('studentID' => $student_hostel->studentID));

						if(count($db_hmember)) {

							if($db_hmember->hbalance > 0) {

								$automation_rec_hostel = $this->automation_rec_m->get_order_by_automation_rec(array(

									'studentID' => $student_hostel->studentID,

									'month' => $month,

									'year' => $year,

									'nofmodule' => 467835

								));


								if(!count($automation_rec_hostel)) {

									$alltotalamounthostel = ($student_hostel->totalamount)+($db_hmember->hbalance);

									$array_hostel = array(

										'totalamount' => $alltotalamounthostel

									);

									$dbclasses = $this->classes_m->get_classes($student_hostel->classesID);

									$array_hostel_invoice = array(

										'classesID' => $student_hostel->classesID,

										'classes' => $dbclasses->classes,

										'studentID' => $student_hostel->studentID,

										'student' => $student_hostel->name,

										'roll' => $student_hostel->roll,

										'feetype' => $this->lang->line('dashboard_hostelfee'),

										'amount' => $db_hmember->hbalance,

										'status' => 0,

										'date' => $date,

										'year' => $year

									);

									$this->invoice_m->insert_invoice($array_hostel_invoice);

									$this->student_m->update_student($array_hostel, $student_hostel->studentID);

									$this->automation_rec_m->insert_automation_rec(array(

										'studentID' => $student_hostel->studentID,

										'date' => $date,

										'day' => $day,

										'month' => $month,

										'year' => $year,

										'nofmodule' => 467835

									));

									$alltotalamounthostel = 0;

								}

							}

						}

					}

					/* Hostel Close */

					$this->automation_shudulu_m->insert_automation_shudulu(array(

						'date' => $date,

						'day' => $day,

						'month' => $month,

						'year' => $year

					));

				}

			} else {

				$this->automation_shudulu_m->insert_automation_shudulu(array(

					'date' => $date,

					'day' => $day,

					'month' => $month,

					'year' => $year

				));

			}

		}

		/* Automation Close */

	}

	public function index() 
	{
		// print_r("fesckjcsn");die();
		$usertype = $this->session->userdata('usertype');
		$day = abs(date('d'));
		$monthyear = date('m-Y');
		$this->data['event'] = $this->event_m->get_event();
		$this->data['holiday'] = $this->holiday_m->get_holiday();
		// dump($this->data['event']);
		// dump($this->data['holiday']);
		// die;
		if($usertype == "superadmin") 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
			// print_r($this->data['user']);die;
            $this->data['notices']  = $this->notice_m->get_notice_array(array('userID'=>$this->data['user']->adminID));
           /* print_r($this->data['notices']);
            exit();*/
	     	//$this->data['notices'] = $this->notice_m->get_notice();
	     	$this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
	     	$this->data['Totaluser'] = $this->admin_m->nub_ofuser_by_superAdmin();
			$this->data['student'] = $this->student_m->get_student_by_superAdmin();
			$this->data['professors_count'] = $this->professor_m->get_professor_all_by_superAdmin();
			$this->data['courses'] = $this->classes_m->get_classes_active_by_superAdmin();
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['parents'] = $this->parentes_m->get_parentes();
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		if($usertype == "ClgAdmin" || $usertype == "Support") 
		{
		 $adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			$loginuserID = $this->session->userdata('loginuserID');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
			//$this->data['notices'] = $this->notice_m->notice_icon_count($loginuserID);
            $callAllNotice  = $this->notice_m->get_notice_array(array('adminID'=>$this->data['user']->adminID));
			foreach($callAllNotice as $key => $value) 
			{
				if($value['usertype']=='Professor')
				{					 
				   $callAllNotice[$key]['name'] = "Professor";
					$callAllNotice[$key]['bysend'] = "Professor";

				}
				elseif($value['usertype']=='ClgAdmin')
				{
					$callAllNotice[$key]['name'] = "Director";
					$callAllNotice[$key]['bysend'] = "Director";	
				}
				elseif ($value['usertype']=='Accountant') 
				{
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant1";
				}
				elseif ($value['usertype']=='Admin') 
				{
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
			}
			$this->data['admindetails']=$this->admin_m->getalladmindata($adminID);
			$this->data['notices']  = $callAllNotice;
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser($adminID);
          	$this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
	      	//$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['student_notification'] = $this->student_m->student_notification($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['professors_notification'] = $this->professor_m->professors_notification($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['courses_notification'] = $this->classes_m->courses_notification($adminID);
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		if($usertype == "Accountant" ) 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			/*$userid= $this->session->userdata('userID');
			print_r( $userid);
			exit(); */
			$this->data['user'] = $this->admin_m->get_admin_data(array('username'  => $username)); 
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser($adminID);
          	$this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
          	$this->data['notices']  = $this->notice_m->get_notice_Acc(array('userID'=>$this->data['user']->userID));
	      	//$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		elseif($usertype == "Admin") 
		{
			$adminID = $this->session->userdata("adminID");
			$userID = $this->session->userdata("loginuserID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
            $callAllNotice  = $this->notice_m->notic_record_admin();
			/*print_r($callAllNotice);
			exit(); */
			// print_r($callAllNotice);die;
            foreach($callAllNotice as $key => $value) 
			{
				if($value['usertype']=='Professor')
				{					 
				   $callAllNotice[$key]['name'] = "Professor";
					$callAllNotice[$key]['bysend'] = "Professor";

				}
				elseif($value['usertype']=='ClgAdmin')
				{
					$callAllNotice[$key]['name'] = "Director";
					$callAllNotice[$key]['bysend'] = "Director";	
				}
				elseif ($value['usertype']=='Accountant') 
				{
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant1";
				}
				elseif ($value['usertype']=='Admin') 
				{
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
			}
			$this->data['notices']  = $callAllNotice;
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser($adminID);
			$this->data['user_notification'] = $this->admin_m->user_notification($adminID);
            $this->data['totalPayment'] = $this->invoice_m->total_revenue_dashboard();
          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		elseif($usertype == "Professor") 
		{
			$adminID = $this->session->userdata("adminID");
            $loginuserID = $this->session->userdata("loginuserID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
			$professor = $this->professor_m->get_single_professor(array('username' => $username));
			/*print_r($professor);
			exit();*/ 
			$this->data['professor'] = $professor;
			$this->data['notices']  = $this->notice_m->get_notice_Acc(array('professorID'=>$professor->professorID));
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser($adminID);
          	$this->data['totalPayment'] = $this->invoice_m->totalPayment();
           	$this->data['duePayment'] = $this->invoice_m->duePayment();
          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
			//	$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
	        $this->data['professor_signle']	=  $this->professor_m->get_professor2(array('professorID'=>$loginuserID));
			$this->data['courses'] = $this->professor_m->get_classes($this->data['professor_signle']->departmentID);
            $this->data['student'] = $this->professor_m->get_student($this->data['professor_signle']->departmentID);
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->data['subject'] = $this->professor_m->get_subject($this->data['professor_signle']->departmentID);
			$record=$this->professor_m->getsubjectandtopic($loginuserID);
        } 
		elseif($usertype == "Teacher") 
		{
			$adminID = $this->session->userdata("adminID");
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));
			// print_r($this->data['Totaluser']);
			// exit();
			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();
          	$this->data['totalPayment'] = $this->invoice_m->totalPayment();
           	$this->data['duePayment'] = $this->invoice_m->duePayment();
          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();
          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();
          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();
	     	$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);
			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);
			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);
			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));
			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));
			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->data['subject'] = $this->subject_m->get_subject();
		} 
		elseif($usertype == "Accountant") {
			$this->data['account'] = $this->invoice_m->get_invoice(array('username'  => $username));
			$username = $this->session->userdata('username');
			/*$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));*/
			/*$this->data['notices']  = $this->notice_m->get_notice_account(array('userID'=>$this->data['user']->userID));*/
			//$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['invoice'] = $this->invoice_m->get_invoice();
			$this->data['feetype'] = $this->feetype_m->get_feetype();
			$this->data['expense'] = $this->expense_m->get_expense();
		} 
		elseif($usertype == "Librarian") {
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));
			$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['lmember'] = $this->lmember_m->get_lmember();
			$this->data['book'] = $this->book_m->get_book();
			$this->data['issue'] = $this->issue_m->get_order_by_issue(array('return_date' => NULL));
		} 
		elseif($usertype == "Student") 
		{
			$loginuserID = $this->session->userdata("loginuserID");
			$username = $this->session->userdata('username');
			$adminID = $this->session->userdata('adminID');
			if ($this->input->post('weatherfunctionSession')) {
				$this->session->set_userdata('weatherfunctionSession');
			}
			$this->data['user'] = $this->student_m->get_single_student(array('username'  => $username));
			$this->data['Examtime'] = $this->etsexam_m->exam_dateStart($this->data['user']->sessionType,$this->data['user']->session,$this->data['user']->examType,$this->data['user']->yearsOrSemester,$this->data['user']->education_mode,$this->data['user']->yosPosition);
			//$this->data['NoticeD'] = $this->notice_m->order_Desc($this->data['user']->classesID);
			$this->data['totalamountSend'] = $this->invoice_gen_m->TotalFee($this->data['user']->studentID,$adminID);
			$callAllNotice  = $this->notice_m->notic_record_5(array('studentID'=>$this->session->userdata('loginuserID')));
			foreach($callAllNotice as $key => $value) 
			{
				if($value['usertype']=='Professor')
				{					 
				   $callAllNotice[$key]['name'] = "Professor";
				   $callAllNotice[$key]['bysend'] = "Professor";

				}
				elseif($value['usertype']=='ClgAdmin')
				{
					$callAllNotice[$key]['name'] = "Director";
					$callAllNotice[$key]['bysend'] = "Director";	
				}
				elseif ($value['usertype']=='Accountant') 
				{
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant1";
				}
				elseif ($value['usertype']=='Admin') 
				{
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
			}
			$this->data['NoticeD']  = $callAllNotice;
			$this->data['Allnotic']  = $this->exam_m->get($this->data['user']->classesID); 
			$this->data['exams'] = $this->exam_m->get_exam_count($this->data['user']->studentID);								
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['subject'] = $this->subject_m->get_join_where_subject_count($loginuserID,$this->data['user']->classesID,$this->data['user']->sub_coursesID);
			$this->data['optionalsubject'] = $this->subject_m->get_join_where_optionalsubject_count($loginuserID,$this->data['user']->classesID,$this->data['user']->sub_coursesID);
			$this->data["student"] = $this->student_m->get_student(array('studentID'=>$this->data['user']->studentID,'adminID'=>$this->data['user']->adminID));
			$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
			// $this->data["education"] = $this->student_m->get_educations($this->data['user']->studentID);
			if($this->data["student"] && $this->data["class"]) 
			{
				$this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				$this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);
			}
			$lmember = $this->lmember_m->get_single_lmember(array('studentID' => $this->data['user']->studentID));
			if($lmember) 
			{
				$this->data['issue'] = $this->issue_m->get_order_by_issue(array("lID" => $lmember->lID, 'return_date' => NULL));
			} 
			else 
			{
				$this->data['issue'] = NULL;
			}
			$this->data['invoice'] = $this->invoice_m->get_order_by_invoice(array("studentID" => $this->data['user']->studentID));
		} 
		elseif($usertype == "Parent") 
		{
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->parentes_m->get_single_parentes(array('username'  => $username));
			$this->data['notices'] = $this->notice_m->get_notice();
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$students = $this->student_m->get_order_by_student(array('parentID'  => $this->data['user']->parentID));
			$issue = 0;
			$invoice = 0;
			foreach ($students as $student) 
			{
				$lmember = $this->lmember_m->get_single_lmember(array('studentID' => $student->studentID));
				if($lmember)
				{
					$getissue = $this->issue_m->get_order_by_issue(array("lID" => $lmember->lID, 'return_date' => NULL));
					$issue+=count($getissue);
				}
				$getinvoice = $this->invoice_m->get_order_by_invoice(array('studentID' => $student->studentID));
				if($getinvoice) 
				{
					$invoice+=count($getinvoice);
				}
			}
			$this->data['issue'] = $issue;
			$this->data['invoice'] = $invoice;
			$this->data['books'] = $this->book_m->get_book();
		} 
		elseif($usertype == "Receptionist") 
		{
			$username = $this->session->userdata('username');
			$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));
			$this->data['teacher'] = $this->teacher_m->get_teacher();
			$this->data['visitorinfo'] = $this->visitorinfo_m->get_visitorinfo();
			$this->data['notices'] = $this->notice_m->get_notice();
		}
		
		$this->data["subview"] = "dashboard/index";
		$this->load->view('_layout_main', $this->data);
	}
	
	function feedbackMessage(){

    $usertype = $this->session->userdata("usertype");
    $loginuserID = $this->session->userdata("loginuserID");
    $adminID = $this->session->userdata("adminID");
    $setting = $this->setting_m->get_setting_byAdmin($adminID);
    $this->db->where('studentID',$loginuserID);
    $students = $this->db->get('student')->row();
    $category = $this->input->post('category');
    $getadmin=$this->professor_m->getsuperadminid($adminID);
	$institute=$setting->sname;
	if($getadmin)
	{
		$superadminemail=$getadmin->email;

	}
	$getemail=$this->professor_m->getadminid($loginuserID);
	$adminemail="";
	if($getemail)
	{
		$adminemail=$getemail->email;
	}
		$data = array(

		'studentID'=>$loginuserID,

		'name'=>$students->name,

		'message'=>$this->input->post('val'),

		'email'=>$this->input->post('email'),
		'category'=>$category

		);
		$email = $students->email;

		$this->db->insert('feedback',$data);
		$subject = "Feedback From Student";

		$message = '<html><body>';

		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

		$message .= "<tr><td><strong>Category:</strong> </td><td>" . $category . "</td></tr>";

		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";

		$message .= "<tr><td><strong>Message:</strong> </td><td>" . strip_tags($_POST['val']) . "</td></tr>";

		$message .= "</table>";

		$message .= "</body></html>";
		$data['institute']=$institute;
        $data['logo']=$setting->photo;
        $data['superadminemail']=$superadminemail;
        $data['adminemail']=$adminemail;
        $fullname=$students->name;
		$sendmail=emailBySendGrid($email,$fullname,$subject,$message,$data);
				    
		
// team@edgetechnosoft.com,nishantitm5@gmail.com,drvkhanna51@gmail.com




	}





	function paymentscall() {
		
		$usertype = $this->session->userdata('usertype');
		$adminID = $this->session->userdata('adminID');


		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" ||  $usertype == "Teacher" || $usertype == "Accountant") {
if ($usertype == "superadmin") {
			$payments = $this->payment_m->get_payment($adminID);
			$invoices = $this->invoice_m->get_invoice($adminID);
}else{
			$payments = $this->payment_m->get_payment_byAdminID($adminID);
			$invoices = $this->invoice_m->get_invoice_byAdminID($adminID);

}

			
			$npaid = 0;



			$ppaid = 0;



			$fpaid = 0;



			$cash = 0;



			$cheque = 0;



			$paypal = 0;



			$PayuMoney = 0;



			$OnlinePayment = 0;



			$Bank = 0;



			if(count($invoices)) {



				foreach ($invoices as $invoice) {



					if($invoice->status ==3) {

						$npaid++;

					} elseif($invoice->status == 1) {

						$ppaid++;

					} elseif($invoice->status == 2) {

						$fpaid++;

					}



				}



			}

			if(count($payments)) {
				foreach ($payments as $payment) {
					if('Cash' == $payment->paymenttype) {
						$cash++;
					} elseif('Cheque' == $payment->paymenttype) {
						$cheque++;
					} elseif('Paypal' == $payment->paymenttype) {
						$paypal++;
					}
					elseif('PayuMoney' == $payment->paymenttype) {
						$PayuMoney++;
					}elseif('onlinePayment' == $payment->paymenttype) {

						$OnlinePayment++;

					}elseif('Bank' == $payment->paymenttype) {



						$Bank++;



					}



				}



			}







			if(count($invoices)) {



				$json = array("npaid" => $npaid, "ppaid" => $ppaid, "fpaid" => $fpaid, "cash" => $cash, "cheque" => $cheque, "paypal" => $paypal,"PayuMoney" => $PayuMoney,'OnlinePayment'=>$OnlinePayment,'Bank'=>$Bank, "st" => 1);





				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			} else {



				$json = array("npaid" => $npaid, "ppaid" => $ppaid, "fpaid" => $fpaid, "cash" => $cash, "cheque" => $cheque, "paypal" => $paypal, "PayuMoney" => $PayuMoney,'OnlinePayment'=>$OnlinePayment,'Bank'=>$Bank, "st" => 0);



				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			}



		}



	}







	function graphcall() {
		$adminID = $this->session->userdata('adminID');
		$usertype = $this->session->userdata('usertype');
		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Teacher" || $usertype == "Accountant") {
			$payments = $this->payment_m->get_order_by_payment($adminID);
	      	$lastEarn = 0;
	      	$percent = 0;
	      	$monthBalances = array();
	      	$hightEarn['hight'] = 0;
	      	$dataarr = array();
	      	$allyear = array('2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026');
			if(count($payments)) {
				foreach ($allyear as $key => $allyears) {
					foreach ($payments as $key => $payment) {
                    $paymentsa = date("Y", strtotime($payment->paymentdate));
					    if($allyears ==  $paymentsa) {
					      	$lastEarn+=$payment->paymentamount;
					      	$monthBalances[$allyears] = $lastEarn;
					    } else {
					      	if(!array_key_exists($allyears, $monthBalances)) {
					        	$monthBalances[$allyears] = 0;
					      	}
					    }
					 }
				  	if($lastEarn > $hightEarn['hight']) {
				    	$hightEarn['hight'] = $lastEarn;
				  	}
				  	$lastEarn = 0;
				}
				foreach ($monthBalances as $monthBalancekey => $monthBalance) {
					$dataarr[] = $monthBalance;
				}
				$json = array("balance" => $dataarr);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			} else {
				foreach ($allyear as $allyears) {
					$dataarr[] = 0;
				}
				$json = array("balance" => $dataarr);
				header("Content-Type: application/json", true);
				echo json_encode($json);
				exit;
			}
		}
	}

function professor_gender_count(){

$male = 0;

$female = 0;

$na = 0;
$adminID = $this->session->userdata('adminID');

	$query  =  $this->db->query("SELECT COUNT(CASE WHEN sex = 'Male' THEN 1 END) m, COUNT(CASE WHEN sex = 'Female' THEN 1 END) f, COUNT(CASE WHEN sex = '0' THEN 1 END) na FROM professor where status = 1 AND adminID = $adminID ")->row();
if($query){

$male = $query->m;

$female = $query->f;

$na = $query->na;
}
				$json = array("male" => $male, "female" => $female, "na" => $na);

				header("Content-Type: application/json", true);

				echo json_encode($json,JSON_NUMERIC_CHECK);
	

}

    function student_gender_count()
    {
        $male = 0;
        $female = 0;
        $na = 0;
        $adminID = $this->session->userdata('adminID');
        $query  =  $this->db->query("SELECT COUNT(CASE WHEN sex = 'Male' THEN 1 END) m, COUNT(CASE WHEN sex = 'Female' THEN 1 END) f, COUNT(CASE WHEN sex = '0' THEN 1 END) na FROM student where status = 1 AND adminID = $adminID")->row();
        if($query)
        {
            $male = $query->m;
            $female = $query->f;
            $na = $query->na;
        }
        $json = array("male" => $male, "female" => $female, "na" => $na);
        header("Content-Type: application/json", true);
        echo json_encode($json,JSON_NUMERIC_CHECK);
    }

    public function changeli()
    {
        $id = $this->input->post('id');
        $ids = explode("_",$id);
        $type = $ids[0];
        $userid = $ids[1];
        $this->dashboard_m->notification($type,$userid);
    }
    public function pie_chart_js()
    {
    	$this->load->helper('string'); 
    	$loginuserID = $this->session->userdata('loginuserID');
    	$usertype=$this->session->userdata('usertype');
    	if($usertype == "Professor")
    	{
	    	$record=$this->professor_m->getsubjectandtopic($loginuserID);
	    	$responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Topping", 
	            "pattern" => "", 
	            "type" => "string" 
	        ); 
	        $responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Total", 
	            "pattern" => "", 
	            "type" => "number" 
	        ); 

	        $count=0;$count1=0;$count2=0;$data4=array();$data5=array();$data6=array();
	        foreach($record as $row)
	        {
	         $unitavg1=0;$unitavg2=0;$unitavg3=0;
	         
	      	 foreach ($row['object'] as $key )
			 {

	            $a=$key->topicid;
				$b=$key->totalunit;
				if($a)
				{
					$data1=array();
					$data2=array();
					$data3=array();
	                $topicconut=explode(",",$a);
	     			$avg=(count($topicconut)*100)/$b;
	                $unitavg=round($avg,0);
	                if($unitavg >= 1 && $unitavg <= 50)
	                {
	                	
	                	$count1=$count1+1;
		            	$data1=$row['subject'];
	                }
		            else if($unitavg >= 51 && $unitavg <= 75)
	                {
                	
	                	$count2=$count2+1;
	                	$data2=$row['subject'];
	                }
	                else if($unitavg >= 76)
	                {
	                	
	                	$count=$count+1;
	                	$data3=$row['subject'];
	                } 
	               
	            }
	         }
	         $data4[]=$data1; 
	         $data5[]=$data2; 
	         $data6[]=$data3; 
	        }
       
	        $rt[]=$count1;
	        $rt[]=$count2;
	        $rt[]=$count;
      // print_r($count1);die;
      		for ($i=0; $i <count($rt) ; $i++)
            { 
            	$q=array();
            	// print_r(($rt[$i]));die;
            	if($i==0)
            	{
            		if($data4)
            		{
            			foreach($data4 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data4[$key]);
				    }
				    foreach($data4 as $key => $value)  
				    {
				        $q=$value;
				    }
            			// print_r($a);die;
            			// $data['label'][]=$data4;
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $q
                			) 
            		);
            		}
            		else
            		{
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
            		}
            	
            	 	
            	}
            	else if($i==1)
            	{
            		foreach($data5 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data5[$key]);
				    }
				    foreach($data5 as $key => $value)  
				    {
				        $y[]=$value;
				    }
                   // print_r($b);die;
                    // $data['data'][]=$rt[$i];
                    if($data5)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $y
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
            	 	
            	}
            	else
            	{
            		
            		foreach($data6 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data6[$key]);
				    }
				    foreach($data6 as $key => $value)  
				    {
				        $z[]=$value;
				    } 
				    if($data6)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $z
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
               
            	 	
            	}
            	

            }
        echo json_encode($responce);
        }
      // $this->load->view('pie_chart',$data);
    }
    public function pie_chart_js_byadmin()
    {
        $this->load->helper('string'); 
        $adminID = $this->session->userdata('adminID');
    	$professor = $this->professor_m->getall_professor($adminID);
    	$count=0;$count1=0;$count2=0;$data4=array();$data5=array();$data6=array();
        foreach ($professor as $professor)
	    {
	    	// print_r($professor);die;
        	$loginuserID=$professor->professorID;
        	$professorname=$professor->name;
           	$record=$this->professor_m->getsubjectandtopic($loginuserID);
           	// print_r($record);die;
           	 $responce->cols[] = array( 
            "id" => "", 
            "label" => "Topping", 
            "pattern" => "", 
            "type" => "string" 
        	); 
        	$responce->cols[] = array( 
            "id" => "", 
            "label" => "Total", 
            "pattern" => "", 
            "type" => "number" 
        	);
        	$totaltopic="";
        	$totalunit=0;
        	$unit=0;
        	$topic="";
        	// print_r($record);die;
        	foreach($record as $row)
      		{
      			$unitavg1=0;$unitavg2=0;$unitavg3=0;
      			// print_r($row['object']);
        		foreach ($row['object'] as $key )
				{
					// print_r($key);die;
					$a=$key->topicid;
					$b=$key->totalunit;
					if($a)
					{
						
		                $topicconut=explode(",",$a);
		     			$totaltopic=(count($topicconut));
		     			$totalunit=$b;
		     			// print_r($totalunit);die;
		             
               		}
               		
        		}
        		$topic += $totaltopic;
               		$unit += $totalunit;
        		
		    }
		    if($unit != 0)
		    {
		    	$per=($topic*100)/$unit;
			    // print_r($per);
			    $unitavg=round($per,0);
			    // print_r($unitavg);
			    $data1=array();
				$data2=array();
				$data3=array();
	                if($unitavg >= 1 && $unitavg <= 50)
	                {
	                	$count1=$count1+1;
		            	$data1=$professorname;
	                }
		            else if($unitavg >= 51 && $unitavg <= 75)
	                {
			        	$count2=$count2+1;
	                	$data2=$professorname;
	                }
		            else if($unitavg >= 76)
		            {
		            	$count=$count+1;
		            	$data3=$professorname;
	                } 
			    $data4[]=$data1; 
				$data5[]=$data2; 
				$data6[]=$data3; 
		    }
		    else
		    {

		    }
		    
        }
        
      $rt[]=$count1;
      $rt[]=$count2;
      $rt[]=$count;
      for ($i=0; $i <count($rt) ; $i++)
      { 
       	if($i==0)
      	{
        	if($data4)
        	{
        		// print_r($rt[$i]);die;
        		foreach($data4 as $key => $value) 
          		{         
		          if(empty($value)) 
		          unset($data4[$key]);
		        }
			    foreach($data4 as $key => $value)  
			    {
			        $q[]=$value;
			    }
            	$responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Below 50%" 
                    ),
                	array( 
		            "v" => $rt[$i],
		            "f" => $q
                    ) 
            		);
            }
            else
            {
                $responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Below 50%"
                    ),
            	    array( 
		            "v" => 0, 
		            "f" => null 
                	) 
            		);
            }
        }
        else if($i==1)
        {
        	foreach($data5 as $key => $value) 
        	{         
		        if(empty($value)) 
		        unset($data5[$key]);
		    }
		    foreach($data5 as $key => $value)  
		    {
		        $y[]=$value;
		    }
            if($data5)
            {
               	$responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Upto 51%-75%"
                    ),
            		array( 
		            "v" => $rt[$i], 
		            "f" => $y
                	) 
            		);
            }
            else
            {
               	$responce->rows[]["c"] = array( 
                    array(
                    "v" => $rt[$i],
                    "f" => "Upto 51%-75%"
                    ),
            		array( 
		            "v" => 0, 
		            "f" => null 
                	) 
            		);
            }
        }
        else
        {
        	
        	if($i==2)
        	{
        		// print_r($data6);die;
	        	foreach($data6 as $key => $value) 
	    		{         
			        if(empty($value)) 
			        unset($data6[$key]);
			    }
			    foreach($data6 as $key => $value)  
			    {
			        $z[]=$value;
			    } 
			    if($data6)
	            {
	            	$responce->rows[]["c"] = array( 
	                    array(
	                    "v" => $rt[$i],
	                    "f" => "Above 75%" 
	                    ),
	        			array( 
	                    "v" => $rt[$i], 
	                    "f" => $z
		    			) 
	            		);
	            }
	            else
	            {
	            	$responce->rows[]["c"] = array( 
	                    array(
		                "v" => $rt[$i],
	                    "f" => "Above 75%" 
	                    ),
	        			array( 
	                    "v" => 0, 
	                    "f" => null 
	        			) 
	            		);
	            }
	        }
    	}
      }
      echo json_encode($responce);
    }
    public function pie_chart_js_showattendance()
    {
       	$this->load->helper('string'); 
    	$loginuserID = $this->session->userdata('loginuserID');
    	$usertype=$this->session->userdata('usertype');
    	if($usertype == "Professor")
    	{
	    	$record=$this->professor_m->getstudentattendance($loginuserID);
	    	$responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Topping", 
	            "pattern" => "", 
	            "type" => "string" 
	        ); 
	        $responce->cols[] = array( 
	            "id" => "", 
	            "label" => "Total", 
	            "pattern" => "", 
	            "type" => "number" 
	        ); 
            $totalstudentattendance=0;
	        $count=0;$count1=0;$count2=0;$data4=array();$data5=array();$data6=array();
	        $data1=array();
					$data2=array();
					$data3=array();
	        foreach($record as $row)
	        {
	         $countattendance=0;	
	         $unitavg1=0;$unitavg2=0;$unitavg3=0;
	         $totalstudentattendance=count($row['object']);
	      	 foreach ($row['object'] as $key )
			 {
                $present1=0;
	            $present=$key->atd;
				$b=$totalstudentattendance;
				if($present==1)
	            {
	              $countattendance=$countattendance+1;
	            }
	         }
	         if($countattendance >=1)
             {

             	 $avg=($countattendance*100)/$b;
           		 $unitavg=round($avg,0);
           		 if($unitavg >= 0 && $unitavg <= 50)
           		 {
                    $count1=$count1+1;
		            $data1=$row['studentname'];
           		 }
           		 else if($unitavg >= 51 && $unitavg <= 75)
           		 {
                    $count2=$count2+1;
	                $data2=$row['studentname'];
           		 }
           		 else if($unitavg >= 76)
           		 {
                    $count=$count+1;
	                $data3=$row['studentname'];
           		 }
           	 }
	        
	         $data4[]=$data1; 
	         $data5[]=$data2; 
	         $data6[]=$data3; 
	        }
       
	        $rt[]=$count1;
	        $rt[]=$count2;
	        $rt[]=$count;
      // print_r($count1);die;
      		for ($i=0; $i <count($rt) ; $i++)
            { 
            	// print_r(($rt[$i]));die;
            	if($i==0)
            	{
            		if($data4)
            		{
            			foreach($data4 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data4[$key]);
				    }
				    foreach($data4 as $key => $value)  
				    {
				        $q[]=$value;
				    }
            			// print_r($a);die;
            			// $data['label'][]=$data4;
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $q
                			) 
            		);
            		}
            		else
            		{
            			$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Below 50%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
            		}
            	
            	 	
            	}
            	else if($i==1)
            	{
            		foreach($data5 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data5[$key]);
				    }
				    foreach($data5 as $key => $value)  
				    {
				        $y[]=$value;
				    }
                   // print_r($b);die;
                    // $data['data'][]=$rt[$i];
                    if($data5)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $y
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Upto 51%-75%"
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
            	 	
            	}
            	else
            	{
            		
            		foreach($data6 as $key => $value) 
            		{         
				        if(empty($value)) 
				        unset($data6[$key]);
				    }
				    foreach($data6 as $key => $value)  
				    {
				        $z[]=$value;
				    } 
				    if($data6)
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => $rt[$i], 
		                    "f" => $z
                			) 
            		);
                    }
                    else
                    {
                    	$responce->rows[]["c"] = array( 
                            array(
                            "v" => $rt[$i],
                            "f" => "Above 75%" 
                        ),
            			array( 
		                    "v" => 0, 
		                    "f" => null 
                			) 
            		);
                    }
                
               
            	 	
            	}
            	

            }
        echo json_encode($responce);
        }
      // $this->load->view('pie_chart',$data);
    }
    public function bar_chart_all_student_attendance()
    {
    	$adminID = $this->session->userdata('adminID');
    	$month=$this->input->post('year');
    	$result = $this->professor_m->all_student_attendance_bymonth($month,$adminID);
    	foreach($result as $row)
		 {
		   $totalattendance=$this->professor_m->totalattendancebycoursewise($month,$row["classesID"]);
		   if($totalattendance)
		   {
		   	  if($totalattendance->totalstudent != 0)
		   	  {
		   	  	  foreach ($row['student_attendance'] as $key => $value)
				  {

				  	 $output[] = array(
				   'month'   => $row["classes"],'profit' =>floatval(($value->totalpresent*100)/$totalattendance->totalstudent));
				  	
				  }
		   	  }
		   	  else
		   	  {
		   	  	foreach ($row['student_attendance'] as $key => $value)
			    {

			  	   $output[] = array(
			       'month'   => $row["classes"],'profit' =>floatval($value->totalpresent));
			  	
			    }
		   	  }
		   	  
		   }
		   else
		   {
		   	foreach ($row['student_attendance'] as $key => $value)
			  {

			  	 $output[] = array(
			   'month'   => $row["classes"],'profit' =>floatval($value->totalpresent));
			  	
			  }
		   }
		  
		     
		  
		 }
		 echo json_encode($output);
    }

}



