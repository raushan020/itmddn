<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		$this->load->model('signup_m');
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		// $this->load->model('otp_m');

		 
	}

	protected function rules()
	{

		$rules = array(


			array(
				'field' => 'businessname',
				'label' => 'Business Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'inclEmployee',
				'label' => 'Include Employee',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'country',
				'label' => 'Country',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'fname',
				'label' => 'First Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'lname',
				'label' => 'Last Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|max_length[40]|valid_email|callback_unique_email'
			),
			// array(
			// 	'field' => 'phone',
			// 	'label' => 'Phone',
			// 	'rules' => 'trim|required'
			// ),
						

		);
// print_r($rules);
// 		exit();
		return $rules;


	}

	public function index()
	{
	
   		$this->data['country'] = $this->signup_m->get_country();


		if($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				//  echo validation_errors();
				// exit();
				// $this->data["subview"] = "register";
				$this->load->view('signup');
			} else {
				
                $encrypted_password = $this->signup_m->hash($this->input->post('password'));

				$randomNumber = rand(1,9).rand(0,9).rand(0,9).rand(0,9);

				$result = substr($this->input->post("businessname"), 0, 2);

				$companyUid = $result.'-'.rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(1,9);
				 
				$array = array();
				$array["companyName"] = $this->input->post("businessname");   
				$array["companyUid"] = $companyUid; 
				$array["companyStrength"] = $this->input->post("inclEmployee");          
				$array["companyCountry"] = $this->input->post("country"); 
				$array["firstName"] = $this->input->post("fname");
				$array["lastName"] = $this->input->post("lname");
				$array["phone"] = $this->input->post("phone");
				$array["username"] = $this->input->post("email");
				$array["email"] = $this->input->post("email");
				
				$array["password"] = $encrypted_password;
				$array["usertype"] = 'company';
				$array["verificationCode"] = $randomNumber;
				$array["otpDate"] = time();
				
				// $array["verification_key"] = $verification_key;
				print_r($array);
				exit();
				$insertID = $this->signup_m->insertData($array);
				$this->send_mail_new_regitration($this->input->post("email"));
				$this->session->set_userdata('emailOtp',$this->input->post('email'));

				$this->session->set_flashdata('success', 'Conformation Mail has been Sent Succesfully please verify your email');
				  redirect(base_url("otp"));
			}
		} else {
			$this->load->view('signup');
		}		
   		
	}


public function resendotp(){
		$email	= $this->session->userdata('emailOtp');
		$this->send_mail_new_regitration($email);
       $this->otp_m->updateTime($email);

       $this->session->set_flashdata('success', 'Conformation Mail has been ReSent Succesfully please check your email');
       redirect('otp');
}



	public function send_mail_new_regitration($email){

		$this->db->select('*');
        $this->db->where('email',$email);
        $this->db->from('company');
      	$query2  = $this->db->get();

      	$dataEmail['emailData'] = $query2->row();
		$this->load->library('email');

			$config1=array(
			'charset'=>'utf-8',
			'wordwrap'=> TRUE,
			'mailtype' => 'html'
			);
			$verify = $this->load->view('emailTemplates/verify_mail', $dataEmail ,true);						
            $this->email->initialize($config1);
			$this->email->set_mailtype("html");
			$this->email->from('rahul.kumar@edgetechnosoft.com','JOBSTERZ');
			$this->email->to($email);
			$this->email->subject('e-Campus Email Verification');
			$this->email->message($verify);
		    $this->email->send();
}

	
function unique_email(){

 $this->db->where('email',$this->input->post('email'));
  $query  = $this->db->get('company')->num_rows();

if($query){
	$this->form_validation->set_message("unique_email", "%s already exists");
	return FALSE;
}else{
	return TRUE;
}
}

}
