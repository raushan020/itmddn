<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {


	public function index()
	{
		$this->load->view('web/contact');
	}

	public function mailer()
	{
		if ($_POST) {
			
			$firstname = $this->input->post('firstName');
			$email = $this->input->post('email');
			$msg = "Name -" .$firstname."\r\n";
			$msg .="E-mail  -" . $email."\r\n";
			$msg .="Message -" . $this->input->post('msg');

			$headers = "From: support@edgetechnosoft.com" . "\r\n" ."CC: anjum@edgetechnosoft.com, faiz@edgetechnosoft.com";

			mail('support@edgetechnosoft.com' ,' New E-mail from E-campus',$msg,$headers);

			$this->session->set_flashdata('success','Mail Send Sucessfully');
           	redirect("https://e-campus.in/contact");
		}
	}
}


