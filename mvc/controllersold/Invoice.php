<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


use Omnipay\Omnipay;

class Invoice extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("invoice_m");

		$this->load->model("invoice_gen_m");

		$this->load->model("feetype_m");

		$this->load->model('payment_m');

		$this->load->model("classes_m");

		$this->load->model("student_m");

		$this->load->model("parentes_m");

		$this->load->model("section_m");

		$this->load->model('user_m');

		$this->load->model("payment_settings_m");
		$this->load->model("professor_m");
		$this->load->model("setting_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('invoice', $language);

		require_once(APPPATH."libraries/Omnipay/vendor/autoload.php");

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Teacher" || $usertype == "Support" ) {
			$id  =  $this->uri->segment(3);

				if ($this->input->post('classesID_invoice')) {
					$this->session->unset_userdata('subCourseID_invoice');
					$this->session->set_userdata('classesID_invoice', $this->input->post('classesID_invoice'));
				}
				if ($this->input->post('chenge_fee')) {
					$this->session->unset_userdata('classesID_invoice');
					$this->session->unset_userdata('yos_filter_invoice');
					$this->session->unset_userdata('payment_status_invoice');
					$this->session->unset_userdata('SessionTo_invoice');
					$this->session->unset_userdata('SessionFrom_invoice');
					$this->session->unset_userdata('sessionFilter_invoice');
					$this->session->set_userdata('chenge_fee', $this->input->post('chenge_fee'));
				}
				if(empty($this->session->userdata('chenge_fee'))){
					$this->session->set_userdata('chenge_fee', 2);
				}

			    if ($this->input->post('yos_filter_invoice')) {
					$this->session->set_userdata('yos_filter_invoice', $this->input->post('yos_filter_invoice'));
				}
		         if ($this->input->post('payment_status_invoice')) {
					$this->session->set_userdata('payment_status_invoice', $this->input->post('payment_status_invoice'));
				}
			   	if ($this->input->post('subCourseID_invoice')) {
					$this->session->set_userdata('subCourseID_invoice', $this->input->post('subCourseID_invoice'));
				}
				if ($this->input->post('SessionTo_invoice') && $this->input->post('SessionFrom_invoice')) {
				    $SessionTo = $this->input->post('SessionTo_invoice');
				    $SessionFrom = $this->input->post('SessionFrom_invoice');
				    $sessionFilter = $SessionFrom.'-'.$SessionTo;
				    $this->session->set_userdata('sessionFilter_invoice', $sessionFilter);
				}




			if ($id)
			{

				if ($usertype == "superadmin") {	
					$this->data['invoices'] = $this->invoice_gen_m->get_invoice_byID($id);
				}else{
					$this->data['invoices'] = $this->invoice_gen_m->get_invoice_byID_byAdmin($id,$adminID);
				}
        	}
        	else
        	{
        		if ($usertype == "superadmin") {
	        	$this->data['invoices'] = $this->invoice_gen_m->get_invoice();
	        	}else{
	        	 $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
	        	 $this->data['invoices'] = $this->invoice_gen_m->get_invoice_byAdmin($adminID);
	        	 $this->data['total_revenue'] = $this->invoice_m->total_revenue();
	        	 $this->data['total_due'] = $this->invoice_m->total_due();
	        	 $this->data['total_paid'] = $this->invoice_m->total_paid();
	        	 // print_r($this->data['total_revenue']);die;
	        	
	        	}
        	
        	}
			$this->data["subview"] = "invoice/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Student") {

			$username = $this->session->userdata("username");

			$student = $this->student_m->get_single_student(array("username" => $username));

			$this->data['invoices'] = $this->invoice_gen_m->get_invoice_for_student($student->studentID);

			$this->data["subview"] = "invoice/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					if($checkstudent->parentID == $parent->parentID) {

						$classesID = $checkstudent->classesID;

						$this->data['set'] = $id;

						$this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('studentID' => $id));

						$this->data["subview"] = "invoice/index_parent";

						$this->load->view('_layout_main', $this->data);

					} else {

						$this->data["subview"] = "error";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "invoice/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



    function ajaxStudents()
    {
        $usertype = $this->session->userdata("usertype");
        $adminID = $this->session->userdata("adminID");
        $loginuserID = $this->session->userdata("loginuserID");
        $totalData = $this->invoice_m->get_order_by_student_by_join_Count($adminID);
        $totalFiltered = $totalData;
        $posts = $this->invoice_m->make_datatables($adminID);
        $data = array();
        if(!empty($posts))
        {
            $i = 1;
            foreach ($posts as $post)
            {
                if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant") 
                {
                    // $nestedData['sn2'] =  0;
                    $nestedData['check'] ="<label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->studentID." '/>
                                                <span class='checkmark checkmark-action-layout'></span>
                                            </label>
                                            </td>";
                    $nestedData['roll'] = $post->roll;
                    $nestedData['name'] = $post->name.' / '.$post->username;
                    $nestedData['yearsOrSemester'] = str_replace('_', ' ', $post->yearsOrSemester);
                    $nestedData['amount'] = $post->amount;
                    $nestedData['paidamount'] = $post->paidamount;
                    $nestedData['dueamount'] = $post->amount-$post->paidamount;                        
                    if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant") 
                    {
                        $buttons  =  "<center><a data-toggle='tooltip'  data-placement='top' title='Fee' href='".base_url()."invoice/view/$post->studentID?type=clg&year=".return_year($post->yearsOrSemester)."' class='btn btn-info edit for_margR'><i class='fa fa-inr'></i></a></center>";
                        $nestedData['action'] = $buttons;  	
                    }
                    $data[] = $nestedData;
                }
            }
        }    
        $json_data = array(
                        "draw"            => intval($this->input->post('draw')),  
                        "recordsTotal"    => $totalData,  
                        "recordsFiltered" => $totalFiltered, 
                        "data"            => $data   
                    );
        echo json_encode($json_data); 
    }


    public function send_mail_account() 
    {
        $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        $email = $this->session->userdata("email");
        $username = $this->session->userdata("username");
        $setting = $this->setting_m->get_setting_byAdmin($adminID);
        if($usertype == "Accountant") 
        {
        	$getadmin=$this->professor_m->getsuperadminid($adminID);
        	$institute=$setting->sname;
        	if($getadmin)
        	{
        		$superadminemail=$getadmin->email;

        	}
        	$getemail=$this->professor_m->getadminid($loginuserID);
        	$adminemail="";
        	if($getemail)
        	{
        		$adminemail=$getemail->email;
        	}
            
            $totalData = $this->invoice_m->get_order_by_student_by_join_Count($adminID);
            $totalFiltered = $totalData;
            $data['posts'] = $this->invoice_m->make_datatabless($adminID);
            $email = $this->data["siteinfos"]->email;
            $subject="Submission of Student Fee";
            $data['institute']=$institute;
	        $data['logo']=$setting->photo;
	        $data['superadminemail']=$superadminemail;
            $data['adminemail']=$adminemail;
            $fullname="Admin";
            $html = $this->load->view('emailTemplates/feeupdation/feeupdationaccountant', $data , true);
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$data);
		    if($sendmail == true)
		    {
		        echo "student fee report sent";
		    } 
		    else 
		    {
		        echo "fail";
		    }
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}


  function mail()
  {
    if($this->input->post('sendmailbyadmin'))
	{
        
		$checked_id =  array_unique($this->input->post('checked_id'));
		$subjectname=$this->input->post('subjectname');
        $writemail=$this->input->post('writemail');
        $loginuserID = $this->session->userdata("loginuserID");
        $emailsendby=$this->professor_m->getemailidsender($loginuserID);
        $adminID = $this->session->userdata("adminID");
        $setting = $this->setting_m->get_setting_byAdmin($adminID);

        $getadmin=$this->professor_m->getsuperadminid($adminID);
    	$institute=$setting->sname;
    	if($getadmin)
    	{
    		$superadminemail=$getadmin->email;

    	}
    	$getemail=$this->professor_m->getadminid($loginuserID);
    	$adminemail="";
    	if($getemail)
    	{
    		$adminemail=$getemail->email;
    	}
        // print_r($emailsendby);die;
        $dataEamil['institute']=$institute;
        $dataEamil['logo']=$setting->photo;
        $dataEamil['superadminemail']=$superadminemail;
        $dataEamil['adminemail']=$adminemail;
        $dataEamil['title'] = $subjectname;
		$dataEamil['date'] = date("d-m-Y");
		$dataEamil['description'] = $writemail;

		for ($i=0; $i <count($checked_id) ; $i++) 
		{
			$emailreciverby=$this->professor_m->getreciverstudentid($checked_id[$i]);
			$html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
			$subject=$subjectname;
            $email=$emailreciverby->email;
            $fullname=$emailreciverby->name;
            $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
		}
		if($sendmail == true)
		{
			$this->session->set_flashdata('success',"E-mail Sent successfully");
			redirect(base_url("invoice/index"));
		}
		else 
        {
            $this->session->set_flashdata('error', $this->lang->line('mail_error'));
            redirect(base_url("invoice/index"));
            
        }
	}


  }
	protected function rules() {

		$rules = array(

				array(

					'field' => 'classesID',

					'label' => $this->lang->line("invoice_classesID"),

					'rules' => 'trim|required|max_length[11]|numeric|callback_unique_classID'

				),

				array(

					'field' => 'studentID',

					'label' => $this->lang->line("invoice_studentID"),

					'rules' => 'trim|required|max_length[11]|numeric|callback_unique_studentID'

				),

				array(

					'field' => 'feetype',

					'label' => $this->lang->line("invoice_feetype"),

					'rules' => 'trim|required|max_length[128]'

				),

				array(

					'field' => 'amount',

					'label' => $this->lang->line("invoice_amount"),

					'rules' => 'trim|required|max_length[20]|numeric|callback_valid_number'

				),

				array(

					'field' => 'date',

					'label' => $this->lang->line("invoice_date"),

					'rules' => 'trim|required|max_length[10]|callback_date_valid'

				),



			);

		return $rules;

	}

	protected function payment_rules() {

		$rules = array(

				array(

					'field' => 'amount',

					'label' => $this->lang->line("invoice_amount"),

					'rules' => 'trim|required|max_length[11]|numeric|callback_valid_number'

				),

				array(

					'field' => 'payment_method',

					'label' => $this->lang->line("invoice_paymentmethod"),

					'rules' => 'trim|required|max_length[20]|callback_unique_paymentmethod'

				)

			);

		return $rules;

	}

	public function add() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Accountant") {

			$this->data['classes'] = $this->classes_m->get_classes();

			$this->data['feetypes'] = $this->feetype_m->get_feetype();

			$classesID = $this->input->post("classesID");

			if($classesID != 0) {

				$this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $classesID));

			} else {

				$this->data['students'] = "empty";

			}

			$this->data['studentID'] = 0;

			if($_POST) {

				$this->data['studentID'] = $this->input->post('studentID');

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data["subview"] = "invoice/add";

					$this->load->view('_layout_main', $this->data);

				} else {

					if($this->input->post('studentID')) {

						$classesID = $this->input->post('classesID');

						$getclasses = $this->classes_m->get_classes($classesID);

						$studentID = $this->input->post('studentID');

						$getstudent = $this->student_m->get_student($studentID);

						$amount = $this->input->post("amount");

						$array = array(

							'classesID' => $classesID,

							'classes' => $getclasses->classes,

							'studentID' => $studentID,

							'student' => $getstudent->name,

							'roll' => $getstudent->roll,

							'feetype' => $this->input->post("feetype"),

							'amount' => $amount,

							'status' => 0,

							'date' => date("Y-m-d", strtotime($this->input->post("date"))),

							'year' => date('Y')

						);

						$oldamount = $getstudent->totalamount;

						$nowamount = $oldamount+$amount;

						$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);

						$returnID = $this->invoice_m->insert_invoice($array);

						$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					 	redirect(base_url("invoice/view/$returnID"));

					} else {

						$classesID = $this->input->post('classesID');

						$getclasses = $this->classes_m->get_classes($classesID);

						$getstudents = $this->student_m->get_order_by_student(array("classesID" => $classesID));

						$amount = $this->input->post("amount");

						foreach ($getstudents as $key => $getstudent) {

							$array = array(

								'classesID' => $classesID,

								'classes' => $getclasses->classes,

								'studentID' => $getstudent->studentID,

								'student' => $getstudent->name,

								'roll' => $getstudent->roll,

								'feetype' => $this->input->post("feetype"),

								'amount' => $amount,

								'status' => 0,

								'date' => date("Y-m-d", strtotime($this->input->post("date"))),

								'year' => date('Y')

							);

							$oldamount = $getstudent->totalamount;

							$nowamount = $oldamount+$amount;

							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);

							$this->invoice_m->insert_invoice($array);

						}

						$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					 	redirect(base_url("invoice/index"));

					}

				}

			} else {

				$this->data["subview"] = "invoice/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}






	public function edit() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$this->data['invoice'] = $this->invoice_m->get_invoice($id);

				$this->data['classes'] = $this->classes_m->get_classes();

				if($this->data['invoice']) {

					if($this->data['invoice']->classesID != 0) {

						$this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $this->data['invoice']->classesID));

					} else {

						$this->data['students'] = "empty";

					}

					$this->data['studentID'] = $this->data['invoice']->studentID;



					if($_POST) {

						$this->data['studentID'] = $this->input->post('studentID');

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) {

							$this->data["subview"] = "invoice/edit";

							$this->load->view('_layout_main', $this->data);

						} else {

							$status = 0;

							$oldstudent = $this->student_m->get_student($this->data['invoice']->studentID);

							$osoldamount = $oldstudent->totalamount;

							$oldnowamount = ($osoldamount)-($this->data['invoice']->amount);

							$this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);



							$classesID = $this->input->post('classesID');

							$getclasses = $this->classes_m->get_classes($classesID);

							$studentID = $this->input->post('studentID');

							$getstudent = $this->student_m->get_student($studentID);

							$amount = $this->input->post("amount");



							if(empty($this->data['invoice']->paidamount)) {

								$status = 0;

							} elseif($this->data['invoice']->paidamount == $amount) {

								$status = 2;

							} else {

								$status = 1;

							}



							$array = array(

								'classesID' => $classesID,

								'classes' => $getclasses->classes,

								'studentID' => $studentID,

								'student' => $getstudent->name,

								'roll' => $getstudent->roll,

								'feetype' => $this->input->post("feetype"),

								'amount' => $amount,

								'status' => $status,

							);

							$oldamount = $getstudent->totalamount;

							$nowamount = $oldamount+$amount;



							$this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);

							$this->invoice_m->update_invoice($array, $id);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

						 	redirect(base_url("invoice/index"));



						}

					} else {

						$this->data["subview"] = "invoice/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$this->data['invoice'] = $this->invoice_m->get_invoice($id);

				if($this->data['invoice']) {

					$oldstudent = $this->student_m->get_student($this->data['invoice']->studentID);

					$osoldamount = $oldstudent->totalamount;

					$oldnowamount = ($osoldamount)-($this->data['invoice']->amount);

					$this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);

					$this->invoice_m->delete_invoice($id);

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					redirect(base_url('invoice/index'));

				} else {

					redirect(base_url('invoice/index'));

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	public function view() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($this->input->get('year')){
			$year = $_GET['year'];
		}else{
			$year = '1st_Year';
		}
	   if($this->input->get('type')){
              $feeType = $_GET['type'];
              }else{
              $feeType = 'clg';
              }
              if($this->input->get('month')){
              $month = $_GET['month'];
              }else{
              $month = date('m');
              }
              if($this->input->get('monthYear')){
              $monthYear = $_GET['monthYear'];
              }else{
              $monthYear = date('Y');
              }
        $this->data["setting"] = $this->setting_m->get_setting_byAdmin($adminID);
		if($usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Support") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				if($feeType=='clg'){
				$this->data["invoice"] = $this->invoice_m->get_invoice_byAdmin($id,$adminID,$year);
               }else{
               	$this->data["invoice"] = $this->invoice_m->get_invoice_hstl($id,$adminID);
               }
				if($this->data["invoice"]) {

					$this->data["student"] = $this->invoice_gen_m->get_student($this->data["invoice"]->studentID);

					$this->data["classes"] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

					$this->data["departmentData"] = $this->classes_m->get_single_invoice_department_data($this->data["classes"]->departmentID);

					$this->data["feeApplied"] = $this->invoice_gen_m->get_feeApplied($this->data["invoice"]->invoiceID);

				if($feeType=='clg'){
				$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment($this->data["invoice"]->invoiceID,$year,'clg');
				}else{
					$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment_hstl($this->data["invoice"]->invoiceID,$month,$monthYear,'hstl');
				}
					$this->data["subview"] = "invoice/view";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}	

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} elseif($usertype == "Student") {

			$id = $this->session->userdata('loginuserID');

			if((int)$id) {

				$username = $this->session->userdata("username");
				

				$adminID = $this->session->userdata("adminID");

				$getstudent = $this->student_m->get_single_student(array("username" => $username));

				$this->data["invoice"] = $this->invoice_m->get_invoice_byAdmin($id,$adminID,$year);
		
	
				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {

					$this->data["student"] = $this->invoice_gen_m->get_student($this->data["invoice"]->studentID);

					$this->data["classes"] = $this->classes_m->get_single_classes($this->data["student"]->classesID);
					$this->data["departmentData"] = $this->classes_m->get_single_invoice_department_data($this->data["classes"]->departmentID);

				      $looping    =  (int) $this->data["classes"]->duration;
                      $years_array = array();             
                      for ($i=1; $i <=$looping; $i++) {
                      	$years_array[] = CallYears($i); 
                      }
                      // if (!in_array($year, $years_array)) {
                      // redirect('dashboard');	
                      // }

					$this->data["feeApplied"] = $this->invoice_gen_m->get_feeApplied($this->data["invoice"]->invoiceID);
				if($feeType=='clg'){
				$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment($this->data["invoice"]->invoiceID,$year,'clg');
				}else{
					$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment_hstl($this->data["invoice"]->invoiceID,$month,$monthYear,'hstl');
					// print_r($this->data["paymentDetails"]);
					// exit();
				}
					$this->data["subview"] = "invoice/view";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} elseif($usertype == "Parent") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$username = $this->session->userdata("username");

				$parent = $this->student_m->get_parent_info($username);

				$this->data["invoice"] = $this->invoice_m->get_single_invoice(array('invoiceID' => $id));

				if($this->data['invoice']) {

					$getstudent = $this->student_m->get_single_student(array("studentID" => $this->data['invoice']->studentID));

					if($this->data['invoice'] && ($parent->parentID == $getstudent->parentID)) {

						$this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);

						$this->data["subview"] = "invoice/view";

						$this->load->view('_layout_main', $this->data);

					} else {

						$this->data["subview"] = "error";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	// public function updateCsv_with_inner_join(){
	// 	$this->db->query("");
	// }


	public function print_preview() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$this->data["invoice"] = $this->invoice_m->get_invoice($id);

				if($this->data["invoice"]) {

					
				    $this->data["student"] = $this->invoice_gen_m->get_student($this->data["invoice"]->studentID);

					$this->data["classes"] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

					$this->data["feeApplied"] = $this->invoice_gen_m->get_feeApplied($this->data["invoice"]->invoiceID);

					$this->load->library('html2pdf');

				    $this->html2pdf->folder('./assets/pdfs/');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');

				    $html = $this->load->view('invoice/print_preview', $this->data, true);

				$dss =	$this->html2pdf->html($html);

					$this->html2pdf->create();

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		}

	}


	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin") {

			$id = $this->input->post('id');

			if ((int)$id) {

				$this->data["invoice"] = $this->invoice_m->get_invoice($id);

				if($this->data["invoice"]) {

					$this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);

					$this->load->library('html2pdf');

				    $this->html2pdf->folder('./assets/pdfs/');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

				    $html = $this->load->view('invoice/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}



				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

public function dau(){

$this->db->select('studentID,classesID,yearID');
$daus  =  $this->db->get('student')->result();

foreach ($daus as $key => $value) {
	$this->db->select('mode,duration,classesID');
	$this->db->where('classesID',$value->classesID);
$single_Class	= $this->db->get('classes')->row();
;
$this->db->where('studentID',$value->studentID);
$invoice	= $this->db->get('invoice')->row();


$Array =  array(1=>'1st_Year',2=>'2nd_Year',3=>'3rd_Year',4=>'4th_Year',5=>'5th_Year',6=>'6th_Year',7=>'7th_Year',8=>'8th_Year',9=>'9th_Year',10=>'10th_Year');

$current_key = array_search($value->yearID,$Array);

$loop =  $single_Class->duration;

for ($i=1; $i <= $loop; $i++) { 

    if($current_key>$i){
$this->db->where('studentID',$invoice->studentID);
$this->db->where('feeType','clg');
$this->db->where('yearsOrSemester',CallYears($i));
$pay = $this->db->get('payment')->result();
$fee = 0;
foreach ($pay as $ke => $values) {
	$fee +=  $values->paymentamount;
}
if($invoice->totalfeepaidperyear==$fee){
	$fee_status = 2;
}
if($invoice->totalfeepaidperyear>$fee & $fee !=0){
	$fee_status = 1;
}
if($fee==0){
	$fee_status = 3;
}
    	$data = array(
    	'adminID'=>$invoice->adminID,
    	'studentID'=>$invoice->studentID,
    	'feetype'=>'clg',
    	'amount'=>$invoice->totalfeepaidperyear,
    	'total_amount'=>$invoice->amount,
    	'userID'=>$invoice->userID,
    	'paidamount'=>$fee,
    	'usertype'=>$invoice->usertype,
    	'uname'=>$invoice->uname,
    	'date'=>$invoice->date,
    	'year'=>$invoice->year,
    	'lf_no'=>$invoice->lf_no,
    	'status'=>$fee_status,
    	'discount'=>$invoice->discount,
    	'tuitionFee'=>$invoice->tuitionFee,
    	'totalfeepaidperyear'=>$invoice->totalfeepaidperyear,
    	'yearsOrSemester'=>CallYears($i),
    	'student_position'=>0
    	);
    $this->db->insert('invoice',$data);
    $is_s = $this->db->insert_id();
$data_sd =  array(
'invoiceID'=>$is_s
);
$this->db->where('studentID',$invoice->studentID);
$this->db->where('feeType','clg');
$this->db->where('yearsOrSemester',CallYears($i));
$this->db->update('payment',$data_sd);
    }
    if ($current_key==$i) {

$this->db->where('studentID',$invoice->studentID);
$this->db->where('feeType','clg');
$this->db->where('yearsOrSemester',CallYears($i));
$pay = $this->db->get('payment')->result();
$fee = 0;
foreach ($pay as $ke => $values) {
	$fee +=  $values->paymentamount;
}
if($invoice->totalfeepaidperyear==$fee){
	$fee_status = 2;
}
if($invoice->totalfeepaidperyear>$fee & $fee !=0){
	$fee_status = 1;
}
if($fee==0){
	$fee_status = 3;
}

    	$dataghg = array(
    	'adminID'=>$invoice->adminID,
    	'studentID'=>$invoice->studentID,
    	'feetype'=>'hstl',
    	'amount'=>$invoice->totalfeepaidperyear,
    	'total_amount'=>$invoice->amount,
    	'userID'=>$invoice->userID,
    	'usertype'=>$invoice->usertype,
    	'uname'=>$invoice->uname,
    	'date'=>$invoice->date,
    	'year'=>$invoice->year,
    	'lf_no'=>$invoice->lf_no,
    	'discount'=>$invoice->discount,
    	'tuitionFee'=>$invoice->tuitionFee,
    	'hostelFee'=>$invoice->hostelFee,
    	'totalfeepaidperyear'=>$invoice->totalfeepaidperyear,
    	'yearsOrSemester'=>CallYears($i),
    	'student_position'=>0
    	);
    $this->db->insert('invoice',$dataghg);

        $data_s = array(
    	'adminID'=>$invoice->adminID,
    	'studentID'=>$invoice->studentID,
    	'feetype'=>'clg',
    	'status'=>3,
    	'amount'=>$invoice->totalfeepaidperyear,
    	'total_amount'=>$invoice->amount,
    	'userID'=>$invoice->userID,
    	'paidamount'=>$fee,
    	'usertype'=>$invoice->usertype,
    	'uname'=>$invoice->uname,
    	'date'=>$invoice->date,
    	'year'=>$invoice->year,
    	'status'=>$fee_status,
    	'lf_no'=>$invoice->lf_no,
    	'discount'=>$invoice->discount,
    	'tuitionFee'=>$invoice->tuitionFee,
    	'totalfeepaidperyear'=>$invoice->totalfeepaidperyear,
    	'yearsOrSemester'=>CallYears($i),
    	'student_position'=>1
    	);
    $this->db->where('invoiceID',$invoice->invoiceID);   	
    $this->db->update('invoice',$data_s);

    }

}


}

}


public function PaymentDelete($id){

if ($id){
  $uri_four = $this->uri->segment(4);
  $feeType = $this->uri->segment(5);
  $month = $this->uri->segment(6);
  $monthYear = $this->uri->segment(7);

  $payment =  $this->db->where('paymentID',$id)->get('payment')->row();

$invoice = $this->db->where('invoiceID',$payment->invoiceID)->get('invoice')->row();
$total_reduce =  $invoice->paidamount-$payment->paymentamount;
if($total_reduce==0){
	$status = 3;
}
if($total_reduce!=0 & $invoice->amount>$total_reduce){
	$status = 1;
}
if($invoice->amount==$total_reduce){
	$status = 2;
}
$data = array(
"paidamount"=>$total_reduce,
"status"=>$status
);
  $this->db->where('invoiceID',$payment->invoiceID);
  $this->db->update('invoice',$data);

$this->db->where('paymentID',$id);
$this->db->delete('payment');
if ($feeType=='clg') {
  $this->session->set_flashdata('success', $this->lang->line('menu_success'));	
redirect("invoice/view/$invoice->studentID?type=clg&year=$uri_four");
}
else {
	  $this->session->set_flashdata('success', $this->lang->line('menu_success'));
redirect("invoice/view/$invoice->studentID?type=hstl&month=$month&monthYear=$monthYear");	
}
}
}
	public function payment() {

          if($this->input->get('year')){
              $year = $_GET['year'];
              }else{
              $year = '1st_Year';
              }
              if($this->input->get('type')){
              $feeType = $_GET['type'];
              }else{
              $feeType = 'clg';
              }
              if($this->input->get('month')){
              $month = $_GET['month'];
              }else{
              $month = date('m');
              }
              if($this->input->get('monthYear')){
              $monthYear = $_GET['monthYear'];
              }else{
              $monthYear = date('Y');
              }		
		    $yearsOrSemester = $year;
		     
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Support") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {
$this->data['invoice'] = $this->invoice_m->get_invoice($id);

				if($feeType=='clg'){
				$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment($this->data["invoice"]->invoiceID,$year,'clg');
				}else{
					$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment_hstl($this->data["invoice"]->invoiceID,$month,$monthYear,'hstl');
				}
			   	$this->data['feePaidPerYear'] = 0;
			   	foreach ($this->data["paymentDetails"] as $key => $value) {
			    	$this->data['feePaidPerYear'] += $value->paymentamount;
			   	}
				if($this->data['invoice']) {

					$this->data["student"] = $this->invoice_gen_m->get_student($this->data["invoice"]->studentID);

					$this->data["classes"] = $this->classes_m->get_single_classes($this->data["student"]->classesID);
if($_POST) {
if($year=='hstl'){
if(($this->data['feePaidPerYear'] == $this->data['invoice']->hostelFee)) {
redirect('student');
}
}else{
   if(($this->data['invoice']->paidamount == $this->data['invoice']->amount)) {
redirect('student');
   }
}

							$rules = $this->payment_rules();

							$this->form_validation->set_rules($rules);

							if ($this->form_validation->run() == FALSE) {

								$this->data["subview"] = "invoice/payment";

								$this->load->view('_layout_main', $this->data);

							} else {
								if($year=='hstl'){
									$status = 0;
								$payable_amount = $this->input->post('amount')+$this->data['feePaidPerYear'];

								if ($payable_amount > $this->data['invoice']->hostelFee) {

									$this->session->set_flashdata('error', 'Payment amount is much than Total Amount');

									redirect(base_url("invoice/payment/$id?year=$year"));
								}
								}else{
								$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;
								if($payable_amount==$this->data['invoice']->amount){
								$status = 2;
								}
								if($payable_amount !=0 & $payable_amount < $this->data['invoice']->amount){
                                $status = 1;
								}
						       if($payable_amount ==0){
                                $status = 3;
								}	
								if ($payable_amount > $this->data['invoice']->amount) {
									$this->session->set_flashdata('error', 'Payment amount is much than Total Amount');
									redirect(base_url("invoice/payment/$id?year=$year"));
								}

								} 

									$this->post_data = $this->input->post();

									if ($this->input->post('payment_method') == 'Paypal') {

										$get_configs = $this->payment_settings_m->get_order_by_config();

										$this->post_data['id'] = $this->uri->segment(3);

										$this->invoice_data = $this->invoice_m->get_invoice($this->post_data['id']);

										$this->Paypal();

									} elseif($this->input->post('payment_method') == 'Cash') {

										$username = $this->session->userdata('username');

										$dbuserID = 0;

										$dbusertype = '';

										$dbuname = '';

										$nowpaymenttype = '';


									    $nowpaymenttype = 'Cash';

										$array = array(

											"paidamount" =>$payable_amount,
											"paymenttype" =>$nowpaymenttype,
											"status" => $status,
											"paiddate" => date('Y-m-d'),											

										);

										$payment_array = array(

											"invoiceID" => $id,
											"adminID" => $adminID,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date("Y-m-d", strtotime($this->input->post("paymentdate"))),
											"paymentdateUpdate"=>date('Y-m-d'),
											"receipt"=>$this->input->post('receipt'),
											"uname"=>$this->session->userdata('username'),
											"usertype"=>$this->session->userdata('usertype'),
											"userID"=>$this->session->userdata('loginuserID')
										);

										if ($feeType=='hstl') {
								      	$payment_array['paymentmonth'] = $month;
										$payment_array['paymentyear'] = $monthYear;
										$payment_array['feeType'] = 'hstl';
										}else{
										$payment_array['feeType'] = 'clg';
										$payment_array['yearsOrSemester']=$yearsOrSemester;
										}

										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										if ($feeType=='clg') {
									    $this->invoice_m->update_invoice($array, $id);
										}

if($feeType=='clg') 
{
 $this->session->set_flashdata('success_invoice', 'Fee has been processed successfully');
 redirect(base_url("invoice/view/".$studentID."?type=clg&year=".$yearsOrSemester));
 
}else
 {
  $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$id."?type=hstl&month=".$month.'&monthYear='.$monthYear));
 
 }

									} elseif($this->input->post('payment_method') == 'Cheque') {

								$username = $this->session->userdata('username');
                                $nowpaymenttype = 'Cheque';		
								$array = array(

											"paidamount" => $payable_amount,

											"paymenttype" => $nowpaymenttype,

											"status" => $status,

											"paiddate" => date('Y-m-d'),

											

										);



										$payment_array = array(

											"invoiceID" => $id,
											"adminID" => $adminID,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date("Y-m-d", strtotime($this->input->post("paymentdate"))),

											"acnumber" => $this->input->post('acnumber'),
											"cnumber" => $this->input->post('cnumber'),
											"cname" => $this->input->post('cname'),
											"cbank" => $this->input->post('cbank'),
											"dd_name" => $this->input->post('ddname'),
											"dd_number" => $this->input->post('DDnumber'),
											"dd_date" => date("Y-m-d", strtotime($this->input->post('dddate'))),

											"paymentdateUpdate"=>date('Y-m-d'),
											"receipt"=>$this->input->post('receipt'),
											"uname"=>$this->session->userdata('username'),
											"usertype"=>$this->session->userdata('usertype'),
											"userID"=>$this->session->userdata('loginuserID')

										);

								     if ($feeType=='hstl') {
								      	$payment_array['paymentmonth'] = $month;
										$payment_array['paymentyear'] = $monthYear;
										$payment_array['feeType'] = 'hstl';
										}else{
										$payment_array['feeType'] = 'clg';
										$payment_array['yearsOrSemester']=$yearsOrSemester;
										}

										$this->payment_m->insert_payment($payment_array);



										$studentID = $this->data['invoice']->studentID;

						
		                          if ($feeType=='clg') {
									    $this->invoice_m->update_invoice($array, $id);
										}
if($feeType=='clg') 
{
 $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$studentID."?type=clg&year=".$yearsOrSemester));
 
}else
 {
  $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$id."?type=hstl&month=".$month.'&monthYear='.$monthYear));
 
 }

									}


									elseif ($this->input->post('payment_method') == 'Draft') {

										$username = $this->session->userdata('username');

										$nowpaymenttype = 'Draft';
										
										$array = array(

											"paidamount" => $payable_amount,

											"paymenttype" => $nowpaymenttype,

											"status" => $status,

											"paiddate" => date('Y-m-d'),

											

										);

										$payment_array = array(

											"invoiceID" => $id,
											"adminID" => $adminID,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date("Y-m-d", strtotime($this->input->post("paymentdate"))),
											
											"dd_name" => $this->input->post('ddname'),
											"dd_number" => $this->input->post('DDnumber'),
											"dd_date" => date("Y-m-d", strtotime($this->input->post('dddate'))),
											"paymentdateUpdate"=>date('Y-m-d'),
											"receipt"=>$this->input->post('receipt'),
											"uname"=>$this->session->userdata('username'),
											"usertype"=>$this->session->userdata('usertype'),
											"userID"=>$this->session->userdata('loginuserID')


										);

										if ($feeType=='hstl') {
								      	$payment_array['paymentmonth'] = $month;
										$payment_array['paymentyear'] = $monthYear;
										$payment_array['feeType'] = 'hstl';
										}else{
										$payment_array['feeType'] = 'clg';
										$payment_array['yearsOrSemester']=$yearsOrSemester;
										}



										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										if ($feeType=='clg') {
									    $this->invoice_m->update_invoice($array, $id);
										}
if($feeType=='clg') 
{
 $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$studentID."?type=clg&year=".$yearsOrSemester));
 
}else
 {
  $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$id."?type=hstl&month=".$month.'&monthYear='.$monthYear));
 
 }			
									}

									elseif($this->input->post('payment_method') == 'Bank') {
										$username = $this->session->userdata('username');
									    $nowpaymenttype = 'Cheque';
								$array = array(
											"paidamount" => $payable_amount,
											"paymenttype" => $nowpaymenttype,
											"status" => $status,
											"paiddate" => date('Y-m-d'),
										);
										$payment_array = array(
											"invoiceID" => $id,
											"adminID" => $adminID,
											"studentID"	=> $this->data['invoice']->studentID,
											"paymentamount" => $this->input->post('amount'),
											"paymenttype" => $this->input->post('payment_method'),
											"paymentdate" => date("Y-m-d", strtotime($this->input->post("paymentdate"))),
											"cbank" => $this->input->post('cbank'),
											"payee_name" => $this->input->post('payname'),
											"beneficiary_number" => $this->input->post('ben_accNumber'),
											"ref_number" => $this->input->post('refNumber'),
			                                "paymentdateUpdate"=>date('Y-m-d'),
											"uname"=>$this->session->userdata('username'),
											"usertype"=>$this->session->userdata('usertype'),
											"userID"=>$this->session->userdata('loginuserID')
										);

													if ($feeType=='hstl') {
								      	$payment_array['paymentmonth'] = $month;
										$payment_array['paymentyear'] = $monthYear;
										$payment_array['feeType'] = 'hstl';
										}else{
										$payment_array['feeType'] = 'clg';
										$payment_array['yearsOrSemester']=$yearsOrSemester;
										}

										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										if ($feeType=='clg') {
									    $this->invoice_m->update_invoice($array, $id);
										}
if($feeType=='clg') 
{
 $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$studentID."?type=clg&year=".$yearsOrSemester));
 
}else
 {

  $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$id."?type=hstl&month=".$month.'&monthYear='.$monthYear));
 
 }
									}
									elseif($this->input->post('payment_method') == 'onlinePayment') {

								$nowpaymenttype = 'onlinePayment';
								$username = $this->session->userdata('username');
								
								$array = array(

											"paidamount" => $payable_amount,

											"paymenttype" => $nowpaymenttype,

											"status" => $status,

											"paiddate" => date('Y-m-d'),

											

										);

										$payment_array = array(

											"invoiceID" => $id,

											"adminID" => $adminID,

											"studentID"	=> $this->data['invoice']->studentID,

											"paymentamount" => $this->input->post('amount'),

											"paymenttype" => $this->input->post('payment_method'),

											"paymentdate" => date("Y-m-d", strtotime($this->input->post("paymentdate"))),

											"cbank" => $this->input->post('cbank'),
											"payee_name" => $this->input->post('payname'),
											"beneficiary_number" => $this->input->post('ben_accNumber'),
											"ref_number" => $this->input->post('refNumber'),
											"paymentdateUpdate"=>date('Y-m-d'),
											"receipt"=>$this->input->post('receipt'),
											"uname"=>$this->session->userdata('username'),
											"usertype"=>$this->session->userdata('usertype'),
											"userID"=>$this->session->userdata('loginuserID')
										);

					if ($feeType=='hstl') {
								      	$payment_array['paymentmonth'] = $month;
										$payment_array['paymentyear'] = $monthYear;
										$payment_array['feeType'] = 'hstl';
										}else{
										$payment_array['feeType'] = 'clg';
										$payment_array['yearsOrSemester']=$yearsOrSemester;
										}



										$this->payment_m->insert_payment($payment_array);

										$studentID = $this->data['invoice']->studentID;
										if ($feeType=='clg') {
									    $this->invoice_m->update_invoice($array, $id);
										}


if($feeType=='clg') 
{
 $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$studentID."?type=clg&year=".$yearsOrSemester));
 
}else
 {
  $this->session->set_flashdata('success', $this->lang->line('menu_success'));
 redirect(base_url("invoice/view/".$id."?type=hstl&month=".$month.'&monthYear='.$monthYear));
 
 }

									}
									
								
									 else {

										$this->data["subview"] = "invoice/payment";

										$this->load->view('_layout_main', $this->data);

									}

								

							}

						} else {

							$this->data["subview"] = "invoice/payment";

							$this->load->view('_layout_main', $this->data);

						}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} elseif($usertype == "Student") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$this->data['invoice'] = $this->invoice_m->get_invoice($id);
					$this->data["student"] = $this->invoice_gen_m->get_student($this->data["invoice"]->studentID);
					$this->data["classes"] = $this->classes_m->get_single_classes($this->data["student"]->classesID);
				$username = $this->session->userdata("username");
				$getstudent = $this->student_m->get_single_student(array("username" => $username));
				$this->data["invoice"] = $this->invoice_m->get_invoice($id);

				if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {

			if($feeType=='clg'){
				$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment($this->data["invoice"]->invoiceID,$year,'clg');
				}else{
					$this->data["paymentDetails"] = $this->invoice_gen_m->get_payment_hstl($this->data["invoice"]->invoiceID,$month,$monthYear,'hstl');
				}

   $this->data['feePaidPerYear'] = 0;
   foreach ($this->data["paymentDetails"] as $key => $value) {
    $this->data['feePaidPerYear'] += $value->paymentamount;
   }
						if($_POST) {
							$rules = $this->payment_rules();
							unset($rules[1]);

							$this->form_validation->set_rules($rules);

							if ($this->form_validation->run() == FALSE) {

								$this->data["subview"] = "invoice/payment";

								$this->load->view('_layout_main', $this->data);

							} else {

							  if($year=='hstl'){
								$payable_amount = $this->input->post('amount')+$this->data['feePaidPerYear'];

								if ($payable_amount > $this->data['invoice']->hostelFee) {

									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');

									redirect(base_url("invoice/payment/$id?year=$year"));
								}

								}else{
								$payable_amount = $this->input->post('amount')+$this->data['feePaidPerYear'];

								if ($payable_amount > $this->data['invoice']->totalfeepaidperyear) {

									$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');

									redirect(base_url("invoice/payment/$id?year=$year"));
								}									
								}

									$this->post_data = $this->input->post();

									$this->post_data['id'] = $id;

									$this->post_data['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

									$this->post_data['hash'] = "";

									$this->post_data['firstname'] = $getstudent->name;

									$this->post_data['email'] = $getstudent->email;

									$this->post_data['phone'] = $getstudent->phone;

									$this->post_data['productinfo'] = "testing";

									$this->post_data['surl'] = base_url().'invoice/surl';

									$this->post_data['furl'] =base_url().'invoice/furl';

									$this->post_data['service_provider'] ="payu_paisa";

									
									$this->invoice_data = $this->invoice_m->get_invoice($id);

 	                                 // $this->instamojo();
                                    	$this->session->set_userdata('feeType',$feeType);
                                    	$this->session->set_userdata('month',$month);
                                    	$this->session->set_userdata('monthYear',$monthYear);



									$this->PayUmoney($yearsOrSemester);

									// $this->Paypal();

								

							}

						} else {

							$this->data["subview"] = "invoice/payment";

							$this->load->view('_layout_main', $this->data);

						}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} elseif($usertype == "Parent") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$this->data['invoice'] = $this->invoice_m->get_invoice($id);

				$username = $this->session->userdata("username");

				$this->data["invoice"] = $this->invoice_m->get_invoice($id);



				if($this->data["invoice"]) {

					$getstudent = $this->student_m->get_single_student(array("studentID" => $this->data['invoice']->studentID));

					if($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {

						if(($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1)) {

							if($_POST) {

								$rules = $this->payment_rules();

								unset($rules[1]);

								$this->form_validation->set_rules($rules);

								if ($this->form_validation->run() == FALSE) {

									$this->data["subview"] = "invoice/payment";

									$this->load->view('_layout_main', $this->data);

								} else {

									$payable_amount = $this->input->post('amount')+$this->data['invoice']->paidamount;

									if ($payable_amount > $this->data['invoice']->amount) {

										$this->session->set_flashdata('error', 'Payment amount is much than invoice amount');

										redirect(base_url("invoice/payment/$id"));

									} else {

										$this->post_data = $this->input->post();

										$this->post_data['id'] = $id;

										$this->invoice_data = $this->invoice_m->get_invoice($id);

										$this->Paypal();

									}

								}

							} else {

								$this->data["subview"] = "invoice/payment";

								$this->load->view('_layout_main', $this->data);

							}

						} else {

							$this->data["subview"] = "error";

							$this->load->view('_layout_main', $this->data);

						}

					} else {

						$this->data["subview"] = "error";

						$this->load->view('_layout_main', $this->data);

					}



				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



//instamojo

 
public function instamojo(){


include_once APPPATH."libraries/Instamojo.php";

// for live

// $api = new Instamojo\Instamojo('02a581144462d1103fb2745e24f02406','eff334292abcd7bb96bcfc0c7acceadd','https://www.instamojo.com/api/1.1/');

$api = new Instamojo\Instamojo('d82016f839e13cd0a79afc0ef5b288b3','3827881f669c11e8dad8a023fd1108c2','https://test.instamojo.com/api/1.1/');


try {
    $response = $api->paymentRequestCreate(array(
    'purpose' =>"ds",
    'amount' => $this->post_data['amount'],
    'phone' => $this->post_data['phone'],
    'buyer_name' => $this->post_data['firstname'],
    'redirect_url' => base_url().'invoice/RedirectIntamojo/',
    'send_email' => true,
    'webhook' => '',
    'send_sms' => true,
    'email' =>$this->post_data['email'],
    'allow_repeated_payments' => false,
    'shipping_zip'=>'89899',
    'shipping_country'=>'india',
/*    'custom_fields'=>$this->input->post('uid'),
    'uid'=>$this->input->post('uid'),
    'qcid'=>$this->input->post('qcid'),
    'gid'=>$this->input->post('gid'),*/
 ));
    $longurl   = $response['longurl'];

    // custom fields for save Data

   // $_SESSION["custom_fields"]  =  array(
   //  'uid'=>$this->input->post('uid'),
   //  'qcid'=>$this->input->post('qcid'),
   //  'gid'=>$this->input->post('gid')
   //  );   

    // custom fields for save Data

    redirect($longurl); 

}
catch (Exception $e) {
    print('Error: ' . $e->getMessage());
}

 }
 

public function addReceipt(){
						$paymentId  = $_POST['paymentID'];
						$invoice  = $_POST['invoiceID'];
						$year  = $_POST['year'];
						$feeType  = $_POST['feeType'];
						$month  = $_POST['month'];
						$monthYear  = $_POST['monthYear'];						

$studentID = $this->db->select('studentID')->where('invoiceID',$invoice)->get('invoice')->row()->studentID;

$config = array(
'upload_path' => "./uploads/receipt",
'allowed_types' => "gif|jpg|png|jpeg|pdf",
'overwrite' => TRUE,
'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
'max_height' => "768",
'max_width' => "1024"
);

                $this->load->library('upload', $config);
                 $this->upload->do_upload();
                $upload_data =  $this->upload->data();


$data =array(
'receipt_src'=>$upload_data['file_name'],
);

$this->db->where('paymentID',$paymentId);
$this->db->update('payment',$data);
$this->session->set_flashdata('success', 'Image has been uploaded successful !');
if ($feeType=='clg') {
redirect("invoice/view/$studentID?type=clg&year=$year");
}else{
redirect("invoice/view/$studentID?type=hstl&month=$month&monthYear=$monthYear");
}

}



// pay u money start here

public function RedirectIntamojo(){
	echo "sucess and thanku for a payment";
}

public function PayUmoney($yearsOrSemester){
 	

$MERCHANT_KEY = "864A8gX3";
$SALT = "8s6Z69Xvxm";
$this->post_data['key'] = $MERCHANT_KEY;

// Merchant Key and Salt as provided by Payu.
$PAYU_BASE_URL = "https://sandboxsecure.payu.in";		// For Sandbox Mode
// $PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode

$action = '';

$posted = array();
if(!empty($this->post_data)) {
    // print_r($this->post_data);
  foreach($this->post_data as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
   
     $this->session->set_userdata('params',$this->post_data['id']); 
     $this->session->set_userdata('yearsOrSemester',$yearsOrSemester); 

        $udf1 = '';
        $udf2 = '';
        $udf3 = '';
        $udf4 = '';
        $udf5 = '';
        $hashstring = $MERCHANT_KEY . '|' . $posted['txnid'] . '|' . $posted['amount'] . '|' . $posted['productinfo'] . '|' . $posted['firstname'] . '|' . $posted['email'] . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;

    $hash = strtolower(hash('sha512', $hashstring));

    $action = $PAYU_BASE_URL . '/_payment';
    $this->post_data['action'] = $action;
    $this->post_data['hash'] = $hash; 
   
    $this->data    = array_merge($this->data,$this->post_data);
    $this->data["subview"] = "invoice/confirmation";

    $this->load->view('_layout_main', $this->data); 
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}


}


function surl(){

				$usertype = $this->session->userdata("usertype");
                $adminID = $this->session->userdata('adminID');
				$username = $this->session->userdata("username");

				
				$params = $this->session->userdata('params');
				$this->data['invoice'] = $this->invoice_m->get_invoice($params);

				if ($_POST["amount"]==$this->data['invoice']->amount) {
					$statusInvoce = 2;
				}else{
					$statusInvoce = 1;
				}

$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="8s6Z69Xvxm";

// Salt should be same Post Request 

$array = array(

					"paidamount" => $_POST["amount"]+$this->data['invoice']->paidamount,

					"paymenttype" => "PayuMoney",

					"paiddate" => date('Y-m-d'),

					'uname' => $username

				);
   $feeType  = $this->session->userdata('feeType');
                             $month =      	$this->session->userdata('month');
                              $monthYear     = 	$this->session->userdata('monthYear');
		$yearsOrSemester = $this->session->userdata('yearsOrSemester');

				$payment_array = array(

					"invoiceID" => $params,

					"adminID" => $adminID,

					"studentID"	=> $this->data['invoice']->studentID,

					"paymentamount" => $_POST["amount"],

					"paymenttype" => 'PayUmoney',

					"paymentdate" => date('Y-m-d'),
					 "paymentdateUpdate"=>date('Y-m-d'),
					"uname"=>$this->session->userdata('username'),
					"usertype"=>$this->session->userdata('usertype'),
				    "userID"=>$this->session->userdata('loginuserID')


				);

										if ($feeType=='hstl') {
								      	$payment_array['paymentmonth'] = $month;
										$payment_array['paymentyear'] = $monthYear;
										$payment_array['feeType'] = 'hstl';
										}else{
										$payment_array['feeType'] = 'clg';
										$payment_array['yearsOrSemester']=$yearsOrSemester;
										}

				$this->payment_m->insert_payment($payment_array);
if ($feeType=='clg') {
       		 $insertInvoiceID	= $this->invoice_m->update_invoice($array, $params);
 
}
 			$this->sendEamiltoStudent($params);

 			// $this->sendEamiltoAdmin();
               

If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
  } else {
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
         }

 $hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
	$this->session->set_flashdata('error', 'Payer id not found!');
}else {
  	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
  			
  }	
if($feeType=='clg') 
{
 redirect(base_url("invoice/view/".$params."?type=clg&year=".$yearsOrSemester));
}else
 {
 redirect(base_url("invoice/view/".$params."?type=hstl&month=".$month.'&monthYear='.$monthYear));
 }

}

function furl()
{
	echo "falure";
}



	/* Paypal payment start*/

	public function Paypal() {

		$api_config = array();

		$get_configs = $this->payment_settings_m->get_order_by_config();

		foreach ($get_configs as $key => $get_key) {

			$api_config[$get_key->config_key] = $get_key->value;

		}

		$this->data['set_key'] = $api_config;

		if($api_config['paypal_api_username'] =="" || $api_config['paypal_api_password'] =="" || $api_config['paypal_api_signature']==""){

			$this->session->set_flashdata('error', 'Paypal settings not available');

			redirect($_SERVER['HTTP_REFERER']);

		} else {

			$this->item_data = $this->post_data;

			$this->invoice_info = (array) $this->invoice_data;



			$params = array(

	  		'cancelUrl' 	=> base_url('invoice/getSuccessPayment'),

	  		'returnUrl' 	=> base_url('invoice/getSuccessPayment'),

	  		'invoice_id'	=> $this->item_data['id'],

	    	'name'		=> $this->invoice_info['student'],

	    	'description' 	=> $this->invoice_info['feetype'],

	    	'amount' 	=> number_format(floatval($this->item_data['amount']),2),

	    	'currency' 	=> $this->data["siteinfos"]->currency_code,

			);

			$this->session->set_userdata("params", $params);

			$gateway = Omnipay::create('PayPal_Express');

			$gateway->setUsername($api_config['paypal_api_username']);

			$gateway->setPassword($api_config['paypal_api_password']);

			$gateway->setSignature($api_config['paypal_api_signature']);



			$gateway->setTestMode($api_config['paypal_demo']);



			$response = $gateway->purchase($params)->send();



			if ($response->isSuccessful()) {

				// payment was successful: update database

			} elseif ($response->isRedirect()) {

				$response->redirect();

			} else {

			  // payment failed: display message to customer

			  echo $response->getMessage();

			}

		}

		/*omnipay Paypal end*/

	}



	public function getSuccessPayment() {

  		$userID = $this->userID();

  		$api_config = array();

		$get_configs = $this->payment_settings_m->get_order_by_config();

		foreach ($get_configs as $key => $get_key) {

			$api_config[$get_key->config_key] = $get_key->value;

		}

		$this->data['set_key'] = $api_config;

   		$gateway = Omnipay::create('PayPal_Express');

		$gateway->setUsername($api_config['paypal_api_username']);

		$gateway->setPassword($api_config['paypal_api_password']);

		$gateway->setSignature($api_config['paypal_api_signature']);

		$gateway->setTestMode($api_config['paypal_demo']);

		$params = $this->session->userdata('params');

  		$response = $gateway->completePurchase($params)->send();

  		$paypalResponse = $response->getData(); // this is the raw response object

  		$purchaseId = $_GET['PayerID'];

  		$this->data['invoice'] = $this->invoice_m->get_invoice($params['invoice_id']);

  		$recent_paidamount = $params['amount']+$this->data['invoice']->paidamount;

  		if(isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {

  			// Response

  			if ($purchaseId) {



				$status = 0;

				if($recent_paidamount == $this->data['invoice']->amount) {

					$status = 2;

				} else {

					$status = 1;

				}

				$usertype = $this->session->userdata("usertype");

				$username = $this->session->userdata('username');

				$dbuserID = 0;

				$dbusertype = '';

				$dbuname = '';

				if($usertype == "Admin") {

					$user = $this->user_m->get_username_row("systemadmin", array("username" => $username));

					$dbuserID = $user->systemadminID;

					$dbusertype = $user->usertype;

					$dbuname = $user->name;

				} elseif($usertype == "Accountant") {

					$user = $this->user_m->get_username_row("user", array("username" => $username));

					$dbuserID = $user->userID;

					$dbusertype = $user->usertype;

					$dbuname = $user->name;

				} elseif($usertype == "Student") {

					$user = $this->user_m->get_username_row("student", array("username" => $username));

					$dbuserID = $user->studentID;

					$dbusertype = $user->usertype;

					$dbuname = $user->name;

				} elseif($usertype == "Parent") {

					$user = $this->user_m->get_username_row("parent", array("username" => $username));

					$dbuserID = $user->parentID;

					$dbusertype = $user->usertype;

					$dbuname = $user->name;

				}



				$nowpaymenttype = '';

				if(empty($this->data['invoice']->paymenttype)) {

					$nowpaymenttype = 'Paypal';

				} else {

					if($this->data['invoice']->paymenttype == 'Paypal') {

						$nowpaymenttype = 'Paypal';

					} else {

						$exp = explode(',', $this->data['invoice']->paymenttype);

						if(!in_array('Paypal', $exp)) {

							$nowpaymenttype =  $this->data['invoice']->paymenttype.','.'Paypal';

						} else {

							$nowpaymenttype =  $this->data['invoice']->paymenttype;

						}

					}

				}



				$array = array(

					"paidamount" => $recent_paidamount,

					"status" => $status,

					"paymenttype" => $nowpaymenttype,

					"paiddate" => date('Y-m-d'),

					"userID" => $dbuserID,

					"usertype" => $dbusertype,

					

				);



				$payment_array = array(

					"invoiceID" => $params['invoice_id'],

					"studentID"	=> $this->data['invoice']->studentID,

					"paymentamount" => $params['amount'],

					"paymenttype" => 'Paypal',

					"paymentdate" => date('Y-m-d'),

					"paymentmonth" => date('M'),

					"paymentyear" => date('Y')

				);

				$this->payment_m->insert_payment($payment_array);

				$studentID = $this->data['invoice']->studentID;

				$getstudent = $this->student_m->get_student($studentID);

				$nowamount = ($getstudent->paidamount)+($params['amount']);

				$this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

				$this->invoice_m->update_invoice($array, $params['invoice_id']);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

  			} else {

  				$this->session->set_flashdata('error', 'Payer id not found!');

  			}

  			redirect(base_url("invoice/view/".$params['invoice_id']));

  		} else {


     		//Failed transaction

      		$this->session->set_flashdata('error', 'Payment not success!');

  			redirect(base_url("invoice/view/".$params['invoice_id']));

  		}
  	}

	/* Paypal payment end*/


	function call_all_student() {

		$classesID = $this->input->post('id');

		if((int)$classesID) {

			echo "<option value='". 0 ."'>". $this->lang->line('invoice_select_student') ."</option>";

			$students = $this->student_m->get_order_by_student(array('classesID' => $classesID));

			foreach ($students as $key => $student) {

				echo "<option value='". $student->studentID ."'>". $student->name ."</option>";

			}

		} else {

			echo "<option value='". 0 ."'>". $this->lang->line('invoice_select_student') ."</option>";

		}

	}

	function feetypecall() {

		$feetype = $this->input->post('feetype');

		if($feetype) {

			$allfeetypes = $this->feetype_m->allfeetype($feetype);

			foreach ($allfeetypes as $allfeetype) {

				echo "<li id='". $allfeetype->feetypeID ."'>".$allfeetype->feetype."</li>";

			}

		}

	}


	function date_valid($date) {

		if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);

	        $dd = $arr[0];

	        $mm = $arr[1];

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    }

	}


	function unique_classID() {

		if($this->input->post('classesID') == 0) {

			$this->form_validation->set_message("unique_classID", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}


	function valid_number() {

		if($this->input->post('amount') && $this->input->post('amount') < 0) {

			$this->form_validation->set_message("valid_number", "%s is invalid number");

			return FALSE;

		}

		return TRUE;

	}

	function unique_paymentmethod() {

		if($this->input->post('payment_method') === '0') {

			$this->form_validation->set_message("unique_paymentmethod", "The %s field is required");

	     	return FALSE;

		} elseif($this->input->post('payment_method') === 'Paypal') {

			$api_config = array();

			$get_configs = $this->payment_settings_m->get_order_by_config();

			foreach ($get_configs as $key => $get_key) {

				$api_config[$get_key->config_key] = $get_key->value;

			}

			if($api_config['paypal_api_username'] =="" || $api_config['paypal_api_password'] =="" || $api_config['paypal_api_signature']==""){

				$this->form_validation->set_message("unique_paymentmethod", "Paypal settings required");

				return FALSE;

			}

		}

		return TRUE;

	}



	public function student_list() {

		$studentID = $this->input->post('id');

		if((int)$studentID) {

			$string = base_url("invoice/index/$studentID");

			echo $string;

		} else {

			redirect(base_url("invoice/index"));

		}

	}


public function sendEamiltoStudent($insertInvoiceID){

				$adminID = $this->session->userdata("adminID");
        		$setting = $this->setting_m->get_setting_byAdmin($adminID);
				$username = $this->session->userdata("username");

				$getstudent = $this->student_m->get_single_student(array("username" => $username));

				$this->data["invoice"] = $this->invoice_m->get_invoice_for_students($insertInvoiceID,$this->session->userdata("loginuserID"));
			

				if($this->data['invoice']) {

					$getadmin=$this->professor_m->getsuperadminid($adminID);
		        	$institute=$setting->sname;
		        	if($getadmin)
		        	{
		        		$superadminemail=$getadmin->email;

		        	}
		        	$getemail=$this->professor_m->getadminid($loginuserID);
		        	$adminemail="";
		        	if($getemail)
		        	{
		        		$adminemail=$getemail->email;
		        	}

					$data["student"] = $this->invoice_gen_m->get_student($this->data["invoice"]->studentID);

					$data["classes"] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

					$data["feeApplied"] = $this->invoice_gen_m->get_feeApplied($this->data["invoice"]->invoiceID);

					$data['institute']=$institute;
			        $data['logo']=$setting->photo;
			        $data['superadminemail']=$superadminemail;
		            $data['adminemail']=$adminemail;

					$html =  $this->load->view('emailTemplates/invoceSendToStudent', $data, true);

			        $subject="your Invoice Have Been Generated";
				    $email="sudhakar@edgetechnosoft.com";
				    $fullname="E-Campus";
				    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$data);
				    if($sendmail == true)
				    {
				        $this->session->set_flashdata('success', $this->lang->line('mail_success'));
				    } 
				    else 
				    {
				        $this->session->set_flashdata('error', $this->lang->line('mail_error'));
				    }
				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

}

	public function userID() {

		$usertype = $this->session->userdata('usertype');

		$username = $this->session->userdata('username');

		if ($usertype=="Admin") {

			$table = "systemadmin";

			$tableID = "systemadminID";

		} elseif($usertype=="Accountant" || $usertype=="Librarian") {

			$table = "user";

			$tableID = "userID";

		} else {

			$table = strtolower($usertype);

			$tableID = $table."ID";

		}

		$query = $this->db->get_where($table, array('username' => $username));

		$userID = $query->row()->$tableID;

		return $userID;

	}

function ResetSesession(){ 

	$this->session->unset_userdata('sessionFilter_invoice');
	   
}

function ResetCourses(){

	$this->session->unset_userdata('classesID_invoice');
	$this->session->unset_userdata('subCourseID_invoice');
	   
}

function ResetSubcourses(){

	$this->session->unset_userdata('subCourseID_invoice');

}


function Resetpaymentstatus(){

	$this->session->unset_userdata('payment_status_invoice');

}


function Resetyos(){
	$this->session->unset_userdata('yos_filter_invoice');
}

  

function ResetAllfilter(){
	$this->session->unset_userdata('student_position');
	$this->session->unset_userdata('yos_filter_invoice');
	$this->session->unset_userdata('sessionFilter_invoice');
	$this->session->unset_userdata('classesID_invoice');
	$this->session->unset_userdata('subCourseID_invoice');
	$this->session->unset_userdata('payment_status_invoice');

   
}






}



