<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SecureServer extends CI_Controller {

	function __construct () {
		parent::__construct();
		$this->load->helper("form");
		$this->load->library("email");
		$this->load->library("form_validation");
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model("student_m");
		$this->load->model("signin_m");
		$this->load->model("site_m");
			$this->load->model("reset_m");
	}

function updateUsernameAndPassword(){

		$this->load->database();
	

		$this->data['form_validation'] = "No";
		$this->data['siteinfos'] = $this->reset_m->get_admin();
 
 $seuireData = array(
        'secureServerLableOne'  => $this->input->post('secureServerLableOne'),//password
        'secureServerLableTwo'  => $this->input->post('secureServerLableTwo'), // username
        'secureServerLableThree'  => $this->input->post('secureServerLableThree'),//studentID
        'secureServerLableFour'  => $this->input->post('secureServerLableFour'), // usertype       
);

$this->session->set_userdata($seuireData);

if ($this->session->userdata('secureServerLableTwo')){
	 $this->data["subview"] = "secureServer/confirmPassword";
	$this->load->view('_layout_reset', $this->data);

}else{
		$this->data["subview"] = "error";
		$this->load->view('_layout_reset', $this->data);
}

}
	protected function rules() {

		$rules = array(
			
				 array(

					'field' => 'username',

					'label' => "Username",

					'rules' => 'trim|required|max_length[40]|xss_clean'

				),

				array(

					'field' => 'password',

					'label' => "Password",

					'rules' => 'trim|required|max_length[40]|xss_clean'

				)

			);

		return $rules;

	}
function changePassword(){

// update password

			$rules = $this->rules();

			$this->form_validation->set_rules($rules);

if ($this->form_validation->run() == FALSE) {
				$this->data['form_validation'] = validation_errors();
				$this->data["subview"] = "secureServer/confirmPasswor";
				$this->load->view('_layout_reset', $this->data);


}else{

$data =  array(
"password"=>$this->student_m->hash($this->input->post('password'))
);
$this->db->where('password',$this->input->post('secureServerLableOne'));
$this->db->where('username',$this->input->post('secureServerLableTwo'));
$this->db->where('usertype',$this->input->post('secureServerLableFour'));
if ($this->input->post('secureServerLableFour')=='Student') {
$this->db->where('studentID',$this->input->post('secureServerLableThree'));
$query =  $this->db->update('student',$data);
}elseif($this->input->post('secureServerLableFour')=='Teacher'){
$this->db->where('teacherID',$this->input->post('secureServerLableThree'));	
$query =  $this->db->update('teacher',$data);
}elseif($this->input->post('secureServerLableFour')=='Professor'){
 $this->db->where('professorID',$this->input->post('secureServerLableThree'));
 $query =  $this->db->update('professor',$data);
}
if ($query){
		if($this->signin_m->signin() == TRUE) {

					redirect(base_url('dashboard/index'));

				} else {

					$this->session->set_flashdata("errors", "That user does not signin");
					$this->data['siteinfos'] =  $this->site_m->get_site(array('adminID'=>$this->session->userdata("adminID")));
					$this->data['form_validation'] = "Incorrect Signin";

					$this->data["subview"] = "secureServer/confirmPassword";

					$this->load->view('_layout_reset', $this->data);

				}


}else{

				    $this->session->set_flashdata("errors", "That user does not signin");
				   

				    $this->data['siteinfos'] =  $this->site_m->get_site(array('adminID'=>$this->session->userdata("adminID")));
					$this->data['form_validation'] = "Incorrect Signin";

					$this->data["subview"] = "secureServer/confirmPassword";

					$this->load->view('_layout_reset', $this->data);

}

}





} 




}
