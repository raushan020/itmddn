<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Signin extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("signin_m");
		$this->load->model("setting_m");
        if($this->data["siteinfos"])
        {
        	$data = array(

			"lang" => $this->data["siteinfos"]->language,

		    );
		    $this->session->set_userdata($data);

        }
		

		
		$language = $this->session->userdata('lang');

		$this->lang->load('signin', $language);

	}



	protected function rules() {

		$rules = array(

				 array(

					'field' => 'username',

					'label' => "Username",

					'rules' => 'trim|required|max_length[40]'

				),

				array(

					'field' => 'password',

					'label' => "Password",

					'rules' => 'trim|required|max_length[40]'

				)

			);

		return $rules;

	}



	protected function rules_cpassword() {

		$rules = array(

				array(

					'field' => 'old_password',

					'label' => $this->lang->line('old_password'),

					'rules' => 'trim|required|max_length[40]|min_length[3]|callback_old_password_unique'

				),

				array(

					'field' => 'new_password',

					'label' => $this->lang->line('new_password'),

					'rules' => 'trim|required|max_length[40]|min_length[4]'

				),

				array(

					'field' => 're_password',

					'label' => $this->lang->line('re_password'),

					'rules' => 'trim|required|max_length[40]|min_length[4]|matches[new_password]'

				)

			);

		return $rules;

	}



	public function index() {

		$this->signin_m->loggedin() == FALSE || redirect(base_url('dashboard/index'));

		$this->data['form_validation'] = 'No';

		if($_POST) {
			

			$rules = $this->rules();
			
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE) {

				$this->data['form_validation'] = validation_errors();

				$this->data["subview"] = "signin/index";

				$this->load->view('_layout_signin', $this->data);

			} else {

				if($this->signin_m->signin() == TRUE) {
				 
             if (!empty($this->input->get('redirect'))) {
              redirect(base_url($this->input->get('redirect')));
              }else{
					redirect(base_url('dashboard/index'));
              }

				} else {

					$this->session->set_flashdata("errors", "That user does not signin");

					$this->data['form_validation'] = "Your username or password is incorrect";

					$this->data["subview"] = "signin/index";

					$this->load->view('_layout_signin', $this->data);

				}

			}

		} else {

			$this->data["subview"] = "signin/index";
			$this->data["siteinfos"] = $this->site_m->get_site(array('adminID'=>0));
			// print_r($this->data);die();
			$this->load->view('_layout_signin', $this->data);

			$this->session->sess_destroy();

		}



	}



	public function cpassword() 
	{
		$username = $this->session->userdata("username");
		$email = $this->session->userdata("email");
        $adminID = $this->session->userdata("adminID");
        
		$this->load->library("session");
		if($_POST) 
		{
            if ($username=='demo') 
            {
                $this->session->set_flashdata('error',"Demo user can not change password");
                redirect(base_url('signin/cpassword'));
            }
            $rules = $this->rules_cpassword();
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) 
            {
                $this->data["subview"] = "signin/cpassword";
                $this->load->view('_layout_main', $this->data);
            } 
            else 
            {
                redirect(base_url('signin/cpassword'));
            }
		} 
		else 
		{
			$this->data["subview"] = "signin/cpassword";
			$this->load->view('_layout_main', $this->data);
		}
	}



	function old_password_unique() {

		if($this->signin_m->change_password() == TRUE) {
            
            $this->send_mail_password_reset();
			return TRUE;

		} else {

			$this->form_validation->set_message("old_password_unique", "%s does not match");

			return FALSE;

		}

	}
	
	public function send_mail_password_reset()
	{
	    $usertype = $this->session->userdata("usertype");
        $loginuserID = $this->session->userdata("loginuserID");
        $adminID = $this->session->userdata("adminID");
        if($usertype == "Admin" || $usertype == 'ClgAdmin') 
        {
            $this->load->library('email');
            $config=array(
            'charset'=>'utf-8',
            'wordwrap'=> TRUE,
            'mailtype' => 'html'
            );
            
            $this->email->initialize($config);
            $html = $this->load->view('emailTemplates/passwordresetsuperadmin',$this->data,true);
      
            $this->email->set_mailtype("html");
            $this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
            $this->email->to($this->data["siteinfos"]->email);
            $this->email->subject("ITM Password Reset");
            $this->email->message($html);
            $this->email->send();
        } 
        else 
        {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
	}



	public function signout() {

		$this->signin_m->signout();

		redirect(base_url("signin/index"));

	}

		public function logoutAllDevices() {

		$this->signin_m->logoutOtherDevice();

		$this->session->set_flashdata('success', 'Log out Successfully');
				redirect(base_url("signin/cpassword"));

		redirect(base_url("signin/index"));

	}

	public function newuser()
	{
		$firstName=$this->input->post('firstName');
		$lastName=$this->input->post('lastName');
		$fullname=$firstName.' '.$lastName;
		$email=$this->input->post('email');
		$phone=$this->input->post('phone');
		$subject="Welcome To E-campus";
		$checknewuser=$this->signin_m->checknewuserdetails($email,$phone);
		if($checknewuser)
		{
			$this->session->set_flashdata('error',"Your Email Id and Phone Number is Already Register!!");
              redirect(base_url());
		}
		else
		{
			$username=$this->signin_m->newusersignin();
			if($username)
			{ 
				$setting = $this->setting_m->get_setting_byAdmin($username['adminID']);
				$institute=$setting->sname;
				$dataEamil['username'] = $username['pass'];
				$dataEamil['fullname']=$fullname;
				$dataEamil['password']=$username['password'];
				$dataEamil['email']=$email;
				$dataEamil['adminID']=$username['adminID'];
				$dataEamil['institute']=$institute;
		        $dataEamil['superadminemail']="";
	            $dataEamil['adminemail']="";
				
				$html = $this->load->view('emailTemplates/welcomeemail', $dataEamil , true);

				$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
				if($sendmail == true)
				{
					$this->session->set_flashdata('success',"Thank You For Using E-campus");
					redirect(base_url('dashboard/index'));
				}
				else
				{
					$this->session->set_flashdata('error',"Check Your Email-ID");
				    redirect(base_url());
				}
				
	        }
		}
		


	}

	function Verifyemail()
	{
		$uri=$this->uri->segment(4);
		$adminID=$this->signin_m->verifymailbyadminid($uri);
        if($adminID == true)
        {
        	$this->load->view('Upgradeplan/verifymail');
        }
        else
        {
        	$this->session->set_flashdata('success',"Your Email-ID is Already Verifyed");
		    redirect("https://e-campus.in/Home");
        }
		
	}

  



}








