<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("notice_m");
		$this->load->model("subject_m");
		$this->load->model("alert_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$this->load->model("classes_m");
		$this->load->model("student_info_m");
		$this->load->model("professor_m");
		$this->load->model("videos_m");
		$this->load->model("lms_m");
		$this->load->library('pagination');

		$language = $this->session->userdata('lang');

		$this->lang->load('video', $language);	

	}


	public function index() {

		$usertype = $this->session->userdata("usertype");
$SpecialUsertype = $this->session->userdata('SpecialUsertype');
		$adminID = $this->session->userdata("adminID");
		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
		// $this->data['videogenID'] = $this->UploadVideos();
		if($usertype == 'ClgAdmin' || $usertype == 'Support' || $usertype == 'Academic' || $usertype == 'Admin' || $SpecialUsertype=='Btech') {

			$year = date("Y");



			// $this->data['notices'] = $this->notice_m->get_order_by_notice(array('year' => $year));

			if ($this->input->post('classesID')) {
				$this->session->unset_userdata('FilterVideosubCourseID');
				$this->session->unset_userdata('FilterVideoyearSemesterID');
				$this->session->set_userdata('FilterVideosubjectID', $this->input->post('classesID'));
				$this->session->set_userdata('FilterVideoclassesID', $this->input->post('classesID'));
			}
	        if ($this->input->post('subCourseID')) {
				$this->session->set_userdata('FilterVideosubCourseID', $this->input->post('subCourseID'));
			}
		  	if ($this->input->post('yearSemesterID')) {
				$this->session->set_userdata('FilterVideoyearSemesterID', $this->input->post('yearSemesterID'));
			}
		    if ($this->input->post('subjectIDforvideo')) {
				$this->session->set_userdata('FilterVideosubjectID', $this->input->post('subjectIDforvideo'));
			}

			if ($this->input->post('unitIDforvideo')) {
				$this->session->set_userdata('FilterVideounitID', $this->input->post('unitIDforvideo'));
			}

			if ($this->input->post('professorIDvideo')) {
				$this->session->set_userdata('FilterVideoprofessorID', $this->input->post('professorIDvideo'));
			}

			if ($this->input->post('option_user_selection')) {
				$this->session->set_userdata('option_user_selection', $this->input->post('option_user_selection'));
			}
			

		$config = array();

        $seg = $this->uri->segment(3);
        $config['base_url'] = site_url('videos/index/'.$seg);
        
        $rem =   $this->lms_m->getPaginationrowvideo();

        $config['total_rows'] = $rem;
        $config['per_page'] = "4";
        $config["uri_segment"] = 4;
        
        $choice = ($config["total_rows"] / $config["per_page"]);
        
        $config["num_links"] = floor($choice);

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $this->data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;            
        $this->data['pagination'] = $this->pagination->create_links();
		$this->data['subjects'] = $this->lms_m->get_join_where_subject_video($config["per_page"], $this->data['page'],$adminID);
		$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterVideoclassesID'));
		$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterVideoclassesID'));
			$this->data["subview"] = "videos/index";

			$this->load->view('_layout_main', $this->data);

		} elseif ($usertype == "Student") {
			$student = $this->student_info_m->get_student_info();
			$this->data['notice'] = $this->notice_m->notice_list_all($student->studentID);			
				$this->data["subview"] = "videos/index";

			$this->load->view('_layout_main', $this->data);

		}else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	protected function rules() {

		$rules = array(

				 array(

					'field' => 'title', 

					'label' => $this->lang->line("notice_title"), 

					'rules' => 'trim|required|xss_clean|max_length[128]'

				), 

				array(

					'field' => 'date', 

					'label' => $this->lang->line("notice_date"),

					'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid'

				),

				array(

					'field' => 'notice', 

					'label' => $this->lang->line("notice_notice"),

					'rules' => 'trim|required|xss_clean',


				)

			);

		return $rules;

	}



	public function add() {

		$usertype = $this->session->userdata("usertype");

		$SpecialUsertype = $this->session->userdata('SpecialUsertype');
		$adminID = $this->session->userdata("adminID");

		$year = date("Y");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin' || $usertype == 'Support' || $usertype == 'Academic' || $SpecialUsertype=='Btech') {

			if($SpecialUsertype=='Btech'){
			  $this->db->where('departmentID',6);
			  $this->data['classes'] =  $this->db->get('classes')->result();	
				}else{
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			    }


		$this->data['professor'] = $this->professor_m->get_professor_all($adminID);


	    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterVideoclassesID'));

        $this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterVideoclassesID'));
if(empty($this->session->userdata('FilterVideosubCourseID'))){
$sub_coursesID = 0;
}else{
$sub_coursesID = $this->session->userdata('FilterVideosubCourseID');	
}
        $this->data['subject']  =   $this->subject_m->GetSubjectByParam(array('classesID'=>$this->session->userdata('FilterVideoclassesID'),'sub_coursesID'=>$sub_coursesID, 'yearsOrSemester'=>$this->session->userdata('FilterVideoyearSemesterID')));
        

			if($_POST) {
				// print_r(date("Y-m-d", strtotime($this->input->post("date"))));
				// exit();

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors();

					$this->data["subview"] = "videos/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
					
					$classesID  = $this->input->post('classesID');
					$subCourseID  = $this->input->post('sub_coursesID');
					$yearSemesterID = $this->input->post('yearsOrSemester');
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						'classesID' => $classesID, 
						'sub_coursesID' => $subCourseID,
						'yearsOrSemester' => $yearSemesterID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"professorID" => $this->input->post("professor"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num

					);
					
				
					$noticeID = $this->videos_m->insert_videos($array);

					$array['notice_type'] = $this->input->post("notice_type");

					$array['noticeID'] = $noticeID;

					           $this->notice_m->notice_x_student($array);

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					redirect(base_url("videos/index"));

				}

			} else {

				$this->data["subview"] = "videos/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	
	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->notice_m->get_join_notice_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->notice_m->make_datatables($adminID);

            // $totalFiltered  = $this->student_m->get_filtered_data();
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['title'] = $post->title;
				$nestedData['date'] = $post->noticeDate;
                $nestedData['classesID'] = $post->classes;
                $nestedData['sub_coursesID'] = $post->sub_course;
                $nestedData['yearsOrSemester'] = $post->yearsOrSemester;
                $nestedData['notice'] = substr($post->notice,0,20).'...';

   			if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
   		    $buttons     = btn_view('notice/view/'.$post->noticeID, $this->lang->line('view')).btn_edit('notice/edit/'.$post->noticeID, $this->lang->line('edit')).btn_delete('notice/delete/'.$post->noticeID, $this->lang->line('delete'));
 		

			    $nestedData['action'] = $buttons;
			}
               
                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

      
            
        echo json_encode($json_data); 
}


	public function edit() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Admin" ||  $usertype == 'ClgAdmin') {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
	       $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			if((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {
				$this->data['get_sub_courses'] = $this->student_m->get_sub_courses($this->data['notice']->classesID);
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data['notice']->classesID);
			
					if($_POST) {

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) {

							$this->data["subview"] = "notice/edit";

							$this->load->view('_layout_main', $this->data);			

						} else { 	


					$classesID  = $this->input->post('classesID');
					$subCourseID  = $this->input->post('sub_coursesID');
					$yearSemesterID = $this->input->post('yearsOrSemester');
						$num = 1;
							$year = date("Y");
						$array = array(
						'adminID' => $adminID,
						'classesID' => $classesID, 
						'sub_coursesID' => $subCourseID,
						'yearsOrSemester' => $yearSemesterID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num

							);

							$this->notice_m->update_notice($array, $id);

					$array['notice_type'] = $this->input->post("notice_type");

					$array['noticeID'] = $id;

							$this->notice_m->notice_x_student_update($array);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("notice/index"));

						}

					} else {

						$this->data["subview"] = "notice/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}




	public function view() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			if ($this->session->userdata("usertype")=='ClgAdmin') {
			$this->data['notice'] = $this->notice_m->get_notice($id);
			if($this->data['notice']) {

			$this->data["subview"] = "notice/view";

			$this->load->view('_layout_main', $this->data);

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}
}elseif($this->session->userdata("usertype")=='Student'){
$this->data['notice'] = $this->notice_m->notice_single($this->session->userdata('loginuserID'),$id);
			if($this->data['notice']) {

				$this->notice_m->update_notice_x_student_read($this->session->userdata('loginuserID'),$id);

			$this->data["subview"] = "notice/view";

			$this->load->view('_layout_main', $this->data);
			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}
}


		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}	

	}

	function hide_Notice_icon(){		
$data = array(
'iconStatus'=>0
);
	$this->db->where('studentID',$this->session->userdata('loginuserID'));
	$this->db->update('notice_x_student',$data);
}

	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "ClgAdmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$this->notice_m->delete_notice($id);
				$this->notice_m->delete_notice_x_student($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("notice/index"));

			} else {

				redirect(base_url("notice/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	function date_valid($date) {

		if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);   

	        $dd = $arr[0];            

	        $mm = $arr[1];              

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    } 

	} 



public function UploadVideos(){
// print_r("expression");die;
$bhikhari  = mt_rand(1000,9999);
$VideosubjectID = $this->session->userdata('FilterVideosubjectID');
$videounitID=$this->session->userdata('FilterVideounitID');
$coursesid=$this->session->userdata('FilterVideoclassesID');
$yearsOrSemester=$this->session->userdata('FilterVideoyearSemesterID');
                    $config_sing['upload_path'] = 'uploads/videos';
					$config_sing['allowed_types'] = 'mp4|video/mp4';
					$config_sing['encrypt_name'] = TRUE;
					$this->load->library('upload', $config_sing);
					 $this->upload->initialize($config_sing);
					$this->upload->do_upload('file');
					
					$this->gallery_path = realpath(APPPATH.'../uploads/videos');
					$filename = $this->upload->data();
					$videoname=$this->upload->file_name;
// print_r($filename);die;
					$userdata=array(
									 'src'=>$videoname,
									 'genID'=>$bhikhari,
									 'subjectID'=>$VideosubjectID,
									 'unitID'=>$videounitID,
									 'classesID'=>$coursesid,
									 'yearsOrSemester'=>$yearsOrSemester	
									);
					$this->db->select('videoID');
					// $this->db->where('classesID',$coursesid);
					// $this->db->where('yearsOrSemester',$yearsOrSemester);
					// $this->db->where('subjectID',$VideosubjectID);
					$this->db->where('unitID',$videounitID);
					$video=$this->db->get('videos')->row();
					// print_r($video->videoID);die;
					 if($video)
					 {
					   $this->db->where('videoID',$video->videoID);
					   $this->db->update('videos',$userdata);
					   // $this->session->set_flashdata('success', $this->lang->line('menu_success'));
					   // redirect(base_url("videos/index"));
					   // print_r("expression");die;
					 }
					 else
					 {
					   $this->db->where('genID',$bhikhari);
					   $num = $this->db->get('videos')->num_rows();
					   if ($num==0)
					   {
					     $this->db->insert('videos',$userdata);
					     $videogenID = $this->db->insert_id();
					     // $ID=$videogenID->row();
					     $updatedata= array('classesID' =>$classesID,'yearsOrSemester'=>$yearsOrSemester);
					     $this->db->where('videoID',$videogenID);
					     $this->db->update('videos',$updatedata);
					     // $this->session->set_flashdata('success', $this->lang->line('menu_success'));
					     // redirect(base_url("videos/index"));
					   }
					 }
					// $this->db->where('genID',$bhikhari);
				 //    $num = $this->db->get('videos')->num_rows();
				 //    if ($num==0)
				 //    {
					//      $this->db->insert('videos',$userdata);
					//      $videogenID = $this->db->insert_id();
					//      print_r($videogenID);die;
					     // $ID=$videogenID->row();
					     // $updatedata= array('classesID' =>$classesID,'yearsOrSemester'=>$yearsOrSemester);
					     // $this->db->where('videoID',$videogenID);
					     // $this->db->update('videos',$updatedata);
					     // $this->session->set_flashdata('success', $this->lang->line('menu_success'));
					     // redirect(base_url("videos/index"));
				    // }


		// print_r('Image Uploaded Successfully.');die;





	// $this->load->library('fancyFileUploaderHelper');
	// Depending on your server, you might have to use $_POST instead of $_REQUEST.
	// $unitvideo = $this->input->post('unitvideo');
	// $VideoprofessorID = $this->session->userdata('FilterVideoprofessorID');
 //    $classesID=$this->input->post('classesID');
 //    $yearsOrSemester=$this->input->post('yearsOrSemester');

	
	// print_r($unitvideo);die;
	// $option_user_selection  = $this->session->userdata('option_user_selection');
    // $bhikhari  = mt_rand(1000,9999);
    
// print_r($videounitID);die;
   
	// $videosetfor = $this->session->set_userdata('FilterVideoprofessorID');

		// header("Content-Type: application/json");

		// $allowedexts = array(
		// 	"jpg" => true,
		// 	"png" => true,
		// 	"mp4" => true,
		// );

		// $files = FancyFileUploaderHelper::NormalizeFiles("files");

		// if (!isset($files[0]))  $result = array("success" => false, "error" => "File data was submitted but is missing.", "errorcode" => "bad_input");
		// else if (!$files[0]["success"])  $result = $files[0];
		// else if (!isset($allowedexts[strtolower($files[0]["ext"])]))
		// {
		// 	$result = array(
		// 		"success" => false,
		// 		"error" => "Invalid file extension.  Must be '.jpg' or '.png'.",
		// 		"errorcode" => "invalid_file_ext"
		// 	);
		// }
		// else
		// {
		// 	// For chunked file uploads, get the current filename and starting position from the incoming headers.
		// 	$name = FancyFileUploaderHelper::GetChunkFilename();
		// 	if ($name !== false)
		// 	{
		// 		$startpos = FancyFileUploaderHelper::GetFileStartPosition();

		// 		// [Do stuff with the file chunk.]
		// 	}
		// 	else
		// 	{
		// 		// [Do stuff with the file here.]
		// 	$textdatforvideo = $bhikhari.$this->session->userdata('option_user_selection'). "." . strtolower($files[0]["ext"]);
            

		// 	 	// copy($files[0]["file"], FCPATH. "/uploads/videos/" .$bhikhari.$this->session->userdata('option_user_selection'). "." . strtolower($files[0]["ext"]));


				


		// 	}

		// 	$result = array(
		// 		"success" => true
		// 	);
		// }
// $videoname="";
// print_r($_FILES['files']['name']);die;
		// if($_FILES['files']['name'])
		// 	{	
					
		// 			$config_sing['upload_path'] = 'uploads/videos';
		// 			$config_sing['allowed_types'] = 'gif|jpg|png|jpeg|mp4|video/mp4';
		// 			$config_sing['encrypt_name'] = TRUE;
		// 			$this->load->library('upload', $config_sing);
		// 			 $this->upload->initialize($config_sing);
		// 			$this->upload->do_upload('files');
					
		// 			$this->gallery_path = realpath(APPPATH.'../uploads/videos');
		// 			$filename = $this->upload->data();
		// 			$videoname=$this->upload->file_name;
		// 	        // print_r($filename);die;
		// 	}


	


		// echo json_encode($result, JSON_UNESCAPED_SLASHES);



}

	public function print_preview() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			$this->data['notice'] = $this->notice_m->get_notice($id);

			if($this->data['notice']) {



				$this->load->library('html2pdf');

			    $this->html2pdf->folder('./assets/pdfs/');

			    $this->html2pdf->filename('Report.pdf');

			    $this->html2pdf->paper('a4', 'portrait');

			    $this->data['panel_title'] = $this->lang->line('panel_title');

				$html = $this->load->view('notice/print_preview', $this->data, true);

				$this->html2pdf->html($html);

				$this->html2pdf->create();

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}	

	}


function ResetCourses(){

$this->session->unset_userdata('FilterVideoclassesID');
$this->session->unset_userdata('FilterVideoyearSemesterID');   
}
function ResetSubcourses(){
$this->session->unset_userdata('FilterNoticesubCourseID');
}

function ResetSemesterYear(){

$this->session->unset_userdata('FilterVideoyearSemesterID');
	   
}
function ResetAllfilter(){

$this->session->unset_userdata('FilterNoticesubCourseID');
$this->session->unset_userdata('FilterNoticeclassesID');
$this->session->unset_userdata('FilterNoticeyearSemesterID');  
}



	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" ||  $usertype == 'ClgAdmin') {

			$id = $this->input->post('id');

			if ((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {



					$this->load->library('html2pdf');

				    $this->html2pdf->folder('uploads/report');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('notice/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));	

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}
	public function deletevideos()
	{
		$id=$this->input->post('ID');
		$this->db->select('src');
		$this->db->where('videoID',$id);
		$videoname=$this->db->get('videos')->row();
		if($videoname)
		{
			// unlink('uploads/videos',$videoname->src);
			unlink("uploads/videos/".$videoname->src);
			$this->db->where('videoID',$id);
			$this->db->delete('videos');
		}
		
	}

}



/* End of file notice.php */

/* Location: .//D/xampp/htdocs/school/mvc/controllers/notice.php */