<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Customemail extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("notice_m");
		$this->load->model("subject_m");
		$this->load->model("alert_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$this->load->model("classes_m");
		$this->load->model("student_info_m");
		$this->load->model("professor_m");
		$this->load->model("setting_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('customemail', $language);

	}



	public function index() {


		
		date_default_timezone_set('Asia/Kolkata');
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$setting = $this->setting_m->get_setting_byAdmin($adminID);
		$year = date("Y");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin' || $usertype == 'Professor' || $usertype == 'Accountant') 
		{
			$getadmin=$this->professor_m->getsuperadminid($adminID);
	        	$institute=$setting->sname;
	        	if($getadmin)
	        	{
	        		$superadminemail=$getadmin->email;

	        	}
	        	$getemail=$this->professor_m->getadminid($loginuserID);
	        	$adminemail="";
	        	if($getemail)
	        	{
	        		$adminemail=$getemail->email;
	        	}
		    $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		    $this->data['Professor'] = $this->professor_m->get_professor_bycustommail($adminID);
		    $this->data['superadmin'] = $this->professor_m->superadminid($adminID);
		    $this->data['Admin'] = $this->professor_m->adminid($adminID);
		    $this->data['Accountant'] = $this->professor_m->accountantid($adminID);
			if($_POST) 
			{
		        $rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) 
				{
					$this->data['form_validation'] = validation_errors();
					$this->data["subview"] = "customemail/index";
					$this->load->view('_layout_main', $this->data);			
				} 
				else 
				{
					
					$dataEamil['title'] = $this->input->post("title");
					$dataEamil['date'] = date("Y-m-d", strtotime($this->input->post("date")));
					$dataEamil['description'] = strip_tags(html_entity_decode($this->input->post("notice")));
					$dataEamil['institute']=$institute;
	        		$dataEamil['logo']=$setting->photo;
	        		$dataEamil['superadminemail']=$superadminemail;
            		$dataEamil['adminemail']=$adminemail;
					$superadmin=$this->input->post('superadmin');
					$admin=$this->input->post('admin');
					$professor=$this->input->post('professor');
					$accountant=$this->input->post('accountant');
					$getemailidsender=$this->professor_m->getemailidsender($loginuserID);
					$sendmail="";
					if($superadmin)
					{
						for($i=0;$i< count($superadmin);$i++)
                    	{
							$getemail=$this->professor_m->getsuperadminid($superadmin[$i]);
							if($getemail)
                    		{
                    			$subject=$usertype;
							    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
							    $email=$getemail->email;
							    $fullname=$getemail->name;
							    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
							}
                    	}
					}
					if($admin)
					{
						for($i=0;$i< count($admin);$i++)
                    	{
							$getemail=$this->professor_m->getadminid($admin[$i]);
							if($getemail)
                    		{
                    			$subject=$usertype;
							    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
							    $email=$getemail->email;
							    $fullname=$getemail->name;
							    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
                    			
                    		}
                    	}
					}
					if($accountant)
					{
						for($i=0;$i< count($accountant);$i++)
                    	{
							$getemail=$this->professor_m->getadminid($accountant[$i]);
							if($getemail)
                    		{
                    			$subject=$usertype;
							    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
							    $email=$getemail->email;
							    $fullname=$getemail->name;
							    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
                    		}
                    	}
					}
					
					// echo $this->data["siteinfos"]->email." ".$this->data['siteinfos']->sname;die;
                    if($professor)
                    {
                    	$professorid=$this->input->post('professorid');
                    	// print_r($professorid);die;
                    	for($i=0;$i< count($professorid);$i++)
                    	{
                    		$getemail=$this->professor_m->getprofessoremailid($professorid[$i]);
                    		if($getemail)
                    		{
                    			$subject=$usertype;
							    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
							    $email=$getemail->email;
							    $fullname=$getemail->name;
							    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
                    		}
                    	}
                    }
                    if($sendmail == true)
					{
						// print_r($send_mail);die;
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("customemail/index"));
					}
					else
					{
						$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						redirect(base_url("customemail/index"));
					}
				}
			} 
			else 
			{
				$this->data["subview"] = "customemail/index";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}

	}



	protected function rules() {

		$rules = array(
				
				array(

					'field' => 'title', 

					'label' => $this->lang->line("notice_title"), 

					'rules' => 'trim|required|max_length[128]'

				), 

				array(

					'field' => 'date', 

					'label' => $this->lang->line("notice_date"),

					'rules' => 'trim|required|max_length[10]'

				)


			);

		return $rules;

	}
	public function sendmailbyprofessor()
	{
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		$studentID=$this->input->post('studentID');
		$subjectname=$this->input->post('subjectname');
		$writemail=$this->input->post('writemail');
		$setting = $this->setting_m->get_setting_byAdmin($adminID);
		if($studentID)
	    {
	    	$getadmin=$this->professor_m->getsuperadminid($adminID);
        	$institute=$setting->sname;
        	if($getadmin)
        	{
        		$superadminemail=$getadmin->email;
        	}
        	$getemail=$this->professor_m->getadminid($loginuserID);
        	$adminemail="";
        	if($getemail)
        	{
        		$adminemail=$getemail->email;
        	}
	    	$loginuserID = $this->session->userdata("loginuserID");
	    	$dataEamil['institute']=$institute;
	        $dataEamil['logo']=$setting->photo;
	        $dataEamil['superadminemail']=$superadminemail;
            $dataEamil['adminemail']=$adminemail;
	    	$dataEamil['title'] = $subjectname;
			$dataEamil['date'] = date("d-m-Y");
			$dataEamil['description'] = $writemail;
			$emailsendby=$this->professor_m->getprofessoremailid($loginuserID);
    		$emailreciverby=$this->professor_m->getreciverstudentid($studentID);
    		if($emailsendby)
    		{
    		    $subject=$usertype;
			    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
			    $email=$emailreciverby->email;
			    $fullname=$emailreciverby->name;
			    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
    			

    		}
	    	
	    }
	    if($sendmail == true)
		{
			// print_r($send_mail);die;
			$a="E-mail Sent Successfuly";
			echo $a;
		}
		else
		{
			$a="E-mail Not Send!!";
			echo $a;
		}
	}

	function verifyemailsuperadmin()
	{
		$adminID=$this->input->post('adminID');
		if($adminID)
		{
			$setting = $this->setting_m->get_setting_byAdmin($adminID);
			$getadmin=$this->professor_m->getsuperadminid($adminID);
	    	$institute=$setting->sname;
	    	if($getadmin)
	    	{
	    		$superadminemail=$getadmin->email;
	    	}
	    	$fullname=$getadmin->name;
			$email=$superadminemail;
			$dataEamil['logo']=$setting->photo;
			$dataEamil['adminID']=$adminID;
			$dataEamil['password']="d6162a34495e2a5c0a43598d480a31b916347e85e8bd0d4b33a2be16922d0435cbda249cea34bdb2e869e0ac3d1aa4a2ad4d17390cdcc0be0144f14698e17927";
			$dataEamil['institute']=$institute;
	        $dataEamil['superadminemail']="";
	        $dataEamil['adminemail']="";
			$subject="verify Email";
			$html = $this->load->view('emailTemplates/verifyemail', $dataEamil , true);

			$sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);
			if($sendmail == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	

	

}



