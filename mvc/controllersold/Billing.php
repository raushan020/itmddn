<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("teacher_m");
		$this->load->model("user_m");
		$this->load->model("systemadmin_m");
		$this->load->model("professor_m");
		$this->load->model("admin_m");
		$language = $this->session->userdata('lang');
		$this->lang->load('profile', $language);

		$language = $this->session->userdata('lang');

		$this->lang->load('dashboard', $language);
	}
		function index()
		{
			$adminID=$this->session->userdata('adminID');
			$this->data['plans']=$this->admin_m->getalladmindata($adminID);
			$this->data["subview"] = "billing/index";
			$this->load->view('_layout_main',$this->data);
		}

		function invoice()
		{
			$adminID=$this->session->userdata('adminID');
			$this->data['plans']=$this->admin_m->getalladmindata($adminID);
			$this->data['admininvoice']=$this->admin_m->getalladmininvoice($adminID);
			$this->data["subview"] = "billing/invoice";
			$this->load->view('_layout_main',$this->data);
		}

}