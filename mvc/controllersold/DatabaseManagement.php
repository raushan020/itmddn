<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class DatabaseManagement extends Admin_Controller {



	function __construct() {

		parent::__construct();

		$this->load->model("book_m");
		$this->load->model("education_details_m");

		$language = $this->session->userdata('lang');
		$this->load->model("setting_m");

				$language = $this->session->userdata('lang');

		$this->lang->load('classes', $language);	

	}



public function Courses(){
$usertype = $this->session->userdata("usertype");
$adminID = $this->session->userdata("adminID");
if ($usertype == "superadmin") {
$this->db->where('adminID',1);
$query =  $this->db->get('classes');
$this->data['courses'] = $query->result();
$query =  $this->db->get('admin');
$this->data['admins'] = $query->result();
}elseif($usertype == 'ClgAdmin'){

$SuperAdminId = 1;
			$this->data['education_details'] = $this->education_details_m->get_eduction_details($SuperAdminId);

	if ($_POST) {
      $this->session->set_userdata('sessionCourseDataManagement', $this->input->post('id'));
	}
	if ($this->session->userdata('sessionCourseDataManagement')) {
	   $this->db->where('classesID',$this->session->userdata('sessionCourseDataManagement'));
	}
$this->db->where('adminID',$adminID);
$query =  $this->db->get('classes');
$this->data['courses'] = $query->result();

$this->db->where('adminID',$adminID);
$query =  $this->db->get('classes');
$this->data['coursesList'] = $query->result();


$query =  $this->db->get('admin');
$this->data['admins'] = $query->result();
}

$this->data["subview"] = "databaseManagement/courses";
$this->load->view('_layout_main', $this->data);


}

function insertCopyCourses(){

$courses = $this->input->post('courses');
$education_detailsID = $this->input->post('education_detailsID');
$classesID = $this->input->post('classesID');
$duration = $this->input->post('duration');
$mode = $this->input->post('mode');
$fee = $this->input->post('fee');
$adminID = $this->input->post('adminID');;

for ($i=0; $i <count($courses); $i++) { 
	 

				    $digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int

				    $char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string

                    $setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));

                    $sitenameString   =  $setingsData->sname;
 						
 					$firstCharacter = substr($sitenameString, 0, 2);

 					$courseCode      =   strtoupper($firstCharacter).'-'.$char.$digit;

$data = array(
"education_detailsID"=>$education_detailsID[$i],
"adminID"=>$adminID,
"courseCode"=>$courseCode,
"classes"=>$courses[$i],
"duration"=>$duration[$i],
"mode"=>$mode[$i],
"fee"=>$fee[$i]
	);

 $this->db->insert('classes',$data);  
 $id = $this->db->insert_id();

$subcoursesID = $this->input->post('subcoursesID');
$subcourses = $this->input->post('subcourses');
for($j=0; $j<count($subcoursesID) ; $j++) { 


				    $digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int

				    $char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string

                    $setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));

                    $sitenameString   =  $setingsData->sname;
 						
 					$firstCharacter = substr($sitenameString, 0, 2);

 					$SubcourseCode      =   strtoupper($firstCharacter).'-'.$char.$digit;
 					
if ($subcoursesID[$j]==$classesID[$i]) {
$data = array(
"sub_course"=>$subcourses[$j],
"subCourseCode"=>$SubcourseCode,
"adminID"=>$adminID,
"classesID"=>$id
	);

$this->db->insert('sub_courses',$data);

}

}


}

}


function ResetCoursesForDataManagement(){

$this->session->unset_userdata('sessionCourseDataManagement');
	 
}


function CourseView(){
$adminID = $this->session->userdata("adminID");
$this->db->where('adminID',$adminID);
$query =  $this->db->get('classes');
$this->data['courses'] = $query->result();

$this->data["subview"] = "databaseManagement/courseview";

$this->load->view('_layout_main', $this->data);

}
	

}

