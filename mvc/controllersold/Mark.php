<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mark extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("student_m");

		$this->load->model("mark_m");

		$this->load->model("grade_m");

		$this->load->model("classes_m");

		$this->load->model("exam_m");

		$this->load->model("subject_m");

		$this->load->model("sub_courses");

		$this->load->model("user_m");

		$this->load->model("student_info_m");

		$this->load->model("section_m");

		$this->load->model("parentes_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('mark', $language);

	}

	protected function rules() {

		$rules = array(

			array(

				'field' => 'examID',

				'label' => $this->lang->line("mark_exam"),

				'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_exam'

			),

			array(

				'field' => 'classesID',

				'label' => $this->lang->line("mark_classes"),

				'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_classes'

			),

			array(

				'field' => 'subjectID',

				'label' => $this->lang->line("mark_subject"),

				'rules' => 'trim|required|xss_clean|max_length[11]|callback_check_subject'

			)

		);

		return $rules;

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {

	     $this->data['all_count']  =  $this->subject_m->all_count($adminID);
		$this->data['ActiveSubjects_count']  =  $this->subject_m->ActiveSubjects_count($adminID);
		$this->data['DraftSubjects_count']  =  $this->subject_m->DraftSubjects_count($adminID);
		$this->data['TrashSubjects_count']  =  $this->subject_m->TrashSubjects_count($adminID);

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
            $id=$this->uri->segment(3);
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterMarksubCourseID');
					$this->session->unset_userdata('FilterMarkyearSemesterID');
					$this->session->set_userdata('FilterMarkclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterMarksubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
				}
// print_r($this->session->userdata('FilterMarkclassesID'));
// exit();
				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterMarkclassesID'));
				
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterMarkclassesID'));

                $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "mark/search";

				$this->data['subjects'] = $this->mark_m->get_join_mark($adminID);	
				$this->data["subview"] = "mark/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$student = $this->student_info_m->get_student_info();



			$this->data['subjects'] = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID);

			$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);

		if ($this->input->post('yearSemesterID')) {
	
			if ($this->input->post('yearSemesterID')=='Select') {
			$this->session->unset_userdata('FilterMarkyearSemesterID');
			}else{
			$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
		  }
			}


			$this->data["subview"] = "subject/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					$classesID = $checkstudent->classesID;

					$this->data['set'] = $id;

					$this->data['subjects'] = $this->subject_m->get_join_subject($classesID);

					$this->data["subview"] = "subject/index_parent";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "subject/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->mark_m->get_join_result_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->mark_m->make_datatables($adminID);

$data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] =$i++;
				$nestedData['name'] = $post->name;
				$nestedData['subject_code'] = $post->subject_code;
				$nestedData['subject'] = $post->subject;
                $nestedData['classes'] = $post->classes;
                $nestedData['sub_course'] = $post->sub_course;
                $nestedData['yearsOrSemester'] = str_replace('_', ' ', $post->yearsOrSemester);

  		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			// $button   =  "<a data-toggle='tooltip'  data-placement='top' title='Fee' href='".base_url()."lms/document/$post->videoID' class='btn btn-info for_margR'><i class='fa fa-inr'></i></a>";			
			$buttons = btn_view('mark/index/#', $this->lang->line('view'));
   			$nestedData['action'] = $buttons;
		}
               
        $data[] = $nestedData;


    }
}

	$json_data = array(
    "draw"            => intval($this->input->post('draw')),  
    "recordsTotal"    => $totalData,  
    "recordsFiltered" => $totalFiltered, 
    "data"            => $data   
    );

  // print_r($data);
  // exit();
    echo json_encode($json_data); 
}

	public function add() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Teacher") {

			$this->data['students'] = array();

			$this->data['set_exam'] = 0;

			$this->data['set_classes'] = 0;

			$this->data['set_subject'] = 0;

			$classesID = $this->input->post("classesID");

			if($classesID != 0) {

				if($usertype == "Admin") {

					$this->data['subjects'] = $this->subject_m->get_subject_call($classesID);

				} elseif($usertype == "Teacher") {

					$username = $this->session->userdata("username");

					$teacher = $this->user_m->get_username_row("teacher", array("username" => $username));

					$this->data['subjects'] = $this->subject_m->get_order_by_subject(array("classesID" => $classesID, "teacherID" => $teacher->teacherID));

				}

			} else {

				$this->data['subjects'] = 0;

			}

			$this->data['exams'] = $this->exam_m->get_exam();

			$this->data['classes'] = $this->classes_m->get_classes();

			if($_POST) {

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data["subview"] = "mark/add";

					$this->load->view('_layout_main', $this->data);

				} else {

					$examID = $this->input->post('examID');

					$classesID = $this->input->post('classesID');

					$subjectID = $this->input->post('subjectID');

					$this->data['set_exam'] = $examID;

					$this->data['set_classes'] = $classesID;

					$this->data['set_subject'] = $subjectID;

					$exam = $this->exam_m->get_exam($examID);

					$subject = $this->subject_m->get_subject($subjectID);

					$year = date("Y");

					$students = $this->student_m->get_order_by_student(array("classesID" => $classesID));

						if(count($students)) {

							foreach ($students as $student) {

								$studentID = $student->studentID;

								$in_student = $this->mark_m->get_order_by_mark(array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $studentID));

								if(!count($in_student)) {

									$array = array(

										"examID" => $examID,

										"exam" => $exam->exam,

										"studentID" => $studentID,

										"classesID" => $classesID,

										"subjectID" => $subjectID,

										"subject" => $subject->subject,

										"year" => $year

									);

									$this->mark_m->insert_mark($array);

								}

							}

							$this->data['students'] = $students;

							$all_student = $this->mark_m->get_order_by_mark(array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID));

							$this->data['marks'] = $all_student;

						}

					$this->data["subview"] = "mark/add";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "mark/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function mark_send() {

		$examID = $this->input->post("examID");

		$classesID = $this->input->post("classesID");

		$subjectID = $this->input->post("subjectID");

		$inputs = $this->input->post("inputs");

		$exploade = explode("$" , $inputs);

		$ex_array = array();

		foreach ($exploade as $key => $value) {

			if($value == "") {

				break;

			} else {

				$ar_exp = explode(":", $value);

				$ex_array[$ar_exp[0]] = $ar_exp[1];

			}

		}

		$students = $this->student_m->get_order_by_student(array("classesID" => $classesID));

		foreach ($students as $student) {

			foreach ($ex_array as $key => $mark) {

				if($key == $student->studentID) {

					$array = array("mark" => $mark);

					$this->mark_m->update_mark_classes($array, array("examID" => $examID, "classesID" => $classesID, "subjectID" => $subjectID, "studentID" => $student->studentID));

					break;

				}

			}

		}

		echo $this->lang->line('mark_success');

	}

	public function view() {

		if ($this->input->post('yearSemesterID')) {
			$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
			}
		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == "ClgAdmin") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			// $url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
			$url=$this->uri->segment(4);


			$username = $this->session->userdata("username");
			$adminID = $this->session->userdata("adminID");

			$student = "";
		
			 $this->data["student"] = $this->student_m->get_student(array('studentID'=>$id,'adminID'=>$adminID));
			    $student =  $this->data["student"];
			 

			$this->data["classes"] = $this->student_m->get_class($student->classesID);
			
			$this->data["sub_course"] = $this->sub_courses->get_sub_courses_ByID($student->sub_coursesID);


			if($student && $this->data["classes"]) {
				$this->data["exams"] = $this->exam_m->get_exam($student->studentID);

				if($this->data["exams"]){
				$this->data["marks"] = $this->mark_m->get_mark($this->data["exams"]->examID);
			   }
				$this->data["subview"] = "mark/view";

				$this->load->view('_layout_main', $this->data);

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} elseif($usertype == "Student") {

			$username = $this->session->userdata("username");
			$adminID = $this->session->userdata("adminID");

			$student = "";
	
			 $student = $this->student_info_m->get_student_info();
			 $this->data["student"]   = $student;
		
			$this->data["classes"] = $this->student_m->get_class($student->classesID);

			$this->data["sub_course"] = $this->sub_courses->get_sub_courses_ByID($student->sub_coursesID);

			if($student && $this->data["classes"]) {
				$this->data["exams"] = $this->exam_m->get_exam($student->studentID);

				if($this->data["exams"]){
				$this->data["marks"] = $this->mark_m->get_mark($this->data["exams"]->examID);
			   }
				$this->data["subview"] = "mark/view";

				$this->load->view('_layout_main', $this->data);

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function exportExcel() {

		if ($this->input->post('yearSemesterID')) {
			$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
			}
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == "ClgAdmin") {

			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

		     if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterMarksubCourseID');
					$this->session->unset_userdata('FilterMarkyearSemesterID');
					$this->session->set_userdata('FilterMarkclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterMarksubCourseID', $this->input->post('subCourseID'));
				}
			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
				}
			if ($this->session->userdata('FilterMarkclassesID')) {
               $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));
               $this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));

            }
			if($this->data["classes"]) {
		     $examSingle   = $this->exam_m->get_exam_Single();
		      $this->data['examSingle'] = $examSingle;
         if ($this->data['examSingle']) {
			$this->data["classesSingle"] = $this->student_m->get_class($examSingle->classesID);

			$this->data["sub_courseSingle"] = $this->sub_courses->get_sub_courses_ByID($examSingle->sub_coursesID);

				$this->data['subjects'] =  $this->exam_m->get_Subjects($examSingle->examID);

       }
				// $this->data["exams"] = $this->exam_m->get_exam($student->studentID);

				$this->data["exams"] = $this->exam_m->get_exam_innerJoin();
				

				$this->data["subview"] = "mark/exportExcel";

				$this->load->view('_layout_main', $this->data);

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} 

	}

	public function excel() {

		if ($this->input->post('yearSemesterID')) {
			$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
			}
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == "ClgAdmin") {


			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);


		     if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterMarksubCourseID');
					$this->session->unset_userdata('FilterMarkyearSemesterID');
					$this->session->set_userdata('FilterMarktclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterMarksubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterMarkyearSemesterID', $this->input->post('yearSemesterID'));
				}


               $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


               $this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));

			if($this->data["classes"]) {

		     $examSingle   = $this->exam_m->get_exam_Single();
		      $this->data['examSingle'] = $examSingle;

			$this->data["classesSingle"] = $this->student_m->get_class($examSingle->classesID);

			$this->data["sub_courseSingle"] = $this->sub_courses->get_sub_courses_ByID($examSingle->sub_coursesID);

				$this->data['subjects'] =  $this->exam_m->get_Subjects($examSingle->examID);

				// $this->data["exams"] = $this->exam_m->get_exam($student->studentID);

				$this->data["exams"] = $this->exam_m->get_exam_innerJoin();

				$this->load->view('mark/excel', $this->data);

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} 

	}

function absent(){

	$this->data["exams"] = $this->exam_m->absent_subject();

		$this->data["subview"] = "mark/absent";

		$this->load->view('_layout_main', $this->data);

}

	function print_preview() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Teacher") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			// $url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
			$url=$this->uri->segment(4);

			if ((int)$id && (int)$url) {

				$this->data["student"] = $this->student_m->get_student($id);

				$this->data["classes"] = $this->student_m->get_class($url);

				if($this->data["student"] && $this->data["classes"]) {

					$this->data['set'] = $url;

					$this->data["exams"] = $this->exam_m->get_exam();

					$this->data["grades"] = $this->grade_m->get_grade();

					$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" =>$id, "classesID" => $url));

					$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);

					$this->load->library('html2pdf');

				    $this->html2pdf->folder('./assets/pdfs/');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('mark/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create();

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "Teacher") {

			$id = $this->input->post('id');

			$url = $this->input->post('set');

			if ((int)$id && (int)$url) {

				$this->data["student"] = $this->student_m->get_student($id);

				$this->data["classes"] = $this->student_m->get_class($url);

				if($this->data["student"] && $this->data["classes"]) {

					$this->data['set'] = $url;

					$this->data["exams"] = $this->exam_m->get_exam();

					$this->data["grades"] = $this->grade_m->get_grade();

					$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" =>$id, "classesID" => $url));

					$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);

				    $this->load->library('html2pdf');

				    $this->html2pdf->folder('uploads/report');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('mark/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function mark_list() {

		$classID = $this->input->post('id');

		if((int)$classID) {

			$string = base_url("mark/index/$classID");

			echo $string;

		} else {

			redirect(base_url("mark/index"));

		}

	}

	function student_list() {

		$studentID = $this->input->post('id');

		if((int)$studentID) {

			$string = base_url("mark/index/$studentID");

			echo $string;

		} else {

			redirect(base_url("mark/index"));

		}

	}

	function subjectcall() {

		$usertype = $this->session->userdata("usertype");

		$id = $this->input->post('id');

		if((int)$id) {

			if($usertype == "Admin") {

				$allsubject = $this->subject_m->get_order_by_subject(array("classesID" => $id));

				echo "<option value='0'>", $this->lang->line("mark_select_subject"),"</option>";

				foreach ($allsubject as $value) {

					echo "<option value=\"$value->subjectID\">",$value->subject,"</option>";

				}

			} elseif($usertype == "Teacher") {

				$username = $this->session->userdata("username");

				$teacher = $this->user_m->get_username_row("teacher", array("username" => $username));

				$allsubject = $this->subject_m->get_order_by_subject(array("classesID" => $id, "teacherID" => $teacher->teacherID));

				echo "<option value='0'>", $this->lang->line("mark_select_subject"),"</option>";

				foreach ($allsubject as $value) {

					echo "<option value=\"$value->subjectID\">",$value->subject,"</option>";

				}

			}

		}

	}

	function check_exam() {

		if($this->input->post('examID') == 0) {

			$this->form_validation->set_message("check_exam", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

function ResetCourses(){

	$this->session->unset_userdata('FilterMarkclassesID');
	$this->session->unset_userdata('FilterMarksubCourseID');	 
	$this->session->unset_userdata('FilterMarkyearSemesterID');    
}

function ResetSubcourses(){

	$this->session->unset_userdata('FilterMarksubCourseID');
	   
}

function ResetSemesterYear(){

	$this->session->unset_userdata('FilterMarkyearSemesterID');
	   
}

function check_classes() {

		if($this->input->post('classesID') == 0) {

			$this->form_validation->set_message("check_classes", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;
	}

	function check_subject() {

		if($this->input->post('subjectID') == 0) {

			$this->form_validation->set_message("check_subject", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

}



