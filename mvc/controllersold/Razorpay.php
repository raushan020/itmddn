<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Razorpay extends CI_Controller {
    // construct
    public function __construct()
    {
        parent::__construct();  
        $this->load->model("admin_m"); 
         
    }
     // initialized cURL Request
    private function get_curl_handle($payment_id, $amount)  {
        // print_r($amount);die;
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        // print_r($ch);die;
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');

        return $ch;
    }   
        
    // callback method
    public function callback() { 
      $usertype = $this->session->userdata("usertype");
      $adminID = $this->session->userdata("adminID");

      // if($usertype == "ClgAdmin")
      // {         
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {
            // print_r($amount);die;                
                $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                //execute post
                // print_r($ch);die;  
                $result = curl_exec($ch);
                // print_r($result);exit();
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    // print_r("expression1");die;  
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                   // echo "<pre>";print_r($response_array);exit;
                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
                        } else {
                            $success = false;
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'OPENCART_ERROR:Request to Razorpay Failed';
            }
            // print_r($success);die;
            if ($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                 }
                //  $order_info=array();
                // if (!$order_info['order_status_id']) {
                //     redirect($this->input->post('merchant_surl_id'));
                // } else {
                 $subscription = array('adminID' =>$adminID,'paymentID'=>$razorpay_payment_id,'orderID'=>$merchant_order_id,'currency_code'=>$currency_code,'amount'=>$amount,'created_date'=>date('Y-m-d H:i:s'),'status'=>1 );
                 $payment=$this->admin_m->adminsubscription($subscription);
                 if($payment)
                 {
                    $updateadmin=$this->admin_m->updateadminaftersubscription($adminID);
                    redirect($this->input->post('merchant_surl_id'));
                 }
                 else
                 {
                    print_r("expression");die;
                 }
                    
                // }
 
            } else {
                redirect($this->input->post('merchant_furl_id'));
            }
        }
        else
        {
            echo 'An error occured. Contact site administrator, please!';
        }
      // }
      // else
      // {
      //   redirect(base_url("upgradeplan"));
      // }
    } 
    public function success() {

        $this->session->unset_userdata('adminpackage');
        $this->session->set_userdata('adminpackage');
        $this->session->set_flashdata('success',"Thank You, Your payment has been processed successfully"); 

        redirect(base_url("dashboard"));

    }  
    public function failed() {
        $this->session->set_flashdata('error',"Sorry, your payment failed, Try agian...");  
        redirect(base_url("upgradeplan"));
    } 
}
?>