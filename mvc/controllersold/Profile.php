<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("teacher_m");
		$this->load->model("user_m");
		$this->load->model("systemadmin_m");
		$this->load->model("professor_m");
		$this->load->model('admin_m');
		$language = $this->session->userdata('lang');
		$this->lang->load('profile', $language);

		$language = $this->session->userdata('lang');

		$this->lang->load('dashboard', $language);
	}

	protected function rules() {
		$rules = array(			

			array(
				'field' => 'dob',
				'label' => $this->lang->line("student_dob"),
				'rules' => 'trim|required|max_length[10]|callback_date_valid'
			),
			array(
				'field' => 'sex',
				'label' => $this->lang->line("student_sex"),
				'rules' => 'trim|required|max_length[10]'
			),
			array(
				'field' => 'nationality',
				'label' => $this->lang->line("nationality"),
				'rules' => 'trim|max_length[25]'
			),
			array(
				'field' => 'street',
				'label' => $this->lang->line("street"),
				'rules' => 'trim|max_length[25]'
			),
			array(
				'field' => 'pin',
				'label' => $this->lang->line("pin"),
				'rules' => 'trim|min_length[4]|max_length[6]|numeric'
			),
			array(
				'field' => 'email',
				'label' => $this->lang->line("student_email"),
				'rules' => 'trim|required|max_length[40]|valid_email|callback_unique_email'
			),
			array(
				'field' => 'phone',
				'label' => $this->lang->line("student_phone"),
				'rules' => 'trim|max_length[25]|min_length[5]|required'
			),
			array(
				'field' => 'address',
				'label' => $this->lang->line("student_address"),
				'rules' => 'trim|max_length[200]'
			)
			
				

		);
		return $rules;
	}

	public function index() {
		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata('username');
		if($usertype == 'ClgAdmin' || $usertype == "superadmin") {
			$this->data['admin'] = $this->systemadmin_m->get_systemadmin($username);
			// $this->data['admindetails']=$this->admin_m->getalladmindata($adminID);
			$this->data["subview"] = "profile/index";
			$this->load->view('_layout_main', $this->data);
		} elseif($usertype == "Librarian" || $usertype == "Accountant" || $usertype == "Admin" || $usertype == "HOD" || $usertype == "Support") {
			$user = $this->user_m->get_single_user(array("username" => $username));
			if($user) {
				$this->data['user'] = $user;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Teacher") {
			$teacher = $this->teacher_m->get_single_teacher(array('username' => $username));
			if($teacher) {
				$this->data['teacher'] = $teacher;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		}elseif($usertype == "Professor") {
			$professor = $this->professor_m->get_single_professor(array('username' => $username));
			
			if($professor) {
				$this->data['professor'] = $professor;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		}elseif($usertype == "Student") {
			$student = $this->student_m->get_single_student(array('username' => $username));
			$this->data["student"] = $this->student_m->get_student(array('studentID'=>$student->studentID,'adminID'=>$student->adminID));
			$this->data["class"] = $this->student_m->get_class($student->classesID);
			

			
			if($this->data["class"]) {
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
		$this->data["education"] = $this->student_m->get_educations($student->studentID,$ArrayForRequired);

			     $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

				// $this->data["parent"] = $this->parentes_m->get_parentes($student->parentID);

				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} elseif($usertype == "Parent") {
			$parentes = $this->parentes_m->get_single_parentes(array("username" => $username));
			if($parentes) {
				$this->data['parentes'] = $parentes;
				$this->data["subview"] = "profile/index";
				$this->load->view('_layout_main', $this->data);
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function edit(){
		$loginuserID = $this->session->userdata('loginuserID');

		$this->data["student"] = $this->student_m->get_student(array('studentID'=>$loginuserID));

		if ($_POST) {

				if ($this->form_validation->run() == FALSE) 
				{
					//  echo validation_errors();
					// exit();
					$this->data["subview"] = "student/add";
					$this->load->view('_layout_main', $this->data);
				}else{
					
					$array["name"] = $this->input->post("name");
					$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
					$array["sex"] = $this->input->post("sex");
					$array["father_name"] = $this->input->post("father_name");
					$array["mother_name"] = $this->input->post("mother_name");
					$array["email"] = $this->input->post("email");
					$array["phone"] = $this->input->post("phone");
					$array["adminID"] = $adminID;
					$array["address"] = $this->input->post("address");
					$array["aadhar"] = $this->input->post("aadhar");
					$array["street"] = $this->input->post("street");
					$array["pin"] = $this->input->post("pin");
					$array["classesID"] = $this->input->post("classesID");
					$array["sub_coursesID"] = $this->input->post("sub_coursesID");
					$array["nationality"] = $this->input->post("nationality");					
					$array["session"] = $std_session;
				}		
			
		}else{
			$this->data["subview"] = "profile/editProfile";
			$this->load->view('_layout_main', $this->data);
		}
	}


	function ChangePicture(){
		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata('username');
		// print_r($username);die;
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Student" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "Professor") {
			$this->data['admin'] = $this->systemadmin_m->get_systemadmin($username);
			$this->data["subview"] = "profile/ChangePicture";
			$this->load->view('_layout_main', $this->data);
			// $this->session->set_flashdata('success',"Your Profile Photo Change Successfully ");
			// $this->load->view('_layout_main', $this->data);
			// redirect(base_url());

		}

	}

	public function unique_email() {
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		// echo "string";
		// die();
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
			
				return FALSE;
			} else {
		
				return TRUE;
			}
		}
	}

	function uploadPic(){
	    $uri = $this->uri->segment(3);
		$data = $_POST["image"];
		$image_array_1 = explode(";", $data);
		$image_array_2 = explode(",", $image_array_1[1]);
		$data = base64_decode($image_array_2[1]);
		$imageName = time() . '.png';
		$root = 'uploads/images/';
		file_put_contents($root.$imageName, $data);

			 $table = strtolower($this->session->userdata("usertype"));

		if ($uri=='undefined') {
			if($table == "admin") {

				$table = "setting";

			}

			if($table == "Student") {

				$table = "student";

			}

			if($table == "accountant") {

				$table = "user";

			}

			if($table == "clgadmin") {

				$table = "admin";

			}

			if($table == "librarian") {

				$table = "user";

			}
		}else{
			$table = "student";	
		}

			$username = $this->session->userdata("username");
	$data =  array(
	'photo'=>$imageName
	);
	if($uri=='undefined'){
			$this->db->where('username',$username);	
		
	}else{
	$this->db->where('studentID',$uri);	
	}
			
			$this->db->update($table,$data);
	if($uri=='undefined'){
	    $this->session->set_userdata('photo',$imageName);
	}
	
		echo '<img src="'.base_url().$root.$imageName.'" class="img-thumbnail" />';
	}


}


