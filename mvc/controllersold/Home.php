<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	
	public function index()
	{
		

		$this->load->view('web/index');
	}

	public function about()
	{

		$this->load->view('web/about');
	}

	public function pricing()
	{

		$this->load->view('web/pricing');
	}

	public function contact()
	{

		$this->load->view('web/contact');
	}

	public function university_management_system()
	{

		$this->load->view('web/university_management_system');
	}

	public function learning_management_system()
	{

		$this->load->view('web/learning_management_system');
	}

	public function webcam_based_assessment_system()
	{

		$this->load->view('web/webcam_based_assessment_system');
	}

	public function examination_management_system()
	{

		$this->load->view('web/examination_management_system');
	}

	public function fee_and_account_management()
	{

		$this->load->view('web/fee_and_account_management');
	}

	public function report_and_analytics()
	{

		$this->load->view('web/report_and_analytics');
	}

	public function student_management_system()
	{

		$this->load->view('web/student_management_system');
	}

	public function terms_of_service()
	{

		$this->load->view('web/terms_of_service');
	}

	public function privacy_policy()
	{

		$this->load->view('web/privacy_policy');
	}

	public function help()
	{

		$this->load->view('web/help');
	}

	public function getting_started()
	{

		$this->load->view('web/getting_started');
	}
	public function add_syllabus()
	{

		$this->load->view('web/add_syllabus');
	}
	public function add_exam()
	{

		$this->load->view('web/add_exam');
	}
	public function upload_video()
	{

		$this->load->view('web/upload_video');
	}
	public function add_unit()
	{

		$this->load->view('web/add_unit');
	}

	public function tour()
	{

		$this->load->view('web/tour');
	}

	public function mailer()
	{
		if ($_POST) {
			$msg = '';
			$firstname = $this->input->post('firstName');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$institutional = $this->input->post('institutional');
			$date_time = $this->input->post('date_time');
			$msg .= "Name -" .$firstname."\r\n";
			$msg .="E-mail  -" . $email."\r\n";
			$msg .="phone  -" . $phone."\r\n";
			$msg .="institutional  -" . $institutional."\r\n";
			$msg .="date_time  -" . $date_time."\r\n";

			$headers = "From: support@edgetechnosoft.com" . "\r\n" ."CC: anjum@edgetechnosoft.com, faiz@edgetechnosoft.com";

			mail('support@edgetechnosoft.com' ,' New Lead from E-campus',$msg,$headers);

			$this->session->set_flashdata('success','Mail Send Sucessfully');
           	redirect("https://e-campus.in");
		}
	}


	public function mailerforcontact()
	{
		if ($_POST) {
	
			$msg = '';
			$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			$e_mail = $this->input->post('e_mail');
			$mobile = $this->input->post('mobile');
			$insti_tution = $this->input->post('insti_tution');
			$requirement = $this->input->post('requirement');
			$msg .= "Name -" .$fname."\r\n";
			$msg .= "last name -" .$lname."\r\n";
			$msg .="E-mail  -" . $e_mail."\r\n";
			$msg .="phone  -" . $mobile."\r\n";
			$msg .="institutional  -" . $insti_tution."\r\n";
			$msg .="requirement  -" . $requirement."\r\n";

			$headers = "From: support@edgetechnosoft.com" . "\r\n" ."CC: raushan@edgetechnosoft.com, rahul@edgetechnosoft.com";

			mail('raushan@edgetechnosoft.com' ,' New Lead from E-campus contact us from',$msg,$headers);

			$this->session->set_flashdata('success','Mail Send Sucessfully');
           	redirect("https://e-campus.in");
		}
	}

	public function page()
	{
		$data['title'] = 'Checkout payment | E-Campus';  
       	$data['return_url'] = base_url().'Razorpay/callback';
       	$data['surl'] = base_url().'Razorpay/success';
       	$data['furl'] = base_url().'Razorpay/failed';
       	$data['currency_code'] = 'INR';

		$this->load->view('web/page',$data);
	}
}




	
