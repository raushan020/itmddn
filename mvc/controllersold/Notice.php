<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Notice extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("notice_m");
		$this->load->model("subject_m");
		$this->load->model("alert_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$this->load->model("classes_m");
		$this->load->model("student_info_m");
		$this->load->model("professor_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('notice', $language);	

	}



	public function index() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");
		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
		if($usertype == 'ClgAdmin' || $usertype == 'Admin') {

			$year = date("Y");

			// $this->data['notices'] = $this->notice_m->get_order_by_notice($adminID);

			if ($this->input->post('classesID')) {
				$this->session->unset_userdata('FilterNoticesubCourseID');
				$this->session->unset_userdata('FilterNoticeyearSemesterID');
				$this->session->set_userdata('FilterNoticeclassesID', $this->input->post('classesID'));
			}

	        if ($this->input->post('subCourseID')) {
				$this->session->set_userdata('FilterNoticesubCourseID', $this->input->post('subCourseID'));
			}

		  	if ($this->input->post('yearSemesterID')) {
				$this->session->set_userdata('FilterNoticeyearSemesterID', $this->input->post('yearSemesterID'));
			}


			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterNoticeclassesID'));


			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterNoticeclassesID'));

			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);
	
		}elseif ($usertype == "Accountant"){
					$year = date("Y");
 
 		/*=> $userdata->userID,*/
			$usertype = $this->session->userdata("usertype");
			$adminID = $this->session->userdata("loginuserID");

			$this->data['notices'] = $this->notice_m->get_user_notice($adminID);
			

			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);
		}elseif ($usertype == "Professor"){
			$year = date("Y");
			$usertype = $this->session->userdata("usertype");
			$userID = $this->session->userdata("loginuserID");

			$this->data['notices'] = $this->notice_m->get_user_notice($userID); 

			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);
		
		}elseif ($usertype == "Student") {

			$student = $this->student_info_m->get_student_info(); 

			
			$year = date("Y");
			$callAllNotice = $this->notice_m->notic_record(array('studentID'=>$student->studentID));
			 foreach ($callAllNotice as $key => $value) {

				if($value['usertype']=='Professor'){

				 	$callAllNotice[$key]['name'] ="Professor" ;
					$callAllNotice[$key]['bysend'] = "Professor";

				}elseif($value['usertype']=='ClgAdmin'){
					$callAllNotice[$key]['name'] = "Super Admin";
					$callAllNotice[$key]['bysend'] = "Super Admin";	
				}elseif ($value['usertype']=='Accountant') {
					 
					$callAllNotice[$key]['name'] = "Accountant";
					$callAllNotice[$key]['bysend'] = "Accountant";
				}elseif ($value['usertype']=='Admin') {
					$name = $this->notice_m->notice_byname('user',$value['userID'],'userID');
					$callAllNotice[$key]['name'] = $name->name;
					$callAllNotice[$key]['bysend'] = "Admin";
				}
			}	

			$this->data['notice']  = $callAllNotice;	


			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);

		}else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	protected function rules() {

		$rules = array(
				
				array(

					'field' => 'title', 

					'label' => $this->lang->line("notice_title"), 

					'rules' => 'trim|required|xss_clean|max_length[128]'

				), 

				array(

					'field' => 'date', 

					'label' => $this->lang->line("notice_date"),

					'rules' => 'trim|required|max_length[10]|xss_clean'

				),

				array(

					'field' => 'classesID[]', 

					'label' => 'Course',

					'rules' => 'xss_clean|callback_courseWith_semester',

				)


			);

		return $rules;

	}



	public function add() {
		// print_r($this->data["siteinfos"]->email);die;
		date_default_timezone_set('Asia/Kolkata');
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$year = date("Y");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin' || $usertype == 'Professor' || $usertype == 'Accountant') 
		{

		    $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			if($_POST) 
			{

		        $rules = $this->rules();
				$this->form_validation->set_rules($rules);
				if ($this->form_validation->run() == FALSE) 
				{
					$this->data['form_validation'] = validation_errors();
					$this->data["subview"] = "notice/add";
					$this->load->view('_layout_main', $this->data);			
				} 
				else 
				{
					$subCourseID  = $this->input->post('sub_coursesID');
					// $yearSemesterID = $this->input->post('yearOrSemester');
					$num = 1;
					$config['upload_path'] = "./uploads/notice";
					$config['allowed_types'] = "gif|jpg|png|pdf|docx|doc|xls|xlsx|txt";
					$config['encrypt_name'] = TRUE;
					$config['max_size'] = '5120';
					$config['max_width'] = '3000';
					$config['max_height'] = '3000';
					$this->load->library('upload', $config);
					$this->upload->do_upload();
					$file_name = $this->upload->data();
					$array_notice = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"create_date" => date('Y-m-d H:i:s'),
						"status" => $num,
						'usertype' => $usertype,
						'noticeDocument' => $file_name['file_name'],
						'userID'=> $loginuserID
					);
					$noticeID = $this->notice_m->insert_notice($array_notice);
					$classesID  = $this->input->post('classesID');
					$coursemixyear = '';
					$class_count = count($classesID);
					for ($i=0; $i < count($classesID); $i++) 
					{
    					$semesterOryear = $this->input->post('yearOrSemester'.$classesID[$i]);
    					$array['classesID'] = $classesID[$i];
    					$array['yearsOrSemester'] = implode(',', $semesterOryear);
    					$array['noticeID'] = $noticeID;
    					//$this->notice_m->notice_x_classes($array_clases, $i, $class_count , $classesID);
    					$this->db->where('noticeID',$array['noticeID']);
    					$this->db->where('classesID',$array['classesID']);
    					$num = $this->db->get('notice_x_classes')->num_rows();
    					if($num==0)
    					{
    					 	$this->db->insert('notice_x_classes',$array);
    					}
    					else
    					{
                            $this->db->where('noticeID',$array['noticeID']);
                            $this->db->where('classesID',$array['classesID']);	
                            $this->db->update('notice_x_classes',$array);	
    					}
    					$count_one = $class_count-1;   
    					if($count_one==$i)
    					{
        					$this->db->where('noticeID',$array['noticeID']);
        					$this->db->where_not_in('classesID',$classesID);
        					$this->db->delete('notice_x_classes');	
    					}
    					// for student
                        $this->db->select('phone,studentID,email,name');
                        $this->db->where('classesID',$classesID[$i]);
                        $this->db->where('status',1);
                        $this->db->where_in('yearsOrSemester',$semesterOryear);
                        $object = $this->db->get('student')->result();
    					if($object)
    					{
    					 	foreach ($object as $key => $value) 
    					 	{
								$student_email[] = $value->email;
    					 	    $student_phone[] = $value->phone;
            					$studentID_array[] = $value->studentID; 
            					$studentname[]=$value->name;		
            					$this->db->where('studentID',$value->studentID);
            					$this->db->where('noticeID',$noticeID);
            					$num = $this->db->get('notice_x_student')->num_rows();
            					if($num==0)
            					{
        					 	 	$data = array(
            					 	    'studentID' =>$value->studentID,
            					 	    'noticeID'=>$noticeID,
            					 	    'sendByUSer'=>$this->session->userdata('usertype'),
            					 	    'sendByUSerName'=>$this->session->userdata('name')
        					 	 	);
        					 	 	$this->db->insert('notice_x_student',$data);
            					}
    					    }
                        	
        					$count_one = $class_count-1;   
        					if($count_one==$i)
        					{
            					$this->db->where('noticeID', $noticeID);
            					$this->db->where_not_in('studentID',$studentID_array); 
            					$this->db->delete('notice_x_student');	
    					    }
    					}
					}
					$message = "You have got a new notification \n"."Title: ".$this->input->post("title")."\nDate:".date("Y-m-d", strtotime($this->input->post("date")))."\nSubject: ".strip_tags(html_entity_decode($this->input->post("notice")))."\nLog in to your dashboard to check in details. \n Happy Learning!";
                	$requestParams = array(
                		'user' => 'academicedge',
                		'pass' => 'bbz10@123',
                		'sender' => 'EDGSIS',
                		'phone' => trim(json_encode($student_phone, JSON_NUMERIC_CHECK),'[]'),
                		'text' => $message,
                		'priority' => 'ndnd',
                		'stype' => 'normal'
                	);
                	$apiUrl = "http://bhashsms.com/api/sendmsg.php?";
                	foreach($requestParams as $key => $val){
                		$apiUrl .= $key.'='.urlencode($val).'&';
                	}
                
                	$apiUrl = rtrim($apiUrl, "&");
                	$ch = curl_init();
                	curl_setopt($ch, CURLOPT_URL, $apiUrl);
                	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                	$result = curl_exec($ch);
                	curl_close($ch);
                	$return = json_encode($result);

                	$setting = $this->setting_m->get_setting_byAdmin($adminID);
                	$getadmin=$this->professor_m->getsuperadminid($adminID);
		        	$institute=$setting->sname;
		        	if($getadmin)
		        	{
		        		$superadminemail=$getadmin->email;

		        	}
		        	$getemail=$this->professor_m->getadminid($loginuserID);
		        	$adminemail="";
		        	if($getemail)
		        	{
		        		$adminemail=$getemail->email;
		        	}
					
					$dataEamil['title'] = $this->input->post("title");
					$dataEamil['date'] = date("Y-m-d", strtotime($this->input->post("date")));
					$dataEamil['description'] = strip_tags(html_entity_decode($this->input->post("notice")));
					$dataEamil['institute']=$institute;
	        		$dataEamil['logo']=$setting->photo;
	        		$dataEamil['superadminemail']=$superadminemail;
            		$dataEamil['adminemail']=$adminemail;
					// echo $this->data["siteinfos"]->email." ".$this->data['siteinfos']->sname;die;
					foreach($student_email as $stu_email)
					{
                        $subject="Student Notification E-campus";
					    $html = $this->load->view('emailTemplates/studentaddition/noticeEmailer', $dataEamil , true);
					    $email=$stu_email;
					    $fullname="demo";
					    $sendmail=emailBySendGrid($email,$fullname,$subject,$html,$dataEamil);

						
					}
					if($sendmail == true)
					{
						$this->session->set_flashdata('success', $this->lang->line('menu_success'));
						redirect(base_url("notice/index"));
					}
					else
					{
						$this->data["subview"] = "error";
						$this->load->view('_layout_main', $this->data);
					}
				}
			} 
			else 
			{
				$this->data["subview"] = "notice/add";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function edit() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$loginuserID = $this->session->userdata("loginuserID");


		$year = date("Y");

		if($usertype == "Admin" ||  $usertype == 'ClgAdmin' || $usertype == 'Professor' || $usertype == 'Accountant') {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);
			if((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
				$this->data['notice_x_classes'] = $this->notice_m->get_classes_by_notice($this->data['notice']->noticeID);
				
					if($_POST) {

$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors();

					$this->data["subview"] = "notice/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
					$subCourseID  = $this->input->post('sub_coursesID');
					// $yearSemesterID = $this->input->post('yearOrSemester');
					$num = 1;



					$array_notice = array(
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num,
						'usertype' => $usertype,
						'userID'=> $loginuserID
					);

if($_FILES['userfile']['name']!=''){
	if ($this->data['notice']->noticeDocument!='') {
		unlink(FCPATH.'uploads/notice/'.$this->data['notice']->noticeDocument);
	}

				    $config['upload_path'] = "./uploads/notice";
					$config['allowed_types'] = "gif|jpg|png|pdf|jpeg";
					$config['encrypt_name'] = TRUE;
					$config['max_size'] = '5120';
					$config['max_width'] = '3000';
					$config['max_height'] = '3000';
					$this->load->library('upload', $config);
					$this->upload->do_upload();
					$file_name = $this->upload->data();
					$array_notice['noticeDocument'] = $file_name['file_name'];
}

$this->notice_m->update_notice($array_notice, $id);
$noticeID = $id;


$classesID  = $this->input->post('classesID');
$coursemixyear = '';
 $class_count = count($classesID);
for ($i=0; $i < count($classesID); $i++) { 


$semesterOryear = $this->input->post('yearOrSemester'.$classesID[$i]);
					$array['classesID'] = $classesID[$i];
					$array['yearsOrSemester'] = implode(',', $semesterOryear);
					$array['noticeID'] = $noticeID;
					//$this->notice_m->notice_x_classes($array_clases, $i, $class_count , $classesID);
$this->db->where('noticeID',$array['noticeID']);
$this->db->where('classesID',$array['classesID']);
$num = $this->db->get('notice_x_classes')->num_rows();

if($num==0){
 	 	$this->db->insert('notice_x_classes',$array);
 }else{
 $this->db->where('noticeID',$array['noticeID']);
 $this->db->where('classesID',$array['classesID']);	
 $this->db->update('notice_x_classes',$array);	
 }
$count_one = $class_count-1;   
if($count_one==$i){
$this->db->where('noticeID',$array['noticeID']);
$this->db->where_not_in('classesID',$classesID);
$this->db->delete('notice_x_classes');	
}
// for student
       $this->db->select('studentID');
       $this->db->where('classesID',$classesID[$i]);
        $this->db->where('status',1);
       $this->db->where_in('yearsOrSemester',$semesterOryear);
 	   $object = $this->db->get('student')->result();
if($object){
 	foreach ($object as $key => $value) {
$studentID_array[] = $value->studentID; 		
$this->db->where('studentID',$value->studentID);
$this->db->where('noticeID',$noticeID);
$num = $this->db->get('notice_x_student')->num_rows();
if($num==0){
 	 	$data = array(
 	    'studentID' =>$value->studentID,
 	    'noticeID'=>$noticeID,
 	    'sendByUSer'=>$this->session->userdata('usertype'),
 	    'sendByUSerName'=>$this->session->userdata('name')
 	 	);
 	 	$this->db->insert('notice_x_student',$data);
 }

 	 }
$count_one = $class_count-1;   
if($count_one==$i){
$this->db->where('noticeID', $noticeID);
$this->db->where_not_in('studentID',$studentID_array);
$this->db->delete('notice_x_student');	
}
}

}
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					// print_r($array);
					// exit();

					redirect(base_url("notice/index"));

				}

					} else {

						$this->data["subview"] = "notice/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}




	public function send_notice_email()
	{
 

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");
		
		$year = date("Y");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin') {

		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

		$this->data['classes'] = $this->notice_m->get_notice_limit($adminID);

			if($_POST) {
			
				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors();

					$this->data["subview"] = "notice/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
					
					$classesID  = $this->input->post('classesID');
					$subCourseID  = $this->input->post('sub_coursesID');
					$yearSemesterID = $this->input->post('yearsOrSemester');
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						'classesID' => $classesID, 
						'sub_coursesID' => $subCourseID,
						'yearsOrSemester' => $yearSemesterID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num,
						

					);

				
					$noticeID = $this->notice_m->insert_notice($array);

					$array['notice_type'] = $this->input->post("notice_type");

					$array['noticeID'] = $noticeID;

					           $this->notice_m->notice_x_email_student($array);
					           foreach ($object as $name => $address)
								{
								        $this->email->clear();

								        $this->email->to($address);
								        $this->email->from('your@example.com');
								        $this->email->subject('Here is your info '.$name);
								        $this->email->message('Hi '.$name.' Here is the info you requested.');
								        $this->email->send();
								}

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));


					redirect(base_url("notice/index"));

				}

			} else {

				$this->data["subview"] = "notice/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}
	}

	
	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->notice_m->get_join_notice_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->notice_m->make_datatables($adminID);

            // $totalFiltered  = $this->student_m->get_filtered_data();
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['title'] = $post->title;
				$nestedData['date'] = date('d-m-Y',strtotime($post->noticeDate));
                $nestedData['notice'] = substr($post->notice,0,20).'...';

   			if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == 'Professor' || $usertype == 'Accountant') {
   		    $buttons     = btn_view('notice/view/'.$post->noticeID, $this->lang->line('view')).btn_edit('notice/edit/'.$post->noticeID, $this->lang->line('edit')).btn_delete('notice/delete/'.$post->noticeID, $this->lang->line('delete'));
 		

			    $nestedData['action'] = $buttons;
			}
               
                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

      
            
        echo json_encode($json_data); 
}


	

	public function view() 
	{
		// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		$id=$this->uri->segment(3);
		if((int)$id) 
		{
			$usertype = $this->session->userdata("usertype");
			if($usertype == "ClgAdmin" || $usertype == "Admin" || $usertype == "Accountant" || $usertype == "Professor") 
			{ 
				$this->data['notice'] = $this->notice_m->get_notice($id);
				if($this->data['notice']!=null)
				{
					$this->data['sender'] = $this->notice_m->sender_data($this->data['notice']->userID,$this->data['notice']->usertype);
					if($this->data['notice']) 
					{
						$this->data["subview"] = "notice/view";
						$this->load->view('_layout_main', $this->data);
					}
				}
				else 
				{
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			}
			else if($usertype=='Student')
			{
				$this->data['notice'] = $this->notice_m->notice_single($this->session->userdata('loginuserID'),$id);
				$this->data['sender'] = $this->notice_m->sender_data($this->data['notice']->userID,$this->data['notice']->usertype);  
					
					$this->notice_m->update_notice_x_student_read($this->session->userdata('loginuserID'),$id);
					$this->data["subview"] = "notice/view";
					$this->load->view('_layout_main', $this->data);
				/*if($this->data['notice']!=null)
				{
					
				} 
				else 
				{
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}*/
			}
		}   
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function hide_Notice_icon(){		
$data = array( 
'iconStatus'=>0
);
	$this->db->where('studentID',$this->session->userdata('loginuserID'));
	$this->db->update('notice_x_student',$data);
}

	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Professor") {

			// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$id=$this->uri->segment(3);

			if((int)$id) {

				$this->notice_m->delete_notice($id);
				$this->notice_m->delete_notice_x_student($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("notice/index"));

			} else {

				redirect(base_url("notice/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	function date_valid($date) {

		if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);   

	        $dd = $arr[0];            

	        $mm = $arr[1];              

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    } 

	} 



	public function print_preview() {

		// $id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		$id=$this->uri->segment(3);

		if((int)$id) {

			$this->data['notice'] = $this->notice_m->get_notice($id);

			if($this->data['notice']) {



				$this->load->library('html2pdf');

			    $this->html2pdf->folder('./assets/pdfs/');

			    $this->html2pdf->filename('Report.pdf');

			    $this->html2pdf->paper('a4', 'portrait');

			    $this->data['panel_title'] = $this->lang->line('panel_title');

				$html = $this->load->view('notice/print_preview', $this->data, true);

				$this->html2pdf->html($html);

				$this->html2pdf->create();

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}	

	}


function ResetCourses(){

$this->session->unset_userdata('FilterNoticeclassesID');
$this->session->unset_userdata('FilterNoticesubCourseID');
$this->session->unset_userdata('FilterNoticeyearSemesterID');	   
}
function ResetSubcourses(){
$this->session->unset_userdata('FilterNoticesubCourseID');
}

function ResetSemesterYear(){

$this->session->unset_userdata('FilterNoticeyearSemesterID');
	   
}
function ResetAllfilter(){

$this->session->unset_userdata('FilterNoticesubCourseID');
$this->session->unset_userdata('FilterNoticeclassesID');
$this->session->unset_userdata('FilterNoticeyearSemesterID');  
}

	

	function courseWith_semester(){

		if (isset($_POST['classesID'])) {
		   $classes_countID =count($_POST['classesID']);
		   $classesID  =  $_POST['classesID']; 
		}else{
		 $classes_countID =0;
		}

   if($classes_countID==0){

	   	$this->form_validation->set_message("classesID","Please Select Atleast One Course");
	   	$retrun =  false;
   	}else{
   	 	for ($i=0; $i <$classes_countID; $i++) { 

			if (isset($_POST['yearOrSemester'.$classesID[$i]])) {
			   $semesterOryear =count($_POST['yearOrSemester'.$classesID[$i]]);
			}else{
				 $semesterOryear =0;
			}
		    if($semesterOryear==0){
		    	$this->form_validation->set_message("classesID","Please Select Atleast One Semester");
		  		$retrun =  false;
		  	}else{
	
		  		$retrun = true;
			}
		}
	
   }
   return $retrun;

	}

	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" ||  $usertype == 'ClgAdmin') {

			$id = $this->input->post('id');

			if ((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {



					$this->load->library('html2pdf');

				    $this->html2pdf->folder('uploads/report');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('notice/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));	

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function ratingmail(){

		$mail = $this->session->userdata("email");
	  $name = $this->session->userdata('name');

		 
		
		

$angry= $this->input->post('angry');

 
		$this->load->library('email');

		$this->email->from($mail, $name);
		$this->email->to('support@edgetechnosoft.com');


		$this->email->subject($angry);
		$this->email->message($angry);

		$this->email->send();

		echo 'Thanks For Rating';
	}

	

}



