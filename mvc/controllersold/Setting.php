<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class setting extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("setting_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('setting', $language);

	}

	protected function rules() {

		$rules = array(

				array(

					'field' => 'sname',

					'label' => $this->lang->line("setting_school_name"),

					'rules' => 'trim|required|max_length[128]'

				),

				array(

					'field' => 'phone',

					'label' => $this->lang->line("setting_school_phone"),

					'rules' => 'trim|required|max_length[25]'

				),

				array(

					'field' => 'email',

					'label' => $this->lang->line("setting_school_email"),

					'rules' => 'trim|required|valid_email|max_length[40]'

				),

				array(

					'field' => 'footer',

					'label' => $this->lang->line("setting_school_footer"),

					'rules' => 'trim|required|max_length[200]'

				),

				array(

					'field' => 'address',

					'label' => $this->lang->line("setting_school_address"),

					'rules' => 'trim|required|max_length[200]'

				),


			);

		return $rules;

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "ClgAdmin") {

			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));

			if($this->data['setting']) {

				if($_POST) {

					$rules = $this->rules();

					$this->form_validation->set_rules($rules);

					if ($this->form_validation->run() == FALSE) {
					$this->data['form_validation'] = validation_errors();

						$this->data["subview"] = "setting/index";

						$this->load->view('_layout_main', $this->data);

					} else {

				
						$array = array(
						'sname'=>$this->input->post('sname'),
						'phone'=>$this->input->post('phone'),
						'email'=>$this->input->post('email'),
						'footer'=>$this->input->post('footer'),
						'address'=>$this->input->post('address'),
						);

if ($this->input->post('images_data')) {
     $array['photo'] = $this->input->post('images_data');
}
							   $this->setting_m->update_setting($array,$adminID);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("setting/index"));
					
					}

				} else {

					$this->data["subview"] = "setting/index";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


function uploadPic(){
	
	$data = $_POST["image"];
	$image_array_1 = explode(";", $data);
	$image_array_2 = explode(",", $image_array_1[1]);
	$data = base64_decode($image_array_2[1]);
	$imageName = 'site'.time() . '.png';
    $root = 'uploads/images/';
	file_put_contents($root.$imageName, $data);

echo "<input type ='hidden' value = '".$imageName."' name = 'images_data' >";

}

	public function unique_day() {

		$day = $this->input->post('automation');

		if((int)$day) {

			if($day < 0 || $day > 28) {

				$this->form_validation->set_message("unique_day", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$this->form_validation->set_message("unique_day", "%s already exists");

			return FALSE;

		}

	}

	public function pcode_validation($pcode) {

		$purchase_code = $pcode;

		// $purchase_code_username = "sangwh";

		// $purchase_code = "f541d688-9d40-40db-99fb-65e6f80692ab";

	    $username = 'inilabs';

	    $api_key = 'a7hfhirfq8dw64old1bafe2dpimk5zdb';

	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_USERAGENT, 'API');

	    curl_setopt($ch, CURLOPT_URL, "http://marketplace.envato.com/api/edge/". $username ."/". $api_key ."/verify-purchase:". $purchase_code .".json");

	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    $purchase_data = json_decode(curl_exec($ch), true);

	    if(!count($purchase_data['verify-purchase'])) {

	    	$this->form_validation->set_message("pcode_validation", "Your Purchase Code Is Not Valid.");

			return FALSE;

		} else {

			return TRUE;

		}

	}

	public function attendance() {
	
		if($this->input->post('attendance') === "0") {

			$this->form_validation->set_message("attendance", "The %s field is required");

			 	return FALSE;
		}

		return TRUE;

	}



public  function settings_extra(){
	
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		
	    $this->data['department']   =	$this->db->get('deparment')->result();

		if($usertype == "Admin" || $usertype == "ClgAdmin") {

		$this->data["subview"] = "setting/settings_extra";

	     $this->load->view('_layout_main', $this->data);

 
			} else{

	     echo "404 page not found";	
     
			}

		}



}




