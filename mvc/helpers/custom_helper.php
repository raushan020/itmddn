<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('get_notification_super'))
{
	function get_notification_super()
	{
	    $ci =& get_instance();
        $ci->load->database();
	    
	    $ci->load->library('session');
	    
		$usertype = $ci->session->userdata('usertype');
		$ClgAdmin = $ci->session->userdata('adminID');
		$array1s = array();
		
		$ci->db->select('*');
		$ci->db->from('student');
		$where1s = "create_usertype = 'Admin' and notification = '1'";
		$ci->db->where($where1s);
		$ci->db->order_by('s_date','DESC');
		$sql1s = $ci->db->get();
		$result1s = $sql1s->result_array();
		foreach($result1s as $arr1s)
		{
			$array1s[] = array(
								'ids' => 'student_'.$arr1s['studentID'],
								'link' => 'student',
								'photo' => $arr1s['photo'],
								'values' => '<b>'.$arr1s['create_username'].'</b>',
								'msg' => 'added new student <b>'.$arr1s['name'].'</b>',
								'dates' => $arr1s['s_date']
							);
		}
		
		$array2ss = array();
		$ci->db->select('*');
		$ci->db->from('professor');
		$where2ss = "create_usertype = 'Admin' and notification = '1'";
		$ci->db->where($where2ss);
		$ci->db->order_by('s_date','DESC');
		$sql2ss = $ci->db->get();
		$result2ss = $sql2ss->result_array();
		foreach($result2ss as $arr2ss)
		{
			$array2ss[] = array(
								'ids' => 'professor_'.$arr2ss['professorID'],
								'link' => 'professor',
								'photo' => $arr2ss['photo'],
								'values' => '<b>'.$arr2ss['create_username'].'</b>',
								'msg' => 'added new professor <b>'.$arr2ss['name'].'</b>',
								'dates' => $arr2ss['s_date']
							);
		}
		
		$array3sss = array();
		$ci->db->select('*');
		$ci->db->from('classes');
		$where3sss = "create_usertype = 'Admin' and notification = '1'";
		$ci->db->where($where3sss);
		$ci->db->order_by('s_date','DESC');
		$sql3sss = $ci->db->get();
		$result3sss = $sql3sss->result_array();
		foreach($result3sss as $arr3sss)
		{
			$array3sss[] = array(
								'ids' => 'classes_'.$arr3sss['classesID'],
								'link' => 'classes',
								'photo' => '',
								'values' => '<b>'.$arr3sss['create_username'].'</b>',
								'msg' => 'added new course <b>'.$arr3sss['classes'].'</b>',
								'dates' => $arr3sss['s_date']
							);
		}
		
		$array4sss = array();
		$ci->db->select('*');
		$ci->db->from('student');
		$where4sss = "status = '0'";
		$ci->db->where($where4sss);
		$ci->db->order_by('s_date','DESC');
		$sql4sss = $ci->db->get();
		$result4sss = $sql4sss->result_array();
		foreach($result4sss as $row)
		{
		    $ci->db->select('name,photo');
		    $ci->db->from('professor');
    		$where = "professorID = '".$row['update_by']."'";
    		$ci->db->where($where);
    		$sql = $ci->db->get();
		    $re = $sql->result_array();
		    foreach($re as $rom)
		    {
		                    
    			$array4sss[] = array(
    								'ids' => 'student_'.$row['studentID'],
    								'link' => 'student',
									'photo' => $rom['photo'],
    								'values' => '<b>'.$rom['name'].'</b>',
									'msg' => 'removed student <b>'.$row['name'].'</b>',
    								'dates' => $row['update_date']
    							);
		    }
		}
		
		$result = array_merge($array1s,$array2ss,$array3sss,$array4sss);
		return $result;
	} 
}