<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message_m extends Admin_Model {

	protected $_table_name = 'message';
	protected $_primary_key = 'messageID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "messageID desc";

	function __construct() {
		parent::__construct();
	}

	function get_message($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_recivers($single=FALSE, $array=NULL) {
		if ($array) {
			$query = $this->db->get_where($single, $array);
		} else {
			$query = $this->db->get($single);
		}
		return $query->result();
	}

	function get_order_by_message($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}
	function get_trash_message($email, $userID, $usertype) {
		$where = "(email = '$email' AND to_status = 1) OR (usertype = '$usertype' AND userID = $userID AND from_status = 1)";
		$this->db->where($where);
		$query = $this->db->get('message');
		return $query->result();
	}


	function insert_message($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_message($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_message($id){
		parent::delete($id);
	}

	public function counter($array=NULL)
	{
		$query = $this->db->get_where($this->_table_name, $array);
		return count($query->result());
	}
	function fetch_data($query)
	{
		// $arrayName = array('student','professor','user');
	  $this->db->select("name,studentID,usertype");
	  $this->db->from('student');
	  if($query != '')
	  {
	   $this->db->like('student.name', $query);
	   // $this->db->or_like('professor.name', $query);
	   // $this->db->or_like('user.name', $query);
	  }
	  return $this->db->get();
	}
	public function requestsendformsender($arrayName)
	{
		$this->db->insert('messenger_invite',$arrayName);
		return $this->db->insert_id();
	}
	public function showallmessagerequest()
	{
		$loginuserID=$this->session->userdata('loginuserID');
		$userrtype=$this->session->userdata('usertype');
		$this->db->select('*');
		$this->db->from('messenger_invite');
		$this->db->where('requestID',$loginuserID);
		$this->db->where('requestusertype',$userrtype);
		$this->db->where('status',0);
		return $this->db->get()->result();

	}
	public function showallrequestaccept()
	{
		$loginuserID=$this->session->userdata('loginuserID');
		$usertype=$this->session->userdata('usertype');
		$this->db->select('*');
		$this->db->from('messenger_invite');
		$l= array('requestID' =>$loginuserID);
		$u= array('requestusertype'=>$usertype);
		$this->db->where($l);
		$this->db->where($u);
		$this->db->where('status',1);
		return $this->db->get()->result();
	}
	public function showallsenderaccept()
	{
		$loginuserID=$this->session->userdata('loginuserID');
		$usertype=$this->session->userdata('usertype');
		$this->db->select('*');
		$this->db->from('messenger_invite');
		$l= array('SenderID'=>$loginuserID );
		$u= array('senderusertype'=>$usertype);
		$this->db->where($l);
		$this->db->where($u);
		$this->db->where('status',1);
		return $this->db->get()->result();
	}
	public function updatestatusforchat($Id,$usertype,$updatedata)
	{
		$loginuserID=$this->session->userdata('loginuserID');
		$usertypes=$this->session->userdata('usertype');
		$where= array('requestID' =>$loginuserID,'requestusertype'=>$usertypes,'SenderID'=>$Id,'senderusertype'=>$usertype);
		$this->db->where($where);
		$this->db->update('messenger_invite',$updatedata);
		$a=1;
		return $a; 

	}
}

/* End of file message_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/message_m.php */
