<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends Admin_Model {

	protected $_table_name = 'admin';
	protected $_primary_key = 'adminID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "adminID";

	function __construct() {
		parent::__construct();
	}

	function get_username($table, $data=NULL) {
		$query = $this->db->get_where($table, $data);
		return $query->result();
	}

	function updateadminsubscription($billing_invoiceID,$amount){
		$data =array(
			'paidAmount'=>$amount,
			'status'=>1,
			'payment_method'=>'Razor Pay'
		);
		$this->db->where('billing_invoiceID',$billing_invoiceID);
		$this->db->update('billing_invoice',$data);


	}
	function get_single_examiner_detail($array){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where($array);
		$query= $this->db->get();
		 
		return $query->row();
	}
	function nub_ofuser() {
		$query = $this->db->select('count(*)')->get('user')->num_rows();
		return $query;
	}
	
	function user_notification()
	{
	    $query = $this->db->query('select create_usertype, notification, notification_date from user where status = 1 and notification = "1" and notification_date = "'.date('Y-m-d').'"');
    	if($query->num_rows() > 0)
    	{
    		return $query->result();
    	}
    	else
    	{
    		echo "";
    	}
	}

	// function get_systemadminname_row($table, $data=NULL) {
	// 	$query = $this->db->get_where($table, $data);
	// 	return $query->row();
	// }

	function get_systemadmin($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}


	function get_order_by_systemadmin($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

function get_admin_data($array){
	   
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where($array);
		$query= $this->db->get();
		 
		return $query->row();

}

 

	function get_single_systemadmin($array) {
 
		$query = parent::get_single($array); 
// print_r($query);
// exit();
		return $query;
	}

	function insert_systemadmin($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_systemadmin($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	function delete_systemadmin($id){
		parent::delete($id);
	}

	function hash($string) {
		return parent::hash($string);
	}	
	function adminsubscription($subscription)
	{
		$this->db->insert('adminsubscription',$subscription);
		$a=$this->db->insert_id();
		return $a;
	}
}

/* End of file systemadmin_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/systemadmin_m.php */