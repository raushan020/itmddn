<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Online_reader_m extends Admin_Model {

	protected $_table_name = 'student';

	protected $_primary_key = 'studentID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "roll asc";


	function __construct() {

		parent::__construct();

	}


	public function getonlinereaderpass($loginuserID,$id){
		
		
		$this->db->where('epubid',$id);
		$this->db->where('studentID',$loginuserID);
		$rowdata = $this->db->get('onlineReader_status')->row();
		
if ($rowdata) {

		$timeArray = explode(',',$rowdata->individual_time);
		$wordArray = explode(',',$rowdata->words_count);
		$percentage = 0;
		//print_r($timeArray);
		//exit();
		for ($i=0; $i < count($timeArray); $i++) { 
			for ($j=0; $j < count($wordArray); $j++) { 
				if ($i==$j) {

					$totalTime = $wordArray[$j]*0.1;
					if ($totalTime == 0) {
						$percentage += 0;
					}else{
						$percentage +=  ($timeArray[$i]*100)/$totalTime;
					}
				}
			}
		}


		$realActivity = round($percentage)/$rowdata->chapter_count;
return round($realActivity);
		
	}else{
	return 0;	
	}
}


	public function getdetail_validation_epub($subjectID,$uri4){
		// print_r($uri4);
		// print_r($subjectID);
		// exit();
		$this->db->where('subjectID',$subjectID);
		$this->db->where('id',$uri4);
		
		return $this->db->get('epub',1)->row();
	}


	function get_join_subject_count($adminID) {		

		if(isset($_POST["search"]["value"]))  

        {  

 		$searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 



		$this->db->select('*');



		$this->db->from('subject');



		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');





		if ($this->session->userdata('FilteronlinereaderclassesID')) {

		 $this->db->where('subject.classesID', $this->session->userdata('FilteronlinereaderclassesID'));

		}



		if ($this->session->userdata('FilteronlinereadersubCourseID')) {

		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilteronlinereadersubCourseID'));

		}



		if ($this->session->userdata('FilteronlinereaderyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilteronlinereaderyearSemesterID'));

		}



	     if ($this->session->userdata('ActiveSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('ActiveSubjects'));

		}



	   if ($this->session->userdata('DraftSubjects')) {

		 $this->db->where('subject.status', 0);

		}



	   if ($this->session->userdata('TrashSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('TrashSubjects'));

		}	

		if (empty($this->session->userdata('DraftSubjects')) && empty($this->session->userdata('TrashSubjects'))) {
			
           $this->db->where('subject.status',1);
        }


		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.adminID', $adminID);



		$query = $this->db->get();



		return $query->num_rows();



	}


		function GetSubjectBySyllabus($array){

		$adminID = $this->session->userdata("adminID");
		$this->db->from('units');
		$this->db->where($array);
		return $this->db->get()->result_array();

	}



	function get_join_subject() {



		$adminID = $this->session->userdata("adminID");

		$this->db->select('subject.*,classes.*,sub_courses.*,subject.status as studentStatus');

		$this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		if ($this->session->userdata('FilteronlinereaderclassesID')) {

		 $this->db->where('subject.classesID', $this->session->userdata('FilteronlinereaderclassesID'));

		}

		if ($this->session->userdata('FilteronlinereadersubCourseID')) {

		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilteronlinereadersubCourseID'));

		}

		if ($this->session->userdata('FilteronlinereaderyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilteronlinereaderyearSemesterID'));

		}



	   if ($this->session->userdata('ActiveSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('ActiveSubjects'));

		}



	   if ($this->session->userdata('DraftSubjects')) {

		 $this->db->where('subject.status', 0);

		}



	   if ($this->session->userdata('TrashSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('TrashSubjects'));

		}	
		if (empty($this->session->userdata('DraftSubjects')) && empty($this->session->userdata('TrashSubjects'))) {
           $this->db->where('subject.status',1);
        }




		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.adminID', $adminID);





if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('subject.subject_code', 'ASC');  

           } 

           if (isset($_POST["length"])) {

  

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       }



	}



	function make_datatables($adminID){  

           $this->get_join_subject();  

           $query = $this->db->get();  

           return $query->result();  

      } 



	function get_join_where_subject($classesID,$sub_coursesID,$id,$yearsOrSemester=Null) {

        if($id != 0)
        {
             $loginuserID = $id;
        }
        else
        {
             $loginuserID = $this->session->userdata('loginuserID');
        }
		
		$this->db->select('yearsOrSemester');
		$this->db->from('student');
		$this->db->where('studentID',$loginuserID);
		$a=$this->db->get()->row();
       // print_r($a->yearsOrSemester);die;

		$this->db->select('subject.*,sylabus_pdf.subjectID as otherID,sylabus_pdf.pdf,notes_pdf.pdf as assignmentpdf');

		$this->db->from('subject');

		$this->db->join('sylabus_pdf', 'sylabus_pdf.subjectID = subject.subjectID', 'LEFT');
		$this->db->join('notes_pdf','notes_pdf.subjectID=subject.subjectID','LEFT');
		// $this->db->join('optionalsubject','optionalsubject.subjectID=subject.subjectID','left');


	if ($this->session->userdata('FilterSubjectyearSemesterID'))
	{
		if($a->yearsOrSemester >= $this->session->userdata('FilterSubjectyearSemesterID'))
		{
			$this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));
		}
		else
		{
			$this->db->where('subject.yearsOrSemester',"null");
		}
	}
	else
	{
		if($yearsOrSemester)
		{
			if($a->yearsOrSemester >= $yearsOrSemester)
			{
				$this->db->where('subject.yearsOrSemester', $yearsOrSemester);
			}
			else
			{
				$this->db->where('subject.yearsOrSemester',"null");
			}
		}
		else
		{
			$this->db->where('subject.yearsOrSemester',$a->yearsOrSemester);
		}
	}


		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->where("subject.status",1);
		$this->db->where("subject.subject_mode",0);
		$this->db->group_by('subject.subjectID');
		$this->db->order_by('subject.subject_code', 'DESC');  

		// if("subject.subject_mode"==1)
		// {
		// 	print_r("expression");die;

		// }

		$query = $this->db->get();
		// print_r($query->result_array());die;
		return $query->result_array();

		// foreach ($assignsubject as $key => $value)
		// {
		// 	$this->db->select('o.subjectID,s.subject');
		// 	$this->db->from('optionalsubject as o');
		// 	$this->db->join('subject as s','s.subjectID=o.subjectID','left');
		// 	$this->db->where('o.studentID',$loginuserID);
		// 	$this->db->where('o.subjectID',$value['subjectID']);
		// 	$assignsubject[$key]['optionalsubject']= $this->db->get()->result();
		// }
		// return $assignsubject;



	}
	function showoptionalsubjectbystudent($classesID,$sub_coursesID,$id)
	{
           if($id != 0)
        {
             $loginuserID = $id;
        }
        else
        {
             $loginuserID = $this->session->userdata('loginuserID');
        }
		
		$this->db->select('yearsOrSemester');
		$this->db->from('student');
		$this->db->where('studentID',$loginuserID);
		$a=$this->db->get()->row();
       // print_r($a->yearsOrSemester);die;

		$this->db->select('subject.*,sylabus_pdf.subjectID as otherID,sylabus_pdf.pdf,optionalsubject.subjectID as optionalsubject,notes_pdf.pdf as assignmentpdf');

		$this->db->from('optionalsubject');

		$this->db->join('sylabus_pdf', 'sylabus_pdf.subjectID = optionalsubject.subjectID', 'LEFT');
		$this->db->join('subject','subject.subjectID=optionalsubject.subjectID','left');
        $this->db->join('notes_pdf','notes_pdf.subjectID=optionalsubject.subjectID','LEFT');

	if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}
		else
		{
			$this->db->where('subject.yearsOrSemester',$a->yearsOrSemester);
		}


		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->where("subject.status",1);
		$this->db->where("subject.subject_mode",1);
		$this->db->where("optionalsubject.studentID",$loginuserID);
		$this->db->group_by('subject.subjectID');
		$this->db->order_by('subject.subject_code', 'DESC');  

		// if("subject.subject_mode"==1)
		// {
		// 	print_r("expression");die;

		// }

		$query = $this->db->get();
		// print_r($query->result_array());die;
		return $query->result_array();
	}

function attd_percent($subjectID,$loginuserID){

$this->db->select('count(subjectID) as num_attd, sum(atd) as present');
$this->db->where('subjectID',$subjectID);
$this->db->where('studentID',$loginuserID);
// $this->db->group_by('atd_date');

return $this->db->get('attendance_student')->row();

}

	function get_join_where_subject_count($loginuserID,$classesID,$sub_coursesID) {


$this->db->select('yearsOrSemester');
		$this->db->from('student');
		$this->db->where('studentID',$loginuserID);
		$a=$this->db->get()->row();

		$this->db->select('*');



		$this->db->from('subject');



		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->where('subject.yearsOrSemester',$a->yearsOrSemester);

		$query = $this->db->get();



		return $query->result();



	}



	function get_classes() {



		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');



		$query = $this->db->get();



		return $query->result();



	}
}