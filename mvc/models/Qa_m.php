<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Qa_m extends Admin_Model {



	protected $_table_name = 'subject';

	protected $_primary_key = 'subjectID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "classesID asc";



	function __construct() {

		parent::__construct();

	}


		function getPaginationrowvideo() {

		$usertype = $this->session->userdata('usertype');
		$this->db->from('qa');

		// if($this->session->userdata('FilterVideoclassesID')){
		// 	$this->db->where('subject.classesID',$this->session->userdata('FilterVideoclassesID'));
		// }
		// if($this->session->userdata('FilterVideoyearSemesterID')){
		// 	$this->db->where('subject.yearsOrSemester',$this->session->userdata('FilterVideoyearSemesterID'));
		// }
  //     	if($this->session->userdata('FilterVideosubject1ID')){
		// 	$this->db->where('subject.subjectID',$this->session->userdata('FilterVideosubject1ID'));
		// }
// if($usertype=='Professor'){
// 		$this->db->where('videos.professorID',$this->session->userdata('loginuserID'));
//  }
		$query = $this->db->get();
		return $query->num_rows();

		

	}


function get_join_where_subject_video($limit, $start,$usertype=null,$loginuserID=null) {
if($usertype && $loginuserID)
{
	$usertype =    $usertype;
	$loginuserID = $loginuserID;
}
else
{
	$usertype =    $this->session->userdata('usertype');
	$loginuserID = $this->session->userdata('loginuserID');
}


 if($usertype=='Student'){
  $student_info = $this->db->select('classesID,sub_coursesID,yearsOrSemester,studentID')->where('studentID',$loginuserID)->get('student')->row();
  }

		$this->db->from('qa');
		$this->db->join('subject', 'subject.subjectID = qa.subjectID', 'left');
		$this->db->join('units', 'qa.unitID = units.unitID', 'left');
// 		if($this->session->userdata('FilterVideoclassesID')){
// 			$this->db->where('subject.classesID',$this->session->userdata('FilterVideoclassesID'));
// 		}
// 		if($this->session->userdata('FilterVideoyearSemesterID')){
// 			$this->db->where('subject.yearsOrSemester',$this->session->userdata('FilterVideoyearSemesterID'));
// 		}
//       	if($this->session->userdata('FilterVideosubject1ID')){
// 			$this->db->where('subject.subjectID',$this->session->userdata('FilterVideosubject1ID'));
// 		}
// if($usertype=='Professor'){
// 		$this->db->where('videos.professorID',$this->session->userdata('loginuserID'));
//  }
		// $this->db->where('subject.classesID',$this->session->userdata('FilterVideoclassesID'));
		// $this->db->where('subject.yearsOrSemester',$this->session->userdata('FilterVideoyearSemesterID'));

 if($usertype=='Student'){
		$this->db->where('subject.classesID',$student_info->classesID);
        $this->db->where('subject.yearsOrSemester',$student_info->yearsOrSemester);
}
 if($usertype=='Professor'){
		$this->db->where('subject.professorID',$loginuserID);
}
		$this->db->order_by('qa.qaID','desc');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		$setarray = $query->result();
	
		// echo "<pre>";
		// print_r($setarray);die;
		return $setarray;
		

	}

	function get_join_qa_count($adminID) {

		$this->db->select('qa.que,subject.subject,units.unit_name,student.name');

		$this->db->from('qa');

		$this->db->join('subject', 'subject.subjectID = qa.subjectID', 'LEFT');

		$this->db->join('units', 'units.unitID = qa.unitID', 'LEFT');

		$this->db->join('student','student.studentID = qa.CreateduserID', 'LEFT');


		// if ($this->session->userdata('FilterNoticeclassesID')) {
		//  $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		// }
		$this->db->order_by('qa.createTime','DESC');

		$query = $this->db->get();

		return $query->num_rows();

	}
function make_datatables($adminID){  
           $this->get_join_notice();  
           $query = $this->db->get();  
           return $query->result();  
      } 
      function get_join_notice() {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('qa.que,qa.createTime,subject.subject,units.unit_name,student.name');
		$this->db->from('qa');

		$this->db->join('subject', 'subject.subjectID = qa.subjectID', 'LEFT');

		$this->db->join('units', 'units.unitID = qa.unitID', 'LEFT');

		$this->db->join('student','student.studentID = qa.CreateduserID', 'LEFT');


		// if ($this->session->userdata('FilterNoticeclassesID')) {
		//  $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		// }


if(isset($_POST["search"]["value"]))  
           {  
 $searches  =  $_POST["search"]["value"];

		$order_column = array( 

                            0 =>'sn', 
                            1 =>'subject',
                            2 =>'unit_name',
                            3 => 'name',
                            4 => 'que',
                            
                            
                            
                        );
           $where = "(qa.que LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR units.unit_name LIKE '%$searches%' OR student.name LIKE '%$searches%')";
           $this->db->where($where);
           } 
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                // $this->db->order_by('notice.noticeID', 'DESC');  
           	$this->db->order_by('qa.createTime','DESC');
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       }

	}


}