<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Etsexam_m extends Admin_Model {
	protected $_table_name = 'ets_quiz';
	protected $_primary_key = 'quid';
	protected $_primary_filter = 'intval';
	protected $_order_by = "quiz_name asc";
	function __construct() {
		parent::__construct();
	}
function get_quiz($quid){
    $loginuserID = $this->session->userdata("loginuserID");
    $usertype = $this->session->userdata("usertype");
if($usertype=='Student'){
    $this->db->select('classesID,sub_coursesID,yearsOrSemester');
    $this->db->where('studentID',$loginuserID);
    $queryStuddent   =  $this->db->get('student');
    $row           =    $queryStuddent->row();
   $this->db->where('classesID',$row->classesID);
   $this->db->where('sub_coursesID',$row->sub_coursesID);
if ($row->yearsOrSemester=='y') {
$this->db->where('yearsOrSemester',$row->yearsOrSemester);
}
}
	$this->db->where('quid',$quid);
	$query=$this->db->get('ets_quiz');
	 return $query->row_array();
 }
function update_exam_result($rid){

$data = array(
'result_status'=>'Open'
);
$this->db->where('rid',$rid);
$this->db->update('ets_result',$data);

}

function insert_exam_result($quid,$uid){
	 // get quiz info
	$usertype = $this->session->userdata('usertype');
	  $this->db->where('quid',$quid);
	 $query=$this->db->get('ets_quiz');
	$quiz=$query->row_array();
	
	// get questions	
    	
	$qids_shufel=explode(',',$quiz['qids']);

$set_qids = '';

$this->db->select('type');
$this->db->where('quid',$quid);
$this->db->order_by('ordering','asc');
$check_order = $this->db->get('ets_ordering')->result();

foreach ($check_order as $key => $values) {
$this->db->select('qid');
$this->db->where_in('qid',$qids_shufel);
$this->db->where('question_type',$values->type); 
$quids_from_table = $this->db->get('ets_qbank')->result();

foreach ($quids_from_table as $key2 => $value) {
	if($key2 ==0){
     $set_qids .= $value->qid;
  }else{
  	 $set_qids .= ','.$value->qid;
  }

   }
}




	// shuffle($qids_shufel);
	if($usertype=='Student'){
    //$qids  =  array_slice($qids_shufel,0,50);// add 20 when question  is 70\
	$qids  =	explode(',', $set_qids);
     }else{
    //$qids  =  array_slice($qids_shufel,0);// add 20 when question  is 70
    $qids  =	explode(',', $set_qids);
     }
    $noq = count($qids);

	$categories=array();
	$category_range=array();
	$i=0;
	$wqids=implode(',',$qids);
	// $noq=array();
	// $query=$this->db->query("select * from ets_qbank where qid in ($wqids) ORDER BY FIELD(qid,$wqids)  ");	
	// $questions=$query->result_array();
	$zeros=array();
	 foreach($qids as $qidval){
	 $zeros[]=0;
	 }
	 $userdata=array(
	 'quid'=>$quid,
	 'uid'=>$uid,
	 'r_qids'=>$wqids,
	 'r_noq'=>$noq,
	 'start_time'=>time(),
	 'usertype'=>$usertype,
	 'individual_time'=>implode(',',$zeros),
	 'score_individual'=>implode(',',$zeros),
	 'attempted_ip'=>$_SERVER['REMOTE_ADDR']
	 );
	 // if($this->session->userdata('photoname')){
		//  $photoname=$this->session->userdata('photoname');
		//  $userdata['photo']=$photoname;
	 // }
	 // print_r($userdata);
	 // exit();
	 $this->db->insert('ets_result',$userdata);
	  $rid=$this->db->insert_id();
	return $rid;

}

 function exam_result($rid){
 
	$query=$this->db->query("select * from ets_result join ets_quiz on ets_result.quid=ets_quiz.quid where ets_result.rid='$rid' "); 
	return $query->row_array(); 
	 
 }

function saved_answers($rid){
 
	$query=$this->db->query("select * from ets_answers  where ets_answers.rid='$rid' "); 
	return $query->result_array(); 
	 
 }

 function get_questions($qids){
	 if($qids == ''){
		$qids=0; 
	 }else{
		 $qids=$qids;
	 }

/*
	 if($cid!='0'){
		 $this->db->where('ets_qbank.cid',$cid);
	 }
	 if($lid!='0'){
		 $this->db->where('ets_qbank.lid',$lid);
	 }
*/
	  
	 $query=$this->db->query("select * from ets_qbank where qid in ($qids) order by FIELD(qid,$qids) 
	 ");	
	 return $query->result_array();

 }

function get_options($qids){
// print_r($qids);
// exit();
	 $query=$this->db->query("select * from ets_options where qid in ($qids) order by FIELD(ets_options.qid,$qids)");
	 // print_r($query);
	 // exit();
	 return $query->result_array();
	 
 }

function insert_answer(){

	$rid=$_POST['rid'];
	$srid=$this->session->userdata('rid');

	$uid = $this->session->userdata("loginuserID");;
	if($srid != $rid){

	return "Something wrong";
	}
	$query=$this->db->query("select * from ets_result join ets_quiz on ets_result.quid=ets_quiz.quid where ets_result.rid='$rid' "); 
	$quiz=$query->row_array(); 
	$correct_score=$quiz['correct_score'];
	$incorrect_score=$quiz['incorrect_score'];
	$qids=explode(',',$quiz['r_qids']);
	$vqids=$quiz['r_qids'];
	$correct_incorrect=explode(',',$quiz['score_individual']);
	
	
	// remove existing answers
	$this->db->where('rid',$rid);
	$this->db->delete('ets_answers');
	
	 foreach($_POST['answer'] as $ak => $answer){
		 
		 // multiple choice single answer
		 if($_POST['question_type'][$ak] == '1' || $_POST['question_type'][$ak] == '2'){
			 
			 $qid=$qids[$ak];
			 $query=$this->db->query(" select * from ets_options where qid='$qid' ");
			 $options_data=$query->result_array();
			 $options=array();
			 foreach($options_data as $ok => $option){
				 $options[$option['oid']]=$option['score'];
			 }
			 $attempted=0;
			 $marks=0;
				foreach($answer as $sk => $ansval){
					if($options[$ansval] <= 0 ){
					$marks+=-1;	
					}else{
					$marks+=$options[$ansval];
					}
					$userdata=array(
					'rid'=>$rid,
					'qid'=>$qid,
					'uid'=>$uid,
					'q_option'=>$ansval,
					'score_u'=>$options[$ansval]
					);
					$this->db->insert('ets_answers',$userdata);
				$attempted=1;	
				}
				if($attempted==1){
					if($marks >= '0.99' ){
					$correct_incorrect[$ak]=1;	
					}else{
					$correct_incorrect[$ak]=2;							
					}
				}else{
					$correct_incorrect[$ak]=0;
				}
		 }
		 // short answer
		 if($_POST['question_type'][$ak] == '3'){
			 
			 $qid=$qids[$ak];
			 $query=$this->db->query(" select * from ets_options where qid='$qid' ");
			 $options_data=$query->row_array();
			 $options_data=explode(',',$options_data['q_option']);
			 $noptions=array();
			 foreach($options_data as $op){
				 $noptions[]=strtoupper(trim($op));
			 }
			 
			 $attempted=0;
			 $marks=0;
				foreach($answer as $sk => $ansval){
					if($ansval != ''){
					if(in_array(strtoupper(trim($ansval)),$noptions)){
					$marks=1;	
					}else{
					$marks=0;
					}
					
				$attempted=1;

					$userdata=array(
					'rid'=>$rid,
					'qid'=>$qid,
					'uid'=>$uid,
					'q_option'=>$ansval,
					'score_u'=>$marks
					);
					$this->db->insert('ets_answers',$userdata);

				}
				}
				if($attempted==1){
					if($marks==1){
					$correct_incorrect[$ak]=1;	
					}else{
					$correct_incorrect[$ak]=2;							
					}
				}else{
					$correct_incorrect[$ak]=0;
				}
		 }
		 
		 // long answer
		 if($_POST['question_type'][$ak] == '4'){
			  $attempted=0;
			 $marks=0;
			  $qid=$qids[$ak];
					foreach($answer as $sk => $ansval){
					if($ansval != ''){
					$userdata=array(
					'rid'=>$rid,
					'qid'=>$qid,
					'uid'=>$uid,
					'q_option'=>$ansval,
					'score_u'=>0
					);
					$this->db->insert('ets_answers',$userdata);
					$attempted=1;
					}
					}
				if($attempted==1){
					
					$correct_incorrect[$ak]=3;							
					
				}else{
					$correct_incorrect[$ak]=0;
				}
		 }
		 
		 // match
			 if($_POST['question_type'][$ak] == '5'){
				 			 $qid=$qids[$ak];
			 $query=$this->db->query(" select * from ets_options where qid='$qid' ");
			 $options_data=$query->result_array();
			$noptions=array();
			foreach($options_data as $op => $option){
				$noptions[]=$option['q_option'].'___'.$option['q_option_match'];				
			}
			 $marks=0;
			 $attempted=0;
					foreach($answer as $sk => $ansval){
						if($ansval != '0'){
						$mc=0;
						if(in_array($ansval,$noptions)){
							$marks+=1/count($options_data);
							$mc=1/count($options_data);
						}else{
							$marks+=0;
							$mc=0;
						}
					$userdata=array(
					'rid'=>$rid,
					'qid'=>$qid,
					'uid'=>$uid,
					'q_option'=>$ansval,
					'score_u'=>$mc
					);
					$this->db->insert('ets_answers',$userdata);
					$attempted=1;
					}
					}
					if($attempted==1){
					if($marks==1){
					$correct_incorrect[$ak]=1;	
					}else{
					$correct_incorrect[$ak]=2;							
					}
				}else{
					$correct_incorrect[$ak]=0;
				}
		 }
		 
	 }
	 
	 $userdata=array(
	 'score_individual'=>implode(',',$correct_incorrect),
	 'individual_time'=>$_POST['individual_time'],
	 
	 );
	 $this->db->where('rid',$rid);
	 $this->db->update('ets_result',$userdata);
	 
	 return true;
	 
 }

 function exam_dateStart($sessionType,$session,$examType,$yearsOrSemester,$education_mode,$yosPosition){

$sessionBoth = explode('-', $session);

$sessionStart = $sessionBoth[0];

$FindYearNumber = FindYearNumber($yearsOrSemester);

$FindSemesterNumber = FindSemesterNumber($yearsOrSemester); 

$findExamYear =  $sessionStart+$FindYearNumber;

$findExamSemester =  $sessionStart+$FindSemesterNumber;

if ($examType=='y') {

	if ($sessionType=='C') {

			if ($education_mode == 1) {
				if($yosPosition=='Passed Out'){
					return "Passed Out";
				}else{
		        $monthlytime = "Dec"."&nbsp;". $findExamYear;
				return $monthlytime;
				}

			}else{
				$Currentmont =  strtotime(date('m'));
				$staticMonthJun =  strtotime('jun');
				$staticMonthDec =  strtotime('dec');

				if ($Currentmont>$staticMonthDec) {
					
				}

			}
	}else{

			if ($education_mode == 1) {
				if($yosPosition=='Passed Out'){
					return "Passed Out";
				}else{
		        $monthlytime = "Jun". "&nbsp;".$findExamYear;
				return $monthlytime;
				}

			}else{
				$Currentmont =  strtotime(date('m'));
				$staticMonthJun =  strtotime('jun');
				$staticMonthDec =  strtotime('dec');

				if ($Currentmont>$staticMonthDec) {
					
				}
		}
	}
}

 }


function getPaginationrow(){

    $this->db->where('result_status','Open');
    $this->db->or_where('result_status','Warning');
   return  $this->db->get('ets_result')->num_rows();
}

function get_join_where_video($limit, $start){
       
		$this->db->select('*');

		$this->db->from('ets_result');

		$this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

		$this->db->join('student', 'student.studentID = ets_result.uid', 'LEFT');

		$this->db->join('classes', 'classes.classesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

		$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');

		   $this->db->where('ets_result.result_status','Open');
		   $this->db->or_where('ets_result.result_status','Warning');
		   $this->db->limit($limit,$start);
		 $query = $this->db->get();
		return $query->result_array();
}

  function set_ind_time(){
	 	$rid=$this->session->userdata('rid');

	 $userdata=array(
	 'individual_time'=>$_POST['individual_time'],
	 
	 );
	 $this->db->where('rid',$rid);
	 $this->db->update('ets_result',$userdata);
	 
	 return true;
 }

function submit_result(){
	 // $email=$logged_in['email'];
	 $rid=$this->session->userdata('rid');
	$query=$this->db->query("select * from ets_result join ets_quiz on ets_result.quid=ets_quiz.quid where ets_result.rid='$rid' "); 
	$quiz=$query->row_array(); 
	$score_ind=explode(',',$quiz['score_individual']);
	$r_qids=explode(',',$quiz['r_qids']);
	$qids_perf=array();
	$marks=0;
	$correct_score=$quiz['correct_score'];
	$incorrect_score=$quiz['incorrect_score'];
	$total_time=array_sum(explode(',',$quiz['individual_time']));
	$manual_valuation=0;
	foreach($score_ind as $mk => $score){
		$qids_perf[$r_qids[$mk]]=$score;
		
		if($score == 1){
			
			$marks+=$correct_score;
			
		}
		if($score == 2){
			
			$marks+=$incorrect_score;
		}
		if($score == 3){
			
			$manual_valuation=1;
		}
		
	}
	$percentage_obtained=($marks/($quiz['r_noq']*$correct_score))*100;
	if($percentage_obtained >= $quiz['pass_percentage']){
		$qr=$this->lang->line('pass');
	}else{
		$qr=$this->lang->line('fail');
		
	}
	 $userdata=array(
	  'total_time'=>$total_time,
	  'end_time'=>time(),
	  'score_obtained'=>$marks,
	 'percentage_obtained'=>$percentage_obtained,
	 'manual_valuation'=>$manual_valuation
	 );
	 if($manual_valuation == 1){
		 $userdata['result_status']=$this->lang->line('pending');
	}else{
		$userdata['result_status']=$qr;
	}
	 $this->db->where('rid',$rid);
	 $this->db->update('ets_result',$userdata);
	 
	 
	 foreach($qids_perf as $qp => $qpval){
		 $crin="";
		 if($qpval=='0'){
			$crin=", no_time_unattempted=(no_time_unattempted +1) "; 
		 }else if($qpval=='1'){
			$crin=", no_time_corrected=(no_time_corrected +1)"; 	 
		 }else if($qpval=='2'){
			$crin=", no_time_incorrected=(no_time_incorrected +1)"; 	 
		 }
		  $query_qp="update ets_qbank set no_time_served=(no_time_served +1)  $crin  where qid='$qp'  ";
	 $this->db->query($query_qp);
		 
	 }


	return true;
 }

}