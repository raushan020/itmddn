<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_course_m extends Admin_Model {

	// protected $_table_name = 'subject';
	// protected $_primary_key = 'subjectID';
	// protected $_primary_filter = 'intval';
	// protected $_order_by = "classesID asc";

	function __construct() {
		parent::__construct();
	}

	function get_sub_courses(){
		         $this->db->where('classesID',$this->uri->segment(3));
		$query = $this->db->get('sub_courses');
		return $query->result();
		
	}

	function get_single_subcoursealldata($id){
		$this->db->where('classesID',$id);
		$this->db->where('adminID',$this->session->userdata('adminID'));
		$this->db->where('status',1);
		$query = $this->db->get('sub_courses');
		return $query->result();
	}

		function get_sub_courses_ByID($sub_coursesID){
		         $this->db->where('sub_coursesID',$sub_coursesID);
		$query = $this->db->get('sub_courses');
		return $query->row();
		
	}

		function get_sub_courses_by_session($adminID,$classesID){
				$this->db->where('status',1);
		         $this->db->where('classesID',$classesID);
		         $this->db->where('adminID',$adminID);
		$query = $this->db->get('sub_courses');
		return $query->result();
		
	}

	function get_sub_courses_by_session_latest($adminID,$classesID){
				$this->db->where('status',1);
		         $this->db->where('classesID',$classesID);
		         $this->db->where('adminID',$adminID);
		$query = $this->db->get('sub_courses');
		return $query->result();
		
		
	}

	public function get_classes(){
		$adminID  = $this->session->userdata('adminID');  

	   $this->db->where('adminID',$adminID);
	  
	   return $this->db->get('classes')->result();
   
	}

	function get_SubCourses_forID($id){
		$adminID  = $this->session->userdata('adminID');  
		$this->db->where('status',1);
	    $this->db->where('sub_coursesID',$id);
	    $this->db->where('adminID',$adminID);
		$query = $this->db->get('sub_courses');
		return $query->row();
	}

	function update_subcoursedata($array, $id){
		$adminID  = $this->session->userdata('adminID');  
		$this->db->where('sub_coursesID',$id);
	    $this->db->where('adminID',$adminID);
		$query = $this->db->update('sub_courses',$array);
		// if($query){

		// }
		
	}
	// function update_subcourseforpayment($array,$feeAppliedID){
	// 	$this->db->where('feeAppliedID',$feeAppliedID);

	// }

	function updateinsert_subcourseforpayment($arraySingleUnit){
		$this->db->insert('feeApplied',$arraySingleUnit);
	}

     function get_sub_courses_by_session_by_superAdmin(){

		         $this->db->where('classesID',$this->session->userdata('classesID'));
		$query = $this->db->get('sub_courses');
		return $query->result();
		
	}

	function insert_subcourse($array) {
		$this->db->insert('sub_courses',$array);
		return $this->db->insert_id();
	}

	function insert_subcourseforpayment($fee_array){
		
		$this->db->insert('feeApplied',$fee_array);
		// return $this->db->insert_id();
	}

	function get_subcousre_no($adminID)
	{			
	
		$this->db->select('sub_courses.*,classes.classes');
		$this->db->from('sub_courses');
		$this->db->join('classes','classes.classesID=sub_courses.classesID','LEFT');
		$this->db->join('department', 'classes.departmentID = department.departmentID', 'LEFT');

		if ($this->session->userdata('ActiveSubcourse')) {
		 	$this->db->where('sub_courses.status', $this->session->userdata('ActiveSubcourse'));
		}
		if ($this->session->userdata('DraftSubcoursecontrytrytrytrytry')) {
		 	$this->db->where('sub_courses.status', 0);
		}
		if ($this->session->userdata('TrashSubcourse')) {
		 	$this->db->where('sub_courses.status', $this->session->userdata('TrashSubcourse'));
		}

		if ($this->session->userdata('subcourseForfilterDepartment')) {
		 	$this->db->where('classes.departmentID', $this->session->userdata('subcourseForfilterDepartment'));
		}
		if ($this->session->userdata('subcourseForfilterCourses')) {
		 	$this->db->where('sub_courses.classesID', $this->session->userdata('subcourseForfilterCourses'));
		}
		if(empty($this->session->userdata('TrashSubcourse')) && empty($this->session->userdata('DraftSubcoursecontrytrytrytrytry'))  && empty($this->session->userdata('ActiveSubcourse'))){
			$this->db->where('sub_courses.status', 1);
		}
		if(isset($_POST["search"]["value"]))  

        { 

			$searches  =  $_POST["search"]["value"];

			$order_column = array( 
 
                            0 =>'subCourseCode',

                            1=> 'sub_course',

                        );

			$where = "(sub_courses.sub_course LIKE '%$searches%')";

           	$this->db->where($where);

		}

		
		$this->db->where('sub_courses.adminID',$adminID);	

		// $this->db->where('sub_courses.status', 1);

		$result = $this->db->get();

		$query = $result->num_rows();

		return $query;		

	}

	function get_subcourse_byAdmin($adminID) {

		$this->get_subcourse_ClgAdmin(); 

		$result = $this->db->get();

		$query = $result->result();
		
		return $query;

	}

	function get_subcourse_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('sub_courses.*,classes.classes');
		$this->db->from('sub_courses');

		$this->db->join('classes','classes.classesID=sub_courses.classesID','LEFT');

		$this->db->join('department', 'classes.departmentID = department.departmentID', 'LEFT');

		if ($this->session->userdata('ActiveSubcourse')) {
		 	$this->db->where('sub_courses.status', $this->session->userdata('ActiveSubcourse'));
		}
		if ($this->session->userdata('DraftSubcoursecontrytrytrytrytry')) {
		 	$this->db->where('sub_courses.status', 0);
		}
		if ($this->session->userdata('TrashSubcourse')) {
		 	$this->db->where('sub_courses.status', $this->session->userdata('TrashSubcourse'));
		}
		if ($this->session->userdata('subcourseForfilterDepartment')) {
		 	$this->db->where('classes.departmentID', $this->session->userdata('subcourseForfilterDepartment'));
		}
		if ($this->session->userdata('subcourseForfilterCourses')) {
		 	$this->db->where('sub_courses.classesID', $this->session->userdata('subcourseForfilterCourses'));
		}
		if(empty($this->session->userdata('TrashSubcourse')) && empty($this->session->userdata('DraftSubcoursecontrytrytrytrytry'))  && empty($this->session->userdata('ActiveSubcourse'))){
			$this->db->where('sub_courses.status', 1);
		}
		$this->db->where('sub_courses.adminID',$adminID);	    

	 
		if(isset($_POST["search"]["value"]))  

        { 

			$searches  =  $_POST["search"]["value"];

			$order_column = array(                             

                            0 =>'subCourseCode',

                            1=> 'sub_course',                            

                        );

           	$where = "(sub_courses.sub_course LIKE '%$searches%')";

           	$this->db->where($where);

           	if(isset($_POST["order"]))  

           { 
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           { 

                $this->db->order_by('sub_courses.sub_coursesID', 'DESC');  

           }           

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       } 

	}
	function ActiveClasess_count($adminID){

		$this->db->where('status',1);
		$this->db->where('adminID',$adminID);
		$query	= $this->db->get('sub_courses');
		return $query->num_rows();

	}
	function DraftClasess_count($adminID){
		$this->db->where('status',0);
		$this->db->where('adminID',$adminID);
		$query	= $this->db->get('sub_courses');
		return $query->num_rows();
	}

	function TrashClasess_count($adminID){
		// print_r($adminID);exit();
		$this->db->where('status',2);
		$this->db->where('adminID',$adminID);
		$query	= $this->db->get('sub_courses');
		return $query->num_rows();
	}

}

/* End of file subject_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/subject_m.php */
