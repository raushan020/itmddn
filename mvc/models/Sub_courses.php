<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_courses extends Admin_Model {

	// protected $_table_name = 'subject';
	// protected $_primary_key = 'subjectID';
	// protected $_primary_filter = 'intval';
	// protected $_order_by = "classesID asc";

	function __construct() {
		parent::__construct();
	}

	function get_sub_courses(){
		         $this->db->where('classesID',$this->uri->segment(3));
		$query = $this->db->get('sub_courses');
		return $query->result();
		
	}

		function get_sub_courses_ByID($sub_coursesID){
		         $this->db->where('sub_coursesID',$sub_coursesID);
		$query = $this->db->get('sub_courses');
		return $query->row();
		
	}

		function get_sub_courses_by_session($adminID,$classesID){
				$this->db->where('status',1);
		         $this->db->where('classesID',$classesID);
		         $this->db->where('adminID',$adminID);
		$query = $this->db->get('sub_courses');
		return $query->result();
		
	}

	function get_sub_courses_by_session_latest($adminID,$classesID){
				$this->db->where('status',1);
		         $this->db->where('classesID',$classesID);
		         $this->db->where('adminID',$adminID);
		$query = $this->db->get('sub_courses');
		return $query->result();
		
		
	}

     function get_sub_courses_by_session_by_superAdmin(){

		         $this->db->where('classesID',$this->session->userdata('classesID'));
		$query = $this->db->get('sub_courses');
		return $query->result();
		
	}


}

/* End of file subject_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/subject_m.php */
