<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Mark_m extends Admin_Model {



	protected $_table_name = 'mark';

	protected $_primary_key = 'markID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "subject asc";



	function __construct() {

		parent::__construct();

	}



	function get_mark($examID) {

		
		  $this->db->where('examID',$examID);
          $query = $this->db->get('mark');
		  return $query->result();

	}

	function exam_result($id,$classesID,$sub_coursesID,$yearsOrSemester){
 
		// $query=$this->db->query("select * from ets_result join ets_quiz on ets_result.quid=ets_quiz.quid where ets_result.rid='$rid' "); 
		// return $query->row_array(); 

		$this->db->select('*');
		$this->db->from('subject');
		$this->db->join('subject','subject.subjectID = ets_quiz.subjectID','LEFT');
		$this->db->where('ets_quiz.yearsOrSemester',$yearsOrSemester);
		$this->db->where('ets_quiz.classesID',$classesID);
		$this->db->where('ets_quiz.sub_coursesID',$sub_coursesID);
				$uri = $this->uri->segment('5');
 	 $this->db->where('ets_quiz.exam_type_new', $uri);
return	$getData = $this->db->get()->result_array();

		// print_r($getData);
		// exit();
		
		 
	 }

	 function exam_result_aggregate($id,$yearsOrSemester){
		$this->db->select('sum(score_obtained) as obtainedmarks,sum(r_noq) as fullMarksstudent,percentage_obtained');
		$this->db->from('ets_result');	
		$this->db->join('ets_quiz','ets_quiz.quid = ets_result.quid','LEFT');
		$this->db->where('ets_quiz.yearsOrSemester',$yearsOrSemester);
		$this->db->where('uid',$id);
		$this->db->group_by('uid');
		return	$getData = $this->db->get()->result();
	}
	function exam_result_fullmark($id,$yearsOrSemester){
		$this->db->select('sum(pass_percentage) as fullMarks');
		$this->db->from('ets_quiz');	
	    //$this->db->join('ets_result','ets_result.quid = ets_quiz.quid','LEFT');
		$this->db->where('ets_quiz.yearsOrSemester',$yearsOrSemester);
		$this->db->where('classesID',$id);
		$this->db->where('ets_quiz.status',1);
		// $this->db->group_by('uid');
				$uri = $this->uri->segment('5');
 	 $this->db->where('ets_quiz.exam_type_new', $uri);

		return	$getData = $this->db->get()->result();
	}

	function student_detail_with_result($id){
		$this->db->select('student.*,classes.classes,classes.classesID');
		$this->db->from('student');
		$this->db->join('classes','classes.classesID = student.classesID','LEFT');
		$this->db->where('student.studentID',$id);
		$query = $this->db->get()->row();
		return $query;
	}

	function get_order_by_mark($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}



	function insert_mark($array) {

		$error = parent::insert($array);

		return TRUE;

	}



	function update_mark($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}



	function update_mark_classes($array, $id) {

		$this->db->update($this->_table_name, $array, $id);

		return $id;

	}



	public function delete_mark($id){

		parent::delete($id);

	}



	function sum_student_subject_mark($studentID, $classesID, $subjectID) {

		$array = array(

			"studentID" => $studentID,

			"classesID" => $classesID,

			"subjectID" => $subjectID

		);

		$this->db->select_sum('mark');

		$this->db->where($array);

		$query = $this->db->get('mark');

		return $query->row();

	}



	function count_subject_mark($studentID, $classesID, $subjectID) {

		$query = "SELECT COUNT(*) as 'total_semester' FROM mark WHERE studentID = $studentID && classesID = $classesID && subjectID = $subjectID && (mark != '' || mark <= 0 || mark >0)";

	    $query = $this->db->query($query);

	    $result = $query->row();

	    return $result;

	}


	function get_join_result_count($adminID) {

		if(isset($_POST["search"]["value"]))  
        {  
 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 
                            1 =>'studentID',
                            2 =>'subject_code',
                            3 =>'subject',
                            4 =>'classes',
                            5 =>'sub_course',
                            6 =>'yearsOrSemester',
                            7 =>'r_noq',
                            8 =>'score_obtained',
                            9 =>'percentage_obtained',
                            10 =>'action'
                        );
           $where = "(student.name LIKE '%$searches%' OR subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 

		$this->db->select('*');

		$this->db->from('ets_result');

		$this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

		$this->db->join('student', 'student.studentID = ets_result.uid', 'LEFT');

		$this->db->join('classes', 'classes.classesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

		$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');

		
		if ($this->session->userdata('FilterMarkclassesID')) {
		 $this->db->where('ets_quiz.classesID', $this->session->userdata('FilterMarkclassesID'));
		}

		if ($this->session->userdata('FilterMarksubCourseID')) {
		 $this->db->where('ets_quiz.sub_coursesID', $this->session->userdata('FilterMarksubCourseID'));
		}

		if ($this->session->userdata('FilterMarkyearSemesterID')) {
		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterMarkyearSemesterID'));
		}
		if ($this->session->userdata('FilterMarksubjectID')) {
		 $this->db->where('ets_quiz.subjectID', $this->session->userdata('FilterMarksubjectID'));
		}
		$uri = $this->uri->segment('3');


 	 $this->db->where('ets_quiz.exam_type_new', $uri);

	     	
		$this->db->group_by('student.studentID');
		$this->db->where('ets_result.result_status','0');
		$this->db->where('ets_result.usertype','Student');
		
		$query = $this->db->get();


		return $query->num_rows();

	}


	function get_join_result() {

		$adminID = $this->session->userdata("adminID");
		
		$this->db->select('ets_result.*,ets_quiz.*,student.*,classes.*,sub_courses.*,subject.*,count(ets_result.score_obtained) as score');

		$this->db->from('ets_result');

		$this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

		$this->db->join('student', 'student.studentID = ets_result.uid', 'LEFT');

		$this->db->join('classes', 'classes.classesID = ets_quiz.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = ets_quiz.sub_coursesID', 'LEFT');

		$this->db->join('subject', 'subject.subjectID = ets_quiz.subjectID', 'LEFT');
		
		if ($this->session->userdata('FilterMarkclassesID')) {
		 $this->db->where('ets_quiz.classesID', $this->session->userdata('FilterMarkclassesID'));
		}

		if ($this->session->userdata('FilterMarksubCourseID')) {
		 $this->db->where('ets_quiz.sub_coursesID', $this->session->userdata('FilterMarksubCourseID'));
		}

		if ($this->session->userdata('FilterMarkyearSemesterID')) {
		 $this->db->where('ets_quiz.yearsOrSemester', $this->session->userdata('FilterMarkyearSemesterID'));
		}
		if ($this->session->userdata('FilterMarksubjectID')) {
		 $this->db->where('ets_quiz.subjectID', $this->session->userdata('FilterMarksubjectID'));
		}
$uri = $this->uri->segment('3');

 	 $this->db->where('ets_quiz.exam_type_new', $uri);



		$this->db->group_by('student.studentID');
		$this->db->where('ets_result.result_status','0');
		$this->db->where('ets_result.usertype','Student');
		// $this->db->where('subject.adminID', $adminID);



		if(isset($_POST["search"]["value"]))  
        {  
 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 =>'sn', 
                            1 =>'studentID',
                            2 =>'subject_code',
                            3 =>'subject',
                            4 =>'classes',
                            5 =>'sub_course',
                            6 =>'yearsOrSemester',
                            7 =>'r_noq',
                            8 =>'score_obtained',
                            9 =>'percentage_obtained',
                            10 =>'action'
                        );
           $where = "(student.name LIKE '%$searches%' OR subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 

           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
           		$this->db->order_by('ets_result.rid', 'DESC'); 
               
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 

            // $this->db->group_by('ets_result.uid', 'DESC');  
       }

	}

	function make_datatables($adminID){  
		$this->get_join_result();             
           $query = $this->db->get();  
           // print_r($this->db->last_query());
           // exit();
           return $query->result();  
      } 


	function get_join_mark($adminID) {		

		// $this->db->join('subject', 'subject.subjectID = mark.subjectID', 'INNER');

		$this->db->select('*');
		// $this->db->where();

		$this->db->from('ets_result');

	    // $this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

	 //    $this->db->join('ets_quiz', 'ets_quiz.classesID = ets_result.classesID', 'LEFT');

		// $this->db->join('ets_quiz', 'ets_quiz.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

		

		$query = $this->db->get();

	    $result = $query->row();

	    return $result;

	}	



	function get_order_by_mark_with_subject($classes,$year) {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->join('mark', 'subject.subjectID = mark.subjectID', 'LEFT');

		$this->db->join('exam', 'exam.examID = mark.examID');

		$this->db->where('mark.classesID', $classes);

		$this->db->where('mark.year', $year);

		$query = $this->db->get();

		return $query->result();

	}



	function get_order_by_mark_with_highest_mark($classID,$studentID) {

		$this->db->select('M.markID,M.examID, M.exam, M.subjectID, M.subject, M.studentID, M.classesID,  M.mark, M.year, (

		SELECT Max( mark.mark )

		FROM mark

		WHERE mark.subjectID = M.subjectID

		AND mark.examID = M.examID

		) highestmark');

		$this->db->from('exam E');

		$this->db->join('mark M', 'M.examID = E.examID', 'LEFT');

		$this->db->join('subject S', 'M.subjectID = S.subjectID');

		$this->db->where('M.classesID', $classID);

		$this->db->where('M.studentID', $studentID);

		$query = $this->db->get();

		return $query->result();

	}

	function suspend_examDtat(){
		$this->db->select('ets_result.*,student.name,student.father_name,ets_quiz.quiz_name');
		$this->db->from('ets_result');
		$this->db->join('student','student.studentID = ets_result.uid','LEFT');
		$this->db->join('ets_quiz','ets_quiz.quid = ets_result.quid','LEFT');
		$this->db->where('result_status','Suspended');
		$suspendDATA = $this->db->get()->result();
		return $suspendDATA;
	}

}



/* End of file mark_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/mark_m.php */

