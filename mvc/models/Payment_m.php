<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');







class Payment_m extends Admin_Model {







	protected $_table_name = 'payment';



	protected $_primary_key = 'paymentID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "paymentID desc";







	function __construct() {



		parent::__construct();



	}







	function get_payment($array=NULL, $signal=FALSE) {



		$query = parent::get($array, $signal);



		return $query;



	}







	function get_payment_byAdminID($adminID) {

				$this->db->select('payment.*, student.studentID,payment.paymentID');
		        $this->db->from('payment');
		        $this->db->join('invoice','invoice.invoiceID=payment.invoiceID','LEFT');
				$this->db->join('student','invoice.studentID=student.studentID','LEFT');
                $this->db->where('student.status',1);
                $this->db->where('invoice.feetype','clg');
				$this->db->where('invoice.student_position',1);				

		$query = $this->db->get();





		return $query->result();



	}







	function get_order_by_payment() {



				$this->db->select('payment.*, student.studentID,payment.paymentID');
		        $this->db->from('payment');
		        $this->db->join('invoice','invoice.invoiceID=payment.invoiceID','LEFT');
				$this->db->join('student','invoice.studentID=student.studentID','LEFT');
                $this->db->where('student.status',1);
                $this->db->where('payment.feeType','clg');				

		$query = $this->db->get()->result();

		// $query = parent::get_order_by($array);



		return $query;



	}







	function get_single_payment($array=NULL) {



		$query = parent::get_single($array);



		return $query;



	}







	function insert_payment($array) {



		$error = parent::insert($array);



		return TRUE;



	}







	function update_payment($data, $id = NULL) {



		parent::update($data, $id);



		return $id;



	}



 	function update_paymentCsv($payment_array,$studentID,$year){

 		$this->db->where('yearsOrSemester',$year);

 		$this->db->where('studentID',$studentID);

 		$this->db->update('payment',$payment_array);

 	}



	public function delete_payment($id){



		parent::delete($id);



	}



}







/* End of file payment_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/payment_m.php */