<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Live_m extends Admin_Model {



	protected $_table_name = 'subject';

	protected $_primary_key = 'subjectID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "classesID asc";



	function __construct() {

		parent::__construct();

	}


function broad_info($id,$adminID){
$this->db->where('streamID',$id);
$this->db->where('adminID',$adminID);
return $this->db->get('live_broadcast')->row();
}

	function get_join_broad_count($adminID,$usertype,$loginuserID) {


		$this->db->from('live_broadcast');

		$this->db->join('subject', 'subject.subjectID = live_broadcast.subjectID', 'LEFT');

		$this->db->where('live_broadcast.adminID',$adminID);

		$this->db->where('live_broadcast.CreatedUsertype',$usertype);

		$this->db->where('live_broadcast.createdID',$loginuserID);


		$query = $this->db->get();

		return $query->num_rows();


	}
function make_datatables($adminID,$usertype,$loginuserID){  
           $this->get_join_notice($adminID,$usertype,$loginuserID);  
           $query = $this->db->get();  
           return $query->result();  
      } 
      function get_join_notice($adminID,$usertype,$loginuserID) {

		$this->db->from('live_broadcast');

		$this->db->join('subject', 'subject.subjectID = live_broadcast.subjectID', 'LEFT');

		$this->db->where('live_broadcast.adminID',$adminID);

		$this->db->where('live_broadcast.CreatedUsertype',$usertype);

		$this->db->where('live_broadcast.createdID',$loginuserID);


		// if ($this->session->userdata('FilterNoticeclassesID')) {
		//  $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		// }


if(isset($_POST["search"]["value"]))  
           {  
 $searches  =  $_POST["search"]["value"];

		$order_column = array( 

                            0 =>'sn', 
                            1 => 'stream_name',
                            2 => 'channel_name',
                            3 => 'av',
                            4 => 'status',
                            
                            
                            
                        );
           $where = "(live_broadcast.stream_name LIKE '%$searches%' OR live_broadcast.channel_name LIKE '%$searches%' OR subject.subject LIKE '%$searches%')";
           $this->db->where($where);
           } 
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                // $this->db->order_by('notice.noticeID', 'DESC');  
           	$this->db->order_by('live_broadcast.streamID','DESC');
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       }

	}
  function broad_info_subject($subjectID)
  {
    $this->db->select('subjectID,subject,professor.name,professor.photo');
    $this->db->from('subject');
    $this->db->join('professor','professor.professorID=subject.professorID','left');
    $this->db->where('subjectID',$subjectID);
    return $this->db->get()->row();
  }
  function broad_live_student($data)
  {
    $this->db->insert('live_broadcast_student',$data);
    return $this->db->insert_id();
  }
  function broad_live_studentdata($streamID)
  {
    $this->db->select('student.name,student.photo');
    $this->db->from('live_broadcast_student');
    $this->db->join('student','student.studentID=live_broadcast_student.studentID','left');
    $this->db->where('live_broadcast_student.streamID',$streamID);
    $this->db->where('live_broadcast_student.studentID !=',$this->session->userdata('loginuserID'));
    return $this->db->get()->result();
  }


}