<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Signin_m extends Admin_Model {



	function __construct() {

		parent::__construct();

		$this->load->model("setting_m");

	}

	public function signin() {
		$tables = array('student' => 'student', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','super_admin' => 'super_admin','admin'=>'admin','professor'=>'professor','parent'=>'parent');
	// print_r($tables);
	// exit();
		$array = array();

		$i = 0;

		$username = $this->input->post('username');

		$password = $this->hash($this->input->post('password'));
		$userdata = '';
// echo $username;print_r($password);die;
		foreach ($tables as $table) {

			$user = $this->db->get_where($table, array("username" => $username, "password" => $password,'status'=>1));
// print_r($user);die;
			$alluserdata = $user->row();
			// print_r($alluserdata);die;

			if($alluserdata) {
				$academicsession=$this->db->select('academic_session')->from('academic_session')->where(array('adminID' =>$alluserdata->adminID,'status'=> 1))->get()->row();
				if($table != "admin")
				{
				
					$adminID=$alluserdata->adminID;

					$checkadmin=$this->db->select('adminID')->from('admin')->where(array('systemadminactive'=>1,'adminID'=>$adminID))->get()->row();
					if($checkadmin)
					{
						$userdata = $alluserdata;
						// print_r($userdata);die;
						$array['permition'][$i] = 'yes';
					}
					else
					{
						$array['permition'][$i] = 'no';
					}
				}
				else
				{
					$userdata = $alluserdata;
						// print_r($userdata);die;
						$array['permition'][$i] = 'yes';
				}
			} else {

				$array['permition'][$i] = 'no';

			}

			$i++;

		}




		if(in_array('yes', $array['permition'])) {

		if($userdata->usertype == "superadmin") {
		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->superAdminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
					//"userID" => $userdata->userID,

					 "phone" => $userdata->phone,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);
				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'adminID'=>$userdata->adminID
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;

			}
			elseif($userdata->usertype == "ClgAdmin") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->adminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
                     "phone" => $userdata->phone,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"address"=>$userdata->address,

					"lang" => $lang,

					"loggedin" => TRUE

				);
				$this->session->set_userdata($data);
			   $sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'adminID'=>$userdata->adminID
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;

			}
			
			elseif($userdata->usertype == "Student") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->studentID,

					"name" => $userdata->name,

					"examType" => $userdata->examType,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
					"email" => $userdata->email,

                     "phone" => $userdata->phone,
                     "semester"=>$userdata->yearsOrSemester,
					"usertype" => $userdata->usertype,
					"education_mode"=>$userdata->education_mode,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);
				// print_r($data);die;
				$this->db->where('classesID',$userdata->classesID);
				$this->db->where('status',1);
			    $classCount =	$this->db->get('classes')->row();

			    if ($classCount) {
			     if($classCount->IsSubCourse==1){
			    $this->db->where('sub_coursesID',$userdata->sub_coursesID);
				$this->db->where('status',1);
			    $subCourseCount =	$this->db->get('sub_courses')->num_rows();	
			    if ($subCourseCount>0) {
			    $this->session->set_userdata($data);
			   
			    $sessionData = array(
			    	'session_id' =>session_id(),
			    	'studentID'=>$userdata->studentID,
			    	'classesID'=>$userdata->classesID,
			    	'sub_coursesID'=>$userdata->sub_coursesID,
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;
			    }
			    }else{
			    $this->session->set_userdata($data);
			     $sessionData = array(
			    	'session_id' =>session_id(),
			    	'studentID'=>$userdata->studentID,
			    	'classesID'=>$userdata->classesID,
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;
			    }
			}

			} elseif($userdata->usertype == "Teacher") {
				
		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->teacherID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'teacherID'=>$userdata->teacherID
			    );   
			    $this->db->insert('school_sessions',$sessionData);


				return TRUE;

			} elseif($userdata->usertype == "Parent") {
				
				$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
				$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->studentID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);
				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->parentID
			    );   
			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			} elseif($userdata->usertype == "Admin" || $userdata->usertype == "Support" || $userdata->usertype == "Accountant" || $userdata->usertype == "HOD" || $userdata->usertype == "Senior_Examiner" || $userdata->usertype == "Assistant_Examiner") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
// print_r($settings);die;
		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->userID,
					"name" => $userdata->name,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
					"email" => $userdata->email,

					"phone" => $userdata->phone,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,
					"address"=>$userdata->address,
					"lang" => $lang,
					"verifyemail" =>$userdata->verifyemail,
					"verifymobileno"=>$userdata->verifymobileno,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->userID
			    );   
			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			}elseif($userdata->usertype == "Professor") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));

		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->professorID,
					"name" => $userdata->name,

					"adminID" => $userdata->adminID,
					"sessionyear"=>$academicsession->academic_session,
					"email" => $userdata->email,

					 "phone" => $userdata->phone,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					 "lang" => $lang,

					 "photo" => $userdata->photo,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>session_id(),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->professorID
			    );   
			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			}else {

 				return FALSE;

			}

		} else {

			return FALSE;

		}

	}


	function checkin(){
		$this->db->where('session_id',session_id());
		$this->db->where('current_login',1);
		$row  = $this->db->get('school_sessions')->num_rows();
		if ($row==0) {
			$this->signout();
		}
		}


function logoutOtherDevice(){
	$usertype = $this->session->userdata("usertype");
	$loginuserID = $this->session->userdata("loginuserID");

			  $data = array(
                'current_login'=>0
               );
			if ($usertype=='Student') {
				$this->db->where('studentID',$loginuserID);
			}elseif ($usertype=='ClgAdmin') {
			$this->db->where('adminID',$loginuserID);
			}elseif ($usertype=='Teacher') {
			  $this->db->where('teacherID',$loginuserID);
			}
			$this->db->update('school_sessions',$data);

}

	function change_password() {
	
		$table = strtolower($this->session->userdata("usertype"));
// print_r($table);die;
		if($table == "admin") {

			$table = "user";

		}

		if($table == "accountant") {

			$table = "user";

		}

		if($table == "clgadmin") {

			$table = "admin";

		}

		if($table == "librarian" || $table == "hod") {

			$table = "user";

		}
		if($table == "superadmin") {

			$table = "super_admin";

		}

		$username = $this->session->userdata("username");

		$old_password = $this->hash($this->input->post('old_password'));

		$new_password = $this->hash($this->input->post('new_password'));

		$user = $this->db->get_where($table, array("username" => $username, "password" => $old_password));

		$alluserdata = $user->row();

		if($alluserdata) {

			if($alluserdata->password == $old_password){

				$array = array(

					"password" => $new_password

				);

				$this->db->where(array("username" => $username, "password" => $old_password));

				$this->db->update($table, $array);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				return TRUE;

			}

		} else {

			return FALSE;

		}

	}

	public function signout() {

		$this->session->sess_destroy();

	}

	public function loggedin_exam() {

		return (bool) $this->session->userdata("loggedin_exam");

	}

	public function loggedin() {

		return (bool) $this->session->userdata("loggedin");

	}
	public function verifymailbyadminid($uri)
	{
		$usertype=$this->session->userdata('usertype');
    	$loginuserID=$this->session->userdata('loginuserID');
    	if($usertype == "Admin" || $usertype == "Accountant" || $usertype == "HOD")
		{
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('userID',$uri);
			$this->db->where('verifyemail',0);
			$admin= $this->db->get()->row();
	        if($admin)
	        {
	        	$this->db->update('user',array('verifyemail' => 1 ));
	        	$this->session->unset_userdata('verifyemail');
        		$this->session->set_userdata(array('verifyemail' =>1));
	        	return $value=true;
	        }
	        else
	        {
	        	return $value=false;
	        }
		}
		else if($usertype == "Professor")
    	{
    		$this->db->select('*');
			$this->db->from('professor');
			$this->db->where('professorID',$uri);
			$this->db->where('verifyemail',0);
			$admin= $this->db->get()->row();
	        if($admin)
	        {
	        	$this->db->update('professor',array('verifyemail' => 1 ));
	        	return $value=true;
	        }
	        else
	        {
	        	return $value=false;
	        }
    	}
    	else if($usertype == "Student")
    	{
    		$this->db->select('*');
			$this->db->from('student');
			$this->db->where('studentID',$uri);
			$this->db->where('verifyemail',0);
			$admin= $this->db->get()->row();
	        if($admin)
	        {
	        	$this->db->update('student',array('verifyemail' => 1 ));
	        	return $value=true;
	        }
	        else
	        {
	        	return $value=false;
	        }
    	}
    	else
    	{
    		$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
    	}
		
		
	}

}

/* End of file signin_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/signin_m.php */