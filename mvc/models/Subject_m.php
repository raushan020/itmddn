<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Subject_m extends Admin_Model {



	protected $_table_name = 'subject';



	protected $_primary_key = 'subjectID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "classesID asc";



	function __construct() {



		parent::__construct();



	}



	function get_join_subject_count($adminID) {

		

		if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 



		$this->db->select('*');



		$this->db->from('subject');



		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');





		if ($this->session->userdata('FilterSubjectclassesID')) {

		 $this->db->where('subject.classesID', $this->session->userdata('FilterSubjectclassesID'));

		}



		if ($this->session->userdata('FilterSubjectsubCourseID')) {

		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));

		}



		if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}



	     if ($this->session->userdata('ActiveSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('ActiveSubjects'));

		}



	   if ($this->session->userdata('DraftSubjects')) {

		 $this->db->where('subject.status', 0);

		}



	   if ($this->session->userdata('TrashSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('TrashSubjects'));

		}	

		if (empty($this->session->userdata('DraftSubjects')) && empty($this->session->userdata('TrashSubjects'))) {
			
           $this->db->where('subject.status',1);
        }


		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.adminID', $adminID);



		$query = $this->db->get();



		return $query->num_rows();



	}


		function GetSubjectBySyllabus($array){

		$adminID = $this->session->userdata("adminID");
		$this->db->from('units');
		$this->db->where($array);
		return $this->db->get()->result_array();

	}



	function get_join_subject() {



		$adminID = $this->session->userdata("adminID");

		$this->db->select('subject.*,classes.*,sub_courses.*,subject.status as studentStatus');

		$this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');





		if ($this->session->userdata('FilterSubjectclassesID')) {

		 $this->db->where('subject.classesID', $this->session->userdata('FilterSubjectclassesID'));

		}



		if ($this->session->userdata('FilterSubjectsubCourseID')) {

		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));

		}



		if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}



	   if ($this->session->userdata('ActiveSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('ActiveSubjects'));

		}



	   if ($this->session->userdata('DraftSubjects')) {

		 $this->db->where('subject.status', 0);

		}



	   if ($this->session->userdata('TrashSubjects')) {

		 $this->db->where('subject.status', $this->session->userdata('TrashSubjects'));

		}	
		if (empty($this->session->userdata('DraftSubjects')) && empty($this->session->userdata('TrashSubjects'))) {
           $this->db->where('subject.status',1);
        }




		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.adminID', $adminID);





if(isset($_POST["search"]["value"]))  

           {  

 $searches  =  $_POST["search"]["value"];



		$order_column = array( 



                            0 =>'sn', 

                            1 =>'subject_code',

                            2 =>'subject',

                            3=> 'classes',

                            4=> 'sub_course',

                            5=> 'yearsOrSemester',

                            6=>'action'

                        );

           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";

           $this->db->where($where);

           } 

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('subject.subject_code', 'ASC');  

           } 

           if (isset($_POST["length"])) {

  

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       }



	}



	function make_datatables($adminID){  

           $this->get_join_subject();  

           $query = $this->db->get();  

           return $query->result();  

      } 



	function get_join_where_subject($classesID,$sub_coursesID,$id,$yearsOrSemester=Null) {

        if($id != 0)
        {
             $loginuserID = $id;
        }
        else
        {
             $loginuserID = $this->session->userdata('loginuserID');
        }
		
		$this->db->select('yearsOrSemester');
		$this->db->from('student');
		$this->db->where('studentID',$loginuserID);
		$a=$this->db->get()->row();
       // print_r($a->yearsOrSemester);die;

		$this->db->select('subject.*,sylabus_pdf.subjectID as otherID,sylabus_pdf.pdf,notes_pdf.pdf as assignmentpdf');

		$this->db->from('subject');

		$this->db->join('sylabus_pdf', 'sylabus_pdf.subjectID = subject.subjectID', 'LEFT');
		$this->db->join('notes_pdf','notes_pdf.subjectID=subject.subjectID','LEFT');
		// $this->db->join('optionalsubject','optionalsubject.subjectID=subject.subjectID','left');


	if ($this->session->userdata('FilterSubjectyearSemesterID'))
	{
		if($a->yearsOrSemester >= $this->session->userdata('FilterSubjectyearSemesterID'))
		{
			$this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));
		}
		else
		{
			//$this->db->where('subject.yearsOrSemester',"null");
			$this->db->where('subject.yearsOrSemester',$yearsOrSemester);
		}
	}
	else
	{
		if($yearsOrSemester)
		{
			if($a->yearsOrSemester >= $yearsOrSemester)
			{
				$this->db->where('subject.yearsOrSemester', $yearsOrSemester);
			}
			else
			{
				//$this->db->where('subject.yearsOrSemester',"null");
				$this->db->where('subject.yearsOrSemester',$yearsOrSemester);
			}
		}
		else
		{
			$this->db->where('subject.yearsOrSemester',$a->yearsOrSemester);
		}
		
	}


		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->where("subject.status",1);
		$this->db->where("subject.subject_mode",0);
		$this->db->group_by('subject.subjectID');
		$this->db->order_by('subject.subject_code', 'DESC');  

		// if("subject.subject_mode"==1)
		// {
		// 	print_r("expression");die;

		// }

		$query = $this->db->get();
		 //print_r($this->db->last_query());die;
		return $query->result_array();

		// foreach ($assignsubject as $key => $value)
		// {
		// 	$this->db->select('o.subjectID,s.subject');
		// 	$this->db->from('optionalsubject as o');
		// 	$this->db->join('subject as s','s.subjectID=o.subjectID','left');
		// 	$this->db->where('o.studentID',$loginuserID);
		// 	$this->db->where('o.subjectID',$value['subjectID']);
		// 	$assignsubject[$key]['optionalsubject']= $this->db->get()->result();
		// }
		// return $assignsubject;



	}
	function showoptionalsubjectbystudent($classesID,$sub_coursesID,$id,$yearsOrSemester = NULL)
	{
           if($id != 0)
        {
             $loginuserID = $id;
        }
        else
        {
             $loginuserID = $this->session->userdata('loginuserID');
        }
		
		$this->db->select('yearsOrSemester');
		$this->db->from('student');
		$this->db->where('studentID',$loginuserID);
		$a=$this->db->get()->row();
       // print_r($a->yearsOrSemester);die;

		$this->db->select('subject.*,sylabus_pdf.subjectID as otherID,sylabus_pdf.pdf,optionalsubject.subjectID as optionalsubject,notes_pdf.pdf as assignmentpdf');

		$this->db->from('optionalsubject');

		$this->db->join('sylabus_pdf', 'sylabus_pdf.subjectID = optionalsubject.subjectID', 'LEFT');
		$this->db->join('subject','subject.subjectID=optionalsubject.subjectID','left');
        $this->db->join('notes_pdf','notes_pdf.subjectID=optionalsubject.subjectID','LEFT');

	if ($this->session->userdata('FilterSubjectyearSemesterID')) {

		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));

		}
		else
		{
			if($yearsOrSemester)
			{
				if($a->yearsOrSemester >= $yearsOrSemester)
				{
					$this->db->where('subject.yearsOrSemester', $yearsOrSemester);
				}
				else
				{
					$this->db->where('subject.yearsOrSemester',"null");
				}
			}
			else
			{
				$this->db->where('subject.yearsOrSemester',$a->yearsOrSemester);
			}
		}


		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->where("subject.status",1);
		$this->db->where("subject.subject_mode",1);
		$this->db->where("optionalsubject.studentID",$loginuserID);
		$this->db->group_by('subject.subjectID');
		$this->db->order_by('subject.subject_code', 'DESC');  

		// if("subject.subject_mode"==1)
		// {
		// 	print_r("expression");die;

		// }

		$query = $this->db->get();
		// print_r($query->result_array());die;
		return $query->result_array();
	}

function attd_percent($subjectID,$loginuserID){

$this->db->select('count(subjectID) as num_attd, sum(atd) as present');
$this->db->where('subjectID',$subjectID);
$this->db->where('studentID',$loginuserID);
// $this->db->group_by('atd_date');

return $this->db->get('attendance_student')->row();

}

	function get_join_where_subject_count($loginuserID,$classesID,$sub_coursesID) {


$this->db->select('yearsOrSemester');
		$this->db->from('student');
		$this->db->where('studentID',$loginuserID);
		$a=$this->db->get()->row();

		$this->db->select('*');



		$this->db->from('subject');



		$this->db->where("subject.classesID", $classesID);
		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$this->db->where('subject.yearsOrSemester',$a->yearsOrSemester);

		$query = $this->db->get();



		return $query->result();



	}



	function get_classes() {



		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');



		$query = $this->db->get();



		return $query->result();



	}



	function get_subject($array=NULL, $signal=FALSE) {



		$query = parent::get($array, $signal);



		return $query;



	}
function get_subject_ebookproffessor($getClasses,$getSubcourse,$getSemester)
{
	$this->db->where('classesID',$getClasses);
	$this->db->where('sub_coursesID',$getSubcourse);
	$this->db->where('yearsOrSemester',$getSemester);
	if($getSemester=="1st_Semester" || $getSemester=="3rd_Semester" || $getSemester=="5th_Semester" || $getSemester=="7th_Semester")
	{
		$year_mode=1;
	}
	else
	{
		if($getSemester=="1st_Year" || $getSemester=="2nd_Year" || $getSemester=="3rd_Year")
		{
			$year_mode=3;
		}
		else
		{
			$year_mode=2;
		}
	}
	$this->db->select('*');
	$this->db->from('subject');
	$this->db->join('epub','epub.subjectID = subject.subjectID','left');
	$this->db->where('subject.year_mode',$year_mode);
	$query = $this->db->get()->result();
	return $query;
}
function getsubjectebook()
{

	$this->db->select('*');
	$this->db->from('subject');
	$this->db->join('epub','epub.subjectID = subject.subjectID','left');
	$this->db->where('subject.status',1);
	$query = $this->db->get()->result();
	return $query;
}

function get_subject_proffessors($getClasses,$getSubcourse,$getSemester,$loginuserID)
{
	// print_r($getSemester);die;
	
	if($getSemester=="1st_Semester" || $getSemester=="3rd_Semester" || $getSemester=="5th_Semester" || $getSemester=="7th_Semester")
	{
		$year_mode=1;
	}
	else
	{
		if($getSemester=="1st_Year" || $getSemester=="2nd_Year" || $getSemester=="3rd_Year")
		{
			$year_mode=3;
		}
		else
		{
			$year_mode=2;
		}
	}
	$this->db->where('classesID',$getClasses);
	$this->db->where('sub_coursesID',$getSubcourse);
	$this->db->where('yearsOrSemester',$getSemester);
	$this->db->where('professorID',$loginuserID);
	$this->db->where('year_mode',$year_mode);
	$query = $this->db->get('subject')->result();
	// print_r($query);die;
	return $query;
}
function get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID)
{
	// print_r($getSemester);die;
	$this->db->where('classesID',$getClasses);
	$this->db->where('sub_coursesID',$getSubcourse);
	$this->db->where('yearsOrSemester',$getSemester);
	$this->db->where('professorID',$loginuserID);
	if($getSemester=="1st_Semester" || $getSemester=="3rd_Semester" || $getSemester=="5th_Semester" || $getSemester=="7th_Semester")
	{
		$year_mode=1;
	}
	else
	{
		if($getSemester=="1st_Year" || $getSemester=="2nd_Year" || $getSemester=="3rd_Year")
		{
			$year_mode=3;
		}
		else
		{
			$year_mode=2;
		}
	}
	// print_r($getSemester);die;
	$this->db->where('year_mode',$year_mode);
	$query = $this->db->get('subject')->result();
	return $query;
}

/*function get_subject_proffessor_join($yearsOrSemester,$classesID) {

$this->db->join('professor','professor.professorID=subject.subjectID','left');
$this->db->where('subject.yearsOrSemester',$yearsOrSemester);
$this->db->where('subject.classesID',$classesID);
$query = $this->db->get('subject')->result(); and subject.status = '1'
	return $query;

	}*/

    function get_subject_proffessor_join($yearsOrSemester,$classesID) {
    $loginuserID = $this->session->userdata("loginuserID");
    $this->db->select('professor_lecture.days, professor_lecture.times, subject.subject,subject.subjectID,student.studentactive, subject.subject_code, professor.name,professor.professorID,professor_lecture.semester,professor_lecture.course_id');
    $this->db->from('professor_lecture');
    $this->db->join('student','student.classesID=professor_lecture.course_id','left');
    $this->db->join('professor','professor_lecture.professor_id = professor.professorID','left');
    $this->db->join('subject','professor_lecture.subject_id = subject.subjectID','left');
    
    $where = "professor_lecture.semester = '".$yearsOrSemester."' and student.studentID ='".$loginuserID."' and professor_lecture.course_id = '".$classesID."' and professor.status = '1' and subject.subject_mode = '0'";
    $this->db->where($where);
    $this->db->where('professor_lecture.status',1);
    $query = $this->db->get();
    $result = $query->result();
	return $result;

	}
	function get_subject_proffessor_join_studentdashboard($yearsOrSemester,$classesID,$loginuserID=NULL) {
		if($loginuserID)
		{
			$loginuserID=$loginuserID;//For mobile app api
		}
		else
		{
			$loginuserID = $this->session->userdata("loginuserID");
		}
    
    $this->db->select('professor_lecture.days, professor_lecture.times, subject.subject,subject.subjectID,student.studentactive, subject.subject_code, professor.name,professor.professorID,professor_lecture.semester,professor_lecture.course_id');
    $this->db->from('professor_lecture');
    $this->db->join('student','student.classesID=professor_lecture.course_id','left');
    $this->db->join('professor','professor_lecture.professor_id = professor.professorID','left');
    $this->db->join('subject','professor_lecture.subject_id = subject.subjectID','left');
    $where = "professor_lecture.semester = '".$yearsOrSemester."' and student.studentID ='".$loginuserID."' and professor_lecture.course_id = '".$classesID."' and professor.status = '1' and subject.subject_mode = '0'";
    $this->db->where($where);
    $query = $this->db->get();
    $result = $query->result_array();
	return $result;

	}
	function get_subject_proffessor_join_studentonline($semester,$course_id,$subjectID,$professorID,$loginuserID)
	{
		$date=date('Y-m-d');
	    $this->db->select('activestatus');
	    $this->db->from('attendance_student');
	    $where = "yearsOrSemester = '".$semester."' and studentID ='".$loginuserID."' and classesID = '".$course_id."' and atd_date='".$date."' and year_mode = '5' and subjectID='".$subjectID."' and professorID='".$professorID."'";
	    $this->db->where($where);
	    $query = $this->db->get();
	    $result = $query->row();
		return $result;
	}
	
	function get_subject_proffessor_join_optionalsubjects($yearsOrSemester,$classesID,$studentID)
	{
		$loginuserID = $this->session->userdata("loginuserID");
		$this->db->select('professor_lecture.days, professor_lecture.times, subject.subject,subject.subjectID,student.studentactive, subject.subject_code, professor.name,professor.professorID,professor_lecture.semester,professor_lecture.course_id');
		$this->db->from('professor_lecture');
		$this->db->join('student','student.classesID=professor_lecture.course_id','left');
		$this->db->join('professor','professor_lecture.professor_id = professor.professorID','left');
		$this->db->join('subject','professor_lecture.subject_id = subject.subjectID','left');
		$this->db->join('optionalsubject','optionalsubject.subjectID = subject.subjectID','left');
		$where = "professor_lecture.semester = '".$yearsOrSemester."' and student.studentID ='".$loginuserID."' and professor_lecture.course_id = '".$classesID."' and professor.status = '1' and subject.subject_mode = '1' and optionalsubject.studentID = '".$studentID."'";
		$this->db->where($where);
		$query = $this->db->get();

		$result = $query->result();
		// print_r($result);
		// exit;
		return $result;
	}

	function get_subject_proffessor_join_optionalsubjects_studentdashboard($yearsOrSemester,$classesID,$studentID)
	{
		$loginuserID = $this->session->userdata("loginuserID");
		$this->db->select('professor_lecture.days, professor_lecture.times, subject.subject,subject.subjectID,student.studentactive, subject.subject_code, professor.name,professor.professorID,professor_lecture.semester,professor_lecture.course_id');
		$this->db->from('professor_lecture');
		$this->db->join('student','student.classesID=professor_lecture.course_id','left');
		$this->db->join('professor','professor_lecture.professor_id = professor.professorID','left');
		$this->db->join('subject','professor_lecture.subject_id = subject.subjectID','left');
		$this->db->join('optionalsubject','optionalsubject.subjectID = subject.subjectID','left');
		$where = "professor_lecture.semester = '".$yearsOrSemester."' and student.studentID ='".$loginuserID."' and professor_lecture.course_id = '".$classesID."' and professor.status = '1' and subject.subject_mode = '1' and optionalsubject.studentID = '".$studentID."'";
		$this->db->where($where);
		$query = $this->db->get();
		

		$result = $query->result_array();
		return $result;
	}
	function get_subject_proffessor_join_optionalsubjects_studentonline($yearsOrSemester,$course_id,$subjectID,$professorID,$loginuserID)
	{
		$date=date('Y-m-d');
	    $this->db->select('activestatus');
	    $this->db->from('attendance_student');
	    $where = "yearsOrSemester = '".$yearsOrSemester."' and studentID ='".$loginuserID."' and classesID = '".$course_id."' and atd_date='".$date."' and year_mode = '5' and subjectID='".$subjectID."' and professorID='".$professorID."'";
	    $this->db->where($where);
	    $query = $this->db->get();
	    $result = $query->row();
		return $result;
	}

	function get_subject_call($id) {



		$query = $this->db->get_where('subject', array('classesID' => $id));



		return $query->result();



	}



	function get_order_by_subject($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}



	function insert_subject($array) {



		$error = parent::insert($array);



		return TRUE;



	}

	function insert_syllabus($array) {

		$this->db->insert('syllabus',$array);
		return TRUE;



	}



	function update_subject($data, $id = NULL) {



		parent::update($data, $id);



		return $id;

	}



	 function GetSubjectByParam($array)

{



	$this->db->where($array);
	$this->db->order_by('subject.subject_code', 'ASC'); 
	$query  = $this->db->get('subject');

	return $query->result();

}



	public function delete_subject($id){



		parent::delete($id);



	}



		function all_count(){

		$this->db->where('status',1);

		$this->db->or_where('status',0);

	$query	= $this->db->get('subject');

	return $query->num_rows();

	}



		function ActiveSubjects_count(){

		 $this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.status',1);

			$query	= $this->db->get();

		return $query->num_rows();

	}



		function DraftSubjects_count(){

		 $this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.status',0);

		$query	= $this->db->get();

		return $query->num_rows();

	}



		function TrashSubjects_count(){

	    $this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');



		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');

		$this->db->where('classes.status',1);

		$this->db->where('sub_courses.status',1);

		$this->db->where('subject.status',2);

		$query	= $this->db->get();

	return $query->num_rows();

	}
	
	function assign_lecture($adminID)
	{
		$this->db->select('professor_lecture.id,professor_lecture.semester,classes.classes,subject.subject,professor.name');
		$this->db->from('professor_lecture');
		$this->db->join('classes','professor_lecture.course_id = classes.classesID','inner');
		$this->db->join('subject','professor_lecture.subject_id = subject.subjectID','inner');
		$this->db->join('professor','professor_lecture.professor_id = professor.professorID','inner');
		$where = "professor_lecture.admin_id = '".$adminID."'";
		$this->db->where($where);
		$sqls = $this->db->get();
		$results = $sqls->result_array();
		return $results;
	}
	
	function edit_professor_lecture($id)
	{
		$this->db->select('professor_lecture.id,professor_lecture.department_id,professor_lecture.course_id,professor_lecture.semester,professor_lecture.subject_id,professor_lecture.professor_id,professor_lecture.times,professor_lecture.days,classes.classes,subject.subject,professor.name,department.department_name');
		$this->db->from('professor_lecture');
		$this->db->join('classes','professor_lecture.course_id = classes.classesID','inner');
		$this->db->join('subject','professor_lecture.subject_id = subject.subjectID','inner');
		$this->db->join('professor','professor_lecture.professor_id = professor.professorID','inner');
		$this->db->join('department','professor_lecture.department_id = department.departmentID','inner');
		$where = "professor_lecture.id = '".$id."' ";
		$this->db->where($where);
		$sqls = $this->db->get();
		if($sqls->num_rows() > 0)
		{
			$results = $sqls->result_array();
			return $results;
		}
		else
		{
			$this->session->set_flashdata('msg','<p style="color:red;text-align:center;">No timetable record found!</p>');
		}
	}

	public function get_subject_unit_topic($subjectid)
	{
		// print_r($subjectid);die;
		$currentyear=date('Y');
		$this->db->select('*');
		$this->db->from('units');
		$this->db->where('subjectID',$subjectid);
		$result=$this->db->get()->result();
		// print_r($result);die;
		return $result;
	}
	public function get_subject_unit_checktopic($unitID)
	{
		// $currentyear=date('Y');
		$this->db->select('ut.*,u.*,s.classesID,s.yearsOrSemester,s.subjectID,s.professorID');
		$this->db->from('unit_topic as ut');
        $this->db->join('units as u','u.unitID=ut.unitID','left');
        $this->db->join('subject as s','s.subjectID=u.subjectID','left');
		$this->db->where('ut.unitID',$unitID);
		// $this->db->where('s.year',$currentyear);
		$result=$this->db->get()->result();
		return $result;
	}
	// public function progress_report($subjectid)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('complete_topic');
	// 	$this->db->where('subjectID',$subjectid);
	// 	$result=$this->db->get()->result();
	// 	return $result;
	// }
	public function getunit($a)
	{
		$this->db->select('unitID');
		$this->db->from('unit_topic');
		$this->db->where('topicID',$a);
		$result=$this->db->get()->row();
		return $result;
	}
	public function checkcompleteunit($subjectid,$yearsOrSemester=NULL,$year=NULL)
	{
		// print_r($year);die;
		$currentyear=$this->session->userdata('sessionyear');
		$currentmonth=date('m');
        $this->db->select('*');
		$this->db->from('complete_topic');
		$this->db->where('subjectid',$subjectid);
		if($yearsOrSemester != NULL)
		{
			if($yearsOrSemester=="1st_Year" || $yearsOrSemester=="2nd_Year" || $yearsOrSemester=="3rd_Year")
			{
				$this->db->where('year',$currentyear);				
			}
			else
			{
				$this->db->where('year',$currentyear);				
			}
		}
		else
		{
			$this->db->where('year',$currentyear);
		}

		$result=$this->db->get()->result();
		return $result;
	}
	public function getalltopicbyunitid($checkunitid)
	{
		$this->db->select('*');
		$this->db->from('unit_topic');
		$this->db->where('unitID',$checkunitid);
		$result=$this->db->get()->result();
		return $result;
	}
	public function getsubjectprogrees($subjectid)
	{
		$year = $this->session->userdata('sessionyear');
		$month = date('m');
		
       	$this->db->select('c.topicid,count(ut.unitID) as totalunit');
		$this->db->from('units as u');
		$this->db->join('complete_topic as c','c.subjectid=u.subjectID','left');
	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
	    $this->db->where('u.subjectID',$subjectid);
	    $this->db->where('c.year',$year);
	    $result=$this->db->get()->result();
	    // print_r($result);
	    // exit;
	    return $result;
	    // $this->db->where('s.subjectID',$value['subjectID']);
	}
	public function getoptionalsubjectprogrees($subjectID)
	{
		$year = date('Y');
		$month = date('m');
       $this->db->select('c.topicid,count(ut.unitID) as totalunit');
    		$this->db->from('units as u');
    		$this->db->join('complete_topic as c','c.subjectid=u.subjectID','left');
    	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
    	    $this->db->where('u.subjectID',$subjectID);
    	    $this->db->where('c.year',$year-1);//remove 1
    	    $result=$this->db->get()->result();
		    return $result;
	}
	public function getsubjectname($subjectid)
	{
		$this->db->select('subject');
		$this->db->from('subject');
		$this->db->where('subjectID',$subjectid);
		$result=$this->db->get()->row();
		    return $result;
	}
	public function get_student_info($studentid)
	{
       
		
			$query = $this->db->get_where("student", array('studentID' => $studentid));
			return $query->row();
		
		
	}
	function getSubjectforvideo($classesID,$sub_courses,$yearsOrSemester){
		
		$this->db->where('classesID',$classesID);
		$this->db->where('sub_coursesID',$sub_courses);
		$this->db->where('yearsOrSemester',$yearsOrSemester);
		return $this->db->get('subject')->result();

	   			
	}
	public function getstudentsubjectwise($getClasses,$getSemester)
	{
		$this->db->select('name,studentID');
		$this->db->where('classesID',$getClasses);
		$this->db->where('yearsOrSemester',$getSemester);
	   	$this->db->where('status',1);
	   	$this->db->order_by('name','ASC');
		$a = $this->db->get('student')->result_array();
		foreach($a as $key => $value)
		{
            $this->db->select('subject,subjectID');
			$this->db->from('subject');
			$this->db->where('classesID',$getClasses);
			$this->db->where('yearsOrSemester',$getSemester);
			$this->db->where('subject_mode',1);
			$a[$key]['object']= $this->db->get()->result();
		}
		// echo "<pre>";
		// print_r($a);die;
		return $a;
	}
	// public function getallsubjectwise($getClasses,$getSemester)
	// {
	// 	$this->db->select('subject,subjectID');
	// 	$this->db->from('subject');
	// 	$this->db->where('classesID',$getClasses);
	// 	$this->db->where('yearsOrSemester',$getSemester);
	// 	$this->db->where('subject_mode',1);
	// 	return $this->db->get()->result();
	// }
	public function checkoptionalsubjectid($studentid,$subjectid)
	{
		// print_r($subjectid);die;
		$this->db->select('studentID,subjectID,ontionalsubjectID');
		$this->db->from('optionalsubject');
		$this->db->where('subjectID',$subjectid);
		$this->db->where('studentID',$studentid);
		
		return $this->db->get()->row();
	}
	public function saveoptionalsubject($optionalsubject,$subjectid,$studentid)
	{
		$this->db->select('studentID,subjectID');
		$this->db->from('optionalsubject');
		$this->db->where('subjectID',$subjectid);
		$this->db->where('studentID',$studentid);
		
		$a=$this->db->get()->row();
		if($a)
		{
            return true;
		}
		else
		{
			$this->db->insert('optionalsubject',$optionalsubject);
		    return true;
		}
		
	}
	public function updateoptionalsubject($optionalsubject,$studentid)
	{
		$this->db->where('ontionalsubjectID',$studentid);
		$this->db->update('optionalsubject',$optionalsubject);
		return true;
	}
	public function getalldataoptionalsubject()
	{
		$this->db->select('optionalsubject.subjectID,subject.subject,optionalsubject.studentID');
		$this->db->from('optionalsubject');
		$this->db->join('subject','subject.subjectID=optionalsubject.subjectID','left');
		return $this->db->get()->result();

	}
	public function savetopicdatewise($savedata,$a)
	{
		$this->db->select('topicid');
		$this->db->from('topicwisedateshow');
		$this->db->where('topicid',$a);
		$b=$this->db->get()->row();
        if($b)
        {
        	// $this->db->insert('topicwisedateshow',$savedata);
		    return true;
        }
        else
        {
        	$this->db->insert('topicwisedateshow',$savedata);
		    return true;
        }
		
	}
	public function gettopicdate($topicID)
	{
		$this->db->select('date,topicid');
		$this->db->from('topicwisedateshow');
		$this->db->where('topicid',$topicID);
		return $this->db->get()->row();

	}
	public function checkattendance($topicdate,$subjectid)
	{
		$this->db->select('count(subjectID) as totaldays');
		$this->db->from('attendance_student');
		$this->db->where('atd_date',$topicdate);
		$this->db->where('subjectID',$subjectid);
		// $this->db->or_where('atd',1);
		return $this->db->get()->row();
		// print_r($z);die;
	}
	public function countattendance($topicdate,$subjectid)
	{
		$this->db->select('count(atd) as totalpresent');
		$this->db->from('attendance_student');
		$this->db->where('atd_date',$topicdate);
		$this->db->where('subjectID',$subjectid);
		$this->db->where('atd',1);
		return $this->db->get()->row();
	}
	public function deleteoptionalsubject($checkstudentid)
	{
		$this->db->where('ontionalsubjectID',$checkstudentid);
		$this->db->delete('optionalsubject');
		return true;
	}
	public function getunitsname($subjectID)
	{
		$this->db->select('*');
		$this->db->from('units');
		$this->db->where('subjectID',$subjectID);
		return $this->db->get()->result();
	}
	public function get_subject_by_session($adminID,$classesID,$yearsOrSemester)
	{
		$where = "classesID = '".$classesID."' and yearsOrSemester = '".$yearsOrSemester."' and status = '1' and adminID = '".$adminID."'";
		$sql = $this->db->select('subjectID,subject')->from('subject')->where($where)->get()->result();
		return $sql;
	}
	public function getallstudentattendance($checkinformation)
	{
		
			
			$this->db->select('att.atd,att.studentID,att.atd_date');
			$this->db->from('attendance_student as att');
			$this->db->where($checkinformation);
			$this->db->where('att.year',$year);
			$this->db->group_by('att.atd_date');
			$a= $this->db->get()->result();
		
		// $b[]=$a[$key]['object'];
		
		// echo "<pre>";
		// print_r($a);die;
		return $a;
	}
	

}

