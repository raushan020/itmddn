<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Teacher_m extends Admin_Model {



	protected $_table_name = 'teacher';



	protected $_primary_key = 'teacherID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "name asc";



	function __construct() {



		parent::__construct();



	}



	function get_username($table, $data=NULL) {



		$query = $this->db->get_where($table, $data);



		return $query->result();



	}



	function get_teacher($array=NULL, $signal=FALSE) {



		$query = parent::get($array, $signal);

		return $query;

	}

	

 function get_teacher_all($adminID){

	$this->db->where('status',1);

	$this->db->where('adminID',$adminID);

	  return $this->db->get('teacher')->result();

 }



	function get_teacher_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");

	    $this->db->where('adminID',$adminID);

	    $this->db->where('status',1);

		if(isset($_POST["search"]["value"]))  

           {  



			$searches  =  $_POST["search"]["value"];



			$order_column = array( 



                            0 =>'sn', 

                            1 =>'name',

                            2 => 'username',

                            3 => 'email',

                            4=> 'action'

                        );



           $where = "(teacher.name LIKE '%$searches%' OR teacher.username LIKE '%$searches%' OR teacher.email LIKE '%$searches%')";

           $this->db->where($where);

        

           

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('teacher.teacherID', 'DESC');  

           } 

          

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       } 



	}

	

	   function make_datatables(){  

        $this->get_teacher_ClgAdmin();  

   	    $query = $this->db->get('teacher'); 

		return $query->result();  

      } 

	

	function get_teacher_no($adminID)

	{

		// raushan

		 if(isset($_POST["search"]["value"]))  

            {  



		 	$searches  =  $_POST["search"]["value"];



		 	$order_column = array( 



                             0 =>'sn', 

                             1 =>'name',

                             2 => 'username',

                             3 => 'email',

                             4=> 'action'

                         );



            $where = "(teacher.name LIKE '%$searches%' OR teacher.username LIKE '%$searches%' OR teacher.email LIKE '%$searches%')";

          $this->db->where($where);

       }

		 	// raushan end



		$this->db->where('adminID',$adminID);

		   $this->db->where('status',1);

	    $query = $this->db->get('teacher'); 

		return $query->num_rows();

	}



	function get_teacher_ClgAdminID($adminID) {

	$this->db->where('adminID',$adminID);

	   $this->db->where('status',1);

	$query = $this->db->get('teacher');

	 return $query->result();

	}



	function get_order_by_teacher($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}





	function get_single_teacher($array) {



		$query = parent::get_single($array);



		return $query;



	}





	function insert_teacher($array) {



		$error = parent::insert($array);

		return TRUE;



	}



	function update_teacher($data, $id = NULL) {



		parent::update($data, $id);



		return $id;



	}



	function delete_teacher($id){



$data  = array(

	'status' =>2,

	 );

	   // $this->db->where('teacherID',$id);

	   // $this->db->update('teacher',$data);

$this->db->query("UPDATE teacher
	INNER JOIN student ON teacher.teacherID = student.counsellor
	
	
	set student.status = 2, teacher.status = 2 where teacher.teacherID = $id");

	}



	function hash($string) {



		return parent::hash($string);



	}

	function get_lectureTiming($adminID) {
		$this->db->select('*');
		$this->db->from('lectureTiming');
		$this->db->where('adminID',$adminID);
		$this->db->order_by("STR_TO_DATE(start_time,'%H:%i')");
		$sql = $this->db->get();
		$result = $sql->result();
		return $result;
	}



}



/* End of file teacher_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/teacher_m.php */



