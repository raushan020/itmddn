<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Year_m extends Admin_Model {

	// protected $_table_name = 'subject';
	// protected $_primary_key = 'subjectID';
	// protected $_primary_filter = 'intval';
	// protected $_order_by = "classesID asc";

	function __construct() {
		parent::__construct();
	}

	function get_year() {

		$query = $this->db->get('year_wise');
		return $query->result_array();
		
	}


}

/* End of file subject_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/subject_m.php */
