<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Classes_m extends Admin_Model {


	protected $_table_name = 'classes';



	protected $_primary_key = 'classesID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "classes_numeric asc";







	function __construct() {



		parent::__construct();



	}



function get_single_invoice_department_data($department){
		$this->db->where('departmentID',$department);
		$query = $this->db->get('department')->row();
		return $query;
}



function insert_sub_courses($id,$adminID,$sitenameString){

 

 $subcourses =  $this->input->post('subcourse');



for ($i=0; $i <count($this->input->post('subcourse')) ; $i++) { 



				    $digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int

				    $char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string 						

 					$firstCharacter = substr($sitenameString, 0, 2);



 					$SubcourseCode      =   strtoupper($firstCharacter).'-'.$char.$digit; 



$data = array(

'sub_course'=>$subcourses[$i],

'subCourseCode'=>$SubcourseCode,

'classesID'=>$id,

'adminID'=>$this->session->userdata("adminID")

	);



  $this->db->insert('sub_courses' ,$data);



}

return true;



}





	function get_teacher() {



		$this->db->select('*')->from('teacher');



		$query = $this->db->get();



		return $query->result();



	}







	function get_classes_byAdmin($adminID) {

	$this->get_course_ClgAdmin(); 

	$result = $this->db->get();

	$query = $result->result();

	return $query;



	}

	

	function get_course_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('*');
		$this->db->from('classes');

		$this->db->join('department', 'classes.departmentID = department.departmentID', 'LEFT');

	    // $this->db->where('adminID',$adminID);	    

	   if ($this->session->userdata('classesIDForfilterCourses')) {

		 $this->db->where('classesID', $this->session->userdata('classesIDForfilterCourses'));

		}

	   if ($this->session->userdata('durationID')) {

		 $this->db->where('duration', $this->session->userdata('durationID'));

		}

	   if ($this->session->userdata('ActiveClasess')) {

		 $this->db->where('status', $this->session->userdata('ActiveClasess'));

		}


	   if ($this->session->userdata('DraftClasess')) {

		 $this->db->where('status', 0);

		}



	   if ($this->session->userdata('TrashClasess')) {

		 $this->db->where('status', $this->session->userdata('TrashClasess'));

		}	



     if (empty($this->session->userdata('DraftClasess')) && empty($this->session->userdata('TrashClasess'))) {

           $this->db->where('classes.status',1);

            }





		if(isset($_POST["search"]["value"]))  

           {  



			$searches  =  $_POST["search"]["value"];



			$order_column = array( 



                            0 =>'sn', 

                            1 =>'classesID',

                            2=> 'classes',

                            3=> 'duration',

                            4=> 'mode',

                            5=> 'fee',

							6=> 'department_name'

                        );

           $where = "(classes.classes LIKE '%$searches%' OR classes.duration LIKE '%$searches%' OR classes.mode LIKE '%$searches%' OR classes.fee LIKE '%$searches%' OR classes.classesID LIKE '%$searches%')";

           $this->db->where($where);



        

           

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('classes.classesID', 'DESC');  

           } 

          

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       } 



	}	

	function get_classes_bysuperAdmin() {

  

	$result = $this->db->where('status',1)->get('classes');

	$query = $result->result();
// print_r($query);die;
		// $query = parent::get($array, $signal);

		return $query;

	}

	



		function get_classes_active() {

  			$this->db->where('status',1);

	$result = $this->db->get('classes');

	$query = $result->result();

		// $query = parent::get($array, $signal);

		return $query;

	}
	
	function courses_notification()
	{
		$query = $this->db->query('select create_usertype, notification, notification_date from classes where status = 1 and notification = "1" and notification_date = "'.date('Y-m-d').'" group by notification_date');
    	if($query->num_rows() > 0)
    	{
    		return $query->result();
    	}
    	else
    	{
    		echo "";
    	}
	}





	function get_cousre_no($adminID)

	{			

			if(isset($_POST["search"]["value"]))  

           {  



			$searches  =  $_POST["search"]["value"];



			$order_column = array( 



                            0 =>'sn', 

                            1 =>'classesID',

                            2=> 'classes',

                            3=> 'duration',

                            4=> 'mode',

                            5=> 'fee',

                            6=> 'department_name'
							

                        );

           $where = "(classes.classes LIKE '%$searches%' OR classes.duration LIKE '%$searches%' OR classes.mode LIKE '%$searches%' OR classes.fee LIKE '%$searches%' OR classes.classesID LIKE '%$searches%')";

           $this->db->where($where);

}



		 	    $this->db->where('adminID',$adminID);

	    

	   if ($this->session->userdata('classesIDForfilterCourses')) {

		 $this->db->where('classesID', $this->session->userdata('classesIDForfilterCourses'));

		}



	   if ($this->session->userdata('durationID')) {

		 $this->db->where('duration', $this->session->userdata('durationID'));

		}



	   if ($this->session->userdata('ActiveClasess')) {

		 $this->db->where('status', $this->session->userdata('ActiveClasess'));

		}



	   if ($this->session->userdata('DraftClasess')) {

		 $this->db->where('status', 0);

		}



	   if ($this->session->userdata('TrashClasess')) {

		 $this->db->where('status', $this->session->userdata('TrashClasess'));

		}	

		

     if (empty($this->session->userdata('DraftClasess')) && empty($this->session->userdata('TrashClasess'))) {

           $this->db->where('classes.status',1);

            }



		$result = $this->db->get('classes');

		$query = $result->num_rows();

		return $query;

		

	}



function get_classes($array=NULL, $signal=true) {


$adminID  = $this->session->userdata('adminID');  

   $this->db->where('adminID',$adminID);
   $this->db->where('classesID',$array);
   $result = $this->db->get('classes');
   $query = $result->row();



		// $query = parent::get($array, $signal);

		// $query = parent::get($array, $signal);



		return $query;



	}

	function get_order_by_classes($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}



function get_SubCourses($id){

	$this->db->where('classesID',$id);

// 	$this->db->query("UPDATE sub_courses
// 	INNER JOIN student ON sub_courses.sub_coursesID = student.sub_coursesID
// 	INNER JOIN subject ON sub_courses.sub_coursesID = subject.sub_coursesID 
// 	INNER JOIN ets_quiz ON sub_courses.sub_coursesID = ets_quiz.sub_coursesID
	
// 	 set student.status = 1 
// ,ets_quiz.status = 1, subject.status = 1 ,sub_courses.status = 2 where sub_courses.classesID = $id");

	$this->db->where('status',1);

	$query = $this->db->get('sub_courses');

	return $query->result();

}

function get_SubCourses_trash($id){

	$this->db->where('classesID',$id);
// 	$this->db->query("UPDATE sub_courses
// 	INNER JOIN student ON sub_courses.sub_coursesID = student.sub_coursesID
// 	INNER JOIN subject ON sub_courses.sub_coursesID = subject.sub_coursesID 
// 	INNER JOIN ets_quiz ON sub_courses.sub_coursesID = ets_quiz.sub_coursesID
	
// 	 set student.status = 2 
// ,ets_quiz.status = 2, subject.status = 2 ,sub_courses.status = 2 where sub_courses.classesID = $id");

	$this->db->where('status',2);

	$query = $this->db->get('sub_courses');

	return $query->result();

}



function GetCoursesByParam($array){



	$this->db->where($array);

	$query = $this->db->get('sub_courses');

	return $query->result();



}



function addSubCourse($classID,$sub_course,$subCourseCode )

{

	$data = array(

	'classesID'=>$classID,

	'sub_course'=>$sub_course,

	'adminID'=>$this->session->userdata("adminID"),

	'subCourseCode'=>$subCourseCode ,

	);

	$this->db->insert('sub_courses',$data);

}



function update_sub_courses($id){



	$this->db->where('classesID',$id);

	$this->db->delete('sub_courses');



$subcourses =  array_filter($this->input->post('subcourse'));



for ($i=0; $i <count($subcourses) ; $i++) { 

  

$data = array(

'sub_course'=>$subcourses[$i],

'classesID'=>$id,

'adminID'=>$this->session->userdata("adminID")

	);



  $this->db->insert('sub_courses' ,$data);



}

return true;



}



	function insert_classes($array) {



		$error = parent::insert($array);



		return $error;



	}







	function update_classes($data, $id = NULL) {



		parent::update($data, $id);



		return $id;



	}







	public function delete_classes($id){



		parent::delete($id);



	}







	function get_order_by_numeric_classes() {



		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');



		$query = $this->db->get();



		return $query->result();



	}







    function get_single_classes($id) {

    	

	$this->db->where('classesID',$id);

	$query = $this->db->get('classes');

   return  $query->row();



	}


	function get_single_department($id) {

    	

	$this->db->where('departmentID',$id);

	$query = $this->db->get('classes');

   return  $query->result();



	}




	function all_count(){

		$this->db->where('status',1);

		$this->db->or_where('status',0);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function ActiveClasess_count(){

		$this->db->where('status',1);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function DraftClasess_count(){

		$this->db->where('status',0);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function TrashClasess_count(){

		$this->db->where('status',2);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function get_check_classesID($id){

		$this->db->where('classesID',$id);

		$studentID = $this->db->get('student')->num_rows();

		if ($studentID>0) {

		 return $studentID;	

		}else{

			$this->db->where('classesID',$id);

		    $subjectID = $this->db->get('subject')->num_rows();

		    if ($subjectID>0) {

		       return $studentID;	

		    }else{

		    $this->db->where('classesID',$id);

		    $quizID = $this->db->get('ets_quiz')->num_rows();

		    return $quizID;

		    }

		}

	}



		function check_classesID_Subcourse($id){

			$this->db->where('classesID',$id);

			$sub_courseID = $this->db->get('sub_courses')->num_rows();

		

				return $sub_courseID;

			

		}


}







/* End of file classes_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/classes_m.php */