<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professor_m extends Admin_Model {

	protected $_table_name = 'professor';

	protected $_primary_key = 'professorID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "name asc";

	function __construct() {

		parent::__construct();

	}

	function get_username($table, $data=NULL) {

		$query = $this->db->get_where($table, $data);

		return $query->result();

	}

	function get_professor($array=NULL, $signal=FALSE) {

		$query = parent::get($array, $signal);
		return $query;
	}
	function get_professor_bycustommail()
	{
		$this->db->select('*');
		$this->db->from('professor');
		$this->db->where('status',1);
		return $this->db->get()->result();
	}

	function change_password_with_professor_by_superadmin($id,$newPassword){
		
		$data = array(
			'password' => $newPassword, 
		);
		$this->db->where('professorID',$id);
		$this->db->update('professor',$data);
	}

function total_lecture(){
	$loginuserID  =$this->session->userdata('loginuserID');
	return $this->db->where('professorID',$loginuserID)->get('subject')->num_rows();
}
	function get_professor_with_join($array=NULL, $signal=FALSE) {
		$this->db->join('department','professor.departmentID=department.departmentID','LEFT');
		$query = parent::get($array, $signal);
		return $query;
	}



	function get_single_professor($array) {
$this->db->join('designation', 'professor.designationID = designation.designationID','LEFT');
$this->db->join('department','professor.departmentID=department.departmentID','LEFT');
		$query = parent::get_single($array);

		return $query;

	}

	function updatemobilenumberbyprofessor($loginuserID,$adminID,$number)
	{
		$this->db->where(array('adminID' =>$adminID,'professorID'=>$loginuserID));
		$this->db->update('professor',$number);
		return true;

	}
	function updatemobilenumberbyusers($loginuserID,$adminID,$number)
	{
		$this->db->where(array('adminID' =>$adminID,'userID'=>$loginuserID));
		$this->db->update('user',$number);
		return true;
	}


		function get_professor2($array=NULL, $signal=FALSE) {
		
		return $this->db->where($array)->get('professor')->row();
	}
	
 function get_professor_all($adminID){
	$this->db->where('status',1);
	$this->db->where('adminID',$adminID);
	  return $this->db->get('professor')->result();
 }

function get_department($departmentID){

	$this->db->where('department');
	$this->db->where('departmentID',$departmentID);
		return $this->db->get('department')->row();

}


	function get_professor_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");
		$this->db->join('designation', 'professor.designationID = designation.designationID', 'left');
		$this->db->join('department', 'professor.departmentID = department.departmentID', 'left');
		if ($this->session->userdata('ActiveProfessor')) {
		 $this->db->where('professor.status', $this->session->userdata('ActiveProfessor'));
		}
	   	if ($this->session->userdata('DraftProfessor')) {
		 $this->db->where('professor.status', 0);
		}
	   	if ($this->session->userdata('TrashProfessor')) {
		 $this->db->where('professor.status', 2);
		}
		if (empty($this->session->userdata('DraftProfessor')) && empty($this->session->userdata('TrashProfessor'))) {
           $this->db->where('professor.status',1);
        }
	    $this->db->where('adminID',$adminID);
	 
		if(isset($_POST["search"]["value"]))  
           {  

			$searches  =  $_POST["search"]["value"];

			$order_column = array( 

                            0 => 'sn', 
                            1 => 'name',
                            2 => 'username',
                            3 => 'designation_name',
                            4 => 'department_name',
                            5 => 'email',
                            6 => 'action'
                        );

           $where = "(professor.name LIKE '%$searches%' OR professor.username LIKE '%$searches%' OR professor.email LIKE '%$searches%')";
           $this->db->where($where);
        
           
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('professor.professorID', 'DESC');  
           } 
          
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       } 

	}
	
	   function make_datatables(){  
	   	
        $this->get_professor_ClgAdmin();  
   	    $query = $this->db->get('professor'); 
		return $query->result();  
      } 
	
	function get_professor_no($adminID)
	{
		if ($this->session->userdata('ActiveProfessor')) {
		 $this->db->where('status', $this->session->userdata('ActiveProfessor'));
		}
	   	if ($this->session->userdata('DraftProfessor')) {
		 $this->db->where('status', $this->session->userdata('DraftProfessor'));
		}
	   	if ($this->session->userdata('TrashProfessor')) {
		 $this->db->where('status', $this->session->userdata('TrashProfessor'));
		}
		if (empty($this->session->userdata('DraftProfessor')) && empty($this->session->userdata('TrashProfessor'))) {
           $this->db->where('status',1);
        }
		// raushan
		 if(isset($_POST["search"]["value"]))  
            {  

		 	$searches  =  $_POST["search"]["value"];

		 	$order_column = array( 

                            0 => 'sn', 
                            1 => 'name',
                            2 => 'username',
                            3 => 'designation_name',
                            4 => 'department_name',
                            5 => 'email',
                            6 => 'action'
                         );

            $where = "(professor.name LIKE '%$searches%' OR professor.username LIKE '%$searches%' OR professor.email LIKE '%$searches%')";
          $this->db->where($where);
       }
		 	// raushan end


		$this->db->where('adminID',$adminID);
		   
	    $query = $this->db->get('professor'); 
		return $query->num_rows();
	}

	function get_professor_ClgAdminID($adminID) {
	$this->db->where('adminID',$adminID);
	   $this->db->where('status',1);
	$query = $this->db->get('professor');
	 return $query->result();
	}

	function get_order_by_professor($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}

	function getstudentforcondition($getClasses,$getSubcourse,$getSemester,$getSubject)
	{
        $this->db->select('subject_mode');
        $this->db->from('subject');
        $this->db->where('subjectID',$getSubject);
        $a=$this->db->get()->row();

        if($a->subject_mode==0)
        {
        	$this->db->select('student.name,student.studentID,student.roll,student.studentactive');
			$this->db->where('student.classesID',$getClasses);
			$this->db->where('student.sub_coursesID',$getSubcourse);
			$this->db->where('student.yearsOrSemester',$getSemester);
		   	$this->db->where('student.status',1);
		   	$this->db->order_by('student.roll','ASC');
			$query = $this->db->get('student');
			 return $query->result();
        }
        else
        {
        	$this->db->select('student.name,optionalsubject.studentID,student.roll');
        	$this->db->from('optionalsubject');
        	$this->db->join('student','student.studentID=optionalsubject.studentID','left');
			$this->db->where('optionalsubject.courseID',$getClasses);
			$this->db->where('optionalsubject.yearsOrSemester',$getSemester);
			$this->db->where('optionalsubject.subjectID',$getSubject);
		   	$this->db->where('student.status',1);
		   	$this->db->order_by('student.roll','ASC');
			$query = $this->db->get();
			return $query->result();
        }

		
	}

	function getstudentforconditiononlinecheck($getClasses,$getSubcourse,$getSemester,$getSubject)
	{
        $this->db->select('subject_mode');
        $this->db->from('subject');
        $this->db->where('subjectID',$getSubject);
        $a=$this->db->get()->row();

        if($a->subject_mode==0)
        {
        	$this->db->select('student.name,student.studentID,student.roll,student.studentactive');
			$this->db->where('student.classesID',$getClasses);
			$this->db->where('student.sub_coursesID',$getSubcourse);
			$this->db->where('student.yearsOrSemester',$getSemester);
		   	$this->db->where('student.status',1);
		   	$this->db->group_by('student.studentID');
		   	$this->db->order_by('student.roll','ASC');
			$query = $this->db->get('student');
			 return $query->result_array();
        }
        else
        {
        	$this->db->select('student.name,optionalsubject.studentID,student.roll');
        	$this->db->from('optionalsubject');
        	$this->db->join('student','student.studentID=optionalsubject.studentID','left');
			$this->db->where('optionalsubject.courseID',$getClasses);
			$this->db->where('optionalsubject.yearsOrSemester',$getSemester);
			$this->db->where('student.yearsOrSemester',$getSemester);
			$this->db->where('optionalsubject.subjectID',$getSubject);
		   	$this->db->where('student.status',1);
		   	$this->db->group_by('student.studentID');
		   	$this->db->order_by('student.roll','ASC');
			$query = $this->db->get();
			return $query->result_array();
        }

		
	}


	function getstudent_from_atnd($getSubject,$loginuserID,$date){
		$studentonline=$this->db->select('activestatus,year_mode')->where(array('subjectID' =>$getSubject,'atd_date'=>$date))->get('attendance_student')->row();
		if($studentonline)
		{
			// print_r($studentonline);die;
			if($studentonline->year_mode==5)
			{
				// print_r("1");die;
				$this->db->from('student');
				$this->db->join('attendance_student','student.studentID=attendance_student.studentID','INNER');
				$this->db->where('attendance_student.subjectID',$getSubject);
				$this->db->where('attendance_student.activestatus',3);
			   $this->db->where('attendance_student.atd_date',$date);
			   $this->db->order_by('student.roll','ASC');
			   return $this->db->get()->result();
		   }
		   else
		   {
		   	// print_r("12");die;
		   	$this->db->from('student');
			$this->db->join('attendance_student','student.studentID=attendance_student.studentID','INNER');
			$this->db->where('attendance_student.subjectID',$getSubject);
			$this->db->order_by('student.roll','ASC');
			// $this->db->where('attendance_student.professorID',$loginuserID);
		   $this->db->where('attendance_student.atd_date',$date);
		   $this->db->group_by('attendance_student.studentID');
		   return $this->db->get()->result_array();
		   // print_r($a);die;	
		   }
		}
		else
		{
			// print_r("123");die;
			$this->db->from('student');
			$this->db->join('attendance_student','student.studentID=attendance_student.studentID','INNER');
			$this->db->where('attendance_student.subjectID',$getSubject);
			$this->db->order_by('student.roll','ASC');
			// $this->db->where('attendance_student.professorID',$loginuserID);
		   $this->db->where('attendance_student.atd_date',$date);
		   $this->db->group_by('attendance_student.studentID');
		   return $this->db->get()->result_array();
		   // print_r($a);die;	
		}
	


	}

	
	function get_classes($departmentID){
		$this->db->where('departmentID',$departmentID);
		$this->db->where('status',1);
		return $this->db->get('classes')->result();
	
	}


		function get_student($departmentID){
		$this->db->join('student','classes.classesID=student.classesID','INNER');	
		$this->db->where('departmentID',$departmentID);
		return $this->db->get('classes')->result();
	
	}

	function get_subject($departmentID){
		$this->db->join('subject','classes.classesID=subject.classesID','INNER');	
		$this->db->where('departmentID',$departmentID);
		return $this->db->get('classes')->result();
	
	}




	function get_professor_datatables() {
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
        $this->db->join('sub_courses', 'sub_courses.sub_coursesID=subject.sub_coursesID', 'left');
		$this->db->join('classes', 'classes.classesID = subject.classesID', 'left');
	    $this->db->where('subject.professorID',$loginuserID);
		$query = $this->db->get('subject')->result();

		return $query;
	}

	function get_attendance() {
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		
		$this->db->from('attendance_student');
		$this->db->join('student', 'attendance_student.studentID = student.studentID', 'INNER');
	    
		$query = $this->db->get()->result();
		return $query;
	}


	function insert_professor($array) {

		$error = parent::insert($array);
		return $this->db->insert_id();
		// return TRUE;

	}

	function update_professor($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}

	function delete_professor($id){

$data  = array(
	'status' =>2,
	 );
	   $this->db->where('professorID',$id);
	   $this->db->update('professor',$data);
	}

	function insert_attendance($array) {

		$this->db->insert('attendance_student',$array);
		return TRUE;

	}

	function hash($string) {

		return parent::hash($string);

	}
	// and subject.status = '1'
	function get_professor_data($loginuserID)
	{
		$currentdate=date('m');
		$currentyear=date('Y');
		$this->db->select('professor_lecture.id,professor_lecture.department_id,professor_lecture.course_id,professor_lecture.semester,professor_lecture.subject_id,professor_lecture.professor_id,professor_lecture.days,professor_lecture.times,classes.classes,subject.subject,professor.name,department.department_name,professor_lecture.year_mode,professor_lecture.year,professor_lecture.status');
		$this->db->from('professor_lecture');
		$this->db->join('classes','professor_lecture.course_id = classes.classesID','inner');
		$this->db->join('subject','professor_lecture.subject_id = subject.subjectID','inner');
		$this->db->join('professor','professor_lecture.professor_id = professor.professorID','inner');
		$this->db->join('department','professor_lecture.department_id = department.departmentID','inner');
		$where = "professor_lecture.professor_id = '".$loginuserID."' and subject.status = '1' and professor.status = '1' ";
		$this->db->where($where);
		// if($currentdate <= 6)
		// {
		// 	$year_mode = array(2,3);
		// 	$year = array($currentyear,$currentyear-1);
		// 		$this->db->where('professor_lecture.year_mode',$year_mode);
		// 		// $this->db->or_where('professor_lecture.year_mode',3);
		// 		// $this->db->where('professor_lecture.year',$year);
		// 		// $this->db->where('professor_lecture.year',$currentyear-1);
			
		// }
		// else
		// {
		// 	$this->db->where('professor_lecture.year_mode',1);
		// 	// $this->db->or_where('professor_lecture.year_mode',4);
		// 	$this->db->where('professor_lecture.year',$currentyear);
			
		// }
		
		$this->db->group_by(array('professor_lecture.subject_id','professor_lecture.times'));
		$sqls = $this->db->get();
		if($sqls->num_rows() > 0)
		{
			$results = $sqls->result();
			// print_r($results);die;
			return $results;
		}
		else
		{
			$this->session->set_flashdata('msg','<p style="color:red;text-align:center;">No record found!</p>');
		}
	}
	function get_todayattendance($loginuserID)
	{
		$todaydate=date('Y-m-d');
		$this->db->select('attendance_student.subjectID,subject.subject');
		$this->db->from('attendance_student');
		$this->db->join('subject','subject.subjectID=attendance_student.subjectID','left');
		$this->db->where('attendance_student.professorID',$loginuserID);
		$this->db->where('attendance_student.atd_date',$todaydate);
		$this->db->where('attendance_student.year_mode !=',5);
		$this->db->group_by('subjectID');
		$a=$this->db->get()->result_array();
		foreach ($a as $key => $value)
		{
			$this->db->select('attendance_student.atd,attendance_student.subjectID,attendance_student.atd_date');
			$this->db->from('attendance_student');
			$this->db->where('attendance_student.professorID',$loginuserID);
			$this->db->where('attendance_student.subjectID',$value['subjectID']);
			$this->db->where('attendance_student.atd_date',$todaydate);
			$this->db->where('attendance_student.year_mode !=',5);
			$this->db->group_by('attendance_student.studentID');
		    $a[$key]['object']=$this->db->get()->result();
		}
		// echo "<pre>";
		// print_r($a);die;
		return $a;
	}
	
	function professors_notification()
    {
    	$query = $this->db->query('select create_usertype, notification, notification_date from professor where status = 1 and notification = "1" and notification_date = "'.date('Y-m-d').'"');
    	if($query->num_rows() > 0)
    	{
    		return $query->result();
    	}
    	else
    	{
    		echo "";
    	}
    }
    public function checksubjectid($subjectid,$year,$year_mode)
    {
    	$this->db->select('subjectid');
    	$this->db->from('complete_topic');
    	$this->db->where('subjectid',$subjectid);
    	$this->db->where('year',$year);
    	$this->db->where('year_mode',$year_mode);
    	return $this->db->get()->row();
    }
    public function insertcompletetopic($insertcompletetopic)
    {
    	$this->db->insert('complete_topic',$insertcompletetopic);
    	return TRUE;
    }
    public function updatecompletetopic($updatecompletetopic,$subjectid,$year,$year_mode)
    {
    	$this->db->where('subjectid',$subjectid);
    	$this->db->where('year',$year);
    	$this->db->where('year_mode',$year_mode);
    	$this->db->update('complete_topic',$updatecompletetopic);
    	return TRUE;
    }
    public function getsubjectandtopicyearwise($loginuserID,$year = NULL,$getSemester= NULL,$month= NULL)
    {
    	// $this->load->library('subquery');
    	$currentyear=$this->session->userdata('sessionyear');
    	$currentmonth=date('m');
    	$this->db->select('s.subjectID,s.subject,s.yearsOrSemester,c.classes');
    	$this->db->from('subject as s');
    	$this->db->join('classes as c','c.classesID=s.classesID','left');
    	$this->db->where('s.professorID',$loginuserID);
    	if($getSemester)
    	{
    		$this->db->where('s.yearsOrSemester',$getSemester);
    	}
    	
    	$this->db->where('s.status',1);
	    
    	$a= $this->db->get()->result_array();
    	
    	foreach ($a as $key => $value)
    	{
    		// print_r($a->subjectID);die;
    		$this->db->select('s.professorID,s.subjectID,c.unitid,c.topicid,s.subject,u.unitID,count(ut.unitID) as totalunit');
    		$this->db->from('subject as s');
    		$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	    $this->db->join('complete_topic as c','c.subjectid=s.subjectID','left');
    	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
    	    if($year != NULL)
    	    {    	    	
    	    	$this->db->where('c.year',$year);
    	    }
    	    else
    	    {
    	    	$this->db->where('c.year',$currentyear);
    	    }
    	    if($month != NULL)
    	    {
    	    	$dateselect =$year.'-'.$month;
    	    	$this->db->like('c.create_at',$dateselect);
    	    }
    	    $this->db->where('c.professorID',$loginuserID);
    	    $this->db->where('s.subjectID',$value['subjectID']);
    	    $this->db->where('s.yearsOrSemester',$getSemester);
            $this->db->where('s.status',1);
    	$a[$key]['object']= $this->db->get()->result();
    	
    		
    	}
    	 return $a; 
    }
    public function getsubjectandtopicyearwiseloadpage($loginuserID,$year)
    {
    	// $this->load->library('subquery');
    	$currentyear=date('Y');
    	$currentmonth=date('m');
    	$yearwise= array('1st_Year','2nd_Year','3rd_Year');
    	$this->db->select('s.subjectID,s.subject,s.yearsOrSemester,c.classes');
    	$this->db->from('subject as s');
    	$this->db->join('classes as c','c.classesID=s.classesID','left');
    	$this->db->where('s.professorID',$loginuserID);
    	$this->db->where_in('s.yearsOrSemester',$yearwise);    	
    	
    	$this->db->where('s.status',1);
	    
    	$a= $this->db->get()->result_array();
    	
    	foreach ($a as $key => $value)
    	{
    		// print_r($a->subjectID);die;
    		$this->db->select('s.professorID,s.subjectID,c.unitid,c.topicid,s.subject,u.unitID,count(ut.unitID) as totalunit');
    		$this->db->from('subject as s');
    		$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	    $this->db->join('complete_topic as c','c.subjectid=s.subjectID','left');
    	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
    	    if($year != NULL)
    	    {    	    	
    	    	$this->db->where('c.year',$year);
    	    }
    	    $this->db->where('c.professorID',$loginuserID);
    	    $this->db->where('s.subjectID',$value['subjectID']);
    	    $this->db->where_in('s.yearsOrSemester',$yearwise);
            $this->db->where('s.status',1);
    	$a[$key]['object']= $this->db->get()->result();
    	
    		
    	}
    	 return $a; 
    }
    public function piechart_getsubjectandtopic($loginuserID)
    {
    	// $this->load->library('subquery');
    	$currentyear=$this->session->userdata('sessionyear');
    	$currentmonth=date('m');
    	$this->db->select('s.subjectID,s.subject,s.yearsOrSemester,c.classes,u.unitID as totalunits');
    	$this->db->from('subject as s');
    	$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	$this->db->join('classes as c','c.classesID=s.classesID','left');
    	$this->db->where('s.professorID',$loginuserID);
    	if($yearsOrSemester != NULL)
    	{
    		$this->db->where('s.yearsOrSemester',$yearsOrSemester);
    	}

    	$this->db->where('s.status',1);
    	$this->db->group_by('s.subjectID');
    	$yearwise= array('1st_Year','2nd_Year','3rd_Year');
    	if($currentmonth <= 3)//6
	    {
	       $year_mode = array(1,3);
	    }
	    else
	    {
	      $year_mode = array(2,3);
	    }
	    if($yearsOrSemester=="1st_Year" || $yearsOrSemester=="2nd_Year" || $yearsOrSemester=="3rd_Year")
	    {

	    	$year_mode=3;
	    }
	    if($year == NULL)
	    {
	    	// $this->db->where_not_in('s.yearsOrSemester',$yearwise);
	    	$this->db->where_in('s.year_mode',$year_mode);	
	    }
	    if($yearsOrSemester != NULL)
	    {
	    	
	    	if($yearsOrSemester == "1st_Semester" || $yearsOrSemester == "3rd_Semester" || $yearsOrSemester == "5th_Semester" || $yearsOrSemester == "7th_Semester")
			{
			   $year_mode=1;
			  
			}
			else if($yearsOrSemester=="1st_Year" || $yearsOrSemester=="2nd_Year" || $yearsOrSemester=="3rd_Year")
		    {

		    	$year_mode=3;
		    }
			else
			{
			  $year_mode=2;
			}
			$this->db->where_not_in('s.yearsOrSemester',$yearwise);
			$this->db->where('s.year_mode',$year_mode);	
	    }
	    
    	$a= $this->db->get()->result_array();
    	// print_r($a);die;
    	foreach ($a as $key => $value)
    	{
    		// print_r($a->subjectID);die;
    		$this->db->select('s.professorID,s.subjectID,c.unitid,c.topicid,s.subject,u.unitID,count(ut.unitID) as totalunit');
    		$this->db->from('subject as s');
    		$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	    $this->db->join('complete_topic as c','c.subjectid=s.subjectID','left');
    	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
    	    if($year != NULL)
    	    {
    	    	if($value['yearsOrSemester'] == '1st_Semester' || $value['yearsOrSemester'] == '3rd_Semester' || $value['yearsOrSemester'] == '5th_Semester' || $value['yearsOrSemester'] == '7th_Semester')
    	    	{
    	    		 $year_mode=1;
			    }
			    else if($value['yearsOrSemester']=="1st_Year" || $value['yearsOrSemester']=="2nd_Year" || $value['yearsOrSemester']=="3rd_Year")
			    {

			    	$year_mode=3;
			    }
			    else
			    {
			      $year_mode=2;
			    }
    	    	// print_r("expression");die;
    	    	$this->db->where('c.year',$year);
    	    	$this->db->where('s.year_mode',$year_mode);
    	    }
    	    else
    	    {
    	    	if($value['yearsOrSemester'] == '1st_Semester' || $value['yearsOrSemester'] == '3rd_Semester' || $value['yearsOrSemester'] == '5th_Semester' || $value['yearsOrSemester'] == '7th_Semester')
    	    	{
    	    		 $year_mode=1;
			    }
			    else if($value['yearsOrSemester']=="1st_Year" || $value['yearsOrSemester']=="2nd_Year" || $value['yearsOrSemester']=="3rd_Year")
			    {

			    	$year_mode=3;
			    }
			    else
			    {
			      $year_mode=2;
			    }
    	    	// print_r("expression");die;
    	    	$this->db->where('c.year',$currentyear);
    	    	$this->db->where('s.year_mode',$year_mode);
    	    }
    	    $this->db->where('c.professorID',$loginuserID);
    	    $this->db->where('s.subjectID',$value['subjectID']);
            $this->db->where('s.status',1);
    	$a[$key]['object']= $this->db->get()->result();
    	
    		
    	}
    	// print_r($a);die;
    	 return $a;
    }
    public function getsubjectandtopic($loginuserID,$year = NULL,$yearsOrSemester = NULL)
    {
    	// $this->load->library('subquery');
    	$currentyear=$this->session->userdata('sessionyear');
    	$currentmonth=date('m');
    	$this->db->select('s.subjectID,s.subject,s.yearsOrSemester,c.classes,u.unitID as totalunits');
    	$this->db->from('subject as s');
    	$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	$this->db->join('classes as c','c.classesID=s.classesID','left');
    	$this->db->where('s.professorID',$loginuserID);
    	if($yearsOrSemester != NULL)
    	{
    		$this->db->where('s.yearsOrSemester',$yearsOrSemester);
    	}

    	$this->db->where('s.status',1);
    	$this->db->group_by('s.subjectID');
    	$yearwise= array('1st_Year','2nd_Year','3rd_Year');
    	if($currentmonth <= 3)//6
	    {
	       $year_mode=1;//2
	    }
	    else
	    {
	      $year_mode=2;//1
	    }
	    if($yearsOrSemester=="1st_Year" || $yearsOrSemester=="2nd_Year" || $yearsOrSemester=="3rd_Year")
	    {

	    	$year_mode=3;
	    }
	    if($year == NULL)
	    {
	    	// $this->db->where_not_in('s.yearsOrSemester',$yearwise);
	    	$this->db->where('s.year_mode',$year_mode);	
	    }
	    if($yearsOrSemester != NULL)
	    {
	    	
	    	if($yearsOrSemester == "1st_Semester" || $yearsOrSemester == "3rd_Semester" || $yearsOrSemester == "5th_Semester" || $yearsOrSemester == "7th_Semester")
			{
			   $year_mode=1;
			  
			}
			else if($yearsOrSemester=="1st_Year" || $yearsOrSemester=="2nd_Year" || $yearsOrSemester=="3rd_Year")
		    {

		    	$year_mode=3;
		    }
			else
			{
			  $year_mode=2;
			}
			$this->db->where_not_in('s.yearsOrSemester',$yearwise);
			$this->db->where('s.year_mode',$year_mode);	
	    }
	    
    	$a= $this->db->get()->result_array();
    	// print_r($a);die;
    	foreach ($a as $key => $value)
    	{
    		// print_r($a->subjectID);die;
    		$this->db->select('s.professorID,s.subjectID,c.unitid,c.topicid,s.subject,u.unitID,count(ut.unitID) as totalunit');
    		$this->db->from('subject as s');
    		$this->db->join('units as u','u.subjectID=s.subjectID','left');
    	    $this->db->join('complete_topic as c','c.subjectid=s.subjectID','left');
    	    $this->db->join('unit_topic as ut','ut.unitID=u.unitID','left');
    	    if($year != NULL)
    	    {
    	    	if($value['yearsOrSemester'] == '1st_Semester' || $value['yearsOrSemester'] == '3rd_Semester' || $value['yearsOrSemester'] == '5th_Semester' || $value['yearsOrSemester'] == '7th_Semester')
    	    	{
    	    		 $year_mode=1;
			    }
			    else if($value['yearsOrSemester']=="1st_Year" || $value['yearsOrSemester']=="2nd_Year" || $value['yearsOrSemester']=="3rd_Year")
			    {

			    	$year_mode=3;
			    }
			    else
			    {
			      $year_mode=2;
			    }
    	    	// print_r("expression");die;
    	    	$this->db->where('c.year',$year);
    	    	$this->db->where('s.year_mode',$year_mode);
    	    }
    	    else
    	    {
    	    	if($value['yearsOrSemester'] == '1st_Semester' || $value['yearsOrSemester'] == '3rd_Semester' || $value['yearsOrSemester'] == '5th_Semester' || $value['yearsOrSemester'] == '7th_Semester')
    	    	{
    	    		 $year_mode=1;
			    }
			    else if($value['yearsOrSemester']=="1st_Year" || $value['yearsOrSemester']=="2nd_Year" || $value['yearsOrSemester']=="3rd_Year")
			    {

			    	$year_mode=3;
			    }
			    else
			    {
			      $year_mode=2;
			    }
    	    	// print_r("expression");die;
    	    	$this->db->where('c.year',$currentyear);
    	    	$this->db->where('s.year_mode',$year_mode);
    	    }
    	    $this->db->where('c.professorID',$loginuserID);
    	    $this->db->where('s.subjectID',$value['subjectID']);
            $this->db->where('s.status',1);
    	$a[$key]['object']= $this->db->get()->result();
    	
    		
    	}
    	// print_r($a);die;
    	 return $a;    	
    }

    function getstudentforprogress($getClasses,$getSubcourse,$getSemester,$getSubject,$year)
    {
    	$currentyear=$this->session->userdata('sessionyear');
		$this->db->select('att.atd,att.studentID,s.name,s.roll');
		$this->db->from('attendance_student as att');
		$this->db->join('student as s','s.studentID=att.studentID','left');
		$this->db->where('att.classesID',$getClasses);
		$this->db->where('att.subjectID',$getSubject);
		$this->db->where('att.yearsOrSemester',$getSemester);
		$this->db->where('att.year',$year);
		$this->db->where('s.status',1);
		$this->db->group_by('att.studentID');
		$a= $this->db->get()->result_array();
		foreach ($a as $key => $value)
		{
			$this->db->select('att.atd,att.studentID');
			$this->db->from('attendance_student as att');
			$this->db->where('att.studentID',$value['studentID']);
			$this->db->where('att.classesID',$getClasses);
			$this->db->where('att.subjectID',$getSubject);
			$this->db->where('att.yearsOrSemester',$getSemester);
			if($getSemester == "1st_Year" || $getSemester == "2nd_Year" || $getSemester == "3rd_Year")
            {
          	  $abn= array($year,$currentyear);
              $this->db->where_in('att.year',$abn);              
            }
            else
            {
          	  $this->db->where('att.year',$year);
            }
            $this->db->group_by('att.atd_date');
			  // $this->db->where('att.year',$year);
			  $a[$key]['object']= $this->db->get()->result();
		}

		return $a;
		
	}
	public function getstudentforpercentage($getClasses,$getSubcourse,$getSemester,$getSubject,$year,$percentage)
	{
		// print_r($year);die;
		$currentyear=2020;
		$ds= array();
		$b=array();
		$this->db->select('att.atd,att.studentID,s.name,s.roll');
		$this->db->from('attendance_student as att');
		$this->db->join('student as s','s.studentID=att.studentID','left');
		$this->db->where('att.classesID',$getClasses);
		$this->db->where('att.subjectID',$getSubject);
		$this->db->where('att.yearsOrSemester',$getSemester);
		if($getSemester == "1st_Year" || $getSemester == "2nd_Year" || $getSemester == "3rd_Year")
      	{
      		$abc= array($year,$currentyear);
        	$this->db->where_in('att.year',$abc);              
      	}
      	else
      	{
      		$this->db->where('att.year',$year);
      	}
		// $this->db->where('att.year',$year);
		$this->db->where('s.status',1);
		$this->db->group_by('s.studentID');
		$a= $this->db->get()->result_array();
		foreach ($a as $key => $value)
		{
			$this->db->select('att.atd,att.studentID');
			$this->db->from('attendance_student as att');
			$this->db->where('att.studentID',$value['studentID']);
			$this->db->where('att.classesID',$getClasses);
			$this->db->where('att.subjectID',$getSubject);
			$this->db->where('att.yearsOrSemester',$getSemester);
			$this->db->where('att.year',$year);
			$a[$key]['object']= $this->db->get()->result();
		}
		foreach ($a as $key => $value)
		{
			$ab=count($value['object']);
			$count=0;$studentid=array();
			// print_r($value['object']);die;
			foreach ($value['object'] as $key )
            {
            	$present1=0;
                $present=$key->atd;
                if($present==1)
                {
                  $count=$count+1;
                }
            }
            if($count >=0)
	        {
	        	// print_r($count);die;
	            $avg=($count*100)/$ab;
	           // print_r($avg);die; 
	            $unitavg=round($avg,0);
	            // print_r($unitavg);die; 
	            if($unitavg >= 0 && $unitavg < $percentage)
	            {	               
	               $studentid=$value['studentID'];
	            } 
	            $ds[]=$studentid;
	            // print_r($studentid);die;
        		$avga[]=$unitavg;	                          
	        }            
           
		}
		if(count($ds))
		{
			for($h=0;$h<count($ds);$h++)
			{
				if($ds[$h])
				{
					$this->db->select('att.studentID,att.classesID,att.subjectID,att.yearsOrSemester,att.year,s.name,s.roll,count(att.atd_date) as totaldays');
					$this->db->from('attendance_student as att');
					$this->db->join('student as s','s.studentID=att.studentID','left');
					$this->db->where('att.studentID',$ds[$h]);
					$this->db->where('att.classesID',$getClasses);
					$this->db->where('att.subjectID',$getSubject);
					$this->db->where('att.yearsOrSemester',$getSemester);
					$this->db->where('s.status',1);
					if($getSemester == "1st_Year" || $getSemester == "2nd_Year" || $getSemester == "3rd_Year")
		          	{
		          		$abc= array($year,$currentyear);
		            	$this->db->where_in('att.year',$abc);              
		          	}
		          	else
		          	{
		          		$this->db->where('att.year',$year);
		          	}
					// $this->db->where('att.year',$year);
					$this->db->group_by('att.studentID');
					$this->db->order_by('s.roll');
					$b[]= $this->db->get()->result_array();
					
				}
				// $gh=(object)$vb;
				// $b[]=(object)$vb;
			}
			// print_r($b);die;
			return $b;
		}
		else
		{
			$b=0;
			return $b;
		}
		

		// print_r($b);die;
		
	}
	public function getstudentpersentattendance($subjectID,$studentID,$yearsOrSemester,$classesID,$year)
	{
		$this->db->select('att.studentID,count(att.atd) as totalpresent');
		$this->db->from('attendance_student as att');
		// $this->db->join('student','student.studentID=att.studentID','right');
		$this->db->where('att.studentID',$studentID);
		$this->db->where('att.classesID',$classesID);
		$this->db->where('att.subjectID',$subjectID);
		$this->db->where('att.yearsOrSemester',$yearsOrSemester);
		$this->db->where('att.atd',1);
		$this->db->where('att.year',$year);
		$this->db->group_by('att.studentID');
		return $this->db->get()->row();
	}
	public function getstudentattendancebyweek($getClasses,$getSubcourse,$getSemester,$getSubject,$firstdate,$lastdate)
	{
		$this->db->select('att.studentID,s.name,s.roll');
		$this->db->from('attendance_student as att');
		$this->db->join('student as s','s.studentID=att.studentID','left');
		$this->db->where('att.classesID',$getClasses);
		$this->db->where('att.subjectID',$getSubject);
		$this->db->where('att.yearsOrSemester',$getSemester);
		$this->db->where('date(att.atd_date)<=',$lastdate);
		$this->db->where('date(att.atd_date)>=',$firstdate);
		$this->db->where('s.status',1);
		$this->db->group_by('s.studentID');
		$this->db->order_by('s.roll');
		$a= $this->db->get()->result_array();
		foreach ($a as $key => $value)
		{
			
			$this->db->select('att.atd,att.studentID,att.atd_date');
			$this->db->from('attendance_student as att');
			$this->db->where('att.studentID',$value['studentID']);
			$this->db->where('att.classesID',$getClasses);
			$this->db->where('att.subjectID',$getSubject);
			$this->db->where('att.yearsOrSemester',$getSemester);
			// $this->db->where('att.year',$year);
			$this->db->where('date(att.atd_date)<=',$lastdate);
			$this->db->where('date(att.atd_date)>=',$firstdate);
			$this->db->order_by('att.atd_date');
			$a[$key]['object']= $this->db->get()->result();
		
		// $b[]=$a[$key]['object'];
		}
		// echo "<pre>";
		// print_r($a);die;
		return $a;
	}
	public function getstudentattendance($loginuserID)
	{
		// print_r($loginuserID);die();
		$this->db->select('subject.subjectID,student.classesID,student.yearsOrSemester,student.studentID,student.name as studentname');
		$this->db->from('professor');
		$this->db->join('subject','subject.professorID=professor.professorID','left');
	    $this->db->join('student','student.classesID=subject.classesID','left');
	    $this->db->where('professor.professorID',$loginuserID);
	    $this->db->where('student.status',1);
	    $this->db->order_by('student.name','ASC');
	    $a=$this->db->get()->result_array();
	    foreach ($a as $key => $value)
		{
			$this->db->select('att.atd,studentID');
			$this->db->from('attendance_student as att');
			$this->db->where('att.studentID',$value['studentID']);
			$this->db->where('att.classesID',$value['classesID']);
			$this->db->where('att.subjectID',$value['subjectID']);
			$a[$key]['object']= $this->db->get()->result();
		}
	    // echo "<pre>";
	    // print_r($a);die;
	    return $a;
	}
	public function get_alldepartment($departmentID=NULL)
	{
		$this->db->select('*');
		if($departmentID)
		{
			$this->db->where('departmentID',$departmentID);
		}		
		return $this->db->get('department')->result();
	}
	public function get_alldepartmentbyprofessor($loginuserID)
	{
		$this->db->select('department.departmentID,department.department_name');
		$this->db->from('subject');
		$this->db->join('classes','classes.classesID=subject.classesID','left');
		$this->db->join('department','department.departmentID=classes.departmentID','left');
		$this->db->where('subject.professorID',$loginuserID);
		$this->db->group_by('department.departmentID');
		$this->db->where('subject.status',1);
		return $this->db->get()->result();
		// print_r($a);die;
	}
	public function get_classlist($departmentID)
	{
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->where('departmentID',$departmentID);
		return $this->db->get()->result();
	}

   
	function ActiveProfessor_count(){

		$this->db->from('professor');
		$this->db->where('status',1);
		$query	= $this->db->get();
		return $query->num_rows();

	}

	function DraftProfessor_count(){

		$this->db->from('professor');
		$this->db->where('status',0);
		$query	= $this->db->get();
		return $query->num_rows();
		
	}

	function TrashProfessor_count(){

		$this->db->from('professor');
		$this->db->where('status',2);
		$query	= $this->db->get();
		return $query->num_rows();
		
	}
	public function getall_professor()
	{
		$this->db->from('professor');
		$this->db->where('status',1);
		$query	= $this->db->get()->result();
		return $query;
	}
	public function all_student_attendance_bymonth($month)
	{
		$date=date('Y');
		$num_padded = sprintf("%02d",$month);
		$this->db->select('classesID,classes');
		$this->db->from('classes');
		$this->db->where('status',1);
		$getallcourse=$this->db->get()->result_array();
		foreach ($getallcourse as $key => $value)
		{
			$this->db->select('count(atd) as totalpresent');
			$this->db->from('attendance_student');
			$this->db->where('classesID',$value['classesID']);
			$this->db->where('month(atd_date)',$num_padded); 
			$this->db->where('year(atd_date)',$date);  
			$this->db->where('atd',1); 
			$getallcourse[$key]['student_attendance']=$this->db->get()->result();

		}
		return $getallcourse;
	}
	public function totalattendancebycoursewise($month,$classesID)
	{
		$date=date('Y');
        $this->db->select('count(atd_date) as totalstudent');
			$this->db->from('attendance_student');
			$this->db->where('classesID',$classesID);
			$this->db->where('month(atd_date)',$month);
			$this->db->where('year(atd_date)',$date);    
			return $this->db->get()->row();
	}
	public function getprofessoremailid($professorid)
	{
		$this->db->select('email,name');
		$this->db->from('professor');
		$this->db->where('professorID',$professorid);
		$this->db->where('status',1);
		return $this->db->get()->row();
	}
	public function getsuperadminid($superadminid)
	{
		$usertype=$this->session->userdata('usertype');
		$this->db->select('email,name');
		$this->db->from('admin');
		$this->db->where('adminID',$superadminid);
		$this->db->where('status',1);
		return $this->db->get()->row();
		
	}
	public function getalluserid($superadminid)
	{
		$usertype=$this->session->userdata('usertype');
		if($usertype=="Admin" || $usertype=="Accountant" || $usertype=="HOD")
		{
			$this->db->select('email,name');
			$this->db->from('user');
			$this->db->where('userID',$superadminid);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
		if($usertype=="Student")
		{
			$this->db->select('email,name');
			$this->db->from('student');
			$this->db->where('studentID',$superadminid);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
		if($usertype=="Professor")
		{
			$this->db->select('email,name');
			$this->db->from('professor');
			$this->db->where('professorID',$superadminid);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
		if($usertype=="ClgAdmin")
		{
			$this->db->select('email,name');
			$this->db->from('admin');
			$this->db->where('adminID',$superadminid);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
	}
	public function getadminid($adminid)
	{
		$this->db->select('email,name');
		$this->db->from('user');
		$this->db->where('userID',$adminid);
		$this->db->where('status',1);
		return $this->db->get()->row();
	}
	public function getemailidsender($loginuserID)
	{
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Accountant")
		{
			$this->db->select('email');
			$this->db->from('user');
			$this->db->where('userID',$loginuserID);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
		if($usertype == "ClgAdmin")
		{
			$this->db->select('email');
			$this->db->from('admin');
			$this->db->where('adminID',$loginuserID);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
		if($usertype == "Professor")
		{
			$this->db->select('email');
			$this->db->from('professor');
			$this->db->where('professorID',$loginuserID);
			$this->db->where('status',1);
			return $this->db->get()->row();
		}
		
	}
	public function superadminid()
	{
		$this->db->select('adminID,email');
		$this->db->from('admin');
		$this->db->where('status',1);
		$this->db->where('adminID',2);
	 	return $this->db->get()->row();

	}
	public function adminid()
	{
		$this->db->select('userID,email');
		$this->db->from('user');
		$this->db->where('usertype',"Admin");
		$this->db->where('status',1);
		$this->db->where('adminID',2);
	 	return $this->db->get()->result_array();

	}
	public function accountantid()
	{
		$this->db->select('userID');
		$this->db->from('user');
		$this->db->where('usertype',"Accountant");
		$this->db->where('status',1);
	 	return $this->db->get()->result();
	}
	public function getreciverstudentid($studentID)
	{
		$this->db->select('email,name');
		$this->db->from('student');
		$this->db->where('studentID',$studentID);
		$this->db->where('status',1);
		return $this->db->get()->row();

	}
	public function sentemailsave($arrayName)
	{
		$this->db->insert('emailsent',$arrayName);
		return $this->db->insert_id();
	}
	public function getsentemail($loginuserID)
	{

		$this->db->select('*');
		$this->db->from('emailsent');
		$this->db->where('sentby',$loginuserID);
		$this->db->order_by('sentdate','desc');
	
		return $this->db->get()->result();
	}
	public function checkdatealltable($date)
	{
		$table=array('professor','student','user');
		for ($i=0; $i < count($table) ; $i++)
		{ 
			if($table[$i])
			{
				$this->db->select('*');
				$this->db->from($table[$i]);
				$this->db->like('dob',$date);
				$this->db->where('status',1);
				$a= $this->db->get()->result();
			}
			$b[]=$a;
			
		}
		return $b;
	}
	public function clglogo($adminID)
	{
		$this->db->select('*');
		$this->db->from('setting');
		$this->db->where('adminID',$adminID);
		return $this->db->get()->row();
	}
	public function getsubjectbyprofessor($professorID)
	{
		$this->db->select('year_mode');
		$this->db->where('professorID',$professorID);
		$this->db->group_by('year_mode');
		return $this->db->get('subject')->result();
		
	}
	public function get_assignedsubjectsemesterwise($loginuserID)
	{
		$currentmonth=date('m');
		$this->db->select('classes.classes,subject.yearsOrSemester,subject.subject');
		$this->db->from('subject');
		$this->db->join('classes','classes.classesID=subject.classesID','left');
		$this->db->where('subject.professorID',$loginuserID);
		$this->db->where('subject.status',1);
		$this->db->where('subject.year_mode',1);//Jab Even Subject suro ho ga toh yha pe 1 ke jgh 2 likhna ho ga.
		$query = $this->db->get()->result();
		return $query;
	}
	public function get_assignedsubjectyearwise($loginuserID)
	{
		$this->db->select('classes.classes,subject.yearsOrSemester,subject.subject');
		$this->db->from('subject');
		$this->db->join('classes','classes.classesID=subject.classesID','left');
		$this->db->where('subject.professorID',$loginuserID);
		$this->db->where('year_mode',3);
		$this->db->where('subject.status',1);
		$query = $this->db->get()->result();
		return $query;
	}
	public function checkstudentattendclass($getSubject,$studentID,$getSemester,$getClasses,$loginuserID)
	{

		$date=date('Y-m-d');
	    $this->db->select('activestatus');
	    $this->db->from('attendance_student');
	    $where = "yearsOrSemester = '".$getSemester."' and studentID ='".$studentID."' and classesID = '".$getClasses."' and atd_date='".$date."' and year_mode = '5' and subjectID='".$getSubject."' and professorID='".$loginuserID."'";
	    $this->db->where($where);
	    $this->db->group_by('studentID');
	    $query = $this->db->get();
	    $result = $query->row();
		return $result;
	}
	public function update_user($status, $id)
	{
		$this->db->where('id',$id);
		$this->db->update('professor_lecture',$status);
		return true;
	}
	


}

