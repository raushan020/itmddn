<?php



/* List Language  */

$lang['panel_title'] = "Academic Partner";

$lang['add_title'] = "Add a Academic Partner";

$lang['slno'] = "#";

$lang['teacher_photo'] = "Photo";

$lang['teacher_name'] = "Owner Name";

$lang['organization_name'] = "Organization Name"; 

$lang['teacher_designation'] = 'Designation';

$lang['teacher_email'] = "Email";

$lang['teacher_dob'] = "Date of Birth";

$lang['teacher_sex'] = "Gender";

$lang['teacher_sex_male'] = "Male";

$lang['teacher_sex_female'] = "Female";

// $lang['teacher_religion'] = "Religion";

$lang['phone'] = "Mobile No.";

$lang['teacher_address'] = "Address";

$lang['teacher_jod'] = "Joining Date";

$lang['teacher_username'] = "Username";

$lang['teacher_password'] = "Password";

$lang['teacher_status'] = "Status";





$lang['action'] = "Action";

$lang['view'] = 'View';

$lang['edit'] = 'Edit';

$lang['delete'] = 'Delete';

$lang['print'] = "Print";

$lang['pdf_preview'] = "PDF Preview";

$lang['idcard'] = "ID Card";

$lang['mail'] = "Send Pdf to Mail";



/* Add Language */

$lang['personal_information'] = "Personal Information";

$lang['add_teacher'] = 'Add Academic Partner';

$lang['update_teacher'] = 'Update Academic Partner';



$lang['to'] = 'To';

$lang['subject'] = 'Subject';

$lang['message'] = 'Message';

$lang['send'] = 'Send';

$lang['mail_to'] = "The To field is required.";

$lang['mail_valid'] = "The To field must contain a valid email address.";

$lang['mail_subject'] = "The Subject field is required.";

$lang['mail_success'] = 'Email send successfully!';

$lang['mail_error'] = 'oops! Email not send!';