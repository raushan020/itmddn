<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Classes_m extends MY_Model {


	protected $_table_name = 'classes';



	protected $_primary_key = 'classesID';



	protected $_primary_filter = 'intval';



	protected $_order_by = "classes_numeric asc";


	function __construct() {



		parent::__construct();



	}



function get_single_invoice_department_data($department){
		$this->db->where('departmentID',$department);
		$query = $this->db->get('department')->row();
		return $query;
}


function get_classes_by_department($departmentID){
  return $this->db->where('departmentID',$departmentID)->where('status',1)->get('classes')->result();
}


function insert_sub_courses($id,$adminID,$sitenameString){

 

 $subcourses =  $this->input->post('subcourse');



for ($i=0; $i <count($this->input->post('subcourse')) ; $i++) { 



				    $digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int

				    $char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string 						

 					$firstCharacter = substr($sitenameString, 0, 2);



 					$SubcourseCode      =   strtoupper($firstCharacter).'-'.$char.$digit; 



$data = array(

'sub_course'=>$subcourses[$i],

'subCourseCode'=>$SubcourseCode,

'classesID'=>$id,

'adminID'=>$this->session->userdata("adminID")

	);



  $this->db->insert('sub_courses' ,$data);



}

return true;



}





	function get_teacher() {



		$this->db->select('*')->from('teacher');



		$query = $this->db->get();



		return $query->result();



	}


	function insert_fee($fee_array,$insert){

for ($i=0; $i <count($fee_array) ; $i++) { 

$data = array('feetype' =>'tution',
'adminID'=>2,
'amount'=>$fee_array[$i],
'classesID'=>$insert
);
if($fee_array[$i]!=''){
$this->db->insert('feetype',$data);
}

}


	}

	function update_feetype($fee_array, $id){

foreach ($fee_array['feetypeID'] as $key => $value) {
 
 $data =  array(
 'amount'=>$value[0]
 );
if($value[0]==''){
	$this->db->where('feetypeID',$key);
	$this->db->delete('feetype');
}else{
 $this->db->where('feetypeID',$key)->update('feetype',$data);
}

}

for ($i=0; $i <count($fee_array['fee']); $i++) { 

$data = array(
'feetype' =>'tution',
'adminID'=>2,
'amount'=>$fee_array['fee'][$i],
'classesID'=>$id
);
if($fee_array['fee'][$i]!=''){
$this->db->insert('feetype',$data);
}

}


		
	}

	function find_fee($id){

  return $this->db->where('classesID',$id)->get('feetype')->result();


	}







	function get_classes_byAdmin($adminID) {

	$this->get_course_ClgAdmin(); 

	$result = $this->db->get();

	$query = $result->result();

	return $query;



	}

	

	function get_course_ClgAdmin() {

		$adminID = $this->session->userdata("adminID");
        $usertype = $this->session->userdata("usertype");
	    // $this->db->where('adminID',$adminID);

	    $this->db->select('*');
		$this->db->from('classes');

		$this->db->join('department', 'classes.departmentID = department.departmentID', 'LEFT');
		 

	   	if ($this->session->userdata('departmentID')) {

		 $this->db->where('classes.departmentID', $this->session->userdata('departmentID'));

		}
	   	if ($this->session->userdata('classesIDForfilterCourses')) {

		 $this->db->where('classesID', $this->session->userdata('classesIDForfilterCourses'));

		}



	   if ($this->session->userdata('durationID')) {

		 $this->db->where('duration', $this->session->userdata('durationID'));

		}



	   if ($this->session->userdata('ActiveClasess')) {

		 $this->db->where('status', $this->session->userdata('ActiveClasess'));

		}



	   if ($this->session->userdata('DraftClasess')) {

		 $this->db->where('status', 0);

		}



	   if ($this->session->userdata('TrashClasess')) {

		 $this->db->where('status', $this->session->userdata('TrashClasess'));

		}	



     if (empty($this->session->userdata('DraftClasess')) && empty($this->session->userdata('TrashClasess'))) {

           $this->db->where('classes.status',1);

            }





		if(isset($_POST["search"]["value"]))  

           {  



			$searches  =  $_POST["search"]["value"];



			$order_column = array( 



                            0 =>'sn', 

                            1 =>'classesID',

                            2=> 'classes',

                            3=> 'duration',

                            4=> 'mode',

                            5=> 'fee',

                            6=> 'department_name'

							

                        );

           $where = "(classes.classes LIKE '%$searches%' OR classes.duration LIKE '%$searches%' OR classes.mode LIKE '%$searches%' OR classes.fee LIKE '%$searches%' OR classes.courseCode LIKE '%$searches%')";

           $this->db->where($where);



        

           

           if(isset($_POST["order"]))  

           {  

                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  

           }  

           else  

           {  

                $this->db->order_by('classes.classesID', 'DESC');  

           } 

          

           if($_POST["length"] != -1)  

           {  

                $this->db->limit($_POST['length'], $_POST['start']);  

           } 

       } 



	}	


	function get_course_with_fee() {

$adminID = $this->session->userdata("adminID");

$this->db->select('classes.classesID,classes.classes, feetype.*,GROUP_CONCAT(amount ORDER BY feetype.classesID) as fees,GROUP_CONCAT(feetypeID ORDER BY feetype.classesID) as ids');
$this->db->from('classes');
$this->db->join('feetype', 'classes.classesID = feetype.classesID', 'LEFT');
$this->db->where('feetype.adminID',$adminID);
$this->db->where('classes.status',1);
$this->db->group_by('feetype.classesID');
return $this->db->get()->result();


}
	function inner_join_with_fee_module($teacherID){
	 	$usertype = $this->session->userdata("usertype");
		$this->db->select('*');
		$this->db->from('classes');
		$this->db->join('fee_x_classes_ap', 'classes.classesID = fee_x_classes_ap.classesID', 'inner');
	 	$this->db->where('fee_x_classes_ap.teacherID',$teacherID);
		return $this->db->get()->result();	
		// echo "<pre>";$b=
		// print_r($b);die;
	}

	function inner_join_with_fee_module_for_dashboard($loginuserID){
		$usertype = $this->session->userdata("usertype");
		$this->db->select('*');
		$this->db->from('classes');
	    $this->db->join ('teacher','teacher.teacherID = classes.teacherID', 'LEFT');
	    
	    $this->db->join ('fee_x_classes_ap','fee_x_classes_ap.classesID = classes.classesID', 'LEFT');
	$this->db->join ('feetype','fee_x_classes_ap.feetypeID = feetype.feetypeID', 'LEFT');
	$this->db->group_by('fee_x_classes_ap.classesID');
	    $this->db->where('teacher.spa',$loginuserID);
	$this->db->where('teacher.status',1);
       	
		$query	= $this->db->get();
		return $query->num_rows();

	 	
	}

	function get_class_super_ap($adminID){

	 	$usertype = $this->session->userdata("usertype");
		$this->db->select('*');
		$this->db->from('classes');
	    $this->db->join ('teacher','teacher.teacherID = classes.teacherID', 'LEFT');
	    
	    $this->db->join ('fee_x_classes_ap','fee_x_classes_ap.classesID = classes.classesID', 'LEFT');
	$this->db->join ('feetype','fee_x_classes_ap.feetypeID = feetype.feetypeID', 'LEFT');
	$this->db->group_by('fee_x_classes_ap.classesID');
	    $this->db->where('teacher.spa',$adminID);
		$this->db->where('teacher.status',1);

	 	return $this->db->get()->result();	
 	 
	}

	function get_classes_bysuperAdmin() {

  

		$result = $this->db->get('classes');

		$query = $result->result();

		// $query = parent::get($array, $signal);

		return $query;

	}

	



		function get_classes_active() {

  			$this->db->where('status',1);

	$result = $this->db->get('classes');

	$query = $result->result();

		// $query = parent::get($array, $signal);

		return $query;

	}



	function get_cousre_no($adminID)

	{			

			if(isset($_POST["search"]["value"]))  

           {  



			$searches  =  $_POST["search"]["value"];



			$order_column = array( 



                            0 =>'sn', 

                            1 =>'classesID',

                            2=> 'classes',

                            3=> 'duration',

                            4=> 'mode',

                            5=> 'fee',

                            6=> 'department_name'

							

                        );

           $where = "(classes.classes LIKE '%$searches%' OR classes.duration LIKE '%$searches%' OR classes.mode LIKE '%$searches%' OR classes.fee LIKE '%$searches%' OR classes.courseCode LIKE '%$searches%')";

           $this->db->where($where);

}



		 	    $this->db->where('adminID',$adminID);

	    

	   if ($this->session->userdata('classesIDForfilterCourses')) {

		 $this->db->where('classesID', $this->session->userdata('classesIDForfilterCourses'));

		}



	   if ($this->session->userdata('durationID')) {

		 $this->db->where('duration', $this->session->userdata('durationID'));

		}



	   if ($this->session->userdata('ActiveClasess')) {

		 $this->db->where('status', $this->session->userdata('ActiveClasess'));

		}



	   if ($this->session->userdata('DraftClasess')) {

		 $this->db->where('status', 0);

		}



	   if ($this->session->userdata('TrashClasess')) {

		 $this->db->where('status', $this->session->userdata('TrashClasess'));

		}	

		

     if (empty($this->session->userdata('DraftClasess')) && empty($this->session->userdata('TrashClasess'))) {

           $this->db->where('classes.status',1);

            }



		$result = $this->db->get('classes');

		$query = $result->num_rows();

		return $query;

		

	}



function get_classes($array=NULL, $signal=true) {



   $this->db->where('adminID',$this->session->userdata("loginuserID"));

   $this->db->where('classesID',$array);

   $result = $this->db->get('classes');

   $query = $result->row();



		// $query = parent::get($array, $signal);

		// $query = parent::get($array, $signal);



		return $query;



	}

	function get_order_by_classes($array=NULL) {



		$query = parent::get_order_by($array);



		return $query;



	}



function get_SubCourses($id){

	$this->db->where('classesID',$id);

// 	$this->db->query("UPDATE sub_courses
// 	INNER JOIN student ON sub_courses.sub_coursesID = student.sub_coursesID
// 	INNER JOIN subject ON sub_courses.sub_coursesID = subject.sub_coursesID 
// 	INNER JOIN ets_quiz ON sub_courses.sub_coursesID = ets_quiz.sub_coursesID
	
// 	 set student.status = 1 
// ,ets_quiz.status = 1, subject.status = 1 ,sub_courses.status = 2 where sub_courses.classesID = $id");

	$this->db->where('status',1);

	$query = $this->db->get('sub_courses');

	return $query->result();

}

function get_SubCourses_trash($id){

	$this->db->where('classesID',$id);
// 	$this->db->query("UPDATE sub_courses
// 	INNER JOIN student ON sub_courses.sub_coursesID = student.sub_coursesID
// 	INNER JOIN subject ON sub_courses.sub_coursesID = subject.sub_coursesID 
// 	INNER JOIN ets_quiz ON sub_courses.sub_coursesID = ets_quiz.sub_coursesID
	
// 	 set student.status = 2 
// ,ets_quiz.status = 2, subject.status = 2 ,sub_courses.status = 2 where sub_courses.classesID = $id");

	$this->db->where('status',2);

	$query = $this->db->get('sub_courses');

	return $query->result();

}



function GetCoursesByParam($array){



	$this->db->where($array);

	$query = $this->db->get('sub_courses');

	return $query->result();



}



function addSubCourse($classID,$sub_course,$subCourseCode )

{

	$data = array(

	'classesID'=>$classID,

	'sub_course'=>$sub_course,

	'adminID'=>$this->session->userdata("adminID"),

	'subCourseCode'=>$subCourseCode ,

	);

	$this->db->insert('sub_courses',$data);

}



function update_sub_courses($id){



	$this->db->where('classesID',$id);

	$this->db->delete('sub_courses');



$subcourses =  array_filter($this->input->post('subcourse'));



for ($i=0; $i <count($subcourses) ; $i++) { 

  

$data = array(

'sub_course'=>$subcourses[$i],

'classesID'=>$id,

'adminID'=>$this->session->userdata("adminID")

	);



  $this->db->insert('sub_courses' ,$data);



}

return true;



}



	function insert_classes($array) {



		$error = parent::insert($array);



		return $error;



	}







	function update_classes($data, $id = NULL) {



		parent::update($data, $id);



		return $id;



	}







	public function delete_classes($id){



		parent::delete($id);



	}







	function get_order_by_numeric_classes() {



		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');



		$query = $this->db->get();



		return $query->result();



	}







    function get_single_classes($id) {

    	

	$this->db->where('classesID',$id);

	$query = $this->db->get('classes');

   return  $query->row();



	}


	function get_single_department($id) {

    	

	$this->db->where('departmentID',$id);

	$query = $this->db->get('classes');

   return  $query->result();



	}

function new_SuperAP_Count_Class_dashboard($loginuserID){
		$usertype = $this->session->userdata('usertype');

		$this->db->from('student');
		
		$this->db->join('teacher', 'student.counsellor = teacher.teacherID', 'LEFT');
		$this->db->join('user','teacher.spa = user.userID','LEFT');




		// $this->db->join('teacher', 'student.counsellor = teacher.teacherID', 'LEFT');
		// $this->db->join('user', 'user.userID = teacher.spa', 'LEFT');
		if($usertype=='Super_A_P'){
		$this->db->where('teacher.spa',$loginuserID);	
		}
       	$where = "(student.status=3 or student.status =4 or student.status =1)";
        
         $this->db->where($where);
		$query	= $this->db->get();
		return $query->num_rows();

	}


	function all_count(){

		$this->db->where('status',1);

		$this->db->or_where('status',0);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function ActiveClasess_count(){

		$this->db->where('status',1);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function DraftClasess_count(){

		$this->db->where('status',0);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function TrashClasess_count(){

		$this->db->where('status',2);

	$query	= $this->db->get('classes');

	return $query->num_rows();

	}



		function get_check_classesID($id){

		$this->db->where('classesID',$id);

		$studentID = $this->db->get('student')->num_rows();

		if ($studentID>0) {

		 return $studentID;	

		}else{

			$this->db->where('classesID',$id);

		    $subjectID = $this->db->get('subject')->num_rows();

		    if ($subjectID>0) {

		       return $studentID;	

		    }else{

		    $this->db->where('classesID',$id);

		    $quizID = $this->db->get('ets_quiz')->num_rows();

		    return $quizID;

		    }

		}

	}



		function check_classesID_Subcourse($id){

			$this->db->where('classesID',$id);

			$sub_courseID = $this->db->get('sub_courses')->num_rows();

		

				return $sub_courseID;

			

		}





}







/* End of file classes_m.php */



/* Location: .//D/xampp/htdocs/school/mvc/models/classes_m.php */