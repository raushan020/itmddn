

<div class="">


    <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="fa fa-users"></i> <?=$this->lang->line('panel_title')?> </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("user/index")?>"><?=$this->lang->line('menu_user')?></a></li>

            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_user')?></li>
                </ol>
            </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
         
            <div class="card">
            <div class="card-body">

            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post" autocomplete="off" enctype="multipart/form-data">



                    <?php

                        if(form_error('name'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="user_name" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_name")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name', $user->name)?>" >



                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>



                  <!--   <?php

                        if(form_error('dob'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="dob" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_dob")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control datepicker_quiz_data" id="dob" name="dob" value="<?=set_value('dob', date("d-m-Y", strtotime($user->dob)))?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('dob'); ?>

                        </span>

                    </div> -->



                    <?php

                        if(form_error('sex'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_sex")?>

                        </label>

                        <div class="col-sm-6">

                            <?php

                                echo form_dropdown("sex", array($this->lang->line("user_sex_male") => $this->lang->line("user_sex_male"), $this->lang->line("user_sex_female") => $this->lang->line("user_sex_female")), set_value("sex", $user->sex), "id='sex' class='form-control'");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('sex'); ?>

                        </span>

                    </div>


                    <?php

                        if(form_error('email'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_email")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $user->email)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('phone'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_phone")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $user->phone)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>



                  <!--   <?php

                        if(form_error('address'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="address" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_address")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $user->address)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('address'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('jod'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="jod" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_jod")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control datepicker_quiz_data" id="jod" name="jod" value="<?=set_value('jod', date("d-m-Y", strtotime($user->jod)))?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('jod'); ?>

                        </span>

                    </div> -->



                    <?php

                        if(isset($image))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">

                            <?=$this->lang->line("user_photo")?>

                        </label>

                        <div class="col-sm-4 col-xs-6 col-md-4">

                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />

                        </div>



                        <div class="col-sm-2 col-xs-6 col-md-2">

                            <div class="fileUpload btn btn-success form-control">

                                <span class="fa fa-repeat"></span>

                                <span><?=$this->lang->line("upload")?></span>

                                <input id="uploadBtn" type="file" class="upload" name="image" />

                            </div>

                        </div>

                         <span class="col-sm-4 control-label col-xs-6 col-md-4">



                            <?php if(isset($image)) echo $image; ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('usertype'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="usertype" class="col-sm-2 control-label">

                            <?=$this->lang->line("user_type")?>

                        </label>

                        <div class="col-sm-6">

                            <?php

                                echo form_dropdown("usertype", array(

                                    "Accountant" => $this->lang->line("accountant"), 
                                    "Admin" => "Admin",
                                    "Academic" =>'Admin Academic',
                                    "Project_lead" => 'Project Lead',
                                    "Support" => 'Admin University',
                                    "Super_A_P" => 'Super Academic Partner',

                                       "Admin_btech" => 'Admin B.Tech'
                                ),
                                     set_value("usertype", $user->usertype), "id='usertype' class='form-control'");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('usertype'); ?>

                        </span>

                    </div>
                    <h3  class="border_heading">Reset Password</h3>
                    <div class='form-group'>
                        <label for="new_password" class="col-sm-2 control-label">
                            New Password
                        </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control ccpassword" id="new_password" name="new_password" value="" >
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for="cpassword" class="col-sm-2 control-label">
                            Confirm Password
                        </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control ccpassword" id="cpassword" name="cpassword" value="" >&nbsp;<span id="pass_err"></span>
                        </div>
                    </div>



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("update_user")?>" >

                        </div>

                    </div>



                </form>

            </div>

        </div>

    </div>

</div>



<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });

$('#jod').datepicker();

</script>
<script type="text/javascript">
    function find_classes_using_teacher(val){

     $.ajax({
    type:'post',

    url:"<?php echo base_url() ?>AjaxController/find_classes_using_teacher",

    data:{val:val},

    success:function(response) {
    $('#classesID').html(response);

    }



}); 
    }

    $('.ccpassword').focusout(function(){
        var pass = $('#new_password').val();
        var pass2 = $('#cpassword').val();
        if(pass != pass2){
            $("#pass_err").html("<p style='color:red;'>Password doesn't match</p>");
        }
        else
        {
            $("#pass_err").html("<p style='color:green;'></p>");
        }

    });
</script>


