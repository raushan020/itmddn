
<div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-users"></i> <?=$this->lang->line('panel_title')?></h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active"><?=$this->lang->line('menu_user')?></li>

            </ol>
        </div>
    </div>
<!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

            <div class="col-sm-12">

                <?php 

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "ClgAdmin") {

                ?>

                    <h5>

                        <a href="<?php echo base_url('user/add') ?>" class="btn-top check-all btn bg-success pull-right">

                            <i class="fa fa-plus"></i> 

                            <?=$this->lang->line('add_title')?>

                        </a>

                    </h5>

                <?php } ?>



                <div id="hide-table">

                    <table id="usersTable" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th class="col-lg-1"><?=$this->lang->line('slno')?></th>

                                <th class="col-lg-2"><?=$this->lang->line('user_photo')?></th>

                                <th class="col-lg-2"><?=$this->lang->line('user_name')?></th>

                                <th class="col-lg-2"><?=$this->lang->line('user_email')?></th>

                                <th class="col-lg-2"><?=$this->lang->line('user_phone')?></th>

                                <th class="col-lg-2"><?=$this->lang->line('user_type')?></th>

                               

                                <th class="col-lg-2"><?=$this->lang->line('action')?></th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php if(count($users)) {$i = 1; foreach($users as $user) { ?>

                                <tr>

                                    <td data-title="<?=$this->lang->line('slno')?>">

                                        <?php echo $i; ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('user_photo')?>">

                                        <?php $array = array(

                                                "src" => base_url('uploads/images/'.$user->photo),

                                                'width' => '35px',

                                                'height' => '35px',

                                                'class' => 'img-rounded'



                                            );

                                            echo img($array); 

                                        ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('user_name')?>">

                                        <?php echo $user->name; ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('user_email')?>">

                                        <?php echo $user->email; ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('user_phone')?>">

                                        <?php echo $user->phone; ?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('user_type')?>">

                                        

                                        <?php if ($user->usertype=='Accountant') {
                                         echo "Relationship Manager";
                                        } elseif($user->usertype=='Support'){
                                          echo "Admin University";
                                        }
                                        elseif($user->usertype=='Academic'){
                                          echo "Admin Academic";
                                        }

                                        else{
                                        echo  $user->usertype;
                                        }
                                        ?>


                                    </td>

                              
                                    <td data-title="<?=$this->lang->line('action')?>">

                                        <?php echo btn_view('user/view/'.$user->userID, $this->lang->line('view')) ?>

                                        <?php echo btn_edit('user/edit/'.$user->userID, $this->lang->line('edit')) ?>

                                        <?php echo btn_delete('user/delete/'.$user->userID, $this->lang->line('delete')) ?>

                                    </td>

                                </tr>

                            <?php $i++; }} ?>

                        </tbody>

                    </table>

                </div>


                </div>
              </div>

            </div> <!-- col-sm-12 -->

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->



<script>

  var status = '';

  var id = 0;

  $('.onoffswitch-small-checkbox').click(function() {

      if($(this).prop('checked')) {

          status = 'chacked';

          id = $(this).parent().attr("id");

      } else {

          status = 'unchacked';

          id = $(this).parent().attr("id");

      }



      if((status != '' || status != null) && (id !='')) {

          $.ajax({

              type: 'POST',

              url: "<?=base_url('user/active')?>",

              data: "id=" + id + "&status=" + status,

              dataType: "html",

              success: function(data) {

                  if(data == 'Success') {

                      toastr["success"]("Success")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  } else {

                      toastr["error"]("Error")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  }

              }

          });

      }

  }); 

</script>

