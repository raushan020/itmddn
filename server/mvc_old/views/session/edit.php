<div class="box">

    <div class="box-header">

        <h3 class="box-title"><i class="fa fa-sitemap"></i> <?=$this->lang->line('panel_title')?></h3>



       

        <ol class="breadcrumb">

            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li><a href="<?=base_url("classes/index")?>"></i> <?=$this->lang->line('menu_classes')?></a></li>

            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_classes')?></li>

        </ol>

    </div><!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">

            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post">



                    <?php 

                        if(form_error('classes')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        

                        <label for="classes" class="col-sm-2 control-label">

                            <?=$this->lang->line("courses_name")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="classes" name="classes" value="<?=set_value('classes', $classes->classes);?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classes'); ?>

                        </span>

                    </div>
                     <?php 

                        if(form_error('education_details[]')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classes" class="col-sm-2 control-label">

                            Eductions Qualification

                        </label>

                        <div class="col-sm-6 ulliclass">
                          <ul>
                          <?php
                      $education_detailsIDs = explode(' , ', $classes->education_detailsID);

                          foreach ($education_details as $key => $value) {
                            
                                ?>
                                <li>
                                    <div class="checkbox">
                                        <label><input type="checkbox" <?php if(in_array($value->education_detailsID, $education_detailsIDs)){echo "checked";}else{echo"";} ?> name="education_details[]" value="<?=set_value('education_details',$value->education_detailsID) ?>"><?php echo $value->education ?></label>
                                    </div>
                                </li>
                            <?php } ?>    
                                                          
                          </ul>
                            

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('education_details[]'); ?>

                        </span>
                    </div>
                    <?php 

                        if(form_error('note')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-2 control-label">

                            <?=$this->lang->line("courses_note")?>

                        </label>

                        <div class="col-sm-6">

                            <textarea style="resize:none;" class="form-control" id="note" name="note"><?=set_value('note', $classes->note);?></textarea>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('note'); ?>

                        </span>

                    </div>

<!-- <style type="text/css">
 .dadaCla ul li{
display:inline-block;
 }
 .dadaCla ul li a {
    background: #23292f;
    padding: 10px 16px;
    color: #fff;
 }
 .dadaCla ul li a span{
    color: red;
    padding-left: 9px;
 }
</style> -->
<!-- <div class="form-group col-sm-6 dadaCla">
  <ul>
      <li><a href="">BA Hindi<span> <i class="fa fa-times" aria-hidden="true"></i></span></a></li>
  </ul>
</div> -->
<!-- <div class="clearfix"></div>
                <div class="form-group">

                     <label for="classes" class="col-sm-2 control-label">

                Add Sub Courses

                        </label>

                        <div class="col-sm-6">

<div class="input-group">
  <input type="text" class="form-control" placeholder="Sub Courses" aria-describedby="basic-addon2">
  <span class="input-group-addon" id="basic-addon2"><a href="">Add</a></span>
</div>

                        </div>


   
                </div>-->



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("update_course")?>" >

                        </div>

                    </div>



                </form>

            </div>    

        </div>

    </div>

</div>

