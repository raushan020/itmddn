<div class="">
   <form action="<?=base_url('exam/dau_exam');?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="ti ti-ruler-pencil"></i> <?=$this->lang->line('panel_title')?> </h3>
      </div>
      <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active"><?=$this->lang->line('panel_title')?></li>
          </ol>
      </div>
    </div>

    

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
         <div class="col-md-12 nopading">
                
                <?php 

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

                ?>


         </div>

       <div class="clearfix"></div>

         </div>
         <div class="clearfix"></div>  
           

        <div class="col-sm-12">
         <div class="theme_input_blue">
            <?php 
               // code goes here...
               ?>

            <div class="col-sm-4">
               <div class="">
                 
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Course
                        </label>
                        <div class="">
                           <?php
                             $array = array("0" => "Select Course");

                                        foreach ($classes as $classa) {

                                            $array[$classa->classesID] = $classa->classes;

                                        }

                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterExamclassesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");
                              
                              ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                     </div>
               
               </div>
            </div>
            <div class="col-sm-4">
               <div class="">
         
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Sub Courses
                        </label>
                        <div class="">
                           <select id="subCourseID" name="sub_coursesID" class="form-control">
                              <option value="0">Select</option>
                               <?php  foreach ($subCourses as $key => $value) {
                                            if ($value->sub_coursesID==$this->session->userdata('FilterExamsubCourseID')) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                              <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                             
                           </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                     </div>
                  
               </div>
            </div>
            
            <div class="col-sm-4">
               <div class="">
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Semester
                        </label>
                        <div class="">
                           <select id="yearSemesterID" class="form-control" name="yearsOrSemester">
                              <option value="">Select</option>
                              <?php 
                                if ($classesRow) {
                                   $looping    =  (int) $classesRow->duration;
                                 if ($classesRow->mode==1) {
                               
                                    for ($i=1; $i <=$looping; $i++) {
                                                if (CallYears($i)==$this->session->userdata('FilterExamyearSemesterID')) {
                                      $select = 'Selected';
                                   }else{
                                   $select = '';
                                   }
                                   echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                   
                                 }
                              }

                              else{
                                    for ($i=1; $i <=(2*$looping); $i++) {
                                   if (CallSemester($i)==$this->session->userdata('FilterExamyearSemesterID')) {
                                      $select = 'Selected';
                                   }else{
                                   $select = '';
                                   }
                                   echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                 }
                                 }
                              }
                               ?>
                              
                           </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset" ><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                     </div>
                  
               </div>
            </div>
    <div class="clearfix"></div>
            <div class="col-sm-4">
               <div class="">
               
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                        Subject
                        </label>
                        <div class="">
                           <select id="subject_id" name="subjectID" onchange="AddQuizName($(this).val())"  class="form-control">
                              <option value="">Select</option>
                               <?php  foreach ($subject as $key => $value) {
                                         ?>
                              <option value="<?php echo $value->subjectID ?>"><?php echo $value->subject ?></option>
                                        <?php } ?>
                             
                           </select>
                        </div>
                        <div class="clearfix"></div>
                        
                     </div>
           
               </div>
            </div>

          <div class="col-sm-4">
               <div class="">
               
                     <div class="form-group">
                        <label for="classesID" class="control-label">
                       Exam Name
                        </label>
                        <div class="">
                         <input type="text" id="quizID" required="" name="quiz_name">
                        </div>
                        <div class="clearfix"></div>
                     </div>
             
               </div>
            </div>


            <div class="clearfix"></div>
         </div>
      </div>



            
<div class="clearfix"></div>
                <?php } ?> 
<style type="text/css">
    .uploadsExcelQuiz{
        margin-top:4%;
    }
   .uploadsExcelQuiz .form-box {
    
    border-radius: 4px;
    width:100%;
    padding: 3% 4%;
  }
  .form-group {
    overflow: hidden;
  }
  .form-btn {
    margin-bottom: 0 !important;
  }
  input {
    width: 100%;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  .submit-btn {
    padding: 6px 30px;
    margin: 0 auto;
    width: auto;
    font-size: 18px;
    display: block;
  }
  #openContainer{
    display:none;
  }
</style>

<div class="uploadsExcelQuiz">
<div class="form-box">
    <div class="row">
      <div class="col-md-8">
  <table class="table">
    <thead>
      <tr>
        <th>S.N</th>
        <th>Quiz</th>
        <th>No of Question</th>
        <th>Download Excel</th>
        <th>Upload Question</th>
      </tr>
    </thead>
    <tbody> 
      <?php

     foreach ($quiz as $key => $value) {  
       ?>
      <tr>
        <td><?php echo $key++; ?></td>
        <td><?php echo $value->name ?></td>
        <td>
        <?php 

        $otherdb->where('quiz_id',$value->id);
        $otherdb->group_by('q_id');
        $queryGet = $otherdb->get('tableBysunil');
        echo $queryGet->num_rows();
        ?>      
        </td>
        <td><input type="submit" class="btn btn-success btn-active etsfilertButton" name="Submit" value="<?php echo $value->id ?>"></td>
      </tr>
      <?php } ?>
    </tbody>  
  </table>
               <input type="hidden" name="start_date" id="" class="form-control form-control datetimepicker_quiz_start" value="2018/08/01 10:00" placeholder="Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )"  required >
               <input type="hidden" name="end_date" id="datetimepicker_quiz_end" value="2018/08/04 23:00" class="form-control form-control datetimepicker_quiz_start" placeholder="End Date (Exam can be attempted before this date. eg. 2017-12-31 23:59:00 )"  required >
        

          <div class="form-row">
     <div class="form-group">
         <label for="courseName" class="col-md-2 col-form-label">Change Other Details</label>
          <div class="col-md-10">
        <div class="col-sm-3">
           <input type="radio" name="cod" id="chde1" value="1"><label for="chde1">YES</label>
        </div>
        <div class="col-sm-3">
        <input type="radio" name="cod" value="0" id="chde2" checked><label for="chde2">NO</label>
        </div>
        <div class="clearfix"></div>
          </div>
     </div>
            </div>


  <div class="form-row">
     <div class="form-group">
        
          <div class="col-md-10">
        <div class="col-sm-3">
           <input type="hidden" name="extype" id="extype1" value="1">
        </div>
        <div class="clearfix"></div>
          </div>
     </div>


<!--           <div class="form-group">
             <label for="courseName" class="col-md-2 col-form-label">Exam Mode</label>
              <div class="col-md-10">
            <div class="col-sm-3">
               <input type="radio" name="ch" id="jnuexam" value="1" checked><label for="jnuexam">Jnu Exam</label>
            </div>
            <div class="col-sm-3">
            <input type="radio" name="ch" value="2" id="normalexam"><label for="normalexam">Vmsu</label>
            </div>
            <div class="clearfix"></div>
              </div>
          </div> -->

            </div>

            <div id="openContainer">
              <div class="form-group">     
                 <label for="">Description</label> 
                 <textarea   name="description"  class="form-control tinymce_textarea" ></textarea>
              </div>
              <div class="form-group">     
                 <label for=""  >Duration (in min.)</label> 
                 <input type="text" name="duration"  value="<?php echo $quiz_settings->duration ?>" class="form-control" placeholder="Duration (in min.)"  required  >
              </div>
              <!-- <div class="form-group">     
                 <label for=""  >Allow Maximum Attempts</label> 
                 <input type="text" name="maximum_attempts"  value="10" class="form-control" placeholder="Allow Maximum Attempts"   required >
              </div> -->
              <div class="form-group">     
                 <label for="" >Correct Score</label> 
                 <input type="text" name="correct_score"  value="<?php echo $quiz_settings->correct_score ?>" class="form-control" placeholder="Correct Score"   required >
              </div>
              <div class="form-group">     
                 <label for=""  >InCorrect Score</label> 
                 <input type="text" name="incorrect_score"  value="<?php echo $quiz_settings->incorrect_score ?>" class="form-control" placeholder="InCorrect Score"  required  >
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div> <!-- col-sm-12 -->
            

        </div><!-- row -->

    </div><!-- Body -->
  </form>

</div><!-- /.box -->

<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>


<script type="text/javascript">

    $('#subCourseID').change(function() {

        var subCourseID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('exam/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>
<script type="text/javascript">
    
    function ResetCourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){

            $.ajax({
                type: 'POST',

                url: "<?=base_url('exam/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}



</script>
<script type="text/javascript">
    function ajaxGet_subCoursesExcel(id){
 $('.showLoaderSubcour').show();
    $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Get_subCourses')?>",
        data:{"id":id},
        success: function(response) {
            console.log(response);
            $("#subCourseIDExcel").html(response);
            $('.showLoaderSubcour').hide();
        }
            });
}
</script>

<script type="text/javascript">
    $(document).ready(function(){ 
    $("input[name=cod]").change(function() {
        var test = $(this).val();
        if (test==1) {
    $("#openContainer").show();
        }else{
      $("#openContainer").hide();  
        }
    
    }); 
});
</script>

<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesIDExcel").val();

       var  sub_CourseID  = $("#sub_CourseIDExcel").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerQuestion/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>

<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>
<style type="text/css">
	#change_start_date_time{
		width: 124px;
	}
	#change_end_date_time{
		width: 124px;
	}
</style>
