<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?=$this->lang->line('panel_title')?></title>
        <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" />
        <script>
        var uri =  "<?php echo $this->uri->segment(3) ?>";
        var uri4 =  "<?php echo $this->uri->segment(4) ?>";
        var base_url="<?php echo base_url();?>";

    </script>
        
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/pignose.calendar.css">

        
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/croppie.css" />
       
        <!-- Font CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/all_icons.css">

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/style.css">
        
        <!-- Loader CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/loader.css">

        <!-- MetisMenu CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/metisMenu/metisMenu.min.css">

        <!-- Custom CSS -->
        
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/animate.css">
        <!-- Change Color CSS --> 
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/skin/default-skin.css" />
        <link href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">

        <link href="<?php echo base_url('assets/etslabs/css/rid.css'); ?>" rel="stylesheet">
        
        <link href="<?php echo base_url('assets/etslabs/css/intlTelInput.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" >

        <link href="<?php echo base_url('assets/etslabs/css/styleExam.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/etslabs/css/login.css');?>" rel="stylesheet">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        
        <!-- <link href="<?php echo base_url('assets/datepicker/datepicker.css'); ?>" rel="stylesheet"> -->
        <?=link_tag('assets/datepicker/datepicker.css');?>
        <link href="<?php echo base_url('assets/timepicker/timepicker.css'); ?>" rel="stylesheet">
        <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/kavach.min.css"> -->

        <?=link_tag('assets/etslabs/css/kavach.min.css');?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/inilabs/EtDesigns/css/jquery.datetimepicker.min.css"/>

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/slick-flash.css" media="screen" title="no title" charset="utf-8">

    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"> -->


        <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/toastr/toastr.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/webcamjs/webcam.js'); ?>"></script>
        <script src="<?php echo base_url('assets/etslabs/js/basic.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/etslabs/js/slick-flash.js"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>




        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Exam'],
          ['1st Sem', 10],
          ['2nd Sem',  2],
          ['3rd Sem',  2],
          ['4th Sem', 2],
          ['5th Sem',   7]
        ]);

        var options = {
          title: 'My Exam'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
      <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Assignment'],
          ['1st Sem',     11],
          ['2nd Sem',      2],
          ['3rd Sem',  2],
          ['4th Sem', 2],
          ['5th Sem',    7]
        ]);

        var options = {
          title: 'My Assignment',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Semester', 'Exam', 'Assignment'],
          ['1st Sem', 600, 400],
          ['2nd Sem', 400, 250],
          ['3rd Sem', 600, 350],
          ['4th Sem', 400, 250],
          ['5rd Sem', 600, 350],
          ['6th Sem', 400, 250],
          ['7th Sem', 400, 250]
        ]);

        var options = {
          chart: {
            title: 'Student Performance',
            subtitle: 'Exam, Assignment: 2014-2017',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
    </head>
    <body>
     <?php
    $URI = "$_SERVER[REQUEST_URI]";
    $uriMain= str_replace('/subharti/','',$URI);
    ?>   
    <input type="hidden" name="uri_findOutFrom_urls_jquery" value="<?php echo $uriMain ?>" id="uri_findOutFrom_urls_jquery" >
    <script type="text/javascript">
      var redirecturi =  $('#uri_findOutFrom_urls_jquery').val();
    </script>
<div id="wrapper" class="">
<div class="fakeLoader"></div>
<?php if ($this->session->flashdata('success')): ?>
<script type="text/javascript">
   
   $.slickFlash('success', '<?=$this->session->flashdata('success') ?>');
   setTimeout( "$('.slick-flash').hide();", 10000);
</script>
<?php endif ?>
<?php if ($this->session->flashdata('error')): ?>
<script type="text/javascript">
   $.slickFlash('error', '<?=$this->session->flashdata('error') ?>');
   setTimeout( "$('.slick-flash').hide();", 10000);
</script>
<?php endif ?>