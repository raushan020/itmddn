      <div id="sidebar-wrapper">
        <a id="right-close-sidebar-toggle" href="#" class="theme-bg btn close-toogle toggle">
        Setting Pannel <i class="ti-close"></i></a>
        <div class="right-sidebar" id="side-scroll">
          <div class="user-box">
            <div class="profile-img">
              <img src="http://via.placeholder.com/400x400" alt="user">
              <!-- this is blinking heartbit-->
              <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <div class="profile-text">
              <h4>Daniel Dax</h4>
               <a href="#" class="user-setting"><i class="ti-settings"></i></a>
               <a href="#" class="user-mail"><i class="ti-email"></i></a>
               <a href="#" class="user-call"><i class="ti-headphone"></i></a>
               <a href="#" class="user-logout"><i class="ti-power-off"></i></a>
            </div>
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                <li class="active">
                  <a href="#options" data-toggle="tab">
                  <i class="ti-palette"></i> </a>
                </li>
                <li>
                  <a href="#comments" data-toggle="tab">
                  <i class="ti-bell"></i> </a>
                </li>
                <li>
                  <a href="#freinds" data-toggle="tab">
                  <i class="ti-comment-alt"></i> </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="options">
                  <div class="accept-request">
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status online"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Adam Dax <span class="task-time pull-right">Just Now</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status offline"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Rita Ray <span class="task-time pull-right">2 Min Ago</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status busy"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Daniel Mark <span class="task-time pull-right">7 Min Ago</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status offline"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Shruti Singh <span class="task-time pull-right">10 Min Ago</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="comments">
                  <div class="task">
                    <div class="task-overview">
                      <div class="alpha-box alpha-a">
                        <span>A</span>
                      </div>
                      <div class="task-detail">
                        <p>Hello, I am Maryam.</p>
                        <span class="task-time">2 Min Ago</span>
                      </div>
                    </div>
                    <div class="task-overview">
                      <div class="alpha-box alpha-d">
                        <span>D</span>
                      </div>
                      <div class="task-detail">
                        <p>Hello, I am Maryam.</p>
                        <span class="task-time">2 Min Ago</span>
                      </div>
                    </div>
                    <div class="task-overview">
                      <div class="alpha-box alpha-s">
                        <span>S</span>
                      </div>
                      <div class="task-detail">
                        <p>Hello, I am Maryam.</p>
                        <span class="task-time">2 Min Ago</span>
                      </div>
                    </div>
                    <div class="task-overview">
                      <div class="alpha-box alpha-h">
                        <span>H</span>
                      </div>
                      <div class="task-detail">
                        <p>Hello, I am Maryam.</p>
                        <span class="task-time">2 Min Ago</span>
                      </div>
                    </div>
                    <div class="task-overview">
                      <div class="alpha-box alpha-g">
                        <span>G</span>
                      </div>
                      <div class="task-detail">
                        <p>Hello, I am Maryam.</p>
                        <span class="task-time">2 Min Ago</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="freinds">
                  <div class="accept-request">
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status online"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Adam Dax <span class="task-time pull-right">Just Now</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status offline"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Rita Ray <span class="task-time pull-right">2 Min Ago</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status busy"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Daniel Mark <span class="task-time pull-right">7 Min Ago</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                    <div class="friend-overview">
                      <div class="friend-overview-img">
                        <img src="http://via.placeholder.com/400x400" class="img-responsive img-circle" alt="">
                        <span class="fr-user-status offline"></span>
                      </div>
                      <div class="fr-request-detail">
                        <h4>Shruti Singh <span class="task-time pull-right">10 Min Ago</span></h4>
                        <p>Accept Your Friend Request</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer"> © 2017 Kavach Admin by Themezhub.com </footer>
    </div>

       <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/metisMenu/metisMenu.min.js"></script>
    
    <script src="<?php echo base_url() ?>assets/plugins/slim-scroll/js/jquery.slimscroll.js"></script>
    
    <!-- FLOT CHARTS -->
    <script src="<?php echo base_url() ?>assets/plugins/Flot/jquery.flot.js"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    
    <script src="<?php echo base_url() ?>assets/plugins/Flot/jquery.flot.resize.js"></script>
    
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="<?php echo base_url() ?>assets/plugins/Flot/jquery.flot.pie.js"></script>
    
    <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
    <script src="<?php echo base_url() ?>assets/plugins/Flot/jquery.flot.categories.js"></script>
    
    <!-- ChartJS -->
    <script src="<?php echo base_url() ?>assets/plugins/chart.js/Chart.bundle.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/chart.js/Chart.js"></script>
    
    <!-- Loader JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/loader.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?>assets/etslabs/js/kavach.min.js"></script>
    
    <!-- Custom Float JavaScript -->
    <script src="<?php echo base_url() ?>assets/etslabs/js/custom/dashboard/dashboard1.js"></script>


    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.5.0/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>
  
<script>
    $(".CalenderYear" ).datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
    });
      $('#dob').datepicker({ startView: 2 });
  </script>
        <script src="<?php echo base_url() ?>assets/etslabs/js/etsStudent.js"></script>
        <script src="<?php echo base_url() ?>assets/etslabs/js/studentFilter.js"></script>
        <script src="<?php echo base_url() ?>assets/etslabs/js/etsExam.js"></script>
        <script src="<?php echo base_url() ?>assets/etslabs/js/etsClasses.js"></script>
     <script src="<?php echo base_url() ?>assets/etslabs/js/etsSubject.js"></script>
     <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->

<script>
     function inline_edit_quiz(val,column,quiz_id,editValue){
          $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
            $.ajax({
                type: "POST",
                url:"<?=base_url('exam/inline_edit_quiz')?>",
                data:{"quiz_id":quiz_id,"column":column,"editValue":editValue},
                success: function(response) {
                  console.log(response);
                 $(val).css("background","#FDFDFD");
                }
                    });

          }
        </script>

              <script>
     function Comman_field_quiz_setting(val,column,id,editValue){
          $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
            $.ajax({
                type: "POST",
                url:"<?=base_url('exam/quizSetting')?>",
                data:{"id":id,"column":column,"editValue":editValue},
                success: function(response) {
                  console.log(response);
                 $(val).css("background","#FDFDFD");
                }
                    });

          }
        </script>

<script type="text/javascript">
  
 function  EditSubCourse(val,column,SubcourseID,editValue){

  $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");

    $.ajax({
        type: "POST",
        url:"<?=base_url('AjaxController/EditSubCourse')?>",
        data:{"val":editValue,"SubcourseID":SubcourseID,"column":column},
        success: function(response) {
         $(val).css("background","#FDFDFD");
        }
            });

  }
</script>
<script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#example4').DataTable({
            dom: 'lBfrtip',
              scrollY:"800px",
              scrollX:true,
              scrollCollapse: true,
              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
          fixedColumns:   {
            leftColumns: 3,
            rightColumns: 1
        },
          search: false, 
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'student/ajaxStudents'; ?>",  
                type:"POST"  
           }, 
      "columnDefs": [
             { "orderable": false,"targets":0},
             { "orderable": false,"targets":1},
             { "orderable": false,"targets":18},
             { "orderable": false,"targets":21},
             { "orderable": false,"targets":22},
             { "orderable": false,"targets":23},
              ], 

      "columns": [
              { "data": "sn" },
              { "data": "img" },
              { "data": "name" },
              { "data": "father_name" },
              { "data": "mother_name" },
              { "data": "roll" },
              { "data": "classes" },
              { "data": "sub_course" },
              { "data": "session" },
              { "data": "sessionType" },
              { "data": "examType" },
              { "data": "education_mode" },
              { "data": "student_status" },
              { "data": "phone" },
              { "data": "dob" },
              { "data": "sex" },
              { "data": "email" },
              { "data": "aadhar" },
              { "data": "nationality" },
              { "data": "create_date" },
              { "data": "teacherName" },
              { "data": "employment_status" },
              { "data": "status" },
              { "data": "action" },
           ], 
      });  
 });  
 </script>

<script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#subjectsTables').DataTable({
            dom: 'lBfrtip',
              scrollY:"800px",
              scrollX:true,
              scrollCollapse: true,
              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
          search: false, 
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'subject/AjaxTable'; ?>",  
                type:"POST"  
           }, 
  "columnDefs": [
    { "orderable": false,"targets":0},
    { "orderable": false,"targets":6}
  ],
      "columns": [   
              { "data": "sn" },
              { "data": "subject_code" },
              { "data": "subject" },
              { "data": "classes" },
              { "data": "sub_course" },
              { "data": "yearsOrSemester" },
              { "data": "action" },

           ], 
      });  
 });  
 </script>

<script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#subjectsTablesPdf').DataTable({
            dom: 'Bfrtip',
              scrollY:"800px",
              scrollX:true,
              scrollCollapse: true,
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
          search: false, 
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'lms/AjaxTable'; ?>",  
                type:"POST"  
           },  
      "columns": [
              { "data": "sn" },
              { "data": "subject_code" },
              { "data": "subject" },
              { "data": "classes" },
              { "data": "sub_course" },
              { "data": "yearsOrSemester" },
              { "data": "countpdf" },
              { "data": "action" },
           ], 
      });  
 });  
 </script>
<script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#examTables').DataTable({
            dom: 'lBfrtip',
              scrollY:"800px",
              scrollX:true,
              scrollCollapse: true,
            lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
         "columnDefs": [
             { "orderable": false,"targets":0},
             { "orderable": false,"targets":8},
             { "orderable": false,"targets":9},
              ], 
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
          fixedColumns:   {
            leftColumns: 3,
            rightColumns: 1
        },
          search: false, 
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'exam/AjaxTable'; ?>",  
                type:"POST"  
           },  
      "columns": [
              { "data": "sn" },
              { "data": "quiz_name"},
              { "data": "subject_code" },
              { "data": "start_date" },
              { "data": "end_date" },
              { "data": "subject" },
              { "data": "classes" },
              { "data": "sub_course" },
              { "data": "yearsOrSemester" },
              { "data": "action" },
           ], 
      });  
 });  
 </script>



<script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#exam_listTables').DataTable({
            dom: 'Bfrtip',
              scrollY:"800px",
              scrollX:true,
              scrollCollapse: true,
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
          search: false, 
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'exam/AjaxTable_exam_list/'.$this->uri->segment(3); ?>",  
                type:"POST"  
           },  
      "columns": [
              { "data": "sn" },
              { "data": "quiz_name"},
              { "data": "subject_code" },
              { "data": "subject" },
              { "data": "yearsOrSemester" },
              { "data": "action" },
           ], 
      });  
 });  
 </script>

<script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#questionTables').DataTable({
            dom: 'lBfrtip',
              scrollY:"800px",
              scrollX:true,
              scrollCollapse: true,
              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
         "columnDefs": [
             { "orderable": false,"targets":0},
             { "orderable": false,"targets":2},
              ], 
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],

          search: false, 
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'exam/ /'.$this->uri->segment(3); ?>",  
                type:"POST"  
           },  
      "columns": [
              { "data": "sn" },
              { "data": "question"},
              { "data": "action" },
           ], 
      });  
 });  
 </script>

 <script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#courses').DataTable({
        dom: 'lBfrtip',
         lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
         "columnDefs": [
             { "orderable": false,"targets":0},
             { "orderable": false,"targets":6},
             { "orderable": false,"targets":7},
              ], 
       buttons: [
                    'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
          search: false,
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'classes/ajaxCourse'; ?>",  
                type:"POST"  
           },  
      "columns": [
              { "data": "sn" },
              { "data": "name" },
              { "data": "classes" },
              { "data": "duration" },
              { "data": "mode" },
              { "data": "fee" },
        { "data": "notice"},
              { "data": "action" }
           ], 
      });  
 });  
 </script> 

 
 <!-- Ajax code for Teacher --> 

 <script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#example1').DataTable({
        dom: 'lBfrtip',
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5',
                  'colvis'
              ],
        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
         "columnDefs": [
             { "orderable": false,"targets":0},
             { "orderable": false,"targets":4},
              ],
          search: false,
        language: {
       processing: "<img src='<?php echo base_url() ?>uploads/images/loading-circle.gif'>",
       },
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'teacher/ajaxTeachers'; ?>",  
                type:"POST"  
           },  
      "columns": [
              { "data": "sn" },
              { "data": "name" },
              { "data": "username" },
              { "data": "email" },
              { "data": "action" },
           ], 
      });  
 });  
 </script>
 <?php if ($this->session->flashdata('success')): ?>
            <script type="text/javascript">
                toastr["success"]("<?=$this->session->flashdata('success');?>")
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "500",
                  "hideDuration": "500",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
            </script>
        <?php endif ?>
        <?php if ($this->session->flashdata('error')): ?>
           <script type="text/javascript">
                toastr["error"]("<?=$this->session->flashdata('error');?>")
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "500",
                  "hideDuration": "500",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
            </script>
        <?php endif ?>
<script type="text/javascript">
  function  CourseSDependent(id){ 
     $('.showLoaderSubcour').show();
    $.ajax({
        type: "POST",
        url:"<?=base_url('student/Get_subCourses')?>",
        data:{"id":id},
        success: function(response) {
            $("#subCourseID").html(response);
            $('.showLoaderSubcour').hide();
        }
            });
    
        $.ajax({
        type: "POST",
        url:"<?=base_url('AjaxController/fetch_Semester')?>",
        data:{"id":id},
        success: function(response) {
           
            $("#fetchYearsAndSem").html(response);
        }
            });

  }
</script>
<script type="text/javascript">
  
 function  AddSubject(){
var classesID =  $('#classesID').val();
var subjectName =  $('#subjectName').val();
var subjectCode =  $('#subjectCode').val();
var yearSemesterID =  $('#yearSemesterID').val();
var subCourseID =  $('#sub_CourseID').val();
    $.ajax({
        type: "POST",
        url:"<?=base_url('AjaxController/AddSubject')?>",
        data:{"classesID":classesID,"subjectName":subjectName,"subjectCode":subjectCode,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID},
        success: function(response) {
   
            $('#subjectCode').val('');
            $('#subjectName ').val('');
            $("#appendSujectsDetails").html(response);

        }
            });

  }
</script>

<script type="text/javascript">
  
 function  DeleteSubject(subjectID){
var classesID =  $('#classesID').val();
var yearSemesterID =  $('#yearSemesterID').val();
var subCourseID =  $('#sub_CourseID').val();
    $.ajax({
        type: "POST",
        url:"<?=base_url('AjaxController/DeleteSubject')?>",
        data:{"classesID":classesID,"subjectID":subjectID,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID},
        success: function(response) {
         
            $("#appendSujectsDetails").html(response);
        }
            });

  }

</script>

<script type="text/javascript">
  
 function  EditSubject(val,column,subjectID,editValue){
  $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");

    $.ajax({
        type: "POST",
        url:"<?=base_url('AjaxController/EditSubject')?>",
        data:{"val":editValue,"subjectID":subjectID,"column":column},
        success: function(response) {
         $(val).css("background","#FDFDFD");
        }
            });

  }

</script>

<script type="text/javascript">
  function passwordShow() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>

<script type="text/javascript">
  $(".CalenderYear").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years"
});

</script>

<script type="text/javascript">
  function  Set_sub_coureses_for_filter(subCourseID){
alert(subCourseID);
$.ajax({
type:'POST',
url:"<?php echo base_url() ?>student/set_session_of_subCourse",
data:{subCourseID:subCourseID},
success:function(){

}
}); 


  }
</script>
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="<?php echo base_url('assets/inilabs/intlTelInput.js') ?>"></script>
  <script>
    var telInput = $("#phonecountry"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");

    $("#phonecountry").intlTelInput({
      // allowDropdown: false,
      autoHideDialCode: false,
      autoPlaceholder: "off",
      dropdownContainer: "body",
      excludeCountries: ["us"],
      formatOnDisplay: false,
      geoIpLookup: function(callback) {
        $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          callback(countryCode);
        });
      },
      hiddenInput: "full_number",
      initialCountry: "auto",
      nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      placeholderNumberType: "MOBILE",
      preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript:"<?php echo base_url() ?>assets/inilabs/utils.js"
    });
    var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};
// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hide");
    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hide");
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);

  </script>

  


<!-- EtDesigns -->
  <script type="text/javascript" src="https://cdn.rawgit.com/monkeecreate/jquery.simpleWeather/master/jquery.simpleWeather.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/inilabs/EtDesigns/js/weather.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/inilabs/EtDesigns/js/calender.js"></script>

        <script>
            /*jslint browser:true*/
            /*global jQuery, document*/

            jQuery(document).ready(function () {
                'use strict';
                jQuery('.datetimepicker_quiz_start').datetimepicker();
            });
        </script>

<!-- old file -->



