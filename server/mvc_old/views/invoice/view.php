<?php
$tuition_fee_semester = 0;
$examFee = 0;
foreach ($paymentDetails as $key => $value) {
  if($value->feeType=='tuitionFee'){
 $tuition_fee_semester  += $value->paymentamount; 
}  if($value->feeType=='examinationFee'){
$examFee  += $value->paymentamount;
}
}
          if($this->input->get('year')){
              $year = $_GET['year'];
              }else{
              $year = '1st_Semester';
              }
   ?>
<div class="">
   <div class="row page-titles">
      <div class="col-md-5 align-self-center">
         <h3 class="text-themecolor"><i class="fa fa-bullhorn"></i>Fee</h3>
      </div>
      <div class="col-md-7 align-self-center">
         <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("invoice/index")?>">Fee</a></li>
            <li class="active"><?=$this->lang->line('view')?></li>
         </ol>
      </div>
   </div>
   <!-- /.box-header -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
               <div class="card-body">
                  <?php 
                     if(count($invoice)) {
                     
                         $usertype = $this->session->userdata("usertype");
                     
                         if($usertype == "Admin" || $usertype == "Accountant" || $usertype == 'Student' || $usertype == "ClgAdmin" || $usertype == "superadmin" || $usertype == "Teacher" || $usertype == "Support"|| $usertype == "Academic") {

                    
                     ?>
                     <div class="row">
                     </div>
                  <div class="well">
                     <div class="row">
                        <div class="col-sm-6">
                           <button class="btn btn-success btn-xs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
                           <!-- <?php
                              echo btn_add_pdf('invoice/print_preview/'.$invoice->invoiceID, $this->lang->line('pdf_preview')) 
                              
                              ?> -->
               

                        </div>
                        <div class="col-sm-6">
                    
                           <div class="col-sm-3">Select Year</div>
                           <div class="col-sm-9">
                              <select class='form-control' name='semesterId' onchange='invoice_filter_url($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>
                                <option >Select Semester</option>
                                 <?php 
                                    $looping    =  (int) $classes->duration;
                                    
                                    
                                          for ($i=1; $i <=2*$looping; $i++) {

                                                 if (CallSemester($i)==$year) {

                                            $select = 'Selected';

                                         }else{

                                         $select = '';

                                         }

                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                         

                                       }
                                    
                                    
                                     ?>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="printablediv">
                     <section class="content invoice" >
                        <!-- title row -->
               
                        <!-- info row -->

                        <!-- /.row -->
                        <!-- Table row -->
                        <br />
                          <?php if($usertype=='ClgAdmin' || $usertype=='Admin' || $usertype=='Accountant'  || $usertype == "Support"|| $usertype == "Academic" ){ ?>
                        <div class="row">
                           <div class="col-xs-12" id="hide-table">
                              
<form action="<?php echo base_url() ?>invoice/payment/<?php echo $invoice->invoiceID?>/<?php echo $year?>" onsubmit="return confirm('Do you really want to submit the form?');" method = "get">

<div class="col-sm-6"><h3>Fee Structure</h3></div>
<div class="col-sm-6">
                <div class="action-layout pull-right">
                    <ul>

                        <li class="active-btn">

                            <input type="submit" class="btn btn-success etsfilertButton disabled"  name="Active" value="Payment" disabled>

                        </li>

                    </ul>

                </div>  
                </div>

                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                      <th>Fee Type</th>
                                       <th>Total Fee</th>
                                        <th>Due Fee</th>
                                       <th>Status</th>
                                       <th>Add to pay</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                    <td>
                                  <div class="input-group">
                                    Tution Fee 
                                      </div>
                                      </td>
                                      <td>
                                      <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->tuitionFee)  ?>
                                       </td>
                                      <td>
                                       <?php  
                                       echo  $invoice->tuitionFee-$tuition_fee_semester

                                       ?> 
                                      </td> 
                                      <td>
                                        <?php 
                          if($tuition_fee_semester){      
                          if($tuition_fee_semester==$invoice->tuitionFee){
                          $status = $this->lang->line('invoice_fully_paid');  
                          }
                          else{
                          $status = $this->lang->line('invoice_partially_paid');
                          } 
                          }else{
                                 $status = $this->lang->line('invoice_notpaid');
                               }
                                   echo "<strong>".$this->lang->line('invoice_status'). " : "."</strong>"."<a id = 'StatusAfterAjax' class='btn btn-success btn-xs'>".$status."</a>";
                                 ?>
                                      </td>
                                      <td>
                                        <?php
                                        if ($tuition_fee_semester!=$invoice->tuitionFee) {
                                         ?>
                          <label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value='tuitionFee'/>
                            <span class='checkmark checkmark-action-layout'></span>
                        </label>
                      <?php } ?>
                      </td>
                                    </tr>

  <?php 
if ($AllpaymentDetails) {
}else{
 if($student->student_status=='lateral'){ 
   ?>                                
                                <tr>
                                    <td>
                                    <div class="input-group">
                                      Lateral Fee 
                                      </div>
                                      </td>
                                      <td>
                                      <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->lateral)  ?>
                                       </td>
                                       <td>
                                         <?php echo number_format($invoice->lateral)  ?>
                                       </td>
                                      <td> 
  
                               <?php 
                            $status = $this->lang->line('invoice_notpaid');

                                   echo "<strong>".$this->lang->line('invoice_status'). " : "."</strong>"."<a id = 'StatusAfterAjax' class='btn btn-success btn-xs'>".$status."</a>";
                                 ?></td>
                                 <td>

                     <label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value='lateral'/>
                            <span class='checkmark checkmark-action-layout'></span>
                        </label>
 

                      </td>
                                    </tr>
<?php } else{ ?>

                                <tr>
                                    <td>
                                    <div class="input-group">
                                      Registration Fee 
                                      </div>
                                      </td>
                                      <td>
                                      <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->registration_fee)  ?>
                                       </td>
                                       <td>
                                         <?php echo number_format($invoice->registration_fee)  ?>
                                       </td>
                                      <td> 
  
                               <?php 
                            $status = $this->lang->line('invoice_notpaid');

                                   echo "<strong>".$this->lang->line('invoice_status'). " : "."</strong>"."<a id = 'StatusAfterAjax' class='btn btn-success btn-xs'>".$status."</a>";
                                 ?></td>
                                 <td>

                     <label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value='registration_fee'/>
                            <span class='checkmark checkmark-action-layout'></span>
                        </label>
 

                      </td>
                                    </tr>
<?php }  } ?>

                                  <tr>

                                    <td>
                                        <div class="input-group">
                                      Examination Fee
                                      </div>
                                      </td>
                                      <td>
                                      <i class="fa fa-inr" aria-hidden="true"></i> <?php echo number_format($invoice->examinationFee)  ?>
                                       </td>
                                      
                                       <td>
<?php 
  if ($examFee) { 
echo 0;
  }else{
echo number_format($invoice->examinationFee-$examFee); 
  }  

    ?>    

                                       </td>

                                      <td>
           <?php 
  if ($examFee) { 
$status = $this->lang->line('invoice_fully_paid');  
}else{
  $status = $this->lang->line('invoice_notpaid');
}
    
                                   echo "<strong>".$this->lang->line('invoice_status'). " : "."</strong>"."<a id = 'StatusAfterAjax' class='btn btn-success btn-xs'>".$status."</a>";
                                 ?>

                                      </td>
                                      <td>
                                   <?php 

  if ($examFee==0) { ?>       
                            <label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value='examinationFee'/>
                            <span class='checkmark checkmark-action-layout'></span>
                        </label>
                      <?php } ?>
                                      </td>
                                    </tr>
                                
                                 </tbody>
                              </table>
                            </form>
                           </div>
                        </div>
 <?php } ?>
                  <!-- paid fee --> 
                          <?php if ($usertype == 'ClgAdmin' || $usertype == "Teacher" || $usertype == "Accountant") { ?>
                        <div class="row">
                           <div class="col-xs-12" id="hide-table">
                              <h3>Paid Fee</h3>
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                      <th>Fee Type</th>
                                       <th>Installment</th>
                                       <th>Year</th>
                                       <th>Fee Date</th>
<!--                                        <th>Receipt No.</th>
                                       <th>Receipt image</th> -->
                                       <th>Paid Fee</th>
                                       <th>Payment Type</th>
                                      <?php if($usertype!='Student') { ?>
                                       <th>Action</th>
                                      <?php } ?>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php

                                       foreach ($paymentDetails as $key => $value) { ?>
                                    <tr class="displayNone" id="for_write_tr_fee_paid_<?php echo $key ?>">
                                      <td><?php echo $value->feeType ?></td>
                                       <td>Installment <?php echo  $key+1 ?></td>
                                       <td><?php echo str_replace('_', " ", $value->yearsOrSemester); ?></td>
                                       <td> <input type="text" name="payment_date" class="form-control datetimepicker_quiz_data" value="<?php echo $value->paymentdate  ?>" onBlur= "inline_edit_invoice(this, 'paymentdate', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val()),'<?php echo $value->feeType ?>'"></td>
                                  <!--      <td><input type="" name="receipt" class="form-control" value="<?php echo $value->receipt  ?>" onBlur= "inline_edit_invoice(this, 'receipt', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())">
                                       </td> -->
                              <!--          <td>
                                        <?php 
                                        if ($value->receipt_src == ' ') { ?>
                                             no image
                                        <?php  } else { ?>
                                          <a href="<?php echo base_url() ?>uploads/receipt/<?php echo $value->receipt_src ?>" target =_blank >View</a>   
                                      <?php  } ?>
                                       </td> -->
<?php if($value->feeType=='tuitionFee'){ ?>
                                       <td>
                                          <input type="number" name="paymentamount" class="form-control payment_amout_count" value="<?php echo ($value->paymentamount)  ?>" id ="langda_aam<?php echo $key  ?>" onBlur= "inline_edit_invoice(this, 'paymentamount', '<?php echo $value->invoiceID ?>','<?php echo $value->paymentID ?>', $(this).val())">
                                       </td>
 <?php } else{ ?>             
 <td><?php echo ($value->paymentamount)  ?></td>
 <?php } ?>                         

                                       <td>
                                          <div>
                                             <select class="form-control" onchange="changeInvoicepaymenttype(this, '<?php echo $value->paymentID ?>', $(this).val()) ">
                                                <option value="" >invoice select paymentmethod</option>
                                                <option value="Cash" <?php if($value->paymenttype=='Cash'){echo "Selected";} ?>>Cash Mode</option>
                                                <option value="Cheque" <?php if($value->paymenttype=='Cheque'){echo "Selected";} ?>>Cheque Mode</option>
                                                <option value="Bank" <?php if($value->paymenttype=='Bank'){echo "Selected";} ?>>Bank Transfer</option>
                                                <option value="Draft" <?php if($value->paymenttype=='Draft'){echo "Selected";} ?>>DD</option>
                                                <option value="PayuMoney" <?php if($value->paymenttype=='PayuMoney'){echo "Selected";} ?>>PayU Money</option>
                                                <option value="onlinePayment" <?php if($value->paymenttype=='onlinePayment'){echo "Selected";} ?>>Online Payment</option>
                                             </select>
                                          </div>
                                       </td>
                                    <td><!-- 
                                    <a href="<?php echo base_url() ?>invoice/PaymentDelete/<?php echo $value->paymentID ?>/<?php echo $year ?>" onclick="return confirm('Do you Really want to Delete This?')" class="btn btn-success btn-xs mrg for_margR">Remove</a> -->
                                   <!--  <a  data-target="#addBookDialog" data-toggle="modal" data-id="<?php echo $value->paymentID ?>" title="Add this item" class="btn btn-success btn-xs mrg for_margR open-AddBookDialog">Add Receipt</a> -->
                                     <a   class="btn btn-success btn-xs mrg for_margR open-AddBookDialog" onclick='Edit_fee_paid_row_back("<?php echo $key ?>")'>Back</a>
                                  </td>
                                    </tr>
                                    <tr id="for_read_tr_fee_paid_<?php echo $key; ?>">
                                      <td><?php echo $value->feeType ?></td>
                                       <td>Installment <?php echo  $key+1 ?></td>
                                       <td><?php echo str_replace('_', " ", $value->yearsOrSemester); ?></td>
                                       <td><?php echo $value->paymentdate  ?></td>
                                      <!--  <td><?php echo $value->receipt  ?></td> -->
<!--                                        <td>
                                        <?php 
                                        if ($value->receipt_src == ' ') { ?>
                                             no image
                                        <?php  } else { ?>
                                          <a href="<?php echo base_url() ?>uploads/receipt/<?php echo $value->receipt_src ?>" target =_blank >View</a>   
                                      <?php  } ?>
                                       </td> -->
                                       <td>
                                        <?php echo ($value->paymentamount)  ?>
                                       </td>
                                       <td>
                                      <?php echo $value->paymenttype; ?>
                                       </td>
                                    <!-- <td> -->
                                      <td>
                                        <button type="button" onclick='Edit_fee_paid_row("<?php echo $key; ?>")' class="btn btn-success btn-xs mrg for_margR">Edit</button>
                                     </td>
                                    </tr>
                                    <?php } ?>           
                                 </tbody>
                              </table>
 <?php } if($usertype=='Student'){  ?>
                      <div class="row">
                           <div class="col-xs-12" id="hide-table">
                          
                              <table class="table table-striped">
                                 <thead>
                                    <tr>
                                       <th>Due Amount</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                  <tr>
                                    <td>
                                     <?php 
                                          $due =  floor($invoice->totalfeepaidperyear)-$feePaidPerYear;
                                        echo number_format($due);
                                      ?> 
                                    </td>
                                  </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                      <?php  } ?>
                           </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18">
                          </div>
                           <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18">
                              <p class="lead"><?=$this->lang->line('invoice_amount')?></p>
                              <div class="table-responsive">
<!--                                  <table class="table">
                                    <tr>
                                       <th class="col-sm-8 col-xs-8">Total Fee</th>
                                       <td class="col-sm-4 col-xs-4"><i class="fa fa-inr" aria-hidden="true"></i> 
                                        <?php 
                                        if($feeType=='clg'){
                                          if ($year=='all') {
                                          echo number_format($invoice->amount);
                                          }else{
                                          echo number_format($invoice->totalfeepaidperyear); 
                                          }
                                        }else{
                                          echo number_format($invoice->hostelFee);
                                        }

                                           ?></td>
                                    </tr>
                                 </table> -->
<!--                                  <table class="table">
                                    <tr>
                                       <th class="col-sm-8 col-xs-8"><?=$this->lang->line('invoice_made');?></th>
                                       <td class="col-sm-4 col-xs-4" id="paid_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> 
                                          <?php 
                      
                                             echo  number_format($feePaidPerYear);
                                                 

                                              ?>
                                       </td>
                                    </tr>
                                 </table> -->
                                 <table class="table">
                                    <tr>
                                       <th class="col-sm-8 col-xs-8">Total tuition fee</th>
                                    
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> <?= number_format($invoice->tuitionFee)  ?></td>
                                    </tr>

                                 <tr>
                                       <th class="col-sm-8 col-xs-8">Exam Fee</th>
                                  
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> <?= number_format($invoice->examinationFee)  ?></td>
                                    </tr>

                                          <tr>
                                       <th class="col-sm-8 col-xs-8">Total Fee</th>
                                  
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> <?= number_format($invoice->examinationFee+$invoice->tuitionFee)  ?></td>
                                    </tr>

                                  <tr>
                                       <th class="col-sm-8 col-xs-8">Total Due</th>
                                     <?php
                                    
                                     $due = $invoice->tuitionFee-$tuition_fee_semester+$invoice->examinationFee-$examFee;
                                      
                                           ?>
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> <?= number_format($due)  ?></td>
                                    </tr>

                                 </table>
                              </div>
                           </div>
                           <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <div id = "alertAfterinvoiceAjax">
                        </div>
                     </section>
                     <!-- /.content -->
                  </div>

      </div>
   </div>
</div>
</div>
</div>

                              <script>
                                 function inline_edit_invoice(val,column,invoiceID,paymentID,editValue,feeType){    
                                 swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                 
                                       var count_payment_box = $(".payment_amout_count").length;
                                
                                 var countPayment = 0;
                                 
                                     for (i = 0; i < count_payment_box; i++) {
                                 
                                 countPayment +=  parseInt($("#langda_aam"+i+" ").val());
                                 
                                 }
                                 
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"paymentID":paymentID,"countPayment":countPayment,"feeType":feeType},
                                 
                                            success: function(response) {
                                
                                                if (response=='Exceede') {
                                  console.log(response);  
                                                    var alertd = `<div class='alert alert-danger' role='alert>This is a danger alert—check it out!</div>`;
                                 
                                                // location.reload(true);
                                 
                                                }else if(response=='Empty'){
                                  console.log(response);
                                                 // location.reload(true);
                                 
                                                }
                                 
                                                else{
                                
                                 console.log(response);
                                                // location.reload(true);
                                 
                                                }
                                 
                                 
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 location.reload(true);
                                 swal("Your imaginary file is safe!");
                                 }
                                 });
                                      
                                 
                                 
                                 
                                      }
                                 
                                    
                              </script>
                              <script>
                                 function inline_edit_invoice_fee_structure(val,column,invoiceID,duration,editValue){
                                 
                                   
                             
                                 
                                 
                                    swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice_fee_structure')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"duration":duration},
                                 
                                            success: function(response) {
                                 
                                                 location.reload(true);
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 swal("Your imaginary file is safe!");
                                 location.reload(true);
                                 }
                                 });
                                 
                                   
                                 
                                 
                                      }
                                 
                                    
                              </script>
                          <script>
                                 function inline_edit_invoice_fee_structure_forHostel(val,column,invoiceID,duration,editValue){
                                      var month  = "<?php echo $month  ?>";
                                  var monthYear  = "<?php echo $monthYear  ?>";
                                 
                                 
                                    swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice_fee_structure_forHostel')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"duration":duration,"month":month,"monthYear":monthYear},
                                 
                                            success: function(response) {
                                 
                                                 location.reload(true);
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 swal("Your imaginary file is safe!");
                                 location.reload(true);
                                 }
                                 });
                                 
                                   
                                 
                                 
                                      }
                                 
            
                              </script>
                              <script>
                                 function inline_edit_invoice_amount(val,column,invoiceID,paymentID,editValue){
                                 
                                    // confirm('Are You Sure ???');
                                 var yearsOrSemester =  "<?php echo $year ?>"
                                 
                                    swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                        $.ajax({
                                 
                                            type: "POST",
                                 
                                            url:"<?=base_url('AjaxController/inline_edit_invoice_amount')?>",
                                 
                                            data:{"invoiceID":invoiceID,"column":column,"editValue":editValue,"yearsOrSemester":yearsOrSemester},
                                 
                                            success: function(response) {
                                 
                                                 location.reload(true);
                                 
                                             $(val).css("background","#FDFDFD");
                                 
                                            }
                                 
                                                });
                                 
                                 
                                 swal("Payment has been changed", {
                                  icon: "success",
                                 });
                                 } else {
                                 swal("Your imaginary file is safe!");
                                 location.reload(true);
                                 }
                                 });
                                 
                                   
                                 
                                 
                                      }
                                 
                                    
                              </script>
                              <script type="text/javascript">
                                 function changeInvoicepaymenttype(val,paymentID,editValue){
                                 
                                     
                                 
                                 swal({
                                 title: "Are you sure?",
                                 text: "",
                                 icon: "warning",
                                 buttons: true,
                                 dangerMode: true,
                                 })
                                 .then((willDelete) => {
                                 if (willDelete) {
                                 
                                 $(val).css("background","#FFF url(<?php echo base_url() ?>uploads/images/loaderIcon.gif) no-repeat right");
                                         $.ajax({
                                 
                                             type: "POST",
                                 
                                             url:"<?=base_url('AjaxController/changeInvoicepaymenttype   ')?>",
                                 
                                             data:{"paymentID":paymentID,"editValue":editValue},
                                 
                                             success: function(response) {
                                 
                                                  location.reload(true);
                                 
                                              $(val).css("background","#FDFDFD");
                                 
                                             }
                                 
                                         });
                                 
                                 swal("Payment has been changed", {
                                   icon: "success",
                                 });
                                 } else {
                                 location.reload(true);
                                 swal("Your imaginary file is safe!");
                                 }
                                 });
                                 
                                 }
                                 
                              </script>
<!-- email end here -->
<script language="javascript" type="text/javascript">
   function printDiv(divID) {
   
       //Get the HTML of div
   
       var divElements = document.getElementById(divID).innerHTML;
   
       //Get the HTML of whole page
   
       var oldPage = document.body.innerHTML;
   
   
   
       //Reset the page's HTML with div's HTML only
   
       document.body.innerHTML = 
   
         "<html><head><title></title></head><body>" + 
   
         divElements + "</body>";
   
   
   
       //Print Page
   
       window.print();
   
   
   
       //Restore orignal HTML
   
       document.body.innerHTML = oldPage;
   
   }
   
   function closeWindow() {
   
       location.reload(); 
   
   }
   
   
   function invoice_filter_url(val){
   
   var invoiceID = "<?php echo $this->uri->segment(3) ?>";
   
   window.location.href=base_url+"invoice/view/"+invoiceID+"?type=clg&year="+val;
   
   }
   
   
   function check_email(email) {
   
       var status = false;     
   
       var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
   
       if (email.search(emailRegEx) == -1) {
   
           $("#to_error").html('');
   
           $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');
   
       } else {
   
           status = true;
   
       }
   
       return status;
   
   }
   
   
   
   
   
   $("#send_pdf").click(function(){
   
       var to = $('#to').val();
   
       var subject = $('#subject').val();
   
       var message = $('#message').val();
   
       var id = "<?=$invoice->invoiceID;?>";
   
       var error = 0;
   
   
   
       if(to == "" || to == null) {
   
           error++;
   
           $("#to_error").html("");
   
           $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');
   
       } else {
   
           if(check_email(to) == false) {
   
               error++
   
           }
   
       } 
   
   
   
       if(subject == "" || subject == null) {
   
           error++;
   
           $("#subject_error").html("");
   
           $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');
   
       } else {
   
           $("#subject_error").html("");
   
       }
   
   
   
       if(error == 0) {
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('invoice/send_mail')?>",
   
               data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,
   
               dataType: "html",
   
               success: function(data) {
   
                   location.reload();
   
               }
   
           });
   
       }
   
   });
   
   
   
</script>
<?php }} ?>
<style type="text/css">
   .boredr_none .nav-tabs{
   border-bottom: 0px solid #e8edef;
   }
   .nopading{
   padding-right:0px;
   padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #2057a8;
   color:#fff;
   }
   .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
   color: #fff;
   }
   .theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */
   color: #fff;
   }
   .theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */
   color: #fff;
   }
   .theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */
   color: #fff;
   }
   .forpostionReletive {
   position: relative;
   }
   .postionAbsoluter {
   position: absolute;
   right: 7px;
   top: 8px;
   }
   .action-layout ul li{
   display:inline-block;
   }
</style>



  <!-- Modal -->
  <div class="modal fade" id="addBookDialog" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Receipt</h4>
        </div>
        <div class="modal-body">
          <form  action="<?php echo base_url() ?>invoice/addReceipt" method="post" enctype="multipart/form-data">
    <label>Upload Receipt</label>
    <input type='file' name='userfile' />
    <input type="hidden" name="invoiceID" value="<?php echo $invoice->invoiceID ?>">
    <input type="hidden" name="year" value="<?php echo $year ?>">
    <input type="hidden" name="paymentID" id="bookId" value="" >
    <input type="hidden" name="feeType"  value="<?php echo $feeType ?>" >
    <input type="hidden" name="month"  value="<?php echo $month ?>" >
    <input type="hidden" name="monthYear"  value="<?php echo $monthYear ?>" >
    <input type="submit" name="submit" value="Submit" class="btn-success">
    </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <script type="text/javascript">
      $(document).on("click", ".open-AddBookDialog", function () {
     var myBookId = $(this).data('id');
     $(".modal-body #bookId").val( myBookId );
     // As pointed out in comments, 
     // it is unnecessary to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
  </script>
  <style type="text/css">
  #for_write_tr_fee_str{
    display:none;
  }
</style>
<script type="text/javascript">
  function Edit_fee_struc_row(){
    $('#for_read_tr_fee_str').hide();
    $('#for_write_tr_fee_str').show();

  }
    function Edit_fee_struc_row_back(){
    $('#for_read_tr_fee_str').show();
    $('#for_write_tr_fee_str').hide();

  }
</script>
<style type="text/css">
  .displayNone{
    display:none;
  }
</style>
<script type="text/javascript">
  function Edit_fee_paid_row(val){
    $('#for_read_tr_fee_paid_'+val).hide();
    $('#for_write_tr_fee_paid_'+val).show();

  }
    function Edit_fee_paid_row_back(val){
    $('#for_read_tr_fee_paid_'+val).show();
    $('#for_write_tr_fee_paid_'+val).hide();

  }
</script>