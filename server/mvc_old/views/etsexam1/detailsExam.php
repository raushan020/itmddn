<script>
   $(document).ready(function() {
       function disableBack() { window.history.forward() }

       window.onload = disableBack();
       window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
   });
</script>
 <style type="text/css">
 #page-wrapper{
 	margin-top: 25px;
 }
 .container{
	box-shadow: 0px 1px 10px 3px #fff;
}
</style>

<script>
$(document).ready(function(){
    $("#hide").click(function(){
        $("p").hide();
    });
    $("#show").click(function(){
        $("p").show();
    });
});
</script>

<!-- for f12 -->
<script>
$(document).ready(function(){
$("body").bind("contextmenu", function(e) {
    e.preventDefault();
});
document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            return false;
        }
    }
document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            return false;
        }
    }
});
</script>

<!-- for right click disable -->
<script>
	 $(document).ready(function(){
 	$(document).bind("contextmenu",function(e){
   		return false;
  	});
 });

</script>

 <!-- for ctrl disable -->

<script>
jQuery(document).ready(function($){
    $(document).keydown(function(event) { 
        var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
        
        if (event.ctrlKey && (pressedKey == "c" || pressedKey == "u")) {
            return false; 
        }
    });
});
</script>

<script>

$(function(){   
    $(document).keydown(function(objEvent) {        
        if (objEvent.ctrlKey) {          
            if (objEvent.keyCode == 65) {                         

                return false;
            }            
        }        
    });
});  
</script> 


 <div class="">


<p><strong style="margin-left: 54px; color: #67757c;"><?php echo $quiz['quiz_name'];?></strong></p>

<div class="container-fluid" onkeypress="return disableCtrlKeyCombination(event);" onkeydown="return disableCtrlKeyCombination(event);">
      <div class="row">
         
            <div class="card">
            <div class="card-body"> 
    <!-- instraction start -->
<div class="">  
	<!-- <div class="container"> -->
	<div class="row page-titles" style="padding: 2px;">
	    <div class="col-md-6 align-self-left">
	        <p class="text-themecolor insfont-size">Instructions</p>
	    </div>

	    <div class="col-md-6 align-self-right">
	        <p class="text-themecolor insfont-size readCarefully">Please Read The Instructions Carefully:</p>
	    </div>
	</div>

<div class="container-fluid instraction-bg">
<h4 class="h-size">General Instructions:</h4><br>

<p class="instfont" style="margin-top: -23px;"><strong style="color: #67757c;">Please Read The Instructions Carefully:</p></strong> 

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>A Candidate Who Breaches Any Of Examination Regulations Will be liable to Disciplinary Action Including (but not limited to) Suspention or Expulsion From The University</p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>Kindly refresh your browser at the beginning of exam.</p>

<p class="instfont"style="color: #67757c;"><i class="fa fa-hand-o-right icons-style"></i><strong style="color: #67757c;">Please carry your Identity Card and Admit Card at the time of Examination.</strong></p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>Each exam contains questions with 4 options. Select only one of the options as your answer.</p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>Candidates must complete their online exams within 1:30 hours. No additional time would be provided.</p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>To change any answer, simply click the desired option button.</p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>There will be three buttons on each screen, the use of buttons is given below: </p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i>Review question: Answer the question and mark it for review at later stage. Submit & Next: Save the selected answer and proceed to next question. Finish Exam Now: Exit from exam</p>

<p class="instfont"><i class="fa fa-hand-o-right icons-style"></i><strong style="color: #67757c;">Do Not CLICK on the 'Finish Exam Now' unless you have completed the exam. In case you click 'Finish Exam Now ' button, you will not be permitted to restart the exam.</strong></p>	
	

</div>
</div>
</div>

 <!-- instraction end -->


 <!-- button start -->

 <div class="container_summary1">
		<div class="new-summary">
				<!-- <span class="btn btn-info"></span>Answered -->
				<span class="btn btn-info btn_examsummary btn-answered" style="background: #0fb76b;border: 0;"></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">You have answered the question.</strong>
		</div>
<div class="new-summary">
		<span class="btn btn-info btn_examsummary btn-marked"><i class="fa fa-bookmark-o" style="margin-top: 5px;"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">You have answered the question but have marked the question for review.</strong>
</div>

<div class="new-summary">
		<span class="btn btn-info btn_examsummary btn-notanswered" style="background: #da6c6c; border: 0;"></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">You have not answered the question.</strong>
	</div>						

<div class="new-summary">
		<span class="btn btn-info btn_examsummary  btn-notvisited" style="background: #999;border: 0;"><i class="fa fa-eye-slash" style="margin-top: 5px;"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong style="color: #67757c;">You have not Visited the question yet.</strong>
	</div>
</div>
	<!-- button end-->

	<!-- cheak box start -->
	<div class="col-md-12">
		<div class="check-style1 col-md-1">
			<label><input class="raushan checkbox" id="show" onchange="TermAndCondition()" type="checkbox"><label>
		</div>
		<div class="col-md-9 text-align">
			The computer provide me is in proper working condition. I have read and understand the instructions given below:
		</div><br><br>

		<div class="col-md-3 term22">

		<p style="color:red;" id = "hide_termsAndCondText">
			Please accept term And conditions
		</p>
		</div>
	</div>
	<!-- cheak box end -->
<script type="text/javascript">
	function TermAndCondition(){
		document.getElementById('start_button'). style.display= "none";
	if (document.getElementById('show').checked) {
		document.getElementById('hide_termsAndCondText').style.display= "none";
		document.getElementById('start_button').style.display= "block";
}
			

		}
	</script>


<!-- /.box-header -->
  
<div class="row">
<form method="post" id="quiz_detail" action="<?php echo site_url('etsexam/check_exam/'.$quiz['quid']);?>">	
			<?php 
		if($this->session->flashdata('message')){
			echo $this->session->flashdata('message');	
		}
		?>	

	<?php 

		$a = $quiz['end_date'];
		$b = $quiz['start_date'];
		$diff = $a - $b;
		$hours = floor($diff / (60 * 60));
		
		$min = $diff - $hours * (60 * 60);
		$duration_date = floor( $min / 60 );
		
	 ?>

 <?php 

if(1){
if($quiz['camera_req']==1 && $this->config->item('webcam')==true){
?>
<!--<div style="color:#ff0000;"><?php echo $this->lang->line('camera_instructions');?></div>
<div id="my_photo" style="width:500px;height:500px;background:#212121;padding:2px;border:1px solid #666666;color:red"></div>
<br><br>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamjs/webcam.js"></script>
	<script language="JavaScript">
		Webcam.set({
			width: 500,
			height: 500,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
		Webcam.attach( '#my_photo' );

		
		 function take_snapshot() {
		     Webcam.snap( function(data_uri) {
                document.getElementById('my_photo').innerHTML = '<img src="'+data_uri+'"/>';
            } );
        }
		
		function upload_photo(){
		Webcam.snap( function(data_uri) {

    Webcam.upload( data_uri, '<?php echo site_url('quiz/upload_photo');?>',function(code, text) {
        // Upload complete!
        // 'code' will be the HTTP response code from the server, e.g. 200
        // 'text' will be the raw response content
	 document.getElementById('quiz_detail').submit();
    });
	});
	
	}
	
	 function capturephoto(){
		 
		void(take_snapshot());upload_photo(); 
	 }
	</script> -->
	
	<button class="btn btn-success" type="button" onClick="javascript:capturephoto();"><?php echo $this->lang->line('capture_start_quiz');?></button>

<?php 
}else{
?>	
	<center><button class="btn btn-success start-button" type="submit" id="start_button" style="display: none;">Start Now</button></center>
 
 <?php 
}
}else{
	if($quiz['with_login']==0){ 
	?>
	
	<button class="btn btn-success" type="submit"><?php echo $this->lang->line('start_quiz');?></button>
 &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo site_url('quiz/open_quiz/0');?>" ><?php echo $this->lang->line('back');?></a>

	
	<?php 
	}else{
?>
<div class="alert alert-danger"><?php echo str_replace('{base_url}',base_url(),$this->lang->line('login_required'));?></div>
&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo site_url('quiz/open_quiz/0');?>" ><?php echo $this->lang->line('back');?></a>
<?php
	} 
}
?>
</div>
</div>
 
 </div>
    </form>
</div>
</div>
</div>
</div>
</div>
 
 </div>


<div  id="warning_div" style="padding:10px; position:fixed;z-index:100;display:none;width:100%;border-radius:5px;height:200px; border:1px solid #dddddd;left:4px;top:70px;background:#ffffff;">
<center><b> <?php echo $this->lang->line('to_which_position');?></b><br><input type="text" style="width:30px" id="qposition" value=""><br><br>
<a href="javascript:cancelmove();"class="btn btn-danger" style="cursor:pointer;">Cancel</a> &nbsp; &nbsp; &nbsp; &nbsp;
<a href="javascript:movequestion();"class="btn btn-info" style="cursor:pointer;">Move</a>

</center>
</div>

<style type="text/css">
	.panel-body {
    margin-top: -200px;
}
</style>

