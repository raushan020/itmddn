

<div class="">

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="fa icon-teacher"></i> <?=$this->lang->line('panel_title')?> </h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">

                            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                            <li><a href="<?=base_url("teacher/index")?>"><?=$this->lang->line('menu_teacher')?></a></li>

                            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_teacher')?></li>
                        </ol>
                    </div>
            </div>


    <!-- form start -->
 <div class="container-fluid">
        <div class="row">
       <div class="card">
            <div class="card-body">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post" autocomplete="off" name="surveyform" onsubmit=" return validateform()" enctype="multipart/form-data">
                <p>Field are required with <span class="red-color">*</span></p>
                <h3 class="border_heading">Personal Information/Sign up</h3>
                                    <?php 

                        if(form_error('organization_name')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="organization_name" class="col-sm-3 control-label">

                            <?=$this->lang->line("organization_name")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="organization_name" onchange="AddUsernamePass()" name="organization_name" value="<?=set_value('organization_name',$teacher->organization_name)?>" >

                        </div>

                        <span class="col-sm-3 control-label">

                            <?php echo form_error('organization_name'); ?>

                        </span>

                    </div>
                    <?php 

                        if(form_error('name')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="name_id" class="col-sm-3 control-label">

                            <?=$this->lang->line("teacher_name")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="name_id" onchange="AddUsernamePass()" name="name" value="<?=set_value('name',$teacher->name)?>" >

                        </div>

                        <span class="col-sm-3 control-label">

                            <?php echo form_error('name'); ?>

                        </span>

                    </div>

                    <?php 

                        if(form_error('sex')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sex" class="col-sm-3 control-label">

                            <?=$this->lang->line("teacher_sex")?>

                        </label>

                        <div class="col-sm-6">

                               <?php 

                                echo form_dropdown("sex", array($this->lang->line('teacher_sex_male') => $this->lang->line('teacher_sex_male'), $this->lang->line('teacher_sex_female') => $this->lang->line('teacher_sex_female')), set_value("sex", $teacher->sex), "id='sex' class='form-control'"); 

                            ?>

                        </div>

                        <span class="col-sm-3 control-label">

                            <?php echo form_error('sex'); ?>

                        </span>

                    </div>

                    <?php 

                        if(form_error('email')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-3 control-label">

                            <?=$this->lang->line("teacher_email")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email',$teacher->email)?>" >

                        </div>

                        <span class="col-sm-3 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>



                    <?php 

                        if(form_error('phone')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-3 control-label">

                            <?=$this->lang->line("phone")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone',$teacher->phone)?>" >

                        </div>

                        <span class="col-sm-3 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>

                    <?php 
                        if(form_error('std_add_status')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="std_add_status" class="col-sm-3 control-label">
                            Student Add Status
                        </label>
                        <div class="col-sm-6">
                               <?php 
                               $options = array(
                                            '1' => 'Activated',
                                            '0' => 'Deactivated',
                                            );
                                echo form_dropdown("std_add_status", $options, set_value("std_add_status", $teacher->std_add_status), "id='std_add_status' name='std_add_status' class='form-control'"); 
                            ?>
                        </div>
                        <span class="col-sm-3 control-label">
                            <?php echo form_error('std_add_status'); ?>
                        </span>
                    </div>

                  
<h3 class="border_heading">Blackboard Info:</h3>

                     <?php
                        if(form_error('spa'))
                            echo "<div class='form-group has-error' >";
                        else
                            echo "<div class='form-group' >";
                    ?>
                        <label for="spa" class="col-sm-3 control-label">
                    Super Academic Partner<span class="red-color">*</span>
                        </label>

                        <div class="col-sm-6">
                            
                                <!-- <?php if($student->gender == $key){ echo 'selected="selected"';} ?> -->
                                <select class="form-control" name="spa" id="spa">
                                <option value="0" <?php if($teacher->spa == 0){ echo 'selected="selected"';} ?>>General Academic Partner</option>
                                <?php

                                 if($Super_Academic_data){
                                $button_add = "";    
                            foreach ($Super_Academic_data as $key => $value) {
                                ?>
                            <!-- $sp_academic_data["$value->userID"] = $value->name; -->
                                <option value="<?php echo $value->userID;?>" <?php if($value->userID == $teacher->spa){ echo 'selected="selected"';} ?>><?php echo $value->name; ?></option>
                            <?php }
                        }else{
                            $sp_academic_data[""] = "There is no contact person "; 
                            $button_add = "<a href='".base_url()."user/add'>Add</a>";
                        }
                        ?>      
                            </select>
                        </div>
                            <span class="col-sm-3"><?php echo $button_add; ?></span>
                            <span class="col-sm-3 control-label">
                            <?php echo form_error('spa'); ?>
                            </span>
                    </div>


                    <?php

                        if(form_error('cpa'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="cpa" class="col-sm-3 control-label">

                    Contact Person (Academic)  <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <?php
                                 if($Academic_data){
                                     $button_add = ""; 
                            foreach ($Academic_data as $key => $value) {
                            $academic_data["$value->userID"] = $value->name;
                            }
                        }else{
                             $academic_data[""] = "There is no contact person "; 
                            $button_add = "<a href='".base_url()."user/add'>Add</a>";
                        }

                                echo form_dropdown("cpa",

                                $academic_data,

                                    set_value("cpa",$teacher->cpa),  "id='cpa' required class='form-control'"

                                );

                            ?>

                        </div>
<span class="col-sm-3"><?php echo $button_add; ?></span>
                        <span class="col-sm-3 control-label">

                            <?php echo form_error('cpa'); ?>

                        </span>

                    </div>




<?php

                        if(form_error('rm'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="rm" class="col-sm-3 control-label">

                    Relationship Manager <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <?php
                                if($Accountant_data){
                                    $button_add_aca = ""; 
                            foreach ($Accountant_data as $key => $value) {
                            $Accountant["$value->userID"] = $value->name;
                            }
                        }else{
                              $Accountant[""] = "There is no relationship manager";
                              $button_add_aca = "<a href='".base_url()."user/add'>Add</a>";
                            }

                                echo form_dropdown("rm",

                                $Accountant,

                                    set_value("rm",$teacher->rm), "id='rm' required class='form-control'"

                                );

                            ?>

                        </div>
<span class="col-sm-3"><?php echo $button_add_aca; ?></span>
                        <span class="col-sm-3 control-label">

                            <?php echo form_error('usertype'); ?>

                        </span>

                    </div>

                    <?php

                        if(form_error('pl'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="pl" class="col-sm-3 control-label">

                    Project Lead <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <?php
                            if($Project_lead_data){
                                 $button_add_pl = ""; 
                            foreach ($Project_lead_data as $key => $value) {
                            $Project_lead["$value->userID"] = $value->name;
                            }
                        }else{
                            $Project_lead[""] = "There is no project lead"; 
                           $button_add_pl = "<a href='".base_url()."user/add'>Add</a>";
                        }

                                echo form_dropdown("pl",

                                $Project_lead,

                                    set_value("pl",$teacher->pl), "id='pl' required class='form-control'"

                                );

                            ?>

                        </div>
<span class="col-sm-3"><?php echo $button_add_pl; ?></span>
                        <span class="col-sm-3 control-label">

                            <?php echo form_error('pl'); ?>

                        </span>

                    </div>

                         <h3  class="border_heading">Reset Password</h3>
                    <div class='form-group'>
                        <label for="new_password" class="col-sm-2 control-label">
                            New Password
                        </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control ccpassword" id="new_password" name="new_password" value="" >
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for="cpassword" class="col-sm-2 control-label">
                            Confirm Password
                        </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control ccpassword" id="cpassword" name="cpassword" value="" >&nbsp;<span id="pass_err"></span>
                        </div>
                    </div>

<h3 class="border_heading">Assign Course:</h3>

    <?php 
    foreach ($fee as $key => $value) {
        if (in_array($value->classesID,$classesID_array)) {
         $cheacked  = "checked";  
        }else{
         $cheacked  = "";    
        }
    ?>


  
                            <div class="col-sm-6"> 
                                <div class="dsd" style="    box-shadow: 0px 0px 2px 1px #bfbfbf;
    padding: 5px;">
                                  <label class="nexCheckbox"><?php echo $value->classes ?>
                                <input type="checkbox" name="classesID[]" <?php echo $cheacked ?> value="<?php echo $value->classesID ?>" id="select_all">
                                <span class="checkmark checkmark-action-layout"></span>
                            </label> 
                                <?php 
                                $ex =  explode(',',$value->fees);
                                $ids =  explode(',',$value->ids);
                                 ?>
                            <select class="form-control" name="feetypeID[]">
                                <option value="0">Select Fee</option>
                                <?php for ($i=0; $i <count($ex) ; $i++) {
    if (in_array($ids[$i],$feetypeID_array)) {
         $selected  = "selected";  
        }else{
         $selected  = "";    
        }  
  ?>
                                <option value="<?php echo $ids[$i]; ?>" <?php echo $selected ?>><?php echo $ex[$i]; ?></option>
                            <?php } ?>
                            </select> 
                            </div>
                            </div> 
                            

<?php } ?>
<div class="clearfix"></div>



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("add_teacher")?>" >

                        </div>

                    </div>



                </form>



            </div><!-- /col-sm-8 -->

        </div>

    </div>

</div>
</div>
</div>

<style type="text/css">
    .displayNone{
        display:none;
    }
</style>
<script type="text/javascript">
    function AddUsernamePass(){
    $('.displayNone').show();
    var name_id = $('#name_id').val();
    $.ajax({
    type:'post',
    url:"<?php echo base_url() ?>teacher/AddUsernamePass",
    data:{name_id:name_id},
    success:function(response) {
        var objective = jQuery.parseJSON(response);
        $('#username').val(objective.username);
        $('#password').val(objective.password);
        $('.displayNone').hide();
    
    }

});
    }
</script>
<script type="text/javascript">
    function validateform()
{
   
    var success = false;


for (i = 0; i < document.surveyform.elements['classesID[]'].length; i++)
{
    //if (document.surveyform.classesID[i].checked)
    if (document.surveyform.elements['classesID[]'][i].checked){
  
            success = true;
        }
    }

    return success;
}
</script>


<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

$('#dob').datepicker({ startView: 2 });

$('#jod').datepicker();

</script>
<script type="text/javascript">
    function find_classes_using_teacher(val){

     $.ajax({
    type:'post',

    url:"<?php echo base_url() ?>AjaxController/find_classes_using_teacher",

    data:{val:val},

    success:function(response) {
    $('#classesID').html(response);

    }



}); 
    }

    $('.ccpassword').focusout(function(){
        var pass = $('#new_password').val();
        var pass2 = $('#cpassword').val();
        if(pass != pass2){
            $("#pass_err").html("<p style='color:red;'>Password doesn't match</p>");
        }
        else
        {
            $("#pass_err").html("<p style='color:green;'></p>");
        }

    });
</script>

