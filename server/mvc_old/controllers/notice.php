<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Notice extends Admin_Controller {


	function __construct() {

		parent::__construct();

		$this->load->model("notice_m");
		$this->load->model("subject_m");
		$this->load->model("alert_m");
		$this->load->model("sub_courses");
		$this->load->model("student_m");
		$this->load->model("classes_m");
		$this->load->model("student_info_m");
		$this->load->model("teacher_m");
		$this->load->model("user_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('notice', $language);	

	}



	public function index() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");
		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
		if($usertype == 'ClgAdmin' || $usertype == 'Support' ) {

			$year = date("Y");

			$this->data['notices'] = $this->notice_m->get_order_by_notice(array('year' => $year));

			if ($this->input->post('classesID')) {
				$this->session->unset_userdata('FilterNoticesubCourseID');
				$this->session->unset_userdata('FilterNoticeyearSemesterID');
				$this->session->set_userdata('FilterNoticeclassesID', $this->input->post('classesID'));
			}
	        if ($this->input->post('subCourseID')) {
				$this->session->set_userdata('FilterNoticesubCourseID', $this->input->post('subCourseID'));
			}

		  	if ($this->input->post('yearSemesterID')) {
				$this->session->set_userdata('FilterNoticeyearSemesterID', $this->input->post('yearSemesterID'));
			}


			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterNoticeclassesID'));


			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterNoticeclassesID'));

			$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);

		} 
          elseif($usertype =="Super_A_P" || $usertype =="Teacher" || $usertype =="Accountant" || $usertype =="Academic" ){
          	$usertype = $this->session->userdata("usertype");
          		$loginuserID = $this->session->userdata("loginuserID");
/*print_r($loginuserID);
			 exit();*/
              $this->data['notice'] =$this->notice_m->user_notice_list($loginuserID);
             $this->data["subview"] = "notice/index";
			$this->load->view('_layout_main', $this->data);
			// 
          } 
		elseif ($usertype == "Student") {
			
			$student = $this->student_info_m->get_student_info();
			$this->data['notice'] = $this->notice_m->notice_list_all($student->studentID);			
				$this->data["subview"] = "notice/index";

			$this->load->view('_layout_main', $this->data);

		}else {

			$this->data["subview"] = "notice/add";

			$this->load->view('_layout_main', $this->data);

		}

	}



	protected function rules() {

		$rules = array(

				 array(

					'field' => 'classesID', 

					'label' => 'Course', 

					'rules' => 'trim|required|xss_clean|max_length[128]'

				),

				array(

					'field' => 'title', 

					'label' => $this->lang->line("notice_title"), 

					'rules' => 'trim|required|xss_clean|max_length[128]'

				), 

				array(

					'field' => 'date', 

					'label' => $this->lang->line("notice_date"),

					'rules' => 'trim|required|max_length[10]|xss_clean|callback_date_valid'

				),

				array(

					'field' => 'notice', 

					'label' => $this->lang->line("notice_notice"),

					'rules' => 'trim|required|xss_clean',


				)

			);

		return $rules;

	}



	public function add() 
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$year = date("Y");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin') 
		{
			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			$usertyp_from_form =  $this->input->post('usertype');

			if($_POST) 
			{

				// print_r(date("Y-m-d", strtotime($this->input->post("date"))));
				// exit();
				if(in_array("Admin",$usertyp_from_form))
				{
					if($this->input->post('all_admin')!='')
					{

						$object = $this->user_m->get_admin_all($adminID);
						// echo $this->db->last_query();
						// die();
					}
					else
					{
						// echo "hello";
						// die();
					}
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num
					);
					$noticeID = $this->notice_m->insert_notice($array);
					// print_r($object);die;
					foreach ($object as $key => $value) 
					{
						$userArray =  array(
							'userID'=>$value->userID,
							'noticeID'=>$noticeID,
							'usertype'=>$value->usertype,
						);
						$this->db->insert('notice_x_user',$userArray);
					}
				}

				else if(in_array("Accountant",$usertyp_from_form))
				{
					if($this->input->post('all_accountant')!='')
					{
						$object = $this->user_m->get_accountant_all($adminID);
					}
					else
					{
					}
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num
					);
					$noticeID = $this->notice_m->insert_notice($array);
					foreach ($object as $key => $value) {
						$userArray =  array(
							'userID'=>$value->userID,
							'noticeID'=>$noticeID,
							'usertype'=>$value->usertype,
						);
						$this->db->insert('notice_x_user',$userArray);
					}
				}

				else if(in_array("super_AP",$usertyp_from_form))
				{
					if($this->input->post('all_Super_ap')!='')
					{
						$object = $this->user_m->get_super_ap_all($adminID);
					}
					else
					{
					}
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num
					);
					$noticeID = $this->notice_m->insert_notice($array);
					foreach ($object as $key => $value) 
					{
						$userArray =  array(
							'userID'=>$value->userID,
							'noticeID'=>$noticeID,
							'usertype'=>$value->usertype,
						);
						$this->db->insert('notice_x_user',$userArray);
					}
				}
				else if(in_array("Teacher",$usertyp_from_form))
				{
					if($this->input->post('all_academic')!='')
					{
						$object = $this->teacher_m->get_teacher_all($adminID);
					}
					else
					{
					}
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num
					);
					$noticeID = $this->notice_m->insert_notice($array);
					foreach ($object as $key => $value) 
					{
						$userArray =  array(
							'userID'=>$value->teacherID,
							'noticeID'=>$noticeID,
							'usertype'=>$value->usertype,
						);
						$this->db->insert('notice_x_user',$userArray);
					}
				}
				
				if(in_array('Student',$usertyp_from_form))
				{
					// print_r($usertyp_from_form);
					// die();
					$rules = $this->rules();
					$this->form_validation->set_rules($rules);
					if ($this->form_validation->run() == FALSE) 
					{
						$this->data['form_validation'] = validation_errors();
						$this->data["subview"] = "notice/add";
						$this->load->view('_layout_main', $this->data);			
					} 
					else 
					{
						$classesID  = $this->input->post('classesID');
						$subCourseID  = $this->input->post('sub_coursesID');
						$yearSemesterID = $this->input->post('yearsOrSemester');
						$num = 1;
						$array = array(
							'adminID' => $adminID,
							'classesID' => $classesID, 
							'sub_coursesID' => $subCourseID,
							'yearsOrSemester' => $yearSemesterID,
							"title" => $this->input->post("title"),
							"notice" => $this->input->post("notice"),
							"notice_type" => $this->input->post("notice_type"),
							"year" => $year,
							"date" => date("Y-m-d", strtotime($this->input->post("date"))),
							"status" => $num
						);
						$noticeID = $this->notice_m->insert_notice($array);
						$array['notice_type'] = $this->input->post("notice_type");
						$array['noticeID'] = $noticeID;
						$this->notice_m->notice_x_student($array);				
					}
				}
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("notice/index"));
			} 
			else 
			{
				$this->data["subview"] = "notice/add";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function send_notice_email()
	{
		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$year = date("Y");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'superadmin') {

		$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

			if($_POST) {
				// print_r(date("Y-m-d", strtotime($this->input->post("date"))));
				// exit();

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) {

					$this->data['form_validation'] = validation_errors();

					$this->data["subview"] = "notice/add";

					$this->load->view('_layout_main', $this->data);			

				} else {
					
					$classesID  = $this->input->post('classesID');
					$subCourseID  = $this->input->post('sub_coursesID');
					$yearSemesterID = $this->input->post('yearsOrSemester');
					$num = 1;
					$array = array(
						'adminID' => $adminID,
						'classesID' => $classesID, 
						'sub_coursesID' => $subCourseID,
						'yearsOrSemester' => $yearSemesterID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num

					);

				
					$noticeID = $this->notice_m->insert_notice($array);

					$array['notice_type'] = $this->input->post("notice_type");

					$array['noticeID'] = $noticeID;

					           $this->notice_m->notice_x_email_student($array);
					           foreach ($object as $name => $address)
								{
								        $this->email->clear();

								        $this->email->to($address);
								        $this->email->from('your@example.com');
								        $this->email->subject('Here is your info '.$name);
								        $this->email->message('Hi '.$name.' Here is the info you requested.');
								        $this->email->send();
								}

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));


					redirect(base_url("notice/index"));

				}

			} else {

				$this->data["subview"] = "notice/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}
	}

	
	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->notice_m->get_join_notice_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->notice_m->make_datatables($adminID);

            // $totalFiltered  = $this->student_m->get_filtered_data();
        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['title'] = $post->title;
				$nestedData['date'] = date('d-m-Y',strtotime($post->noticeDate));
                $nestedData['classesID'] = $post->classes;
                $nestedData['sub_coursesID'] = $post->sub_course;
                $nestedData['yearsOrSemester'] = $post->yearsOrSemester;
                $nestedData['notice'] = substr($post->notice,0,20).'...';

   			if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {
   		    $buttons     = btn_view('notice/view/'.$post->noticeID, $this->lang->line('view')).btn_edit('notice/edit/'.$post->noticeID, $this->lang->line('edit')).btn_delete('notice/delete/'.$post->noticeID, $this->lang->line('delete'));
 		

			    $nestedData['action'] = $buttons;
			}
              elseif($usertype == "Support" || $usertype == "Academic" || $usertype == "Teacher" || $usertype == "Super_A_P" || $usertype == "Accountant"  ) {
   		    $buttons     = btn_view('notice/view/'.$post->noticeID, $this->lang->line('view'));
 		

			    $nestedData['action'] = $buttons;
			}

                $data[] = $nestedData;

            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );

      
            
        echo json_encode($json_data); 
}


	public function edit() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Admin" ||  $usertype == 'ClgAdmin') {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
	       $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			if((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {
				$this->data['get_sub_courses'] = $this->student_m->get_sub_courses($this->data['notice']->classesID);
				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data['notice']->classesID);
			
					if($_POST) {

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) {

							$this->data["subview"] = "notice/edit";

							$this->load->view('_layout_main', $this->data);			

						} else { 	


					$classesID  = $this->input->post('classesID');
					$subCourseID  = $this->input->post('sub_coursesID');
					$yearSemesterID = $this->input->post('yearsOrSemester');
						$num = 1;
							$year = date("Y");
						$array = array(
						'adminID' => $adminID,
						'classesID' => $classesID, 
						'sub_coursesID' => $subCourseID,
						'yearsOrSemester' => $yearSemesterID,
						"title" => $this->input->post("title"),
						"notice" => $this->input->post("notice"),
						"notice_type" => $this->input->post("notice_type"),
						"year" => $year,
						"date" => date("Y-m-d", strtotime($this->input->post("date"))),
						"status" => $num

							);

							$this->notice_m->update_notice($array, $id);

					$array['notice_type'] = $this->input->post("notice_type");

					$array['noticeID'] = $id;

							$this->notice_m->notice_x_student_update($array);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("notice/index"));

						}

					} else {

						$this->data["subview"] = "notice/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}




	public function view() {
	
$usertype	=$this->session->userdata("usertype");
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {


			if ($usertype=='ClgAdmin' || $usertype =="Support" || $usertype =="Academic" || $usertype =="Teacher" ||$usertype =="Super_A_P"  ||$usertype =="Academic" ||$usertype =="Accountant") {
			$this->data['notice'] = $this->notice_m->get_notice($id);
			if($this->data['notice']) {

			$this->data["subview"] = "notice/view";

			$this->load->view('_layout_main', $this->data);

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}
}elseif($this->session->userdata("usertype")=='Student'){
$this->data['notice'] = $this->notice_m->notice_single($this->session->userdata('loginuserID'),$id);
			if($this->data['notice']) {

				$this->notice_m->update_notice_x_student_read($this->session->userdata('loginuserID'),$id);

			$this->data["subview"] = "notice/view";

			$this->load->view('_layout_main', $this->data);
			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}
}


		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}	

	}

	function hide_Notice_icon(){		
$data = array(
'iconStatus'=>0
);
	$this->db->where('studentID',$this->session->userdata('loginuserID'));
	$this->db->update('notice_x_student',$data);
}

	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "ClgAdmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$this->notice_m->delete_notice($id);
				$this->notice_m->delete_notice_x_student($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("notice/index"));

			} else {

				redirect(base_url("notice/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}



	function date_valid($date) {

		if(strlen($date) <10) {

			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     	return FALSE;

		} else {

	   		$arr = explode("-", $date);   

	        $dd = $arr[0];            

	        $mm = $arr[1];              

	        $yyyy = $arr[2];

	      	if(checkdate($mm, $dd, $yyyy)) {

	      		return TRUE;

	      	} else {

	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");

	     		return FALSE;

	      	}

	    } 

	} 



	public function print_preview() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			$this->data['notice'] = $this->notice_m->get_notice($id);

			if($this->data['notice']) {



				$this->load->library('html2pdf');

			    $this->html2pdf->folder('./assets/pdfs/');

			    $this->html2pdf->filename('Report.pdf');

			    $this->html2pdf->paper('a4', 'portrait');

			    $this->data['panel_title'] = $this->lang->line('panel_title');

				$html = $this->load->view('notice/print_preview', $this->data, true);

				$this->html2pdf->html($html);

				$this->html2pdf->create();

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}	

	}


function ResetCourses(){

$this->session->unset_userdata('FilterNoticeclassesID');
$this->session->unset_userdata('FilterNoticesubCourseID');
$this->session->unset_userdata('FilterNoticeyearSemesterID');	   
}
function ResetSubcourses(){
$this->session->unset_userdata('FilterNoticesubCourseID');
}

function ResetSemesterYear(){

$this->session->unset_userdata('FilterNoticeyearSemesterID');
	   
}
function ResetAllfilter(){

$this->session->unset_userdata('FilterNoticesubCourseID');
$this->session->unset_userdata('FilterNoticeclassesID');
$this->session->unset_userdata('FilterNoticeyearSemesterID');  
}



	public function send_mail() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" ||  $usertype == 'ClgAdmin') {

			$id = $this->input->post('id');

			if ((int)$id) {

				$this->data['notice'] = $this->notice_m->get_notice($id);

				if($this->data['notice']) {



					$this->load->library('html2pdf');

				    $this->html2pdf->folder('uploads/report');

				    $this->html2pdf->filename('Report.pdf');

				    $this->html2pdf->paper('a4', 'portrait');

				    $this->data['panel_title'] = $this->lang->line('panel_title');

					$html = $this->load->view('notice/print_preview', $this->data, true);

					$this->html2pdf->html($html);

					$this->html2pdf->create('save');

					

					if($path = $this->html2pdf->create('save')) {

					$this->load->library('email');

					$this->email->set_mailtype("html");

					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

					$this->email->to($this->input->post('to'));

					$this->email->subject($this->input->post('subject'));

					$this->email->message($this->input->post('message'));	

					$this->email->attach($path);

						if($this->email->send()) {

							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

						} else {

							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

						}

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

}



/* End of file notice.php 

/* Location: .//D/xampp/htdocs/school/mvc/controllers/notice.php */