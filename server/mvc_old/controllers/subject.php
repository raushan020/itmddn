<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Subject extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_info_m");

		$this->load->model("parentes_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("teacher_m");

		$this->load->model("student_m");

		$this->load->model("department_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('subject', $language);

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");
 $SpecialUsertype = $this->session->userdata('SpecialUsertype');
		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Academic" || $usertype == "Super_A_P" || $SpecialUsertype=='Btech') {

	     	$this->data['all_count']  =  $this->subject_m->all_count();

			$this->data['ActiveSubjects_count']  =  $this->subject_m->ActiveSubjects_count();

			$this->data['DraftSubjects_count']  =  $this->subject_m->DraftSubjects_count();

			$this->data['TrashSubjects_count']  =  $this->subject_m->TrashSubjects_count();

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				if($SpecialUsertype=='Btech'){
				$this->db->where('departmentID',6);
				$this->data['classes'] =  $this->db->get('classes')->result();
				
				}else{
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			    }

			if ($this->input->post('classesID')) {

				$this->session->unset_userdata('FilterSubjectsubCourseID');

				$this->session->unset_userdata('FilterSubjectyearSemesterID');

				$this->session->set_userdata('FilterSubjectclassesID', $this->input->post('classesID'));

			}

	        if ($this->input->post('subCourseID')) {

				$this->session->set_userdata('FilterSubjectsubCourseID', $this->input->post('subCourseID'));

			}

		  	if ($this->input->post('yearSemesterID')) {

				$this->session->set_userdata('FilterSubjectyearSemesterID', $this->input->post('yearSemesterID'));

			}

			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));			

			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));

            $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	

			$this->data["subview"] = "subject/search";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$student = $this->student_info_m->get_student_info();

			$this->data['subjects'] = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID);

			$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);

		if ($this->input->post('yearSemesterID')) {	

			if ($this->input->post('yearSemesterID')=='Select') {

			$this->session->unset_userdata('FilterSubjectyearSemesterID');

			}else{

			$this->session->set_userdata('FilterSubjectyearSemesterID', $this->input->post('yearSemesterID'));

		  	}

		}


			$this->data["subview"] = "subject/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					$classesID = $checkstudent->classesID;

					$this->data['set'] = $id;

					$this->data['subjects'] = $this->subject_m->get_join_subject($classesID);

					$this->data["subview"] = "subject/index_parent";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "subject/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


public function AjaxTable(){

$usertype = $this->session->userdata("usertype");

$adminID = $this->session->userdata("adminID");

$loginuserID = $this->session->userdata("loginuserID");

$totalData = $this->subject_m->get_join_subject_count($adminID);

$SpecialUsertype = $this->session->userdata('SpecialUsertype');

$totalFiltered = $totalData;

$posts = $this->subject_m->make_datatables($adminID);

            // $totalFiltered  = $this->student_m->get_filtered_data();



        $data = array();

        if(!empty($posts))

        {

        	$i = 1;

            foreach ($posts as $post)

            {

 if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $SpecialUsertype =='Btech') {

				$nestedData['check'] ="<label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->subjectID." '/>

                            <span class='checkmark checkmark-action-layout'></span>

                        </label>

			</td>";

	}	


			    $nestedData['sn2'] = '';

				$nestedData['subject_code'] = $post->subject_code;

				$nestedData['subject'] = $post->subject;

                $nestedData['classes'] = $post->classes;

                $nestedData['sub_course'] = $post->sub_course;

                $nestedData['yearsOrSemester'] = $post->yearsOrSemester;



  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Academic" || $SpecialUsertype=='Btech') {


// fdsfdsfd
if ($post->studentStatus!=2) {

   $buttons     =  btn_delete('subject/delete/'.$post->subjectID, $this->lang->line('delete')).btn_add('syllabus/add_syllabus/'.$post->subjectID.'?type=unit', $this->lang->line('add'));

}else{

 $buttons = "<a class = 'appClassDelete' href = ".base_url()."subject/restore/".$post->subjectID.">Restore</a><a class = 'appClassDelete' href = 'subject/restore/'".$post->subjectID.">Delete</a>";

  }

if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $SpecialUsertype=='Btech') {
   $nestedData['action'] = $buttons;
}
 
}

if($usertype == "Academic" )
{
	
	$buttons     = btn_add('syllabus/add_syllabus/'.$post->subjectID.'?type=unit', $this->lang->line('add'));
	 $nestedData['action'] = $buttons;
	
}
               

                $data[] = $nestedData;



            }

        }



       $json_data = array(

                    "draw"            => intval($this->input->post('draw')),  

                    "recordsTotal"    => $totalData,  

                    "recordsFiltered" => $totalFiltered, 

                    "data"            => $data   

                    );

            

        echo json_encode($json_data); 

}







	protected function rules() {



		$rules = array(



				array(



					'field' => 'classesID', 



					'label' => $this->lang->line("subject_class_name"), 



					'rules' => 'trim|numeric|required|xss_clean|max_length[11]|callback_allclasses'



				),



				array(



					'field' => 'teacherID', 



					'label' => $this->lang->line("subject_teacher_name"), 



					'rules' => 'trim|required|xss_clean|max_length[60]|callback_allteacher'



				),



				array(



					'field' => 'subject', 



					'label' => $this->lang->line("subject_name"), 



					'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_subject'



				), 



				array(



					'field' => 'subject_author', 



					'label' => $this->lang->line("subject_author"), 



					'rules' => 'trim|xss_clean|max_length[100]'



				), 



				array(



					'field' => 'subject_code', 



					'label' => $this->lang->line("subject_code"),



					'rules' => 'trim|required|max_length[20]|xss_clean|callback_unique_subject_code'



				),



			);



		return $rules;



	}







	public function add() {



		$usertype = $this->session->userdata("usertype");
        $SpecialUsertype = $this->session->userdata('SpecialUsertype');
		$adminID = $this->session->userdata('adminID');

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Academic" || $SpecialUsertype=='Btech') {

			if($SpecialUsertype=='Btech'){
					$this->db->where('departmentID',6);
				$this->data['classes'] =  $this->db->get('classes')->result();
				
				}else{
			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
			    }


			if($SpecialUsertype=='Btech'){
					$this->db->where('departmentID',6);
				$this->data['department'] =  $this->db->get('department')->result();
				
				}else{
                $this->data['department'] = $this->department_m->fetch_departments('department');
			    }

			

			$this->data['teachers'] = $this->teacher_m->get_teacher();



			if($_POST) {



				$rules = $this->rules();



				$this->form_validation->set_rules($rules);



				if ($this->form_validation->run() == FALSE) { 



					$this->data["subview"] = "subject/add";



					$this->load->view('_layout_main', $this->data);			



				} else {



					$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));



					$array = array(



						"classesID" => $this->input->post("classesID"),



						"teacherID" => $this->input->post("teacherID"),



						"subject" => $this->input->post("subject"),



						"subject_author" => $this->input->post("subject_author"),



						"subject_code" => $this->input->post("subject_code"),



						"teacher_name" => $teacher->name,



						"create_date" => date("Y-m-d h:i:s"),



						"modify_date" => date("Y-m-d h:i:s"),



						"create_userID" => $this->session->userdata('loginuserID'),



						"create_username" => $this->session->userdata('username'),



						"create_usertype" => $this->session->userdata('usertype')



					);



					$this->subject_m->insert_subject($array);



					$this->session->set_flashdata('success', $this->lang->line('menu_success'));



					redirect(base_url("subject/index"));



				}



			} else {



				$this->data["subview"] = "subject/add";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function edit() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));



			if((int)$id && (int)$url) {



				$this->data['classes'] = $this->subject_m->get_classes();



				$this->data['teachers'] = $this->teacher_m->get_teacher();



				$this->data['subject'] = $this->subject_m->get_subject($id);



				if($this->data['subject']) {



					$this->data['set'] = $url;



					if($_POST) {



						$rules = $this->rules();



						$this->form_validation->set_rules($rules);



						if ($this->form_validation->run() == FALSE) {



							$this->data['form_validation'] = validation_errors(); 



							$this->data["subview"] = "subject/edit";



							$this->load->view('_layout_main', $this->data);			



						} else {



							$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));



							$array = array(



								"classesID" => $this->input->post("classesID"),



								"teacherID" => $this->input->post("teacherID"),



								"subject" => $this->input->post("subject"),



								"subject_author" => $this->input->post("subject_author"),



								"subject_code" => $this->input->post("subject_code"),



								"teacher_name" => $teacher->name,



								"modify_date" => date("Y-m-d h:i:s")



							);



							$this->subject_m->update_subject($array, $id);



							$this->session->set_flashdata('success', $this->lang->line('menu_success'));



							redirect(base_url("subject/index/$url"));



						}



					} else {



						$this->data["subview"] = "subject/edit";



						$this->load->view('_layout_main', $this->data);



					}



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function delete() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));



			if((int)$id) {

			$data = array(

                'status'=>2

               );

				// $this->db->where('subjectID',$id);
				// $this->db->update('subject',$data);

	$this->db->query("UPDATE subject
	left JOIN epub ON subject.subjectID = epub.subjectID
	left JOIN pdf_lms ON subject.subjectID = pdf_lms.subjectID 
	left JOIN videos ON subject.subjectID = videos.subjectID 
	left JOIN ets_quiz ON subject.subjectID = ets_quiz.subjectID
	 set epub.status = 2 
,pdf_lms.status = 2, videos.status = 2, ets_quiz.status = 2 ,subject.status = 2 where subject.subjectID = $id");

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));



				redirect(base_url("subject/index"));



			} else {



				redirect(base_url("subject/index"));



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function unique_subject() {



		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if((int)$id) {



			$subject = $this->subject_m->get_order_by_subject(array("subject" => $this->input->post("subject"), "subjectID !=" => $id, "classesID" => $this->input->post("classesID")));



			if(count($subject)) {



				$this->form_validation->set_message("unique_subject", "%s already exists");



				return FALSE;



			}



			return TRUE;



		} else {



			$subject = $this->subject_m->get_order_by_subject(array("subject" => $this->input->post("subject"), "classesID" => $this->input->post("classesID"), "subject_code" => $this->input->post("subject_code")));







			if(count($subject)) {



				$this->form_validation->set_message("unique_subject", "%s already exists");



				return FALSE;



			}



			return TRUE;



		}	



	}







	public function unique_subject_code() {



		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if((int)$id) {



			$subject = $this->subject_m->get_order_by_subject(array("subject_code" => $this->input->post("subject_code"), "subjectID !=" => $id));



			if(count($subject)) {



				$this->form_validation->set_message("unique_subject_code", "%s already exists");



				return FALSE;



			}



			return TRUE;



		} else {



			$subject = $this->subject_m->get_order_by_subject(array("subject_code" => $this->input->post("subject_code")));







			if(count($subject)) {



				$this->form_validation->set_message("unique_subject_code", "%s already exists");



				return FALSE;



			}



			return TRUE;



		}	



	}







	public function subject_list() {



		$classID = $this->input->post('id');



		if((int)$classID) {



			$string = base_url("subject/index/$classID");



			echo $string;



		} else {



			redirect(base_url("subject/index"));



		}



	}







	public function student_list() {



		$studentID = $this->input->post('id');



		if((int)$studentID) {



			$string = base_url("subject/index/$studentID");



			echo $string;



		} else {



			redirect(base_url("subject/index"));



		}



	}







	public function restore() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if((int)$id) {

$data = array(

'status'=>1

);

				// $this->db->where('subjectID',$id);

				// $this->db->update('subject',$data);

		$this->db->query("UPDATE subject
	left JOIN epub ON subject.subjectID = epub.subjectID
	left JOIN pdf_lms ON subject.subjectID = pdf_lms.subjectID 
	left JOIN videos ON subject.subjectID = videos.subjectID 
	left JOIN ets_quiz ON subject.subjectID = ets_quiz.subjectID
	 set epub.status = 1 
,pdf_lms.status = 1, videos.status = 1, ets_quiz.status = 1 ,subject.status = 1 where subject.subjectID =$id");

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("subject/index"));



			} else {



				redirect(base_url("subject/index"));



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}



	function multipleAction(){

if ($this->input->post('Delete')) {

$checked_id =  $this->input->post('checked_id');

for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) { 

				$data = array(

                'status'=>2

               );

				// $this->db->where('subjectID',$checked_id[$i]);

				// $this->db->update('subject',$data);

					$this->db->query("UPDATE subject
	left JOIN epub ON subject.subjectID = epub.subjectID
	left JOIN pdf_lms ON subject.subjectID = pdf_lms.subjectID 
	left JOIN videos ON subject.subjectID = videos.subjectID 
	left JOIN ets_quiz ON subject.subjectID = ets_quiz.subjectID
	 set epub.status = 2 
,pdf_lms.status = 2, videos.status = 2, ets_quiz.status = 2 ,subject.status = 2 where subject.subjectID =$checked_id[$i]");




			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("subject/index"));

}



if ($this->input->post('Draft')) {

$checked_id =  $this->input->post('checked_id');

for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) { 

				$data = array(

                'status'=>0

               );

				// $this->db->where('subjectID',$checked_id[$i]);

				// $this->db->update('subject',$data);

								$this->db->query("UPDATE subject
	left JOIN epub ON subject.subjectID = epub.subjectID
	left JOIN pdf_lms ON subject.subjectID = pdf_lms.subjectID 
	left JOIN videos ON subject.subjectID = videos.subjectID 
	left JOIN ets_quiz ON subject.subjectID = ets_quiz.subjectID
	 set epub.status = 0 
,pdf_lms.status = 0, videos.status = 0, ets_quiz.status = 0 ,subject.status = 0 where subject.subjectID =$checked_id[$i]");



			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("subject/index"));

}

if ($this->input->post('Active')) {

$checked_id =  $this->input->post('checked_id');

for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) { 

				$data = array(

                'status'=>1

               );

				// $this->db->where('subjectID',$checked_id[$i]);

				// $this->db->update('subject',$data);


	$this->db->query("UPDATE subject
	left JOIN epub ON subject.subjectID = epub.subjectID
	left JOIN pdf_lms ON subject.subjectID = pdf_lms.subjectID 
	left JOIN videos ON subject.subjectID = videos.subjectID 
	left JOIN ets_quiz ON subject.subjectID = ets_quiz.subjectID
	 set epub.status = 1 
,pdf_lms.status = 1, videos.status = 1, ets_quiz.status = 1 ,subject.status = 1 where subject.subjectID =$checked_id[$i]");



			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("subject/index"));

}

	}





function ResetCourses(){



$this->session->unset_userdata('FilterSubjectclassesID');

$this->session->unset_userdata('FilterSubjectsubCourseID');	 

$this->session->unset_userdata('FilterSubjectyearSemesterID');    

}



function ResetSubcourses(){



$this->session->unset_userdata('FilterSubjectsubCourseID');

	   

}



function ResetSemesterYear(){



$this->session->unset_userdata('FilterSubjectyearSemesterID');

	   

}

function ResetAllfilter(){



$this->session->unset_userdata('FilterSubjectsubCourseID');

$this->session->unset_userdata('FilterSubjectclassesID');

$this->session->unset_userdata('FilterSubjectyearSemesterID');  

}



function ActiveSubjects(){

$this->session->unset_userdata('FilterSubjectsubCourseID');

$this->session->unset_userdata('FilterSubjectclassesID');

$this->session->unset_userdata('FilterSubjectyearSemesterID');

$this->session->unset_userdata('TrashSubjects');

$this->session->unset_userdata('DraftSubjects');

$this->session->unset_userdata('AllSubjects');

$this->session->set_userdata('ActiveSubjects',1);

}

function DraftSubjects(){

$this->session->unset_userdata('FilterSubjectsubCourseID');

$this->session->unset_userdata('FilterSubjectclassesID');

$this->session->unset_userdata('FilterSubjectyearSemesterID');

$this->session->unset_userdata('ActiveSubjects');

$this->session->unset_userdata('TrashSubjects');

$this->session->unset_userdata('AllSubjects');

$this->session->set_userdata('DraftSubjects',3);

}

function TrashSubjects(){

$this->session->unset_userdata('FilterSubjectsubCourseID');

$this->session->unset_userdata('FilterSubjectclassesID');

$this->session->unset_userdata('FilterSubjectyearSemesterID');

$this->session->unset_userdata('ActiveSubjects');

$this->session->unset_userdata('DraftSubjects');

$this->session->unset_userdata('AllSubjects');

$this->session->set_userdata('TrashSubjects',2);

}

function AllSubjects(){

$this->session->unset_userdata('FilterSubjectsubCourseID');

$this->session->unset_userdata('FilterSubjectclassesID');

$this->session->unset_userdata('FilterSubjectyearSemesterID');

$this->session->unset_userdata('ActiveSubjects');

$this->session->unset_userdata('DraftSubjects');

$this->session->unset_userdata('TrashSubjects');

$this->session->set_userdata('AllSubjects',4);

}



function allclasses() {



		if($this->input->post('classesID') == 0) {



			$this->form_validation->set_message("allclasses", "The %s field is required");



	     	return FALSE;



		}



		return TRUE;



	}







	public function allteacher() {



		if($this->input->post('teacherID') === '0') {







			$this->form_validation->set_message("allteacher", "The %s field is required");



	     	return FALSE;



		}



		return TRUE;



	}



}







/* End of file subject.php */



/* Location: .//D/xampp/htdocs/school/mvc/controllers/subject.php */