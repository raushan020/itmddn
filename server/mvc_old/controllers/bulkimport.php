<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulkimport extends Admin_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->load->model("teacher_m");
        $this->load->model("parentes_m");
        $this->load->model("student_m");
        $this->load->model("invoice_m");
        $this->load->model("payment_m");
        $this->load->model("subject_m");
        $this->load->model("user_m");
        $this->load->model("book_m");
        $this->load->model("admin_m");
        $this->load->model("exam_m");
        $this->lang->load('bulkimport', $language);
        $this->load->library('csvimport');
    }
    
    public function index()
    {
        
        
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Teacher" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support") {
            $this->data["adminData"] = $this->admin_m->get_systemadmin();
            $this->data["subview"]   = "bulkimport/index";
            $this->load->view('_layout_main', $this->data);
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }
    public function unique_email()
    {
        $this->db->select('email');
        $query          = $this->db->get('teacher');
        $teacher_emails = $query->result();
        $this->db->select('email');
        $query1     = $this->db->get('student');
        $std_emails = $query1->result();
        $this->db->select('email');
        $query2        = $this->db->get('parent');
        $parent_emails = $query2->result();
        $this->db->select('email');
        $query3      = $this->db->get('user');
        $user_emails = $query3->result();
        $this->db->select('email');
        $query4             = $this->db->get('systemadmin');
        $systemadmin_emails = $query4->result();
        $emails             = array_merge($teacher_emails, $std_emails, $parent_emails, $user_emails, $systemadmin_emails);
        $result             = array();
        foreach ($emails as $key => $value) {
            array_push($result, $value->email);
        }
        return $result;
    }
    public function unique_username()
    {
        $this->db->select('username');
        $query             = $this->db->get('teacher');
        $teacher_usernames = $query->result();
        $this->db->select('username');
        $query1        = $this->db->get('student');
        $std_usernames = $query1->result();
        $this->db->select('username');
        $query2           = $this->db->get('parent');
        $parent_usernames = $query2->result();
        $this->db->select('username');
        $query3         = $this->db->get('user');
        $user_usernames = $query3->result();
        $this->db->select('username');
        $query4                = $this->db->get('systemadmin');
        $systemadmin_usernames = $query4->result();
        $usernames             = array_merge($teacher_usernames, $std_usernames, $parent_usernames, $user_usernames, $systemadmin_usernames);
        $result                = array();
        foreach ($usernames as $key => $value) {
            array_push($result, $value->username);
        }
        return $result;
    }
  
    public function student_bulkimport()
    {
                
        $usertype = $this->session->userdata("usertype");
        $adminID  = $this->session->userdata("adminID");
        if ($usertype == "Admin" || $usertype == "Teacher" || $usertype == "superadmin" || $usertype == "ClgAdmin") {
            if (isset($_FILES["csvStudent"])) {
                // print_r($_FILES["csvStudent"]);
                // exit();
                $config['upload_path']   = "./uploads/csv/";
                $config['allowed_types'] = 'text/plain|text/csv|csv';
                $config['file_name']     = $_FILES["csvStudent"]['name'];
                $config['max_size']      = "2048000000";
                $config['overwrite']     = TRUE;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload("csvStudent")) {
                    $this->session->set_flashdata('error', $this->lang->line('import_error'));
                    redirect(base_url("bulkimport/index"));
                } else {
                    $file_data = $this->upload->data();
                    $file_path = './uploads/csv/' . $file_data['file_name'];
                    
                    if ($this->csvimport->get_array($file_path)) {
                        $csv_array = $this->csvimport->get_array($file_path);

                        foreach ($csv_array as $key => $row){
                          
                            $findsingleStudent = $this->student_m->get_student(array(
                                'username' => $row['Registration_ID']
                            ));

                            if(count($findsingleStudent)>0){

                                if (isset($row['Roll_number'])) {

                                    $insert_data['roll'] = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Roll_number']);

                               if($insert_data['roll']!=''){
                                          $insert_data['status'] = 1;
                                      
                                    }
                                                                     
                                } 
                                
                                if (isset($row['Roll_number'])) {
                                    $insert_data['roll'] = $row['Roll_number'];
                                }

                                if (isset($row['Name'])) {
                                    $insert_data['name']  = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Name']);
                                    
                                }

                                if (isset($row['FatherName'])) {

                                    $insert_data['father_name'] = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['FatherName']);
                               
                                }  

                                if (isset($row['MotherName'])) {
                                    $insert_data['mother_name'] =  preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['MotherName']);
                                     
                                } 

                                if (isset($row['Dob'])) {
                                    $slashToHypne     = str_replace('/', '-', $row['Dob']);
                                    $dobIndian        = date("d-m-Y", strtotime($row['Dob']));
                                    $dob = date("Y-m-d", strtotime($slashToHypne));
                                    $insert_data['dob'] = $dob;
                                    
                                } 

                                if (isset($row['Phone'])) {

                                    $phoneArray = explode('/', preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Phone']));
                                    $insert_data['phone']  = $phoneArray[0];
                                } else {
                                    $insert_data['phone'] = '';
                                } 

                                if (isset($row['Email'])) {
                                    $EmailArray = explode('/', $row['Email']);                                   
                                    $mainEmail  = $EmailArray[0];
                                                                   
                                      $insert_data['email'] = $mainEmail;
                                   
                                } 

                                if (isset($row['Pincode'])) {
                                    $insert_data['pin'] = $row['Pincode'];
                                }

                                if (isset($row['Pincode'])) {
                                    $insert_data['pin'] = $row['Pincode'];
                                }

                                if (isset($row['Address'])) {                                                                   
                                    $insert_data['address'] = $row['Address'];                                   
                                } 

                                if (isset($row['Course'])) {
                                    $ClassesRow = $this->getClass($row['Course'], $adminID); 
                                                                 
                                     $insert_data['classesID'] = $ClassesRow->classesID;
                                   
                                }  
                                if (isset($row['SubCourse'])) {
                                    if ($ClassesRow->IsSubCourse == 0) {
                                        $subCoursesID = 0;                                 
                                    } else {                                                                   
                                        if ($row['SubCourse'] == '') {    
                                            $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Subcourse code is empty');
                                            redirect(base_url("bulkimport/index"));                                            
                                        }else{
                                                $subCoursesID              = $this->getSubCourse($row['SubCourse'], $adminID);
                                                $subCoursecheckwithClasses = $this->subCoursecheckwithClasses($row['SubCourse'], $ClassesRow->classesID, $adminID);
                                            if ($subCoursecheckwithClasses==0){
                                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Subcourse code is mismatch');
                                                redirect(base_url("bulkimport/index"));
                                            }
                                        }
                                    }
                                                                 
                                    $insert_data['SubCourse'] = $subCoursesID;                                   
                                }

                                if (isset($row['year'])) {
                                    
                                    $ClassesRow = $this->getClass($row['Course'], $adminID); 
                                    if ($ClassesRow->mode==1) {
                                        $semester = '';   
                                        $sessionArray  = explode('-', $row['Session']);

                                        $startYear  = $sessionArray[0];
                                        $endYear  = $sessionArray[1];

                                        $StartYearobject = new DateTime($startYear.'-01-06');
                                        $endYearObject = new DateTime($endYear.'-01-06');

                                        $diff = $endYearObject->diff($StartYearobject);

                                        $duration = $diff->y;

                                        $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                                        $d2 = new DateTime('now');

                                        $diff2 = $d2->diff($d1);

                                        $findYears =  $diff2->y;

                                        if ($findYears>=$duration) {

                                            $insert_data['yearsOrSemester'] = CallYears($duration);

                                        }else{
                                            $findYearsAddOne = $findYears+1;
                                            $insert_data['yearsOrSemester'] = CallYears($findYearsAddOne);
                                        }
                                    }else{
                                        $year = '';
                                        $sessionArray  = explode('-', $row['Session']);
                                        $startYear  = $sessionArray[0];
                                        $endYear  = $sessionArray[1];
                                        $StartYearobject = new DateTime($startYear.'-01-06');
                                        $endYearObject = new DateTime($endYear.'-01-06');
                                        $diff = $endYearObject->diff($StartYearobject);
                                        $duration = $diff->y;
                                        $Durationsemester = $duration*2;
                                        $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                                        $d2 = new DateTime('now');
                                        $date1 = $startYear.'-06-01';
                                        $date2 = date("Y-m-d");
                                        $ts1 = strtotime($date1);
                                        $ts2 = strtotime($date2);
                                        $year1 = date('Y', $ts1);
                                        $year2 = date('Y', $ts2);
                                        $month1 = date('m', $ts1);
                                        $month2 = date('m', $ts2);
                                        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                                        $totalSemester =  floor($diff/6);
                                        $semesterRemainder = fmod($diff,6);
                                        if ($totalSemester>=$Durationsemester) {
                                            $insert_data['yearsOrSemester']  = CallSemester($Durationsemester);
                                        }else{
                                            if ($semesterRemainder) {
                                                $semeterAddone = $totalSemester+1;
                                                $insert_data['yearsOrSemester'] = CallSemester($semeterAddone);
                                            }else{
                                                $insert_data['yearsOrSemester'] = CallSemester($totalSemester);
                                            }
                                        }
                                    }                                        
                                }

                                
                               if (isset($row['Aadhar'])) {
                                    $insert_data['aadhar'] = $row['Aadhar'];
                               }
                                if (isset($row['student_status'])) {
                                    if (($row['student_status']=='fresher') || ($row['student_status']=='credit_transfer') || ($row['student_status']=='lateral') || ($row['student_status']=='fast_track')) {
                                       
                                   
                                        $newstydentstatus = strtolower($row['student_status']);
                                        $insert_data['student_status'] = $newstydentstatus;
                                    }else{
                                        echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Student status Should be fresher, credit_transfer, lateral, fast_track');
                                        redirect(base_url("bulkimport/index"));
                                    }
                                }

                                if (isset($row['Gender'])) {
                                    $insert_data['sex'] = $row['Gender'];        
                                }
                                    
                                if (isset($row['Session'])) {
                                    $insert_data['session'] = $row['Session'];        
                                }   

                                if(isset($insert_data)){

                                    $insetID = $this->student_m->update_student_byAdvancedCSV($insert_data,$findsingleStudent->studentID);
                                }else{

                                    $this->session->set_flashdata('error', 'fields did not match');

                                    redirect(base_url("bulkimport/index"));    
                                } 
// }
                                
                            }else{

                
                            if (!isset($row['Name'])) {
                               echo $this->session->set_flashdata('error', 'Oops Name Are Missing!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Registration_ID'])) {
                               echo $this->session->set_flashdata('error', 'Oops Registration id Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Roll_number'])) {
                               echo $this->session->set_flashdata('error', 'Oops Roll number Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['FatherName'])) {
                                echo $this->session->set_flashdata('error', 'Oops Father name Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['MotherName'])) {
                                echo $this->session->set_flashdata('error', 'Oops Mother Name Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Dob'])) {
                                echo $this->session->set_flashdata('error', 'Oops Date of Birth Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Gender'])) {
                                echo $this->session->set_flashdata('error', 'Oops Gender Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Address'])) {
                                echo $this->session->set_flashdata('error', 'Oops Address Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            // elseif (!isset($row['EmployementStatus'])) {
                            //     echo $this->session->set_flashdata('error', 'Oops Employment Status Are Mismatch!!! Please Try Again...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            elseif (!isset($row['Pincode'])) {
                                echo $this->session->set_flashdata('error', 'Oops Pincode Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Nationality'])) {
                                echo $this->session->set_flashdata('error', 'Oops Nationality Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Aadhar'])) {
                                echo $this->session->set_flashdata('error', 'Oops Aadhar Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Photo'])) {
                                echo $this->session->set_flashdata('error', 'Oops Photo Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            // elseif (!isset($row['Examtype'])) {
                            //     echo $this->session->set_flashdata('error', 'Oops Exam Type Are Mismatch!!! Please Try Again...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            elseif (!isset($row['Course'])) {
                                echo $this->session->set_flashdata('error', 'Oops Course Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                             elseif (!isset($row['year'])) {
                                echo $this->session->set_flashdata('error', 'Oops Year Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['student_status'])) {
                               echo $this->session->set_flashdata('error', 'Oops Student Status Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            // elseif (!isset($row['SubCourse'])) {
                            //     echo $this->session->set_flashdata('error', 'Oops Sub Course Are Mismatch!!! Please Try Again...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            elseif (!isset($row['Session'])) {
                                echo $this->session->set_flashdata('error', 'Oops Session Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            // elseif (!isset($row['SessionType'])) {
                            //     echo $this->session->set_flashdata('error', 'Oops Session Type Are Mismatch!!! Please Try Again...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            // elseif (!isset($row['Counselor'])) {
                            //     echo $this->session->set_flashdata('error', 'Oops Counseller Are Mismatch!!! Please Try Again...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            elseif (!isset($row['Phone'])) {
                                echo $this->session->set_flashdata('error', 'Oops Phone Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Email'])) {
                                echo $this->session->set_flashdata('error', 'Oops Email Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['TotalFees'])) {
                                echo $this->session->set_flashdata('error', 'Oops Total Fee Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['PaymentReceived'])) {
                                echo $this->session->set_flashdata('error', 'Oops Payment Recieved Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }

                              elseif (!isset($row['Feetype'])) {
                                echo $this->session->set_flashdata('error', 'Oops Fee Type Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }

                            elseif (!isset($row['Paymenttype'])) {
                               echo $this->session->set_flashdata('error', 'Oops Payment Type Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['Paymentdate'])) {
                               echo $this->session->set_flashdata('error', 'Oops Payment Date Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['10th'])) {
                                echo $this->session->set_flashdata('error', 'Oops 10th Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (!isset($row['12th'])) {
                               echo $this->session->set_flashdata('error', 'Oops 12th Are Mismatch!!! Please Try Again...');
                               redirect(base_url("bulkimport/index"));
                            }
                             elseif (!isset($row['Graduation'])) {
                                echo $this->session->set_flashdata('error', 'Oops Graduation Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                             elseif (!isset($row['PostGraduation'])) {
                                echo $this->session->set_flashdata('error', 'Oops Post Graduation Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }
                             elseif (!isset($row['CurrentStatus'])) {
                                echo $this->session->set_flashdata('error', 'Oops Current Status Are Mismatch!!! Please Try Again...');
                                redirect(base_url("bulkimport/index"));
                            }



                            if (!empty($row['Registration_ID'])) {                             
                            if (empty($row['Name'])) {
                             
                                echo $this->session->set_flashdata('error', $row['Registration_ID']."  ".'Oops Name is blank!!! Please Fill Name Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['Registration_ID'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Registration Id is blank!!! Please Fill Registration Id Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['Dob'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Date of birth is blank!!! Please Fill Date of birth Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['Course'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Course is blank!!! Please Fill Course Field...');
                                redirect(base_url("bulkimport/index"));
                            }
                            // elseif (empty($row['year'])) {
                            //     echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Year is blank!!! Please Fill Course Field...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            // elseif (empty($row['Gender'])) {
                            //     echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Gender is blank!!! Please Fill Gender Field...');
                            //     redirect(base_url("bulkimport/index"));
                            // }
                            elseif (empty($row['student_status'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Student Status is blank!!! Please Fill Student Status Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['Session'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Sesion is blank!!! Please Fill Sesion Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['SessionType'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Sesion Type is blank!!! Please Fill Sesion Type Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['Counselor'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Counselor is blank!!! Please Fill Counselor Field...');
                                redirect(base_url("bulkimport/index"));
                            }elseif (empty($row['TotalFees'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Total Fees is blank!!! Please Fill Total Fees Field...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (empty($row['Paymentdate'])) {
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Payment Date is blank!!! Please Fill Payment Date Field...');
                                redirect(base_url("bulkimport/index"));
                            }
                            elseif (empty($row['Paymenttype'])){
                                echo $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Oops Payment Type is blank!!! Please Fill Payment type Field...');
                                redirect(base_url("bulkimport/index"));
                            }
                            
                               

                   


                    if (($row['Paymenttype']!='cash') && ($row['Paymenttype']!='Cheque') && ($row['Paymenttype']!='Bank') && ($row['Paymenttype']!='Draft') && ($row['Paymenttype']!='PayuMoney') && ($row['Paymenttype']!='onlinePayment')){
                        $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Payment Type Should be Cash, Cheque, Bank, Draft, PayuMoney, onlinePayment');
                        redirect(base_url("bulkimport/index"));
                    }


                            $findsingleStudent = $this->student_m->get_student(array(
                                'registration_ID' => $row['Registration_ID']
                            ));
                            
                            
                            $ClassesRow = $this->getClass($row['Course'], $adminID);




                                if ($ClassesRow->mode==1) {
                                $semester = '';   
                                $sessionArray  = explode('-', $row['Session']);

                                $startYear  = $sessionArray[0];
                                $endYear  = $sessionArray[1];

                                $StartYearobject = new DateTime($startYear.'-01-06');
                                $endYearObject = new DateTime($endYear.'-01-06');

                                $diff = $endYearObject->diff($StartYearobject);

                                $duration = $diff->y;

                                $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                                $d2 = new DateTime('now');

                                $diff2 = $d2->diff($d1);

                                $findYears =  $diff2->y;

                                if ($findYears>=$duration) {

                                $year = CallYears($duration);

                                }else{
                                    $findYearsAddOne = $findYears+1;
                                    $year = CallYears($findYearsAddOne);

                                }

                                }


                                else{
                                $year = '';
                                $sessionArray  = explode('-', $row['Session']);

                                $startYear  = $sessionArray[0];
                                $endYear  = $sessionArray[1];

                                $StartYearobject = new DateTime($startYear.'-01-06');
                                $endYearObject = new DateTime($endYear.'-01-06');

                                $diff = $endYearObject->diff($StartYearobject);

                                $duration = $diff->y;
                                $Durationsemester = $duration*2;

                                $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                                $d2 = new DateTime('now');


                                $date1 = $startYear.'-06-01';
                                $date2 = date("Y-m-d");

                                $ts1 = strtotime($date1);
                                $ts2 = strtotime($date2);

                                $year1 = date('Y', $ts1);
                                $year2 = date('Y', $ts2);

                                $month1 = date('m', $ts1);
                                $month2 = date('m', $ts2);

                                $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

                                $totalSemester =  floor($diff/6);

                                $semesterRemainder = fmod($diff,6);
                                if ($totalSemester>=$Durationsemester) {
                                 $semester  = CallSemester($Durationsemester);
                                }else{
                                if ($semesterRemainder) {
                                 $semeterAddone = $totalSemester+1;
                                $semester       = CallSemester($semeterAddone);
                                }else{
                                  $semester = CallSemester($totalSemester);
                                }
                                }
                                }

                            if (empty($ClassesRow) || $ClassesRow == 'error') {
                                $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'course code is empty or blank');
                                redirect(base_url("bulkimport/index"));
                            }
                            if ($ClassesRow->IsSubCourse == 0) {
                                $subCoursesID = 0;
                         
                            } else {
                                                           
                                if ($row['SubCourse'] == '') {    
                                    $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Subcourse code is empty');
                                    redirect(base_url("bulkimport/index"));
                                    
                                }else{
                                        $subCoursesID              = $this->getSubCourse($row['SubCourse'], $adminID);

                                        $subCoursecheckwithClasses = $this->subCoursecheckwithClasses($row['SubCourse'], $ClassesRow->classesID, $adminID);



                                    if ($subCoursecheckwithClasses==0){
                                        $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Subcourse code is mismatch');
                                        redirect(base_url("bulkimport/index"));
                                    }
                                }
                            }
                            
                            $student_status  =  strtolower($row['student_status']);
                            
           
                            $explodeSession = explode('-', $row['Session']);

                        if (count($explodeSession)!=2) {
                              $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session format is wrong.  Please try again...');
                                redirect(base_url("bulkimport/index"));
                        }
                            for( $m = 2000; $m < 2025; $m++ ){
                             $yearsArray[]  =  $m;
                            }
     
                          
                            if (in_array($explodeSession[0], $yearsArray) == false) {
                                
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                               

                            }
                            if (in_array($explodeSession[1], $yearsArray) == false) {
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                            }


                            if (($row['SessionType']!='A') && ($row['SessionType']!='C') && ($row['SessionType']!='A-C')){
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Session Type is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                            }

                                $payment_recive =  $row['PaymentReceived']+$row['payment2']+$row['payment3'];
                            if ($row['TotalFees']<$payment_recive) {
                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Payment Recieved is mismatch Please try again');
                                redirect(base_url("bulkimport/index"));
                            }

                            $totalfeesnum = $row['TotalFees'];
                            // if (!is_numeric($totalfeesnum)) {
                            //     $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Total Fee not a Sufficient Value!!! Please try again');
                            //     redirect(base_url("bulkimport/index"));
                            // }


                            $paymentfeesnum = $payment_recive;
                            // if (!is_numeric($paymentfeesnum)) {
                            //     $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'Payment Fee not a Sufficient Value!!! Please try again');
                            //     redirect(base_url("bulkimport/index"));
                            // }


                            $counsellor = $this->getTeacher($row['Counselor']);
                            

                            if (count($counsellor)==0){

                                 $this->session->set_flashdata('error', $row['Registration_ID'] . "  ".'Counselor Code Should be Correct...');
                                    redirect(base_url("bulkimport/index"));
                                }



                            $intger = intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
                            // random(ish) 5 digit int
                            
                            
                            $usernamePlusName = $row['Registration_ID'];
                            $slashToHypne     = str_replace('/', '-', $row['Dob']);
                            $dobIndian        = date("d-m-Y", strtotime($row['Dob']));
                             $passwordPlusName = str_replace('-', '', $dobIndian);
                            // $passwordPlusName = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Enrollment']);
                            $dob = date("Y-m-d", strtotime($row['Dob']));
                            
                            if (!empty($row['Phone'])) {
                                $phoneArray = explode('/', preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Phone']));
                                $mainPhone  = $phoneArray[0];
                            } else {
                                $mainPhone = '';
                            }
                            if (!empty($row['Email'])) {
                                $EmailArray = explode('/', $row['Email']);
                                $mainEmail  = $EmailArray[0];
                            } else {
                                $mainEmail = '';
                            }
                            
                            
                                                        
                            
                            $newstydentstatus = strtolower($row['student_status']);
                            $afterName = ucwords($row['Name']);
                                       
                            $afterMotherName = ucwords($row['MotherName']);
                            $afterFatherName = ucwords($row['FatherName']);

                                if (count($findsingleStudent)>0) {
                                    $img_obserb = $findsingleStudent->photo;
                                }else{
                                    $img_obserb ='defualt.png';
                                }

                            $insert_data = array(
                                'adminID' => $adminID,
                                'name' => preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $afterName),
                                'mother_name' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $afterMotherName),
                                'father_name' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $afterFatherName),
                                'dob' => $dob,
                                'sex' => $row['Gender'],
                                'email' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $mainEmail),
                                'phone' => $mainPhone,
                                'address' => $row['Address'],
                                'aadhar' => $row['Aadhar'],
                                'nationality' => $row['Nationality'],
                                'street' => '',
                                'pin' => $row['Pincode'],

                                'education_mode' => $ClassesRow->mode,
                                'student_status' => $newstydentstatus,
                                'session' => $row['Session'],
                                'examType' => $row['Examtype'],
                                'sessionType' => $row['SessionType'],
                                'classesID' => $ClassesRow->classesID,
                                'sub_coursesID' => $subCoursesID,
                                'roll' => preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Roll_number']),
                                'enrollment' => preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', $row['Enrollment']),
                                'username' => $usernamePlusName,
                                'password' => $this->student_m->hash($passwordPlusName),
                                'usertype' => 'Student',
                                'yearsOrSemester' => $year,
                                'counsellor' => $counsellor,
                                'library' => 0,
                                'hostel' => 0,
                                'transport' => 0,
                                'photo' => $img_obserb,
                                'year' => date('Y'),
                                'extraPhone' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['Phone']),
                                'extraEmail' => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['Email']),
                                "create_date" => date("Y-m-d h:i:s"),
                                "modify_date" => date("Y-m-d h:i:s"),
                                "create_userID" => $this->session->userdata('loginuserID'),
                                "create_username" => $this->session->userdata('username'),
                                "create_usertype" => $this->session->userdata('usertype'),
                                "studentactive" => 1,
                                "comments" => $row['comments'],
                                "yosPosition" => $year,
                                'registration_ID' => $row['Registration_ID']
                            );
                            
                            // var_dump($insert_data);
                            $Education['tenth']          = explode(',', $row['10th']);
                            $Education['twelth']         = explode(',', $row['12th']);
                            $Education['Graduation']     = explode(',', $row['Graduation']);
                            $Education['PostGraduation'] = explode(',', $row['PostGraduation']);
                            $Education['Other']          = explode(',', $row['Other']);
                             
                if (($student_status=='fresher') || ($student_status=='credit_transfer') || ($student_status=='lateral') || ($student_status=='fast_track')){

                            if (count($findsingleStudent) > 0) {
                                $insetID = $this->student_m->update_student_byCSV($insert_data, $Education, $findsingleStudent->studentID);
                            } else {
                                $insetID = $this->student_m->insert_student_byCSV($insert_data, $Education);
                            }

                            }else{

                                $this->session->set_flashdata('error', $row['Registration_ID'] ."  ".'please check Status -> fresher, lateral, credit_transfer, fast_track');
                                                                    redirect(base_url("bulkimport/index"));

                            }                            
                            
                            $dueFee = $row['TotalFees'] - $payment_recive;
                            
                            if ($dueFee == 0) {
                                $feeStatus = 2;
                            }
                            if ($dueFee > 0 and $dueFee < $row['TotalFees']) {
                                $feeStatus = 1;
                            }
                            if ($dueFee == $row['TotalFees']) {
                                $feeStatus = 0;
                            }
                            
                            
                            $InsertDataInvoice = array(
                                "adminID" => $adminID,
                                "studentID" => $insetID,
                                "amount" => $row['TotalFees'],
                                "paidamount" => $payment_recive,
                                "usertype" => $usertype,
                                "status" => $feeStatus,
                                "paymenttype" => "Cash"
                            );

                            $UpdateDataInvoice = array(
                                "amount" => $row['TotalFees'],
                                "paidamount" => $payment_recive,
                                "usertype" => $usertype,
                                "status" => $feeStatus,
                                "paymenttype" => "Cash"
                            );
                            
                            if (count($findsingleStudent) > 0) {
                                $insertInvoice = $this->invoice_m->update_invoice_byCsv($UpdateDataInvoice, $findsingleStudent->studentID);
                            } else {
                                $insertInvoice = $this->invoice_m->insert_invoice($InsertDataInvoice);
                            }

                            $payDate =date('Y-m-d',strtotime($row['Paymentdate']));

                            $payDateM = date('m',strtotime($row['Paymentdate']));
                            $payDateY = date('Y',strtotime($row['Paymentdate']));

                            $payment_array = array(
                                
                                "invoiceID" => $insertInvoice,
                                
                                "adminID" => $adminID,
                                
                                "studentID" => $insetID,
                                
                                "paymentamount" => $payment_recive,
                                
                                "paymenttype" => $row['Paymenttype'],
                                
                                "paymentdate" => $payDate,
                                
                                "paymentmonth" =>  $payDateM,
                                
                                "paymentyear" =>$payDateY,
                                
                                "data_source" => '1'
                                
                            );

                 $Updatepayment_array = array(
                                "paymentamount" => $payment_recive,

                                "paymenttype" => $row['Paymenttype'],
                                
                                "paymentdate" => date('Y-m-d'),
                                
                                "paymentmonth" => date('M'),
                                
                                "paymentyear" => date('Y'),
                                
                                "data_source" => '1'
                                
                            );
                            // $this->payment_m->insert_payment($payment_array);
                            if (count($findsingleStudent) > 0) {
                                $this->payment_m->update_paymentCsv($Updatepayment_array, $findsingleStudent->studentID);
                            } else {
                                
                                $this->payment_m->insert_payment($payment_array);
                            }
                            
                        }
                     }
                    }
                        $this->session->set_flashdata('success', $this->lang->line('import_success'));
                        redirect(base_url("bulkimport/index"));
                    } else {
                    
                        $this->session->set_flashdata('error', $this->lang->line('import_error'));
                        redirect(base_url("bulkimport/index"));
                    }
                    
                }
            } else {
               
                $this->session->set_flashdata('error', $this->lang->line('import_error'));
                redirect(base_url("bulkimport/index"));
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('unauthorised'));
            redirect(base_url("bulkimport/index"));
        }
    }
    
    
    
    public function subject_bulkimport()
    {
        $adminID = $this->session->userdata('adminID');
        if (isset($_FILES["csvSubject"])) {
            
            $config['upload_path']   = "./uploads/csv/";
            $config['allowed_types'] = 'text/plain|text/csv|csv';
            $config['max_size']      = '2048';
            $config['file_name']     = $_FILES["csvSubject"]['name'];
            $config['overwrite']     = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("csvSubject")) {
                $this->session->set_flashdata('error', $this->lang->line('import_error'));
                redirect(base_url("bulkimport/index"));
            } else {
                $file_data = $this->upload->data();
                $file_path = './uploads/csv/' . $file_data['file_name'];
                
                if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);
                
                    foreach ($csv_array as $key => $row) {
                        if ($row['year']) {
                            
                            
                            if ($row['subjectcode'] != '') {
                                                          
                      
                      
                                $ClassesRow = $this->getClass($row['Courses'], $adminID);
                                if ($row['SubCourse'] == '') {
                                    $subCoursesID = 0;
                                } else {
                                    
                                    $subCoursesID = $this->getSubCourse($row['SubCourse'], $adminID);

                                }
     



 $subCoursecheckwithClasses = $this->subCoursecheckwithClasses($row['SubCourse'], $ClassesRow->classesID, $adminID);
                                
     
                                if ($ClassesRow->mode == 1) {
                                    
    
                                    $YorS = CallYears($row['year']);
                                } else {
                                    $YorS = CallSemester($row['year']);
                                }

                                

                                $insert_data = array(
                                    'adminID' => $adminID,
                                    'classesID' => $ClassesRow->classesID,
                                    'sub_coursesID' => $subCoursesID,
                                    'yearsOrSemester' => $YorS,
                                    'subject' => $row['Subjects'],
                                    'subject_code' => $row['subjectcode'],
                                    "create_date" => date("Y-m-d h:i:s"),
                                    "modify_date" => date("Y-m-d h:i:s"),
                                    "create_userID" => $this->session->userdata('loginuserID'),
                                    "create_username" => $this->session->userdata('username'),
                                    "create_usertype" => $this->session->userdata('usertype'),
                                    "subjectactive" => 1
                                );

                               


                                $this->subject_m->insert_subject($insert_data);
                            }
                        }
                    }
                    $this->session->set_flashdata('success', $this->lang->line('import_success'));
                    redirect(base_url("bulkimport/index"));
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('import_error'));
                    redirect(base_url("bulkimport/index"));
                }
                
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('import_error'));
            redirect(base_url("bulkimport/index"));
        }
    }
    
    
    
    public function subCoursecheckwithClasses($sub_coursesCorsesCode,$classesID,$adminID)
    {
        
        $this->db->where('adminID', $adminID);
        $this->db->where('classesID', $classesID);
        $this->db->where('subCourseCode', $sub_coursesCorsesCode);
        $query = $this->db->get('sub_courses');
        return $query->num_rows();
        
        
    }
    public function quiz_bulkimport()
    {
        $adminID = $this->session->userdata('adminID');
        
        if (isset($_FILES["csvQuestion"])) {
            $config['upload_path']   = "./uploads/csv/";
            $config['allowed_types'] = 'text/plain|text/csv|csv';
            $config['max_size']      = '2048';
            $config['file_name']     = $_FILES["csvQuestion"]['name'];
            $config['overwrite']     = TRUE;
           
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("csvQuestion")) {
                $this->session->set_flashdata('error', $this->lang->line('import_error'));
                redirect(base_url("bulkimport/index"));
            } else {
                $file_data = $this->upload->data();
                $file_path = './uploads/csv/' . $file_data['file_name'];
                
                if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);
                    if (isset($csv_array[0]['title']) && isset($csv_array[0]['QuestionOption']) && isset($csv_array[0]['YesOrNo'])) {
                        
                        // $otherdb = $this->load->database('otherdb', TRUE);
                        
                        $quid     = $this->exam_m->insert_quiz();
                        $noqStart = 1;
                        foreach ($csv_array as $key => $row) {
                            
                            $title          = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['title']);
                            $QuestionOption = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['QuestionOption']);
                            
                            if ($title == 'Multiple Choice Single Response') {

                                $this->session->unset_userdata('Match_the_Column');
                                $this->session->set_userdata('Multiple_Choice_Single_Answer', 'Multiple Choice Single Answer');
                            } elseif ($title == 'True / False') {
                                $this->session->set_userdata('Multiple_Choice_Single_Answer', 'Multiple Choice Single Answer');
                            } elseif ($title == 'Match the following') {
                                
                                $this->session->unset_userdata('Multiple_Choice_Single_Answer');
                                $this->session->set_userdata('Match_the_Column', 'Match the Column');
                            }
                            if ($this->session->userdata('Multiple_Choice_Single_Answer')) {
                                if ($row['title'] != 'Multiple Choice Single Response' && $title != 'Question' && $title != 'True / False') {
                                    if ($title != '') {
                                        $userdata = array(
                                            'question' => $title,
                                            'question_type' => 'Multiple Choice Single Answer'
                                        );
                                        $this->db->insert('ets_qbank', $userdata);
                                        $qid = $this->db->insert_id();
                                        $this->session->set_userdata('qid', $qid);

                                        if($this->input->post('ch')==1){
                                            if($row['YesOrNo'] != '') {
                                            $score = 1;
                                        } else {
                                            $score = 0;
                                        }
                                    }else{
                                        // remove code after  
                                        for ($i=0; $i < 4 ; $i++) { 
                                            $randi = rand(1,4);
                                            if ($i==3) {
                                                if ($randi==1) {
                                                    $score = 1; 
                                                }else{
                                                    $score = 0; 
                                                }
                                            }
                                        }
                                    }
                                        $userdata = array(
                                            'q_option' => $QuestionOption,
                                            'qid' => $qid,
                                            'score' => $score
                                        );

                        
                                        $this->db->insert('ets_options', $userdata);
                                        
                                        $this->db->where('quid', $quid);
                                        $query    = $this->db->get('ets_quiz');
                                        $quizData = $query->row();
                                        
                                        $qids = $quizData->qids;
                                        $noq  = $quizData->noq;
                                        if ($qids == '') {
                                            $updatedQid = $qid;
                                        } else {
                                            $updatedQid = $qids . ',' . $qid;
                                        }
                                        if ($noq == '') {
                                            $noqUpdate = $noqStart;
                                        } else {
                                            $noqUpdate += 1;
                                        }
                                        $arrayName = array(
                                            'qids' => $updatedQid,
                                            'noq' => $noqUpdate
                                        );
                                        $this->db->where('quid', $quid);
                                        $this->db->update('ets_quiz', $arrayName);
                                        
                                    } else {
                                        if($this->input->post('ch')==1){
                                        if ($row['YesOrNo'] != '') {
                                            $score = 1;
                                        } else {
                                            $score = 0;
                                        }
                                    }else{
                                        $this->db->where('qid',$this->session->userdata('qid'));
                                        $this->db->where('score',1);
                                      $chuh    =  $this->db->get('ets_options');

                                        $this->db->where('qid',$this->session->userdata('qid'));
                                       $Three   =  $this->db->get('ets_options');

                                    if ($chuh->num_rows()==1) {
                                       $score = 0;
                                    }else{
                                    for ($i=0; $i < 4 ; $i++) { 
                                         $randi = rand(1,4);
                                         if ($i==3) {
                                             if ($randi==1) {
                                               $score = 1; 
                                             }else{
                                                $score = 0; 
                                             }
                                         }
                                        }
                                        if ($Three->num_rows()==3) {
                                           if ($chuh->num_rows()==0) {
                                              $score = 1;
                                           }
                                        }                                        

                                    }
                               }
        
                                        $userdata = array(
                                            'q_option' => $QuestionOption,
                                            'qid' => $this->session->userdata('qid'),
                                            'score' => $score
                                        );
                                        $this->db->insert('ets_options', $userdata);                                        
                                        
                                    }
                                }
                                
                            } elseif ($this->session->userdata('Match_the_Column')) {
                                if ($title != 'Match the following') {

                                    
                                    if ($title == 'Question') {
                                        $userdata = array(
                                            'question' => 'Match the following',
                                            'question_type' => 'Match the Column'
                                        );
                                        $this->db->insert('ets_qbank', $userdata);
                                        $qid = $this->db->insert_id();
                                        $this->session->set_userdata('qid', $qid);
                                        
                                        $this->db->where('quid', $quid);
                                        $query    = $this->db->get('ets_quiz');
                                        $quizData = $query->row();
                                        
                                        $qids       = $quizData->qids;
                                        $updatedQid = $qids . ',' . $qid;
                                        $arrayName  = array(
                                            'qids' => $updatedQid
                                        );
                                        $this->db->where('quid', $quid);
                                        $this->db->update('ets_quiz', $arrayName);
                                        
                                    } else {
                                        $score    = (1 / 4);
                                        $userdata = array(
                                            'q_option' => $title,
                                            'q_option_match' => $QuestionOption,
                                            'qid' => $this->session->userdata('qid'),
                                            'score' => $score
                                        );
                                        $this->db->insert('ets_options', $userdata);
                                    }
                                    
                                }
                                
                                
                                
                            }
                        }
                        
                    } else {
                        $this->session->set_flashdata('error', "Please Check Excel Sheet Heading Name!");
                        redirect(base_url("exam/index"));
                    }
                    
                    $this->session->set_flashdata('success', $this->lang->line('import_success'));
                    redirect(base_url("exam/index"));
                    
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('import_error'));
                    redirect(base_url("exam/index"));
                }
                
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('import_error'));
            redirect(base_url("bulkimport/index"));
        }
    }
    
       public function exam_bulkimport() {
        $adminID = $this->session->userdata('adminID');
        if(isset($_FILES["csvExam"])) {
            $config['upload_path'] = "./uploads/csv/";
            $config['allowed_types'] = 'text/plain|text/csv|csv';
            $config['max_size'] = '2048';
            $config['file_name'] = $_FILES["csvExam"]['name'];
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload("csvExam")) {
                $this->session->set_flashdata('error', $this->lang->line('import_error'));
                redirect(base_url("bulkimport/index"));
            } else {
                $file_data = $this->upload->data();
                $file_path =  './uploads/csv/'.$file_data['file_name'];

                if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);

                    foreach ($csv_array as  $key => $row) {
                        
              if ($row['Student_ID']!='') {
                $this->db->where('username',$row['Student_ID']);
                $query  = $this->db->get('student');
                $rowsStudent    = $query->row();
                $rownub  = $query->num_rows();

                      if($rownub==0){
                    echo $this->session->set_flashdata('error', 'Oops Student_ID does not match!!! Please Try Again, student Id = '.$row['Student_ID']);
                     redirect(base_url("bulkimport/index"));
                      } 

               $semester = $row['semester'];
               $Subject_Code = $row['Subject_Code'];
               $Internal_Marks_Max = $row['Internal_Marks_Max'];
               $External_Marks_Max = $row['External_Marks_Max'];
               $Iternal_marks = $row['Iternal_marks'];
               $Exteral_Marks = $row['Exteral_Marks'];
               $Credit = $row['Credit'];
               $Grade = $row['Grade'];
               $PaperType = $row['PaperType'];

               $this->db->where('subject_code',$Subject_Code);
               $this->db->where('classesID',$rowsStudent->classesID);
               $this->db->where('yearsOrSemester',CallSemester($semester));
               $query_subjct = $this->db->get('subject');
                $rowSubject       = $query_subjct->row();
               $rownubSubject  = $query_subjct->num_rows();

                      if($rownubSubject==0){
                    echo $this->session->set_flashdata('error', 'Oops Subjects code does not match!!! Please Try Again, student Id = '.$row['Student_ID'].'Subject Code='.$Subject_Code);
                     redirect(base_url("bulkimport/index"));
                      }       


//start uploading result
$this->db->where('studentID',$rowsStudent->studentID);
$this->db->where('yearsOrSemester',CallSemester($row['semester']));
$examUnique = $this->db->get('exam');
$examUniqueNum  = $examUnique->num_rows();
$examUniqueRow  = $examUnique->row();
$this->session->set_userdata('examUniqueRow', $examUniqueRow);
$this->session->set_userdata('examUniqueNum', $examUniqueNum);
if ($examUniqueNum==0) {
            $insert_dataExam = array(
            'studentID'=>$rowsStudent->studentID,
            'yearsOrSemester'=>CallSemester($row['semester'])
            );                           
            $this->db->insert('exam',$insert_dataExam);
            $examID  = $this->db->insert_id();  

}else{
$examID  = $examUniqueRow->examID;  
  }
  $this->db->where('examID',$examID);
 $this->db->where('subjectID',$rowSubject->subjectID);
 $row_subject = $this->db->get('mark')->row(); 

 if(count($row_subject)){
if($PaperType==''){
$pType = 't';
}else{
$pType = 'p';   
}
            $insert_datamark = array(
            'Internal_Marks_Max'=>$Internal_Marks_Max,
            'Iternal_marks'=>$Iternal_marks,
            'External_Marks_Max'=>$External_Marks_Max,
            'Exteral_Marks'=>$Exteral_Marks,
            'Credit'=>$Credit,
            'Grade'=>$Grade,
            'type'=>$pType,
            );
            $this->db->where('markID',$row_subject->markID);
            $this->db->update('mark',$insert_datamark);  
 } else {
if($PaperType==''){
$pType = 't';
}else{
$pType = 'p';   
}
          $insert_datamark = array(
            'examID'=>$examID,
            'subjectID'=>$rowSubject->subjectID,
            'subject'=>$rowSubject->subject,
            'subject_code'=>$rowSubject->subject_code,
            'Internal_Marks_Max'=>$Internal_Marks_Max,
            'Iternal_marks'=>$Iternal_marks,
            'External_Marks_Max'=>$External_Marks_Max,
            'Exteral_Marks'=>$Exteral_Marks,
            'Credit'=>$Credit,
            'Grade'=>$Grade,
            'type'=>$pType,
            );
            $this->db->insert('mark',$insert_datamark);
}

        }

                    }

                    $this->session->set_flashdata('success', $this->lang->line('import_success'));
                    redirect(base_url("bulkimport/index"));
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('import_error'));
                    redirect(base_url("bulkimport/index"));
                }

            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('import_error'));
            redirect(base_url("bulkimport/index"));
        }
    }


    public function getClass($className, $adminID)
    {
        $usertype = $this->session->userdata("usertype");
 
        if ($className) {
            $query = $this->db->query("SELECT * FROM `classes` WHERE `courseCode` = '$className' and `adminID` = '$adminID' ");
            if (empty($query)) {
                return 'error';
            } else {
                return $query->row();
            }
            
        } else {
            return "error";
        }
    }
    
    public function getSubCourse($subCourseCode, $adminID)
    {
        $usertype = $this->session->userdata("usertype");
        if ($subCourseCode) {
            $query = $this->db->query("SELECT sub_coursesID FROM `sub_courses` WHERE `subCourseCode` = '$subCourseCode' and `adminID` = '$adminID' ");
            if (empty($query)) {
                return 'error';
            } else {
                return $query->row('sub_coursesID');
            }
            
        } else {
            return "error";
        }
    }
    
    public function getTeacher($username)
    {
        $usertype = $this->session->userdata("usertype");
        if ($username) {
            $query = $this->db->query("SELECT teacherID FROM `teacher` WHERE `username` = '$username' ");
            if (empty($query)) {
                return 'error';
            } else {

                return $query->row('teacherID');
            }
            
        } else {
            return "error";
        }
    }
    
    public function getSection($className, $section)
    {
        
        if ($className) {
            $query = $this->db->query("SELECT sectionID, section FROM `section` WHERE `classesID` = '$className' AND `section` = '$section'");
            if (empty($query)) {
                return 'error';
            } else {
                return $query->row();
            }
            
        } else {
            return "error";
        }
    }   
  
    
}

