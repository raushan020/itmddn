<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');





class Viewlecture extends Admin_Controller {







	function __construct() {



		parent::__construct();



		$this->load->model("professor_m");


		$this->load->model("subject_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("student_m");


		$language = $this->session->userdata('lang');



		$this->lang->load('professor', $language);	



	}







	public function index() {



		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$loginuserID = $this->session->userdata("loginuserID");

		if($usertype=="Professor"){

			if ($usertype=='superadmin') {

			 $this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);

			 

			  

			}else{ 

			  $this->data['professorLecture'] = $this->professor_m->get_professor_datatables($adminID);

			  //echo "<pre>"; print_r($this->data['teachers']); die; 

			 

			}

		

			$this->data["subview"] = "viewlecture/index";



			$this->load->view('_layout_main', $this->data);



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}









	public function attendance() {



		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$loginuserID = $this->session->userdata("loginuserID");



		$getClasses = $this->input->get('course');

		$getSubcourse = $this->input->get('subcourse');

		$getSemester = $this->input->get('yearsOrSemester');

		$getSubject = $this->input->get('subject');

		$date = $this->input->get('date');
	
		if($usertype=="Professor"){


            $this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		    $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$getClasses);			
			$this->data['classesRow'] = $this->classes_m->get_single_classes($getClasses);
            $this->data['subjects'] = $this->subject_m->get_subject_proffessor($getClasses,$getSubcourse,$getSemester,$loginuserID);	




   $this->data['student_from_atd'] =  $this->professor_m->getstudent_from_atnd($getSubject,$loginuserID,$date);		 	
$this->data['count_students_atd']=  count($this->data['student_from_atd']);
if(count($this->data['subjects'])){
if ($this->data['count_students_atd']) { 
$this->data['getStudent'] =  $this->data['student_from_atd'];

} else{
$this->data['getStudent'] = $this->professor_m->getstudentforcondition($getClasses,$getSubcourse,$getSemester,$getSubject);	
}
}else{
	$this->data['getStudent'] = 0;
}



	

			 if ($_POST) {

			 	$current_date = date('d-m-Y');

			 	if ($_POST['new_date']<=$current_date) {

			 	$array = array();

			 		for ($i=0; $i <count($_POST['studentID']) ; $i++) { 

			 	    $array['studentID'] = $_POST['studentID'][$i];

			 	    $array['classesID'] =  $_POST['classesID'];

			 	        $array['sub_coursesID'] =  $_POST['sub_coursesID'];

			 	        $array['yearsOrSemester'] =  $_POST['yearsOrSemester'];

			 	       $array['subjectID'] =  $_POST['subjectID'];

					$array['professorID'] = $loginuserID;

					$array['atd'] = $_POST['atd'.$i];

					

					if ($this->data['count_students_atd']) {
				  //$array['atd_update_date'] = $_POST['date'];
						$array['atd_date'] = $_POST['new_date'];
						$this->db->where('atdID',$_POST['atdID'][$i]);
						$this->db->update('attendance_student',$array);

					}else{
					  $array['atd_date'] = $_POST['new_date'];
				      $this->professor_m->insert_attendance($array);
					}

				  

			 		}








				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("viewlecture/attendance?course=".$_POST['classesID']."&subcourse=".$_POST['sub_coursesID']."&yearsOrSemester=".$_POST['yearsOrSemester']."&subject=".$_POST['subjectID']."&date=".$_POST['new_date'].""));

			}else{

				$this->session->set_flashdata('error', 'Please Select Present Date');

				redirect(base_url("viewlecture/index"));

			}

			 }else{

			

			



			

			$this->data["subview"] = "viewlecture/attendance";



			$this->load->view('_layout_main', $this->data);

			}



		} else {



			$this->data["subview"] = "viewlecture/attendance";



			$this->load->view('_layout_main', $this->data);



		}



		

		



	}







	

	//Praveen Pathak

	

	function ajaxProfessors(){



		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		$loginuserID = $this->session->userdata("loginuserID");

		$totalData = $this->professor_m->get_professor_no($adminID);

		$totalFiltered = $totalData;

		$posts = $this->professor_m->make_datatables();

        $data = array();

        if(!empty($posts))

        {

        	$i = 1;

            foreach ($posts as $post)

            {

				

				

				$nestedData['sn'] = '';

				$nestedData['name'] = $post->name;

				$nestedData['username'] = $post->username;

				$nestedData['email'] = $post->email;

			  if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {



			   $buttons     =  btn_view('professor/view/'.$post->professorID, $this->lang->line('view')).btn_edit('professor/edit/'.$post->professorID, $this->lang->line('edit')).btn_delete('professor/delete/'.$post->professorID, $this->lang->line('delete'));



			   $nestedData['action'] = $buttons;



			  }else{



			   $buttons     =  btn_view('professor/view/'.$post->professorID, $this->lang->line('view'));



			   $nestedData['action'] = $buttons;

			  }



                $data[] = $nestedData;



            }

        }



       $json_data = array(

                    "draw"            => intval($this->input->post('draw')),  

                    "recordsTotal"    => $totalData,  

                    "recordsFiltered" => $totalFiltered, 

                    "data"            => $data   

                    );

            

        echo json_encode($json_data); 



}

public function subject_get(){
	
$subject = $this->subject_m->get_subject_proffessor($this->input->post('classesID'),$sub_coursesID=0,$this->input->post('val'),$this->session->userdata('loginuserID'));

  foreach ($subject as $key => $value) {
echo '<option value="'.$value->subjectID.'">'.$value->subject.'</option>';
                                       
}
}





	protected function rules() {



		$rules = array(



			array(



				'field' => 'name', 



				'label' => $this->lang->line("professor_name"), 



				'rules' => 'trim|required|xss_clean|max_length[60]'



			), 



			array(



				'field' => 'sex', 



				'label' => $this->lang->line("professor_sex"), 



				'rules' => 'trim|required|xss_clean'



			),



			// array(



			// 	'field' => 'religion', 



			// 	'label' => $this->lang->line("teacher_religion"), 



			// 	'rules' => 'trim|max_length[25]|xss_clean'



			// ),



			array(



				'field' => 'email', 



				'label' => $this->lang->line("professor_email"), 



				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'



			),



			array(



				'field' => 'phone', 



				'label' => $this->lang->line("professor_phone"), 



				'rules' => 'trim|min_length[5]|max_length[25]|xss_clean|required'



			),



			array(



				'field' => 'username', 



				'label' => $this->lang->line("professor_username"), 



				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'



			),



			array(



				'field' => 'password',



				'label' => $this->lang->line("professor_password"), 



				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_password'



			)



		);



		return $rules;



	}







	function insert_with_image($username) {



	    $random = rand(1, 10000000000000000);



	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));



	    return $makeRandom;



	}







	public function add() {



		$usertype = $this->session->userdata("usertype");

        $adminID = $this->session->userdata("adminID");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "superadmin"){



			if($_POST) {



				$rules = $this->rules();



				$this->form_validation->set_rules($rules);



				if ($this->form_validation->run() == FALSE) {



					$this->data['form_validation'] = validation_errors(); 



					$this->data["subview"] = "professor/add";



					$this->load->view('_layout_main', $this->data);			



				} else {



					$array = array();



					$array['name'] = $this->input->post("name");



					$array['designation'] = $this->input->post("designation");



					$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));



					$array["sex"] = $this->input->post("sex");



					$array['religion'] = $this->input->post("religion");



					$array['email'] = $this->input->post("email");



					$array['phone'] = $this->input->post("phone");



					$array['address'] = $this->input->post("address");

					$array['adminID'] = $adminID;



					$array['jod'] = date("Y-m-d", strtotime($this->input->post("jod")));



					$array['username'] = $this->input->post("username");



					$array['password'] = $this->professor_m->hash($this->input->post("password"));



					$array['usertype'] = "Professor";



					$array["create_date"] = date("Y-m-d h:i:s");



					$array["modify_date"] = date("Y-m-d h:i:s");



					$array["create_userID"] = $this->session->userdata('loginuserID');



					$array["create_username"] = $this->session->userdata('username');



					$array["create_usertype"] = $this->session->userdata('usertype');



					$array["professoractive"] = 1;

			        

			        $new_file = "defualt.png";

						$array["photo"] = $new_file;

						$this->professor_m->insert_professor($array);

						// $this->send_mail_new_regitration();

						$this->session->set_flashdata('success', $this->lang->line('menu_success'));

						redirect(base_url("professor/index"));

					



				}



			} else {



				$this->data["subview"] = "professor/add";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}



	public function view() {



		$usertype = $this->session->userdata('usertype');



		if ($usertype) {



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if ((int)$id) {



				$this->data['professor'] = $this->professor_m->get_professor($id);



				if($this->data['professor']) {



					$this->data["subview"] = "professor/view";



					$this->load->view('_layout_main', $this->data);



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function edit() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Professor"){



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if((int)$id) {



				$this->data['professor'] = $this->professor_m->get_professor($id);



				if($this->data['professor']) {



					if($_POST) {



						$rules = $this->rules();



				

						unset($rules[4],$rules[5]);



						$this->form_validation->set_rules($rules);



						if ($this->form_validation->run() == FALSE) { 



							$this->data["subview"] = "professor/edit";



							$this->load->view('_layout_main', $this->data);



						} else {



							$array = array();



							$array['name'] = $this->input->post("name");

							$array["sex"] = $this->input->post("sex");



							$array['email'] = $this->input->post("email");



							$array['phone'] = $this->input->post("phone");



							$array["modify_date"] = date("Y-m-d h:i:s");



	



								$this->professor_m->update_professor($array, $id);



								$this->session->set_flashdata('success', $this->lang->line('menu_success'));



								redirect(base_url("professor/index"));



						



						}



					} else {



						$this->data["subview"] = "professor/edit";



						$this->load->view('_layout_main', $this->data);



					}



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function delete() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin"){



			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



			if((int)$id) {



				$this->data['professor'] = $this->professor_m->get_professor($id);



				if($this->data['professor']) {



					if($this->data['professor']->photo != 'defualt.png') {



						unlink(FCPATH.'uploads/images/'.$this->data['professor']->photo);



					}



					$this->professor_m->delete_professor($id);



					$this->session->set_flashdata('success', $this->lang->line('menu_success'));



					redirect(base_url("professor/index"));



				} else {



					redirect(base_url("professor/index"));



				}



			} else {



				redirect(base_url("professor/index"));



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function lol_username() {



		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if((int)$id) {



			$professor_info = $this->user_m->get_single_user(array('professorID' => $id));



			$tables = array('student' => 'student', 'parent' => 'parent', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $professor_info->email));



				if(count($user)) {



					$this->form_validation->set_message("lol_username", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}



			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		} else {



			$tables = array('student' => 'student', 'parent' => 'parent', 'professor' => 'professor', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("username" => $this->input->post('username')));



				if(count($user)) {



					$this->form_validation->set_message("lol_username", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}







			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		}			



	}







	public function date_valid($date) {



		if(strlen($date) <10) {



			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");



	     	return FALSE;



		} else {



	   		$arr = explode("-", $date);   



	        $dd = $arr[0];            



	        $mm = $arr[1];              



	        $yyyy = $arr[2];



	      	if(checkdate($mm, $dd, $yyyy)) {



	      		return TRUE;



	      	} else {



	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");



	     		return FALSE;



	      	}



	    } 



	} 







	public function print_preview() {



		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if ((int)$id) {



			$usertype = $this->session->userdata('usertype');



			if ($usertype == "Admin") {



			    $this->load->library('html2pdf');



			    $this->html2pdf->folder('./assets/pdfs/');



			    $this->html2pdf->filename('Report.pdf');



			    $this->html2pdf->paper('a4', 'portrait');







				$this->data["professor"] = $this->professor_m->get_professor($id);



				if($this->data["professor"]) {



					$this->data['panel_title'] = $this->lang->line('panel_title');



					$html = $this->load->view('professor/print_preview', $this->data, true);



					$this->html2pdf->html($html);



					$this->html2pdf->create();



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}	



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function send_mail() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin") {



			$id = $this->input->post('id');



			if ((int)$id) {



				$this->load->library('html2pdf');



			    $this->html2pdf->folder('uploads/report');



			    $this->html2pdf->filename('Report.pdf');



			    $this->html2pdf->paper('a4', 'portrait');







				$this->data["professor"] = $this->professor_m->get_professor($id);



				if($this->data["professor"]) {



					$this->data['panel_title'] = $this->lang->line('panel_title');



					$html = $this->load->view('professor/print_preview', $this->data, true);



					$this->html2pdf->html($html);



					$this->html2pdf->create('save');



					



					if($path = $this->html2pdf->create('save')) {



					$this->load->library('email');



					$this->email->set_mailtype("html");



					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);



					$this->email->to($this->input->post('to'));



					$this->email->subject($this->input->post('subject'));



					$this->email->message($this->input->post('message'));	



					$this->email->attach($path);



						if($this->email->send()) {



							$this->session->set_flashdata('success', $this->lang->line('mail_success'));



						} else {



							$this->session->set_flashdata('error', $this->lang->line('mail_error'));



						}



					}



				} else {



					$this->data["subview"] = "error";



					$this->load->view('_layout_main', $this->data);



				}



			} else {



				$this->data["subview"] = "error";



				$this->load->view('_layout_main', $this->data);



			}



		} else {



			$this->data["subview"] = "error";



			$this->load->view('_layout_main', $this->data);



		}



	}







	public function unique_email() {



		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));



		if((int)$id) {



			$professor_info = $this->professor_m->get_single_professor(array('professorID' => $id));



			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'professor' => 'professor', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $professor_info->username));



				if(count($user)) {



					$this->form_validation->set_message("unique_email", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}



			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		} else {



			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');



			$array = array();



			$i = 0;



			foreach ($tables as $table) {



				$user = $this->professor_m->get_username($table, array("email" => $this->input->post('email')));



				if(count($user)) {



					$this->form_validation->set_message("unique_email", "%s already exists");



					$array['permition'][$i] = 'no';



				} else {



					$array['permition'][$i] = 'yes';



				}



				$i++;



			}







			if(in_array('no', $array['permition'])) {



				return FALSE;



			} else {



				return TRUE;



			}



		}	



	}









function AddUsernamePass(){

 

$password = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) ); // random(ish) 5 digit int



$username = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string



$passwordPlusName = strtok($this->input->post('name_id')," ").'@'.$password;



$usernamePlusName = strtok($this->input->post('name_id')," ").'@'.$username;



$array = array(

	'password'=>$passwordPlusName,

	'username'=>$usernamePlusName

);



 echo  json_encode(($array));



}







// public function send_mail_new_regitration() {

// 		$usertype = $this->session->userdata("usertype");

// 		$loginuserID = $this->session->userdata("loginuserID");

// 		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == 'Professor') {

//      $username  = $this->input->post('username');

//          $this->db->select('*');

//         $this->db->where('username',$username);

//         $this->db->from('teacher');

//       $query2  = $this->db->get();



//       $dataEmail['emailData'] = $query2->row();



// $array = array("src" => base_url('uploads/images/'.$this->data["siteinfos"]->photo),'class' => 'img-rounded');



// $dataEmail['imgData'] = $array;



// //  print_r($dataEmail['emailData']);

// // exit();

// $this->load->library('email');

// $config=array(

// 'charset'=>'utf-8',

// 'wordwrap'=> TRUE,

// 'mailtype' => 'html'

// );



//                     $this->email->initialize($config);

// 					$html = $this->load->view('emailTemplates/newRegisterTeacher', $dataEmail , true);

				 

// 					$this->email->set_mailtype("html");

// 					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);

// 					$this->email->to($dataEmail['emailData']->email);

// 					$this->email->subject("Welecome to Distance Education School Counselor Panel for SVSU");

// 					$this->email->message($html);

// 						if($this->email->send()) {

// 							$this->session->set_flashdata('success', $this->lang->line('mail_success'));

// 						} else {

// 							$this->session->set_flashdata('error', $this->lang->line('mail_error'));

// 						}

// 		} else {

// 			$this->data["subview"] = "error";

// 			$this->load->view('_layout_main', $this->data);

// 		}

// 	}





	function active() {



		$usertype = $this->session->userdata("usertype");



		if($usertype == "Admin") {



			$id = $this->input->post('id');



			$status = $this->input->post('status');



			if($id != '' && $status != '') {



				if((int)$id) {



					if($status == 'chacked') {



						$this->professor_m->update_professor(array('professoractive' => 1), $id);



						echo 'Success';



					} elseif($status == 'unchacked') {



						$this->professor_m->update_professor(array('professoractive' => 0), $id);



						echo 'Success';



					} else {



						echo "Error";



					}



				} else {



					echo "Error";



				}



			} else {



				echo "Error";



			}



		} else {



			echo "Error";



		}



	}



}







/* End of file teacher.php */



/* Location: .//D/xampp/htdocs/school/mvc/controllers/teacher.php */