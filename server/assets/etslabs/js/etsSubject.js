       function AllSubjects(){
        $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"subject/AllSubjects",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

        function ActiveSubjects(){
            $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"subject/ActiveSubjects",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
        function DraftSubjects(){
            $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"subject/DraftSubjects",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
        function TrashSubjects(){
            $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: base_url+"subject/TrashSubjects",

                data:{durationID:'durationID'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}  

$(document).ready(function(){
    $('#select_all').on('click',function(){
        if(this.checked){
            $('.mycheckbox').each(function(){
                this.checked = true;
                $(".etsfilertButton").removeClass("disabled");
                $(".etsfilertButton").removeAttr("disabled");
            });
        }else{
             $('.mycheckbox').each(function(){
                this.checked = false;
                $(".etsfilertButton").addClass("disabled");
                $(".etsfilertButton").attr("disabled",true);
            });
        }
    });
    $('.table').on('click', '.mycheckbox', function(){

        if($('.mycheckbox:checked').length == $('.mycheckbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
         if($('.mycheckbox:checked').length >=1){
    $(".etsfilertButton").removeClass("disabled");
     $(".etsfilertButton").removeAttr("disabled");
}else{
$(".etsfilertButton").addClass("disabled");
 $(".etsfilertButton").attr("disabled",true);
}
    });
});