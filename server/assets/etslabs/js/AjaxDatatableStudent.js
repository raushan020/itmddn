// 1. exam list



$(document).ready(function(){  

      var dataTable = $('#exam_listTables').DataTable({

            dom: 'lBfrtip',

              scrollY:"800px",

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [



              ],

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],  

           "ajax":{  

                url:base_url+'exam/AjaxTable_exam_list/'+uri,  

                type:"POST"  

           },  

"columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":5}

  ],

      "columns": [

              { "data": "sn" },

              { "data": "subject_code" },

              { "data": "quiz_name"},   

              { "data": "start_date" },

              { "data": "yearsOrSemester" },

              { "data": "action" },

           ], 

      });  

      dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

  }); 

});  



// 2. subject

