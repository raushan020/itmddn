<?php

function btn_add($uri, $name) 
    {
    return anchor($uri, "<i class='fa fa-plus'></i>", "class='btn btn-primary primary_1 btn-xs mrg for_margR' data-placement='top' data-toggle='tooltip' title='".$name."'");
    }

function btn_view($uri, $name) 
    {
    return anchor($uri, "<i class='fa fa-eye'></i>", "class='btn btn-success view btn-xs' data-placement='top' data-toggle='tooltip' title='".$name."'");
    }

function btn_print($uri, $name) 
    {
    return anchor($uri, "<i class='fa fa-print'></i>", "class='btn btn-primary primary_1 btn-xs mrg for_margR' data-placement='top' data-toggle='tooltip' title='".$name."'");
    }

// function btn_zip($uri, $name) 
//     {
//     return anchor($uri, "<i class='fa fa-file-archive'></i>", "class='btn btn-primary btn-xs mrg for_margR' data-placement='top' data-toggle='tooltip' title='".$name."'");
//     }

function btn_edit($uri, $name) 
    {
    return anchor($uri, "<i class='fa fa-edit'></i>", "class='btn btn-warning warning_1 btn-xs mrg for_margR' data-placement='top' data-toggle='tooltip' title='".$name."'");
    }

function btn_delete($uri, $name) 
    {
    return anchor($uri, "<i class='fa fa-trash-o'></i>",
                    array(
                        // 'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
                        'class' => 'btn btn-danger trash btn-xs mrg for_margR',
                        'data-placement' => 'top',
                        'data-toggle' => 'tooltip',
                        'title' => $name,
                        'onclick'=>'return confirm(\'Do you Really want to Delete This?\')'
                        )
                );
    }

function delete_file($uri, $id) 
    {
    return anchor($uri, "<i class='fa fa-times '></i>",
                array(
                    'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",
                    'id' => $id,
                    'class' => "close pull-right"
                    )
                );
    }



function share_file($uri, $id) {



    return anchor($uri, "<i class='fa fa-globe'></i>",



        array(



            'onclick' => "return confirm('you are about to delete a record. This cannot be undone. are you sure?')",



            'id' => $id,



            'class' => "pull-right"



        )



    );



}

function btn_dash_view($uri, $name) {



    return anchor($uri, "<span class='fa fa-check-square-o'></span>", "class='btn btn-success btn-xs mrg' style='background-color:#00bcd4;color:#fff;border:1px solid #00bcd4' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");



}

function btn_invoice($uri, $name) {



    return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");



}


function btn_return($uri, $name) {



    return anchor($uri, "<i class='fa fa-mail-forward'></i>",



        array(



            "onclick" => "return confirm('you are return the book . This cannot be undone. are you sure?')",



            "class" => 'btn btn-danger btn-xs',



            'data-placement' => 'top',



            'data-toggle' => 'tooltip',



            'data-original-title' => $name


        )



    );



}


function btn_attendance($id, $method, $class, $name) {



    return "<input type='checkbox' class='".$class."' $method id='".$id."' data-placement='top' data-toggle='tooltip' data-original-title='".$name."' >  ";



    // return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");



}


function btn_promotion($id, $class, $name) {

    return "<input type='checkbox' class='".$class."' id='".$id."' data-placement='top' data-toggle='tooltip' data-original-title='".$name."' >  ";



    // return anchor($uri, "<i class='fa fa-credit-card'></i>", "class='btn btn-primary btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='".$name."'");



}


if (!function_exists('dump')) {

    function dump ($var, $label = 'Dump', $echo = TRUE)

    {

        // Store dump in variable

        ob_start();

        var_dump($var);

        $output = ob_get_clean();

        // Add formatting

        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);

        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';

        // Output
        if ($echo == TRUE) {

            echo $output;

        }

        else {
            return $output;

        }

    }

}

if (!function_exists('dump_exit')) {



    function dump_exit($var, $label = 'Dump', $echo = TRUE) {



        dump ($var, $label, $echo);



        exit;



    }



}


// infinite coding starts here..

function btn_add_pdf($uri, $name) {

    return anchor($uri, "<i class='fa fa-file'></i> ".$name, "class='btn btn-success btn-xs' style='text-decoration: none;' role='button' target='_blank'");

}

function btn_sm_edit($uri, $name) {

    return anchor($uri, "<i class='fa fa-edit'></i> ".$name, "class='btn btn-success btn-xs' style='text-decoration: none;' role='button'");

}

function btn_sm_add($uri, $name) {

    return anchor($uri, "<i class='fa fa-plus'></i> ".$name, "class='btn btn-success btn-xs' style='text-decoration: none;' role='button'");

}

function btn_sm_accept_and_denied_leave($uri, $name, $icon) {

    return anchor($uri, "<i class='fa fa-".$icon."'></i> ".$name, "class='btn btn-success btn-xs' style='text-decoration: none;' role='button'");

}

function btn_payment($uri, $name) {

    return anchor($uri, "<i class='fa fa-credit-card'></i> ".$name, "class='btn btn-success btn-xs'style='text-decoration: none;' role='button'");
}



// infinite coding end here..

// SST



function CallEducationMode($param){

$Array =  array(1=>"Year",2=>"Semester");

return $Array[$param];

}

function CallYears($param){
$Array =  array(1=>'1st_Year',2=>'2nd_Year',3=>'3rd_Year',4=>'4th_Year',5=>'5th_Year',6=>'6th_Year',7=>'7th_Year',8=>'8th_Year',9=>'9th_Year',10=>'10th_Year');



return $Array[$param];



}



function CallSemester($param){



$Array =  array(1=>'1st_Semester',2=>'2nd_Semester',3=>'3rd_Semester',4=>'4th_Semester',5=>'5th_Semester',6=>'6th_Semester',7=>'7th_Semester',8=>'8th_Semester',9=>'9th_Semester',10=>'10th_Semester');



return $Array[$param];



}

function CallSemesterlateral($param){



$Array =  array(3=>'3rd_Semester',4=>'4th_Semester',5=>'5th_Semester',6=>'6th_Semester',7=>'7th_Semester',8=>'8th_Semester',9=>'9th_Semester',10=>'10th_Semester');



return $Array[$param];



}



    function FindYearNumber($year){

        $Array =  array(1=>'1st_Year',2=>'2nd_Year',3=>'3rd_Year',4=>'4th_Year',5=>'5th_Year',6=>'6th_Year',7=>'7th_Year',8=>'8th_Year',9=>'9th_Year',10=>'10th_Year');

        foreach ($Array as $key => $value) {

            if ($year==$value) {

                return $key;

                exit();

            }

        }

    }



    function FindSemesterNumber($semester){

        $Array =  array(1=>'1st_Semester',2=>'2nd_Semester',3=>'3rd_Semester',4=>'4th_Semester',5=>'5th_Semester',6=>'6th_Semester',7=>'7th_Semester',8=>'8th_Semester',9=>'9th_Semester',10=>'10th_Semester');

        foreach ($Array as $key => $value) {

            if ($semester==$value) {

                return $key;

                exit();

            }

        }

    }

function return_year($semester){
    if($semester=='1st_Semester' || $semester=='2nd_Semester'){
    return '1st_Year';
    }elseif ($semester=='3rd_Semester' || $semester=='4th_Semester') {
    return '2nd_Year';
    }elseif ($semester=='5th_Semester' || $semester=='6th_Semester') {
    return '3rd_Year';
    }elseif ($semester=='7th_Semester' || $semester=='8th_Semester') {
    return '4th_Year';
    }else{
     return $semester;  
    }

}


function  findYearOrSemesterByYear($startYear,$duration,$mode,$sessionType){
          if ($sessionType=='A'){ 

                                    if ($mode==1) {
                                        $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                                        $d2 = new DateTime('now');
                                        $diff2 = $d2->diff($d1);
                                        $findYears =  $diff2->y;
                                        if ($findYears>=$duration) {
                                            $insert_data = $duration;
                                        }else{
                                            $findYearsAddOne = $findYears+1;
                                            $insert_data = $findYearsAddOne;
                                        }
                                    }else{
                                        $Durationsemester = $duration*2;
                                        $d1 = new DateTime($startYear.'-06-01'); //y-m-d
                                        $d2 = new DateTime('now');
                                        $date1 = $startYear.'-06-01';
                                        $date2 = date("Y-m-d");
                                        $ts1 = strtotime($date1);
                                        $ts2 = strtotime($date2);
                                        $year1 = date('Y', $ts1);
                                        $year2 = date('Y', $ts2);
                                        $month1 = date('m', $ts1);
                                        $month2 = date('m', $ts2);
                                        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                                        $totalSemester =  floor($diff/6);
                                        $semesterRemainder = fmod($diff,6);
                                        if ($totalSemester>=$Durationsemester) {
                                            $insert_data  = $Durationsemester;
                                        }else{
                                            if ($semesterRemainder) {
                                                $semeterAddone = $totalSemester+1;
                                                $insert_data = $semeterAddone;
                                            }else{
                                                $insert_data = $totalSemester;
                                            }
                                        }
                                    }                                        
                                }else{
                                // calander

                                                                        if ($mode==1) {
                                        $d1 = new DateTime($startYear.'-01-01'); //y-m-d
                                        $d2 = new DateTime('now');
                                        $diff2 = $d2->diff($d1);
                                        $findYears =  $diff2->y;
                                        if ($findYears>=$duration) {
                                            $insert_data = $duration;
                                        }else{
                                            $findYearsAddOne = $findYears+1;
                                            $insert_data = $findYearsAddOne;
                                        }
                                    }else{
                                        $Durationsemester = $duration*2;
                                        $d1 = new DateTime($startYear.'-01-01'); //y-m-d
                                        $d2 = new DateTime('now');
                                        $date1 = $startYear.'-01-01';
                                        $date2 = date("Y-m-d");
                                        $ts1 = strtotime($date1);
                                        $ts2 = strtotime($date2);
                                        $year1 = date('Y', $ts1);
                                        $year2 = date('Y', $ts2);
                                        $month1 = date('m', $ts1);
                                        $month2 = date('m', $ts2);
                                        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                                        $totalSemester =  floor($diff/6);
                                        $semesterRemainder = fmod($diff,6);
                                        if ($totalSemester>=$Durationsemester) {
                                            $insert_data  = $Durationsemester;
                                        }else{
                                            if ($semesterRemainder) {
                                                $semeterAddone = $totalSemester+1;
                                                $insert_data = $semeterAddone;
                                            }else{
                                                $insert_data = $totalSemester;
                                            }
                                        }
                                    }
                                }
                     return $insert_data;           

}


    function comp_numb($input){

        $input = number_format($input);

        $input_count = substr_count($input, ',');

        $arr = array(1=>'K','M','B','T');

        if(isset($arr[(int)$input_count]))      

           return substr($input,0,(-1*$input_count)*4).$arr[(int)$input_count];

        else return $input;

    }
