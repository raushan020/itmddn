<?php

class AjaxControllerDataManagement extends MY_Controller {

	function __construct() {

parent::__construct();

$this->load->library("session");
$this->load->helper('language');
$this->load->helper('form');
$this->load->database();
$this->load->model('subject_m');
$this->load->model('setting_m');
$this->load->model('classes_m');
		$language = $this->session->userdata('lang');

		$this->lang->load('subject', $language);

}

function AddSubcourse(){
$adminID = $this->session->userdata("adminID");
$classID = $this->input->post('classesID');
$sub_course = $this->input->post('subCoursesName');
$ids = $this->input->post('ids');
$digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int
$char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string
$setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));
$sitenameString   =  $setingsData->sname;
$firstCharacter = substr($sitenameString, 0, 2);
$subCourseCode      =   strtoupper($firstCharacter).'-'.$char.$digit;

$data = array(
  'classesID'=>$classID,
  'sub_course'=>$sub_course,
  'adminID'=>$adminID,
  'subCourseCode'=>$subCourseCode ,
  );
$this->db->insert('sub_courses',$data);
$insertID  = $this->db->insert_id();

$this->CommonDataForCourse($adminID,$classID,$ids);

}

function DeleteSubCourseDatamanament(){

$adminID = $this->session->userdata("adminID");
$sub_coursesID = $this->input->post('sub_coursesID');
$classesID = $this->input->post('classesID');
$ids = $this->input->post('ids');
$this->db->where('adminID',$adminID);
$this->db->where('sub_coursesID',$sub_coursesID);
$this->db->delete('sub_courses');
$this->CommonDataForCourse($adminID,$classesID,$ids);

}

function CommonDataForCourse($adminID,$classesID,$ids){
  $array = array(
  'classesID' =>$classesID,
   'adminID' =>$adminID, 
);

$subjects   = $this->classes_m->GetCoursesByParam($array);

foreach ($subjects as $key => $value) {
       echo "<li>
   <div class='col-md-2'><label>".$value->subCourseCode."</label><input type='hidden'  name='subcoursesID[]' value='".$classesID."'>
   </div>
   <div class='col-md-8'>
      <input type='text' class='form-control' name='subcourses[]' value='".$value->sub_course."'>
   </div>
   <div class='col-md-2'><button class='btn btn-remove btn-danger' type='button'>
      <span class='glyphicon glyphicon-minus' onclick='DeleteSubCourseDatamanament(".$classesID .",".$value->sub_coursesID.",".$ids.")'></span></button>
   </div>
   <div class='clearfix'></div>
</li>";
}
echo "<li><div class='form-group fvrduplicate'>
      <div class='fvrclonned'>
         <label for='classes' class='col-sm-2 control-label'>
         Add Sub Courses 
         </label>
         <div class='col-sm-6'>
            <input type='text' class='form-control add' id='subCoursesName".$ids."' name='subcourse[]' placeholder='Add Sub Course here' >
         </div>
         <span><button class='btn btn-success btn-add' type='button' onclick='AddSubCourseDatamanament(".$classesID.", ".$ids.")'><span class='glyphicon glyphicon-plus'></span></button></span>
      </div>
   </div>
</li>";
}

function EditSubCourse(){
  $column = $this->input->post('column');
  $SubcourseID  = (int) $this->input->post('SubcourseID');
  $val =  $this->input->post('val');
  $data = array(
   "sub_course" =>$val, 
);

$this->db->where('sub_coursesID',$SubcourseID);
$query = $this->db->update('sub_courses',$data);
if ($query) {
  echo "string";
}else{
  echo "false";
}
}



}

 ?>