<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '/ZapfDingbats',
    'bold' => $fontDir . '/ZapfDingbats',
    'italic' => $fontDir . '/ZapfDingbats',
    'bold_italic' => $fontDir . '/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '/Symbol',
    'bold' => $fontDir . '/Symbol',
    'italic' => $fontDir . '/Symbol',
    'bold_italic' => $fontDir . '/Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '/DejaVuSans-Bold',
    'bold_italic' => $fontDir . '/DejaVuSans-BoldOblique',
    'italic' => $fontDir . '/DejaVuSans-Oblique',
    'normal' => $fontDir . '/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '/DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '/DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '/DejaVuSansMono-Oblique',
    'normal' => $fontDir . '/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '/DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '/DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '/DejaVuSerif-Italic',
    'normal' => $fontDir . '/DejaVuSerif',
  ),
  'pignose-calendar-icon' => array(
    'normal' => $fontDir . '/6f32e8f753091f152045d50c2324138b',
  ),
  'et-line' => array(
    'normal' => $fontDir . '/a0a52ccdb2cc6602d316ca2fe0221a53',
  ),
  'themify' => array(
    'normal' => $fontDir . '/8ceaec24ab0ac19a7212f67a28f7c536',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '/c4d1ff0bf40d7c95173c04bead20fcd3',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '/47229821ea16b2fdae619608df4f3fc6',
  ),
  'weather' => array(
    'normal' => $fontDir . '/4c66cce24b8bb02768f5f78ed6965993',
  ),
) ?>