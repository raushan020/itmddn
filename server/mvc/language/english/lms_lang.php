<?php

/* List Language  */
$lang['panel_title'] = "LMS / PDF";
$lang['add_title'] = "Add a LMS / PDF";
$lang['slno'] = "#";
$lang['lms_class_name'] = "Class Name";
$lang['lms_teacher_name'] = "Teacher Name";
$lang['lms_student'] = "Student";
$lang['lms_name'] = "lms Name";
$lang['lms_author'] = "lms Author";
$lang['lms_code'] = "lms Code";
$lang['lms_teacher'] = "Teacher";
$lang['lms_classes'] = "Class";
$lang['lms_select_class'] = "Select Class";
$lang['lms_select_classes'] = "Select Class";
$lang['lms_select_teacher'] = "Select Teacher";
$lang['lms_select_student'] = "Select Student";




$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_lms'] = 'Add LMS';
$lang['update_lms'] = 'Update LMS';