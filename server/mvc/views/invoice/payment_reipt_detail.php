  <style type="text/css">
                 
    /*.verticalTable th, td {
      border: 1px solid black !important;
      border-collapse: collapse;
    }

    .verticalTable th, td {
      padding: 5px;
      text-align: left;
    }*/

  </style>

  <div class="">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"><i class="fa fa-table"> </i> Payment Reciept</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">

          <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

          <li class="active">Payment Reciept</li>
        </ol>
      </div>
    </div>

    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
        <div class="card">
          <div class="card-body">
            <div class="well">
              <div class="row">
                <?php if($this->session->flashdata('success')) { ?>
                  <div class="alert alert-success">      
                    <strong>Success!</strong><?=$this->session->flashdata('success')?>
                  </div>
                <?php } ?>

                <div class="col-sm-3">
                   <button class="btn btn-success btn-xs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> Print </button>
                   <!-- <a href="http://101.53.153.40/blackboard/invoice/print_preview/37" class='btn btn-success btn-xs' style='text-decoration: none;' role='button' target='_blank'><i class='fa fa-file'></i> PDF Preview</a> -->
       

                </div>
                <div class="col-sm-9 pull-right">
             
                  <a href="<?php echo base_url() ?>invoice/clgAdd_reciept?panel=<?php echo $this->input->get('panel') ?>" class=" btn btn-success add-btn pull-right">Add Reciept</a>
        
                 </div>
              </div>
            </div>
          </div>
     
          <div class="col-sm-12">    
            <div class="clearfix"></div>
            <div class="breakdownl_tbl"></div>
            <div id="hide-table">
              <table align="center" class="table table-bordered table-hover dataTable no-footer" id="clgforBillingStudent">
                <tr>                  
                  <th>Payment Date</th> 
                  <th>Paid Amount</th>
                  <th>Payment Mode</th>
                  <th>Transaction ID/ Cheque No.</th>
                  <th>Status</th>
                  <th>Receipt Download</th>
                </tr>

                <?php
                  foreach ($billinginvoiceData as $key => $values) {
                ?>
 
                  <tr>
                    <td ><?php echo $values->invoiceDate ?></td>                    
                    <td ><?php echo number_format($values->paidAmount) ?></td>
                    <td ><?php echo $values->payment_mode ?></td>
                    <td >
                      <?php 
                        if($values->payment_mode=='Bank'){
                           echo $values->transactionID; 
                         }
                         elseif($values->payment_mode == 'Cheque'){
                       echo $values->cnumber;
                     }else{
                      echo $values->transactionID; 
                     }
                      ?>
                    </td>
                    <td ><?php if($values->status == 1){echo 'Paid';}else{echo "Not Paid";} ?></td>
                    <td >
                      <!-- <?php echo $values->src ?> -->
                      <a class="btn btn-info download-sample" href="<?=base_url('uploads/invoice_img/'.$values->src)?>" download><i class="fa fa-download"></i> <?=$this->lang->line("bulkimport_sample")?></a>
                      <a class="btn btn-default" href="<?=base_url('invoice/edit_receipt_detail/'.$values->paymentreceiptID)?>?panel=<?php echo $this->input->get('panel') ?>" ><i class="fa fa-pencil"></i> Edit</a>
                      <a class="btn btn-danger" href="<?=base_url('invoice/reciptDelete/'.$values->paymentreceiptID)?>?panel=<?php echo $this->input->get('panel') ?>" ><i class="fa fa-trash"></i> Delete</a>
                    </td>
                  </tr> 
                <?php } ?>

              </table>

              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18"></div>
              <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18"></div>                
            </div>
          </div> <!-- col-sm-12 -->      
        </div><!-- row -->
      </div><!-- Body -->
    </div><!-- /.box -->
  </div>
</div>

<script type="text/javascript">
     function invoice_filter_url(val){
      
   window.location.href=base_url+"invoice/clgPayment/"+val;
   
   }
   
</script>
<script>

  var status = '';

  var id = 0;

  $('.onoffswitch-small-checkbox').click(function() {

      if($(this).prop('checked')) {

          status = 'chacked';

          id = $(this).parent().attr("id");

      } else {

          status = 'unchacked';

          id = $(this).parent().attr("id");

      }



      if((status != '' || status != null) && (id !='')) {

          $.ajax({

              type: 'POST',

              url: "<?=base_url('teacher/active')?>",

              data: "id=" + id + "&status=" + status,

              dataType: "html",

              success: function(data) {

                  if(data == 'Success') {

                      toastr["success"]("Success")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  } else {

                      toastr["error"]("Error")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  }

              }

          });

      }

  }); 

</script>

