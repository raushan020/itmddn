	<style type="text/css">
	.panel-body .tag-new {
		position: absolute;
		right: 0px;
		top: 0;
		font-size: 10px;
		font-weight: 700;
		color: #fff;
		background: #77c13a;
		line-height: 1;
		text-transform: uppercase;
		padding: 5px 10px 3px;
		width: 42px;
	}
	@-webkit-keyframes blinker {
		from {opacity: 1.0;}
		to {opacity: 0.0;}
	}
	.blink{
		text-decoration: blink;
		-webkit-animation-name: blinker;
		-webkit-animation-duration: 0.9s;
		-webkit-animation-iteration-count:infinite;
		-webkit-animation-timing-function:ease-in-out;
		-webkit-animation-direction: alternate;
	}
	.page-section{
		padding: 0px 0px 40px 0px;
	}
	.cow{
		position: relative;
		bottom: 19px;
		z-index: 11;
		background: black;
		color: #fff;
		padding-left: 5px;
		width: 75px;
		text-transform: uppercase;
		font-size: xx-small;
	}
	.paper-shadow .panel-bodyText{
		color: #f2f2f2;
	}
	</style>
	<!-- 2nd choice -->
	<div class="col-md-12">
		<div class="col-md-9">
			<div class="row" data-toggle="isotope">
			
			</div>
		</div>
		<?php $this->load->view("components/page_lmssidebar"); ?>
		<br/>
		<br/>
	</div>
	<script type="text/javascript">
		$('#yearSemesterID').change(function() {
			// alert(yearSemesterID);
			// $('#subjectsTablesPdf').DataTable().state.clear();
			var yearSemesterID = $(this).val();
			$.ajax({
				type: 'POST',
				url: "<?=base_url('lms/lmssubject')?>",
				data: "yearSemesterID=" + yearSemesterID,
				dataType: "html",
				success: function(data) {
					location.reload();
				}
			});
		});
		</script>
		<script type="text/javascript">
		function ResetSemesterYear(){
			$.ajax({
				type: 'POST',
				url: "<?=base_url('lms/ResetSemesterYear')?>",
				data:{ResetSesession:'ResetSesession'},
				dataType: "html",
				success: function(data) {
					location.reload();
				}
			});
		}
	</script>