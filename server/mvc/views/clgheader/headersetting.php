<div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor">Header Setting </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li class="breadcrumb-item"><i class="fa fa-bullseye"></i> <a href="<?=base_url("dashboard/index")?>"><?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="breadcrumb-item"><a href="<?=base_url()?>invoice/ClgHeader?panel=<?php echo $this->input->get('panel') ?>">Header Setting</a></li>

                <li class="breadcrumb-item active"><?=$this->lang->line('menu_add')?>Header Setting</li> </li>

            </ol>

        </div>

     </div>

    <!-- form start -->

    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">

            <div class="col-sm-12">

               <!--  <form class="form-horizontal" autocomplete="off" role="form" name="myForm" method="post" enctype="multipart/form-data" onsubmit="return validateForm()"> -->

                <form class="form-horizontal" autocomplete="off" role="form" novalidate name="myForm" method="post" enctype="multipart/form-data" >

                    <div class="persnolInfo">

                        <p>Field are required with <span class="red-color">*</span></p>
                        
                       

                        <h3  class="border_heading">Header Setting</h3>

                        <?php 



                            if(form_error('headersetting')) 



                                echo "<div class='form-group has-error' >";



                            else     



                                echo "<div class='form-group' >";



                        ?>



                        <label for="name_id" class="col-sm-2 control-label">



                            Header<span class="red-color">*</span>



                        </label>

                        <?php if($showheader)
                        { ?>
                        	<div class="col-sm-6">

                             <textarea type="text" class="form-control" required placeholder="Write Header Information"  name="headersetting" rows="10" cols="5" ><?php echo $showheader->header; ?></textarea>


                        </div>
                      <?php  } 
                        else
                        { ?>
                        	<div class="col-sm-6">

                             <textarea type="text" class="form-control" required placeholder="Write Header Information"  name="headersetting" rows="10" cols="5" ></textarea>


                        </div>

                     <?php } ?>

                        



                        <span class="col-sm-4 control-label">



                            <?php echo form_error('headersetting'); ?>



                        </span>



                    </div>         

                    <div class="col-sm-offset-2 ">

                        <input type="submit" class="btn btn-success add-btn" value="Change" >

                    </div>

                        

                </form>



            </div> <!-- col-sm-8 -->



        </div><!-- row -->

    </div>

</div>

</div>

    </div><!-- Body -->

</div>

