
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("exam/index")?>"><?=$this->lang->line('menu_exam')?></a></li>

            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_exam')?></li>
            </ol>
        </div>
    </div>



<div class="">

    

    <!-- form start -->

    <div class="container-fluid">
      <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-9">
<p>Field are required with <span class="red-color">*</span></p>
<h3 class="border_heading">Exam Information</h3>
<form class="form-horizontal" action="<?php echo base_url() ?>exam/find_question/<?php echo $this->uri->segment(3) ?>" role="form" method="post">
   <div class="col-md-11">
      <div class="">
        <div class="panel-body">
          <?php 
            if(form_error('classesID')) 
              echo "<div class='form-group has-error' >";
            else     
              echo "<div class='form-group' >";
          ?>
        </div>
        <div class="form-group">
              <label for="courseName" class="col-md-2 col-form-label">Copy From</label>
              <div class="col-sm-4">
                <input type="radio" name="extype" required onclick="copy_question_type(1)" id="extype1" value="1" ><label for="extype1">Copy from Units</label>
              </div>
              <div class="col-sm-4">
                <input type="radio" name="extype" required value="2" onclick="copy_question_type(2)" id="extype2"><label for="extype2">Copy from Subject</label>
              </div>
              <div class="clearfix"></div>
              
            </div>

<style type="text/css">
  .ul_li_class li{
display:inline-block;
margin-right:10px; 
  }
</style>
<div id="copy_content">


</div>

            <button class="btn btn-success add-btn" type="submit">View and Copy</button>
      
 
      </div>
   </div>
</form>


<?php if(isset($qbank)){ ?>
<form action="<?php echo base_url() ?>exam/copy_qids" method="post">

<table class="table">
  <thead>

    <tr>
      <th></th>
      <th>So.N</th>
      <th>Question Name</th>
      <th>Type</th>
    </tr>
  </thead>
  <tbody>
<?php 
  foreach ($qbank as $key => $value) {
 ?>
    <tr>
      <td><input type="checkbox" name="checkedID[]" checked="" value="<?php echo $value->qid  ?>" ></td>
      <td><?php echo  $key+1 ?></td>
      <td><?php echo $value->question; ?></td>
      <td><?php echo $value->question_type; ?></td>
    </tr>
<?php } ?>
  </tbody>
</table>  
<input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name = 'quid'>
<button class="btn btn-success add-btn" type="submit">Copy</button>
</form>
<?php } ?>

            </div>    

        </div>

    </div>

</div>



<script type="text/javascript">

$('#date').datepicker();

</script>

<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesID").val();
       var  sub_CourseID  = $("#sub_CourseID").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('exam/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function copy_question_type(val){
    var  id = "<?php echo $this->uri->segment(3); ?>";
       $.ajax({
        type: "POST",
        url:"<?=base_url('exam/copyType')?>",
        data:{"val":val,"id":id},
        success: function(response) {
            $("#copy_content").html(response);
        }
    });

  }
</script>
<script type="text/javascript">
  function AddQuizName(val){
var exam_for =  $('#exam_for').val();
if(exam_for==1){
$("#quizID").val($("#subject_id option:selected").text());
}else{
	     $.ajax({
        type: "POST",
        url:"<?=base_url('exam/GetUnits')?>",
        data:{"subjectID":val},
        success: function(response) {
            $("#unitID").html(response);
        }
    });
  }
}
</script>




