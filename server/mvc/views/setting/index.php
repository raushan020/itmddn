<div class="">


           <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="fa fa-cog"></i> Setting</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i>Dashboard</a></li>
            <li class="active">Setting</li>
            </ol>
                    </div>
            </div>
    <!-- form start -->

<div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">

            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

                    <?php

                        if(form_error('sname'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="sname" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_name")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="sname" name="sname" value="<?=set_value('sname', $setting->sname)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('sname'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('phone'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="phone" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_phone")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $setting->phone)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('phone'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('email'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="email" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_email")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $setting->email)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('email'); ?>

                        </span>

                    </div>
                    
                    <?php

                        if(form_error('footer'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="footer" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_footer")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" id="footer" name="footer" value="<?=set_value('footer', $setting->footer)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('footer'); ?>

                        </span>

                    </div>



                    <?php

                        if(form_error('address'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="address" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_address")?>

                        </label>

                        <div class="col-sm-6">

                            <textarea class="form-control" style="resize:none;" id="address" name="address"><?=set_value('address', $setting->address)?></textarea>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('address'); ?>

                        </span>

                    </div>



                    <!-- <?php

                        if(form_error('lang'))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="lang" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_lang")?>

                        </label>

                        <div class="col-sm-6">

                            <?php

                                echo form_dropdown("language", array("english" => $this->lang->line("setting_english"),

                                "bengali" => $this->lang->line("setting_bengali"),

                                "arabic" => $this->lang->line("setting_arabic"),

                                "chinese" => $this->lang->line("setting_chinese"),

                                "french" => $this->lang->line("setting_french"),

                                "german" => $this->lang->line("setting_german"),

                                "hindi" => $this->lang->line("setting_hindi"),

                                "indonesian" => $this->lang->line("setting_indonesian"),

                                "italian" => $this->lang->line("setting_italian"),

                                "romanian" => $this->lang->line("setting_romanian"),

                                "russian" => $this->lang->line("setting_russian"),

                                "spanish" => $this->lang->line("setting_spanish"),

                                "thai" => $this->lang->line("setting_thai"),

                                "turkish" => $this->lang->line("setting_turkish"),

                                ),

                                set_value("lang", $setting->language), "id='lang' class='form-control'");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('lang'); ?>

                        </span>

                    </div> -->



                

                    <?php

                        if(isset($image))

                            echo "<div class='form-group has-error' >";

                        else

                            echo "<div class='form-group' >";

                    ?>

                        <label for="photo" class="col-sm-2 control-label">

                            <?=$this->lang->line("setting_school_photo")?>

                        </label>

                        <div class="col-sm-4">

                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />

                        </div>



                        <div class="col-sm-2">

                            <div class="fileUpload btn btn-success form-control">

                                <span class="fa fa-repeat"></span>

                                <span><?=$this->lang->line("upload_setting")?></span>

                                <input name="upload_image_setting" id="upload_image_setting" type="file" class="upload"/>
                                <div id="uploaded_image_setting"></div>
                            </div>

                        </div>

                         <span class="col-sm-4 control-label">

                            <?php if(isset($image)) echo $image; ?>

                        </span>

                    </div>



                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("update_setting")?>" >

                        </div>

                    </div>



                </form>

            </div>

        </div>

    </div>

</div>
<div id="uploadimageModal_setting" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                          <div id="image_demo_setting" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                          <button class="btn btn-success crop_image_setting">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

document.getElementById("uploadBtn").onchange = function() {

    document.getElementById("uploadFile").value = this.value;

};

</script>

