<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
      background-color: #555;
    color: white;
    padding: 16px 20px;
     border: none; 
    cursor: pointer;
    opacity: 0.8;
    position: fixed;
     bottom: 23px; 
    right: 28px;
     width: 280px; 
}

/* The popup form - hidden by default */
.form-popup {
 

}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
.border_box{
   border: 2px solid grey;
    padding: 60px;
    border: 1px solid #e2e2e2;
    margin-top: 55;
}
}

}
</style>
</head>
<body>



<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">
    <img src="bird.jpg" alt="logo" style="width:40px;">
  </a>
  
  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#">Link 1</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link 2</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Link 3</a>
    </li>
  </ul>
</nav>





<div class="container">
  
    <div class="row">
      <div class="col-md-12 border_box"> 


<div class="col-md-8 ">
<h2>Question And Answer</h2>

</div>
<div class="col-md-8">
<br>
<h5>Question 1: Which file can you configure to ensure that certain file types are never committed to the local Git repository?</h5>
</div>
<div class="col-md-8">
<h5> Answer : </h5>
<textarea rows="4" cols="50">
At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.
</textarea>
</div>


<div class="col-md-8">
<br>
<h5>Question 2: Which file can you configure to ensure that certain file types are never committed to the local Git repository?</h5>
</div>
<div class="col-md-8">
<h5> Answer : </h5>
<textarea rows="4" cols="50">
At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.
</textarea>
</div>

      </div>
	 </div>

	<div class="form-popup" >
    <center>
  <form action="<?php echo base_url() ?>query/query_insert" method="post" class="form-container">
    <h1>Query Form</h1>

    <label for="email"><strong>Category</strong></label>
	 <select class="form-control" name="Category">
        <option value="git">Git</option>
        <option value="react">React</option>
        <option value="nodejs">NodeJs</option>
        <option value="laravel">Laravel</option>
      </select>

      <label for="psw"><b>Question</b></label>
    <input type="text" placeholder="Question" name="Question" required>

    <label for="email"><b>Answer</b></label>
    <textarea name="Answer" rows="5" cols="30">Answer</textarea>

    

    <button type="submit" class="btn">Submit</button>
    <!-- <button type="button" class="btn cancel" onclick="closeForm()">Close</button> -->
  </form>
</center>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-8">
  <h2>Billing Profile</h2>
  <br>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">Personal Details</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">Billing Details</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2">Virtual Bank Account Details</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
      <h3>Personal Details</h3>


      <form>

  <div class="form-group">
     <div class="row">    
    <label for="exampleInputEmail1" class="col-md-4">Email address</label>
    <div class="col-md-8">
    <input type="email" class="form-control"placeholder="Enter email">
    </div>
   
   </div>
  </div>


  <div class="form-group">
    <div class="row"> 
    <label for="exampleInputEmail1" class="col-md-4">First Name</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="First Name">
  </div>
</div>
</div>

  <div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">Last Name</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="Last Name">
  </div>
</div>
</div>
  
  <button type="submit" class="btn btn-primary">Update</button>
</form>


    </div>


    <div id="menu1" class="container tab-pane fade"><br>
      <h3>Billing Details</h3>

      <div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">Company Name :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="Company Name">
  </div>
</div>
</div>

<div class="form-group">
     <div class="row">
    <label for="exampleFormControlSelect1" class="col-md-4">Customer Type :</label>
    <div class="col-md-8">
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Representing Organisation</option>
      <option>Representing Organisation</option>
      <option>Representing Organisation</option>
      <option>Representing Organisation</option>
      <option>Representing Organisation</option>
    </select>
  </div>
</div>
</div>

<div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">GST :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="07AADCE2687JKH">
  </div>
</div>
</div>

<div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">PAN :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="PAN">
  </div>
</div>
</div>


<div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">Phone :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="9019985221">
  </div>
</div>
</div>

<div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">Address :</label>
    <div class="col-md-8">
    <textarea class="form-control" rows="3"></textarea>
  </div>
</div>
</div>

<div class="form-group">
     <div class="row">
    <label for="exampleFormControlSelect1" class="col-md-4">Customer Type :</label>
    <div class="col-md-8">
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Delhi</option>
      <option>Delhi</option>
      <option>Delhi</option>
      <option>Delhi </option>
      <option>Delhi </option>
    </select>
  </div>
</div>
</div>

<div class="form-group">
     <div class="row">
    <label for="exampleInputEmail1" class="col-md-4">Pin Code :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" placeholder="Pin Code">
  </div>
</div>
</div>



    </div>



    <div id="menu2" class="container tab-pane fade" style="border:1px solid   #F5F5F5;"><br>
      <h3>Bank Account Details</h3>
      
        <div class= "col-md-12" style="background-color:lavender;" >
          <p>These Virtual bank account detail are unique for your MyAccount.</p>
          <p>These Virtual bank account detail are unique for your MyAccount.</p>
          <p>These Virtual bank account detail are unique for your MyAccount.</p>
            <p>These Virtual bank account detail are unique for your MyAccount.</p>
        </div>
       <table class="table table-bordered">
   
      <tr>
        <th>Bank Account No.</th>
        <th>22652367567</th>
      </tr>

      <tr>
        <th>Beneficiary Name</th>
        <th>E2E Networks</th>
      </tr>

      <tr>
        <th>IFSC Code</th>
        <th>YESDGKOF</th>
      </tr>
   
    
  </table>

    </div>
  </div>
</div>
</div>
</div>

</div>

</center>



</body>
</html>
