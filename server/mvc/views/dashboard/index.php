<div class="row page-titles dashboardtitle-background">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-dashboard"></i>   Dashboard</h3>
   </div>
   <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
         <li class="breadcrumb-item active">Dashboard</li>
      </ol>
   </div>
</div>
<div class="container-fluid">
<!-- /row 1-->
<?php 
   $usertype = $this->session->userdata("usertype");
   if( $usertype == "Academic" || $usertype == "Teacher" ||$usertype == "Accountant" ||$usertype == "Super_A_P" || $usertype == "Admin_btech" || $usertype == "Support") {
   ?>
<div class="row">
   <div class="col-md-12">
      <div class="row page-titles">
         <div class="col-md-4">
            <?php
               foreach($recent_notice as $rows)
                {
               ?>
            <h4 class="text-themecolor" style="color: red; ">Recent Notice:
               <span style="color: black"><?php echo $rows->title; ?></span>
               <span><?php echo btn_view('notice/view/'. $rows->noticeID, $this->lang->line('view')); ?></span>
            </h4>
         </div>
         <div class="col-md-8">
            <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
               <span style="color: blue"><?php echo $rows->notice; ?></span>
               </a>
               <?php    
                  } 
                  ?>  
            </marquee>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<?php
   $usertype = $this->session->userdata("usertype");
   
   if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Accountant"  || $usertype == "Academic" || $usertype == "Admin_btech") {
   if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support"  || $usertype == "Academic" || $usertype == "Admin_btech") {
    ?>

    <div class="container-fluid">
<div class="row">
   <div class="col-md-6 col-sm-6">
      <a href="<?=base_url('student')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption focus">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon  fa fa-user"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail student-back">
                        <h3 class="cl-info"> <?php echo $all_count; ?></h3>
                        <span>Total Students</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>

  <?php
    if($usertype == "ClgAdmin" || $usertype == "superadmin") {
  ?>
  <div class="row">
   
   <div class="row">
      <div class="col-md-6 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <script type="text/javascript">
                           $.ajax({
                           
                           type: 'GET',
                           
                           dataType: "json",
                           
                           url: "<?=base_url('dashboard/paymentscall')?>",
                           
                           dataType: "html",
                           
                           success: function(data) {
                           
                           var response = jQuery.parseJSON(data);
                           
                           $('#fpaid').html(response.fpaid);
                           
                           $('#npaid').html(response.npaid);
                           
                           $('#ppaid').html(response.ppaid);
                           
                           }
                           
                           });
                           
                        </script> 
                        <h3 class="cl-info"style="color: #9c1855;"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount ?></h3>
                        <span>Total Revenue</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div></div></div>
      <div class="col-md-6 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-money" aria-hidden="true" style="background: #007bff;color: #ffffff"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail paymentpaid-back">
                        <h3 class="cl-warning" style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->Totalpaidamount ?></h3>
                        <span>Total Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%; background: #007bff;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-ioxhost"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail totalPayment-back">
                        <h3 class="cl-warning"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount-$payment_amount->Totalpaidamount; ?></h3>
                        <span>Due Amount</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:60%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php }?>
   <?php } ?>
   <!-- /row -->
   <!--  Acadmic row  -->
   <?php 
      if( $usertype == "Academic" ) {
          ?>
   <div class="row">
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('subject')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-book"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back">
                           <h3 class="cl-info"> <?=$ActiveSubjects_count?></h3>
                           <span>Total Subjects</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-4 col-sm-6">
         <!-- <a href="<?=base_url('professor')?>"> -->
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption danger">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-list-alt"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail teacher-back">
                        <h3 class="cl-danger"> <?=0?></h3>
                        <span>Total Results</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-danger">
                        <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--  </a> -->
      </div>
   </div>
   <div class="row page-titles">
      <div class="col-md-12 col-sm-6">
         <div class="table-responsive">
            <div class="table-wrapper">
               <table class="table table-striped table-hover ">
                  <thead>
                     <tr>
                        <th>S.N.</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Notice</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        if(count($notices)) 
                        {
                          $i = 1; 
                          foreach($notices as $notice) 
                          { 
                        ?>
                     <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $notice->title; ?></td>
                        <td><?php echo $notice->date; ?></td>
                        <td><?php echo substr($notice->notice,0,20); ?></td>
                        <!--  <td><?php echo btn_view('dashboard/index',$this->lang->line('view')); ?></td> -->
                        <td><?php echo btn_view('notice/view/'.$notice->noticeID, $this->lang->line('view')); ?></td>
                     </tr>
                     <?php 
                        $i++; 
                        }
                        } 
                        ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <!--  Acadmic row end  -->
   
   
  
   <script type="text/javascript">
      $.ajax({
      
      
      
         type: 'GET',
      
      
      
         dataType: "json",
      
      
      
         url: "<?=base_url('dashboard/graphcall')?>",
      
      
      
         dataType: "html",
      
      
      
         success: function(data) {
      
      
      
      var response = jQuery.parseJSON(data);
      
      
      
      var chart = Highcharts.chart('container', {
      
      
      
      title: {
      
       text: 'Earning Graph'
      
      },
      
      
      
      subtitle: {
      
       text: 'Plain'
      
      },
      
      
      
      xAxis: {
      
       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      
      },
      
      
      
      series: [{
      
       type: 'column',
      
       colorByPoint: true,
      
       data: response.balance,
      
       showInLegend: false
      
      }]
      
      
      
      });
      
      }
      
      }); 
      
      $('#plain').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: false,
      
         polar: false
      
       },
      
       subtitle: {
      
         text: 'Plain'
      
       }
      
      });
      
      });
      
      
      
      $('#inverted').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: true,
      
         polar: false
      
       },
      
       subtitle: {
      
         text: 'Inverted'
      
       }
      
      });
      
      });
      
      
      
      $('#polar').click(function () {
      
      chart.update({
      
       chart: {
      
         inverted: false,
      
         polar: true
      
       },
      
       subtitle: {
      
         text: 'Polar'
      
       }
      
      });
      
      });
      
      
      
   </script>  
   <script type="text/javascript">
      $.ajax({
      
      
      
          type: 'GET',
      
      
      
          dataType: "json",
      
      
      
          url: "<?=base_url('dashboard/paymentscall')?>",
      
      
      
          dataType: "html",
      
      
      
          success: function(data) {
      
      
      
            var response = jQuery.parseJSON(data);
      
      
      
      
      
            npaid = response.npaid;
      
      
      
            ppaid = response.ppaid;
      
      
      
            fpaid = response.fpaid;
      
      
      
            cash = response.cash;
      
      
      
            cheque = response.cheque;
      
      
      
            paypal = response.paypal;
      
      
      
            stripe = response.stripe;
      
      
      
            PayuMoney = response.PayuMoney;
      
      
      
            OnlinePayment = response.OnlinePayment;
      
      
      
            Bank = response.Bank;
      
      
      
      //payment status
      
      // Build the chart
      
      Highcharts.chart('pichartPaymentStatus', {
      
      chart: {
      
      plotBackgroundColor: null,
      
      plotBorderWidth: null,
      
      plotShadow: false,
      
      type: 'pie'
      
      },
      
      title: {
      
      text: 'Payment Status Graph'
      
      },
      
      tooltip: {
      
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      
      },
      
      plotOptions: {
      
      pie: {
      
        allowPointSelect: true,
      
        cursor: 'pointer',
      
        dataLabels: {
      
          enabled: true,
      
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
      
          style: {
      
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
      
          },
      
          connectorColor: 'silver'
      
        }
      
      }
      
      },
      
      series: [{
      
      name: 'Payment',
      
      data: [
      
        { name: 'Partial Paid', y:ppaid},
      
        { name: 'Not paid', y:npaid },
      
        { name: 'Full paid', y:fpaid }
      
      ]
      
      }]
      
      });
      
      
      
      // payment type
      
      
      
      Highcharts.chart('piecharthichart', {
      
      chart: {
      
      plotBackgroundColor: null,
      
      plotBorderWidth: null,
      
      plotShadow: false,
      
      type: 'pie'
      
      },
      
      title: {
      
      text: 'Payment Type Graph'
      
      },
      
      tooltip: {
      
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      
      },
      
      plotOptions: {
      
      pie: {
      
        allowPointSelect: true,
      
        cursor: 'pointer',
      
        dataLabels: {
      
          enabled: true,
      
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
      
          style: {
      
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
      
          }
      
        }
      
      }
      
      },
      
      series: [{
      
      name: 'Payment',
      
      colorByPoint: true,
      
      data: [{
      
        name: 'Cash',
      
        y: cash,
      
        sliced: true,
      
        selected: true
      
      },{
      
        name: 'Cheque',
      
        y: cheque
      
      }, {
      
        name: 'paypal',
      
        y: paypal
      
      },{
      
        name: 'PayUmoney',
      
        y: PayuMoney
      
      },
      
      {
      
        name: 'Bank',
      
        y: Bank
      
      },
      
      {
      
        name: 'Online Payment',
      
        y: OnlinePayment
      
      },
      
       {
      
        name: 'stripe',
      
        y: stripe
      
      }]
      
      }]
      
      });
      
      
      
      
      
      
      
      
      
      }
      
      }); 
      
   </script>
   <?php } ?>
   <!-- end admin dashbord -->
   <!-- Professor Dashboard -->
   <?php
      $usertype = $this->session->userdata("usertype");
      if($usertype == "Professor") {
      ?>
   <div class="row">
      <div class="col-md-3 col-sm-6">
         <!-- <a href="<?=base_url('student')?>"> -->
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-graduation-cap"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail student-back">
                        <h3 class="cl-info"> <?=count($student)?></h3>
                        <span>Total <?=$this->lang->line("menu_student")?></span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- </a> -->
      </div>
      <div class="col-md-3 col-sm-6">
         <!-- <a href="<?=base_url('classes')?>"> -->
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-book" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail course-back">
                        <h3 class="cl-warning"><?=count($courses)?></h3>
                        <span>Total Courses</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- </a> -->
      </div>
      <div class="col-md-3 col-sm-6">
         <!-- <a href="<?php echo base_url() ?>subject/index"> -->
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption success">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-book"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail user-back">
                        <h3 class="cl-success"><?php echo count($subject) ?></h3>
                        <span>Total Subjects</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- </a> -->
      </div>
      <div class="col-md-3 col-sm-6">
         <!-- <a href="<?php echo base_url() ?>subject/index"> -->
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption danger">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-users"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail user-back">
                        <h3 class="cl-success"><?php echo "3"; ?></h3>
                        <span>Today's Lecture</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-success">
                        <span style="width:60%;" class="bg-success widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- </a> -->
      </div>
   </div>
   <!-- /row -->
   <!-- second -->
   <!-- <div class="row page-titles">
      <div class="col-md-5 align-self-center">
      
        <h3 class="text-themecolor title-font-size">Earning Graph </h3>
      
      </div>
      
      </div> -->
   <?php } ?>
   <!-- End Professor Dashboard -->
   <!-- teachers dashbord -->
   <?php
      $usertype = $this->session->userdata("usertype");
      if($usertype == "Teacher" ) {
      ?>
   <!-- row 2 student -->
   <div class="row">
      <?php if($teacher->spa == 0){?>
      <div class="col-md-6 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-user"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back">
                           <h3 class="cl-info"> <?=$all_count?></h3>
                           <span>Total Students</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-6 col-sm-6">
         <a href="<?=base_url('classes')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-book" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back">
                           <h3 class="cl-warning"><?=count($courses)?></h3>
                           <span>Total Courses</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <!-- /row -->
   <div class="row">
      <div class="col-md-6 col-sm-6">
         <a href="<?=base_url()?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption warning">
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail course-back" style="
                           text-align: left; margin-left: 20px;" >
                           <h4 class="cl-info cl-info_h4">Relationship Manager</h4>
                           <span><?php echo $rm->name ?></span>
                           <h4 class="cl-info cl-info_h4">Email</h4>
                           <span><?php echo $rm->email ?></span>
                           <h4 class="cl-info cl-info_h4">Mobile</h4>
                           <span><?php echo $rm->phone ?></span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-6 col-sm-6">
         <a href="<?=base_url()?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <style type="text/css">
                        .cl-info_h4{
                        margin:0px;
                        align-content: center;
                        }
                     </style>
                     <div class="col-xs-12 no-pad">
                        <div class="widget-detail student-back" style="
                           text-align: left; margin-left: 20px;"     
                           >
                           <h4 class="cl-info cl-info_h4">Academic Contact</h4>
                           <span><?php echo $cpa->name ?></span>
                           <h4 class="cl-info cl-info_h4">Email</h4>
                           <span><?php echo $cpa->email ?></span>
                           <h4 class="cl-info cl-info_h4">Mobile</h4>
                           <span><?php echo $cpa->phone ?></span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <script type="text/javascript">
                           $.ajax({
                           
                           type: 'GET',
                           
                           dataType: "json",
                           
                           url: "<?=base_url('dashboard/paymentscall')?>",
                           
                           dataType: "html",
                           
                           success: function(data) {
                           
                           var response = jQuery.parseJSON(data);
                           
                           $('#fpaid').html(response.fpaid);
                           
                           $('#npaid').html(response.npaid);
                           
                           $('#ppaid').html(response.ppaid);
                           
                           }
                           
                           });
                           
                        </script> 
                        <h3 class="cl-info" style="color: #9c1855;"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount ?></h3>
                        <span>Total Revenue</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-money" aria-hidden="true" style="background: #007bff;color: #ffffff"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail paymentpaid-back">
                        <h3 class="cl-warning" style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $payment_amount->Totalpaidamount ?></h3>
                        <span>Total Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%; background: #007bff;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-ioxhost"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail totalPayment-back">
                        <h3 class="cl-warning"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount-$payment_amount->Totalpaidamount; ?></h3>
                        <span>Due Amount</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:60%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php }
      else{
      ?>
   <div class="col-md-6 col-sm-6">
      <a href="<?=base_url('student')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-graduation-cap"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail student-back">
                        <h3 class="cl-info"> <?=$all_count?></h3>
                        <span>Total <?=$this->lang->line("menu_student")?></span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-6 col-sm-6">
      <a href="<?=base_url('classes')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-book" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail course-back">
                        <h3 class="cl-warning"><?=count($courses)?></h3>
                        <span>Total Courses</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <!-- /row -->
   <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
         <div class="card simple-card" style="height: 335px;">
            <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
            </div>
            <div class="avatar">
               <a href="#">
               <?=img(base_url('uploads/images/'.$Super->photo))?>
               </a>
            </div>
            <div class="info1">
               <div class="title">
                  <h3>Super Academic Partner</h3>
               </div>
               <!-- <p class="desc">Designer, Developer, Photographer</p> -->
            </div>
            <div class="bottom admin_dash">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;User name</p>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><?php echo $Super->name ?></p>
               </div>
            </div>
            <div class="bottom">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><?php echo $Super->email ?></p>
               </div>
            </div>
            <div class="bottom">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><?php echo $Super->phone ?></p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
         <div class="card simple-card" style="height: 335px;">
            <div class="cardheader" style="background:url(../assets/img/card-bg-3.jpg);">
            </div>
            <div class="avatar">
               <a href="#">
               <?=img(base_url('uploads/images/'.$cpa->photo))?>
               </a>
            </div>
            <div class="info1">
               <div class="title">
                  <h3>Academic Contact</h3>
               </div>
               <!-- <p class="desc">Designer, Developer, Photographer</p> -->
            </div>
            <div class="bottom admin_dash">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><i class="glyphicon glyphicon-user text-maroon-light"></i>&nbsp;Name</p>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><?php echo $cpa->name ?></p>
               </div>
            </div>
            <div class="bottom">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><i class="fa fa-envelope text-maroon-light"></i>&nbsp;E-mail</p>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><?php echo $cpa->email ?></p>
               </div>
            </div>
            <div class="bottom">
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><i class="fa fa-phone text-maroon-light"></i>&nbsp;Phone</p>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-6">
                  <p class="left"><?php echo $cpa->phone ?> </p>
               </div>
               <!--             <div class="box-main-footer clearfix">
                  <a href="<?php echo base_url() ?>profile/index" onload="myfunction()" class="btn btn-sm btn-view pull-right" data-toggle="popover" style="color:white">View Profile</a>
                  </div> -->
            </div>
         </div>
      </div>
   </div>
   <br>
   <div class="row">
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-line-chart" style="background: #9c1855;"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <script type="text/javascript">
                           $.ajax({
                           
                           type: 'GET',
                           
                           dataType: "json",
                           
                           url: "<?=base_url('dashboard/paymentscall')?>",
                           
                           dataType: "html",
                           
                           success: function(data) {
                           
                           var response = jQuery.parseJSON(data);
                           
                           $('#fpaid').html(response.fpaid);
                           
                           $('#npaid').html(response.npaid);
                           
                           $('#ppaid').html(response.ppaid);
                           
                           }
                           
                           });
                           
                        </script> 
                        <h3 class="cl-info" style="color: #9c1855;"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount ?></h3>
                        <span>Total Revenue</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 fpaid">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-money" aria-hidden="true" style="background: #007bff;color: #ffffff"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail paymentpaid-back">
                        <h3 class="cl-warning" style="color: #007bff"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $payment_amount->Totalpaidamount ?></h3>
                        <span>Total Paid</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%; background: #007bff;" class="widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-ioxhost"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail totalPayment-back">
                        <h3 class="cl-warning"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $payment_amount->TotalAmount-$payment_amount->Totalpaidamount; ?></h3>
                        <span>Due Amount</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:60%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <style type="text/css">
      .cl-info_h4{
      margin:0px;
      align-content: center;
      }
   </style>
   <?php }?>  
   <div class="row page-titles">
      <div class="col-md-12 col-sm-6">
         <div class="table-responsive">
            <div class="table-wrapper">
               <table class="table table-striped table-hover ">
                  <thead>
                     <tr>
                        <th>S.N.</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Notice</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        if(count($notices)) 
                        {
                          $i = 1; 
                          foreach($notices as $notice) 
                          { 
                        ?>
                     <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $notice->title; ?></td>
                        <td><?php echo $notice->date; ?></td>
                        <td><?php echo substr($notice->notice,0,20); ?></td>
                        <!--  <td><?php echo btn_view('dashboard/index',$this->lang->line('view')); ?></td> -->
                        <td><?php echo btn_view('notice/view/'.$notice->noticeID, $this->lang->line('view')); ?></td>
                     </tr>
                     <?php 
                        $i++; 
                        }
                        } 
                        ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /row -->
   <?php }?>
   <!-- --------------------Super Academic Pratner -------------------------------- -->
   <?php
      $usertype = $this->session->userdata("usertype");
      if( $usertype == "Super_A_P") {
      ?>
   <!-- row 2 student -->
   <div class="row">
      <div class="col-md-6 col-sm-6">
         <a href="<?=base_url('student')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption info">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon fa fa-user"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail student-back">
                           <h3 class="cl-info"> <?=$all_count?></h3>
                           <span>Total <?=$this->lang->line("menu_student")?></span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-info">
                           <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <div class="col-md-6 col-sm-6">
         <a href="<?=base_url('teacher')?>">
            <div class="widget smart-standard-widget">
               <div class="row">
                  <div class="widget-caption danger">
                     <div class="col-xs-4 no-pad zoom">
                        <i class="icon ti ti-id-badge" aria-hidden="true"></i>
                     </div>
                     <div class="col-xs-8 no-pad">
                        <div class="widget-detail course-back">
                           <h3 class="cl-danger"><?=$teacher?></h3>
                           <span>Total Academic Partner</span>
                        </div>
                     </div>
                     <div class="col-xs-12">
                        <div class="widget-line bg-warning">
                           <span style="width:55%;" class="bg-danger widget-horigental-line"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
      <!-- <div class="col-md-4 col-sm-6">
         <a href="<?=base_url('classes')?>">
           <div class="widget smart-standard-widget">
             <div class="row">
               <div class="widget-caption warning">
                 <div class="col-xs-4 no-pad zoom">
                 <i class="icon fa fa-book" aria-hidden="true"></i>
                 </div>
                 <div class="col-xs-8 no-pad">
                 <div class="widget-detail course-back">
                 <h3 class="cl-warning"><?=$courses?></h3>
                 <span>Total Courses</span>
                 </div>
                 </div>
                 <div class="col-xs-12">
                 <div class="widget-line bg-warning">
                 <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                 </div>
                 </div>
               </div>
             </div>
           </div>
         </a>
         </div> -->
   </div>
</div>
<!-- super  A P profile  -->
<div class="row">
   <div class="col-md-12">
      <div class="box-main border-red">
         <div class="box-main-header">
            <h3>My Profile</h3>
         </div>
         <div class="box-main-body my-profile">
            <div class="profile-head">
               <div class="profile-head-content">
                  <img src="<?=base_url("uploads/images/".$this->session->userdata('photo')); ?>" class="img-responsive img-circle"  alt="user" />
                  <p><strong><?=$Profile->name?></strong></p>
               </div>
            </div>
            <div class="profile-body fading">
               <h2><?=$this->lang->line("personal_information")?></h2>
               <hr/>
               <table class="pull-left text-left">
                  <tr>
                     <td width="15%" class="bold_td"><?=$this->lang->line("student_email")?></td>
                     <td width="5%">:</td>
                     <td width="30%"><?=$Profile->email?></td>
                  </tr>
                  <tr>
                     <td width="15%" class="bold_td"><?=$this->lang->line("student_sex")?></td>
                     <td width="5%">:</td>
                     <td width="30%"> <?=$Profile->sex?></td>
                  </tr>
               </table>
               <table class="pull-right text-left width40">
                  <tr>
                     <td width="15%" class="bold_td"><?=$this->lang->line("student_phone")?></td>
                     <td width="5%">:</td>
                     <td width="30%">
                        <?php if ($Profile->phone!='') 
                           {
                             echo $Profile->phone;
                           }
                           else
                           {
                             echo "--";
                           }
                           ?>
                     </td>
                  </tr>
                  <tr>
                     <td width="15%" class="bold_td">User Type</td>
                     <td width="5%">:</td>
                     <td width="30%">
                        <?php if($Profile->usertype == 'Super_A_P'){
                           echo 'Super Academic Partner';
                           } ?>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
         <div class="box-main-footer clearfix">
            <a href="<?php echo base_url() ?>profile/index" onload="myfunction()" class="btn btn-sm btn-view pull-right" data-toggle="popover">View Profile</a>
         </div>
      </div>
   </div>
</div>
<!-- Notic -->
<?php 
   $usertype = $this->session->userdata('usertype');
         if ( $usertype == "Accountant" ||  $usertype =="Super_A_P" ||  $usertype =="Academic" ) { ?>
<div class="row page-titles">
   <div class="col-md-12 col-sm-6">
      <div class="table-responsive">
         <div class="table-wrapper">
            <table class="table table-striped table-hover ">
               <thead>
                  <tr>
                     <th>S.N.</th>
                     <th>Title</th>
                     <th>Date</th>
                     <th>Notice</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php 
                     if(count($notices)) 
                     {
                       $i = 1; 
                       foreach($notices as $notice) 
                       { 
                     ?>
                  <tr>
                     <td><?php echo $i; ?></td>
                     <td><?php echo $notice->title; ?></td>
                     <td><?php echo $notice->date; ?></td>
                     <td><?php echo substr($notice->notice,0,20); ?></td>
                     <!--  <td><?php echo btn_view('dashboard/index',$this->lang->line('view')); ?></td> -->
                     <td><?php echo btn_view('notice/view/'.$notice->noticeID, $this->lang->line('view')); ?></td>
                  </tr>
                  <?php 
                     $i++; 
                     }
                     } 
                     ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<div class="row page-titles">
   <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor title-font-size">Earning Graph </h3>
   </div>
</div>
<div class="row">
   <div class="col-md-6 col-sm-12">
      <div class="card" style="margin-left: 0px;">
         <div class="card-body">
            <div id="pichartPaymentStatus" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
         </div>
      </div>
   </div>
   <div class="col-md-6 col-sm-12">
      <div class="card" style="margin-left: 0px;">
         <div class="card-body">
            <div id="piecharthichart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
</div>
<div class="row">
   <div class="col-xs-12" style="margin-top:3%;">
      <div class="card" style="margin-left: 0px;">
         <div class="card-body">
            <div id="container"></div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
</div>
<script type="text/javascript">
   $.ajax({
   
      type: 'GET',
   
      dataType: "json",
   
      url: "<?=base_url('dashboard/graphcall')?>",
   
      dataType: "html",
   
      success: function(data) {
   
   var response = jQuery.parseJSON(data);
   
   var chart = Highcharts.chart('container', {
   
   title: {
   
    text: 'Earning Graph'
   
   },
   
   subtitle: {
   
    text: 'Plain'
   
   },
   
   
   xAxis: {
   
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
   
   },
   
   series: [{
   
    type: 'column',
   
    colorByPoint: true,
   
    data: response.balance,
   
    showInLegend: false
   
   }]
   
   });
   
   }
   
   }); 
   
   $('#plain').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Plain'
   
    }
   
   });
   
   });
   
   
   
   $('#inverted').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: true,
   
      polar: false
   
    },
   
    subtitle: {
   
      text: 'Inverted'
   
    }
   
   });
   
   });
   
   
   
   $('#polar').click(function () {
   
   chart.update({
   
    chart: {
   
      inverted: false,
   
      polar: true
   
    },
   
    subtitle: {
   
      text: 'Polar'
   
    }
   
   });
   
   });
   
</script>  
<script type="text/javascript">
   $.ajax({
   
   
   
       type: 'GET',
   
   
   
       dataType: "json",
   
   
   
       url: "<?=base_url('dashboard/paymentscall')?>",
   
   
   
       dataType: "html",
   
   
   
       success: function(data) {
   
   
   
         var response = jQuery.parseJSON(data);
   
   
   
   
   
         npaid = response.npaid;
   
   
   
         ppaid = response.ppaid;
   
   
   
         fpaid = response.fpaid;
   
   
   
         cash = response.cash;
   
   
   
         cheque = response.cheque;
   
   
   
         paypal = response.paypal;
   
   
   
         stripe = response.stripe;
   
   
   
         PayuMoney = response.PayuMoney;
   
   
   
         OnlinePayment = response.OnlinePayment;
   
   
   
         Bank = response.Bank;
   
   
   
   //payment status
   
   // Build the chart
   
   Highcharts.chart('pichartPaymentStatus', {
   
   chart: {
   
   plotBackgroundColor: null,
   
   plotBorderWidth: null,
   
   plotShadow: false,
   
   type: 'pie'
   
   },
   
   title: {
   
   text: 'Payment Status Graph'
   
   },
   
   tooltip: {
   
   pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
   pie: {
   
     allowPointSelect: true,
   
     cursor: 'pointer',
   
     dataLabels: {
   
       enabled: true,
   
       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
       style: {
   
         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
       },
   
       connectorColor: 'silver'
   
     }
   
   }
   
   },
   
   series: [{
   
   name: 'Payment',
   
   data: [
   
     { name: 'Partial Paid', y:ppaid},
   
     { name: 'Not paid', y:npaid },
   
     { name: 'Full paid', y:fpaid }
   
   ]
   
   }]
   
   });
   
   
   
   // payment type
   
   
   
   Highcharts.chart('piecharthichart', {
   
   chart: {
   
   plotBackgroundColor: null,
   
   plotBorderWidth: null,
   
   plotShadow: false,
   
   type: 'pie'
   
   },
   
   title: {
   
   text: 'Payment Type Graph'
   
   },
   
   tooltip: {
   
   pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   
   },
   
   plotOptions: {
   
   pie: {
   
     allowPointSelect: true,
   
     cursor: 'pointer',
   
     dataLabels: {
   
       enabled: true,
   
       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
   
       style: {
   
         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
   
       }
   
     }
   
   }
   
   },
   
   series: [{
   
   name: 'Payment',
   
   colorByPoint: true,
   
   data: [{
   
     name: 'Cash',
   
     y: cash,
   
     sliced: true,
   
     selected: true
   
   },{
   
     name: 'Cheque',
   
     y: cheque
   
   }, {
   
     name: 'paypal',
   
     y: paypal
   
   },{
   
     name: 'PayUmoney',
   
     y: PayuMoney
   
   },
   
   {
   
     name: 'Bank',
   
     y: Bank
   
   },
   
   {
   
     name: 'Online Payment',
   
     y: OnlinePayment
   
   },
   
    {
   
     name: 'stripe',
   
     y: stripe
   
   }]
   
   }]
   
   });
   
   
   
   
   
   
   
   
   
   }
   
   }); 
   
</script>
<?php } ?>
<!-- end teacher dashbord -->
<!-- student dashbord -->  
<?php
   $usertype = $this->session->userdata("usertype");
   
   if($usertype == "Student") {
   
   ?>
<div class="row">
   <div class="col-md-4 col-sm-6" >
      <a href="<?=base_url('subject')?>">
         <div class="widget smart-standard-widget">
            <!-- <div class="row" data-step="2" data-intro="Ok, wasn't that fun?" data-position='right'> -->
            <div class="row">
               <div class="widget-caption info">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-book" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <h3 class="cl-info"> <?=count($subject)?></h3>
                        <span>Total <?=$this->lang->line("menu_subject")?></span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <div class="col-md-4 col-sm-6">
      <!-- <a href="<?=base_url('exam/exam_list/1')?>"> -->
      <div class="widget smart-standard-widget">
         <div class="row">
            <div class="widget-caption danger">
               <div class="col-xs-4 no-pad zoom">
                  <i class="icon ti ti-ruler-pencil"></i>
               </div>
               <div class="col-xs-8 no-pad">
                  <div class="widget-detail">
                     <!-- <h3 class="cl-danger"><?php echo $Examtime ?></h3> -->
                     <h3 class="cl-danger">Dec 2019</h3>
                     <span>Exam Date</span>
                  </div>
               </div>
               <div class="col-xs-12">
                  <div class="widget-line bg-danger">
                     <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--  </a> -->
   </div>
   <div class="col-md-4 col-sm-6">
      <a href="<?=base_url('mark/view')?>">
         <div class="widget smart-standard-widget">
            <div class="row">
               <div class="widget-caption warning">
                  <div class="col-xs-4 no-pad zoom">
                     <i class="icon fa fa-flask" aria-hidden="true"></i>
                  </div>
                  <div class="col-xs-8 no-pad">
                     <div class="widget-detail">
                        <!--  <h3 class="cl-warning"><?=$exams ?></h3> -->
                        <h3 class="cl-warning"> 0 </h3>
                        <span>Total Results Out</span>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </a>
   </div>
   <!-- /row -->
   <!-- row 2 student -->
   <div class="row">
      <div class="col-md-12">
         <div class="row page-titles">
            <div class="col-md-2 align-self-center">
               <h4 class="text-themecolor title-font-size" style="color: red;
                  float: left;">Recent Notice</h4>
            </div>
            <div class="col-md-10 align-self-center">
               <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                  <?php
                     foreach($recent_notice as $rows)
                      {
                     ?>
                  <a href="<?php echo base_url(); ?>notice/view/<?php echo $rows->noticeID; ?>">
                  <span style="color: blue"><?php echo $rows->title; ?></span>
                  </a>
                  <?php    
                     } 
                     ?> 
               </marquee>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="box-main border-red">
            <div class="box-main-header">
               <h3>My Profile</h3>
            </div>
            <div class="box-main-body my-profile">
               <div class="profile-head">
                  <div class="profile-head-content">
                     <img src="<?=base_url("uploads/images/".$this->session->userdata('photo')); ?>" class="img-responsive img-circle"  alt="user" />
                     <p><strong><?=$student->name?></strong></p>
                     <p class="pdt0">
                        <?=$class->classes?>
                        <!-- <?php
                           if (isset($sub_courses->sub_course)) {
                               echo  "<br>";
                             echo '('.$sub_courses->sub_course.')';
                           }
                           ?>  -->                              
                     </p>
                  </div>
               </div>
               <div class="profile-body fading">
                  <h2><?=$this->lang->line("personal_information")?></h2>
                  <hr/>
                  <table class="pull-left text-left">
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_roll")?></td>
                        <td width="5%">:</td>
                        <td width="30%"><?=$student->roll?></td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_email")?></td>
                        <td width="5%">:</td>
                        <td width="30%"><?=$student->email?></td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_sex")?></td>
                        <td width="5%">:</td>
                        <td width="30%"> <?=$student->sex?></td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("session")?></td>
                        <td width="5%">:</td>
                        <td width="30%"> <?=$student->session?></td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td">Session Type</td>
                        <td width="5%">:</td>
                        <td width="30%"> <?=$student->sessionType?></td>
                     </tr>
                  </table>
                  <table class="pull-right text-left width40">
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("father_name")?></td>
                        <td width="5%">:</td>
                        <td width="30%">
                           <?php if ($student->father_name!='') 
                              {
                                echo $student->father_name;
                              }
                              else
                              {
                                echo "--";
                              }
                               ?> 
                        </td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("mother_name")?></td>
                        <td width="5%">:</td>
                        <td width="30%">
                           <?php if ($student->mother_name!='') 
                              {
                                echo $student->mother_name;
                              }
                              else
                              {
                                echo "--";
                              }
                              ?>
                        </td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("student_phone")?></td>
                        <td width="5%">:</td>
                        <td width="30%">
                           <?php if ($student->phone!='') 
                              {
                              echo $student->phone;
                              }
                              else
                              {
                                echo "--";
                              }
                              ?>
                        </td>
                     </tr>
                     <tr>
                        <td width="15%" class="bold_td"><?=$this->lang->line("Semester_Year")?></td>
                        <td width="5%">:</td>
                        <td width="30%"> <?=$student->yearsOrSemester?></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="box-main-footer clearfix">
               <!--  <a data-step="1" data-intro="This is Your Profile you can use the view profile!" data-position='top' href="<?php echo base_url() ?>profile/index" onload="myfunction()" class="btn btn-sm btn-view pull-right" data-toggle="popover">View Profile</a> -->
               <a href="<?php echo base_url() ?>profile/index" onload="myfunction()" class="btn btn-sm btn-view pull-right" data-toggle="popover">View Profile</a>
            </div>
         </div>
      </div>
   </div>
   <!-- rau notice start --> <?php 
      if ( $usertype == "Student") { ?>
   <div class="row page-titles">
      <div class="col-md-12 col-sm-6">
         <div class="table-responsive">
            <div class="table-wrapper">
               <table class="table table-striped table-hover ">
                  <thead>
                     <tr>
                        <th>S.N.</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Notice</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
                        if(count($notices)) 
                        {
                          $i = 1; 
                          foreach($notices as $notice) 
                          { 
                        ?>
                     <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $notice->title; ?></td>
                        <td><?php echo $notice->date; ?></td>
                        <td><?php echo substr($notice->notice,0,20); ?></td>
                        <td><?php echo btn_view('notice/view/'.$notice->noticeID, $this->lang->line('view')); ?></td>
                     </tr>
                     <?php 
                        $i++; 
                        }
                        } 
                        ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <!-- /row 2 student -->
   <!-- raushan -->
   <!-- style="display: none;" -->
   <div class="row">
      <div class="col-md-4">
         <div class="box-main1 border-yellow">
            <div class="box-main-header">
               <h3>Calender</h3>
            </div>
            <div class="box-main-body">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div id="mycalendars"></div>
                  </div>
                  <!--(./col-lg-12 col-md-12 col-sm-12 col-xs-12"BELOW ROW:)-->
               </div>
               <!--(./row)-->
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="box-main box_rau border-brightyellow">
            <div class="box-main-header">
               <h3>Thought of the Day</h3>
            </div>
            <div class="box-main-body table-display">
               <div class="daily-thought">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                     <div class="col-md-12">
                        <div class="carousel-inner">
                           <div class="item active">
                              <p>“Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.”
                              </p>
                           </div>
                           <div class="item">
                              If you want to be successful, you need to discover something you are actually passionate about.”
                           </div>
                           <div class="item">
                              “Successful and unsuccessful people do not vary greatly in their abilities. They vary in their desires to reach their potential.”
                           </div>
                           <div class="item">
                              “There are no negatives in life, only challenges to overcome that will make you stronger.”
                           </div>
                           <div class="item">
                              “When you think about it, the secret to success in social media isn't any different than the secret to success in life: help others first.”
                           </div>
                           <div class="item">
                              “You see, success isn’t a list of goals to be checked off one after another. It’s not reaching a destination. Success is a journey.”
                           </div>
                           <div class="item">
                              “When one door closes, another opens; but we often look so long and so regretfully upon the closed door that we do not see the one which has opened for us.”
                           </div>
                           <div class="item">
                              “Solve one problem, and you keep a hundred others away.”
                           </div>
                           <div class="item">
                              “At the end of the day we are accountable to ourselves - our success is a result of what we do.” 
                           </div>
                        </div>
                     </div>
                  </div>
                  <span>
                  <img src="<?php echo base_url() ?>assets/inilabs/EtDesigns/img/happiness.png" alt="Good Day" />
                  </span>
                  <div>&nbsp;</div>
                  <div>&nbsp;</div>
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="col-md-9">&nbsp;</div>
                           <div class="col-md-3">
                              <a class="carousel-control" href="#myCarousel" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
                              <span class="sr-only">Previous</span>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="col-md-1">&nbsp;</div>
                           <div class="col-md-2">
                              <a class="carousel-control" href="#myCarousel" role="button" data-slide="next" >
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                              </a>
                           </div>
                           <div class="col-md-9">&nbsp;</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4">
         <div class="box-main box_rau border-blue">
            <div class="box-main-header">
               <h3>Weather</h3>
            </div>
            <div class="box-main-body">
               <!-- <h1><div id="temp"></div><div id="minutely"></div></h1>
                  <h2><div id="location"></div></h2> -->
               <div class="daily-weather">
                  <a class="weatherwidget-io" href="https://forecast7.com/en/28d5477d39/noida/" data-label_1="NOIDA" data-label_2="WEATHER" data-theme="original" >NOIDA WEATHER</a>
                  <script>
                     !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                  </script>
                  <!-- <div id="weather"></div>
                     <button class="js-geolocation" id="weatherfunctionSession" style="display: none;margin: 15px auto;width: 140px;">Use Your Location</button> -->
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript" src="<?php echo base_url('assets/fullcalendar/fullcalendar.min.js'); ?>"></script>
   <!-- end raushan -->
   <!-- end admin stats -->
   <!-- row 2 -->
   <div class="row">
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-left: 0px;">
            <div class="card-header">
               <h4 class="card-title m-b-0" style="text-align: center;">My Exam</h4>
            </div>
            <div class="card-body">
               <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-12">
         <div class="card" style="margin-right: 0px" >
            <div class="card-header">
               <h4 class="card-title m-b-0" style="text-align: center;">There are no assignments available at this time</h4>
            </div>
            <div class="card-body">
               <!-- <h1>Ther is no message</h1> -->
               <div id="container3" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xs-12" style="margin-top:3%;">
      <div id="container"></div>
   </div>
   <!-- /row -->
   <!-- row -->
   <div class="row">
      <div class="col-sm-12">
         <div class="card">
            <div class="card-header">
               <h4 class="card-title m-b-0" style="text-align: center;">There are no results available at this time.</h4>
            </div>
            <center>
               <div id="columnchart_material" style="width: 700px;height:309px;padding:9px;"></div>
            </center>
         </div>
      </div>
      <!--  Recent Result   -->
      <!-- <div class="col-sm-6">
         <div class="card recent-result">
         
           <div class="card-header">
         
            <h4 class="card-title m-b-0">Recent Result</h4>
         
           </div>
         
          <div class="result">
         
         
         
         <div class="row main-result">
         
         <div class="col-md-12 table-responsive">
         
           <table class="table">
         
             <thead>
         
              <tr>
         
                   <th class="text-uppercase col-md-4">Paper Code Title</th>
         
                   <th class="text-uppercase col-md-2"><span class="text-capitalize">MM/Ob. Marks</span></th>
         
                   <th class="text-uppercase col-md-2"><span class="text-capitalize">MM/Ob. Marks</span></th>
         
                   <th class="text-uppercase col-md-2"><span class="text-capitalize">MM/Ob. Marks</span></th>
         
                   <th class="text-uppercase col-md-2">Result</th>
         
               </tr>
         
               </thead>
         
                <tbody>
         
                 <tr>
         
                   <td class="col-md-4">
         
         201                            :Mathematics II                        </td>
         
                                           <td class="col-md-2">30/25</td>
         
                   <td class="col-md-2">70/59</td>
         
                   <td class="col-md-2">84</td>
         
                   <td class="col-md-2">
         
                           P</td>
         
               </tr>
         
                                   <tr>
         
                   <td class="col-md-4">
         
         202                            :Design and analysis of algorithm                        </td>
         
                                           <td class="col-md-2">30/26</td>
         
                   <td class="col-md-2">70/64</td>
         
                   <td class="col-md-2">90</td>
         
                   <td class="col-md-2">
         
                           P</td>
         
               </tr>
         
                                   <tr>
         
                   <td class="col-md-4">
         
         203                            :Telecommunication Systems                        </td>
         
                                           <td class="col-md-2">30/26</td>
         
                   <td class="col-md-2">70/43</td>
         
                   <td class="col-md-2">69</td>
         
                   <td class="col-md-2">
         
                           P</td>
         
               </tr>
         
                                   <tr>
         
                   <td class="col-md-4">
         
         204                            :Professional Skill Development                        </td>
         
                                           <td class="col-md-2">30/25</td>
         
                   <td class="col-md-2">70/55</td>
         
                   <td class="col-md-2">80</td>
         
                   <td class="col-md-2">
         
                           P</td>
         
               </tr>
         
                                   <tr>
         
                   <td class="col-md-4">
         
         205                            :Computer Graphics                        </td>
         
                                           <td class="col-md-2">30/25</td>
         
                   <td class="col-md-2">70/59</td>
         
                   <td class="col-md-2">84</td>
         
                   <td class="col-md-2">
         
                           P</td>
         
               </tr>
         
                               </tbody></table>
         
         </div>
         
         </div>
         
         
         
         </div>
         
         </div>
         
         </div> -->
      <script type="text/javascript">
         Highcharts.chart('container', {
         
         chart: {
         
           plotBackgroundColor: null,
         
           plotBorderWidth: null,
         
           plotShadow: false,
         
           type: 'pie'
         
         },
         
         title: {
         
           text: ''
         
         },
         
         tooltip: {
         
           pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         
         },
         
         plotOptions: {
         
           pie: {
         
             allowPointSelect: true,
         
             cursor: 'pointer',
         
             dataLabels: {
         
               enabled: true,
         
               format: '<b>{point.name}</b>: {point.percentage:.1f} %',
         
               style: {
         
                 color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
         
               }
         
             }
         
           }
         
         },
         
         series: [{
         
           name: 'Brands',
         
           colorByPoint: true,
         
           data: [{
         
             name: '1st Sem',
         
             y: 41.41,
         
             sliced: true,
         
             selected: true
         
           }, {
         
             name: '2nd Sem',
         
             y: 11.84
         
           }, {
         
             name: '3rd Sem',
         
             y: 10.85
         
           }, {
         
             name: '4th Sem',
         
             y: 10.67
         
           }, {
         
             name: '5th Sem',
         
             y: 9.18
         
           }, {
         
             name: '6th Sem',
         
             y: 18.64
         
           }]
         
         }]
         
         });
         
      </script>
      <script type="text/javascript">
         Highcharts.chart('container3', {
         
         chart: {
         
           plotBackgroundColor: null,
         
           plotBorderWidth: null,
         
           plotShadow: false,
         
           type: 'pie'
         
         },
         
         title: {
         
           text: ''
         
         },
         
         tooltip: {
         
           pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         
         },
         
         plotOptions: {
         
           pie: {
         
             allowPointSelect: true,
         
             cursor: 'pointer',
         
             dataLabels: {
         
               enabled: true,
         
               format: '<b>{point.name}</b>: {point.percentage:.1f} %',
         
               style: {
         
                 color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
         
               }
         
             }
         
           }
         
         },
         
         // series: [{
         
         //   name: 'Brands',
         
         //   colorByPoint: true,
         
         //   data: [{
         
         //     name: '1st Sem',
         
         //     y: 41.41,
         
         //     sliced: true,
         
         //     selected: true
         
         //   }, {
         
         //     name: '2nd Sem',
         
         //     y: 11.84
         
         //   }, {
         
         //     name: '3rd Sem',
         
         //     y: 10.85
         
         //   }, {
         
         //     name: '4th Sem',
         
         //     y: 10.67
         
         //   }, {
         
         //     name: '5th Sem',
         
         //     y: 9.18
         
         //   }, {
         
         //     name: '6th Sem',
         
         //     y: 18.64
         
         //   }]
         
         // }]
         
         });
         
      </script>
      <script type="text/javascript">
         $.ajax({
         
            type: 'GET',
         
            dataType: "json",
         
            url: "<?=base_url('dashboard/graphcall')?>",
         
            dataType: "html",
         
            success: function(data) {
         
         var response = jQuery.parseJSON(data);
         
         var chart = Highcharts.chart('container', {
         
         title: {
         
          text: 'Earning Graph'
         
         },
         
         subtitle: {
         
          text: 'Plain'
         
         },
         
         
         xAxis: {
         
          categories: ['1st Semester', '2nd Semester', '3rd Semester', '4th Semester', '5th Semester', '6th Semester', '7th Semester', '8th Semester']
         
         },
         
         series: [{
         
          type: 'column',
         
          colorByPoint: true,
         
          data: response.balance,
         
          showInLegend: false
         
         }]
         
         });
         
         }
         
         }); 
         
         $('#plain').click(function () {
         
         chart.update({
         
          chart: {
         
            inverted: false,
         
            polar: false
         
          },
         
          subtitle: {
         
            text: 'Plain'
         
          }
         
         });
         
         });
         
         
         
         $('#inverted').click(function () {
         
         chart.update({
         
          chart: {
         
            inverted: true,
         
            polar: false
         
          },
         
          subtitle: {
         
            text: 'Inverted'
         
          }
         
         });
         
         });
         
         
         
         $('#polar').click(function () {
         
         chart.update({
         
          chart: {
         
            inverted: false,
         
            polar: true
         
          },
         
          subtitle: {
         
            text: 'Polar'
         
          }
         
         });
         
         });
         
      </script> 
   </div>
   <?php } ?>
</div>
<style type="text/css">
   .fpaid{
   cursor:pointer;
   }
</style>
<style type="text/css">
   .font-icon-shortcut{
   color: #e20b0b;
   }
   .title-font-size{
   font-size: 16px;
   }
   .widget-shortcut-height{
   height: 50px;
   }
   * {
   box-sizing: border-box;
   }
   .zoom {
   transition: transform .2s; 
   }
   .zoom:hover {
   -ms-transform: scale(1.5); /* IE 9 */
   -webkit-transform: scale(1.5); /* Safari 3-8 */
   transform: scale(1.2); 
   }
   .thoughts{
   margin-top: 25px;
   cursor: pointer;
   /*position: fixed;*/
   }
   .left {
   text-align: left;
   margin-left: 30px;
   }
</style>
</style>
<script></script>
<script></script>
<script>
   $('#weatherfunctionSession').click(function() {  
   
     var weatherfunctionSession = $(this).val();   
   
       $.ajax({  
   
         type: 'POST',   
   
         url: "<?=base_url('dashboard/index')?>",   
   
         data: "weatherfunctionSession=" + weatherfunctionSession,   
   
         dataType: "html",   
   
         success: function(data) {   
   
           // location.reload();  
   
         }   
   
       });   
   
     });
   
</script>