<div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="ti ti-user"></i>Bulk Payment</h3>

        </div>

 <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li class="breadcrumb-item"><i class="fa fa-bullseye"></i> <a href="<?=base_url("dashboard/index")?>"><?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="breadcrumb-item"><a href="<?=base_url("student/index")?>"><?=$this->lang->line('menu_student')?></a></li>

                <li class="breadcrumb-item active">Bulk Payment</li>

            </ol>

       </div>
</div>

    <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">
<style type="text/css">
    .top_heading{
      text-align:center;
    }
</style>


<div class="top_heading">
    <h2>Confirmation Page</h2>
    <h3>Academic Partner- <?php echo $teacher->name; ?></h3>
</div>
<form action="<?php echo base_url() ?>student/bulk_payment" method='post'>
<table class="table">
    <thead>
        <tr>
        <th>Student name</th>
        <th>Father name</th>
        <th>Course name</th>
        <th>Semester</th>
        <th>Total fee</th>
        <th>Paid Fee</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
<?php
$total_amount =  0;
$total_paidamount =  0;
 foreach ($total_student as $key => $value) {
    $total_amount += $value->amount;
    $total_paidamount += $value->paidamount;
 ?>
        <tr>
            <td><?php echo $value->name ?></td>
            <td><?php echo $value->father_name ?></td>
            <td><?php echo $value->classes ?></td>
            <td><?php echo $value->yearsOrSemester ?></td>
            <td><?php echo $value->amount ?></td>
            <td><?php echo $value->paidamount ?></td>
            <td><label><input type="checkbox" name="checked_id[]" value="<?php echo $value->studentID ?>" checked onclick='remove_Bpayment(this,"<?php echo $value->amount?>","<?php echo $value->paidamount ?>")'></label></td>
        </tr>
<?php } ?>
    </tbody>
</table>
<div class="col-md-6"></div>
<div class="col-md-6">
    <table class="table">
                                 <?php 
                                 $total_amount_final = $total_amount-$total_paidamount
                                  ?>
                                    <tbody><tr>
                                <th class="col-sm-8 col-xs-8">Total Balance</th>
                                    
                                    <td class="col-sm-4 col-xs-4" ><i class="fa fa-inr" aria-hidden="true"></i><?php 
                                    if($passbook_balance){
                                     echo $passbook_balance->balance;
                                   }else{
                                    echo 0;
                                   }
                                     ?></td>
                                   
                                    </tr>
                                 <tr>
                                       <th class="col-sm-8 col-xs-8">Total Fee</th>
                                       <td class="col-sm-4 col-xs-4" id="total_amount_final"><i class="fa fa-inr" aria-hidden="true"></i><?php echo  $total_amount_final ?></td>
                                    </tr>
                                     <input type="hidden" id="total_payment" name="total_amount_final" value="<?php echo $total_amount_final ?>">
                                 </tbody></table>

                                
                                 <button class="btn btn-success btn-xs ">Pay</button>
                                 <button class="btn btn-success btn-xs">Cancel</button>
</div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function remove_Bpayment(val,amount,paidamount){
    
        var single_amout = +amount - +paidamount;
        var total = $('#total_payment').val();
        if(val.checked==true){
       var total_amount_after = +total + +single_amout; 
        $('#total_payment').val(total_amount_after);
        $('#total_amount_final').html(total_amount_after);
        }
        if(val.checked==false){
       var total_amount_after = +total - +single_amout; 
        $('#total_payment').val(total_amount_after); 
        $('#total_amount_final').html(total_amount_after);  
        }
    }
</script>

