
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">



  <?=link_tag('assets/etslabs/css/pignose.calendar.min.css');?>
        
        <?=link_tag('assets/etslabs/css/croppie.css');?>
        <!-- Font CSS -->
        <?=link_tag('assets/etslabs/css/all_icons.css');?>
        <?=link_tag('assets/plugins/bootstrap/css/bootstrap.css');?>
        
        <?=link_tag('assets/etslabs/css/style.css');?>
        
         <?=link_tag('assets/etslabs/css/feedback.css');?> 
      
        <!-- MetisMenu CSS -->
        
        <?=link_tag('assets/plugins/metisMenu/metisMenu.min.css');?>
        <!-- Custom CSS -->
        
        
        <!-- Change Color CSS --> 
        
        <?=link_tag('assets/etslabs/css/skin/default-skin.css');?>




            
        <?=link_tag('assets/etslabs/css/intlTelInput.css');?>
        <?=link_tag('assets/fonts/font-awesome.css');?>
        <?=link_tag('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css');?>

       
        <?=link_tag('assets/etslabs/css/styleExam.css');?> 
        <?=link_tag('assets/etslabs/css/login.css');?> 
        
        
        <?=link_tag('assets/etslabs/css/kavach.min.css');?>
        <?=link_tag('assets/inilabs/EtDesigns/css/jquery.datetimepicker.min.css');?>
        <?=link_tag('assets/etslabs/css/button-style.css');?>


         <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 

         <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">

<!-- 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css"> -->



     <script>
          var uri =  "<?php echo $this->uri->segment(3) ?>";
          var uri4 =  "<?php echo $this->uri->segment(4) ?>";
          var base_url="<?php echo base_url();?>";
        </script>
<style type="text/css">
    .stable_img img {
    border-radius: 50%;
    width: 42px;
    height: 42px;
    border: 4px solid #ddd;
    margin: auto;
}
</style>
</head>
<body>

 <div class="container">
<table id="example4" class="table table-striped table-bordered table-hover" style="width:100%">
     <thead>
                  <tr>
                     <th></th>
                     <th >S.no</th>
                     <th><?=$this->lang->line('student_photo')?></th>
                     <th>Student Id</th>
                     <th><?=$this->lang->line('student_roll')?></th>
                     <th><?=$this->lang->line('student_name')?></th>
                     <th><?=$this->lang->line('father_name')?></th>                      
                     <th>Year</th>
                     <th><?=$this->lang->line("student_classes")?></th>
                     <th><?=$this->lang->line('sub_coursesID')?></th>           
                     
                     <th><?=$this->lang->line('session')?></th>
                     <th><?=$this->lang->line('sessionType')?></th>
                     <th><?=$this->lang->line('examType')?></th>
                <!-- <th><?=$this->lang->line('education_mode')?></th> -->
                     <th><?=$this->lang->line('student_entry_type')?></th>
                     <th><?=$this->lang->line('student_phone')?></th>
                     <th><?=$this->lang->line('student_dob')?></th>
                     <th><?=$this->lang->line('student_sex')?></th>
                     <th><?=$this->lang->line('student_email')?></th>
                     <th><?=$this->lang->line('mother_name')?></th>                     
                     <th><?=$this->lang->line('aadhar')?></th>
                     <th><?=$this->lang->line('nationality')?></th>
                     <th ><?=$this->lang->line('create_date')?></th>
                     <th ><?=$this->lang->line('counsellor')?></th>
                     <th><?=$this->lang->line('student_employment_status')?></th>
                     <th>Total Amount</th>
                     <th>Paid Amount</th>
                     <th>Due Amount</th>
                     <th>Current Status</th>
                     <th>Payment Status</th>
                     <th>Comment</th>
                     <th><?=$this->lang->line('action')?></th>
                  </tr>
               </thead>
    </table>
    </div>
  <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>
<?php $this->load->view("components/page_footer"); ?>

<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    var table = $('#example222').DataTable( {
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
        fixedColumns:   {
            leftColumns: 2,
            rightColumns: 2
        }
    } );
} );
</script> -->

</body>
</html>
