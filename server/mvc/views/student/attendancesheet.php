<style>
  @media (min-width: 768px)
  {
    .modal-dialog {
      width: 1300px;
      margin: 30px auto;
    }
  }
  .modal-content {
    top: 410px!important;
    overflow-y: scroll!important;
    border-radius: 0px!important;
    height: 50%!important;
  }
  .h3 {
    background: #929292;
    padding: 2px 10px;
    text-align: center;
    color: #fff;
    font-weight: 600;
    margin-top: 0px;
  }
  #imgs {
    width: 90%;
    height: 25%;
    margin-left: 0px;
    margin-top: 0px;
    margin-bottom:20px;
  }
</style>
<div id="print_wrapp">
<?php
  if(empty($attendance))
  {
    $where = "studentID = '".$stuID."' and yearsOrSemester = '".$yearsOrSemester."'";
    $results = $this->db->select('roll,name,father_name,session,sessionType,student_status,photo,classesID')->from('student')->where($where)->get()->result_array();
    foreach($results as $row)
    {
?>
      <div class="col-xs-12 col-lg-12">
        <h3 class="h3">ATTENDANCE/VERIFICATION SHEET<br/> LINGAYA'S VIDYAPEETH<br/>EXAMINATION - DEC <?php echo date('Y'); ?> (MAIN EXAMINATION)</h3>
      </div>
      <div class="col-xs-12 col-lg-12">
        <h4>Roll No: <?php echo $row['roll']; ?></h4>
      </div>
      <div class="col-xs-3 col-lg-3">
        <img src="<?php echo base_url(); ?>uploads/images/<?php echo $row['photo']; ?>" id="imgs"/>
      </div>
      <div class="col-xs-9 col-lg-9">
        <p>Full Name of Candidate: <?php echo $row['name']; ?></p>
        <p>Father's /Husband Name: <?php echo $row['father_name']; ?></p>
        <p>Course: <?php echo $course; ?></p>
        <p>Semester: <?php echo str_replace("_"," ",$yearsOrSemester); ?></p>
        <p>Session Year: <?php echo $row['session']; ?></p>
        <p>Session Type: <?php echo $row['sessionType']; ?></p>
        <p>Student Entry Status: <?php echo $row['student_status']; ?></p>
      </div>
      <div class="col-xs-12 col-lg-12">
        <h4>DESCRIPTION OF THE PRESENCE OF THE CANDIDATE IN THE EXAMINATION HALL:</h4>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <th>S. No.</th>
              <th>Paper Code</th>
              <th>Subject</th>
              <th>Exam Date</th>
              <th>Room No.</th>
              <th>Signature of Candidate</th>
              <th>Signature of Invigilator</th>
            </thead>
            <tbody>
              <?php
                  $k = 1;
                  $where = "classesID = '".$row['classesID']."' and yearsOrSemester = '".$yearsOrSemester."'";
                  $sqls = $this->db->select('subject_code,subject')->from('subject')->where($where)->get()->result_array();
                  foreach ($sqls as $fetch) 
                  {
              ?>
                      <tr>
                        <td><?php echo $k; ?></td>
                        <td><?php echo $fetch['subject_code']; ?></td>
                        <td><?php echo $fetch['subject']; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
              <?php
                    $k++;
                  }
              ?>
            </tbody>
          </table>
        </div>
        <p>The invigilator is requested to get the signature of the examinee and verify the signature & photograph</p>
      </div>
      <div class="col-xs-6 col-lg-6">
        <p>________________________</p>
        <p>(Signature of the Candidate)</p>
      </div>
      <div class="col-xs-6 col-lg-6 text-right">
        <p>_____________________________________________</p>
        <p>(Name & Signature of the Exam Center Superintendent)</p>
      </div>
<?php        
    }
  } 
  else
  {
    foreach($attendance as $attendance)
    {
      $photo = $attendance['photo'];
      $roll = $attendance['roll'];
      $name = $attendance['name'];
      $father_name = $attendance['father_name'];
      $session = $attendance['session'];
      $session_type = $attendance['sessionType'];
      $student_status = $attendance['student_status'];
    }
?>
    <div class="col-xs-12 col-lg-12">
      <h3 class="h3">ATTENDANCE/VERIFICATION SHEET<br/> LINGAYA'S VIDYAPEETH<br/>EXAMINATION - DEC <?php echo date('Y'); ?> (MAIN EXAMINATION)</h3>
    </div>
    <div class="col-xs-12 col-lg-12">
      <h4>Roll No: <?php echo $roll; ?></h4>
    </div>
    <div class="col-xs-3 col-lg-3">
      <img src="<?php echo base_url(); ?>uploads/images/<?php echo $photo; ?>" id="imgs"/>
    </div>
    <div class="col-xs-9 col-lg-9">
      <p>Full Name of Candidate: <?php echo $name; ?></p>
      <p>Father's /Husband Name: <?php echo $father_name; ?></p>
      <p>Course: <?php echo $course; ?></p>
      <p>Semester: <?php echo str_replace("_"," ",$yearsOrSemester); ?></p>
      <p>Session Year: <?php echo $session; ?></p>
        <p>Session Type: <?php echo $session_type; ?></p>
        <p>Student Entry Status: <?php echo $student_status; ?></p>
    </div>
    <div class="col-xs-12 col-lg-12">
      <h4>DESCRIPTION OF THE PRESENCE OF THE CANDIDATE IN THE EXAMINATION HALL:</h4>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <th>S. No.</th>
            <th>Paper Code</th>
            <th>Subject</th>
            <th>Exam Date</th>
            <th>Room No.</th>
            <th>Signature of Candidate</th>
            <th>Signature of Invigilator</th>
          </thead>
          <tbody>
            <?php
              if($attendance['result'][0]['Grade']=='F')
              {
                $fail = explode(",",$attendance['fail_subjectID']);
                $j=0;
                foreach ($fail as $fails) 
                {
                  $sqls = $this->db->select('subject_code,subject')->from('subject')->where('subjectID',$fails)->get()->row();
                  if(!empty($sqls))
                  {
            ?>
                    <tr>
                      <td><?php echo $j; ?></td>
                      <td><?php echo $sqls->subject_code; ?></td>
                      <td><?php echo $sqls->subject; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
            <?php
                  }
                  $j++;
                }
              }
              else
              {
                $i = 1;
                foreach($attendance['result'] as $row)
                {
                  $sqls = $this->db->select('subject_code,subject')->from('subject')->where('subjectID',$row['subjectID'])->get()->row();
                  if(!empty($sqls))
                  {
            ?>
                    <tr>
                      <td><?php echo $j; ?></td>
                      <td><?php echo $sqls->subject_code; ?></td>
                      <td><?php echo $sqls->subject; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
            <?php        
                  }
                }
                $i++;
              }
            ?>
          </tbody>
        </table>
      </div>
      <p>The invigilator is requested to get the signature of the examinee and verify the signature & photograph</p>
    </div>
    <div class="col-xs-6 col-lg-6">
      <p>________________________</p>
      <p>(Signature of the Candidate)</p>
    </div>
    <div class="col-xs-6 col-lg-6 text-right">
      <p>_____________________________________________</p>
      <p>(Name & Signature of the Exam Center Superintendent)</p>
    </div>
<?php
  }
?>
</div>
<script>
  function printDiv() 
  {
    var divToPrint=document.getElementById('print_wrapp');
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write('<html><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet"/><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/print.css" rel="stylesheet"/><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
  }
</script>