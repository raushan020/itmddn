<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor"><i class="ti ti-user"></i> Promote Student</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<i class="fa fa-bullseye"></i> 
				<a href="<?=base_url("dashboard/index")?>">
					<?=$this->lang->line('menu_dashboard')?>
				</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?=base_url("student/index")?>">
					<?=$this->lang->line('menu_student')?>
				</a>
			</li>
			<li class="breadcrumb-item active">Promote Student</li>
		</ol>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="">
			<div class="card">
				<div class="card-body">
					<style type="text/css">
						.top_heading{
							text-align:center;
						}
					</style>
					<form action="<?php echo base_url() ?>student/promote_student" method='post'>
						<table class="table">
							<thead>
								<tr>
									<th>Student name</th>
									<th>Father name</th>
									<th>Session</th>
									<th>Session Type</th>
									<th>Course name</th>
									<th>Semester</th>
									<th>Promote In</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									foreach ($total_student as $key => $value) 
									{
								?>
										<tr>
											<td><?php echo $value->name ?></td>

											<td><?php echo $value->father_name ?></td>
											<td><?php echo $value->session ?></td>
											<td><?php echo $value->sessionType ?></td>
											<td><?php echo $value->classes ?></td>
											<td><?php echo $value->yearsOrSemester ?></td>
											<td>
												<?php
												$yearsOrSemester = findYearOrSemesterByYear($value->start_session,$value->duration,$value->mode,$value->sessionType);

												 ?>
												<select name="yos_filter[]" id="yos_filter" class='form-control'>
													<?php 
													
															$looping = (int)$value->duration;
															if ($value->mode==1) 
															{
																for ($i=1; $i <=$looping; $i++) 
																{
																	if (CallYears($yearsOrSemester+1)==CallYears($i))
																	{
																		$select = 'selected';
																	}else{
																		$select = '';
																	}
																	echo "<option ".$select." value=".CallYears($i).">".str_replace(CallYears)."</option>";
																}
															}
															else
															{
																for ($i=1; $i <=(2*$looping); $i++) 
																{
																

																  if (CallSemester($yearsOrSemester+1)==CallSemester($i))
																	{
																		$select = 'selected';
																	}else{
																		$select = '';
																	}

																	echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
																}
															} 
														
													?>
												</select>
											</td>
											<td>
												<label>
													<input type="checkbox" name="checked_id[]" value="<?php echo $value->studentID ?>" checked>
												</label>
											</td>
										</tr>
								<?php 
									} 
								?>
							</tbody>
						</table>
						<div class="col-md-6"></div>
						<div class="col-md-6">
							<button class="btn btn-success btn-xs ">Promote</button>
							<button class="btn btn-success btn-xs">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>