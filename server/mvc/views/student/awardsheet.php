<style>
    .center {
      float: left;
      text-align: center;
    }
    .center h3 {
      color: #000;
      font-weight: 600;
    }
    .left {
      font-size: 14px;
      float: left;
      width: 30%;
      color: #000;
    }
    .right {
      font-size: 14px;
      float: right;
      width: 70%;
      color: #000;
    }
    #bot {
      margin-bottom: 2em;
    }
    #left {
      font-size: 14px;
      float: left;
      width: 42%;
      color: #000;
    }
    #right {
      font-size: 14px;
      float: right;
      width: 58%;
      color: #000;
    }
    table, td, th {  
      border: 1px solid #000;
      text-align: left;
      font-size: 14px;
      font-weight: inherit;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 15px;
    }
    #pp {
      font-size:14px;
      color:#000;
    }
    .table-responsive {
        overflow-x: hidden!important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button {
      padding:0px!important;
    }
</style>
<div class="row page-titles">
  <div class="col-md-5 align-self-center">
    <h3 class="text-themecolor"><i class="fa fa-trophy"></i> Award Sheet</h3>
  </div>
  <div class="col-md-7 align-self-center">
    <ol class="breadcrumb">
      <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
      <li class="active">Award Sheet</li>
    </ol>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="">
      <div class="card">
        <div class="card-body">          
          <div class="col-sm-12">

            <div class="theme_input_blue">
                <div class="col-md-12" id="bot">
                <div class="col-sm-3">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterclassesID')), "id='classesID' onchange='CourseSDependent($(this).val())' class='form-control'");
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-3">
                  <div class="">
                    <form style="" class="form-horizontal" role="form" method="post">
                      <div class="form-group">
                        <div>
                          <label for="classesID" class="control-label">
                            Session
                          </label>
                        </div>
                        <?php
                          $arraySessionFilter = explode('-', $this->session->userdata('sessionFilter'));
                        ?>
                        <div class="col-sm-6 nopading">
                          <div class="forpostionReletive">
                            <select class="form-control" id="SessionFrom">
                              <option>From</option>
                              <?php 
                                for($i=2015; $i<=2025; $i++)
                                { 
                              ?>
                                  <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[0]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                              <?php 
                                } 
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-6 nopading">
                          <div class="forpostionReletive">
                            <select class="form-control" id="SessionTo">
                              <option>To</option>
                              <?php 
                                for($i=2015; $i<=2025; $i++)
                                { 
                              ?>
                                  <option <?php if(count($arraySessionFilter)==2 && $arraySessionFilter[1]==$i){echo "selected"; }else{echo "";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                              <?php 
                                } 
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="forReset">
                          <a style="cursor:pointer;" onclick="ResetSesession()" id="ResetSesession">Reset This Filter </a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <div class="col-sm-3">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='yearsOrSemester' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('yearsOrSemester'); ?>'> 
                                    <option>Select</option>      
                                    <?php 
                                      if ($classesRow) 
                                      {
                                        $looping    =  (int) $classesRow->duration;
                                        if ($classesRow->mode==1) 
                                        {
                                          for ($i=1; $i <=$looping; $i++) 
                                          {
                                            if (CallYears($i)==$this->session->userdata('FilteryearSemesterID')) 
                                            {
                                              $select = 'Selected';
                                            }
                                            else
                                            {
                                              $select = '';
                                            }
                                            echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                          }
                                        }
                                        else
                                        {
                                          for ($i=1; $i <=(2*$looping); $i++) 
                                          {
                                            if (CallSemester($i)==$this->session->userdata('FilteryearSemesterID')) 
                                            {
                                              $select = 'Selected';
                                            }
                                            else
                                            {
                                              $select = '';
                                            }
                                            echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                          }
                                        }
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="subjectID" class="control-label">
                                    Subject
                                </label>
                                <div class="">
                                    <select id="subjectID" name="subjectID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  
                                          foreach ($subject as $row) 
                                          {
                                        ?>
                                          <option value="<?php echo $row['subjectID'] ?>"><?php echo $row['subject'] ?></option>
                                        <?php 
                                          } 
                                        ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div id="award"></div>

          </div> 
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $('#classesID').change(function() {
      var classesID = $(this).val();
      $.ajax({
          type: 'POST',
          url: "<?=base_url('student/awardsheet')?>",
          data: "classesID=" + classesID,
          dataType: "html",
          success: function(data) {
               location.reload();
          }
      });
    });
</script>
<script type="text/javascript">
  $('#SessionTo').change(function() {
      var SessionTo = $(this).val();
      var SessionFrom  = $('#SessionFrom').val();
      $.ajax({
         type: 'POST',
         url: "<?=base_url('student/awardsheet')?>",
         data:{SessionTo:SessionTo,SessionFrom:SessionFrom},
         dataType: "html",
         success: function(data) {
             location.reload();
         }
      });
   });
</script>
<script type="text/javascript">
    $('#subjectID').change(function(e) {
      e.preventDefault();
      var subjectID = $(this).val();
      var session = $("#SessionFrom").val()+"-"+$("#SessionTo").val();
      var courseID = $("#classesID").val();
      var yearsOrSemester = $("#yearSemesterID").val();
      $.ajax({
          type: 'POST',
          url: "<?=base_url('student/sheet')?>",
          data: {"subjectID":subjectID,"session":session,"courseID":courseID,"yearsOrSemester":yearsOrSemester},
          dataType: "html",
          contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
          success: function(data) {
              $("#award").html(data);
          }
      });
    });
</script>
<script type="text/javascript">
    $('#subCourseID').change(function() {
      var subCourseID = $(this).val();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('student/awardsheet')?>",
            data: "subCourseID=" + subCourseID,
            dataType: "html",
            success: function(data) {
                 location.reload();
            }
        });
    });
</script>

<script type="text/javascript">
    $('#yearSemesterID').change(function() {
      var yearSemesterID = $(this).val();
      $.ajax({
          type: 'POST',
          url: "<?=base_url('student/awardsheet')?>",
          data: "yearSemesterID=" + yearSemesterID,
          dataType: "html",
          success: function(data) {
              location.reload();
          }
      });
    });

function ResetCourses(){
  $.ajax({
      type: 'POST',
      url: "<?=base_url('student/ResetCoursess')?>",
      data:{ResetCourses:'ResetCourses'},
      dataType: "html",
      success: function(data) {
          location.reload();
      }
  });
}

function ResetSesession(){
  $.ajax({
      type: 'POST',
      url: "<?=base_url('student/ResetSesession')?>",
      data:{ResetSesession:'ResetSesession'},
      dataType: "html",
      success: function(data) {
          location.reload();
      }
  });
}
function ResetSemesterYear(){
  $.ajax({
      type: 'POST',
      url: "<?=base_url('student/ResetSemesterYears')?>",
      data:{ResetSesession:'ResetSesession'},
      dataType: "html",
      success: function(data) {
        location.reload();
      }
  });
}
</script>
<script>
  function printDiv() 
  {
    var divToPrint=document.getElementById('print_wrapp');
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write('<html><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet"/><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/print.css" rel="stylesheet"/><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
  }
</script>
