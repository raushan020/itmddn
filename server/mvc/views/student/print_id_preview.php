  
<!-- student form informtion -->
<LINK REL="stylesheet" TYPE="text/css" MEDIA="print, handheld" HREF="foo.css">

<?=link_tag('assets/etslabs/css/info.css')?>

  

<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="Refresh" content="5; url=http://101.53.153.40/dev_black/student/id_card">
    <title>Student ID Card</title>
   <!--   <link rel="stylesheet" href="info.css"> -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    </head>
  <style>
    .wrap {
      width: 400px;
      height: 230px;
      /*min-height:62%;*/
      margin: auto;
      border: 1px solid #80808063;
      border-radius: 5px;
      padding: 14px;
      /*margin-top: 4em;
      margin-bottom: 4em;*/
      box-shadow: 0 2px 4px 0 rgba(0,0,0,.08);
    }
    #img_l {
      float: left;
      width: 20%;
      margin-top: -6px;
    }
    #img_l img {
      width:56%;
    }
    #right_t {
      float: right;
      width: 80%;
      text-align: center;
      line-height: 10px;
      margin-top: -19px;
    }
        #right_t p{
        margin:0 0  0px;
    }
    #col-p {
      padding-bottom: 0px;
    }
    #col-top {
      border-top: 2px solid #717171;
      padding-top: 10px;
    }
    #col-top #p {
      text-align: center;
      font-size: 10px;
      margin: -7px 0 -17px;
    }
    .left {
      float: left;
      width: 19%;
      border: 2px solid #80808033;
      border-radius: 5px;
      margin-right: 6px;
    }
    .left img {
      width: 72px;
      height: 83px;
    }
    .right {
      float:right;
      width:81%;
      padding: 0px 20px;
      line-height: 15px;
    }
/*    .right #in_l {
      width: 63%;
      float: left;
    }
        .right #in_l p {
        font-size: 10px
    }
    .right #in_r {
      width: 38%;
      float: right;
      margin-top: -10%;
    }
        .right #in_r p{
      font-size: 10px;
      margin: -14px 0 -15px;
    }
    .inside_left {
      float: left;
      width: 55%;
    }
    .right #in_r p {
    font-size: 10px;
        margin: -5px 0 0px;
    }*/
/*    .inside_left p {
      font-size: 10px;
          margin: 0px 0 3px;
    }*/
/*    .inside_right {
      float: right;
      width: 45%;
    }*/
/*    tr{
          line-height: 13px;
    }*/
       td{
     font-size:10px;
     line-height: 13px;
   }
  .heading_left{
    width:81px;
  }
  .heading_right{
    width:42px;
  }
  </style>
   <body style="margin-top: 4%" onload="window.print();">
     <!-- onload="window.print();" -->

<?php {
 ?>
 
<?php
 }?>
 <div id="printablediv">
  <div class="container-fluid">
    <div class="row">
      <div class="card">
        <div class="card-body">
          <div class="box-body">
          </br>
            <div class="wrap">
              <?php
                  if(count($student)) 
                  {
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == "ClgAdmin" || $usertype == "Teacher" || $usertype == "Student") 
                    {
              ?>
                      <div class="row">
                        <div class="col-lg-12" id="col-p">
                          <div id="img_l">
                            <center><img src="<? base_url()?>../uploads/images/Lingayas Vidyapeeth Logo.png"></center>
                          </div>
                          <div id="right_t">
                            <h4 style="color: red; margin-bottom: 2px">
                              LINGAYA'S VIDYAPEETH
                            </h4>
                            <p style="font-size: 10px">
                              (Under Section-3 of UGC Act, 1956)
                            </p>
                            <p style="font-size: 8px">
                              NACHAULI,JASANA-OLD FARIDABAD ROAD, FARIDABAD-121002 
                            </p>
                            <p style="font-size: 8px">
                              PHONE 0129-2598200 TO 240
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12" id="col-top">
                          <p id="p"><b>
                              Student Identity Card No. : <?=$student->username?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Roll No.:</b><span><b><?=$student->roll?></b></span>
                          </p>
                          <div id="top">
                            <div class="left">
                              <center>
                                <a href="#">

                    <?=img(base_url('uploads/images/'.$student->photo))?>

                </a>
                              </center>
                            </div>
                                <table style="margin-top:23px; margin-left: 3px">
    <tr>
    <td class="heading_left"><b>Name</b></td><td>: <?=$student->name?></td><!-- <td class="heading_right"><b>Roll No.</b></td><td>:<?=$student->roll?></td> -->
    </tr>
    <tr>
    <td class="heading_left"><b>Father's Name</b></td><td>: <?=$student->father_name?></td>
    </tr>
    <tr>
    <td class="heading_left"><b>Branch</b></td><td style="width: 150px;">: <?=$class->classes?> </td>
    </tr>
    <tr>
    <td class="heading_left"><b>DOB</b></td><td>: <?=date("d M Y", strtotime($student->dob))?></td>
    </tr>
    <tr>
    <td class="heading_left"><b>Blood Group</b></td><td>: -</td>
    </tr>
    <tr>
    <td class="heading_left"><b>Tel</b></td><td>: <?=$student->phone?> </td>
    </tr>
    <tr>
    <td class="heading_left"><b>Address</b></td><td>: <?=$student->address?></td>
    </tr>
    </table>
                            <!-- <div class="right">
                              <div class="inside_left">
                                <div id="in_l">
                                  <p><b>Name: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->name?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Father's Name: </b></p>
                                </div>
                                <div id="in_r">
                                  <p style="width: 164px"><?=$student->father_name?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Branch: </b></p>
                                </div>
                                <div id="in_r">
                                  <p style="width: 164px" ><?=$class->classes?></p>
                                </div>
                                <div id="in_l">
                                  <p style="margin-top:14px"><b>D.O.B: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=date("d M Y", strtotime($student->dob))?></p>
                                </div>                                
                                <div id="in_l">
                                  <p><b>Tel: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->phone?></p>
                                </div>
                                <div id="in_l">
                                  <p><b>Address: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->address?></p>
                                </div>
                              </div>
                              <div class="inside_right">
                                <div id="in_l">
                                  <p><b>Roll No.: </b></p>
                                </div>
                                <div id="in_r">
                                  <p><?=$student->roll?></p>
                                </div>
                                 <div id="in_l">
                                  <p><b>Blood Group: </b></p>
                                </div>
                                <div id="in_r">
                                  <p>-</p>
                                </div>
                              </div>
                            </div> -->
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-sm-12" style="padding-top:6%;">
                          <div class="col-lg-4 col-sm-4">
                            <img src="<? base_url()?>../uploads/images/Issuing Authority.jpeg" height="30px" style="margin-top: -25px">
                            <p style="text-align:left; font-size: 10px;"><b>Issuing Authority</b></p>
                          </div>
                          <div class="col-lg-4 col-sm-4">
                            <p style="text-align: center;  font-size: 10px; width:103px; margin-top: 5px"><b>Valid Upto:</b><span> <?=$student->session?></span></p>
                          </div>
                          <div class="col-lg-4 col-sm-4">
                            <center><img src="<? base_url()?>../uploads/images/Registrar.jpeg" height="30px" style="margin-top: -25px"></center>
                            <p style="text-align: right; font-size: 10px"><b>(Dy. Registrar)</b></p>
                          </div>
                        </div>
                      </div>
                      </div>
             
            </div>
            <br>
            <br>
            <br>
            <div class="wrap">
              <table>
                <h4 align="center"><b>Rules To Be Followed</b></h4>
                <tr>
                  <td style="font-size: 13px">1. Always carry the Identity card and show it on demand</td>
                </tr>
                <tr>
                  <td style="font-size: 13px">2. The loss of Identity card must be reported immediately in writing.</td>
                </tr>
                <!-- <tr>
                  <td style="font-size: 13px">3. Replacement of Identity card will be issued against a payment of Rs. 200.</td>
                </tr> -->
                <tr>
                  <td style="font-size: 13px">4. Deposite your card in the University after completion of Course.</td>
                </tr>
              </table>

              <div class="row">
                        <div class="col-lg-12  col-sm-12" style="padding-top:10px;">
                          <div class="col-lg-8 col-sm-8">
                            <img src="<?php echo base_url('uploads/images/barcode.jpg'); ?>" height="50px" width="100%">
                            <p style="text-align: center; font-size: 12px"><b><?=$student->username?></b></p>
                          </div>
                          <div class="col-lg-4 col-sm-4">
                            <img src="<?php echo base_url('uploads/images/barcode.jpg'); ?>" height="50px" width="100%">
                            <p style="text-align: center; font-size: 12px"><b>Auth. Signatory</b></p>
                          </div>

                        </div>
<!--                              
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('uploads/images/barcode.jpg'); ?>" height="50px" width="50%"> -->

             
                
              </div>
              </div>
               <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

 </div>
</body>
</html>
     
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <script language="javascript" type="text/javascript">

        function printDiv(divID) {
            //Get the HTML of div

            var divElements = document.getElementById(divID).innerHTML;

            //Get the HTML of whole page

            var oldPage = document.body.innerHTML;



            //Reset the page's HTML with div's HTML only

            document.body.innerHTML =

              "<html><head><title></title></head><body>" +

              divElements + "</body>";



            //Print Page

            window.print();



            //Restore orignal HTML

            document.body.innerHTML = oldPage;

        }

</script>
<!-- <script type="text/javascript">
  $(document).ready(function(){
window.onload = function() { window.print()}; 

  });
</script> -->

