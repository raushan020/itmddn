<style>
  .center {
    float: left;
    text-align: center;
  }
  .center h3 {
    color: #000;
    font-weight: 600;
  }
  .left {
    font-size: 14px;
    float: left;
    width: 30%;
    color: #000;
  }
  .right {
    font-size: 14px;
    float: right;
    width: 70%;
    color: #000;
  }
  
  .h3 {
    background: #929292;
    padding: 2px 10px;
    text-align: center;
    color: #fff;
    font-weight: 600;
    margin-top: 0px;
    font-size: 14px;
    margin-bottom: 0px;
  }
  h4 {
    font-size: 14px;
    font-weight: 600;
  }
  p {
    margin: 0 0 5px;
    font-size: 14px;
  }
  .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
      font-size: 10px;
  }
  #imgs {
    width: 90%;
    height: 25%;
    margin-left: 0px;
    margin-top: 0px;
    margin-bottom:0px;
  }
</style>
<div id="print_wrapp">
<?php
  $results = $this->db->select('roll,name,father_name,session,sessionType,student_status,photo,classesID')->from('student')->where_in('studentID',$checked_id)->where('yearsOrSemester', $yearsOrSemester)->get()->result_array();
  foreach($results as $row)
  {
?>
    <div class="col-xs-12 col-lg-12">
      <h3 class="h3">ATTENDANCE/VERIFICATION SHEET<br/> LINGAYA'S VIDYAPEETH<br/>EXAMINATION - DEC <?php echo date('Y'); ?> (MAIN EXAMINATION)</h3>
    </div>
    <div class="col-xs-12 col-lg-12">
      <h4>Roll No: <?php echo $row['roll']; ?></h4>
    </div>
    <div class="col-xs-3 col-lg-3">

      <img src='<?php echo base_url(); ?>uploads/images/<?php echo $row['photo']; ?>' id="imgs"/>
    </div>
    <div class="col-xs-9 col-lg-9">
      <p>Full Name of Candidate: <?php echo $row['name']; ?></p>
      <p>Father's /Husband Name: <?php echo $row['father_name']; ?></p>
      <p>Course: <?php echo $course; ?></p>
      <p>Semester: <?php echo str_replace("_"," ",$yearsOrSemester); ?></p>
      <p>Session Year: <?php echo $row['session']; ?></p>
      <p>Session Type: <?php echo $row['sessionType']; ?></p>
      <p>Student Entry Status: <?php echo $row['student_status']; ?></p>
    </div>
    <div class="col-xs-12 col-lg-12">
      <h4>DESCRIPTION OF THE PRESENCE OF THE CANDIDATE IN THE EXAMINATION HALL:</h4>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <th>S. No.</th>
            <th>Paper Code</th>
            <th>Subject</th>
            <th>Exam Date</th>
            <th>Room No.</th>
            <th>Signature of Candidate</th>
            <th>Signature of Invigilator</th>
          </thead>
          <tbody>
            <?php
                $k = 1;
                $where = "classesID = '".$row['classesID']."' and yearsOrSemester = '".$yearsOrSemester."'";
                $sqls = $this->db->select('subject_code,subject')->from('subject')->where($where)->get()->result_array();
                foreach ($sqls as $fetch) 
                {
            ?>
                    <tr>
                      <td><?php echo $k; ?></td>
                      <td><?php echo $fetch['subject_code']; ?></td>
                      <td><?php echo $fetch['subject']; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
            <?php
                  $k++;
                }
            ?>
          </tbody>
        </table>
      </div>
      <p>The invigilator is requested to get the signature of the examinee and verify the signature & photograph</p>
    </div>
    <div class="col-xs-6 col-lg-6">
      <p>________________________</p>
      <p>(Signature of the Candidate)</p>
    </div>
    <div class="col-xs-6 col-lg-6 text-right">
      <p>_____________________________________________</p>
      <p>(Name & Signature of the Exam Center Superintendent)</p>
    </div>
<?php        
    }
  
if(!empty($attendance_zip))
{
  {
    foreach($attendance_zip as $attendance)
    {
      $photo = $attendance['photo'];
      $roll = $attendance['roll'];
      $name = $attendance['name'];
      $father_name = $attendance['father_name'];
      $session = $attendance['session'];
      $session_type = $attendance['sessionType'];
      $student_status = $attendance['student_status'];
    }
?>
    <div class="col-xs-12 col-lg-12">
      <h3 class="h3">ATTENDANCE/VERIFICATION SHEET<br/> LINGAYA'S VIDYAPEETH<br/>EXAMINATION - DEC <?php echo date('Y'); ?> (MAIN EXAMINATION)</h3>
    </div>
    <div class="col-xs-12 col-lg-12">
      <h4>Roll No: <?php echo $roll; ?></h4>
    </div>
    <div class="col-xs-3 col-lg-3">
      <img src="<?php echo base_url(); ?>uploads/images/<?php echo $photo; ?>" id="imgs"/>
    </div>
    <div class="col-xs-9 col-lg-9">
      <p>Full Name of Candidate: <?php echo $name; ?></p>
      <p>Father's /Husband Name: <?php echo $father_name; ?></p>
      <p>Course: <?php echo $course; ?></p>
      <p>Semester: <?php echo str_replace("_"," ",$yearsOrSemester); ?></p>
      <p>Session Year: <?php echo $session; ?></p>
        <p>Session Type: <?php echo $session_type; ?></p>
        <p>Student Entry Status: <?php echo $student_status; ?></p>
    </div>
    <div class="col-xs-12 col-lg-12">
      <h4>DESCRIPTION OF THE PRESENCE OF THE CANDIDATE IN THE EXAMINATION HALL:</h4>
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <th>S. No.</th>
            <th>Paper Code</th>
            <th>Subject</th>
            <th>Exam Date</th>
            <th>Room No.</th>
            <th>Signature of Candidate</th>
            <th>Signature of Invigilator</th>
          </thead>
          <tbody>
            <?php
              if($attendance_zip['result'][0]['Grade']=='F')
              {
                $fail = explode(",",$attendance_zip['fail_subjectID']);
                $j=0;
                foreach ($fail as $fails) 
                {
                  $sqls = $this->db->select('subject_code,subject')->from('subject')->where('subjectID',$fails)->get()->row();
                  if(!empty($sqls))
                  {
            ?>
                    <tr>
                      <td><?php echo $j; ?></td>
                      <td><?php echo $sqls->subject_code; ?></td>
                      <td><?php echo $sqls->subject; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
            <?php
                  }
                  $j++;
                }
              }
              else
              {
                $i = 1;
                foreach($attendance_zip['result'] as $row)
                {
                  $sqls = $this->db->select('subject_code,subject')->from('subject')->where('subjectID',$row['subjectID'])->get()->row();
                  if(!empty($sqls))
                  {
            ?>
                    <tr>
                      <td><?php echo $j; ?></td>
                      <td><?php echo $sqls->subject_code; ?></td>
                      <td><?php echo $sqls->subject; ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
            <?php        
                  }
                }
                $i++;
              }
            ?>
          </tbody>
        </table>
      </div>
      <p>The invigilator is requested to get the signature of the examinee and verify the signature & photograph</p>
    </div>
    <div class="col-xs-6 col-lg-6">
      <p>________________________</p>
      <p>(Signature of the Candidate)</p>
    </div>
    <div class="col-xs-6 col-lg-6 text-right">
      <p>_____________________________________________</p>
      <p>(Name & Signature of the Exam Center Superintendent)</p>
    </div>
<?php
  }
}
else
{
  echo "";
}
?>
</div>