  
<!-- student form informtion -->
<LINK REL="stylesheet" TYPE="text/css" MEDIA="print, handheld" HREF="foo.css">

<?=link_tag('assets/etslabs/css/info.css')?>

  

<html lang="en">
    <head>
    <meta charset="UTF-8">
    <title>Title</title>
   <!--   <link rel="stylesheet" href="info.css"> -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    </head>
     <style>
         @media print { * 
        {
            -webkit-print-color-adjust:exact;
        }
         }

        @media print {
            @page { size: auto;  margin: 0mm; }
            * {
                margin: 0mm;
                padding: 0mm;
                -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
                color-adjust: exact !important;
                size:A4 landscape;
            }
            body {
                margin: 0;
                padding: 0;
                font-size: 11pt;
                font-family: sans-serif;
                width: 100%;
            }
            .form1{
                   margin-bottom: 25%;
            }
            .print-pdl0 {
                padding-left: 0;
            }
            .print-pdr0 {
                padding-right: 0;
            }
            .print-inline {
                width: auto;
            }
            .print-break {
                margin-top: 10px;
            }
            .print-office {
                margin-top: 10px;
            }
            .footer-text, .footer-text img {
                padding-top: 20px;
            }
            .flexFooter {
                padding-top: 20px;
            }
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }
            .form1 .row {
                margin-top: 10px;
            }
            .form1 .details label {
                font-weight: normal;
            }
            input {
                width: 100%;
                border: none;
                border-bottom: 1px solid black;
            }
            .form1 .photo {
                width: 100px;
                height: 100px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }


             .form1 .photo2 {
                width: 100px;
                height: 50px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }

            .form1 .formBeginning {
                padding: 20px;
            }

            .print:last-child {
                page-break-after: auto;
            }
        }

        @media screen {
            .mgt20 {
                margin-top: 20px;
            }

            .mgt30 {
                margin-top: 30px;
            }

            .mgt40 {
                margin-top: 40px;
            }

            .mgt60 {
                margin-top: 60px;
            }

            input {
                padding: 5px 10px;
            }

            input:hover,
            input:focus {
                outline: none;
            }

            .form1 input {
                width: 100%;
                border: none;
                border-bottom: 1px dotted black;
            }

            .form1 .photo {
                width: 200px;
                height: 200px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }

            .form1 .photo2 {
                width: 200px;
                height: 50px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }

            .form1 .details label {
                font-weight: normal;
                font-size:11px;
                    }


            .form1 input.form-check-input {
                width: auto;
            }

            .form1 .formBeginning {
                padding: 20px;
            }

            .form1 .btn-submit {
                width: auto;
                display: block;
                margin: 20px auto;
            }
        }

        @media only screen and (max-width: 767px) {
            .form1 .mversion {
                display: flex;
                flex-direction: column-reverse;
                margin-top: 40px;
            }

            .form1 .mversion input {
                margin-bottom: 20px;
            }

            .form1 .mversion .mgt60 {
                margin-top: 0;
            }

            .mversion-mgt20 {
                margin-top: 20px !important;
            }

            .mversion-radio {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .flexFooter {
                display: flex;
                flex-direction: column-reverse;
                align-items: center;
            }

            .flexFooter p {
                padding-top: 20px;
                text-align: center;
            }
        }

        @media only screen and (max-width: 991px) {
            .tversion-mgt20 {
                margin-top: 20px;
            }
        }

        @media only screen and (min-width: 768px) {
            .form1 label {
                padding-top: 10px;
            }

            .footer-text {
                padding-top: 40px;
            }
        }

        @media only screen and (min-width: 992px) {
            .lversion-padding {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }
    </style> 
   <body>

<?php {
 ?>
 
<?php
 }?>
 <div id="printablediv">

                
                    <img src="<?php echo base_url()?>images/stu_img/Home_Page_Header.jpg" alt="" class="img-responsive" />
                    <div class="container" >
                        <div class="wrapper">
                            
                            <form class="form1">
                                <div class="formBeginning">
                                    <div class="row mversion">
                                        <div class="col-sm-10 col-md-9">
                                            <div class="row mgt40">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="sno" class="width_11 width_sno">S.No&nbsp;&nbsp;</label>
                                                    <div class="col-sm-9 col-md-7 print-pdl0 width_89">
                                                        <div class="width_sno1"><input type="text"  value="&nbsp;&nbsp;<?=$student->username?>" class="form-control-plaintext" id="sno"></div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-6">
                                                    <label for="sno" class="width_type" style="width: 12%;">Type</label>
                                                    <div class="width_type1"><input type="text"  value="&nbsp;<?=$student->sessionType?>" class="form-control-plaintext" id="sno">
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="year" class="width_se_lbl">Session</label>
                                                    <div class="width_se_dv"><input type="text" value="&nbsp;<?=$student->session?>" class="form-control-plaintext" id="year">
                                                    </div>
                                                </div>
                                                <style type="text/css">
                                                    .width_se_lbl{
                                                        width: 20%;
                                                        float: left;
                                                    }
                                                    .width_se_dv{
                                                        width: 52%;
                                                        float: left;
                                                    }
                                                    .width_sno{
                                                        width: 20%;
                                                        float: left;
                                                    }
                                                    .width_sno1{
                                                        width: 71%;
                                                        float: left;
                                                        text-align: center;
                                                    }
                                                    .width_type{
                                                        width: 20%;
                                                        float: left;
                                                    }
                                                    .width_type1{
                                                        width: 53%;
                                                        float: left;
                                                    }
                                                    .roll_no1{
                                                        width: 20%;
                                                        float: left;
                                                    }
                                                    .roll_no2{
                                                        width: 45%;
                                                        float: left;
                                                        text-align: center;
                                                    }
.form1 label {
    padding-top: 10px;
    margin: 0px;
}
.form1 input {
    width: 100%;
    border: none;
    border-bottom: 1px dotted black;
    padding: 0px 0px;
}
.width_11{
    width:10%;
}
 .width_89 {
    width: 90%;
 }
 .course_app{
width: 25%;
float: left;
 }
 .course_app1{
    width: 60%;
    float: left;
 }
.line_hi
{
    margin:0px;
}
.name1{
width: 16%;
    float: left;
}
.name11{
    width: 84%;
    float: left;
}
.father1{
width: 35%;
    float: left;
}
.father11{
    width: 65%;
    float: left;
}
     </style>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="rollno" class="roll_no1">Roll No.</label>
                                                    <div class="roll_no2">
                                                        <input type="text" value="<?=$student->roll?>" class="form-control-plaintext " id="rollno">
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <label for="appliedCourse" class="course_app" style="width: 23%;">Course Applied for:</label>
                                                    <div class="course_app1">
                                                        <input type="text"  value="<?=$class->classes?>" class="form-control-plaintext" id="appliedCourse">
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-3"><label for="appliedCourse">Course Applied for:</label></div>
                                                <div class="col-md-9"><input type="text"  value="<?=$class->classes?>" class="form-control-plaintext" id="appliedCourse"></div> -->
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-3">
                                            <div class="photo">
                                               <?=img(base_url('uploads/images/'.$student->photo))?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row details">
                                        <div class="col-md-12">
                                            <p class="text-center line_hi mgt20"><strong>(Filling of the form does not guarantee admission in the Institution)</strong></p><br>
                                            <p class="text-center"><strong>(The form is to be filled by the candidate in his/her own handwriting in BLOCK LETTERS only)</strong></p><br>

                                            <div class="row mgt20">
                                                <div class="col-sm-12 col-md-12">
                                                    <label for="name" class="name1" style="width: 14%">1. Name in Full :</label>
                                                    <div class="name11">
                                                        <input type="text" value="<?=$student->name?>" class="form-control-plaintext" id="name">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="fname" class="father1">2. Father's Name :</label>
                                                    <div class="father11">
                                                        <input type="text"  value="<?=$student->father_name?>" class="form-control-plaintext" id="fname">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="mname" class="mother1">Mother's Name :</label>
                                                    <div class="mother11">
                                                        <style>
                                                            .mother1{
                                                                width: 33%;
                                                                float: left;

                                                            }
                                                            .mother11{
                                                                width: 64%;
                                                                float: left;
                                                            }
                                                        </style>
                                                        <input type="text"  value="<?=$student->mother_name?>" class="form-control-plaintext" id="mname">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="dob" class="dob1">3. Date of Birth :</label>
                                                    <div class="dob11">
                                                        <input type="text" value="<?=date("d M Y", strtotime($student->dob))?>" class="form-control-plaintext" id="dob">
                                                    </div>
                                                </div>
                                                <style>
                                                            .dob1{
                                                                width: 29%;
                                                                float: left;

                                                            }
                                                            .dob11{
                                                                width: 67%;
                                                                float: left;
                                                            }
                                                        </style>
                                                <div class="col-xs-12 col-sm-6 col-md-6 mversion-mgt20">
                                                    <label for="gender" class="col-xs-4 col-sm-3 col-md-4 col-form-label print-pdr0" style="width:19%;">Gender :</label>

                                          
                                                    <div class="col-xs-8 col-sm-9 col-md-8">
                                                     
                                                        <div class="form-check form-check-inline col-xs-6 col-sm-5 col-md-6 mversion-radio" style="margin-top: 8px;">                                      
                <input class="form-check-input print-inline"  type="radio" name="inlineRadioOptions" id="inlineRadio1" value="male" <?php if($student->sex=='Male'){ echo "checked"; }else{ echo ""; }?> style="width: 20%; float: left;" >
                                                            <label class="form-check-label" for="inlineRadio1" style="width: 76%;float: left; padding: 0px;" style="margin-top: 8px;">Male</label>
                                                        </div>
                                           
                                                        <div class="form-check form-check-inline col-xs-6 col-sm-7 col-md-6 mversion-radio" style="margin-top: 8px;">
                <input class="form-check-input print-inline" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="female" <?php if($student->sex=='Female'){ echo "checked"; }else{ echo ""; }?> style="width: 20%; float: left;">
                                                            <label class="form-check-label" for="inlineRadio2" style="width: 76%;float: left; padding: 0px;" style="margin-top: 8px;">Female</label>
                                                        </div>
                                                   
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-12">
                                                            <label for="address" class="Permanent1">4. Permanent Address :</label>
                                                            <div class="Permanent11">
                                                                <input type="text"  value="<?=$student->address?>" class="form-control-plaintext" id="address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        .Permanent1{
                                                            width: 23%;
                                                            float: left;
                                                        }
                                                        .Permanent11{
                                                            width: 75%;
                                                            float: left;
                                                        }
                                                    </style>
                                                    <div class="row mgt20">
                                                        <div class="city1">
                                                            <label for="city" class="city_lbl">City :</label>
                                                            <div class="city11 lversion-padding">
                                                                <input type="text" style="width: 82%" class="form-control-plaintext" id="city">
                                                            </div>
                                                        </div>
                                                        <style>
                                                        .city1{
                                                            width: 20%;
                                                            float: left;
                                                            padding-left: 15px;
                                                        }
                                                        .city_lbl{
                                                            width:25%;
                                                            float:left;
                                                        }
                                                        .city11{
                                                            width: 60%;
                                                            float: left;
                                                        }
                                                    </style>
                                                         <div class="pin1">
                                                           <label for="pin" class="pin11_lbl">Pin Code :</label>
                                                            <div class="pin11 lversion-padding">
                                                                <input type="text" value=" <?=$student->pin?>" class="form-control-plaintext" id="pin" style="width: 80%;font-size: 12px;">
                                                            </div>
                                                        </div>
                                                         <style>
                                                        .pin1{
                                                            width: 21%;
                                                            float: left;
                                                        }
                                                        .pin11_lbl{
                                                         width: 50%;
                                                            float: left;   
                                                        }
                                                        .pin11{
                                                            width: 50%;
                                                            float: left;
                                                        }
                                                    </style>
                                                        <div class="phone1">
                                                            <label for="phone" class="phone_lbl">Phone (with STD) :</label>
                                                            <div class="phone11">
                                                                <input type="text" class="form-control-plaintext" id="phone">
                                                            </div>
                                                        </div>
                                                        <style>
                                                            .phone1{
                                                            width: 27%;
                                                            float: left;
                                                            
                                                            }
                                                            .phone_lbl{
                                                                width:47%;
                                                                float:left;
                                                            }
                                                            .phone11{
                                                                width: 46%;
                                                                float:left;
                                                            }
                                                        </style>
                                                        <div class="mobile1">
                                                            <label for="mobile" class="mobile11_lbl">Mobile No.:</label>
                                                            <div class="mobile11">
                                                                <input type="text" value=" <?=$student->phone?>" class="form-control-plaintext" id="mobile">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        .mobile1{
                                                            width:30%;
                                                            float: left;
                                                            padding-right: 15px;
                                                        }
                                                        .mobile11_lbl{
                                                          width: 38%;
                                                            float: left;
                                                            
                                                        }
                                                        .mobile11{
                                                            width:62%;
                                                            float: left;
                                                        }
                                                    </style>

                                                </div>
                                            </div>

                                    
                                            <div class="row mgt20">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-12">
                                                            <label for="address" class="Permanent1_1">5. Correspondence Address :</label><br>
                                                            <div class="Permanent11_1">
                                                                <input type="text"  value="<?=$student->address?>" class="form-control-plaintext" id="address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        .Permanent1_1{
                                                            width: 28%;
                                                            float: left;
                                                        }
                                                        .Permanent11_1{
                                                            width: 98%;
                                                            float: left;
                                                        }
                                                    </style>
                                                    <div class="row mgt20">
                                                        <div class="city1">
                                                            <label for="city" class="city_lbl">City :</label>
                                                            <div class="city11 lversion-padding">
                                                                <input type="text" class="form-control-plaintext " id="city" style="width: 82%">
                                                            </div>
                                                        </div>
                                                        <style>
                                                        .city1{
                                                            width: 20%;
                                                            float: left;
                                                            padding-left: 15px;
                                                        }
                                                        .city_lbl{
                                                            width:25%;
                                                            float:left;
                                                        }
                                                        .city11{
                                                            width: 75%;
                                                            float: left;
                                                        }
                                                    </style>
                                                        <div class="pin1">
                                                            <label for="pin" class="pin11_lbl">Pin Code :</label>
                                                            <div class="pin11 lversion-padding">
                                                                <input type="text" value=" <?=$student->pin?>" class="form-control-plaintext" id="pin" style="width: 80%;font-size: 12px;">
                                                            </div>
                                                        </div>
                                                         <style>
                                                        .pin1{
                                                            width: 16%;
                                                            float: left;
                                                        }
                                                        .pin11_lbl{
                                                         width: 55%;
                                                            float: left;   
                                                        }
                                                        .pin11{
                                                            width: 45%;
                                                            float: left;
                                                        }
                                                    </style>
                                                        <div class="phone1">
                                                            <label for="phone" class="phone_lbl">Phone (with STD) :</label>
                                                            <div class="phone11">
                                                                <input type="text" class="form-control-plaintext" id="phone">
                                                            </div>
                                                        </div>
                                                        <style>
                                                            .phone1{
                                                            width: 33%;
                                                            float: left;
                                                            
                                                            }
                                                            .phone_lbl{
                                                                width:47%;
                                                                float:left;
                                                            }
                                                            .phone11{
                                                                width: 46%;
                                                                float:left;
                                                            }
                                                        </style>
                                                        <div class="mobile1">
                                                            <label for="mobile" class="mobile11_lbl">Mobile No.:</label>
                                                            <div class="mobile11">
                                                                <input type="text" value=" <?=$student->phone?>" class="form-control-plaintext" id="mobile">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style>
                                                        .mobile1{
                                                            width:30%;
                                                            float: left;
                                                            padding-right: 15px;
                                                        }
                                                        .mobile11_lbl{
                                                          width: 38%;
                                                            float: left;
                                                            
                                                        }
                                                        .mobile11{
                                                            width:62%;
                                                            float: left;
                                                        }
                                                    </style>

                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="nationality" class="national1">6. Nationality :</label>
                                                    <div class="national11">
                                                        <input type="text" value="<?=$student->nationality?>" class="form-control-plaintext" id="nationality">
                                                    </div>
                                                </div>
                                                <style>
                                                    .national1{
                                                        width: 28%;
                                                        float: left;

                                                    }
                                                    .national11{
                                                        width: 72%;
                                                        float: left;
                                                    }
                                                </style>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="email" class="emailid1">Email ID (Student):</label>
                                                    <div class="emailid11">
                                                        <input type="email" value="<?=$student->email?>" class="form-control-plaintext" id="email">
                                                    </div>
                                                </div>
                                                <style>
                                                    .emailid1{
                                                        width: 36%;
                                                        float: left;

                                                    }
                                                    .emailid11{
                                                        width: 62%;
                                                        float: left;
                                                    }
                                                </style>
                                            </div>

                                        </div>
                                    </div>
                                    <br><br>

                                    <div class="row mgt30">
                                        <p class="text-center mgt20 print-office"><strong>(For Office use only)</strong></p>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="officeNo" class="no_1">No.</label>
                                                    <div class="no_11">
                                                        <input type="text" class="" id="officeNo">
                                                    </div>
                                                </div>
                                            </div>
                                            <style>
                                                .no_1{
                                                    width: 5%;
                                                    float: left;
                                                }
                                                .no_11{
                                                    width: 32%;
                                                    float: left;
                                                }
                                            </style>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-4">
                                                    <label for="receivedDate" class="on1">Received on</label>
                                                    <div class="on11">
                                                        <input type="text" class="form-control-plaintext" id="receivedDate">
                                                    </div>
                                                </div>
                                                <style>
                                                    .on1{
                                                        width: 27%;
                                                        float: left;
                                                    }
                                                    .on11{
                                                        width: 30%;
                                                        float: left;
                                                    }
                                                </style>
                                                <div class="col-sm-6 col-md-8 mversion-mgt20">
                                                    <label for="requirements" class="Other1">Other Requirements</label>
                                                    <div class="Other11">
                                                        <input type="text" class="form-control-plaintext" id="requirements">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        .Other1{
                                            width: 42%;
                                            float: left;
                                        }
                                        .Other11{
                                            width: 57%;
                                            float: left;
                                        }
                                    </style>

                                    <!--<input type="button" value="Submit" class="btn btn-primary btn-submit">-->
        
                                    <br>
                                    <div class="row flexFooter mgt20">
                                        <!-- <div class="col-sm-10 col-md-6 footer-text">
                                            <p>Undertaking enclosed[Y][N] <br/> Documents not attached <br /> 1/2/3/4/5/6/7/8/9/10/11</p>
                                        </div> -->
                                        <div class="col-md-12">
                                            <img src="<?php echo base_url()?>images/stu_img/Home_Page_Footer.jpg" alt="20 Years" class="img-responsive" width="200" style="float: right;">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                

                <!--  page 2 print -->


        
                    <img src="<?php echo base_url()?>images/stu_img/Bottom_Design.jpg" alt="Border" width="1170" class="printimg-break">
                    <div class="container wrapper">
                        
                        <form class="form2">
                            <div class="row mgt20"><br>
                                <div class="col-sm-12 col-md-12">
                                    <label class="col-sm-12 col-md-12 col-form-label" style="margin: 0px 0px 8px -33px;">10. Details of Previous / Qualifying Examination (Attached attested Mark Sheets):</label>                                     
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name of Exam</th>
                                                <th>Name of Institution</th>
                                                <th>Board / University</th>
                                                <th>Year of Passing</th>
                                               <th>Marks Obtained</th>
                                                <th>Max. Marks</th>
                                                <th>% Marks / CGPA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                                foreach ($education as $key => $value) 
                                                {
                                            ?> 
                                                    <?php
                                                        if($value->education=='10th Class')
                                                        {
                                                    ?>
                                                            <tr>
                                                                <th>X</th>
                                                                <td>-</td>
                                                                <td style="font-size: 9px; height: 40px;"><?php echo $value->board_name;?></td>
                                                                <td><?php echo $value->year_passing; ?></td>
                                                            
                                                                <td> </td>
                                                                <td> </td>
                                                                <td><?php echo $value->percentage; ?></td>
                                                            </tr>
                                                    <?php        
                                                        }

                                                        if($value->education=='12th Class')
                                                        {
                                                    ?>
                                                           <tr>
                                                                <th>XII</th>
                                                                <td>-</td>
                                                                <td style="font-size: 9px; height: 40px;"><?php echo $value->board_name;?></td>
                                                                <td><?php echo $value->year_passing; ?></td>
                                                            
                                                                <td></td>
                                                                <td></td>
                                                                <td><?php echo $value->percentage; ?></td>
                                                            </tr> 
                                                    <?php
                                                        }
                                                    ?>

                                                     <?php 
                                                } 
                                            ?>

                                            <tr>
                                                <th>Diploma</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>Gradu</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>P.G.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>Others</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            
<tr>
                                                <th>Others</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
 
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row mgt20">
                                <div class="col-sm-12 col-md-12">
                                    <label class="col-sm-12 col-md-12 col-form-label" style="margin: 0px 0px 8px -33px;">11.Details of subject marks of qualifying exam (X/XII/Diploma/Graduation/etc.) :</label>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Subject</th>
                                            <th>Max. Marks</th>
                                            <th>Marks obatined</th>
                                            <th>% Marks / CGPA</th>
                                            <th>% PCM</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>1.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>2.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>3.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>4.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>5.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>6.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>7.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row mgt20">
                                <div class="col-sm-12 col-md-12">
                                    <label for="name" class="" style="width: 60%;float: left;">12. Ever disqualified from studies (if yes, give reasons for the same) : </label> 
                                    <!-- <div class="col-sm-12 col-md-6">
                                        <input type="text" class="form-control-plaintext" id="name">
                                    </div> -->
                                    <div class="col-sm-12 col-md-12">
                                        <input type="text" class="form-control-plaintext" id="name">
                                    </div>
                                </div>
                            </div><br>

                            <div class="row mgt20">
                                <div class="col-sm-6 col-md-6">
                                    <label for="f2name" class="nof1">13. Name of Father :</label>
                                    <div class="nof11">
                                        <input type="text" value="<?=$student->father_name?>" class="form-control-plaintext" id="f2name">
                                    </div>
                                </div>
                                <style>
                                    .nof1{
                                        width: 37%;
                                        float: left;

                                    }
                                    .nof11{
                                        width: 55%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6">
                                    <label for="m2name" class="mof1">Name of Mother :</label>
                                    <div class="mof11">
                                        <input type="text"  value="<?=$student->mother_name?>" class="form-control-plaintext" id="m2name">
                                    </div>
                                </div><br>
                                <style>
                                    .mof1{
                                        width: 33%;
                                        float: left;

                                    }
                                    .mof11{
                                        width: 55%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="foccupation" class="off1">Occupation of Father :</label>
                                    <div class="off11">
                                        <input type="text" class="form-control-plaintext" id="foccupation">
                                    </div>
                                </div>
                                <style>
                                    .off1{
                                        width: 42%;
                                        float: left;

                                    }
                                    .off11{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="moccupation" class="occfm11">Occupation of Mother :</label>
                                    <div class="occfm111">
                                        <input type="text" class="form-control-plaintext" id="moccupation">
                                    </div>
                                </div><br>
                                <style>
                                    .occfm11{
                                        width: 42%;
                                        float: left;

                                    }
                                    .occfm111{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="moffce" class="of1">Office Address :</label>
                                    <div class="of11">
                                        <input type="text" class="form-control-plaintext" id="moffce">
                                    </div>
                                </div>
                                <style>
                                    .of1{
                                        width: 31%;
                                        float: left;

                                    }
                                    .of11{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="foffce" class="ofm1">Office Address :</label>
                                    <div class="ofm11">
                                        <input type="text" class="form-control-plaintext" id="foffce">
                                    </div>
                                </div><br>
                                <style>
                                    .ofm1{
                                        width: 31%;
                                        float: left;

                                    }
                                    .ofm11{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="fmobile" class="mob1">Mobile No. :</label>
                                    <div class="mob11">
                                        <input type="text" class="form-control-plaintext" id="fmobile">
                                    </div>
                                </div>
                                <style>
                                    .mob1{
                                        width: 23%;
                                        float: left;
                                    }
                                    .mob11{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="mmobile" class="mob2">Mobile No. :</label>
                                    <div class="mob22">
                                        <input type="text" class="form-control-plaintext" id="mmobile">
                                    </div>
                                </div><br>
                                <style>
                                    .mob2{
                                        width: 23%;
                                        float: left;
                                    }
                                    .mob22{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="femail" class="emid1">Email ID :</label>
                                    <div class="emid11">
                                        <input type="text" class="form-control-plaintext" id="femail">
                                    </div>
                                </div>
                                 <style>
                                    .emid1{
                                        width: 18%;
                                        float: left;
                                    }
                                    .emid11{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>

                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="memail" class="emid2">Email ID :</label>
                                    <div class="emid22">
                                        <input type="text" class="form-control-plaintext" id="memail">
                                    </div>
                                </div>
                                <br><br>
                                <style>
                                    .emid2{
                                        width: 18%;
                                        float: left;
                                    }
                                    .emid22{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                            </div><br>
        
                            <div class="row mgt20">
                                <div class="col-sm-8 col-md-6">
                                    <label for="guardian" class="nolg1">14. Name of Local Guardian (if any)</label>
                                    <div class="nolg11">
                                        <input type="text" class="form-control-plaintext" id="guardian">
                                    </div>
                                </div><br>
                                <style>
                                    .nolg1{
                                        width: 47%;
                                        float: left;
                                    }
                                    .nolg11{
                                        width: 50%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-4 col-md-6">
                                    <label for="relationship" class="rel_1">Relationship :</label>
                                    <div class="rel_11">
                                        <input type="text" class="form-control-plaintext" id="relationship">
                                    </div>
                                </div>
                                <style>
                                    .rel_1{
                                        width: 39%;
                                        float: left;
                                    }
                                    .rel_11{
                                        width: 60%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="loccupation" class="oog1">Occupation of Guardian</label>
                                    <div class="oog11">
                                        <input type="text" class="form-control-plaintext" id="loccupation">
                                    </div>
                                </div>
                                <style>
                                    .oog1{
                                        width: 45%;
                                        float: left;
                                    }
                                    .oog11{
                                        width: 55%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="laddress" class="add11">Address :</label>
                                    <div class="add111">
                                        <input type="text" class="form-control-plaintext" id="laddress">
                                    </div>
                                </div><br>
                                <style>
                                    .add11{
                                        width: 20%;
                                        float: left;
                                    }
                                    .add111{
                                        width: 60%;
                                        float: left;
                                    }
                                </style>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="lmobile" class="mob1">Mobile No. :</label>
                                    <div class="mob11">
                                        <input type="text" class="form-control-plaintext" id="lmobile">
                                    </div>
                                </div>
                                <style>
                                    .mob1{
                                        width: 22%;
                                        float: left;
                                    }
                                    .mob11{
                                        width: 60%;
                                        float: left;
                                    }
                                </style>

                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="lemail" class="e_1">Email ID :</label>
                                    <div class="e_11">
                                        <input type="email" class="form-control-plaintext" id="lemail">
                                    </div>
                                </div><br>
                                <style>
                                    .e_1{
                                        width: 18%;
                                        float: left;
                                    }
                                    .e_11{
                                        width: 60%;
                                        float: left;
                                    }
                                </style>
                            </div>
                            <br><br>
    
                            <div class="row mgt20">
                                <div class="col-md-12">
                                    <label for="annualIncome" class="aiopg1">15. Annual Income of Parents/Guardian :</label>
                                    <div class="aiopg11">
                                        <input type="text" class="form-control-plaintext" id="annualIncome">
                                    </div>
                                </div>
                            </div>
                            <style>
                                    .aiopg1{
                                        width: 35%;
                                        float: left;
                                    }
                                    .aiopg11{
                                        width: 65%;
                                        float: left;
                                    }
                                </style>
                        </form>
                        <div style="padding-bottom: 5%;"></div>
                        <img src="<?php echo base_url()?>images/stu_img/Top_Design.jpg" alt="" width="1170" class="printimg-bottom_break">
                    </div>
                    <div style="padding-bottom: 6%;"></div>


                <!-- page 3 print -->



<style type="text/css">
    .kjkd li{
list-style:none;
    }
</style>
                <img src="<?php echo base_url()?>images/stu_img/Top_Design.jpg" alt="Border" width="1170" class="printimg-break">
                <div class="container wrapper">
                    <form class="">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h4 class="text-uppercase text-center" style="font-weight: bold">
                                    <u>Undertaking by the student / parent / guardian</u>
                                </h4>
                                <div class="paddingprint20" style="text-align: justify;">
                                <ol class="mgt20 kjkd">
                                    <li><div style="width:2%;float:left;">1.</div><div style="width:98%;float:left;">I do hereby certify that the particulars furnished in the application form are true correct. I also agree that admission may be granted as per rules, regulations, terms and conditions of the University. I fully understand that admission will be cancelled and fee paid by me will be forfeited, if any of the particulars given by me at the time of admission or at later stage, are found to be incorrect,concealed or false.</div></li>

                                    <li><div style="width:2%;float:left;">2.</div><div style="width:98%;float:left;">I agree that Vice Chancellor decision in all matters concerning my admission, studies, discipline and conduct will be final and binding upon me.</div></li>

                                    <li><div style="width:2%;float:left;">3.</div><div style="width:98%;float:left;">I understand that the student's association, active or passive, with any unlawful organization unions etc, is forbidden and I shall not have any objection, if I am expelled or rusticated from the University due to my participation in any union/unlawful activity or any other activity which violates code of conduct of the University.</div></li>

                                    <li><div style="width:2%;float:left;">4.</div><div style="width:98%;float:left;">I understand that the consumption of alcohol, drugs or any other intoxicant within the University 
                                    /Institute/Hostel premises is forbidden.</div></li>

                                    <li><div style="width:2%;float:left;">5.</div><div style="width:98%;float:left;">I am aware that as per Hon'ble Supreme Court orders, ragging in any form whatsoever is totally  banned and criminal FIR will be lodged in case, I Am involved in any case of ragging. This will result in any rustication from the University and I will not be able to take admission anywhere in India.</div></li>

                                    <li><div style="width:2%;float:left;">6.</div><div style="width:98%;float:left;">I hereby declare that I was never involved or punished in any case of indiscipline during my school or college career. There is no inquiry pending against me with the school/college/police/other authorities of the state to which I belong or resided in any other state at any time.</div></li>

                                    <li><div style="width:2%;float:left;">7.</div><div style="width:98%;float:left;">I Confirm that I have submitted the original certificates to the University on my own will and shall be returned to me after the same are verified by the University. In case, I discontinue or leave the University in between the duration of the course, I shall have to pay the fee for the remaining duration of the course. Failing to do so the University can take any legal action against me.</div></li>

                                    <li><div style="width:2%;float:left;">8.</div><div style="width:98%;float:left;">I agree, that if any component of fee is revised by the competent authority, the enhanced amount if any, shall be deposited within two weeks of issue of such notification. I understand that the fee has to be paid in advance semester wise.</div></li>

                                    <li><div style="width:2%;float:left;">9.</div><div style="width:98%;float:left;">I understand that I have to undergo mandatory practical training for specified periods in accordance with the relevance of me being placed in an industrial organization within or outside the state in which campus is located, for such practical training. I will not have any objection to my being placed so. The University shall not be held responsible in any way for any mishap that may occur to me during my tenure in the University/Institute/Hostel or during the training outside the University.</div></li>

                                    <li><div style="width:3%;float:left;">10.</div><div style="width:97%;float:left;">I hereby agree to make all the necessary arrangements for the Boarding and Lodging, if hostel accommodation is not available or availed.</div></li>

                                    <li><div style="width:3%;float:left;">11.</div><div style="width:97%;float:left;">I am aware that my name will be stuck off from the rolls of the University, if I fail to pay fee on time, remain absent continuously for a period of 10 days without information or behave in a manner subversive to the discipline or commit grave misconduct such as ragging or indulge in any activity which may bring disrepute to the University.</div></li>

                                    <li><div style="width:3%;float:left;">12.</div><div style="width:97%;float:left;">I am aware that the use of mobile phones in the University premises is banned. In case I violate this, strict action will be taken against me as per the disciplinary rules of the University.</div></li>

                                    <li><div style="width:3%;float:left;">13.</div><div style="width:97%;float:left;">I shall acknowledge that my testimonials and photographs taken during functions/events/workshops/Seminars are deemed to be the property of the University. University has the right to the usage of students photograph in the University website, brochure and other related publications and materials.</div></li>

                                    <li><div style="width:3%;float:left;">14.</div><div style="width:97%;float:left;">I understand that I have to follow all University Rules and Regulations in the subjects of attendance, examination, discipline and  in all other matters.</div></li>

                                    <li><div style="width:3%;float:left;">15.</div><div style="width:97%;float:left;">I will report for Registration as per the schedule announced by the University failing which my admission will stand cancelled without any intimation from the University.</div></li>

                                    <li><div style="width:3%;float:left;">16.</div><div style="width:97%;float:left;">I understand that in the event of breach of any of the aforementioned conditions of the undertaking my admission will stand  cancelled without any obligation on the part of the University to issue show cause notice. In such case, the fee other than the refundable amount shall stand forfeited.</div></li>

                                    <li><div style="width:3%;float:left;">17.</div><div style="width:97%;float:left;">In case, I avail the University transport facility, the University is not responsible for any mishap or discontinuation of bus service. I shall be availing the transport facility at my own risk and peril. The necessary undertaking in this regard shall be submitted as and when I start availing the transport facility.</div></li>

                                    <li><div style="width:3%;float:left;">18.</div><div style="width:97%;float:left;">l agree that I have taken admission in Lingaya's Institution after understanding all the admission procedures of the University.</div></li>

                                    <li><div style="width:3%;float:left;">19.</div><div style="width:97%;float:left;">I would be attending the classes regularly as I am aware that 75% attendance is compulsory in every subject/course.</div></li>

                                    <li><div style="width:3%;float:left;">20.</div><div style="width:97%;float:left;">The use of Laptop is compulsory and I would be using laptop regularly during the full duration of course.</div></li>

                                    <li><div style="width:3%;float:left;">21.</div><div style="width:97%;float:left;">Refund policy is governed as per the process given in the brochure or given on the University website. I have read & understood the FEE REFUND POLICY of the University.</div></li>

                                    <li><div style="width:3%;float:left;">22.</div><div style="width:97%;float:left;">I have received the copy of Brochure along with this form.</div></li>
                                </ol>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-md-6 signature">
                                <p class="text-left" style="margin-top: 0px;margin-left: 67px;"><strong>Signature of Parents/Guardian</strong></p>

                            </div>
                            <div class="col-sm-6 col-md-6 signature photo2"  style="padding-left: 250;margin-top: -30px; ">
                                                <p class="text-right" style="margin: 0px;"><?php 
                                                    $image_properties = array(
                                                    'src'=>base_url('uploads/signature/'.$student->signature),
                                                    'height'=>'50',
                                                    'class'=>'main_ffg'
                                                );
                                                    echo img($image_properties);
                                                ?></p>
                                  <!-- <div class="col-md-2" style="height: 70px;"><?=img(base_url('uploads/signature/'.$student->signature))?></div> -->
                                                
                                             
                                <p class="text-right" style="margin: 0px;"><strong>Signature of Student</strong></p>
                            </div>
                        </div>
                        <img src="<?php echo base_url()?>images/stu_img/Bottom_Design.jpg" alt="Border" width="1170" class="printimg-break">

                    </form>
                    <!-- <div style="padding-bottom: 2%"></div> -->
                    <!-- <img src="<?php echo base_url()?>images/stu_img/Top_Design.jpg" alt="" width="1170" class="printimg-bottom_break"> -->
                </div>
                <!-- <div style="padding-bottom: 10%"></div> -->
<style type="text/css">
    .main_ffg{
        margin-right: 33px;
        /*width: 30%;*/

    }
</style>

                <!-- page 4 print -->




                <img src="<?php echo base_url()?>images/stu_img/Bottom_Design.jpg" alt="Border" width="1170" class="printimg-break">
                <div class="container wrapper">
                    <form class="form4">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h4 class="text-uppercase text-center" style="font-weight: bold"><u>Declaration</u></h4>
                                <p class="mgt20" style="text-align: justify;">I hereby solemnly confirm that I have read the University Prospectus and have clearly understood the
                                    points mentioned in the undertaking. I shall abide by the rules and regulations of the University as
                                    mentioned in the prospectus and also the changes which may be notified from time to time. I further
                                    confirm that the information given in the application form given by me is correct and true to the best of
                                    my knowledge.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-8">
                                <p class="text-right signature">Signature of the candidate</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>I shall abide by the conditions of the undertaking duly signed by my ward.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-8">
                                <p class="text-right signature">Signature of Parent/Guardian</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="width_30_place">
                                <label for="place" class="place1">Place:</label>
                                <div class="place11">
                                    <input type="text" class="form-control-plaintext"id="place" style="margin:  -15px;">
                                </div>
                            </div>
                            <style>
                                .width_30_place{
                                                    width: 26%;
                                    float: left;                
                                }
                                .place1{
                                    width: 30%;
                                    float: left;
                                    /*margin-left: 15px;*/
                                }
                                .place11{
                                    width: 70%;
                                    float: left;
                                }
                            </style>
                            <div class="widt_ad_70">
                                <label for="address" class="widt_ad_70_1">Address:</label>
                                <div class="place111">
                                    <input type="text" class="" id="address"style=" margin: -15px 0px 0px -79px">
                                </div>
                            </div>
                            <style>
                                .widt_ad_70{
                                    width: 60%;
                                    float: left;
                                    
                                }
                                .place111{
                                    width: 49%;
                                    float: left;
                                }
                                .widt_ad_70_1{
                                    width: 30%;
                                    float: left;
                                }
                            </style>

                        </div>
                        <div class="row">
                            <div class="width_date_30">
                                <label for="date" class="date_ffff">Date:</label>
                                <div class="width_date_30_22">
                                    <input type="text" class="input_1 form-control-plaintext" id="date">
                                </div>
                            </div>
                            <style>
                                .width_date_30{
                                    width: 25%;
                                    float: left;
                                    /*margin-left: 15px;*/
                                }
                                .width_date_30_22{
                                    width: 70%;
                                    float: right;
                                }
                                .date_ffff{
                                    width: 30%;
                                    float: left;
                                }
                                .input_1{
                                    width: 70%;
                                    float: left;
                                }
                            </style>
                            <!-- <div class="">
                                <div class="width_date_30_1">
                                    <input type="text" class="width_date_30_1_11" id="date">
                                </div>
                            </div> -->
                            <style>
                                .width_date_30_1{
                                    width: 23%;
                                    float: left;
                                    margin-left: 110px;
                                }
                                .width_date_30_1_11{
                                    width: 50%;
                                    float: left;
                                }
                            </style>
                        </div>
                        <br><br>
                            <div class="row mgt20 printimg-break">
                                <div class="col-md-12">
                                    <p>Following Attested Copies are enclosed with the Application Form: (Tick appropriate one's)</p>
                                    <ol class="kjkd">
                                        <li><div style="width:2%;float:left;">1.</div><div style="width:98%;float:left;">Result Card of any other entrance examination</div></li>

                                        <li><div style="width:2%;float:left;">2.</div><div style="width:98%;float:left;">Admit Card of entrance exam.</div></li>

                                        <li><div style="width:2%;float:left;">3.</div><div style="width:98%;float:left;">Matriculation Certificate/Proof of age.</div></li>

                                        <li><div style="width:2%;float:left;">4.</div><div style="width:98%;float:left;">Detailed Marks Card (10+2) or Equivalent Exam.</div></li>

                                        <li><div style="width:2%;float:left;">5.</div><div style="width:98%;float:left;">Detailed Marks Card of /Diploma /Graduation /or Equivalent Exam.</div></li>

                                        <li><div style="width:2%;float:left;">6.</div><div style="width:98%;float:left;">Character Certificate from Institution of Last attended.</div></li>

                                        <li><div style="width:2%;float:left;">7.</div><div style="width:98%;float:left;">Migration/School Leaving Certificate.</div></li>

                                        <li><div style="width:2%;float:left;">8.</div><div style="width:98%;float:left;">Domicile Certificate/ Affidavit/ Aadhar Card</div></li>

                                        <li><div style="width:2%;float:left;">9.</div><div style="width:98%;float:left;">Passport (if applicable)</div></li>

                                        <li><div style="width:2%;float:left;">9.</div><div style="width:98%;float:left;">Medical Certificate (as per attached format)</div></li>

                                        <li><div style="width:3%;float:left;">10.</div><div style="width:97%;float:left;">Six colored and two black passport size photographs.</div></li>
                                    </ol>
                                    <p  style="font-weight: bold;">Note:</p>
                                    <ul style="text-align: justify;">
                                        <li>Incomplete application forms shall not be accepted</li>
                                        <li>It must be understood that the mere submission of the form, in no way confirms admission to the
                                            Institution. After the completion of the admission process by the competent authority the admission of
                                            lapse seats shall be made the sole discretion of the Governing Body of the Institution in accordance with the relevant Rules &amp; Regulations of the Competent Authority.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <img src="<?php echo base_url()?>images/stu_img/Last_Page_Center.jpg" alt="Lingaya's Group of Institutions" width="1000" class="banner">
                                </div>
                            </div><br><br>
                            <div class="row printimg-break">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Vidyapeeth,</strong><br/>
                                            Faridabad<br/>
                                            <i>(Approved by MHRD/AICTE/ PCI/ 
                                            BCI / COA/NCTE, Govt. of India
                                                u/s 3 of UGC Act 1956)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Lalita Devi Institute
                                            of Management &amp; Sciences,</strong><br/>
                                            Mandi, New Delhi<br/>
                                            <i>(Approved by Govt. of NCT of
                                                Delhi and Affiliated to GGSIP
                                                University)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Institute of
                                            Management &amp; Technology,</strong><br/>
                                            Near Nunna, Vijayawada (A.P)<br/>
                                            <i>(Approved by AICTE Govt. of A.P.
                                                and Affiliated to JNTU)</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Institute of Health
                                            Sciences,Faridabad</strong><br/>
                                            <i>(Approved by Indian Nursing
                                                Council & Govt. of Haryana and
                                                Affiliated to Pt. B.D. Sharma
                                                University)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Public School,</strong><br/>
                                            Faridabad<br/>
                                            <i>(Affiliated to CBSE & Approved by
                                                Govt. of Haryana)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Academy,</strong><br/>
                                            Saket, New Delhi<br/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                <img src="<?php echo base_url()?>images/stu_img/Last_Page_Footer.jpg" alt="" width="1170" class="printimg-bottom_break">
                </div>
                </body>
    
     </html>
     
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <script language="javascript" type="text/javascript">

        function printDiv(divID) {
            //Get the HTML of div

            var divElements = document.getElementById(divID).innerHTML;

            //Get the HTML of whole page

            var oldPage = document.body.innerHTML;



            //Reset the page's HTML with div's HTML only

            document.body.innerHTML =

              "<html><head><title></title></head><body>" +

              divElements + "</body>";



            //Print Page

            window.print();



            //Restore orignal HTML

            document.body.innerHTML = oldPage;

        }

</script>

