  <style type="text/css">

table{
}
.logo h1{
    margin: 0;
}
.logo h1 a{
    color: #000;
    font-size: 20px;
    font-weight: 700;
    text-transform: uppercase;
    font-family: 'Montserrat', sans-serif;
}


.heading-section{
}
.heading-section h2{
    color: #000000;
    font-size: 28px;
    margin-top: 0;
    line-height: 1.4;
}
.heading-section .subheading{
    margin-bottom: 20px !important;
    display: inline-block;
    font-size: 13px;
    text-transform: uppercase;
    letter-spacing: 2px;
    color: rgba(0,0,0,.4);
    position: relative;
}
.heading-section .subheading::after{
    position: absolute;
    left: 0;
    right: 0;
    bottom: -10px;
    content: '';
    width: 100%;
    height: 2px;
    background: #f3a333;
    margin: 0 auto;
}

th, td {
    white-space: normal !important;
}
  </style>

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>

                <li class="active"><?=$this->lang->line('menu_view')?></li>
            </ol>
        </div>
    </div>

    <?php 

    $usertype = $this->session->userdata("usertype");

    if($usertype == "Admin"  ) {

    ?>

    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body"> 

            <div class="col-sm-6">

                <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>

                <?php

                 echo btn_add_pdf('notice/print_preview/'.$notice->noticeID, $this->lang->line('pdf_preview')) 

                ?>

                <?php echo btn_sm_edit('notice/edit/'.$notice->noticeID, $this->lang->line('edit')) 

                ?>  

                <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?=$this->lang->line('mail')?></button>

            </div> 

    </div>


<?php } ?>

<div class="container-fluid">
    <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body"> 

                <div class="col-sm-12">

                    
                    <center style="width: 100%; background-color: white;">
    <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
      &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
        <tr>
          </tr>

        
          <tr>
              <td class="bg_white">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <hr>
                 
                 
                 
                  <tr>
                    <td class="bg_white email-section">
                        <div class="heading-section" style="text-align: center; padding: 0 30px;">
                            <span class="subheading">Description</span>
                        <h2><?php echo $notice->title; ?></h2>
                        <p><?php echo $notice->notice; ?>.</p>
                        <?php echo date("d M Y", strtotime($notice->date)); ?>

                        </div>
                        
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
      </table>
      

    </div>
  </center>

                </div>

            </div>

        </div>

    </div>

</div>
</div>


<form class="form-horizontal" role="form" action="<?=base_url('notice/send_mail');?>" method="post">

    <div class="modal fade" id="mail">

      <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>

            </div>

            <div class="modal-body">

            

                <?php 

                    if(form_error('to')) 

                        echo "<div class='form-group has-error' >";

                    else     

                        echo "<div class='form-group' >";

                ?>

                    <label for="to" class="col-sm-2 control-label">

                        <?=$this->lang->line("to")?>

                    </label>

                    <div class="col-sm-6">

                        <input type="email" class="form-control" id="to" name="to" value="<?=set_value('to')?>" >

                    </div>

                    <span class="col-sm-4 control-label" id="to_error">

                    </span>

                </div>



                <?php 

                    if(form_error('subject')) 

                        echo "<div class='form-group has-error' >";

                    else     

                        echo "<div class='form-group' >";

                ?>

                    <label for="subject" class="col-sm-2 control-label">

                        <?=$this->lang->line("subject")?>

                    </label>

                    <div class="col-sm-6">

                        <input type="text" class="form-control" id="subject" name="subject" value="<?=set_value('subject')?>" >

                    </div>

                    <span class="col-sm-4 control-label" id="subject_error">

                    </span>



                </div>



                <?php 

                    if(form_error('message')) 

                        echo "<div class='form-group has-error' >";

                    else     

                        echo "<div class='form-group' >";

                ?>

                    <label for="message" class="col-sm-2 control-label">

                        <?=$this->lang->line("message")?>

                    </label>

                    <div class="col-sm-6">

                        <textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?=set_value('message')?>" ></textarea>

                    </div>

                </div>
          
            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>

                <input type="button" id="send_pdf" class="btn btn-success" value="<?=$this->lang->line("send")?>" />

            </div>

        </div>

      </div>

    </div>

</form>


<script language="javascript" type="text/javascript">

    function printDiv(divID) {

        

        var divElements = document.getElementById(divID).innerHTML;

        

        var oldPage = document.body.innerHTML;

       
        document.body.innerHTML = 

          "<html><head><title></title></head><body>" + 

          divElements + "</body>";

        

        window.print();

        

        document.body.innerHTML = oldPage;

    }

    function closeWindow() {

        location.reload(); 

    }

    function check_email(email) {

        var status = false;     

        var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

        if (email.search(emailRegEx) == -1) {

            $("#to_error").html('');

            $("#to_error").html("<?=$this->lang->line('mail_valid')?>").css("text-align", "left").css("color", 'red');

        } else {

            status = true;

        }

        return status;

    }

    $("#send_pdf").click(function(){

        var to = $('#to').val();

        var subject = $('#subject').val();

        var message = $('#message').val();

        var id = "<?=$notice->noticeID;?>";

        var error = 0;

        if(to == "" || to == null) {

            error++;

            $("#to_error").html("");

            $("#to_error").html("<?=$this->lang->line('mail_to')?>").css("text-align", "left").css("color", 'red');

        } else {

            if(check_email(to) == false) {

                error++

            }

        } 

        if(subject == "" || subject == null) {

            error++;

            $("#subject_error").html("");

            $("#subject_error").html("<?=$this->lang->line('mail_subject')?>").css("text-align", "left").css("color", 'red');

        } else {

            $("#subject_error").html("");

        }

        if(error == 0) {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('notice/send_mail')?>",

                data: 'to='+ to + '&subject=' + subject + "&id=" + id+ "&message=" + message,

                dataType: "html",

                success: function(data) {

                    location.reload();

                }

            });

        }

    });

</script>
