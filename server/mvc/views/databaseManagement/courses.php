    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa icon-template"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active"><?=$this->lang->line('menu_feetype')?></li>
            </ol>
        </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="card-body">

                    <div class="col-sm-12">



                <h5 class="page-header">

         <!--            <a href="<?php echo base_url('feetype/add') ?>">

                        <i class="fa fa-plus"></i> 

                        <?=$this->lang->line('add_title')?>

                    </a> -->

                </h5>

<style type="text/css">
    .forCoursesData > li{
    background: #d6d6d647;
    padding: 1%;
    border: 2px solid#eee;
    margin-bottom: 31px;
    list-style:none;
    }
     .forCoursesData li{
        list-style:none;
     }

</style>
<?php 
$usertype = $this->session->userdata("usertype");
if($usertype == "superadmin"){ ?>
<form method ="post" action="<?php echo base_url() ?>databaseManagement/insertCopyCourses">
<ul class="forCoursesData">
<?php
foreach ($courses as $row) { ?>
    <li>
    <div class="col-md-3">
        <label>Course Name(<?php echo $row->courseCode ?>)</label><br>

         <input type="hidden"  name="education_detailsID[]" value="<?php  echo $row->education_detailsID ?>"><input type="hidden"  name="classesID[]" value="<?php  echo $row->classesID ?>"><input type="text" name="courses[]" class="form-control" value="<?php echo $row->classes ?>">
    </div>
        <div class="col-md-3">
        <label>Duration</label><br>
       <input type="text" name="duration[]" class="form-control" value="<?php echo $row->duration ?>">
    </div>

        <div class="col-md-3">
        <label>Mode</label><br>
         <select name="mode[]" class="form-control">
             <option value="1" <?php if($row->mode==1){echo "Selected";} ?> >Year</option>
             <option value="2" <?php if($row->mode==2){echo "Selected";} ?> >Semester</option>
         </select>
    </div>
        <div class="col-md-3">
        <label>Fee</label><br>
        <input type="text" name="fee[]" class="form-control" value="<?php echo $row->fee ?>">
    </div>

<div class="clearfix"></div>

    <ul>
    <?php
    $this->db->where('classesID',$row->classesID);
    $query2  =  $this->db->get('sub_courses');
    $data2  = $query2->result();
    foreach ($data2 as $key => $value) { ?>
    <li><label><?php  echo $value->subCourseCode ?></label><input type="hidden"  name="subcoursesID[]" value="<?php  echo $row->classesID ?>"><input type="text" class="form-control" name="subcourses[]" value="<?php echo $value->sub_course  ?>"></li>
     <?php } ?>
    </ul>
    </li>

<?php } ?>

<select name="adminID" required = 'required'>
    <option value="">Select Admins</option>
    <?php
    foreach ($admins as $key => $value) {
     ?>
      <option value="<?php echo $value->adminID ?>"><?php echo $value->name ?></option>
     <?php } ?>
</select>
<input type="submit" name="submit" value="Copy All Courses">    
</form>
<?php  } ?>
<!-- for ClgAdmin -->
<?php if ($usertype == 'ClgAdmin') { ?>
<div class="col-sm-12">

                       
                    <h5 class="page-header">

<ul class="pannleSearchHeading">
<li>
                        <a href="#" id="button_bootstrapModalMail">

                            <i class="fa fa-plus"></i> 

                            Add New Course
                        </a>
 </li>
    </ul>
                    </h5>
                <div class="col-sm-4 list-group for-formGroup">

                    <div class="list-group-item list-group-item-warning">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="col-sm-3 control-label">

                                    Course
                                </label>

                                <div class="col-sm-9">
                                    <select name="classesID" id="classesID" onchange="Ajax_coursesFilterDatamanagement($(this).val())" class="form-control">
<option value="0">Select Course</option>
<?php
foreach ($coursesList as $key => $value) {
    if ($this->session->userdata('sessionCourseDataManagement')) {
       if ($value->classesID==$this->session->userdata('sessionCourseDataManagement')) {
           $select =  "selected";
        }else{
            $select =  "";
        }
    }else{
        $select =  "";
    }
 ?>
 <option value="<?php echo $value->classesID ?>" <?php echo $select ?>><?php echo $value->classes ?></option>
 <?php } ?>
</select>
                                </div>
                                 <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCoursesForDataManagement()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>
            <div class="clearfix"></div>
<ul class="forCoursesData">
<?php
$i = 1;
$m =1;
$n = 1;
foreach ($courses as $row) { 
$j = $i++;
    ?>
    <li>
    <div class="col-md-3">
        <label>Course Name(<?php echo $row->courseCode ?>)</label><br>

         <input type="hidden"  name="education_detailsID[]" value="<?php  echo $row->education_detailsID ?>">
         <input type="hidden"  name="classesID[]" value="<?php  echo $row->classesID ?>">
         <div class="positonRelative">
         <input type="text" onclick="onclickEditCourses($m++)" onblur="EditCoursesDatamanagement()" name="courses[]" class="form-control" value="<?php echo $row->classes ?>">
         <div class="positionAbsolute" id="displayNone<?php echo $n ?>">
             <i class="fa fa-pencil" aria-hidden="true"></i>
         </div>
        </div>
    </div>
        <div class="col-md-3">
        <label>Duration</label><br>
        <div class="positonRelative">
       <input type="text" name="duration[]" onclick="onclickEditCourses()" onblur="EditCoursesDatamanagement()" class="form-control" value="<?php echo $row->duration ?>">
        <div class="positionAbsolute">
             <i class="fa fa-pencil" aria-hidden="true"></i>
         </div>
   </div>
    </div>

        <div class="col-md-3">
        <label>Mode</label><br>
         <div class="positonRelative">
         <select name="mode[]" class="form-control" onclick="onclickEditCourses()" onblur="EditCoursesDatamanagement()">
             <option value="1" <?php if($row->mode==1){echo "Selected";} ?> >Year</option>
             <option value="2" <?php if($row->mode==1){echo "Selected";} ?> >Semester</option>
         </select>
         <div class="positionAbsolute">
             <i class="fa fa-pencil" aria-hidden="true"></i>
         </div>
     </div>
    </div>
        <div class="col-md-3">
        <label>Fee</label><br>
        <div class="positonRelative">
        <input type="text" name="fee[]" onclick="onclickEditCourses()" onblur="EditCoursesDatamanagement()" class="form-control" value="<?php echo $row->fee ?>">
        <div class="positionAbsolute">
             <i class="fa fa-pencil" aria-hidden="true"></i>
         </div>
        </div>
    </div>

<div class="clearfix"></div>

    <ul id="sub_coursesIDs<?php echo $j ?>" class="subcourses">
    <?php
    $this->db->where('classesID',$row->classesID);
    $query2  =  $this->db->get('sub_courses');
    $data2  = $query2->result();
    foreach ($data2 as $key => $value) { ?>
    <li>
    <div class="col-md-2"><label><?php  echo $value->subCourseCode ?></label><input type="hidden"  name="subcoursesID[]" value="<?php  echo $row->classesID ?>">
    </div>  
   <div class="col-md-8">
    <div class="positonRelative">
        <input type="text" class="form-control" name="subcourses[]" value="<?php echo $value->sub_course  ?>">
    <div class="positionAbsolute">
             <i class="fa fa-pencil" aria-hidden="true"></i>
    </div>
    </div>
        </div>
        <div class="col-md-2"><button class="btn btn-remove btn-danger" type="button">
    <span class="glyphicon glyphicon-minus" onclick="DeleteSubCourseDatamanament(<?php echo $row->classesID?>,<?php echo $value->sub_coursesID?>, <?php echo $j ?>)"></span></button></div>
    <div class="clearfix"></div>
    </li>
     <?php } ?>
     <li><div class="form-group fvrduplicate"><div class="fvrclonned">
            <label for="classes" class="col-sm-2 control-label">
                Add Sub Courses 
            </label>

            <div class="col-sm-6">
                <input type="text" class="form-control add" id="subCoursesName<?php echo $j ?>" name="subcourse[]" placeholder="Add Sub Course here" value="">
            </div>

            <span><button class="btn btn-success btn-add" type="button" onclick="AddSubCourseDatamanament(<?php echo $row->classesID ?>,<?php echo $j ?>)"><span class="glyphicon glyphicon-plus"></span></button></span></div></div></li>


    </ul>
    </li>

<?php } ?>

<?php } ?>

            </div>

        </div>

    </div>

</div>


<!-- extra lines code html -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Recipient:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
    .subcourses li{
        margin-bottom:10px;
        margin-top:4px;
    }
    .positonRelative{
        position:relative;
    }
    .positionAbsolute{
position: absolute;
    top: 6px;
    right: 9px;
    font-size: 16px;
    }
</style>

<script type="text/javascript">
    function Ajax_coursesFilterDatamanagement(id){
    $.ajax({
        type: "POST",
        url:"<?=base_url('databaseManagement/Courses')?>",
        data:{"id":id},
        success: function(response) {
            location.reload();
        }
            });

    }

</script>

<script type="text/javascript">
    function ResetCoursesForDataManagement(){
    $.ajax({
        type: "POST",
        url:"<?=base_url('databaseManagement/ResetCoursesForDataManagement')?>",
        success: function(response) {
            location.reload();
        }
            });

    }
    
</script>

<script type="text/javascript">
function  DeleteSubCourseDatamanament(classesID,sub_coursesID,ids){
    $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerDataManagement/DeleteSubCourseDatamanament')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_coursesID,"ids":ids},
        success: function(response) {
    
            $("#sub_coursesIDs"+ids).html(response);
        }
            });

  }

</script>



<script type="text/javascript">
function AddSubCourseDatamanament(classesID,ids){
    var subCoursesName =  $('#subCoursesName'+ids).val();

    $.ajax({
        type: "POST",
        url:"<?=base_url('ajaxControllerDataManagement/AddSubcourse')?>",
        data:{"classesID":classesID,"ids":ids,"subCoursesName":subCoursesName},
        success: function(response) {
        
            $("#sub_coursesIDs"+ids).html(response);
        }
            });

  }

</script>
<form class="form-horizontal" role="form" action="<?=base_url('classes/add');?>" method="post">
<input type="hidden" name="popup" value="popup">
    <div class="modal fade" id="mail">

      <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                <h4 class="modal-title"><?=$this->lang->line('mail')?></h4>

            </div>

            <div class="modal-body">



                    <?php 

                        if(form_error('classes')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classes" class="col-sm-2 control-label">

                            <?=$this->lang->line("courses_name")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" required = "required" placeholder="<?=$this->lang->line("courses_name")?>" id="classes" name="classes" value="<?=set_value('classes')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classes'); ?>

                        </span>
                        </div>

                    <?php 

                        if(form_error('education_details[]')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classes" class="col-sm-2 control-label">

                            Eductions Qualification <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6 ulliclass">
                          <ul>
                          <?php 
                          foreach ($education_details as $key => $value) {
                            
                                ?>
                                <li>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="education_details[]" value="<?php echo $value->education_detailsID ?>"><?php echo $value->education ?></label>
                                    </div>
                                </li>
                            <?php } ?>    
                                                          
                          </ul>
                            

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('education_details'); ?>

                        </span>
                    </div>

                
               

                    <?php 

                        if(form_error('duration')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-2 control-label">

                            <?=$this->lang->line("duration")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                        <input type="number" class="form-control" min="1" max="10" required = "required" placeholder="<?=$this->lang->line("duration")?>" style="resize:none;" id="duration" name="duration"><?=set_value('duration')?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('duration'); ?>

                        </span>

                    </div>


<!-- end time duration -->




<!-- time mode -->

                    <?php 

                        if(form_error('mode')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-2 control-label">

                            <?=$this->lang->line("mode")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">


                     <select class="form-control" placeholder="<?=$this->lang->line("mode")?>" required = "required" value = "<?=set_value('mode')?>" style="resize:none;" id="mode" name="mode">
                     <option value="">Select</option>
                     <option value="2">Semester</option>
                    <option value="1">Yearly</option>
                    </select>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('mode'); ?>

                        </span>

                    </div>


<!-- end time mode -->



<!-- time fee -->

                    <?php 

                        if(form_error('fee')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-2 control-label">

                            <?=$this->lang->line("fee")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">


<div class="input-group">
  <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
  <input class="form-control" placeholder="<?=$this->lang->line("fee")?>" value="<?=set_value('fee')?>" required = "required" style="resize:none;" id="fee" name="fee">
  <span class="input-group-addon">.00</span>
</div>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('fee'); ?>

                        </span>

                    </div>


<!-- end time fee -->


                    <?php 

                        if(form_error('note')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-2 control-label">

                            <?=$this->lang->line("courses_note")?>

                        </label>

                        <div class="col-sm-6">

                            <textarea class="form-control" placeholder="<?=$this->lang->line("courses_note")?>" style="resize:none;" id="note" name="note"><?=set_value('note')?></textarea>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('note'); ?>

                        </span>

                    </div>

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>

                <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_course")?>" >

            </div>

        </div>

      </div>

    </div>

</form>
<script type="text/javascript">
    
function onclickEditCourses(){
    

}
function EditCoursesDatamanagement(){
    alert('bye');
}

function onclickEditSubCourses(){
    alert('hi');
}
function EditSubCoursesDatamanagement(){
    alert('bye');
}

</script>
    