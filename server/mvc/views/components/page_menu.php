        <div class="navbar-default sidebar hold-transition skin-blue sidebar-collapse sidebar-mini" id="dsds" role="navigation">

            <div class="sidebar-nav navbar-collapse">

                <?php $usertype = $this->session->userdata("usertype"); ?>

                <ul class="nav" id="side-menu">

                    <li class="">

                        <?php

                        echo anchor('dashboard?panel='.$this->input->get('panel') ,'<i class="fa fa-dashboard

                        " style="color: black;"></i><span>'.$this->lang->line('menu_dashboard').'</span>',array('class' => 'top_parent','data-toggle'=>'tooltip','title'=>'Dashboard'));

                        ?> 

                    </li>
 


                    <?php 
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Super_A_P" || $usertype == "Academic") { ?>

                    <li>

                        <a data-toggle='tooltip' title='LMS' href="javascript:void(0)"><i class="fa fa-rupee"></i><span>Billing</span><span class="fa arrow"></span></a>

                        <ul class="nav nav-second-level">

                            <li>
                                <a data-toggle='tooltip' title='<?php $this->lang->line('menu_teacher') ?>' href="<?php echo base_url() ?>invoice/clgPayinvoice?panel=<?php echo $this->input->get('panel') ?>" style="margin-left: 0px;"><i class="fa fa-rupee"></i>Pay Now</a>
                            </li>
                            <li>
                                <a data-toggle='tooltip' title='Account Statement' href="<?php echo base_url() ?>invoice/clgBilling?panel=<?php echo $this->input->get('panel') ?>" style="margin-left: 0px;"><i class="fa fa-table"></i>Account Statement</a>
                            </li>
                            <li>
                                <a data-toggle='tooltip' title='Payment Reciept' href="<?php echo base_url() ?>invoice/payment_reciept?panel=<?php echo $this->input->get('panel') ?>" style="margin-left: 0px;"><i class="fa fa-money"></i> Payment History</a>
                            </li>
                            <li>
                                <a data-toggle='tooltip' title='Billing Profile' href="<?php echo base_url() ?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>&type=pds" style="margin-left: 0px;"><i class="fa fa-address-card-o"></i>Billing Profile</a>
                            </li>

                        </ul>

                    </li>
                    <?php  } ?>

                    <li>

                        <a href="<?php echo  base_url() ?>ClgHeader?panel=<?php echo $this->input->get('panel') ?>" class ="top_parent" ><i class="fa fa-user"></i><span>Header Setting</span></a>

                    </li>

                    <li>

                        <a href="<?php echo  base_url() ?>Adminstatus?panel=<?php echo $this->input->get('panel') ?>" class ="top_parent" ><i class="fa fa-user"></i><span>Status Setting</span></a>

                    </li>                            

                </ul>

            </div>

        </div>

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>



       