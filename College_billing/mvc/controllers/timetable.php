<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timetable extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_info_m");

		$this->load->model("parentes_m");

		$this->load->model("classes_m");

		$this->load->model("sub_courses");

		$this->load->model("teacher_m");

		$this->load->model("department_m");

		$this->load->model("student_m");

		$language = $this->session->userdata('lang');

		$this->lang->load('subject', $language);

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");

		$adminID = $this->session->userdata("adminID");

		if($this->input->get('id')){
			$dep_id = $_GET['id'];
		}else{
			$dep_id = 'no';
		}
	

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Academic") {

	     	$this->data['all_count']  =  $this->subject_m->all_count();
	     	$this->data['department'] = $this->department_m->fetch_departments('department');

	     	$this->data['mainCourse'] = $this->classes_m->get_classes_byAdmin($adminID);

	     	$this->db->where('departmentID',$dep_id);
	     	$getDepartmentdata = $this->db->get('classes')->result();

	     $timeTableCourse = array();
	     	foreach ($getDepartmentdata as $key => $value) {
	     		
	     			if ($value->mode==2) {
	     			$loop = $value->duration*2;
	     			}else{
	     			$loop = $value->duration;	
	     			}

	     		for ($i=1; $i <= $loop; $i++) { 
	     		$timeTableCourse[$key][$i]['classes'] = $value->classes;
	     		$timeTableCourse[$key][$i]['classesID'] = $value->classesID;

	     		if($value->mode==2){
	     		$yearsOrSemester =  CallSemester($i);	
	     		} else{
	     		$yearsOrSemester =  CallYears($i);
	     		} 
	     		 // $timeTableCourse[$key][$i]['subjects']  = $this->subject_m->get_subject(array("classesID"=>$value->classesID,"sub_coursesID"=>0,"yearsOrSemester"=>$yearsOrSemester));
	     		 $this->db->where('classesID',$value->classesID);
	     		 $this->db->where('sub_coursesID',0);
	     		 $this->db->where('yearsOrSemester',$yearsOrSemester);
	     		$timeTableCourse[$key][$i]['subjects']  = $this->db->get('subject')->result();

	     		$timeTableCourse[$key][$i]['yearsOrSemester'] = $yearsOrSemester;	


	     		}
	     	}

	   $this->data['timetable'] = 	$timeTableCourse;


            $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	

			$this->data["subview"] = "timetable/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$student = $this->student_info_m->get_student_info();

			$this->data['subjects'] = $this->subject_m->get_join_where_subject($student->classesID,$student->sub_coursesID);

			$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);

		if ($this->input->post('yearSemesterID')) {

			if ($this->input->post('yearSemesterID')=='Select') {

			$this->session->unset_userdata('FilterSubjectyearSemesterID');

			}else{

			$this->session->set_userdata('FilterSubjectyearSemesterID', $this->input->post('yearSemesterID'));

		  }

			}

			$this->data["subview"] = "timetable/index";

			$this->load->view('_layout_main', $this->data);

		} elseif($usertype == "Parent") {

			$username = $this->session->userdata("username");

			$parent = $this->parentes_m->get_single_parentes(array('username' => $username));

			$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));

				if(count($checkstudent)) {

					$classesID = $checkstudent->classesID;

					$this->data['set'] = $id;

					$this->data['subjects'] = $this->subject_m->get_join_subject($classesID);

					$this->data["subview"] = "subject/index_parent";

					$this->load->view('_layout_main', $this->data);

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "subject/search_parent";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


function timeTableChange(){
$subjectID  = $this->input->post('subjectID');
$startTime  = $this->input->post('startTime');
$endTime  = $this->input->post('endTime');
$classesID  = $this->input->post('classesID');
$yearsOrSemester  = $this->input->post('yearsOrSemester');

if ($subjectID==0) {

$data = array(
'startTime'=>0,
'endTime'=>0
);
	  		     $this->db->where('classesID',$classesID);
	     		 $this->db->where('sub_coursesID',0);
	     		 $this->db->where('yearsOrSemester',$yearsOrSemester);
	     		 $this->db->where('startTime',$startTime);
	             $this->db->update('subject',$data);

	             $arrayMsg['professor_name'] = 'Break';
	             $arrayMsg['error'] = 'change';
}else{

$subjectRow   = $this->db->where('subjectID',$subjectID)->get('subject')->row();

$data = array(
'startTime'=>$startTime,
'endTime'=>$endTime,
);
$this->db->where('subjectID',$subjectID);
$this->db->update('subject',$data);
$this->db->select('name');

$arrayMsg['professor_name']   = $this->db->where('professorID',$subjectRow->professorID)->get('professor')->row()->name;

if($subjectRow->startTime){
 $arrayMsg['error'] = "error";
}else{
  $arrayMsg['error'] = "change";
}

}
echo json_encode($arrayMsg);





}



}







/* End of file leave.php */

/* Location: .//D/xampp/htdocs/school/mvc/controllers/leave.php */

