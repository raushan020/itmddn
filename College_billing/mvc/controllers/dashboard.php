<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model('systemadmin_m');

		$this->load->model('admin_m');

		$this->load->model("dashboard_m");

		$this->load->model("automation_shudulu_m");

		$this->load->model("automation_rec_m");

		$this->load->model("setting_m");

		$this->load->model("notice_m");

		$this->load->model("etsexam_m");

		$this->load->model("user_m");

		$this->load->model("student_m");

		$this->load->model("classes_m");

		$this->load->model("teacher_m");

		$this->load->model("invoice_gen_m");

		$this->load->model("parentes_m");

		$this->load->model("sattendance_m");

		$this->load->model("subject_m");

		$this->load->model("feetype_m");

		$this->load->model("invoice_m");

		$this->load->model("exam_m");

		$this->load->model("expense_m");

		$this->load->model("payment_m");

		$this->load->model("lmember_m");

		$this->load->model("book_m");

		$this->load->model("issue_m");

		$this->load->model("student_info_m");

		$this->load->model("parentes_m");

		$this->load->model('hmember_m');

		$this->load->model('tmember_m');

		$this->load->model('professor_m');

		$this->load->model('event_m');

		$this->load->model('holiday_m');

		$this->load->model('visitorinfo_m');

		$language = $this->session->userdata('lang');

		$this->lang->load('dashboard', $language);

		/* Automation Start */

		$cnt = 0;

		$date = date('Y-m-d');

		$day = date('d');

		$month = date('m');

		$year = date('Y');	

		$setting = $this->setting_m->get_setting(array('adminID'=>$this->session->userdata("adminID")));

		if($day >= $setting->automation) {

			$automation_shudulus = $this->automation_shudulu_m->get_automation_shudulu();

			if(count($automation_shudulus)) {

				foreach ($automation_shudulus as $automation_shudulu) {

					if($automation_shudulu->month == $month && $automation_shudulu->year == $year) {

						$cnt = 1;

					}

				}

				if($cnt === 0) {

					$alltotalamount = 0;

					$alltotalamounttransport = 0;

					$alltotalamounthostel = 0;

					/* Library Start */

					$student_librarys = $this->student_m->get_order_by_student(array('library' => 1));

					foreach ($student_librarys as $student_library) {

						$db_lmember = $this->lmember_m->get_single_lmember(array('studentID' => $student_library->studentID));

						if(count($db_lmember)) {

							if($db_lmember->lbalance > 0) {

								$automation_rec_library = $this->automation_rec_m->get_order_by_automation_rec(array(

									'studentID' => $student_library->studentID,

									'month' => $month,

									'year' => $year,

									'nofmodule' => 5427279

								));

								if(!count($automation_rec_library)) {

									$alltotalamount = ($student_library->totalamount)+($db_lmember->lbalance);

									$array_library = array(

										'totalamount' => $alltotalamount

									);

									$dbclasses = $this->classes_m->get_classes($student_library->classesID);

									$array_library_invoice = array(

										'classesID' => $student_library->classesID,

										'classes' => $dbclasses->classes,

										'studentID' => $student_library->studentID,

										'student' => $student_library->name,

										'roll' => $student_library->roll,

										'feetype' => $this->lang->line('dashboard_libraryfee'),

										'amount' => $db_lmember->lbalance,

										'status' => 0,

										'date' => $date,

										'year' => $year

									);

									$this->invoice_m->insert_invoice($array_library_invoice);

									$this->student_m->update_student($array_library, $student_library->studentID);

									$this->automation_rec_m->insert_automation_rec(array(

										'studentID' => $student_library->studentID,

										'date' => $date,

										'day' => $day,

										'month' => $month,

										'year' => $year,

										'nofmodule' => 5427279

									));

									$alltotalamount = 0;

								}

							}

						}

					}

					/* Library Close */

					/* Transport Start */

					$student_transports = $this->student_m->get_order_by_student(array('transport' => 1));

					foreach ($student_transports as $student_transport) {

						$db_tmember = $this->tmember_m->get_single_tmember(array('studentID' => $student_transport->studentID));

						if(count($db_tmember)) {

							if($db_tmember->tbalance > 0) {

								$automation_rec_transport = $this->automation_rec_m->get_order_by_automation_rec(array(

									'studentID' => $student_transport->studentID,

									'month' => $month,

									'year' => $year,

									'nofmodule' => 872677678

								));

								if(!count($automation_rec_transport)) {

									$alltotalamounttransport = ($student_transport->totalamount)+($db_tmember->tbalance);

									$array_transport = array(

										'totalamount' => $alltotalamounttransport

									);

									$dbclasses = $this->classes_m->get_classes($student_transport->classesID);

									$array_transport_invoice = array(

										'classesID' => $student_transport->classesID,

										'classes' => $dbclasses->classes,

										'studentID' => $student_transport->studentID,

										'student' => $student_transport->name,

										'roll' => $student_transport->roll,

										'feetype' => $this->lang->line('dashboard_transportfee'),

										'amount' => $db_tmember->tbalance,

										'status' => 0,

										'date' => $date,

										'year' => $year

									);

									$this->invoice_m->insert_invoice($array_transport_invoice);

									$this->student_m->update_student($array_transport, $student_transport->studentID);

									$this->automation_rec_m->insert_automation_rec(array(

										'studentID' => $student_transport->studentID,

										'date' => $date,

										'day' => $day,

										'month' => $month,

										'year' => $year,

										'nofmodule' => 872677678

									));

									$alltotalamounttransport = 0;

								}

							}

						}

					}


					/* Transport Close */

					/* Hostel Start */

					$student_hostels = $this->student_m->get_order_by_student(array('hostel' => 1));

					foreach ($student_hostels as $student_hostel) {

						$db_hmember = $this->hmember_m->get_single_hmember(array('studentID' => $student_hostel->studentID));

						if(count($db_hmember)) {

							if($db_hmember->hbalance > 0) {

								$automation_rec_hostel = $this->automation_rec_m->get_order_by_automation_rec(array(

									'studentID' => $student_hostel->studentID,

									'month' => $month,

									'year' => $year,

									'nofmodule' => 467835

								));


								if(!count($automation_rec_hostel)) {

									$alltotalamounthostel = ($student_hostel->totalamount)+($db_hmember->hbalance);

									$array_hostel = array(

										'totalamount' => $alltotalamounthostel

									);

									$dbclasses = $this->classes_m->get_classes($student_hostel->classesID);

									$array_hostel_invoice = array(

										'classesID' => $student_hostel->classesID,

										'classes' => $dbclasses->classes,

										'studentID' => $student_hostel->studentID,

										'student' => $student_hostel->name,

										'roll' => $student_hostel->roll,

										'feetype' => $this->lang->line('dashboard_hostelfee'),

										'amount' => $db_hmember->hbalance,

										'status' => 0,

										'date' => $date,

										'year' => $year

									);

									$this->invoice_m->insert_invoice($array_hostel_invoice);

									$this->student_m->update_student($array_hostel, $student_hostel->studentID);

									$this->automation_rec_m->insert_automation_rec(array(

										'studentID' => $student_hostel->studentID,

										'date' => $date,

										'day' => $day,

										'month' => $month,

										'year' => $year,

										'nofmodule' => 467835

									));

									$alltotalamounthostel = 0;

								}

							}

						}

					}

					/* Hostel Close */

					$this->automation_shudulu_m->insert_automation_shudulu(array(

						'date' => $date,

						'day' => $day,

						'month' => $month,

						'year' => $year

					));

				}

			} else {

				$this->automation_shudulu_m->insert_automation_shudulu(array(

					'date' => $date,

					'day' => $day,

					'month' => $month,

					'year' => $year

				));

			}

		}

		/* Automation Close */

	}

	public function index() {

		$usertype = $this->session->userdata('usertype');

		$day = abs(date('d'));

		$monthyear = date('m-Y');

		$this->data['event'] = $this->event_m->get_event();

		$this->data['holiday'] = $this->holiday_m->get_holiday();

		// dump($this->data['event']);

		// dump($this->data['holiday']);

		// die;

		if($usertype == "superadmin") {

			$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');



			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

	     	$this->data['notices'] = $this->notice_m->get_notice();

			$this->data['student'] = $this->student_m->get_student_by_superAdmin();

			$this->data['teacher'] = $this->teacher_m->get_teacher();

			$this->data['parents'] = $this->parentes_m->get_parentes();

			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));

			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));

			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		}

		if($usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin_btech") {

			$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');
			$loginuserID = $this->session->userdata('loginuserID');

			$this->data['ap'] = $this->teacher_m->get_teacher_by_rm($loginuserID);

			$this->data['student_rm'] = $this->student_m->get_student_by_rm_with_teacher($loginuserID);
		


			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

			$this->data['all_count']  =  $this->student_m->all_count();

			$this->data['ActiveSubjects_count']  =  $this->subject_m->ActiveSubjects_count();
			
			$this->data['ActiveStudent_count']  =  $this->student_m->ActiveStudent_count();
			

			$this->data['DraftStudent_count_first']  =  $this->student_m->DraftStudent_count();
			$this->data['recentStudent_count']  =  $this->student_m->recentStudent_count();


			$this->data['TrashStudent_count']  =  $this->student_m->TrashStudent_count();

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$this->data['payment_amount'] = $this->invoice_m->totalPayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

	     	$this->data['notices'] = $this->notice_m->notice_list_last5($loginuserID);

	     	$this->data['recent_notice'] = $this->notice_m->notice_recent_ap($loginuserID);

			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);

			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);

			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);

			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);



			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));



			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";



} 
/*  Academic--Admin Academic */

		elseif($usertype == "Admin" || $usertype == "Academic") {



			$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');
			$loginuserID = $this->session->userdata('loginuserID');

			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

			$this->data['all_count']  =  $this->student_m->all_count();

			$this->data['ActiveSubjects_count']  =  $this->subject_m->ActiveSubjects_count();
			
			$this->data['ActiveStudent_count']  =  $this->student_m->ActiveStudent_count();
			

			$this->data['DraftStudent_count_first']  =  $this->student_m->DraftStudent_count();
			$this->data['recentStudent_count']  =  $this->student_m->recentStudent_count();


			$this->data['TrashStudent_count']  =  $this->student_m->TrashStudent_count();

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$this->data['payment_amount'] = $this->invoice_m->totalPayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

	     	$this->data['notices'] = $this->notice_m->notice_list_last5($loginuserID);

	     	$this->data['recent_notice'] = $this->notice_m->notice_recent_ap($loginuserID);

			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);

			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);

			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);

			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);



			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));



			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";




		}elseif($usertype == "Professor") {



					$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');

			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

			// print_r($this->data['Totaluser']);

			// exit();

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$this->data['totalPayment'] = $this->invoice_m->totalPayment();

           	$this->data['duePayment'] = $this->invoice_m->duePayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

	     	$this->data['notices'] = $this->notice_m->get_notice();



			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);



			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);





			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);



			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));



			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


			$this->data['subject'] = $this->subject_m->get_subject();



		} elseif($usertype == "Teacher"  ) {

			$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');

			$loginuserID = $this->session->userdata('loginuserID');

			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

		

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$totalPayment = $this->invoice_m->totalPayment();
          	$totalAmount = 0;
         	$this->data['all_count']  =  $this->student_m->all_count();
          	$this->data['totalPayment'] = $totalAmount;

           	$this->data['duePayment'] = $this->invoice_m->duePayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

          	$this->data['payment_amount'] = $this->invoice_m->totalPayment();

	     	// $this->data['notices'] = $this->notice_m->get_notice();

	     	$this->data['notices'] = $this->notice_m->notice_list_last5($loginuserID);

	     	$this->data['recent_notice'] = $this->notice_m->notice_recent_ap($loginuserID);


			$this->data['student'] = $this->student_m->get_student_by_admin_with_teacher($adminID,$loginuserID);



			$this->data['Super'] = $this->teacher_m->get_teacher_single($loginuserID);

			$this->data['teacher'] = $this->teacher_m->get_teacher($loginuserID);
       
      
 

			$this->data['courses'] = $this->classes_m->inner_join_with_fee_module($loginuserID);



			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));


			$this->data['rm']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->rm,"userType"=>"Accountant"));	
		 
			$this->data['cpa']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->cpa,"userType"=>"Academic"));	
			$this->data['pl']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->pl,"userType"=>"Project_lead"));		

			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


			$this->data['subject'] = $this->subject_m->get_subject();



		} elseif( $usertype == "Super_A_P" ) {

			$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');

			$loginuserID = $this->session->userdata('loginuserID');

			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

		

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$totalPayment = $this->invoice_m->totalPayment();
          	$totalAmount = 0;
         	$this->data['all_count']  =  $this->student_m->new_SuperAP_Count_for_dashboard($loginuserID);
  /*echo $this->db->last_query();
exit();*/ 
          	$this->data['totalPayment'] = $totalAmount;

           	$this->data['duePayment'] = $this->invoice_m->duePayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

	     	// $this->data['notices'] = $this->notice_m->get_notice();

	     	$this->data['notices'] = $this->notice_m->notice_list_last5($loginuserID);
 
	     	$this->data['recent_notice'] = $this->notice_m->notice_recent_ap($loginuserID);


			$this->data['student'] = $this->student_m->get_student_by_admin_with_teacher($adminID,$loginuserID);

           $this->data['Profile'] = $this->user_m->get_single_user_Profile($loginuserID);
        
			$this->data['teacher'] = $this->teacher_m->get_teacher_no($adminID);
 /*print_r($this->data['teacher']);
 exit();*/
  
			$this->data['courses'] = $this->classes_m->inner_join_with_fee_module_for_dashboard($loginuserID);
			   


			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));

           $this->data['rm'] = $this->student_m->get_counsellor_by_spa($loginuserID);
        
			//$this->data['rm']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->rm,"userType"=>"Accountant"));	
			// $this->data['cpa']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->cpa,"userType"=>"Academic"));	
			//$this->data['pl']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->pl,"userType"=>"Project_lead"));		

			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


			$this->data['subject'] = $this->subject_m->get_subject();



		} 

		elseif( $usertype == "Admin_btech" ) {

			$adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');

			$loginuserID = $this->session->userdata('loginuserID');

			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

		

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$totalPayment = $this->invoice_m->totalPayment();
          	$totalAmount = 0;
         	$this->data['all_count']  =  $this->student_m->new_SuperAP_Count_for_dashboard($loginuserID);
  /*echo $this->db->last_query();
exit();*/ 
          	$this->data['totalPayment'] = $totalAmount;

           	$this->data['duePayment'] = $this->invoice_m->duePayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->totalPayment_full();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

	     	// $this->data['notices'] = $this->notice_m->get_notice();

	     	$this->data['notices'] = $this->notice_m->notice_list_last5($loginuserID);
 
	     	$this->data['recent_notice'] = $this->notice_m->notice_recent_ap($loginuserID);


			$this->data['student'] = $this->student_m->get_student_by_admin_with_teacher($adminID,$loginuserID);

           $this->data['Profile'] = $this->user_m->get_single_user_Profile($loginuserID);
        
			$this->data['teacher'] = $this->teacher_m->get_teacher_no($adminID);
 /*print_r($this->data['teacher']);
 exit();*/
  
			$this->data['courses'] = $this->classes_m->inner_join_with_fee_module_for_dashboard($loginuserID);
			   


			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));

           $this->data['rm'] = $this->student_m->get_counsellor_by_spa($loginuserID);
        
			//$this->data['rm']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->rm,"userType"=>"Accountant"));	
			// $this->data['cpa']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->cpa,"userType"=>"Academic"));	
			//$this->data['pl']	=  $this->user_m->get_user_by_username(array("userID"=>$this->data['teacher']->pl,"userType"=>"Project_lead"));		

			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


			$this->data['subject'] = $this->subject_m->get_subject();



		} 



		elseif($usertype == "Accountant") {



          $adminID = $this->session->userdata("adminID");

			$username = $this->session->userdata('username');
			$loginuserID = $this->session->userdata('loginuserID');

			$this->data['ap'] = $this->teacher_m->get_teacher_by_rm($loginuserID);

			$this->data['student_rm'] = $this->student_m->get_student_by_rm_with_teacher($loginuserID);
		


			$this->data['user'] = $this->admin_m->get_single_systemadmin(array('username'  => $username));

			$this->data['all_count']  =  $this->student_m->all_count();

			$this->data['ActiveSubjects_count']  =  $this->subject_m->ActiveSubjects_count();
			
			$this->data['ActiveStudent_count']  =  $this->student_m->ActiveStudent_count();
			

			$this->data['DraftStudent_count_first']  =  $this->student_m->DraftStudent_count();
			$this->data['recentStudent_count']  =  $this->student_m->recentStudent_count();
	        

			$this->data['TrashStudent_count']  =  $this->student_m->TrashStudent_count();

			$this->data['Totaluser'] = $this->admin_m->nub_ofuser();

          	$this->data['payment_amount'] = $this->invoice_m->totalPayment();

          	$this->data['totalPayment_full'] = $this->invoice_m->total_paid_dashboard();

          	$this->data['totalPayment_part'] = $this->invoice_m->totalPayment_part();

          	$this->data['totalPayment_not'] = $this->invoice_m->totalPayment_not();

	     	$this->data['notices'] = $this->notice_m->notice_list_last5($loginuserID);
	     	
	     	$this->data['recent_notice'] = $this->notice_m->notice_recent_ap($loginuserID);

			$this->data['student'] = $this->student_m->get_student_by_admin($adminID);

			$this->data['teacher'] = $this->teacher_m->get_teacher_ClgAdminID($adminID);

			$this->data['professors_count'] = $this->professor_m->get_professor_all($adminID);

			$this->data['courses'] = $this->classes_m->get_classes_active($adminID);



			$this->data['attendance'] = $this->sattendance_m->get_order_by_attendance(array('monthyear' => $monthyear, 'a'.$day => 'P'));



			$this->data['setting'] = $this->setting_m->get_setting(array('adminID'=>$adminID));



			$this->data['update_site_url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";




		} elseif($usertype == "Librarian") {
 

			$username = $this->session->userdata('username');



			$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));



			$this->data['notices'] = $this->notice_m->get_notice();



			$this->data['teacher'] = $this->teacher_m->get_teacher();



			$this->data['lmember'] = $this->lmember_m->get_lmember();



			$this->data['book'] = $this->book_m->get_book();



			$this->data['issue'] = $this->issue_m->get_order_by_issue(array('return_date' => NULL));



		} elseif($usertype == "Student") {

			$loginuserID = $this->session->userdata("loginuserID");

			$username = $this->session->userdata('username');

			$adminID = $this->session->userdata('adminID');

			if ($this->input->post('weatherfunctionSession')) {

				$this->session->set_userdata('weatherfunctionSession');

			}


			$this->data['user'] = $this->student_m->get_single_student(array('username'  => $username));



		   $this->data['Examtime'] = $this->etsexam_m->exam_dateStart($this->data['user']->sessionType,$this->data['user']->session,$this->data['user']->examType,$this->data['user']->yearsOrSemester,$this->data['user']->education_mode,$this->data['user']->yosPosition);





		   $this->data['totalamountSend'] = $this->invoice_gen_m->TotalFee($this->data['user']->studentID,$adminID);

		

			//$this->data['notices'] = $this->notice_m->get_notice();


			$student = $this->student_info_m->get_student_info();
			$this->data['notices'] = $this->notice_m->notice_list_all($student->studentID);

			$this->data['recent_notice'] = $this->notice_m->notice_recent($student->studentID);
	
			$this->data['exams'] = $this->exam_m->get_exam_count($this->data['user']->studentID);								

			$this->data['teacher'] = $this->teacher_m->get_teacher();



			$this->data['subject'] = $this->subject_m->get_join_where_subject_count($this->data['user']->classesID,$this->data['user']->sub_coursesID);

			

		  $this->data["student"] = $this->student_m->get_student(array('studentID'=>$this->data['user']->studentID,'adminID'=>$this->data['user']->adminID));



				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);



			    // $this->data["education"] = $this->student_m->get_educations($this->data['user']->studentID);

			   

				if($this->data["student"] && $this->data["class"]) {



				    $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);

				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);



		}

			$lmember = $this->lmember_m->get_single_lmember(array('studentID' => $this->data['user']->studentID));



			if($lmember) {



				$this->data['issue'] = $this->issue_m->get_order_by_issue(array("lID" => $lmember->lID, 'return_date' => NULL));



			} else {



				$this->data['issue'] = NULL;



			}



			$this->data['invoice'] = $this->invoice_m->get_order_by_invoice(array("studentID" => $this->data['user']->studentID));



		} elseif($usertype == "Parent") {


			$username = $this->session->userdata('username');



			$this->data['user'] = $this->parentes_m->get_single_parentes(array('username'  => $username));



			$this->data['notices'] = $this->notice_m->get_notice();



			$this->data['teacher'] = $this->teacher_m->get_teacher();



			$students = $this->student_m->get_order_by_student(array('parentID'  => $this->data['user']->parentID));



			$issue = 0;



			$invoice = 0;



			foreach ($students as $student) {



				$lmember = $this->lmember_m->get_single_lmember(array('studentID' => $student->studentID));



				if($lmember) {



					$getissue = $this->issue_m->get_order_by_issue(array("lID" => $lmember->lID, 'return_date' => NULL));



					$issue+=count($getissue);



				}







				$getinvoice = $this->invoice_m->get_order_by_invoice(array('studentID' => $student->studentID));



				if($getinvoice) {



					$invoice+=count($getinvoice);



				}



			}



			$this->data['issue'] = $issue;



			$this->data['invoice'] = $invoice;



			$this->data['books'] = $this->book_m->get_book();



		} elseif($usertype == "Receptionist") {



			$username = $this->session->userdata('username');



			$this->data['user'] = $this->user_m->get_single_user(array('username'  => $username));



			$this->data['teacher'] = $this->teacher_m->get_teacher();



			$this->data['visitorinfo'] = $this->visitorinfo_m->get_visitorinfo();



			$this->data['notices'] = $this->notice_m->get_notice();



		}



		$this->data["subview"] = "dashboard/index";

	

		$this->load->view('_layout_main', $this->data);



	}
	
	function feedbackMessage(){

    $loginuserID = $this->session->userdata("loginuserID");

    $this->db->where('studentID',$loginuserID);

    $students = $this->db->get('student')->row();
$category = $this->input->post('category');
		$data = array(

		'studentID'=>$loginuserID,

		'name'=>$students->name,

		'message'=>$this->input->post('val'),

		'email'=>$this->input->post('email'),
		'category'=>$category

		);


		$this->db->insert('feedback',$data);

$to = "anjum.badar@jobsterz.com,navi.jobsterz@gmail.com";
$mail = "rahul.edge1@gmail.com";

// $to = "rahul.edge1@gmail.com";

$subject = "Feedback From Student";

$message = '<html><body>';

$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

$message .= "<tr><td><strong>Category:</strong> </td><td>" . $category . "</td></tr>";

$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";

$message .= "<tr><td><strong>Message:</strong> </td><td>" . strip_tags($_POST['val']) . "</td></tr>";

$message .= "</table>";

$message .= "</body></html>";


$header = "From:".$mail."\r\n"; 

$header.= "MIME-Version: 1.0\r\n"; 

$header.= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 

$header.= "X-Priority: 1\r\n"; 



mail($to, $subject, $message, $header);



// $to = "anjum.badar@jobsterz.com";

// $to = "rahul.edge1@gmail.com";

// $subject = "Feedback From Student";

// $message = '<html><body>';

// $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

// $message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>".$students->name . "</td></tr>";
// $message .= "<tr><td><strong>Category:</strong> </td><td>" . $category . "</td></tr>";

// $message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";

// $message .= "<tr><td><strong>Message:</strong> </td><td>" . strip_tags($_POST['val']) . "</td></tr>";

// $message .= "</table>";

// $message .= "</body></html>";

// $header = "From:".$students->email."\r\n"; 

// $header.= "MIME-Version: 1.0\r\n"; 

// $header.= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 

// $header.= "X-Priority: 1\r\n"; 



// $status = mail($to, $subject, $message, $header);

	}





	function paymentscall() {

		

		$usertype = $this->session->userdata('usertype');



		$adminID = $this->session->userdata('adminID');



		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" ||  $usertype == "Teacher" || $usertype == "Accountant" || $usertype == "Academic" || $usertype == "Super_A_P" || $usertype == "Admin_btech") {



if ($usertype == "superadmin") {

			$payments = $this->payment_m->get_payment();

			$invoices = $this->invoice_m->get_invoice();

}else{

			$payments = $this->payment_m->get_payment_byAdminID($adminID);

			$invoices = $this->invoice_m->get_invoice_byAdminID($adminID);

}

			



			$npaid = 0;



			$ppaid = 0;



			$fpaid = 0;



			$cash = 0;



			$cheque = 0;



			$paypal = 0;



			$PayuMoney = 0;



			$OnlinePayment = 0;



			$Bank = 0;



			if(count($invoices)) {



				foreach ($invoices as $invoice) {



					if($invoice->status == 0) {

						$npaid++;

					} elseif($invoice->status == 1) {

						$ppaid++;

					} elseif($invoice->status == 2) {

						$fpaid++;

					}



				}



			}







			if(count($payments)) {



				foreach ($payments as $payment) {



					if('Cash' == $payment->paymenttype) {



						$cash++;



					} elseif('Cheque' == $payment->paymenttype) {



						$cheque++;



					} elseif('Paypal' == $payment->paymenttype) {



						$paypal++;



					}

					elseif('PayuMoney' == $payment->paymenttype) {



						$PayuMoney++;



					}elseif('onlinePayment' == $payment->paymenttype) {



						$OnlinePayment++;



					}elseif('Bank' == $payment->paymenttype) {



						$Bank++;



					}



				}



			}







			if(count($invoices)) {



				$json = array("npaid" => $npaid, "ppaid" => $ppaid, "fpaid" => $fpaid, "cash" => $cash, "cheque" => $cheque, "paypal" => $paypal,"PayuMoney" => $PayuMoney,'OnlinePayment'=>$OnlinePayment,'Bank'=>$Bank, "st" => 1);





				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			} else {



				$json = array("npaid" => $npaid, "ppaid" => $ppaid, "fpaid" => $fpaid, "cash" => $cash, "cheque" => $cheque, "paypal" => $paypal, "PayuMoney" => $PayuMoney,'OnlinePayment'=>$OnlinePayment,'Bank'=>$Bank, "st" => 0);



				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			}



		}



	}







	function graphcall() {



		$usertype = $this->session->userdata('usertype');



		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Teacher" || $usertype == "Accountant" || $usertype == "Academic"  || $usertype == "Super_A_P"  || $usertype == "Admin_btech") {



			$payments = $this->payment_m->get_order_by_payment();



	      	$lastEarn = 0;



	      	$percent = 0;



	      	$monthBalances = array();



	      	$hightEarn['hight'] = 0;



	      	$dataarr = array();



	      	$allMonths = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");







			if(count($payments)) {



				foreach ($allMonths as $key => $allMonth) {



					foreach ($payments as $key => $payment) {



					    $paymentMonth = date("M", strtotime($payment->paymentdate));



					    if($allMonth == $paymentMonth) {



					      	$lastEarn+=$payment->paymentamount;



					      	$monthBalances[$allMonth] = $lastEarn;



					    } else {



					      	if(!array_key_exists($allMonth, $monthBalances)) {



					        	$monthBalances[$allMonth] = 0;



					      	}



					    }



					 }







				  	if($lastEarn > $hightEarn['hight']) {



				    	$hightEarn['hight'] = $lastEarn;



				  	}



				  	$lastEarn = 0;



				}







				foreach ($monthBalances as $monthBalancekey => $monthBalance) {



					$dataarr[] = $monthBalance;



				}











				$json = array("balance" => $dataarr);



				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;



			} else {



				foreach ($allMonths as $allMonth) {



					$dataarr[] = 0;



				}



				$json = array("balance" => $dataarr);



				header("Content-Type: application/json", true);



				echo json_encode($json);



				exit;







			}



		}



	}







}







/* End of file dashboard.php */



/* Location: .//D/xampp/htdocs/school/mvc/controllers/dashboard.php */



