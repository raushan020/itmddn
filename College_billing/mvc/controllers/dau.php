<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Api2Pdf\Api2Pdf;

class Dau extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("semester_test_m");
		$this->load->model("invoice_gen_m");
		$this->load->model("sub_courses");
		$this->load->model("country_m");
		$this->load->model("year_m");
		$this->load->model("user_m");
		$this->load->model("teacher_m");
		$this->load->model("department_m");
		$this->load->model("classes_m");
		$this->load->model("subject_m");

		$language = $this->session->userdata('lang');
		$this->lang->load('student', $language);
	}


   function dauAddSession(){

   	$students = $this->db->get('student')->result();

   	foreach ($students as $key => $value) {
   		$arraySesion =  explode('-',$value->session);
   		$sessionFrom    = $arraySesion[0];
   		$sessionTo    = $arraySesion[1];
if($sessionFrom!=''  and  $sessionTo !=''){
   		$data = array(
   			'start_session' =>$sessionFrom,
   			'end_session' =>$sessionTo
   	);
   		

   		$this->db->where('studentID',$value->studentID);
   		$this->db->update('student',$data);

   	}
}



   }

   function dau_set_invoice(){
   		$students = $this->db->get('student')->result();
   		foreach ($students as $key => $value) {
      $data =array(
   		'yearsOrSemester'=>$value->yearsOrSemester
   		);
   		$this->db->where('studentID',$value->studentID);
   		$this->db->update('invoice',$data);
   		
   		}
   		
   }


}