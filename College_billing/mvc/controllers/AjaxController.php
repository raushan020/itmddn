<?php

class AjaxController extends MY_Controller {

	function __construct() {

parent::__construct();


$this->load->library("session");
$this->load->helper('language');
$this->load->helper('form');
$this->load->database();
$this->load->model('subject_m');
$this->load->model('setting_m');
$this->load->model('classes_m');
$this->load->model('teacher_m');
		$language = $this->session->userdata('lang');

		$this->lang->load('subject', $language);
}
function AddSubject(){
  $usertype = $this->session->userdata("usertype");
    $SpecialUsertype = $this->session->userdata('SpecialUsertype');
if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Academic" || $SpecialUsertype=='Btech') {

  $adminID = $this->session->userdata("adminID");
  $classesID  = (int) $this->input->post('classesID');
  $subCourseID  = (int) $this->input->post('subCourseID');
  $professorID = (int) $this->input->post('professorID');
  $startTime = $this->input->post('startTime');
  $endTime =  $this->input->post('endTime');
  // $startDate = $this->input->post('startDate');
  // $endDate = $this->input->post('endDate');
  $data = array(
  'adminID'=>$adminID,
  'classesID' =>$classesID, 
  'sub_coursesID' =>$subCourseID, 
  'yearsOrSemester' =>$this->input->post('yearSemesterID'), 
  'subject' =>$this->input->post('subjectName'), 
  'subject_code' =>$this->input->post('subjectCode'),
  'subject_Credit' =>$this->input->post('subjectCredit'),
  "create_date" => date("Y-m-d h:i:s"),
  "modify_date" => date("Y-m-d h:i:s"),
  "create_userID" => $this->session->userdata('loginuserID'),
  "create_username" => $this->session->userdata('username'),
  "create_usertype" => $this->session->userdata('usertype'),
  "professorID" => $professorID,
  "startTime" => $startTime,
  "endTime" => $endTime
  // "startDate" => $startDate,
  // "endDate" => $endDate
  );

  $this->db->insert('subject', $data);
  $insertID  = $this->db->insert_id();

  $this->CommonDataForSubject($adminID,$classesID,$subCourseID,$professorID);

  }
}

// function AddUnit(){
// $uri = $this->uri->segment(3);
 
//   $usertype = $this->session->userdata("usertype");
//   if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {    

//       if ($_POST) {
//         $unit_name = $this->input->post('unit_name');
//         $unit_code = $this->input->post('unit_number');
//         $subjectID = $this->input->post('subjectID');
//         $addmore = $this->input->post('addmore');


//     $data = array(
//     'unit_name'=>$unit_name,
//     'unit_number'=>$unit_code,
//     'subjectID'=>$subjectID,
//     'addmore'=>$addmore
//     );
//     $this->db->insert('units', $data);
//     $insertID  = $this->db->insert_id();
//     if ($insertID) {
//       for($i=0; $i < count($addmore); $i++) {       
          
//           $topicData = array(
//           'unitID'=>$insertID,
//           'topic_name'=>$addmore[$i]
//           );
//       if ($addmore[$i]!='') {
//       $this->db->insert('unit_topic', $topicData);
//       }
       
//    }
//     }
    
//      }
// $this->CommonDataForSyllabus($subjectID);
//   }

// }



// function CommonDataForSyllabus($subjectID){


//   $array = array(
//     'subjectID' =>$subjectID,
   
//   );

//    $subjects   = $this->subject_m->GetSubjectBySyllabus($array); 


// foreach ($subjects as $key => $value) {
  
//        echo "<div class='AddNewSubjetcsDetails AddNewSyllabus'>
//        <h3>Unit ".$value->unit_code."</h3>
//    <div class='col-sm-6'>
//       <div class=''>
//          <input type='text' class='form-control' id='subjectNameClear' onBlur='EditSubject(this, \"subject\", ".$value->unit_name.",$(this).val())'  placeholder='Unit Name' name='unit_name' value='".set_value('unit_name' , $value->unit_name)."' >
//       </div>
//    </div>
//    <div class='col-sm-6'>
//       <div class=''>
//          <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,\"subject_code\",".$value->unit_code.",$(this).val())' placeholder='Unit Code' name='unit_code' value='".set_value('unit_code',$value->unit_code)."' >
//       </div>
//    </div>";
//    $tid  = explode(',', $value->tid);
//    $tn  = explode(',', $value->tn);
// for ($i=0; $i <count($tid) ; $i++) { 
//    echo  "<div class='col-sm-3'>

//       <div class='control-group input-group' style='margin-top:10px'>
//          <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,\"subject_code\",".$value->unit_code.",$(this).val())' placeholder='Unit Code' name='unit_code' value='".set_value('unit_code',$tn[$i])."' >
//                      <div class='input-group-btn'> 
//               <button class='btn btn-danger' type='button'>
//                 <i class='glyphicon glyphicon-remove'></i></button>
//             </div>
//       </div>
      
//    </div>";
//  }
   
   
//    echo "<div class='clearfix'></div>
// </div>";
// }


// }
function AddUnit(){
$uri = $this->uri->segment(3);
 
  $usertype = $this->session->userdata("usertype");
  $SpecialUsertype = $this->session->userdata('SpecialUsertype');
  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Academic"  || $usertype == "superadmin" || $SpecialUsertype=='Btech') {    

      if ($_POST)
      {
        $unit_name = $this->input->post('unit_name');
        $unit_code = $this->input->post('unit_number');
        $subjectID = $this->input->post('subjectID');
        $checkunitcode=$this->subject_m->checkunitcode($unit_code,$subjectID);
        if($checkunitcode)
        {
           $this->session->set_flashdata('error',"This Unit Number is already Assign!!");
        }
        else
        {
           $data = array(
            'unit_name'=>$unit_name,
            'unit_code'=>$unit_code,
            'subjectID'=>$subjectID
            );
            $this->db->insert('units', $data);
        }
        /*$addmore = $this->input->post('addmore');*/


   
    /*$this->db->insert_id();
		if ($insertID) {
		for($i=0; $i < count($addmore); $i++) {       
          
          $topicData = array(
          'unitID'=>$insertID,
          'topic_name'=>$addmore[$i]
          );
      if ($addmore[$i]!='') {
      $this->db->insert('unit_topic', $topicData);
      }
       
   }
    }*/
    
     }
$this->CommonDataForSyllabus($subjectID);
  }

}


function addTopicsMore(){
$data  = array(
'unitID' =>$this->input->post('unitID'),
'topic_name' =>$this->input->post('val'),
 );
  $val  = $this->db->insert('unit_topic',$data);

$this->CommonDataForSyllabus($this->input->post('subjectID'));

}



function CommonDataForSyllabus($subjectID){


  $array = array(
    'subjectID' =>$subjectID,
   
  );

   $subjects   = $this->subject_m->GetSubjectBySyllabus($array); 


foreach ($subjects as $key => $value) {
  
       echo "<div class='AddNewSubjetcsDetails AddNewSyllabus' id ='unit_'".$key.">
          <div class='delete_unit'><button class='btn btn-danger btn-xs mrg for_margR' onclick='delete_unit(".$value['unitID'].", 'unit_'".$key.")'>Delete Unit</button></div>
   <div class='col-sm-6'>
      <div class=''>
         <input type='text' class='form-control' id='subjectNameClear' onBlur='unitEdit(this, \"unit_name\", ".$value['unitID'].",$(this).val())'  placeholder='Unit Name' name='unit_name' value='".set_value('unit_name' , $value['unit_name'])."' >
      </div>
   </div>";
   /*$tid  = explode(',', $value->tid);
   $tn  = explode(',', $value->tn);
for ($i=0; $i <count($tid) ; $i++) { 
   echo  "<div class='col-sm-3' id='topics_'". $key.$i ."'>

      <div class='control-group input-group' style='margin-top:10px'>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,\"subject_code\",".$value->unit_code.",$(this).val())' placeholder='Unit Code' name='unit_code' value='".set_value('unit_code',$tn[$i])."' >
                     <div class='input-group-btn'> 
              <button class='btn btn-danger' type='button' onclick=delete_topic('".$tid[$i]."', topics_'".$key."')".">
                <i class='glyphicon glyphicon-remove'></i></button> 
            </div>
      </div>
      
   </div>";
 }*/
   
   
   echo "<div class='clearfix'></div>
</div>";
}


}

function CallSubject(){
  
$adminID = $this->session->userdata("adminID");
$classesID  = (int) $this->input->post('classesID');
$subCourseID  = (int) $this->input->post('subCourseID');
$professorID  = (int) $this->input->post('professorID');

$this->CommonDataForSubject($adminID,$classesID,$subCourseID,$professorID);

}
function CallSubjectData(){

$adminID = $this->session->userdata("adminID");
$classesID  = (int) $this->input->post('classesID');
$subCourseID  = (int) $this->input->post('subCourseID');
$yearSemesterID  = (int) $this->input->post('yearSemesterID');
$this->CommonDataForSubjectdDate($adminID,$classesID,$subCourseID,$yearSemesterID);

}


function CommonDataForSubjectdDate($adminID,$classesID,$subCourseID,$yearSemesterID){
  $array = array(
  'classesID' =>$classesID,
  'yearsOrSemester' =>$this->input->post('yearSemesterID'), 
  'sub_coursesID' =>$subCourseID, 
);
  
$this->db->where('adminID',$adminID);
$this->db->where('classesID',$classesID);
$this->db->where('sub_coursesID',$subCourseID);
$this->db->where('yearsOrSemester',$yearSemesterID);

$this->db->limit(1);
$query  = $this->db->get('subject')->row();


 echo json_encode($query);

}
function DeleteSubject(){

$adminID = $this->session->userdata("adminID");
$classesID  = (int) $this->input->post('classesID');
$subjectID  = (int) $this->input->post('subjectID');
$subCourseID  = (int) $this->input->post('subCourseID');
$this->db->where('adminID',$adminID);
$this->db->where('subjectID',$subjectID);
$this->db->delete('subject');
$this->CommonDataForSubject($adminID,$classesID,$subCourseID);

}

function CommonDataForSubject($adminID,$classesID,$subCourseID){
	$array = array(
	'classesID' =>$classesID,
	'yearsOrSemester' =>$this->input->post('yearSemesterID'), 
	'sub_coursesID' =>$subCourseID, 
);

$subjects   = $this->subject_m->GetSubjectByParam($array);
                $this->db->where('classesID',$classesID);
$courseData = $this->db->get('classes')->row();
              $this->db->where('departmentID',$courseData->departmentID);    
               $this->db->select('professorID,name');
$professorData = $this->db->get('professor')->result();             
	
foreach ($subjects as $key => $value) {
  
       echo "<div class='AddNewSubjetcsDetails'>
   <div class='col-sm-3'>
      <div class=''>
         <input type='text' class='form-control' id='subjectNameClear' onBlur='EditSubject(this, \"subject\", ".$value->subjectID.",$(this).val())'  placeholder='Subject Name' name='subject' value='".set_value('subject' , $value->subject)."' >
      </div>
   </div>
   <div class='col-sm-2'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,\"subject_code\",".$value->subjectID.",$(this).val())' placeholder='Subject Code' name='subject' value='".set_value('subject',$value->subject_code)."' >
      </div>
   </div>

      <div class='col-sm-2'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCreditClear' onBlur='EditSubject(this,\"subject_credit\",".$value->subjectID.",$(this).val())' placeholder='Subject Credit' name='subject' value='".set_value('subject',$value->subject_credit)."' >
      </div>
   </div>

    <div class='col-sm-2'>
      
       <select class='form-control' required = 'required' onChange='CallProfessor(this, \"professorID\", ".$value->subjectID.",$(this).val())' value = '<?=set_value('professorID')?>' style='resize:none;' id='professorID' name='professor'>
       <option value=''>Select</option>";
      foreach ($professorData as $key => $rows) {
        if($value->professorID==$rows->professorID){
          $select = "Selected";
        }else{
          $select = "";
        }
        echo "<option value=".$rows->professorID." ".$select.">".$rows->name."</option>";
      }
          echo"</select>
      
   </div>
   
   <div class='col-sm-3'>
      <div class=''>
      <button class='btn btn-remove btn-danger' type='button'><span class='glyphicon glyphicon-minus' onclick='DeleteSubject(".$value->subjectID.")'></span></button>
      </div>
   </div>
   <div class='clearfix'></div>
</div>";
}


}

function EditSubject(){
	$column	= $this->input->post('column');
$subjectID	= (int) $this->input->post('subjectID');
$val =  $this->input->post('val');
	$data = array(
   "$column" =>$val, 
);

$this->db->where('subjectID',$subjectID);
$query = $this->db->update('subject',$data);
if ($query) {
	echo "string";
}else{
	echo "false";
}
}

function EditStarttime(){
  $column = $this->input->post('column');
$subjectID  = (int) $this->input->post('subjectID');
$val =  $this->input->post('val');
  $data = array(
   "$column" =>$val, 
);

$this->db->where('subjectID',$subjectID);
$query = $this->db->update('subject',$data);
if ($query) {
  echo "string";
}else{
  echo "false";
}
}

function Editendtime(){
  $column = $this->input->post('column');
$subjectID  = (int) $this->input->post('subjectID');
$val =  $this->input->post('val');
  $data = array(
   "$column" =>$val, 
);

$this->db->where('subjectID',$subjectID);
$query = $this->db->update('subject',$data);
if ($query) {
  echo "string";
}else{
  echo "false";
}
}

function editProfessor(){
$column = $this->input->post('column');
$subjectID  = (int) $this->input->post('subjectID');
$val =  $this->input->post('val');
  $data = array(
   "$column" =>$val, 
);

$this->db->where('subjectID',$subjectID);
$query = $this->db->update('subject',$data);
// print_r($query);
// exit;
if ($query) {
  echo "string";
}else{
  echo "false";
}
}


function fetch_course(){
  $this->load->model('classes_m');
  $id = $this->input->post('id');

  $classesRows = $this->classes_m->get_single_department($id);
  if ($classesRows) {
    echo "<div class='form-group'>
          <label for='classesID' class='col-sm-2 control-label'>Course Name<span class='red-color'>*</span></label>";
    echo "<div class='col-sm-6'>";

    $id = $this->input->post('id');
    $classesRows = $this->classes_m->get_single_department($id);
    
    if ($classesRows) {      

       $array = array();

        $array[0] = 'Select Course';
      foreach ($classesRows as $key => $classa) {
 
          $array[$classa->classesID] = $classa->classes;

        }

        echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'  onchange = 'CourseSDependent($(this).val())' ");
      
   }
    echo "</div></div><span class='col-sm-4 control-label'>
          </span><div class='clearfix'></div>";
  }  
}



function fetch_Semester(){
	$this->load->model('classes_m');
	$id = $this->input->post('id');
  $classesRow = $this->classes_m->get_single_classes($id);
  if ($classesRow) {
    echo "<div class='form-group'>
					<label for='' class='col-sm-2 control-label'>Year/Semester<span class='red-color'>*</span></label>";
    echo "<div class='col-sm-6' id='yearID2'>";

    $id = $this->input->post('id');
    $classesRow = $this->classes_m->get_single_classes($id);;
    
    if ($classesRow) {
      $looping    =  (int) $classesRow->duration;
      if ($classesRow->mode==1) {
 	      echo"<select class='form-control' name='yearID' onchange='CallSubject($(this).val())' id='yearSemesterID' value='set_value('yearId');'>";
        echo "<option value=''>Select Year/Semester</option>";
        for ($i=1; $i <=$looping; $i++) {
 
          echo "<option value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";	 
        }
        echo  "</select>";
      }else{

        echo"<select class='form-control' name='semesterId' onchange='CallSubject($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>";
        echo "<option value=''>Select Year/Semester</option>";
        for ($i=1; $i <=(2*$looping); $i++) {
          echo "<option value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
        }
        echo  "</select>";
      }
    }
    echo "</div></div><span class='col-sm-4 control-label'>
          </span><div class='clearfix'></div>";
  }  
}

function find_classes_using_teacher(){
  $val = $this->input->post('val');
   $counsellor = $this->input->post('counsellor');
  $result = $this->teacher_m->inner_join_with_fee_module($counsellor,$val);
if($result){
  echo "<option>Select Course</option>";
  foreach ($result as $key => $value) {
echo "<option value ='".$value->classesID."' >";
echo  $value->classes;
echo "</option>";
}
}

}

function find_classes_using_teacher_with_student(){
  $teacherID = $this->input->post('val');
  $counsellor = $this->input->post('counsellor');
  $result = $this->teacher_m->inner_join_with_fee_module_with_student($teacherID);
if($result){
  echo "<option>Select Course</option>";
  foreach ($result as $key => $value) {
echo "<option value ='".$value->classesID."' >";
echo  $value->classes;
echo "</option>";
}
}

}

function fetch_noticefilter(){
  $this->load->model('classes_m');
  $id = $this->input->post('id');
     $classesRow = $this->classes_m->get_single_classes($id);
     if ($classesRow) {
          echo "<div class='form-group'>
                <label for='' class='col-sm-2 control-label'>Year/Semester<span class='red-color'>*</span></label>";
echo  "<div class='col-sm-6' id='yearID2'>";

 $id = $this->input->post('id');
     $classesRow = $this->classes_m->get_single_classes($id);;
    
if ($classesRow) {
   $looping    =  (int) $classesRow->duration;
   if ($classesRow->mode==1) {
     echo"<select class='form-control' name='yearsOrSemester' id='yearSemesterID' value='set_value('yearId');'>";
     echo "<option value=''>Select Semester</option>";
    for ($i=1; $i <=$looping; $i++) {
   
   echo "<option value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
   
   }
   echo  "</select>";
}

else{
  
     echo"<select class='form-control' name='semesterId' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>";
    for ($i=1; $i <=(2*$looping); $i++) {
   echo "<option value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
   }
   echo  "</select>";
   }
}
echo "</div></div><span class='col-sm-4 control-label'>

                            
                        </span><div class='clearfix'></div>";

  }
  
}


function AddSubcourse(){
$adminID = $this->session->userdata("adminID");
$classID = $this->input->post('classID');

$sub_course = $_POST['sub_course'];

$digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int
$char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string
$setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));
$sitenameString   =  $setingsData->sname;
$firstCharacter = substr($sitenameString, 0, 2);
$subCourseCode      =   strtoupper($firstCharacter).'-'.$char.$digit;

$data = array(
  'classesID'=>$classID,
  'sub_course'=>$sub_course,
  'adminID'=>$adminID,
  'subCourseCode'=>$subCourseCode ,
  );
$this->db->insert('sub_courses',$data);
$insertID  = $this->db->insert_id();

$this->CommonDataForCourse($adminID,$classID);

}

function DeleteSubCourse(){

$adminID = $this->session->userdata("adminID");
$sub_coursesID = $this->input->post('sub_coursesID');
$classesID = $this->input->post('classesID');
$data =  array(
'status'=>2
);
$this->db->query("UPDATE sub_courses
  left JOIN student ON sub_courses.sub_coursesID = student.sub_coursesID
  left JOIN subject ON sub_courses.sub_coursesID = subject.sub_coursesID 
  left JOIN ets_quiz ON sub_courses.sub_coursesID = ets_quiz.sub_coursesID
   set student.status = 2 
 ,ets_quiz.status = 2, subject.status = 2 ,sub_courses.status = 2 where sub_courses.sub_coursesID = $sub_coursesID");

$this->db->where('adminID',$adminID);
$this->db->where('sub_coursesID',$sub_coursesID);
$this->db->update('sub_courses',$data);
$data =  array(
'current_login'=>0
);

$this->db->where('sub_coursesID',$sub_coursesID);
$this->db->update('school_sessions',$data);



$this->CommonDataForCourse($adminID,$classesID);
}

function CommonDataForCourse($adminID,$classesID){
  $array = array(
  'classesID' =>$classesID,
   'adminID' =>$adminID,
   'status'=>1 
);

$subjects   = $this->classes_m->GetCoursesByParam($array);
foreach ($subjects as $key => $value) {
  if($key==0){
    echo '<div class="col-md-12 margin20">
  <div class="col-sm-6">
    
      <div class="">
         <input type="text" class="form-control datepicker_quiz_data" placeholder="Start Date" name="startDate" value="'.$value->startDate.'" >
      </div>
   </div>
   <div class="col-sm-6">
      <div class="">
         <input type="text" class="form-control datepicker_quiz_data" placeholder="End Date" name="endDate" value="'.$value->endDate.'" >
      </div>
   </div>
   <div class="clearfix"></div>
   </div>';
  }
       echo "<div class='AddNewSubjetcsDetails'>
   <div class='col-sm-3'>
      <div class=''>
         <input type='text' disabled ='disabled' class='form-control' id=''  placeholder='Subject Name' name='subject' value='".set_value('subject' , $value->subCourseCode)."' >
      </div>
   </div>
   <div class='col-sm-3'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubCourse(this,\"subject_code\",".$value->sub_coursesID.",$(this).val())' placeholder='Subject Code' name='subject' value='".set_value('subject',$value->sub_course)."' >
      </div>
   </div>
   <div class='col-sm-3'>
      
       <select class='form-control' required = 'required' onChange='CallProfessor(this, \"professorID\", ".$value->subjectID.",$(this).val()))' value = '<?=set_value('professorID')?>' style='resize:none;' id='professorID' name='professor'>
       <option value=''>Select</option>";
      foreach ($professorData as $key => $rows) {
        if($value->professorID==$rows->professorID){
          $select = "Selected";
        }else{
          $select = "";
        }
        echo "<option value=".$rows->professorID." ".$select.">".$rows->name."</option>";
      }
          echo"</select>
      
   </div>
   <div class='col-sm-2'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditStarttime(this,\"startTime\",".$value->subjectID.",$(this).val())' placeholder='Start Time' name='startTime' value='".set_value('subject',$value->subject_code)."' >
      </div>
   </div>
   <div class='col-sm-2'>
      <div class=''>
         <input type='text' class='form-control' id='subjectCodeClear' onBlur='Editendtime(this,\"endTime\",".$value->subjectID.",$(this).val())' placeholder='End time' name='endTime' value='".set_value('subject',$value->subject_code)."' >
      </div>
   </div>
    <div class='col-sm-2'>
      <div class=''>
      <button class='btn btn-remove btn-danger' type='button'><span class='glyphicon glyphicon-minus' onclick='DeleteSubCourse(".$value->sub_coursesID.")'></span></button>
      </div>
   </div>
   <div class='clearfix'></div>
</div>";
}
}


function inline_edit_invoice_amount(){
  $column = $this->input->post('column');
  $editValue =  $this->input->post('editValue');
  $invoiceID =  $this->input->post('invoiceID');
  $query  = $this->db->query("SELECT SUM(paymentamount) total FROM payment where invoiceID = '$invoiceID' ");

  if ($query->row()->total > $editValue) {
    echo "error";
    exit();
  }
$data = array(
"$column"=>$editValue
);
$this->db->where('invoiceID',$invoiceID);
$this->db->update('invoice',$data);

}


function inline_edit_invoice_fee_structure(){

  $column = $this->input->post('column');
  $editValue =  $this->input->post('editValue');
  $invoiceID =  $this->input->post('invoiceID');
  $duration =  $this->input->post('duration');

$invoice  = $this->db->where('invoiceID',$invoiceID)->get('invoice')->row();

if ($column=='tuitionFee') {
 // $amount = $editValue-$invoice->discount+$invoice->welfareFund*$duration; uncommnet when add swf
  $amount = $editValue-$invoice->discount;
 $totalfeepaidperyear =  $amount/$duration;
}elseif ($column=='discount') {
 // $amount = $invoice->tuitionFee-$editValue+$invoice->welfareFund*$duration; uncommnet when add swf
  $amount = $invoice->tuitionFee-$editValue;
 $totalfeepaidperyear =  $amount/$duration;
}

$query  = $this->db->query("SELECT SUM(paymentamount) total FROM payment where invoiceID = '$invoiceID' and feeType = 'clg' ");


$beforeUpdatePayment =  $query->row()->total;
if ($amount<$beforeUpdatePayment) {
   echo "Exceede";
  exit();
}

$data = array(
"$column"=>round($editValue),
'amount'=>round($amount),
'totalfeepaidperyear'=>round($totalfeepaidperyear)
);
$this->db->where('invoiceID',$invoiceID);
$this->db->update('invoice',$data);

}

function inline_edit_invoice_fee_structure_forHostel(){
  $column = $this->input->post('column');
  $editValue =  $this->input->post('editValue');
  $invoiceID =  $this->input->post('invoiceID');
  $duration =  $this->input->post('duration');
  $month =  $this->input->post('month');
  $monthYear =  $this->input->post('monthYear');
$query  = $this->db->query("SELECT SUM(paymentamount) total FROM payment where invoiceID = '$invoiceID' and feeType = 'hstl' and paymentmonth = '$month' and paymentyear = '$monthYear' ");
  

$beforeUpdatePayment =  $query->row()->total;
if ($editValue<$beforeUpdatePayment) {
   echo "Exceede";
  exit();
}

$data = array(
"$column"=>$editValue,
);
$this->db->where('invoiceID',$invoiceID);
$this->db->update('invoice',$data);

}

function changeInvoicepaymenttype()
{
  $editValue =  $this->input->post('editValue');
  $paymentID =  $this->input->post('paymentID');
 $data = array(
  "paymenttype" =>$editValue
);

  $this->db->where('paymentID',$paymentID);
  $this->db->update('payment',$data);
}

function inline_edit_invoice()
{
  $column = $this->input->post('column');
  $amount = (int) $this->input->post('amount');
  $editValue =  $this->input->post('editValue');
  $paymentID =  $this->input->post('paymentID');
  $countPayment = $this->input->post('countPayment');
  $invoiceID =  $this->input->post('invoiceID');
  $feeType =  $this->input->post('feeType');
   $yearsOrSemester =  $this->input->post('yearsOrSemester');

  if ($column == 'paymentdate') {
  $data = array(
  "$column" =>date('Y-m-d', strtotime($editValue))
);

  $this->db->where('paymentID',$paymentID);
  $query = $this->db->update('payment',$data);
  }elseif ($column == 'paymentamount') {
if ($editValue=='') {
  echo "Empty";
  exit();
}
$this->db->where('invoiceID',$invoiceID);
$query = $this->db->get('invoice');
 $lastinvoice = $query->row();

$Lastupdatepaymentstatus = $lastinvoice->tuitionFee;
$queryupadet  = $this->db->query("SELECT SUM(paymentamount) total FROM payment where invoiceID = '$invoiceID' and feeType ='tuitionFee' and paymentID !='$paymentID' and yearsOrSemester = '$yearsOrSemester' ");
$stopPayment = $queryupadet->row()->total+$editValue;

if ($stopPayment > $Lastupdatepaymentstatus) {
echo $stopPayment;
  echo "Exceede";
  exit();
}

  $data = array(
  "$column" =>$editValue
);
  $this->db->where('paymentID',$paymentID);
  $this->db->update('payment',$data);

$query  = $this->db->query("SELECT SUM(paymentamount) total FROM payment where invoiceID = '$invoiceID' ");

$lastUpdatePayment =  $query->row()->total;
                          
$data = array(
  'paidamount' =>$lastUpdatePayment,
);
$this->db->where('invoiceID',$invoiceID);
$this->db->update('invoice',$data);


  }elseif($column == 'receipt'){
      $data = array(
  "$column" =>$editValue
);

  $this->db->where('paymentID',$paymentID);
  $query = $this->db->update('payment',$data);
  }

}



function EditSubCourse(){
  $usertype = $this->session->userdata("usertype");
    if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {
  $column = $this->input->post('column');
  $SubcourseID  = (int) $this->input->post('SubcourseID');
  $val =  $this->input->post('val');
  $data = array(
   "sub_course" =>$val, 
);

$this->db->where('sub_coursesID',$SubcourseID);
$query = $this->db->update('sub_courses',$data);
if ($query) {
  echo "string";
}else{
  echo "false";
}
}
}
function restore_subcourses(){
$adminID = $this->session->userdata("adminID");
$sub_coursesID = $this->input->post('sub_coursesID');
$classesID = $this->input->post('classesID');
$data =  array(
'status'=>1
);
$this->db->query("UPDATE sub_courses
  left JOIN student ON sub_courses.sub_coursesID = student.sub_coursesID
  left JOIN subject ON sub_courses.sub_coursesID = subject.sub_coursesID 
  left JOIN ets_quiz ON sub_courses.sub_coursesID = ets_quiz.sub_coursesID
  
   set student.status = 1 
 ,ets_quiz.status =1, subject.status = 1 ,sub_courses.status = 1 where sub_courses.sub_coursesID = $sub_coursesID");
$this->db->where('adminID',$adminID);
$this->db->where('sub_coursesID',$sub_coursesID);
$this->db->update('sub_courses',$data);
$this->CommonDataForSubCourses($adminID,$classesID);
}
function CommonDataForSubCourses($adminID,$classesID){
  $array = array(
  'classesID' =>$classesID,
   'adminID' =>$adminID,
   'status'=>2
);

$subCourses   = $this->classes_m->GetCoursesByParam($array);  
echo "<table class='table table-striped table-bordered table-hover dataTable no-footer'>
<thead>
<tr>
<th>S.No.</th>
<th>SubCourse Name</th>
<th>Action</th>
</tr>
</thead>
<tbody>";
$i =1;
foreach ($subCourses as $key => $value) {
                           echo "<tr><td>".$i++ ."</td>";
                           echo "<td>".$value->sub_course."</td>";
                          echo  "<td><a onclick='restore_subcourses(".$value->sub_coursesID.")'>Restore</a></td>";
                         echo "</tr>";
}
        echo "</tbody></table>";
}



function searchSitemap(){
$usertype  = $this->session->userdata('usertype');
$keyword = $this->input->post('keyword');
if($usertype == 'Student'){
  $where = "FIND_IN_SET('2', role)";   
  $this->db->where($where);
}
elseif ($usertype == 'Admin' || $usertype == 'ClgAdmin') {
  $where = "FIND_IN_SET('1', role)";   
  $this->db->where($where);
}else{
  $where = "FIND_IN_SET('3', role)";   
  $this->db->where($where);
}
$this->db->like('link', $keyword, 'both');
$result = $this->db->get('sitemap')->result();


echo "<ul>";
foreach ($result as $key => $value) {
 echo "<li onClick='selectSearchList()' class='selectSearchList' style='padding:5px;'><i class='".$value->link_icon."'>&nbsp;&nbsp;</i><a href ='".base_url().$value->link."'>".$value->link_name."</a></li>";
}
echo "</ul>";

 
}


public function AddDepartment(){

$department_name = $_POST['department_name'];

$digit = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 5 digit int
$char = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string
$setingsData = $this->setting_m->get_setting(array('adminID'=>$adminID));
$sitenameString   =  $setingsData->sname;
$firstCharacter = substr($sitenameString, 0, 2);
$department_code =   'dpt'.'-'.$char.$digit;

$data = array(
  'department_name'=>$department_name,
  'department_code'=>$department_code
  );
  $this->db->insert('department',$data); 
}

public function EditDepartment(){
  $id =  $this->input->post('id');
  $val =  $this->input->post('val');
  $data = array(
 'department_name'=>$val
  ); 
  $this->db->where('departmentID',$id);
  $this->db->update('department',$data);
  
}

}
 ?>