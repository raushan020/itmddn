<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


use Omnipay\Omnipay;

class ClgHeader extends Admin_Controller
{
    function __construct()
    {

		parent::__construct();
		$this->load->model('user_m');
		$language = $this->session->userdata('lang');
		$this->lang->load('invoice', $language);
		require_once(APPPATH."libraries/Omnipay/vendor/autoload.php");

	}
	public function index()
	{
	    $getclgadminid=$this->data["siteinfos"]->adminID;
	    $panel=$this->input->get('panel');
	    $this->data['showheader']=$this->user_m->clgheadershow($getclgadminid);
	    // print_r($this->data['showheader']);die;
		if($_POST)
		{
			$date=date("Y-m-d h:i");
			$headersetting=$this->input->post('headersetting');
			$checkadminID=$this->db->select('adminID')->where('adminID',$getclgadminid)->get('clgheadersetting')->row();
			if($checkadminID)
			{
				$saveheader= array('header' =>$headersetting,'modified_at'=>$date);
				$updateheader=$this->user_m->clgheaderupdate($saveheader,$getclgadminid);
				$this->session->set_flashdata('success',"Update Successfully"); 
				redirect(base_url("ClgHeader?panel=$panel"));
			}
			else
			{
				$saveheader= array('header' =>$headersetting,'adminID'=>$getclgadminid,'create_at'=>$date);
				$insertheader=$this->user_m->clgheadersave($saveheader);
				$this->session->set_flashdata('success',"Save Successfully"); 
				redirect(base_url("ClgHeader?panel=$panel"));
			}
			

		}
		$this->data["subview"] = "clgheader/headersetting";

		$this->load->view('_layout_main', $this->data);
	}

}
