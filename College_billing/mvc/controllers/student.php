<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Api2Pdf\Api2Pdf;

class Student extends Admin_Controller {

	function __construct () {
		parent::__construct();
		$this->load->model("student_m");
		$this->load->model("parentes_m");
		$this->load->model("semester_test_m");
		$this->load->model("invoice_gen_m");
		$this->load->model("sub_courses");
		$this->load->model("country_m");
		$this->load->model("year_m");
		$this->load->model("user_m");
		$this->load->model("teacher_m");
		$this->load->model("department_m");
		$this->load->model("classes_m");
		$this->load->model("subject_m");

		$language = $this->session->userdata('lang');
		$this->lang->load('student', $language);
	}

	public function index() 
	{
		$loginuserID = $this->session->userdata("loginuserID");
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$SpecialUsertype = $this->session->userdata('SpecialUsertype');
		if($usertype == "Teacher")
		{
			$rows_academic	= $this->db->select('std_add_status')->where('teacherID',$loginuserID)->get('teacher')->row()->std_add_status;	
			$this->data['rows_academic']= $rows_academic;
		}
		if(empty($this->session->userdata('DraftStudent')) && empty($this->session->userdata('TrashStudent')) && empty($this->session->userdata('ActiveStudent'))) 
		{
			$this->session->set_userdata('recentStudent',3);
		}
		$this->data['all_count']  =  $this->student_m->all_count();
		$this->data['ActiveStudent_count']  =  $this->student_m->ActiveStudent_count();
		$this->data['DraftStudent_count']  =  $this->student_m->DraftStudent_count();
		$this->data['TrashStudent_count']  =  $this->student_m->TrashStudent_count();
		$this->data['recentStudent_count']  =  $this->student_m->recentStudent_count();
		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin"  || $usertype == "Accountant"  || $usertype == "Support"|| $usertype == "Academic"   || $usertype == "Super_A_P" || $SpecialUsertype=='Btech') 
		{
			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			if ($this->input->post('classesID')) 
			{
				$this->session->unset_userdata('subCourseID');
				$this->session->set_userdata('classesID', $this->input->post('classesID'));
			}
			if ($this->input->post('subCourseID')) 
			{
				$this->session->set_userdata('subCourseID', $this->input->post('subCourseID'));
			}
		   if ($this->input->post('SessionFrom')) 
			{
				$this->session->set_userdata('SessionFrom', $this->input->post('SessionFrom'));
			}
			if ($this->input->post('SessionTo') && $this->input->post('SessionFrom')) 
			{
				$this->session->unset_userdata('SessionFrom');
				$SessionTo = $this->input->post('SessionTo');
				$SessionFrom = $this->input->post('SessionFrom');
				$sessionFilter = $SessionFrom.'-'.$SessionTo;
				$this->session->set_userdata('sessionFilter', $sessionFilter);
			}

			if ($this->input->post('sessionType')) 
			{
				$this->session->set_userdata('sessionType', $this->input->post('sessionType'));
			}

			if ($this->input->post('entryType')) 
			{
				$this->session->set_userdata('entryType', $this->input->post('entryType'));
			}

			if ($this->input->post('rmFilter')) 
			{
				$this->session->set_userdata('rmFilter', $this->input->post('rmFilter'));
			}
			if ($this->input->post('examType')) 
			{
				$this->session->set_userdata('examType', $this->input->post('examType'));
			}	
			if ($this->input->post('education_mode')) 
			{
				$this->session->set_userdata('education_mode', $this->input->post('education_mode'));
			}
			if ($this->input->post('teacherID')) 
			{
				$this->session->set_userdata('teacherID', $this->input->post('teacherID'));
			}
			if ($this->input->post('payment_status')) 
			{
				$this->session->set_userdata('payment_status', $this->input->post('payment_status'));
			}
			if ($this->input->post('student_position')) 
			{
				$this->session->set_userdata('student_position', $this->input->post('student_position'));
			}
			if ($this->input->post('yos_filter')) 
			{
				$this->session->set_userdata('yos_filter', $this->input->post('yos_filter'));
			}
			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('classesID'));
			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('classesID'));
			if($SpecialUsertype=='Btech')
			{
				$this->db->where('departmentID',6);
				$this->data['classes'] =  $this->db->get('classes')->result();
			}
			else if($usertype == "Teacher")
			{
				$loginuserID = $this->session->userdata('loginuserID');
				$this->data['classes'] = $this->classes_m->inner_join_with_fee_module($loginuserID);
			}
			else if($usertype == "Super_A_P")
			{
				$loginuserID = $this->session->userdata('loginuserID');
				$this->data['classes'] = $this->classes_m->get_class_super_ap($loginuserID);
			}
			else if( $usertype == "Admin")
			{
				$this->data['classes'] = $this->student_m->get_classes_by_Admin($adminID);	
			}
			else
			{
				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);	
			}
			$this->data['total_rm']  =  $this->user_m->total_rm();
			$this->data['students'] = $this->student_m->get_order_by_student_by_join_Count($adminID);

			$this->data['payment_amount'] = $this->student_m->get_order_by_student_by_join_payment($adminID);
			if($usertype == "Super_A_P")
			{ 
				$this->data['counsellorAP'] = $this->student_m->get_counsellor_by_spa($loginuserID);	
				$this->data['students'] =$this->student_m->get_students_by_superap_teacher($loginuserID);
			}	
			if($usertype=="Accountant" )
			{
				$this->data['counsellor'] = $this->student_m->get_counsellor_by_rm($loginuserID);				 	
			}
			else
			{
				$this->data['counsellor'] = $this->student_m->get_counsellor_by_rm($this->session->userdata('rmFilter'));	
			}	
			$this->data["subview"] = "student/search";
			$this->load->view('_layout_main', $this->data);		   
		}
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	function ajaxStudents()
	{
		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");
		$loginuserID = $this->session->userdata("loginuserID");
		$entrytype = $this->session->userdata('entryType');
		$totalData = $this->student_m->get_order_by_student_by_join_Count($adminID);
		$totalFiltered = $totalData;
		$posts = $this->student_m->make_datatables($adminID);
		$data = array();
		if(!empty($posts))
		{
			$i = 1;
			foreach ($posts as $post)
			{
				$array   =  "<div class='profile_pic stable_img'>
							<a href = ".base_url('profile/ChangePicture/'.$post->student_id).">
							<img src='".base_url('uploads/images/'.$post->photo)."' alt='user'>
							</a>
							</div>";		
				// $array = array(
				//                 "src" => base_url('uploads/images/'.$post->photo),
				//                 'width' => '35px',
				//                 'height' => '35px',
				//                 'class' => 'img-rounded'
				// 				);
				$examTypeArray =  array("o"=>"One Time","y"=>"Yearly");
				$education_modeArray =  array(1=>"Year",2=>"Semester");
				if($usertype == "Admin" || $usertype == "Super Admin"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant" || $usertype == "Teacher"  ) 
				{
					$nestedData['check'] = "<label class='nexCheckbox'><input type='checkbox' name='checked_id[]' class='mycheckbox' id ='dd' value=' ".$post->student_id." '/>
					<span class='checkmark checkmark-action-layout'></span>
					</label>";
				}
				$nestedData['sn2'] = '';
				$status = $post->invoiceStatus;
				if($status == 0) 
				{
					$status = 'Not Paid';
				} 
				elseif($status == 1) {
					$status = 'Partial Paid';
				} 
				elseif($status == 2) {
					$status = 'Full Paid';
				}
				// $status_button =   "<a class='btn btn-success btn-xs'>".$status."</a>";
				$status_button =   "";
				if ($post->yosPosition=='Passed Out') 
				{
					$yos  =   'Passed Out';
				}
				else
				{
					$yos = str_replace('_', ' ',  $post->studentYos);
				}
				$nestedData['img'] = $array;
				$nestedData['username'] = $post->username;
				$nestedData['studentRoll'] = $post->studentRoll;
				$nestedData['name'] = $post->name;
				$nestedData['father_name'] = $post->father_name;
				$nestedData['yearsOrSemester'] =  $yos;
				$nestedData['classes'] = $post->classes;  
				$nestedData['counsellor'] = $post->TeacherName;
				$nestedData['session'] = $post->session;
				$nestedData['sessionType'] = $post->sessionType;
				// $nestedData['education_mode'] = $education_modeArray[$post->education_mode];
				$nestedData['student_status'] = ucfirst($post->student_status);
				$nestedData['phone'] = $post->phone;
				$nestedData['dob'] = date('d-m-Y', strtotime($post->dob));
				$nestedData['sex'] = $post->sex;
				$nestedData['email'] = $post->email;
				$nestedData['mother_name'] = $post->mother_name;
				$nestedData['aadhar'] = $post->aadhar;
				$nestedData['nationality'] = $post->nationality;
				$nestedData['create_date'] = $post->student_create_date;
				$nestedData['amount'] = $post->amount;
				$nestedData['paidamount'] = $post->paidamount;
				$nestedData['invoiceStatus'] = " ";
				if($usertype == "Admin" || $usertype == "Super Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Accountant" || $usertype == "Support"|| $usertype == "Academic") 
				{
					if ($post->studentStatus!=2) 
					{
						$buttons =  btn_view('student/view/'.$post->student_id, $this->lang->line('view')).
						$buttons =  btn_print('student/print_info/'.$post->student_id, $this->lang->line('print'))."<a data-toggle='tooltip'  data-placement='top' title='Fee' href='".base_url()."invoice/view/$post->invoiceID?type=clg&year=".$post->studentYos."' class='btn btn-info info_1 for_margR'><i class='fa fa-inr'></i></a>";
						if ($this->session->userdata('ActiveStudent'))
						{ 
							$knowSemester = '';
							if($entrytype == 'Lateral')
							{
								$looping = (int)$post->studentYos;
								for ($i=$looping; $i >=3; $i--)
								{
									$knowSemester .= "<option value='".CallSemesterlateral($i)."'>".CallSemesterlateral($i)."</option>";
								}
							}
							else
							{
								$looping = (int)$post->studentYos;
								for ($i=$looping; $i >=1; $i--)
								{
									$knowSemester .= "<option value='".CallSemester($i)."'>".CallSemester($i)."</option>";
								}
							}

							$buttons =  btn_view('student/view/'.$post->student_id, $this->lang->line('view')).
							$buttons =  btn_print('student/print_info/'.$post->student_id, $this->lang->line('print'))."<a data-toggle='tooltip'  data-placement='top' title='Fee' href='".base_url()."invoice/view/$post->invoiceID?type=clg&year=".$post->studentYos."' class='btn btn-info info_1 for_margR'><i class='fa fa-inr'></i></a>
								<input type='hidden' id='course_".$post->student_id."' value='".$post->classes."'>
								<select class='knowSemester' id='".$post->student_id."'><option value=''>Select Semester</option>".$knowSemester."</select>";
						}
					}
					else
					{
						$buttons = btn_view('student/view/'.$post->student_id, $this->lang->line('view'));
					}
					$nestedData['action'] = $buttons;
				}
				else
				{
					$buttons  =  btn_view('student/view/'.$post->student_id, $this->lang->line('view'));		
					$nestedData['action'] = $buttons;  	
				}
				$data[] = $nestedData;
			}
		}

		$json_data = array(
			
						"recordsTotal"    => $totalData,  
						"recordsFiltered" => $totalFiltered, 
						"data"            => $data   
					);
		echo json_encode($json_data);
	}

	function attendancesheet()
	{
		$studentID = $this->input->post('studentID');
		$yearsOrSemester = $this->input->post('yearsOrSemester');
		$data['stuID'] = $studentID;
		$data['course'] = $this->input->post('course');
		$data['yearsOrSemester'] = $yearsOrSemester;
		$data['attendance'] = $this->student_m->attendance_sheet($studentID,$yearsOrSemester);
		$this->load->view('student/attendancesheet',$data);
	}

	protected function rules() {
		$rules = array(


			// array(
			// 	'field' => 'username',
			// 	'label' => $this->lang->line("student_username"),
			// 	'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
			// ),
			// array(
			// 	'field' => 'password',
			// 	'label' => $this->lang->line("student_password"),
			// 	'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean'
			// ),

			// array(
			// 	'field' => 'cpassword',
			// 	'label' => $this->lang->line("student_cpassword"),
			// 	'rules' => 'trim|min_length[4]|max_length[40]|xss_clean'
			// ),
		// array(
		// 		'field' => 'amount',
		// 		'label' =>'Total Fee',
		// 		'rules' => 'trim|required|max_length[20]|xss_clean'
		// 	),
		// 	array(
		// 		'field' => 'paidamount',
		// 		'label' =>'Admission Fee',
		// 		'rules' => 'trim|required|max_length[20]|xss_clean|callback_exceed_paidamount'
		// 	),
			array(
				'field' => 'name',
				'label' => $this->lang->line("student_name"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'father_name',
				'label' => $this->lang->line("father_name"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),
			array(
				'field' => 'mother_name',
				'label' => $this->lang->line("mother_name"),
				'rules' => 'trim|required|xss_clean|max_length[60]'
			),

			array(
				'field' => 'dob',
				'label' => $this->lang->line("student_dob"),
				'rules' => 'trim|required|max_length[10]|callback_date_valid|xss_clean'
			),
			array(
				'field' => 'sex',
				'label' => $this->lang->line("student_sex"),
				'rules' => 'trim|required|max_length[10]|xss_clean'
			),
			array(
				'field' => 'nationality',
				'label' => $this->lang->line("nationality"),
				'rules' => 'trim|max_length[25]|xss_clean'
			),
			array(
				'field' => 'street',
				'label' => $this->lang->line("street"),
				'rules' => 'trim|max_length[25]|xss_clean'
			),
			array(
				'field' => 'pin',
				'label' => $this->lang->line("pin"),
				'rules' => 'trim|min_length[4]|max_length[6]|xss_clean'
			),
			array(
				'field' => 'email',
				'label' => $this->lang->line("student_email"),
				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
			),
			// array(
			// 	'field' => 'email',
			// 	'label' => $this->lang->line("student_email"),
			// 	'rules' => 'trim|max_length[40]|valid_email|xss_clean|required'
			// ),
			array(
				'field' => 'phone',
				'label' => $this->lang->line("student_phone"),
				'rules' => 'trim|max_length[25]|min_length[5]|xss_clean|required'
			),
			array(
				'field' => 'address',
				'label' => $this->lang->line("student_address"),
				'rules' => 'trim|max_length[200]|xss_clean'
			),
			array(
				'field' => 'subCourseID',
				'label' => 'Sub Courses',
				'rules' => 'numeric|max_length[11]|xss_clean'
			),

			 array(
				'field' => 'classesID',
				'label' => $this->lang->line("student_classes"),
				'rules' => 'trim|required|numeric|max_length[11]|xss_clean'
			),

			array(
				'field' => 'roll',
				'label' => $this->lang->line("student_roll"),
				'rules' => 'trim|max_length[40]|xss_clean'
			),


			array(
				'field' => 'photo',
				'label' => $this->lang->line("student_photo"),
				'rules' => 'trim|max_length[200]|xss_clean'
			),

			array(
				'field' => 'sessionFrom',
				'label' => 'Session',
				'rules' => 'trim|required|max_length[200]|xss_clean'
			),

		    array(
				'field' => 'sessionTo',
				'label' => 'Session',
				'rules' => 'trim|required|max_length[200]|xss_clean'
			),

			

			array(
				'field' => 'education_mode',
				'label' =>'Session Type',
				'rules' => 'trim|required|max_length[20]|xss_clean'
			),

			array(
				'field' => 'yearsOrSemester',
				'label' =>'Year or Semester',
				'rules' => 'trim|max_length[20]|xss_clean|required'
			),		

		);
		return $rules;
	}

	function insert_with_image($username) {
	    $random = rand(1, 10000000000000000);
	    $makeRandom = hash('sha512', $random. $username . config_item("encryption_key"));
	    return $makeRandom;
	}

	public function add() {
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Teacher") 
		{
			if($usertype == "Teacher" )
			{
				$rows_academic	= $this->db->select('std_add_status')->where('teacherID',$loginuserID)->get('teacher')->row()->std_add_status;	
				$this->data['rows_academic']= $rows_academic;
				if($rows_academic==0)
				{
					$this->session->set_flashdata('error',$this->lang->line('menu_success'));
					redirect(base_url("student/index"));
				}
				$this->data['department'] = $this->department_m->fetch_departments('department');
				// $this->data['classes'] = $this->teacher_m->inner_join_with_fee_module($loginuserID);
			}
			else
			{
				$this->data['department'] = $this->department_m->fetch_departments('department');
				$this->data['classes'] = $this->student_m->get_classes($adminID);
			}
			$this->data['country'] = $this->country_m->get_country();
			$classesID = $this->input->post("classesID");
			$this->data['counsellor'] = $this->teacher_m->get_teacher_all($adminID);

			if($_POST) 
			{
				$getClass = $this->classes_m->get_single_classes($classesID);
				$regitrationID =  intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
				$rules = $this->rules();
				$this->form_validation->set_rules($rules);
				//	if ($this->form_validation->run() == FALSE) {
				if ($this->form_validation->run() == FALSE) 
				{
					//  echo validation_errors();
					// exit();
					$this->data["subview"] = "student/add";
					$this->load->view('_layout_main', $this->data);
				} 
				else 
				{
					$adhar_card = $_FILES["userfile_my"]["name"];
					
					$ext_adhar = pathinfo($adhar_card, PATHINFO_EXTENSION);
					$arr = array("jpeg","jpg","png","gif");
					if(in_array($ext_adhar,$arr))
					{
						$year = date("Y");
						$upload_aadhar = $this->input->post('upload_aadhar');
						$std_session = $this->input->post('sessionFrom').'-'.$this->input->post('sessionTo');
						$array = array();
						$array["name"] = $this->input->post("name");
						$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
						$array["sex"] = $this->input->post("sex");
						$array["father_name"] = $this->input->post("father_name");
						$array["mother_name"] = $this->input->post("mother_name");
						$array["email"] = $this->input->post("email");
						$array["phone"] = $this->input->post("phone");
						$array["adminID"] = $adminID;
						$array["address"] = $this->input->post("address");
						$array["aadhar"] = $this->input->post("aadhar");
						$array["aadhar"] = $this->input->post("aadhar");
						$array["street"] = $this->input->post("street");
						$array["pin"] = $this->input->post("pin");
						$array["classesID"] = $this->input->post("classesID");
						$dob             = $this->input->post('dob');
						$slashToHypne     = str_replace('/', '-', $dob);
						$dobIndian        = date("d-m-Y", strtotime($dob));
						$passwordPlusName = str_replace('-', '', $dobIndian);

						$array["registration_ID"] = $regitrationID;
						$array["sub_coursesID"] = $this->input->post("sub_coursesID");
						$array["nationality"] = $this->input->post("nationality");					
						$array["session"] = $std_session;
						$array["sessionType"] = $this->input->post('sessionType');
						$array["education_mode"] = $this->input->post("education_mode");
						$array["student_status"] = $this->input->post("student_status");
						$array["yearsOrSemester"] = $this->input->post("yearsOrSemester");
						$array["roll"] = $this->input->post("roll");
						$array["username"] = $regitrationID;
						$array['password'] = $this->student_m->hash($passwordPlusName);
						$array['usertype'] = "Student";
						$array['library'] = 0;
						$array['hostel'] = 0;
						$array['transport'] = 0;

						$array['create_date'] = date("Y-m-d");
						$array['year'] = $year;
						$array['yearID'] = return_year($this->input->post("yearsOrSemester"));
						$array['totalamount'] = 0;
						$array["create_date"] = date("Y-m-d h:i:s");
						$array["modify_date"] = date("Y-m-d h:i:s");
						$array["create_userID"] = $this->session->userdata('loginuserID');
						$array["create_username"] = $this->session->userdata('username');
						$array["create_usertype"] = $this->session->userdata('usertype');
						$array["studentactive"] = 1;
						$array['status'] = 3;
						if ($usertype == "Teacher") 
						{
							$array['counsellor'] = $this->session->userdata('loginuserID');
						}
						else
						{
							$array['counsellor'] = $this->input->post('counsellor');
						}
						$new_file = "defualt.png";
						$file_name_renameForDocs = $this->insert_with_image($this->input->post("username"));
						if($_FILES['userfile_my']['name'])
						{			
							$target_dir = "uploads/aadhar";
							$file_name_s =  time().$_FILES["userfile_my"]["name"];
							$target_file = $target_dir .'/'.time().$_FILES["userfile_my"]["name"];
							$uploadOk = 1;
							$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
							move_uploaded_file($_FILES["userfile_my"]["tmp_name"], $target_file);      
							$array['aadharPhoto'] = $file_name_s;
						}					
						if($_FILES['userfileSign']['name'])
						{		
							$config_sing['upload_path'] = 'uploads/signature';
							$config_sing['allowed_types'] = 'gif|jpg|png|jpeg';
							$config_sing['encrypt_name'] = TRUE;
							$this->load->library('upload', $config_sing);
							$this->upload->initialize($config_sing);
							$this->upload->do_upload('userfileSign');
							$this->gallery_path = realpath(APPPATH.'../uploads/signature');
							$filename = $this->upload->data();
							//$config_sing['source_image'] = $this->upload->upload_path.$this->upload->file_name;
							$config_sing['maintain_ratio'] = FALSE;
							$config_sing['width'] = 320; 
							$config_sing['height'] = 80;
							$this->load->library('image_lib',$config_sing);
							$this->image_lib->resize();
							$array['signature']=$this->upload->file_name;
						}

						if(isset($_POST["images_data"]) && $_POST["images_data"] !="") 
						{
							$data = $_POST["images_data"];
							$image_array_1 = explode(";", $data);
							$image_array_2 = explode(",", $image_array_1[1]);
							$data = base64_decode($image_array_2[1]);
							$imageName = 'student'.time() . '.png';
							$root = 'uploads/images/';
							file_put_contents($root.$imageName, $data);
							$array['photo']    = $imageName;
						}
						else
						{
							$array["photo"] = $new_file;
						}
						$insertID = $this->student_m->insert_student($array,$file_name_renameForDocs);
						$array_invoice = array();
						// $this->send_mail_new_regitration($insertID);
						$teacher_details= $this->teacher_m->teacher_x_fee_single($this->input->post('counsellor'),$this->input->post('classesID'));
						if ($teacher_details) 
						{
							$fee_amount =  $this->teacher_m->fee_single($teacher_details->feetypeID);
							if ($this->input->post('student_status')=='lateral') 
							{
								$lateral = 2000;
								$registration_fees= 1000;
							}
							else
							{
								$lateral =0;
								$registration_fees= 1000;
							}
							$exam_fees =  750;
							$totalAmount = $fee_amount->amount+$registration_fees+$lateral+$exam_fees;
							$array_invoice["amount"] = $totalAmount;
							$array_invoice["tuitionFee"] =	$fee_amount->amount;
							$array_invoice["totalfeepaidperyear"] =$totalAmount;
							$array_invoice["examinationFee"] = $exam_fees; //as exam fee
							$array_invoice["registration_fee"] = $registration_fees; //as exam fee
							$array_invoice["lateral"] = $lateral; //as exam fee
						}	     
						$array_invoice['adminID']=$adminID;
						$array_invoice['studentID']=$insertID;
						$array_invoice["yearsOrSemester"] = $this->input->post("yearsOrSemester");
						// $this->invoice_gen_m->AddInvoice($insertID,$array,$adminID);
						$this->invoice_gen_m->addInvoicewithpayment($array_invoice);
						$this->session->set_flashdata('success',$this->lang->line('menu_success'));
						redirect(base_url("student/index"));
					}
					else
					{
						$this->data['error_adhar'] = '<div class="alert-group">
														<div class="alert alert-danger alert-dismissable">
															<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
															<strong>Oh snap!</strong> Only jpeg, jpg, png and gif file are allowed in Adhar Card.
														</div>
													</div>';
						$this->data["subview"] = "student/add";
						$this->load->view('_layout_main', $this->data);
					}
				}
			} 
			else 
			{
				$this->data['error_adhar'] = '';
				$this->data["subview"] = "student/add";
				$this->load->view('_layout_main', $this->data);
			}
		} 
		else 
		{
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}





	public function edit() {
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == 'ClgAdmin'  || $usertype == "superadmin" ) {
			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
			if((int)$id) {

				$this->data['classes'] = $this->student_m->get_classes();				
				if ($usertype == "Teacher") {
					$this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID,"counsellor"=>$loginuserID));
				}else{
				    $this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID));
				}

				if($this->data['student']) {

				$this->data['counsellor'] = $this->student_m->counsellor();
				$this->data['get_sub_courses'] = $this->student_m->get_sub_courses($this->data['student']->classesID);
				$this->data['get_acadamic_reqired'] = $this->student_m->get_acadamic_reqired($this->data['student']->classesID);
				$this->data['classes_single'] = $this->classes_m->get_single_classes($this->data['student']->classesID);
				$ArrayForRequired =  explode(',', $this->data["classes_single"]->education_detailsID);
				$this->data['academic_details'] = $this->student_m->get_academic_details($id,$ArrayForRequired);
				$classesID = $this->data['student']->classesID;
				$this->data['set'] = $url;
					if($_POST) {

						$rules = $this->rules();
						
						// unset($rules[0],$rules[1],$rules[21]);
						$this->form_validation->set_rules($rules);
						if ($this->form_validation->run() == FALSE) {
					// 		print_r($rules);
					// echo validation_errors();
					// exit();
							$this->data["subview"] = "student/edit";
							$this->load->view('_layout_main', $this->data);
						} else {
							$array = array();
							$array["name"] = $this->input->post("name");
							$array["father_name"] = $this->input->post("father_name");
							$array["mother_name"] = $this->input->post("mother_name");
							$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
							$array["sex"] = $this->input->post("sex");
							// $array["employment_status"] = $this->input->post("employment_status");
							$array["email"] = $this->input->post("email");
							$array["phone"] = $this->input->post("phone");
							$array["address"] = $this->input->post("address");
							$array["street"] = $this->input->post("street");
							$array["pin"] = $this->input->post("pin");
							$array["sessionType"] = $this->input->post("sessionType");
							// $array["examType"] = $this->input->post("examType");
							$array["nationality"] = $this->input->post("nationality");
							$array["aadhar"] = $this->input->post("aadhar");
							$array["classesID"] = $this->input->post("classesID");
							$array["sub_coursesID"] = $this->input->post("sub_coursesID");

							if($this->input->post("cpassword")=='')
							{

								$array["password"] = $this->data['student']->password;
							}
							else
							{
								$array["password"] = $this->student_m->hash($this->input->post("cpassword"));
							}


						if($this->data['student']->status==1 || $this->data['student']->status==4)
						{
							if($this->input->post("roll")!='')
							{
								$array["roll"] = $this->input->post("roll");
								$array["status"] = 1;	
							}
						}
							$array['yearID'] = return_year($this->input->post("yearsOrSemester"));
						     $array["yearsOrSemester"] = $this->input->post("yearsOrSemester");
							$array["education_mode"] = $this->input->post("education_mode");
							// $array['password'] = $this->student_m->hash($this->input->post("password"));
							// $array['usertype'] = "Student";
											
					       $array["student_status"] = $this->input->post("student_status");

							$array["session"] = $this->input->post("sessionFrom").'-'.$this->input->post("sessionTo");
							
							$array["modify_date"] = date("Y-m-d h:i:s");
							$file_name_renameForDocs = $this->insert_with_image($this->data['student']
								->username);


if($_FILES['userfile_my']['name']){
if($this->data['student']->aadharPhoto!=''){
unlink(FCPATH.'uploads/aadhar/'.$this->data['student']->aadharPhoto);
}		
$target_dir = "uploads/aadhar";
$file_name_s =  time().$_FILES["userfile_my"]["name"];
$target_file = $target_dir .'/'.time().$_FILES["userfile_my"]["name"];
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

move_uploaded_file($_FILES["userfile_my"]["tmp_name"], $target_file);      
    		 $array['aadharPhoto'] = $file_name_s;
}

//*/signature
  

 if($_FILES['userfileSign']['name'])
{	
	
if($this->data['student']->signature!=''){
unlink(FCPATH.'uploads/signature/'.$this->data['student']->signature);
}				
		$config_sing['upload_path'] = 'uploads/signature';
		$config_sing['allowed_types'] = 'gif|jpg|png|jpeg';
		$config_sing['encrypt_name'] = TRUE;
		$this->load->library('upload', $config_sing);
		 $this->upload->initialize($config_sing);
		$this->upload->do_upload('userfileSign');
		
		$this->gallery_path = realpath(APPPATH.'../uploads/signature');
		$filename = $this->upload->data();
		//$config_sing['source_image'] = $this->upload->upload_path.$this->upload->file_name;
		$config_sing['maintain_ratio'] = FALSE;
		$config_sing['width'] = 320; 
		$config_sing['height'] = 80;
		$this->load->library('image_lib',$config_sing);
        $this->image_lib->resize();
        $array['signature']=$this->upload->file_name;
		
		  } 


							if(isset($_POST["images_data"]) && $_POST["images_data"] !="") {
								if($this->data['student']->photo != 'defualt.png') {
									unlink(FCPATH.'uploads/images/'.$this->data['student']->photo);
								}
							$data = $_POST["images_data"];
							$image_array_1 = explode(";", $data);
							$image_array_2 = explode(",", $image_array_1[1]);
							$data = base64_decode($image_array_2[1]);
							$imageName = 'student'.time() . '.png';
						    $root = 'uploads/images/';
							file_put_contents($root.$imageName, $data);
							$array['photo']    = $imageName;
					    										
							}
								$this->student_m->update_student($array, $id,$file_name_renameForDocs);
								$array_invoice = array();
						 // $this->send_mail_new_regitration($insertID);
						$teacher_details= $this->teacher_m->teacher_x_fee_single($this->input->post('counsellor'),$this->input->post('classesID'));
						if ($teacher_details) {
						  $fee_amount =  $this->teacher_m->fee_single($teacher_details->feetypeID);
						  if ($this->input->post('student_status')=='lateral') {
						  	$lateral = 2000;
						  	 $registration_fees= 1000;
						  }else{
						  	$lateral =0;
						  	$registration_fees= 1000;
						  }
						  $exam_fees =  750;
						  	 $totalAmount = $fee_amount->amount+$registration_fees+$lateral+$exam_fees;

							 $array_invoice["amount"] = $totalAmount;
							 $array_invoice["tuitionFee"] =	$fee_amount->amount;
							 $array_invoice["totalfeepaidperyear"] =$totalAmount;
							 $array_invoice["examinationFee"] = $exam_fees; //as exam fee
							 $array_invoice["registration_fee"] = $registration_fees; //as exam fee
							 $array_invoice["lateral"] = $lateral; //as exam fee
						}
						     $array_invoice["yearsOrSemester"] = $this->input->post("yearsOrSemester");

						$this->invoice_gen_m->updateInvoicewithpayment($array_invoice,$id);

								$this->session->set_flashdata('success', $this->lang->line('menu_success'));
								redirect(base_url("student/index/$url"));
							
						}
					} else {
						$this->data["subview"] = "student/edit";
						$this->load->view('_layout_main', $this->data);
					}
				} else {

					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function view() {
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Accountant" || $usertype == "Academic" || $usertype == "Super_A_P") {
			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
			if ((int)$id) {
			if ($usertype == "Teacher") {
					$this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID,"counsellor"=>$loginuserID));
				}else{
				    $this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID));
				}
				if($this->data["student"]){
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
			    $this->data["education"] = $this->student_m->get_educations($id,$ArrayForRequired);
				if($this->data["student"] && $this->data["class"]) {
				    $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);
					$this->data['set'] = $url;
					if ($this->data["student"]->parentID) {
						$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
					}
					$this->data["subview"] = "student/view";
					$this->load->view('_layout_main', $this->data);
				} else {
				
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}

			}else{
				    $this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
			}


			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

public function print_info(){

$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support"  || $usertype == "Academic" || $usertype == "Accountant" ) {
			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
			if ((int)$id) {
			if ($usertype == "Teacher") {
					$this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID,"counsellor"=>$loginuserID));
				}else{
				    $this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID));
				}
				if($this->data["student"]){
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
			    $this->data["education"] = $this->student_m->get_educations($id,$ArrayForRequired);
				if($this->data["student"] && $this->data["class"]) {
				    $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);
					$this->data['set'] = $url;
					if ($this->data["student"]->parentID) {
						$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
					}
					
					$data_print  = $this->load->view("student/print_info", $this->data, true);
require_once APPPATH.'libraries/apitopdf/Api2Pdf.php';
require_once APPPATH.'libraries/apitopdf/ApiResult.php';

$key = 'a2448257-0c4a-42d0-bd84-d174e827a22e';
$apiClient = new Api2Pdf($key);
$apiClient->setInline(true);
$apiClient->setFilename('test.pdf');
$apiClient->setOptions(
    [
        'printMediaType' => true,
        'images'=>true,
        'keepRelativeLinks'=>true,
        'headerLine'=>true,
        'pageSize'=> 'A4'
    ]
);
$result = $apiClient->wkHtmlToPdfFromHtml($data_print);

echo '<a target = "_blank" href="'.$result->getPdf().'" class="btn btn-success" style="float: right; margin: 20px;">Print</a>';
print_r($data_print );

				} else {
				
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}

			}else{
				    $this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
			}


			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}

}
public function print_id_preview(){

$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support"  || $usertype == "Academic" || $usertype == "Accountant"  || $usertype == "Student") {
			$id = $this->session->userdata('loginuserID');
			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
			if ((int)$id) {
			if ($usertype == "Teacher") {
					$this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID,"counsellor"=>$loginuserID));
				}else{
				    $this->data['student'] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$adminID));
				}
				if($this->data["student"]){
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
			    $this->data["education"] = $this->student_m->get_educations($id,$ArrayForRequired);
				if($this->data["student"] && $this->data["class"]) {
				    $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);
					$this->data['set'] = $url;
					if ($this->data["student"]->parentID) {
						$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
					}
					
					$data_print  = $this->load->view("student/print_id_preview", $this->data, true);
require_once APPPATH.'libraries/apitopdf/Api2Pdf.php';
require_once APPPATH.'libraries/apitopdf/ApiResult.php';

$key = 'a2448257-0c4a-42d0-bd84-d174e827a22e';
$apiClient = new Api2Pdf($key);
$apiClient->setInline(true);
$apiClient->setFilename('test.pdf');
$apiClient->setOptions(
    [
        'printMediaType' => true,
        'images'=>true,
        'keepRelativeLinks'=>true,
        'headerLine'=>true,
        'pageSize'=> 'A4',
        'enableJavascript'=>true
    ]
);
$result = $apiClient->wkHtmlToPdfFromHtml($data_print);

// echo '<a href="'.$result->getPdf().'">Print</a>';
print_r($data_print );

				} else {
				
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}

			}else{
				    $this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
			}


			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}

}




	public function print_preview() {

		$loginuserID = $this->session->userdata("loginuserID");
		$usertype = $this->session->userdata("usertype");
	    $adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Student" || $usertype == "superadmin" || $usertype == "Support" || $usertype == "Academic" || $usertype == "Accountant" || $usertype == "Teacher" || $usertype == "Super_A_P" ) {
			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));
		
	
			if ((int)$id) {
					$usertype = $this->session->userdata("usertype");
				if ($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "Support" || $usertype == "Academic" || $usertype == "Accountant" || $usertype == "Teacher" || $usertype == "Super_A_P") {
				$this->data["student"] = $this->student_m->get_student(array('studentID'=>$id,'adminID'=>$adminID));
               }
				
		        $this->data["education"] = $this->student_m->get_educations($id,$adminID);
				if($this->data["student"]) {
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
				$this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				$this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

				    $this->load->library('html2pdf');
				    $this->html2pdf->folder('./assets/pdfs/');
				    $this->html2pdf->filename('Report.pdf');
				    $this->html2pdf->paper('a4', 'portrait');
				    $this->data['panel_title'] = $this->lang->line('panel_title');
					$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);

					$html = $this->load->view('student/print_preview', $this->data, true);
					print_r($html);
					exit();
					$this->html2pdf->html($html);
					$this->html2pdf->create();
				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}



	public function id_card() {

		$usertype = $this->session->userdata("usertype");
		$username = $this->session->userdata('username');
		if($usertype == "Student"){
			$student = $this->student_m->get_single_student(array('username' => $username));
			$this->data["student"] = $this->student_m->get_student(array('studentID'=>$student->studentID,'adminID'=>$student->adminID));
			$this->data["class"] = $this->student_m->get_class($student->classesID);
			if($this->data["class"]) {
				$ArrayForRequired =  explode(',', $this->data["class"]->education_detailsID);
		$this->data["education"] = $this->student_m->get_educations($student->studentID,$ArrayForRequired);

			     $this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
				    $this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

				// $this->data["parent"] = $this->parentes_m->get_parentes($student->parentID);

				$this->data["subview"] = "student/id_card";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}


	public function send_mail() {
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");
		if($usertype == "Admin" || $usertype == 'ClgAdmin') {
			$id = $this->input->post('id');
			$url = $this->input->post('set');

			if ((int)$id) {
				$this->data["student"] = $this->student_m->get_student(array("studentID"=>$id,"adminID"=>$loginuserID));
	

				$this->data["education"] = $this->student_m->get_educations($id);
				$this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);
	
				if($this->data["student"] && $this->data["class"]) {
				   $this->load->library('html2pdf');
				   $this->html2pdf->folder('uploads/report');
				   $this->html2pdf->filename('Report.pdf');
				   $this->html2pdf->paper('a4', 'portrait');
				   $this->data['panel_title'] = $this->lang->line('panel_title');
     
				$this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
				$this->data["sub_courses"] = $this->student_m->get_subCourses($this->data["student"]->sub_coursesID);
			    $this->data['get_session'] = $this->student_m->get_session_id($this->data["student"]->session);

				$this->data['counsellor'] = $this->student_m->counsellor_id($this->data["student"]->counsellor);

					$html = $this->load->view('student/print_preview', $this->data, true);
			
					$this->html2pdf->html($html);
					$this->html2pdf->create('save');
					if($path = $this->html2pdf->create('save')) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
					$this->email->to($this->input->post('to'));
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('message'));
					$this->email->attach($path);

						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
					}

				} else {
					$this->data["subview"] = "error";
					$this->load->view('_layout_main', $this->data);
				}
			} else {
				$this->data["subview"] = "error";
				$this->load->view('_layout_main', $this->data);
			}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}

public function send_mail_new_regitration($insertID) {
		$usertype = $this->session->userdata("usertype");
		$loginuserID = $this->session->userdata("loginuserID");
		if($usertype == "Admin" || $usertype == 'ClgAdmin') {
   
       $this->db->select('*');
        $this->db->where('studentID',$insertID);
        $this->db->from('student');
      $query2  = $this->db->get();

      $dataEamil['emailData'] = $query2->row();

$this->load->library('email');
$config=array(
'charset'=>'utf-8',
'wordwrap'=> TRUE,
'mailtype' => 'html'
);

                    $this->email->initialize($config);
					$html = $this->load->view('emailTemplates/newRegisterStudent', $dataEamil , true);
					$this->email->set_mailtype("html");
					$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
					$this->email->to($this->input->post('email'));
					$this->email->subject("Welecome To Clg");
					$this->email->message($html);
						if($this->email->send()) {
							$this->session->set_flashdata('success', $this->lang->line('mail_success'));
						} else {
							$this->session->set_flashdata('error', $this->lang->line('mail_error'));
						}
		} else {
			$this->data["subview"] = "error";
			$this->load->view('_layout_main', $this->data);
		}
	}
	public function unique_roll() {
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "studentID !=" => $id, "classesID" => $this->input->post('classesID')));
			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "%s already exists");
				return FALSE;
			}
			return TRUE;
		} else {
			$student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "classesID" => $this->input->post('classesID')));

			if(count($student)) {
				$this->form_validation->set_message("unique_roll", "%s already exists");
				return FALSE;
			}
			return TRUE;
		}
	}

	public function lol_username() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == 'ClgAdmin') {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), "email !=" => $student_info->email));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {	
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
				if(count($user)) {
					$this->form_validation->set_message("lol_username", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}

			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}
}
	public function date_valid($sec) {
		$date = date("d-m-Y", strtotime($sec));
		if(strlen($date) <10) {
			$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     	return FALSE;
		} else {
	   		$arr = explode("-", $date);
	        $dd = $arr[0];
	        $mm = $arr[1];
	        $yyyy = $arr[2];
	      	if(checkdate($mm, $dd, $yyyy)) {
	      		return TRUE;
	      	} else {
	      		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	     		return FALSE;
	      	}
	    }
	}

	public function unique_classesID() {
		if($this->input->post('classesID') == 0) {
			$this->form_validation->set_message("unique_classesID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	public function unique_sectionID() {
		if($this->input->post('sectionID') == 0) {
			$this->form_validation->set_message("unique_sectionID", "The %s field is required");
	     	return FALSE;
		}
		return TRUE;
	}

	public function student_list() {
		$classID = $this->input->post('id');
		if((int)$classID) {
			$string = base_url("student/index/$classID");
			echo $string;
		} else {
			redirect(base_url("student/index"));
		}
	}

	public function unique_email() {
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
			
				return FALSE;
			} else {
		
				return TRUE;
			}
		}
	}


	public function exceed_paidamount(){
		$amount = $this->input->post('amount');
		$paidamount = $this->input->post('paidamount');
		
		if($amount < $paidamount){
			$this->form_validation->set_message("exceed_paidamount", "%s is Exceede");
     return FALSE;
		}else{

		return TRUE;
			
		}
	}


	function sectioncall() {
		$classesID = $this->input->post('id');
		if((int)$classesID) {
			$allsection = $this->section_m->get_order_by_section(array('classesID' => $classesID));
			echo "<option value='0'>", $this->lang->line("student_select_section"),"</option>";
			foreach ($allsection as $value) {
				echo "<option value=\"$value->sectionID\">",$value->section,"</option>";
			}
		}
	}

function checkemail(){
	$usertype = $this->session->userdata("usertype");
	$loginuserID = $this->session->userdata("loginuserID");
	if($usertype == "Admin" || $usertype == 'ClgAdmin') {
		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));
		if((int)$id) {
			$student_info = $this->student_m->get_single_student(array('studentID' => $id));
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');
			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $student_info->username ));
				if(count($user)) {
					$this->form_validation->set_message("unique_email", "%s already exists");
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin');

			$array = array();
			$i = 0;
			foreach ($tables as $table) {
				$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
				if(count($user)) {
					$array['permition'][$i] = 'no';
				} else {
					$array['permition'][$i] = 'yes';
				}
				$i++;
			}
			if(in_array('no', $array['permition'])) {
			 echo "no";
			} else {
			 echo "yes";
			}
		
		}
}
}
function required_mode(){

if ($this->input->post('education_mode')==2) {
	if ($this->input->post('semesterId')=='') {
	 $this->form_validation->set_message("semesterId","Please Select Semester");
	 return false;
	}else{
		return true;
	}
}else{
    if ($this->input->post('yearID')=='') {
	 $this->form_validation->set_message("yearID","Please Select Year");
	 return false;
	}else{
		return true;
	}
}

}

function Get_subCourses(){

	if($this->signin_m->loggedin() == FALSE) {
	header("Refresh:0");
	}
    $id = $this->input->post('id');
    $this->db->where('classesID',$id);
    $this->db->where('status',1);
    $getSubcourses =  $this->db->get('sub_courses');
    if ($getSubcourses->result_array()) {
		if(form_error('subCourseID')) 
			echo "<div class='form-group has-error' >";
		else     
			echo "<div class='form-group' >";
			echo "<label for='sectionID' class='col-sm-2 control-label' >Course Subcategory<span class='red-color'>*</span></label>
					<div class='col-sm-6'> 
						<select id='sub_CourseID' required  name='sub_coursesID' class='form-control' value='<?php echo set_value('sub_coursesID'); ?>'>
							<option value='' selected='selected' >Select Cource Subcategory</option>";

    foreach ($getSubcourses->result_array() as $key => $value) {
    	echo "<option value =".$value['sub_coursesID'].">".$value['sub_course']."</option>";
	}
	echo "</div>
		<span class='col-sm-4 control-label'>".form_error('subCourseID')."</span></div>";
    } 
}

function Get_subject(){

	if($this->signin_m->loggedin() == FALSE) {
	header("Refresh:0");
	}
    $id = $this->input->post('id');
    $classesID=$this->input->post('classesID');
    $this->db->where('classesID',$classesID);
    $this->db->where('yearsOrSemester',$id);
    $this->db->where('status',1);
    $getSubcourses =  $this->db->get('subject');
    if ($getSubcourses->result_array()) {
		if(form_error('subjectID')) 
			echo "<div class='form-group has-error' >";
		else     
			echo "<div class='form-group' >";
			echo "<label for='sectionID' class='col-sm-2 control-label' >Subject<span class='red-color'>*</span></label>
					<div class='col-sm-6'> 
						<select id='sub_CourseID' required  name='subjectID' class='form-control' value='<?php echo set_value('subjectID'); ?>'>
							<option value='' selected='selected' >Select Subject</option>";

    foreach ($getSubcourses->result_array() as $key => $value) {
    	echo "<option value =".$value['subjectID'].">".$value['subject']."</option>";
	}
	echo "</div>
		<span class='col-sm-4 control-label'>".form_error('subjectID')."</span></div>";
    } 
}

function ResetrmFilter(){ 

	$this->session->unset_userdata('rmFilter');
	$this->session->unset_userdata('teacherID');
	   
}

function ResetSesession(){ 

	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('SessionFrom');
	   
}

function ResetCourses(){

	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('yos_filter');
	   
}

function ResetSubcourses(){

	$this->session->unset_userdata('subCourseID');

}

function ResetsessionType(){

	$this->session->unset_userdata('sessionType');

}

function ResetentryType(){

	$this->session->unset_userdata('entryType');

}

function ResetexamType(){

	$this->session->unset_userdata('examType');

}

function Reseteducation_mode(){

	$this->session->unset_userdata('education_mode');

}

function ResetteacherID(){

	$this->session->unset_userdata('teacherID');

}

function ResetteacherID_extra(){

	$this->session->unset_userdata('teacherID_extra');

}

function Resetpaymentstatus(){

	$this->session->unset_userdata('payment_status');

}

function Resetstudentposition(){

	$this->session->unset_userdata('student_position');

}

function Resetyos(){
	$this->session->unset_userdata('yos_filter');
}

  

function ResetAllfilter(){
	$this->session->unset_userdata('student_position');
	$this->session->unset_userdata('yos_filter');
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('payment_status');	
	$this->session->unset_userdata('teacherID');   
	$this->session->unset_userdata('rmFilter');
	$this->session->unset_userdata('SessionFrom');
}

function ResetMorefilter(){
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('teacherID');
	   
}


public function uploadPicByCrop(){
 // $uri = $this->uri->segment(3);
 $data = $_POST["image"];
echo "<input type ='hidden' value = '".$data."' name = 'images_data' >";
}


	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {

				$data = array(
                'status'=>2
               );
				$this->db->where('studentID',$id);

	    $loginuserID = $this->session->userdata("loginuserID");
		if ($usertype=='Teacher') {
		 $this->db->where('counsellor', $loginuserID);
		  }
	 $affected	= $this->db->update('student',$data);
if ($affected) {
				$data =  array(
				'current_login'=>0
				);
				$this->db->where('studentID',$id);
				$this->db->update('school_sessions',$data);
}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));

				
			} else {

				redirect(base_url("student/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}


	public function restore() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == "superadmin" || $usertype == 'ClgAdmin') {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			if((int)$id) {
$data = array(
'status'=>1
);
				$this->db->where('studentID',$id);
				$this->db->update('student',$data);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("student/index"));

			} else {

				redirect(base_url("student/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	function multipleAction()
	{
		if ($this->input->post('Delete')) 
		{
			$loginuserID = $this->session->userdata("loginuserID");
			$adminID = $this->session->userdata("adminID");
			$usertype = $this->session->userdata("usertype");
			if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == 'Accountant')
			{
				$checked_id =  array_unique($this->input->post('checked_id'));
				$loginuserID = $this->session->userdata("loginuserID");
				$adminID = $this->session->userdata("adminID");
				$usertype = $this->session->userdata("usertype");
				for ($i=0; $i <count($checked_id); $i++) 
				{ 
					$this->db->select('studentID,name,father_name,mother_name');
					$this->db->where('studentID',$checked_id[$i]);
					$students_signle  = $this->db->get('student')->row();
					$array[$i]['name'] = $students_signle->name;
					$array[$i]['father_name'] = $students_signle->father_name;
					$array[$i]['mother_name'] = $students_signle->mother_name;
					$data_update  = array(
						'comments' =>$this->input->post('reminder_text'),
						'modify_date'=> date("Y-m-d h:i:s"),
						'update_username'=>$this->session->userdata('username'),
						'status'=>2,
					);
					$this->db->where('studentID',$students_signle->studentID);
					$affected = $this->db->update('student',$data_update);
					if ($affected) {
						$data =  array(
							'current_login'=>0
						);
						$this->db->where('studentID',$students_signle->studentID);
						$this->db->update('school_sessions',$data);
					}
				}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
				// $sender_email	= $this->data["siteinfos"]->email;
				// $emailData['emailData_student'] = $array;
				// $emailData['email_text'] =$this->input->post('reminder_text');  
				// $this->load->library('email');
				// $config=array(
				// 'charset'=>'utf-8',
				// 'wordwrap'=> TRUE,
				// 'mailtype' => 'html'
				// );
				//                     $this->email->initialize($config);
				// 					$html = $this->load->view('emailTemplates/remider', $emailData , true);
				// 					$this->email->set_mailtype("html");
				// 					$this->email->from($this->session->userdata('email'), $this->session->userdata('name'));
				// 					$this->email->to($sender_email);
				// 					$this->email->subject("email from academic partner");
				// 					$this->email->message($html);
				//    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
				// redirect(base_url("student/index"));
			}					
		}
		if ($this->input->post('Draft')) 
		{
			$checked_id =  $this->input->post('checked_id');
			$loginuserID = $this->session->userdata("loginuserID");
			$adminID = $this->session->userdata("adminID");
			$usertype = $this->session->userdata("usertype");
			for ($i=0; $i <count($this->input->post('checked_id')) ; $i++) 
			{ 
				$this->db->where('studentID',$checked_id[$i]);
				$students_signle  = $this->db->get('student')->row();

				if($students_signle->status==3){
					$this->session->set_flashdata('error', "New student can not add in enrollment list without Enrollment number");
					redirect(base_url("student/index"));
				}

				$data = array(
					'modify_date'=> date("Y-m-d h:i:s"),
					'update_username'=>$this->session->userdata('username'),
					'status'=>4
				);
				$this->db->where('studentID',$checked_id[$i]);
				$loginuserID = $this->session->userdata("loginuserID");
				$usertype = $this->session->userdata("usertype");
				if ($usertype=='Teacher') {
					$this->db->where('counsellor', $loginuserID);
				}
				$affected = $this->db->update('student',$data);
				if ($affected) {
					$data =  array(
					'current_login'=>0
					);
					$this->db->where('studentID',$checked_id[$i]);
					$this->db->update('school_sessions',$data);
				}
			}
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("student/index"));
		}
		if ($this->input->post('Active')) 
		{
			$loginuserID = $this->session->userdata("loginuserID");
			$adminID = $this->session->userdata("adminID");
			$usertype = $this->session->userdata("usertype");
			if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == 'Accountant')
			{
				$checked_id =  array_unique($this->input->post('checked_id'));
				for ($i=0; $i <count($checked_id); $i++) 
				{ 
					$this->db->where('studentID',$checked_id[$i]);
					$students_signle  = $this->db->get('student')->row();
					$this->db->where('studentID',$students_signle->studentID);
					$this->db->where('status_position',1);
					$invoice_details  = $this->db->get('invoice')->row();
					$data = array(
						'modify_date'=> date("Y-m-d h:i:s"),
						'update_username'=>$this->session->userdata('username'),
					);
					$check_enroll_status =  $students_signle->roll;
					if($check_enroll_status!='' && $check_enroll_status!=0){
					$data['status'] = 1;
					}else{
						if($invoice_details->status!=3){
							$data['status'] = 4;	
						}else{
							$data['status'] = 3;	
						}	
					}
					$this->db->where('studentID',$checked_id[$i]);
					$this->db->update('student',$data);
				}
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
			}
		}
		if ($this->input->post('Pay')) 
		{
			$checked_id =  array_unique($this->input->post('checked_id'));
			$this->data['total_student']  =  $this->student_m->find_student_in_array($checked_id); 
			$this->data['teacher']  =  $this->teacher_m->get_teacher_single($this->session->userdata('teacherID')); 
			$this->data['passbook_balance']  =  $this->teacher_m->total_balance_passbook($this->session->userdata('teacherID')); 


			$this->data["subview"] = "student/confirmation";
			$this->load->view('_layout_main', $this->data);
			// 	if($usertype == 'ClgAdmin' || $usertype == 'Admin' || $usertype == 'Accountant'){

			// 			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			// 			redirect(base_url("student/index"));

			// }
		}
		if ($this->input->post('Promote')) 
		{
			$checked_id =  array_unique($this->input->post('checked_id'));
			$this->data['total_student']  =  $this->student_m->find_student_in_array($checked_id);
		
		
			$this->data["subview"] = "student/promote";
			$this->load->view('_layout_main', $this->data);
		}
		if($this->input->post('zipfile'))
		{
			$checked_id =  array_unique($this->input->post('checked_id'));
			$this->data['checked_id'] = $checked_id;
			$yearsOrSemester = $this->session->userdata('yos_filter');
			$classesID = $this->session->userdata('classesID');
			$posts = $this->student_m->course($classesID);
			$this->data['course'] = $posts->classes;
			$this->data['yearsOrSemester'] = $yearsOrSemester;
			$this->data['attendance_zip'] = $this->student_m->attendance_zip($checked_id,$yearsOrSemester);
			$this->load->view('student/zipfile',$this->data);
			$html = $this->output->get_output();
	        $this->load->library('pdf');
	        $this->dompdf->setBasePath(realpath(FCPATH . '/assets/plugins/bootstrap/css/bootstrap.css'));
	        $this->dompdf->loadHtml($html);
	        $this->dompdf->setPaper('A4', 'landscape');
	        $this->dompdf->render();
	        $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
		}
		if ($this->input->post('Remider')) 
		{
			$checked_id =  array_unique($this->input->post('checked_id'));
			$loginuserID = $this->session->userdata("loginuserID");
			$adminID = $this->session->userdata("adminID");
			$usertype = $this->session->userdata("usertype");

			for ($i=0; $i <count($checked_id); $i++) { 
				$this->db->select('studentID,name,father_name,mother_name');
				$this->db->where('studentID',$checked_id[$i]);
				$students_signle  = $this->db->get('student')->row();
				$array[$i]['name'] = $students_signle->name;
				$array[$i]['father_name'] = $students_signle->father_name;
				$array[$i]['mother_name'] = $students_signle->mother_name;
				if($usertype == 'Admin') {
					$data_update  = array(
						'comments' =>$this->input->post('reminder_text1'),
						'modify_date'=> date("Y-m-d h:i:s"),
						'update_username'=>$this->session->userdata('username')
					);
				}
				else
				{
					$data_update  = array(
						'comments' =>$this->input->post('reminder_text'),
						'modify_date'=> date("Y-m-d h:i:s"),
						'update_username'=>$this->session->userdata('username')
					);
				}

				// print_r($data_update);
				// die();

				$this->db->where('studentID',$students_signle->studentID);
				$this->db->update('student',$data_update);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
			}
			if($usertype == 'Teacher') 
			{
				$sender_email	= $this->data["siteinfos"]->email;
				$emailData['emailData_student'] = $array;

				$emailData['email_text'] =$this->input->post('reminder_text');  
				$this->load->library('email');
				$config=array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html'
				);

				$this->email->initialize($config);
				$html = $this->load->view('emailTemplates/remider', $emailData , true);
				$this->email->set_mailtype("html");
				$this->email->from($this->session->userdata('email'), $this->session->userdata('name'));
				$this->email->to($sender_email);
				$this->email->subject("email from academic partner");
				$this->email->message($html);
				if($this->email->send()) {
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("student/index"));
				}
			}
		}
	}


	function bulk_payment()
	{
		$checked_id = $this->input->post('checked_id');
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		$id = $this->session->userdata('teacherID');
		$bothid = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) ); // 
		$this->data['passbook_balance']  =  $this->teacher_m->total_balance_passbook($this->session->userdata('teacherID')); 
		if($this->data['passbook_balance'])
		{
			$balance_now = $this->data['passbook_balance']->balance;
		}
		else
		{
			$balance_now = 0;
		}
		$this->db->where('debitID',$bothid);
		$num_rows_pass = $this->db->get('passbook')->num_rows();
		if($num_rows_pass==0)
		{
			$data =array(
				'debitID'=>$bothid,
				'debit'=>0,
				'debit_credit_status'=>0,
				'balance'=>$balance_now,
				'teacherID'=>$id
			); 
			$this->db->insert('passbook',$data);
			$insertID = $this->db->insert_id();
		}
		$total_payment_done = 0;
		$balance_now_for_update = $balance_now;
		$before_balance_now = $balance_now; 
		for ($i=0; $i <count($checked_id) ; $i++) 
		{ 
			$this->db->select('name,studentID,status,yearsOrSemester');
			$this->db->where('studentID',$checked_id[$i]);
			$students_signle  = $this->db->get('student')->row();
			$this->db->where('studentID',$students_signle->studentID);
			$this->db->where('status_position',1);
			$invoice_details  = $this->db->get('invoice')->row();
			// if($i==2){
			// print_r($invoice_details->status);die;	
			// }
			if($invoice_details->status!=2)
			{
				$totalamount_paid = $invoice_details->amount-$invoice_details->paidamount;
				$before_balance_now -= $totalamount_paid;
				if($before_balance_now>0 & $before_balance_now!=0)
				{
					$balance_now_for_update -= $totalamount_paid;	
					$total_payment_done += $totalamount_paid;
					if($students_signle->yearsOrSemester=='1st_Semester' or $students_signle->yearsOrSemester=='1st_Year')
					{
						$patment_payment_registration = array(
							'adminID'=>$adminID,
							'studentID'=>$students_signle->studentID,	
							'invoiceID'=>$invoice_details->invoiceID,
							'paymenttype'=>"Cash",
							'paymentdate'=>date('Y-m-d'),
							'paymentamount'=>$invoice_details->registration_fee,
							'feeType'=>'registration_fee',
							'yearsOrSemester'=>$students_signle->yearsOrSemester,
							'uname'=>$this->session->userdata('username'),
							'usertype'=>$this->session->userdata('usertype'),
							'userID'=>$this->session->userdata('loginuserID'),
							'paymentdateUpdate'=>date('Y-m-d'),
						);
						$this->db->insert('payment',$patment_payment_registration);
					}
					else
					{
						if($students_signle->student_status=='lateral')
						{
							$patment_payment_lateral = array(
								'adminID'=>$adminID,
								'studentID'=>$students_signle->studentID,	
								'invoiceID'=>$invoice_details->invoiceID,
								'paymenttype'=>"Cash",
								'paymentdate'=>date('Y-m-d'),
								'paymentamount'=>$invoice_details->lateral,
								'feeType'=>'lateral',
								'yearsOrSemester'=>$students_signle->yearsOrSemester,
								'uname'=>$this->session->userdata('username'),
								'usertype'=>$this->session->userdata('usertype'),
								'userID'=>$this->session->userdata('loginuserID'),
								'paymentdateUpdate'=>date('Y-m-d'),
							);
							$this->db->insert('payment',$patment_payment_lateral);

							$patment_payment_reRegis = array(
								'adminID'=>$adminID,
								'studentID'=>$students_signle->studentID,	
								'invoiceID'=>$invoice_details->invoiceID,
								'paymenttype'=>"Cash",
								'paymentdate'=>date('Y-m-d'),
								'paymentamount'=>$invoice_details->registration_fee,
								'feeType'=>'registration_fee',
								'yearsOrSemester'=>$students_signle->yearsOrSemester,
								'uname'=>$this->session->userdata('username'),
								'usertype'=>$this->session->userdata('usertype'),
								'userID'=>$this->session->userdata('loginuserID'),
								'paymentdateUpdate'=>date('Y-m-d'),
							);
							$this->db->insert('payment',$patment_payment_reRegis);
						}
					}
					$patment_payment_tution = array(
						'adminID'=>$adminID,	
						'studentID'=>$students_signle->studentID,
						'invoiceID'=>$invoice_details->invoiceID,
						'paymenttype'=>"Cash",
						'paymentdate'=>date('Y-m-d'),
						'paymentamount'=>$invoice_details->tuitionFee,
						'feeType'=>'tuitionFee',
						'yearsOrSemester'=>$students_signle->yearsOrSemester,
						'uname'=>$this->session->userdata('username'),
						'usertype'=>$this->session->userdata('usertype'),
						'userID'=>$this->session->userdata('loginuserID'),
						'paymentdateUpdate'=>date('Y-m-d'),
					); 
					$this->db->insert('payment',$patment_payment_tution);
					$data_invoice= array(
						'paidamount'=>$totalamount_paid,
						'status'=>2
					);

					$this->db->where('invoiceID',$invoice_details->invoiceID);
					$this->db->update('invoice',$data_invoice);

					$data_insert = array(
						'studentID'=>$students_signle->studentID,
						'passbookID'=>$insertID
					); 
					$this->db->insert('student_x_passbook',$data_insert);
				}
			}
			if($students_signle->status==3){
				// $data = array(
				//    'modify_date'=> date("Y-m-d h:i:s"),
				//          'update_username'=>$this->session->userdata('username'),
				//             'status'=>4
				//            );
				// $this->db->where('studentID',$checked_id[$i]);
				// $this->db->update('student',$data);
			}
		}
		$data =array(
			'debit'=>$total_payment_done,
			'debit_credit_status'=>0,
			'balance'=>$balance_now_for_update,
			'teacherID'=>$id
		); 
		$this->db->where('passbookID',$insertID);
		$sql_update = $this->db->update('passbook',$data);
		if($sql_update)
		{
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			redirect(base_url("student/index"));
		}
	}
	
	function promote_student()
	{
		$checked_id = $this->input->post('checked_id');
		$loginuserID = $this->session->userdata("loginuserID");
		$adminID = $this->session->userdata("adminID");
		$usertype = $this->session->userdata("usertype");
		$yos_filter = $this->input->post('yos_filter');
		if($usertype=='ClgAdmin' || $usertype=='Admin')
		{
			for ($i=0; $i <count($checked_id) ; $i++) 
			{
					
				$data = array(
								'yearsOrSemester' => $yos_filter[$i] 
							);
				$this->db->where('studentID',$checked_id[$i]);
			    $this->db->update('student',$data);

					$row = $this->db->select('*')->from('invoice')->where('status_position',1)->where('studentID',$checked_id[$i])->get()->row();

					$Check_row = $this->db->select('*')->from('invoice')->where('yearsOrSemester',$yos_filter[$i])->where('studentID',$checked_id[$i])->get()->row();

						$outstandingfee = $row->amount - $row->paidamount;
						$datas = array(
									'classesID' => $row->classesID,
									'adminID' => $row->adminID,
									'studentID' => $checked_id[$i],
									'student' => $row->student,
									'roll' => $row->roll,
									'feetype' => $row->feetype,
									'amount' => $outstandingfee+$row->tuitionFee+$row->examinationFee,
									'paidamount' => '0',
									'userID' => $row->userID,
									'usertype' => $usertype,
									'uname' => $row->uname,
									'status' => 3,
									'paymenttype' => $row->paymenttype,
									'date' => $row->date,
									'paiddate' => $row->paiddate,
									'year' => $row->year,
									'total_install' => $row->total_install,
									'c_install' => $row->c_install,
									'totalfeepaidperyear' => $row->tuitionFee+$row->examinationFee,
									'examinationFee' => $row->examinationFee,
									'enrollmentFee' => $row->enrollmentFee,
									'tuitionFee' => $row->tuitionFee,
									'paid_tuition_fee' => $row->paid_tuition_fee,
									'discount' => $row->discount,
									'registration_fee_status' => $row->registration_fee_status,
									'examinationFee_status' => $row->examinationFee_status,
									'tuitionFee_status' => $row->tuitionFee_status,
									'registration_fee' => 0,
									'lateral' => $row->lateral,
									'status_position' => '1',
									'yearsOrSemester' => $yos_filter[$i]
								);

							if(count($Check_row)==0){
						    $invo = $this->db->insert('invoice',$datas);
							$data_update_current_invoice = array(
											'status_position' => '0'
										);
							$this->db->where('invoiceID',$row->invoiceID);
							$this->db->update('invoice',$data_update_current_invoice);	
						}

				
			}

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index"));
			
		
			
		}
	}

	/*public function createzip(){
   
        // Read file from path
        if($this->input->post('but_createzip1') != NULL){

            // File path
            $filepath1 = FCPATH.'/uploads/image1.jpg';
            $filepath2 = FCPATH.'/uploads/document/users.csv';
           
            // Add file
            $this->zip->read_file($filepath1);
            $this->zip->read_file($filepath2);

            // Download
            $filename = "backup.zip";
            $this->zip->download($filename);

        }

        // Read files from directory
        if($this->input->post('but_createzip2') != NULL){

            // File name
            $filename = "backup.zip";

            // Directory path (uploads directory stored in project root)
            $path = 'uploads';

            // Add directory to zip
            $this->zip->read_dir($path);

            // Save the zip file to archivefiles directory
            $this->zip->archive(FCPATH.'/archivefiles/'.$filename);

            // Download
            $this->zip->download($filename);

        }
        
        // Load view
        $this->load->view('index_view');
    }*/
// ajax function
function ActiveStudent(){

	$this->session->unset_userdata('TrashStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->unset_userdata('payment_status');
	$this->session->unset_userdata('recentStudent');
	$this->session->set_userdata('ActiveStudent',1);

}
function DraftStudent(){
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('TrashStudent');
	$this->session->set_userdata('DraftStudent',4);
	$this->session->unset_userdata('recentStudent');

}
function TrashStudent(){
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->set_userdata('TrashStudent',2);
	$this->session->unset_userdata('recentStudent');
}
function recentStudent(){
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->set_userdata('recentStudent',3);
	$this->session->unset_userdata('TrashStudent');
}
function AllStudent(){
	$this->session->unset_userdata('sessionFilter');
	$this->session->unset_userdata('classesID');
	$this->session->unset_userdata('subCourseID');
	$this->session->unset_userdata('sessionType');
	$this->session->unset_userdata('examType');
	$this->session->unset_userdata('education_mode');
	$this->session->unset_userdata('ActiveStudent');
	$this->session->unset_userdata('DraftStudent');
	$this->session->unset_userdata('TrashStudent');
	$this->session->set_userdata('AllStudent',5);
	$this->session->unset_userdata('recentStudent');
	$this->session->unset_userdata('payment_status');
}


function deleteAfterUSe(){

	$this->load->view('student/testing',$this->data);

}


function set_session_of_subCourse(){
	
}
	function active() {
		$usertype = $this->session->userdata("usertype");
		if($usertype == "Admin" || $usertype == 'ClgAdmin') {
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			if($id != '' && $status != '') {
				if((int)$id) {
					if($status == 'chacked') {
						$this->student_m->update_student(array('studentactive' => 1), $id);
						echo 'Success';
					} elseif($status == 'unchacked') {
						$this->student_m->update_student(array('studentactive' => 0), $id);
						echo 'Success';
					} else {
						echo "Error";
					}
				} else {
					echo "Error";
				}
			} else {
				echo "Error";
			}
		} else {
			echo "Error";
		}
	}

	function awardsheet()
	{
		$usertype = $this->session->userdata("usertype");
		$SpecialUsertype = $this->session->userdata('SpecialUsertype');
		$adminID = $this->session->userdata("adminID");
		if($usertype == "Teacher")
		{
			$loginuserID = $this->session->userdata('loginuserID');

            $this->data['classes'] = $this->classes_m->inner_join_with_fee_module($loginuserID);
		}
		else
		{
			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);
		}
		if($usertype == 'ClgAdmin' || $usertype == 'Support' || $usertype == 'Academic' || $usertype == 'Admin')
		{
			if ($this->input->post('classesID')) {
				$this->session->set_userdata('FiltersubjectID', $this->input->post('classesID'));
				$this->session->set_userdata('FilterclassesID', $this->input->post('classesID'));
			}
			
	        if ($this->input->post('SessionTo') && $this->input->post('SessionFrom')) 
			{
				$SessionTo = $this->input->post('SessionTo');
				$SessionFrom = $this->input->post('SessionFrom');
				$sessionFilter = $SessionFrom.'-'.$SessionTo;
				$this->session->set_userdata('sessionFilter', $sessionFilter);
			}

		  	if ($this->input->post('yearSemesterID')) {
				$this->session->set_userdata('FilteryearSemesterID', $this->input->post('yearSemesterID'));
			}
			if ($this->input->post('subjectID')) {
				$this->session->set_userdata('Filtersubject1ID', $this->input->post('subjectID'));
			}
		    if ($this->input->post('subjectIDfor')) {
				$this->session->set_userdata('FiltersubjectID', $this->input->post('subjectIDfor'));
			}

			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterclassesID'));
			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterclassesID'));
			$this->data["subject"] = $this->classes_m->get_single_subject($this->session->userdata('FilterclassesID'),$this->session->userdata('FilteryearSemesterID'));
		}

		$this->data["subview"] = "student/awardsheet";
		$this->load->view('_layout_main', $this->data);
	}

	function sheet()
	{
		$subjectID = $this->input->post('subjectID');
		$session = $this->input->post('session');
		$courseID = $this->input->post('courseID');
		$yearsOrSemester = $this->input->post('yearsOrSemester'); 
		$data['subjectID'] = $subjectID;
		$data['session'] = $session;
		$data['courseID'] = $courseID;
		$data['yearsOrSemester'] = $yearsOrSemester;
		$data['award'] = $this->classes_m->award($subjectID,$session,$courseID,$yearsOrSemester);
		$this->load->view('student/sheet',$data);
	}

	function ResetCoursess(){
		$this->session->unset_userdata('FilterclassesID');
		$this->session->unset_userdata('FilteryearSemesterID');   
	}
	function ResetSubcoursess(){
		$this->session->unset_userdata('FiltersubCourseID');
	}
	function ResetSemesterYears(){
		$this->session->unset_userdata('FilteryearSemesterID');	   
	}
}

/* End of file student.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/student.php */
