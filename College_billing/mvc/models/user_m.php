<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_m extends MY_Model {

	protected $_table_name = 'user';
	protected $_primary_key = 'userID';
	protected $_primary_filter = 'intval';
	protected $_order_by = "usertype";

	function __construct() {
		parent::__construct();
	}

	function get_username($table, $data=NULL) {
		$query = $this->db->get_where($table, $data);
		return $query->result();
	}

	function get_username_row($table, $data=NULL) {
		$query = $this->db->get_where($table, $data);
		return $query->row();
	}

	function get_user($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_user($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function get_single_user($array) {
		$query = parent::get_single($array);
		return $query;

	}

	function insert_user($array) {
		$error = parent::insert($array);
		return TRUE;
	}

function get_user_by_username($array){
	 

       $this->db->where($array);
     return  $this->db->get('user')->row();
} 

 function get_admin_all($adminID)
 {
 	// $a = array('usertype'=>'Admin','usertype'=>'Academic','usertype'=>'Support');
// print_r($a);die();
	$this->db->where('status',1);

	$this->db->where('adminID',$adminID);
	$this->db->where('usertype','Admin');
	$this->db->or_where('usertype','Academic');
	$this->db->or_where('usertype','Support');
	// $this->db->where($a);



	 return $this->db->get('user')->result();
	  // echo "<pre>";
	  // print_r($b);die();


 }

 function get_accountant_all($adminID){

	$this->db->where('status',1);

	$this->db->where('adminID',$adminID);
	$this->db->where( 'usertype','Accountant');

	  return $this->db->get('user')->result();

 }

  function get_super_ap_all($adminID){

	$this->db->where('status',1);

	$this->db->where('adminID',$adminID);
	$this->db->where( 'usertype','Super_A_P');

	  return $this->db->get('user')->result();

 }

 function get_academic_all($adminID){

	$this->db->where('status',1);

	$this->db->where('adminID',$adminID);
	$this->db->where( 'usertype','Academic');

	  return $this->db->get('user')->result();

 }

function get_single_user_Profile($array){
	 $this->db->select('*');
   $this->db->where('userID',$array);


 return  $this->db->get('user')->row();
} 
	function update_user($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	function delete_user($id){
		parent::delete($id);
	}

	function hash($string) {
		return parent::hash($string);
	}	

	function total_rm(){
		$this->db->where('usertype',"Accountant");
		return $this->db->get('user')->result();
	}
	function clgheaderupdate($saveheader,$getclgadminid)
	{
		$this->db->where('adminID',$getclgadminid);
		$this->db->update('clgheadersetting',$saveheader);
		return true;
	}
	function clgheadersave($saveheader)
	{
		$this->db->insert('clgheadersetting',$saveheader);
		return $this->db->insert_id();
	}
	function clgheadershow($getclgadminid)
	{
		$this->db->select('*');
		$this->db->where('adminID',$getclgadminid);
		return $this->db->get('clgheadersetting')->row();
	}
}

/* End of file user_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/user_m.php */