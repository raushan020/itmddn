<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Lms_m extends MY_Model {



	protected $_table_name = 'subject';

	protected $_primary_key = 'subjectID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "classesID asc";



	function __construct() {

		parent::__construct();

	}

	function get_join_simple_pdflms($adminID,$subjectID) {


		$this->db->select('*');

		$this->db->from('pdf_lms');

		$this->db->where('subjectID', $subjectID);

		$query = $this->db->get();

		return $query->result();

	}

	function get_join_simple_videos($adminID,$subjectID) {

		$this->db->select('*');

		$this->db->from('videos');

		$this->db->where('subjectID', $subjectID);

		$query = $this->db->get();

		return $query->result();

	}


	function get_report_url($studentID,$subjectID,$refer_id){
		$this->db->where('studentID',$studentID);
		$this->db->where('type','videos');
		$this->db->where('subjectID',$subjectID);
		$this->db->where('refer_id',$refer_id);
	     return 	$this->db->get('lms_progress_report')->row();
	}

	function get_report_url_epub($studentID,$subjectID,$refer_id){
		$this->db->where('studentID',$studentID);
		$this->db->where('type','epub');
		$this->db->where('subjectID',$subjectID);
		$this->db->where('refer_id',$refer_id);
	     return 	$this->db->get('lms_progress_report')->row();
	}

	function get_join_single_videos($subjectID,$id_refer) {

		$this->db->select('*');
		$this->db->from('videos');
		$this->db->where('subjectID', $subjectID);
		$this->db->where('videoID', $id_refer);
		$query = $this->db->get();
		return $query->row();

	}

	function get_join_simple_epub($adminID,$subjectID) {

		$this->db->select('*');

		$this->db->from('epub');

		$this->db->where('subjectID', $subjectID);
		

		$query = $this->db->get();

		return $query->result();

	}

	function get_join_simple_assignment($adminID,$subjectID) {

		$this->db->select('*');

		$this->db->from('ets_quiz');

		$this->db->where('subjectID', $subjectID);
		$this->db->where('extype', 2);

		$query = $this->db->get();

		return $query->result();

	}

	function update_lms_report_videos($subjectID,$uri4){
		$data = array(
			'status' => '1'
		);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('refer_id',$uri4);
		$this->db->where('type','videos');
		$this->db->update('lms_progress_report',$data);
	}

	function update_lms_report_epub($subjectID,$uri4){
		$data = array(
			'status' => '1'
		);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('refer_id',$uri4);
		$this->db->where('type','epub');
		$this->db->update('lms_progress_report',$data);
	}

	function get_lms_report_assignment($subjectID,$uri4){
		
		$this->db->where('subjectID',$subjectID);
		$this->db->where('extype',2);
		return $this->db->get('ets_quiz')->row();
	}

	function get_lms_report_videos($subjectID,$uri4){
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','videos');
		$this->db->where('status',0);
		return $this->db->get('lms_progress_report',1)->row();

	}

	function get_lms_report_epub($subjectID,$uri4){
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','epub');
		$this->db->where('status',0);
		return $this->db->get('lms_progress_report',1)->row();

	}


	function get_lms_report_assignment_valid_epub($subjectID,$uri4){
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','epub');
		$this->db->where('status',1);
		return $this->db->get('lms_progress_report')->result();

	}


	function get_lms_report_assignment_valid_Video($subjectID,$uri4){
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','videos');
		$this->db->where('status',1);
		return $this->db->get('lms_progress_report')->result();

	}


	function get_progress_bar_fill_by_percent_epub($subjectID,$studentID){
		
		$this->db->where('studentID',$studentID);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','epub');		
		$epubpercentagewithout = $this->db->get('lms_progress_report')->num_rows();


		$this->db->where('studentID',$studentID);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','epub');	
		$this->db->where('status',1);
		$epubpercentagewith = $this->db->get('lms_progress_report')->num_rows();
		
	
		
		if ($epubpercentagewith>0) {
			$exactPercentforepub = 30/$epubpercentagewithout;
			return $exactPercentforepub * $epubpercentagewith;
		}else{
			return 0;
		}
	}

	function get_progress_bar_fill_by_percent_videos($subjectID,$studentID){
		$this->db->where('studentID',$studentID);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','videos');
		$videospercentagewithout = $this->db->get('lms_progress_report')->num_rows();

		$this->db->where('studentID',$studentID);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','videos');
		$this->db->where('status',1);
		$videospercentagewith = $this->db->get('lms_progress_report')->num_rows();
		
		
		if ($videospercentagewith>0) {
			$exactPercentforvideo = 30/$videospercentagewithout;
			return $videospercentagewith * $exactPercentforvideo;
		}else{
			return 0;
		}
	}

	function get_progress_bar_fill_by_percent_assignment($subjectID,$studentID){
		$this->db->where('studentID',$studentID);
		$this->db->where('subjectID',$subjectID);
		$this->db->where('type','assignment');
		$this->db->where('status',1);
		$assignmentpercentageKanumRows = $this->db->get('lms_progress_report')->row();	
if($assignmentpercentageKanumRows){
		$this->db->where('rid',$assignmentpercentageKanumRows->rid);
					$getAssignmentArray = $this->db->get('ets_result')->row();
		if ($getAssignmentArray) {
						$this->db->where('quid',$getAssignmentArray->quid);
						$noq  =  $this->db->get('ets_quiz')->row()->noq;
						
						$total_right_ques = $getAssignmentArray->score_obtained;
						if($total_right_ques){

					return	$assignment_per = 40*$total_right_ques/$noq;
					
					}else{
					return	$assignment_per = 0;
					}
				}else{
					return $assignment_per = 0;
					}
				}else{
					return $assignment_per = 0;
				}



		
	}


	function get_recent_subject_info($studentID,$subjectID){
		
		$this->db->where ('subjectID',$subjectID);
		$this->db->where ('studentID',$studentID);
		$this->db->where('status',1);
		$this->db->get('lms_progress_report',3);
		

	}


	function get_join_subject_count($adminID) {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');


		if ($this->session->userdata('FilterSubjectclassesID')) {
		 $this->db->where('subject.classesID', $this->session->userdata('FilterSubjectclassesID'));
		}

		if ($this->session->userdata('FilterSubjectsubCourseID')) {
		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));
		}

		if ($this->session->userdata('FilterSubjectyearSemesterID')) {
		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));
		}

		$this->db->where('subject.adminID', $adminID);

		$query = $this->db->get();

		return $query->num_rows();

	}


	function get_join_subject() {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('*');
		$this->db->from('subject');
		$this->db->join('classes', 'classes.ClassesID = subject.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = subject.sub_coursesID', 'LEFT');


		if ($this->session->userdata('FilterSubjectclassesID')) {
		 $this->db->where('subject.classesID', $this->session->userdata('FilterSubjectclassesID'));
		}

		if ($this->session->userdata('FilterSubjectsubCourseID')) {
		 $this->db->where('subject.sub_coursesID', $this->session->userdata('FilterSubjectsubCourseID'));
		}

		if ($this->session->userdata('FilterSubjectyearSemesterID')) {
		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterSubjectyearSemesterID'));
		}

		$this->db->where('subject.adminID', $adminID);


if(isset($_POST["search"]["value"]))  
           {  
 $searches  =  $_POST["search"]["value"];

		$order_column = array( 

                            0 =>'sn', 
                            1 =>'subject_code',
                            2 =>'subject',
                            3=> 'classes',
                            4=> 'sub_course',
                            5=> 'yearsOrSemester',
                            6=>'action'
                        );
           $where = "(subject.subject_code LIKE '%$searches%' OR subject.subject LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR subject.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('subject.subjectID', 'DESC');  
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       }

	}

function make_datatables($adminID){  
           $this->get_join_subject();  
           $query = $this->db->get();  
           return $query->result();  
      } 


    function getPaginationrow($classesID,$sub_coursesID) {

		$this->db->select('*');

		$this->db->from('subject');

	if ($this->session->userdata('FilterLmsyearSemesterID')) {
		 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterLmsyearSemesterID'));
		}

		$this->db->where("subject.classesID", $classesID);

		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$query = $this->db->get();

		return $query->num_rows();
		

	}

	function getPaginationrowvideo() {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->join('videos', 'videos.subjectID = subject.subjectID', 'RIGHT');
		$query = $this->db->get();
		return $query->num_rows();

		

	}


	function get_join_where_subject_video($limit, $start) {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->join('videos', 'videos.subjectID = subject.subjectID', 'RIGHT');

	
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		$setarray = $query->result_array();

		foreach ($setarray as $key => $value) {
			
			$this->db->where('subjectID',$value['subjectID']);
			$query = $this->db->get('videos');
			$setarray[$key]['videoKey'] = $query->result_array();
		}
		
		return $setarray;
		

	}



	function get_join_where_subject($loginuserID,$classesID,$sub_coursesID,$limitDefine) {

		$this->db->select('*');

		$this->db->from('subject');
		

	if($limitDefine==1)
	{

		if ($this->session->userdata('FilterlmsyearSemesterID')) {
			 $this->db->where('subject.yearsOrSemester', $this->session->userdata('FilterlmsyearSemesterID'));
		}
	}

		$this->db->where("subject.classesID", $classesID);
		

		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		if($limitDefine==3){
			$this->db->limit(3);
		}
		$query = $this->db->get();
		$setarray = $query->result_array();
		
		foreach ($setarray as $key => $value) {
			$this->db->where('subjectID',$value['subjectID']);
			$this->db->where('studentID',$loginuserID);
			$query = $this->db->get('lms_progress_report');
			$alternate = $query->result_array();
if(count($alternate)>0){
					$count_videos_total = 0;
				$count_videos_total_read = 0;
				$count_epub_total = 0;
				$count_epub_total_read = 0;
				$assignment_per = 0;
			foreach ($alternate as $key2 => $value2) {

				if($value2['type'] == 'videos'){
					$count_videos_total+=1;
				  	if ($value2['status']==1) {
				  	 $count_videos_total_read+=1;
				  	}
				}elseif($value2['type'] == 'epub'){
				     $count_epub_total+=1;
				  	if ($value2['status']==1) {
				  	 $count_epub_total_read+=1;
				  	}
				}elseif($value2['type'] == 'assignment'){
					 
					$this->db->where('rid',$value2['rid']);
					$getAssignmentArray = $this->db->get('ets_result')->row();
					
					if ($getAssignmentArray) {
						$this->db->where('quid',$getAssignmentArray->quid);
						$noq  =  $this->db->get('ets_quiz')->row()->noq;
						
						$total_right_ques = $getAssignmentArray->score_obtained;
						if($total_right_ques){
						$assignment_per = 40*$total_right_ques/$noq;
						
					}else{
						$assignment_per = 0;
					}
				}else{
					$assignment_per = 0;
					}
				}
			}

		if($count_videos_total==0){
		$videos_per = 0;
		}else
		{
			$videos_per =  $count_videos_total_read*30/$count_videos_total;
		}

			if($count_epub_total==0){
		$epub_per=0;
		}else{
		$epub_per =  $count_epub_total_read*30/$count_epub_total;	
		}

		$total_per = $videos_per+$epub_per+$assignment_per;
		$setarray[$key]['percentage'] =	$total_per;

		}else{
		$setarray[$key]['percentage'] =	0;	
		}
// $epub_per =  $count_epub_total_read*40/$count_epub_total;
		}



	return $setarray;

		
		// $setarray[$key]['percentage']
		
		

	}

function get_join_where_assinment($loginuserID){

		$this->db->select('*');

		$this->db->from('ets_quiz');
		$this->db->join('ets_result', 'ets_quiz.quid = ets_result.quid', 'inner');
		$this->db->where('ets_result.uid', $loginuserID);

		$exit = $this->db->get()->result();
       
       return $exit;





}

	function get_join_where_subject_count($classesID,$sub_coursesID) {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->where("subject.classesID", $classesID);

		$this->db->where("subject.sub_coursesID",$sub_coursesID);
		$query = $this->db->get();

		return $query->result();

	}





	function get_classes() {

		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');

		$query = $this->db->get();

		return $query->result();

	}



	function get_subject($array=NULL, $signal=FALSE) {

		$query = parent::get($array, $signal);

		return $query;

	}



	function get_subject_call($id) {

		$query = $this->db->get_where('subject', array('classesID' => $id));

		return $query->result();

	}



	function get_order_by_subject($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}



	function insert_subject($array) {

		$error = parent::insert($array);

		return TRUE;

	}



	function update_subject($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}
 	function GetSubjectByParam($array)
	{

		$this->db->where($array);
		$query  = $this->db->get('subject');

		return $query->result();

	}

	public function delete_subject($id){

		parent::delete($id);

	}



}



/* End of file subject_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/subject_m.php */