<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class signin_m extends MY_Model {



	function __construct() {

		parent::__construct();

		$this->load->model("setting_m");

	}

	public function signin() {
		$tables = array('student' => 'student', 'teacher' => 'teacher', 'user' => 'user', 'systemadmin' => 'systemadmin','admin'=>'admin','professor'=>'professor' );
	// print_r($tables);
	// exit();
		$array = array();

		$i = 0;

		$username = $this->input->post('username');

		$password = $this->hash($this->input->post('password'));
		$userdata = '';

		foreach ($tables as $table) {

			$user = $this->db->get_where($table, array("username" => $username, "password" => $password,'status'=>1));

			$alluserdata = $user->row();
			if(count($alluserdata)) {

				$userdata = $alluserdata;

				$array['permition'][$i] = 'yes';

			} else {

				$array['permition'][$i] = 'no';

			}

			$i++;

		}




		if(in_array('yes', $array['permition'])) {

		if($userdata->usertype == "superadmin") {
		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->adminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				return TRUE;

			}
			elseif($userdata->usertype == "ClgAdmin") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->adminID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);
			   $sessionData = array(
			    	'session_id' =>$this->session->userdata('session_id'),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'adminID'=>$userdata->adminID
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;

			}
			elseif($userdata->usertype == "Student") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->studentID,

					"name" => $userdata->name,

					"examType" => $userdata->examType,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);
				$this->db->where('classesID',$userdata->classesID);
				$this->db->where('status',1);
			    $classCount =	$this->db->get('classes')->row();
			    if (count($classCount)>0) {
			     if($classCount->IsSubCourse==1){

			    $this->db->where('sub_coursesID',$userdata->sub_coursesID);
				$this->db->where('status',1);
			    $subCourseCount =	$this->db->get('sub_courses')->num_rows();	
			    if ($subCourseCount>0) {
			    $this->session->set_userdata($data);
			    $sessionData = array(
			    	'session_id' =>$this->session->userdata('session_id'),
			    	'studentID'=>$userdata->studentID,
			    	'classesID'=>$userdata->classesID,
			    	'sub_coursesID'=>$userdata->sub_coursesID,
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;
			    }
			    }else{
			    $this->session->set_userdata($data);
			     $sessionData = array(
			    	'session_id' =>$this->session->userdata('session_id'),
			    	'studentID'=>$userdata->studentID,
			    	'classesID'=>$userdata->classesID,
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    );   
			    $this->db->insert('school_sessions',$sessionData);
				return TRUE;
			    }
			}

			} elseif($userdata->usertype == "Teacher" ) {
				
		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));
		$lang = $settings->language;

				$data = array(

					"loginuserID" => $userdata->teacherID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,
					"SpecialUsertype" => $userdata->specialAcademicPartner,
					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>$this->session->userdata('session_id'),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	'teacherID'=>$userdata->teacherID
			    );   
			    $this->db->insert('school_sessions',$sessionData);


				return TRUE;

			} elseif($userdata->usertype == "Parent") {

				$data = array(

					"loginuserID" => $userdata->parentID,

					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				return TRUE;

			} elseif($userdata->usertype == "Admin" || $userdata->usertype == "Support" || $userdata->usertype == "Accountant" || $userdata->usertype == "Academic" || $userdata->usertype == "Super_A_P" ) {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));

		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->userID,
					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					"photo" => $userdata->photo,

					"lang" => $lang,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>$this->session->userdata('session_id'),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->userID
			    );   
			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			}elseif($userdata->usertype == "Professor") {

		$settings = $this->setting_m->get_setting(array('adminID'=>$userdata->adminID));

		$lang = $settings->language;
				$data = array(

					"loginuserID" => $userdata->professorID,
					"name" => $userdata->name,

					"adminID" => $userdata->adminID,

					"email" => $userdata->email,

					"usertype" => $userdata->usertype,

					"username" => $userdata->username,

					 "lang" => $lang,

					 "photo" => $userdata->photo,

					"loggedin" => TRUE

				);

				$this->session->set_userdata($data);

				$sessionData = array(
			    	'session_id' =>$this->session->userdata('session_id'),
			    	'ip_address'=>$this->input->ip_address(),
			    	'last_activity'=>time(),
			    	'user_agent' => $this->input->user_agent(),
			    	 'userID'=> $userdata->professorID
			    );   
			    $this->db->insert('school_sessions',$sessionData);

				return TRUE;

			}else {

 				return FALSE;

			}

		} else {

			return FALSE;

		}

	}


	function checkin(){
		$this->db->where('session_id',$this->session->userdata('session_id'));
		$this->db->where('current_login',1);
		$row  = $this->db->get('school_sessions')->num_rows();
		if ($row==0) {
			$this->signout();
		}
		}


function logoutOtherDevice(){
	$usertype = $this->session->userdata("usertype");
	$loginuserID = $this->session->userdata("loginuserID");

			  $data = array(
                'current_login'=>0
               );
			if ($usertype=='Student') {
				$this->db->where('studentID',$loginuserID);
			}elseif ($usertype=='ClgAdmin') {
			$this->db->where('adminID',$loginuserID);
			}elseif ($usertype=='Teacher') {
			  $this->db->where('teacherID',$loginuserID);
			}
			$this->db->update('school_sessions',$data);

}

	function change_password() {

		$table = strtolower($this->session->userdata("usertype"));

		if($table == "admin") {

			$table = "user";

		}

		if($table == "accountant") {

			$table = "user";

		}

		if($table == "clgadmin") {

			$table = "admin";

		}

		if($table == "librarian") {

			$table = "user";

		}

		$username = $this->session->userdata("username");

		$old_password = $this->hash($this->input->post('old_password'));

		$new_password = $this->hash($this->input->post('new_password'));

		$user = $this->db->get_where($table, array("username" => $username, "password" => $old_password));

		$alluserdata = $user->row();

		if(count($alluserdata)) {

			if($alluserdata->password == $old_password){

				$array = array(

					"password" => $new_password

				);

				$this->db->where(array("username" => $username, "password" => $old_password));

				$this->db->update($table, $array);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				return TRUE;

			}

		} else {

			return FALSE;

		}

	}

	public function signout() {

		$this->session->sess_destroy();

	}

	public function loggedin_exam() {

		return (bool) $this->session->userdata("loggedin_exam");

	}

	public function loggedin() {

		return (bool) $this->session->userdata("loggedin");

	}

}

/* End of file signin_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/signin_m.php */