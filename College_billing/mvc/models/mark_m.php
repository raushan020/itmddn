<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Mark_m extends MY_Model {



	protected $_table_name = 'mark';

	protected $_primary_key = 'markID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "subject asc";



	function __construct() {

		parent::__construct();

	}



	function get_mark($examID) {
		$this->db->select('exam.total_credits, exam.credit_earned, exam.sgpa, mark.markID, mark.subject, mark.Grade, subject.subject_code, subject.subject_credit');
		$this->db->from('exam');
		$this->db->join('mark','exam.examID = mark.examID','LEFT');
		$this->db->join('subject','mark.subjectID = subject.subjectID','LEFT');
		$where = "exam.examID = '".$examID."'";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();

	}

	function get_mark_practical($examID) {

		  $this->db->join('subject','subject.subjectID=mark.subjectID','inner');
		  $this->db->where('mark.examID',$examID);
		  $this->db->where('mark.type','p');
          $query = $this->db->get('mark');
		  return $query->result();

	}



	function get_order_by_mark($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}



	function insert_mark($array) {

		$error = parent::insert($array);

		return TRUE;

	}



	function update_mark($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}



	function update_mark_classes($array, $id) {

		$this->db->update($this->_table_name, $array, $id);

		return $id;

	}



	public function delete_mark($id){

		parent::delete($id);

	}



	function sum_student_subject_mark($studentID, $classesID, $subjectID) {

		$array = array(

			"studentID" => $studentID,

			"classesID" => $classesID,

			"subjectID" => $subjectID

		);

		$this->db->select_sum('mark');

		$this->db->where($array);

		$query = $this->db->get('mark');

		return $query->row();

	}



	function count_subject_mark($studentID, $classesID, $subjectID) {

		$query = "SELECT COUNT(*) as 'total_semester' FROM mark WHERE studentID = $studentID && classesID = $classesID && subjectID = $subjectID && (mark != '' || mark <= 0 || mark >0)";

	    $query = $this->db->query($query);

	    $result = $query->row();

	    return $result;

	}


	function get_join_result_count($adminID) {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('*');
		$this->db->from('exam');
		$this->db->join('student', 'exam.studentID = student.studentID', 'inner');
		$this->db->join('classes', 'classes.classesID = student.classesID', 'inner');
		if ($this->session->userdata('FilterMarkclassesID')) {
		 $this->db->where('student.classesID', $this->session->userdata('FilterMarkclassesID'));
		}
		if ($this->session->userdata('FilterMarkyearSemesterID')) {
		 $this->db->where('student.yearsOrSemester', $this->session->userdata('FilterMarkyearSemesterID'));
		}
		if(isset($_POST["search"]["value"]))  
        {  
 			$searches  =  $_POST["search"]["value"];

           $where = "(student.name LIKE '%$searches%' OR classes.classes LIKE '%$searches%')";
           $this->db->where($where);
           } 

		
		$query = $this->db->get();

		return $query->num_rows();

	}


	function get_join_result() {
		$adminID = $this->session->userdata("adminID");
		$this->db->select('student.name,student.studentID,classes.classes,classes.classesID,exam.yearsOrSemester,exam.examID');
		$this->db->from('exam');
		$this->db->join('student', 'exam.studentID = student.studentID', 'inner');
		$this->db->join('classes', 'classes.classesID = student.classesID', 'inner');
		if ($this->session->userdata('FilterMarkclassesID')) {
		 $this->db->where('student.classesID', $this->session->userdata('FilterMarkclassesID'));
		}
		if ($this->session->userdata('FilterMarkyearSemesterID')) {
		 $this->db->where('student.yearsOrSemester', $this->session->userdata('FilterMarkyearSemesterID'));
		}
		if(isset($_POST["search"]["value"]))  
        {  
 			$searches  =  $_POST["search"]["value"];

			$order_column = array( 
                            0 =>'sn', 
                            1 =>'name',
                            2 =>'classes',
                            3 =>'yearsOrSemester',
                            4 =>'action'
                        );
           $where = "(student.name LIKE '%$searches%' OR classes.classes LIKE '%$searches%')";
           $this->db->where($where);
           } 

           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
           		$this->db->order_by('exam.examID', 'DESC'); 
               
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 

            // $this->db->group_by('ets_result.uid', 'DESC');  
       }

	}

	function make_datatables($adminID){  
		$this->get_join_result();             
           $query = $this->db->get();  
           return $query->result();  
      } 


	function get_join_mark($adminID) {		

		// $this->db->join('subject', 'subject.subjectID = mark.subjectID', 'INNER');

		$this->db->select('*');
		// $this->db->where();

		$this->db->from('ets_result');

	    // $this->db->join('ets_quiz', 'ets_quiz.quid = ets_result.quid', 'LEFT');

	 //    $this->db->join('ets_quiz', 'ets_quiz.classesID = ets_result.classesID', 'LEFT');

		// $this->db->join('ets_quiz', 'ets_quiz.sub_coursesID = sub_courses.sub_coursesID', 'LEFT');

		

		$query = $this->db->get();

	    $result = $query->row();

	    return $result;

	}	



	function get_order_by_mark_with_subject($classes,$year) {

		$this->db->select('*');

		$this->db->from('subject');

		$this->db->join('mark', 'subject.subjectID = mark.subjectID', 'LEFT');

		$this->db->join('exam', 'exam.examID = mark.examID');

		$this->db->where('mark.classesID', $classes);

		$this->db->where('mark.year', $year);

		$query = $this->db->get();

		return $query->result();

	}



	function get_order_by_mark_with_highest_mark($classID,$studentID) {

		$this->db->select('M.markID,M.examID, M.exam, M.subjectID, M.subject, M.studentID, M.classesID,  M.mark, M.year, (

		SELECT Max( mark.mark )

		FROM mark

		WHERE mark.subjectID = M.subjectID

		AND mark.examID = M.examID

		) highestmark');

		$this->db->from('exam E');

		$this->db->join('mark M', 'M.examID = E.examID', 'LEFT');

		$this->db->join('subject S', 'M.subjectID = S.subjectID');

		$this->db->where('M.classesID', $classID);

		$this->db->where('M.studentID', $studentID);

		$query = $this->db->get();

		return $query->result();

	}

}



/* End of file mark_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/mark_m.php */

