			<div id="page-wrapper">
				<div class="row page-titles">
					<div class="col-md-5 align-self-center">
						<h3 class="text-themecolor">404</h3>
					</div>
					<div class="col-md-7 align-self-center">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url() ?>dashboard/index">Home</a></li>
							<li class="breadcrumb-item"><a href="javascript:void(0)">Advance</a></li>
							<li class="breadcrumb-item active">404</li>
						</ol>
					</div>
				</div>
				<div class="container-fluid">
					<!-- /row -->
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="widget default-widget">
								<div class="error-content">
									<h1 class="four-four-error">4<span>0</span>4</h1>
								    <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>

								    <p>
									We will work on fixing that right away.
									Meanwhile, you may <a href="<?php echo base_url() ?>">return to dashboard</a> or try using the search form.
								    </p>
								    <h3 style="font-size:22px;">Please use the feedback button to send us a screenshot to help us in fixing the error faster.</h3>

								</div>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>	
			</div>