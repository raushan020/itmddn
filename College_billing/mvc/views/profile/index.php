    <style type="text/css">

        td, th{

        padding: 10px;

    }

    td, th .stable_img img {

   

    border: 0px solid #ddd;

   

}

/*td{ font-weight: bold;

    }*/





    </style>

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="fa fa-user"></i> Profile</h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>



                <li class="active">Profile</li>

            </ol>

        </div>

    </div>

   <!-- /.box-header -->

   <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">



<?php



    $usertype = $this->session->userdata('usertype');



    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" ) {



?>



    <section class="panel">



        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$admin->photo))?>



            </a>



            <h1><?=$siteinfos->sname ?></h1>



            <!-- <p><?=$this->lang->line($this->lang->line($classes->name))?></p> -->



        </div>



        <div class="panel-body profile-view-dis">



           <center> <b><h3 style="font-size: 20px;"><?=$this->lang->line("personal_information")?></h3></b></center>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$siteinfos->email?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$siteinfos->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$siteinfos->address?></p>



                </div>



            </div>







        </div>



    </section>







<?php }elseif($usertype == "Professor"){ ?>
    

    <section class="panel">



        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$professor->photo))?>



            </a>







            <h1><?=$professor->name?></h1>



            <!--  <p><?=$professor->department_name?></p>  -->



        </div>



        <div class="panel-body profile-view-dis">



            <h1><?=$this->lang->line("personal_information")?></h1>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($professor->dob))?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($professor->jod))?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$professor->sex?></p>



                </div>



                <!-- <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$professor->religion?></p>



                </div> -->



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$professor->email?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$professor->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$professor->address?></p>



                </div>



            </div>



        </div>



    </section>
<?php }elseif($usertype == "Super_A_P"){ ?>
    

    <section class="panel">



        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$Profile->photo))?>



            </a>







            <h1><?=$Profile->name?></h1>



            <!--  <p><?=$professor->department_name?></p>  -->



        </div>



        <div class="panel-body profile-view-dis">



            <h1><?=$this->lang->line("personal_information")?></h1>



            <div class="row">



                <div class="profile-view-tab">



                     



                </div>



                <div class="profile-view-tab">



                    


                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$Profile->sex?></p>



                </div>



                <!-- <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$professor->religion?></p>



                </div> -->



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$Profile->email?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$Profile->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span>User Type</span>:  <?php if($Profile->usertype == 'Super_A_P'){
                       echo 'Super Academic Partner';
                    } ?></p>
                



                


                </div>



            </div>



        </div>



    </section>

<?php } elseif($usertype == "Librarian" || $usertype == "Accountant" || $usertype == "Support" || $usertype == "Academic" ) { ?>



    <section class="panel">



        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$user->photo))?>



            </a>







            <h1><?=$user->name?></h1>



            <p><?=$this->lang->line($user->usertype)?></p>





        </div>







        <div class="panel-body profile-view-dis">



            <h3 class="border_heading"><?=$this->lang->line("personal_information")?></h3>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($user->dob));?></p>



                </div>



                <div class="profile-view-tab">



                   <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($user->jod))?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$user->sex?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$user->religion?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$user->email?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$user->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$user->address?></p>



                </div>



            </div>



        </div>



    </section>



<?php } elseif($usertype == "Teacher") { ?>



    <section class="panel">



        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$teacher->photo))?>



            </a>







            <h1><?=$teacher->name?></h1>



            <p><?=$teacher->designation?></p>



        </div>



        <div class="panel-body profile-view-dis">



            <h1><?=$this->lang->line("personal_information")?></h1>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_dob")?> </span>: <?=date("d M Y", strtotime($teacher->dob))?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_jod")?> </span>: <?=date("d M Y", strtotime($teacher->jod))?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_sex")?> </span>: <?=$teacher->sex?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_religion")?> </span>: <?=$teacher->religion?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_email")?> </span>: <?=$teacher->email?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$teacher->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$teacher->address?></p>



                </div>



            </div>



        </div>



    </section>



<?php } elseif($usertype == "Student") { ?>



    <section class="panel">

    

    </section>















    <div id="printablediv">

    </div>

    <section class="panel">







        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$student->photo))?>



            </a>







            <h1><?=$student->name?> (<?php echo str_replace('_', ' ', $student->yearsOrSemester) ?>)</h1>



            <div><?=$class->classes?><?php

                        

                         ?></div>

        

        </div>



        <div class="panel-body profile-view-dis">



            <h3 class="border_heading"><?=$this->lang->line("personal_information")?></h3>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span>Student Id </span>: <?=$student->username?></p>



                </div>



                <div class="profile-view-tab">



                    

                <p><span><?=$this->lang->line("student_roll")?> </span>: <?=$student->roll?></p>

                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("student_dob")?> </span>: <?=date("d M Y", strtotime($student->dob))?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("student_sex")?> </span>: <?=$student->sex?></p>



                </div>





                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("student_email")?> </span>: <?=$student->email?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("student_phone")?> </span>: <?=$student->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("student_address")?> </span>: <?=$student->address?></p>



                </div>



                <!--<div class="profile-view-tab">-->



                <!--    <p><span>Street:</span>:<?=$student->street?> </p>-->



                <!--</div>-->

                <div class="profile-view-tab">



                    <p><span>Pin:</span>:<?=$student->pin ?> </p>



                </div>





                <!-- <div class="profile-view-tab">



                    <p><span>Employment Status</span>: <?=$student->employment_status?></p>



                </div> -->



               <div class="profile-view-tab">



                    <p><span>Nationality</span>: <?=$student->nationality?></p>



                </div>



                 <div class="profile-view-tab">



                    <p><span>Aadhar</span>: <?=$student->aadhar?></p>



                </div>







                <?php if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support"  ) { ?>



                <div class="profile-view-tab">



                    <p><span>Username</span>: <?=$student->username?></p>



                </div>



                <?php } ?>



            </div>

            <h3 class="border_heading"><?=$this->lang->line("parents_information")?></h3>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("father_name")?> </span>: <?=$student->father_name?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("mother_name")?> </span>: <?=$student->mother_name?></p>



                </div>

            </div>

            <h3 class="border_heading">Course Details</h3>



<div class="row">



    <div class="profile1">

<table>

  <tr>

    <td class="bold1">Course</td>

    <td>:</td>

    <td><?=$class->classes?></td>

  </tr>

  <tr>

    <td class="bold1">Mode</td>

    <td>:</td>

    <td><?php if($student->education_mode==1){

                            echo "Year";

                        }else{

                            echo "Semester";

                        }

                         ?></td>

  </tr>
  <tr>

    <td class="bold1">Session Type</td>

    <td>:</td>

    <td> <?=$student->sessionType?></td>

  </tr>

  

  

  <tr>

    

</table>

</div>



<div class="profile1">

<table>



  <tr>

    <td class="bold1">Semester/Year</td>

    <td>:</td>

    <td><?= str_replace('_', ' ', $student->yearsOrSemester) ?></td>

  </tr>

  <tr>

    <td class="bold1">Session</td>

    <td>:</td>

    <td><?php 

                       echo $student->session;



                     ?></td>

  </tr>

 

  <tr>

    

</table>

</div>

</div>

             <h3 class="border_heading">Academic Details</h3>



            <div class="row">

                

            <table class="table table-striped table-academic">

                <thead>

                    <tr>

                        <th>Education</th>

                        <th>Year</th>

                        <th>Subject</th>

                        <th>Board</th>

                        <th>Percentage</th>

                        <th>Marksheet</th>

                        <th>Certificate</th>

                    </tr>

                </thead>

                <tbody>

                    <?php foreach ($education as $key => $value) {

                      ?> 

                    <tr>

                        <td><?php echo $value->education; ?></td>

                        <td><?php echo $value->year_passing; ?></td>

                        <td><?php echo $value->subject; ?></td>

                        <td><?php echo $value->board_name; ?></td>

                        <td><?php echo $value->percentage; ?> %</td>

                        <td>

                             <?php  

                             if ($value->year_passing!='') {

                              ?>   

                                <?php if ($value->marksheet_detail!='')  { ?>



                                    <strong><a href="<?php echo  base_url() ?>uploads/document/marksheet/<?php echo $value->marksheet_detail; ?>" style="color: #3c8dbc;margin-left: 15px" target="_blank">View</a></strong>

  

                                    <?php } else { ?>

                                        <a disabled>Not Available</a>

                                    <?php } ?>


                                <?php } ?>

                               

                        </td>

                        <td>

                            <?php  

                             if ($value->year_passing!='') {

                              ?>

                              <?php if ($value->certificate_detail!='')  { ?>

                             <strong><a href="<?php echo  base_url() ?>uploads/document/marksheet/<?php echo $value->certificate_detail; ?>" style="color: #3c8dbc; margin-left: 15px" target="_blank">View</a></strong>

                           

                                <?php } else{?>

                                    <a >Not Available</a>

                                <?php } ?>

                                <?php } ?>

                              </td>

                    </tr>

                    <?php } ?>

                </tbody>

            </table>

            </div>

    </section>    

    <?php } elseif($usertype == "Parent") { ?>



    <section class="panel">



        <div class="profile-view-head">



            <a href="#">



                <?=img(base_url('uploads/images/'.$parentes->photo))?>



            </a>







            <h1><?=$parentes->name?></h1>



            <p><?=$parentes->email?></p>



        </div>



        <div class="panel-body profile-view-dis">



            <h1><?=$this->lang->line("personal_information")?></h1>



            <div class="row">



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_father_name")?> </span>: <?=$parentes->father_name?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_father_profession")?> </span>: <?=$parentes->father_profession?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_mother_name")?> </span>: <?=$parentes->mother_name?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_mother_profession")?> </span>: <?=$parentes->mother_profession?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_phone")?> </span>: <?=$parentes->phone?></p>



                </div>



                <div class="profile-view-tab">



                    <p><span><?=$this->lang->line("profile_address")?> </span>: <?=$parentes->address?></p>



                </div>



            </div>



        </div>



    </section>







<?php } ?>

</div>

</div>

</div>

</div>

</div>

 <style type="text/css">

   .boredr_none .nav-tabs{

    border-bottom: 0px solid #e8edef;

   }

   .nopading{

      padding-right:0px;

      padding-left:0px;

   }

   .theme_input_blue .form-control{

   background: #2057a8;

   color:#fff;

   }

  .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */

  color: #fff;

}

.theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */

  color: #fff;

}

.theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */

  color: #fff;

}

.theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */

  color: #fff;

}

.forpostionReletive {

    position: relative;

}

.postionAbsoluter {

    position: absolute;

    

    top: 8px;

}

.action-layout ul li{

   display:inline-block;

}

</style> 