<style>
  .payclr{
    color: #000;
  }
</style>
<div class="">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-rupee"></i> Pay Now </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Fee</li>

            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

    <div class="box-body">

        <div class="row">

           <div class="">
  <div class="row">
    <div class="col-md-8">
  <!-- <h4>Pay Now</h4>
  <br> -->
  <!-- Nav tabs -->
 

  <!-- Tab panes -->
  <div class="tab-content">
    
   


        <form method="post" action="<?=base_url()?>invoice/updatePersonal_detail">
              <div class="form-group payclr">
         <?php
          if ($invoice) {
            $mainInvoice = $invoice->invoiceAmount;
          }else{
            $mainInvoice = 0.00;
          }
          if ($advancePayment) {
              $totaladvanceAmount = $advancePayment->advanceAmount;
          }else{
              $totaladvanceAmount = 0;
          }
         ?>
          <label for="exampleInputEmail1" class="col-md-4">Latest Bill Amount</label>
          <div class="col-md-1">:</div>
          <div class="col-md-7">
            <p><i class="fa fa-inr"></i> <?=$mainInvoice?></p>
          <!-- <input type="text" name="payment" class="form-control" value="<?=$mainInvoice?>" placeholder="payment" readonly> -->
         </div>
        </div>
              <div class="form-group payclr">
         
          <label for="exampleInputEmail1" class="col-md-4">Advanced Amount</label>
          <div class="col-md-1">:</div>
          <div class="col-md-7">
            <p><i class="fa fa-inr"></i> <?=$totaladvanceAmount?></p>
          <!-- <input type="text" name="payment" class="form-control" value="<?=$totaladvanceAmount?>" placeholder="payment" readonly> -->
         </div>
        </div>
        <div class="form-group payclr">
         
          <label for="exampleInputEmail1" class="col-md-4">Outstanding Bill</label>
          <div class="col-md-1">:</div>
          <div class="col-md-7">
            <p><i class="fa fa-inr"></i> <?=$totaladvanceAmount+$mainInvoice-$totaladvanceAmount?><span style="font-size: font-size: 11px;"> (Please ignore if already paid)
Your payment may take up to 3 days to reflect</span></p>
          <!-- <input type="text" name="payment" class="form-control" value="<?=$totaladvanceAmount+$mainInvoice-$totaladvanceAmount?>" placeholder="payment" readonly> -->
         </div>
        </div>

        <div class="form-group payclr">
         
          <label for="exampleInputEmail1" class="col-md-4">Payment Amount</label>
          <div class="col-md-1">:</div>
          <div class="col-md-4">
            <!-- <?=$amountinvoiceData->totalPaid?> -->
          <input type="number" name="payment" class="form-control" value="<?=$totaladvanceAmount+$mainInvoice-$totaladvanceAmount?>" placeholder="payment">
         </div>
         <div class="col-md-3">
           <button type="submit" class="btn btn-primary" disabled style="padding: 5px;">Proceed to pay</button>
         </div>
        </div>


       <!--  <div class="form-group payclr col-md-offset-5">
        <button type="submit" class="btn btn-primary" disabled>Proceed to pay</button>
      </div> -->
      </form>


      </div>

        
        

    </div>
    
    



    


  </div>
</div>
</div>
</div>

        </div>

    </div>
</div>
</div>
</div>
</div>
</div>


</div>



