<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-address-card-o"></i> Billing Profile</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Billing Details</li>

            </ol>
        </div>
    </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

    <div class="box-body">

        <div class="row">

           <div class="">
  <div class="row">
    <div class="col-md-8">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" >
    <li>
      <a href="<?php echo base_url()?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>&type=pds">Personal Details</a>
    </li>
    <li>
      <a href="<?php echo base_url()?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>&type=bds">Billing Details</a>
    </li>
    <li>
      <a href="<?php echo base_url()?>invoice/billing_details?panel=<?php echo $this->input->get('panel') ?>&type=vbad">Virtual Bank Account Details</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <?php 
      $type = $this->input->get('type');
      if ($type == 'pds') {
    ?>
    <div id="pds" class=""><br>
      


        <form method="post" action="<?=base_url()?>invoice/updatePersonal_detail">

        <div class="form-group">
         
          <label for="exampleInputEmail1" class="col-md-4">Email address</label>
          <div class="col-md-8">
          <input type="email" name="email" value="<?=$pdsData->email?>" class="form-control"placeholder="Enter email">
         </div>
        </div>


        <div class="form-group">

          <label for="exampleInputEmail1" class="col-md-4">First Name</label>
          <div class="col-md-8">
          <input type="text" class="form-control" name="name" value="<?=$pdsData->name?>" placeholder="First Name"><br>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>

      </div>
      </form>


    </div>
    <?php   
      }elseif ($type == 'bds') {
    ?>
    <div class=""><br>
      
<form method="post" action="<?=base_url()?>invoice/updateBilling_detail">
      <div class="form-group">
   <input type="hidden" name="billing_detailsID" value="<?=$bdsData->billing_detailsID?>">
    <label for="exampleInputEmail1" class="col-md-4">Company Name :</label>
    <div class="col-md-8">
    <input type="text" name="companyName" value="<?=$bdsData->college_name?>" class="form-control" placeholder="Company Name">

</div>
</div>

<div class="form-group">

    <label for="exampleInputEmail1" class="col-md-4">GST :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" name="gst" value="<?=$bdsData->gst?>" placeholder="07AADCE2687JKH">
  </div>

</div>

<div class="form-group">

    <label for="exampleInputEmail1" class="col-md-4">PAN :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" name="pan" value="<?=$bdsData->pan?>" placeholder="PAN">

</div>
</div>


<div class="form-group">
   
    <label for="exampleInputEmail1" class="col-md-4">Phone :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" name="phone" value="<?=$bdsData->phone?>" placeholder="9019985221">
  </div>

</div>

<div class="form-group">

    <label for="exampleInputEmail1" class="col-md-4">Address :</label>
    <div class="col-md-8">
    <textarea class="form-control" name="address" rows="3"><?=$bdsData->address?> </textarea>
  </div>

</div>

<div class="form-group">
    
    <label for="exampleFormControlSelect1" name="state" class="col-md-4">State :</label>
    <div class="col-md-8">
    <select class="form-control" id="exampleFormControlSelect1">
      <option value="<?=$bdsData->state?>" >Delhi</option>
      <option value="<?=$bdsData->state?>" >Up </option>
      <option value="<?=$bdsData->state?>" >Haryana</option>
      <option value="<?=$bdsData->state?>" >Gujrat </option>
      <option value="<?=$bdsData->state?>" >Bengal </option>
    </select>
  </div>

</div>

<div class="form-group">
    
    <label for="exampleInputEmail1" class="col-md-4">Pin Code :</label>
    <div class="col-md-8">
    <input type="text" class="form-control" name="pin_code" value="<?=$bdsData->pin_code?>" placeholder="Pin Code"><br>
    <button type="submit" class="btn btn-primary">Update</button>

</div>
</div>
</form>

    </div>
    <?php   
      }else{
    ?>
      <div class=""><br>
     
      <div class="-fluid">
        
       <table class="table table-bordered">
   
      <tr>
        <th>Bank Account No.</th>
        <th>22652367567</th>
      </tr>

      <tr>
        <th>Beneficiary Name</th>
        <th>E2E Networks</th>
      </tr>

      <tr>
        <th>IFSC Code</th>
        <th>YESDGKOF</th>
      </tr>
   
    
  </table>
</div>
    </div>

    <?php     
      }
    ?>
  </div>
</div>
</div>
</div>

        </div>

    </div>
</div>
</div>
</div>
</div>
</div>


</div>




<script type="text/javascript">

   $('#classesID').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

       var classesID = $(this).val();
   
           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "classesID_invoice=" + classesID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#subCourseID').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var subCourseID = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "subCourseID_invoice=" + subCourseID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#SessionTo').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var SessionTo = $(this).val();

       var SessionFrom  = $('#SessionFrom').val();

   

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data:{SessionTo_invoice:SessionTo,SessionFrom_invoice:SessionFrom},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#sessionType').change(function() {

    $('#invoice_data_table').DataTable().state.clear();

       var sessionType = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "sessionType=" + sessionType,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#examType').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var examType = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "examType=" + examType,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#education_mode').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var education_mode = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "education_mode=" + education_mode,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>



<script type="text/javascript">

   $('#student_position').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var student_position = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "student_position=" + student_position,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>



<script type="text/javascript">

   $('#yos_filter').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var yos_filter = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "yos_filter_invoice=" + yos_filter,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#payment_status').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var payment_status = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: "payment_status_invoice=" + payment_status,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   $('#teacherID').change(function() {

     $('#invoice_data_table').DataTable().state.clear();

   

       var teacherID = $(this).val();

   

           $.ajax({

   

               type: 'POST',

   

               url: "<?=base_url('invoice/index')?>",

   

               data: " teacherID=" + teacherID,

   

               dataType: "html",

   

               success: function(data) {

   

                    location.reload();

   

               }

   

           });

   

   });

   

</script>

<script type="text/javascript">

   function ResetSesession(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetSesession')?>",

   

               data:{ResetSesession:'ResetSesession'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }

   

   

   function ResetCourses(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetCourses')?>",

   

               data:{ResetCourses:'ResetCourses'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }   

   function ResetsessionType(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetsessionType')?>",

   

               data:{ResetCourses:'ResetCourses'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }

   function ResetAllfilter_student(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/ResetAllfilter')?>",

   

               data:{ResetAllfilter:'ResetAllfilter'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }

   
   
   function Resetyos(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/Resetyos')?>",

   

               data:{ResetMorefilter:'ResetMorefilter'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }





   function Resetpaymentstatus(){

     $('#invoice_data_table').DataTable().state.clear();

           $.ajax({

               type: 'POST',

   

               url: "<?=base_url('invoice/Resetpaymentstatus')?>",

   

               data:{ResetMorefilter:'ResetMorefilter'},

   

               dataType: "html",

   

               success: function(data) {

                   location.reload();

   

               }

   

           });

   }
</script>



<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

        if(classesID == 0) {

            $('#hide-table').hide();

        } else {

            $.ajax({

                type: 'POST',

                url: "<?=base_url('setfee/setfee_list')?>",

                data: "id=" + classesID,

                dataType: "html",

                success: function(data) {

                    window.location.href = data;

                }

            });

        }

    });

</script>

