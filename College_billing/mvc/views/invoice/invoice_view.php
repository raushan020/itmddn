<div class="">
 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="fa fa-table"> </i> Passbook</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">

                       <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                      <li class="active">Passbook<!-- <?=$this->lang->line('menu_teacher')?> --></li>
                        </ol>
                    </div>
            </div>
    <!-- form start -->
    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">

<div class="well">
                     <div class="row">
                      <?php

                         if($this->session->flashdata('success')) { ?>
                      <div class="alert alert-success">      
  <strong>Success!</strong>Invoice Updated
</div>
<?php } ?>
                        <div class="col-sm-3">
                           <button class="btn btn-success btn-xs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> Print </button>
                           <!-- <a href="http://101.53.153.40/blackboard/invoice/print_preview/37" class='btn btn-success btn-xs' style='text-decoration: none;' role='button' target='_blank'><i class='fa fa-file'></i> PDF Preview</a> -->
               

                        </div>
                        <div class="col-sm-9">
                      <?php if($session__data){ ?>    
                    <a href="<?php echo base_url() ?>invoice/clg_invoice?panel=<?php echo $this->input->get('panel') ?>" class=" btn btn-success add-btn ">Update Invoice</a>
                  <?php } else{ ?>
                         <a href="<?php echo base_url() ?>invoice/clg_invoice?panel=<?php echo $this->input->get('panel') ?>" class=" btn btn-success add-btn ">Create Invoice</a>
                  <?php } ?>
                           </div>
                        </div>
                     </div>
                  </div>

     
            <div class="col-sm-12">
            
           
              <div class="clearfix"></div>
              <div class="breakdownl_tbl">
                <style type="text/css">
                  <style>
.verticalTable th, td {
  border: 1px solid black !important;
  border-collapse: collapse;
}
.verticalTable th, td {
  padding: 5px;
  text-align: left;
}
</style>
                </style>
           
             
            
              </div>
                <div id="hide-table">
                  

                    <table align="center" class="table  table-bordered table-hover dataTable no-footer">

   <tr>
      <th>Session</th>
      <th>Semester</th> 
        <th>T.Student</th>
        <th>Total Payment</th>
        <th>Payment Status</th>
   </tr>
<?php
 $total_student = 0;
 $total_payment = 0;
 $total_paid = 0;
 ?>
<?php if($session__data) { ?>

 <?php

  foreach ($session__data as $key => $value) {
 ?>
<?php 
$this->db->where('total_students !=',0);
$this->db->where('session',$value->session);
$studs= $this->db->get('clg_invoice')->result();
 ?>
   <tr>
      <tr>
        <td rowspan="3"><?php echo $value->session ?></td>
      </tr>

 <?php if($studs){ ?>
  <?php
   foreach ($studs as $key => $values) { 
     $total_student += $values->total_students;
     $total_payment += $values->total_payment;
     $total_paid += $values->paid_amount;
     if($values->total_payment==$values->paid_amount){
      $paid_status =  "<span style='color:green'>Paid</span>";
     }else{
      $paid_status =  "<span style='color:red'>Not Paid</span>";
     }
    ?>
      <tr>
        <td ><?php echo $values->yearsOrSemester ?></td>
        <td ><?php echo $values->total_students ?></td>
        <td ><?php echo $values->total_payment ?></td>
        <td ><?php echo $paid_status ?></td>
      </tr>
 <?php } ?>
 <?php } ?>
      </tr>

   <?php } } ?>

 </table>

 <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18">
 </div>
  <div class="col-xs-6 col-sm-6 col-lg-6 col-md-18">
    <table class="table">
                                    <tbody><tr>
                                       <th class="col-sm-8 col-xs-8">Total Students</th>
                                   
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $total_student; ?></td>
                                    </tr>

                                 <tr>
                                       <th class="col-sm-8 col-xs-8">Total Payment</th>
                                 
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i> <?php echo $total_payment; ?></td>
                                    </tr>

                                          <tr>
                                       <th class="col-sm-8 col-xs-8">Due Amount</th>
                                 
                                       <td class="col-sm-4 col-xs-4" id="due_amount_fetch"><i class="fa fa-inr" aria-hidden="true"></i><?php echo $total_payment-$total_paid; ?></td>
                                    </tr>

                           

                                 </tbody></table>
                            


 </div>


                
                </div>
            </div> <!-- col-sm-12 -->
          
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
</div>
</div>
<script type="text/javascript">
     function invoice_filter_url(val){
      
   window.location.href=base_url+"invoice/clgPayment/"+val;
   
   }
   
</script>
<script>

  var status = '';

  var id = 0;

  $('.onoffswitch-small-checkbox').click(function() {

      if($(this).prop('checked')) {

          status = 'chacked';

          id = $(this).parent().attr("id");

      } else {

          status = 'unchacked';

          id = $(this).parent().attr("id");

      }



      if((status != '' || status != null) && (id !='')) {

          $.ajax({

              type: 'POST',

              url: "<?=base_url('teacher/active')?>",

              data: "id=" + id + "&status=" + status,

              dataType: "html",

              success: function(data) {

                  if(data == 'Success') {

                      toastr["success"]("Success")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  } else {

                      toastr["error"]("Error")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  }

              }

          });

      }

  }); 

</script>

