<!DOCTYPE html>

<html lang="en">

<head>

<title><?php echo $panel_title; ?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
     <style>
         @media print { * 
        {
            -webkit-print-color-adjust:exact;
        }
         }

        @media print {
            @page { size: auto;  margin: 0mm; }
            * {
                margin: 0mm;
                padding: 0mm;
                -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
                color-adjust: exact !important;
                size:A4 landscape;
            }
            body {
                margin: 0;
                padding: 0;
                font-size: 11pt;
                font-family: sans-serif;
                width: 100%;
            }
            .print-pdl0 {
                padding-left: 0;
            }
            .print-pdr0 {
                padding-right: 0;
            }
            .print-inline {
                width: auto;
            }
            .print-break {
                margin-top: 10px;
            }
            .print-office {
                margin-top: 10px;
            }
            .footer-text, .footer-text img {
                padding-top: 20px;
            }
            .flexFooter {
                padding-top: 20px;
            }
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }
            .form1 .row {
                margin-top: 10px;
            }
            .form1 .details label {
                font-weight: normal;
            }
            input {
                width: 100%;
                border: none;
                border-bottom: 1px solid black;
            }
            .form1 .photo {
                width: 100px;
                height: 100px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }


             .form1 .photo2 {
                width: 100px;
                height: 50px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }

            .form1 .formBeginning {
                padding: 20px;
            }

            .print:last-child {
                page-break-after: auto;
            }
        }

        @media screen {
            .mgt20 {
                margin-top: 20px;
            }

            .mgt30 {
                margin-top: 30px;
            }

            .mgt40 {
                margin-top: 40px;
            }

            .mgt60 {
                margin-top: 60px;
            }

            input {
                padding: 5px 10px;
            }

            input:hover,
            input:focus {
                outline: none;
            }

            .form1 input {
                width: 100%;
                border: none;
                border-bottom: 1px dotted black;
            }

            .form1 .photo {
                width: 200px;
                height: 200px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }

            .form1 .photo2 {
                width: 200px;
                height: 50px;
                border: 2px solid black;
                display: flex;
                align-items: center;
                margin: 0 auto;
                justify-content: center;
            }

            .form1 .details label {
                font-weight: normal;
            }

            .form1 input.form-check-input {
                width: auto;
            }

            .form1 .formBeginning {
                padding: 20px;
            }

            .form1 .btn-submit {
                width: auto;
                display: block;
                margin: 20px auto;
            }
        }

        @media only screen and (max-width: 767px) {
            .form1 .mversion {
                display: flex;
                flex-direction: column-reverse;
                margin-top: 40px;
            }

            .form1 .mversion input {
                margin-bottom: 20px;
            }

            .form1 .mversion .mgt60 {
                margin-top: 0;
            }

            .mversion-mgt20 {
                margin-top: 20px !important;
            }

            .mversion-radio {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .flexFooter {
                display: flex;
                flex-direction: column-reverse;
                align-items: center;
            }

            .flexFooter p {
                padding-top: 20px;
                text-align: center;
            }
        }

        @media only screen and (max-width: 991px) {
            .tversion-mgt20 {
                margin-top: 20px;
            }
        }

        @media only screen and (min-width: 768px) {
            .form1 label {
                padding-top: 10px;
            }

            .footer-text {
                padding-top: 40px;
            }
        }

        @media only screen and (min-width: 992px) {
            .lversion-padding {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }
    </style>



<style type="text/css">

    #page-wrap {

        width: 700px;

        margin: 0 auto;

    }

    .center-justified {

        text-align: justify;

        margin: 0 auto;

        width: 30em;

    }

    /*ini starts here*/

    .list-group {

      padding-left: 0;

      margin-bottom: 15px;

      width: auto;

    }

    .list-group-item {

      position: relative;

      display: block;

      padding: 7.5px 10px;

      margin-bottom: -1px;

      background-color: #fff;

      border: 1px solid #ddd;

      /*margin: 2px;*/

    }

    table {

      border-spacing: 0;

      border-collapse: collapse;

      font-size: 12px;

    }

    td,

    th {

      padding: 0;

    }

    @media print {

      * {

        color: #000 !important;

        text-shadow: none !important;

        background: transparent !important;

        box-shadow: none !important;

      }

      a,

      a:visited {

        text-decoration: underline;

      }

      a[href]:after {

        content: " (" attr(href) ")";

      }

      abbr[title]:after {

        content: " (" attr(title) ")";

      }

      a[href^="javascript:"]:after,

      a[href^="#"]:after {

        content: "";

      }

      pre,

      blockquote {

        border: 1px solid #999;



        page-break-inside: avoid;

      }

      thead {

        display: table-header-group;

      }

      tr,

      img {

        page-break-inside: avoid;

      }

      img {

        max-width: 100% !important;

      }

      p,

      h2,

      h3 {

        orphans: 3;

        widows: 3;

      }

      h2,

      h3 {

        page-break-after: avoid;

      }

      select {

        background: #fff !important;

      }

      .navbar {

        display: none;

      }

      .table td,

      .table th {

        background-color: #fff !important;

      }

      .btn > .caret,

      .dropup > .btn > .caret {

        border-top-color: #000 !important;

      }

      .label {

        border: 1px solid #000;

      }

      .table {

        border-collapse: collapse !important;

      }

      .table-bordered th,

      .table-bordered td {

        border: 1px solid #ddd !important;

      }

    }

    table {

      max-width: 100%;

      background-color: transparent;

      font-size: 12px;

    }

    th {

      text-align: left;

    }

    .table {

      width: 100%;

      margin-bottom: 20px;

    }

    .table h4 {

      font-size: 15px;

      padding: 0px;

      margin: 0px;

    }

    .head {

       border-top: 0px solid #e2e7eb;

       border-bottom: 0px solid #e2e7eb;  

    }

    .table > thead > tr > th,

    .table > tbody > tr > th,

    .table > tfoot > tr > th,

    .table > thead > tr > td,

    .table > tbody > tr > td,

    .table > tfoot > tr > td {

      padding: 8px;

      line-height: 1.428571429;

      vertical-align: top;

      /*border-top: 1px solid #e2e7eb; */

    }

    /*ini edit default value : border top 1px to 0 px*/

    .table > thead > tr > th {

      font-size: 15px;

      font-weight: 500;

      vertical-align: bottom;

      /*border-bottom: 2px solid #e2e7eb;*/

      color: #242a30;

     

      

    }

    

    .table > caption + thead > tr:first-child > th,

    .table > colgroup + thead > tr:first-child > th,

    .table > thead:first-child > tr:first-child > th,

    .table > caption + thead > tr:first-child > td,

    .table > colgroup + thead > tr:first-child > td,

    .table > thead:first-child > tr:first-child > td {

      border-top: 0;

    }

    .table > tbody + tbody {

      border-top: 2px solid #e2e7eb;

    }

    .table .table {

      background-color: #fff;

    }

    .table-condensed > thead > tr > th,

    .table-condensed > tbody > tr > th,

    .table-condensed > tfoot > tr > th,

    .table-condensed > thead > tr > td,

    .table-condensed > tbody > tr > td,

    .table-condensed > tfoot > tr > td {

      padding: 5px;

    }

    .table-bordered {

      border: 1px solid #e2e7eb;

    }

    .table-bordered > thead > tr > th,

    .table-bordered > tbody > tr > th,

    .table-bordered > tfoot > tr > th,

    .table-bordered > thead > tr > td,

    .table-bordered > tbody > tr > td,

    .table-bordered > tfoot > tr > td {

      border: 1px solid #e2e7eb;

    }

    .table-bordered > thead > tr > th,

    .table-bordered > thead > tr > td {

      border-bottom-width: 2px;

    }

    .table-striped > tbody > tr:nth-child(odd) > td,

    .table-striped > tbody > tr:nth-child(odd) > th {

      background-color: #f0f3f5;

    }

    .panel-title {

      margin-top: 0;

      margin-bottom: 0;

      font-size: 20px;

      color: #fff;

      padding: 0;

    }

    .panel-title > a {

      color: #707478;

      text-decoration: none;

    }

    a {

      background: transparent;

      color: #707478;

      text-decoration: none;

    }

    strong {

        color: #707478;

    }

</style>

</head>

<?php 

  $usertype = $this->session->userdata("usertype");

  if($usertype){

?>

   <body>

<?php {
 ?>
 <div style="padding-left-top: 5px;">
  <button   class="btn btn-success btn-xs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?=$this->lang->line('print')?> </button>
</div>
<?php
 }?>
 <div id="printablediv">

     
                    <div class="container" >
                        <div class="wrapper">
                            <img src="<?php echo base_url()?>images/stu_img/Home_Page_Header.jpg" alt="" class="img-responsive" / style="height: 320px;">
                            <form class="form1">
                                <div class="formBeginning">
                                    <div class="row mversion">
                                        <div class="col-sm-10 col-md-9">
                                            <div class="row mgt40">
                                                <div class="col-sm-4 col-md-2">
                                                    <label for="sno" class="col-sm-3 col-md-5 col-form-label print-pdr0">S.No </label>
                                                    <div class="col-sm-9 col-md-7 print-pdl0">
                                                        <input type="text"  value="&nbsp;&nbsp;<?=$student->username?>" class="form-control-plaintext" id="sno">
                                                    </div>
                                                </div>

                                                <div class="col-sm-4 col-md-3">
                                                    <label for="sno" class="col-sm-5 col-md-5 col-form-label print-pdr0">Session Type</label>
                                                    <div class="col-sm-6 col-md-5 print-pdl0">
                                                        <input type="text"  value="&nbsp;<?=$student->sessionType?>" class="form-control-plaintext" id="sno">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-3">
                                                    <label for="year" class="col-sm-3 col-md-4 col-form-label print-pdr0">Year </label>
                                                    <div class="col-sm-7 col-md-8 print-pdl0">
                                                        <input type="text" value="&nbsp;<?=$student->session?>" class="form-control-plaintext" id="year">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-3">
                                                    <label for="rollno" class="col-sm-6 col-md-5 col-form-label print-pdr0">Roll No.</label>
                                                    <div class="col-sm-9 col-md-7 print-pdl0">
                                                        <input type="text" value="<?=$student->roll?>" class="form-control-plaintext" id="rollno">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="col-sm-12 col-md-12 mgt60">
                                                    <label for="appliedCourse" class="col-md-6 col-form-label">Course Applied for:</label>
                                                    <div class="col-md-12">
                                                        <input type="text"  value="<?=$class->classes?>" class="form-control-plaintext" id="appliedCourse">
                                                    </div>
                                                </div> -->
                                                <div class="col-md-6"><label for="appliedCourse">Course Applied for:</label></div>
                                                <div class="col-md-6"><input type="text"  value="<?=$class->classes?>" class="form-control-plaintext" id="appliedCourse"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-3">
                                            <div class="photo">
                                               <?=img(base_url('uploads/images/'.$student->photo))?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row details">
                                        <div class="col-md-12">
                                            <p class="text-center mgt20"><strong>(Filling of the form does not guarantee admission in the Institution)</strong></p>
                                            <p class="text-center"><strong>(The form is to be filled by the candidate in his/her own handwriting in BLOCK LETTERS only)</strong></p>

                                            <div class="row mgt20">
                                                <div class="col-sm-12 col-md-12">
                                                    <label for="name" class="col-sm-3 col-md-2 col-form-label print-pdr0">1. Name in Full :</label>
                                                    <div class="col-sm-9 col-md-10 print-pdl0">
                                                        <input type="text" value="<?=$student->name?>" class="form-control-plaintext" id="name">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="fname" class="col-sm-6 col-md-4 col-form-label print-pdr0">2. Father's Name :</label>
                                                    <div class="col-sm-6 col-md-8 print-pdl0">
                                                        <input type="text"  value="<?=$student->father_name?>" class="form-control-plaintext" id="fname">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="mname" class="col-sm-6 col-md-4 col-form-label mversion-mgt20 print-pdr0">Mother's Name :</label>
                                                    <div class="col-sm-6 col-md-8 print-pdl0">
                                                        <input type="text"  value="<?=$student->mother_name?>" class="form-control-plaintext" id="mname">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="dob" class="col-sm-6 col-md-4 col-form-label print-pdr0">3. Date of Birth :</label>
                                                    <div class="col-sm-6 col-md-8 print-pdl0">
                                                        <input type="text" value="<?=date("d M Y", strtotime($student->dob))?>" class="form-control-plaintext" id="dob">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 mversion-mgt20">
                                                    <label for="gender" class="col-xs-4 col-sm-3 col-md-4 col-form-label print-pdr0">Gender :</label>

                                          
                                                    <div class="col-xs-8 col-sm-9 col-md-8">
                                                     
                                                        <div class="form-check form-check-inline col-xs-6 col-sm-5 col-md-6 mversion-radio">                                      
                <input class="form-check-input print-inline"  type="radio" name="inlineRadioOptions" id="inlineRadio1" value="male" <?php if($student->sex=='Male'){ echo "checked"; }else{ echo ""; }?>  >
                                                            <label class="form-check-label" for="inlineRadio1">Male</label>
                                                        </div>
                                           
                                                        <div class="form-check form-check-inline col-xs-6 col-sm-7 col-md-6 mversion-radio">
                <input class="form-check-input print-inline" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="female" <?php if($student->sex=='Female'){ echo "checked"; }else{ echo ""; }?>>
                                                            <label class="form-check-label" for="inlineRadio2">Female</label>
                                                        </div>
                                                   
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-12">
                                                            <label for="address" class="col-sm-4 col-md-2 col-form-label print-pdr0">4. Permanent Address :</label>
                                                            <div class="col-sm-8 col-md-10 print-pdr0">
                                                                <input type="text"  value="<?=$student->address?>" class="form-control-plaintext" id="address">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mgt20">
                                                        <div class="col-sm-6 col-md-3">
                                                            <label for="city" class="col-sm-3 col-md-4 col-form-label">City :</label>
                                                            <div class="col-sm-9 col-md-8 lversion-padding">
                                                                <input type="text" class="form-control-plaintext" id="city">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3 mversion-mgt20">
                                                            <label for="pin" class="col-sm-4 col-md-5 col-form-label lversion-padding">Pin Code :</label>
                                                            <div class="col-sm-8 col-md-7 lversion-padding">
                                                                <input type="text" value=" <?=$student->pin?>" class="form-control-plaintext" id="pin">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3 mversion-mgt20 tversion-mgt20 print-break">
                                                            <label for="phone" class="col-sm-6 col-md-7 col-form-label lversion-padding">Phone (with STD) :</label>
                                                            <div class="col-sm-6 col-md-5 lversion-padding">
                                                                <input type="text" class="form-control-plaintext" id="phone">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3 mversion-mgt20 tversion-mgt20 print-break">
                                                            <label for="mobile" class="col-sm-3 col-md-4 col-form-label lversion-padding">Mobile No.:</label>
                                                            <div class="col-sm-9 col-md-8 lversion-padding">
                                                                <input type="text" value=" <?=$student->phone?>" class="form-control-plaintext" id="mobile">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                            <div class="row mgt20">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-12">
                                                            <label for="corrAddress" class="col-sm-4 col-md-3 col-form-label">5. Correspondence Address :</label>
                                                            <div class="col-sm-8 col-md-9">
                                                                <input type="text" value=" <?=$student->address?>" class="form-control-plaintext" id="corrAddress">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mgt20">
                                                        <div class="col-sm-6 col-md-3">
                                                            <label for="corrCity" class="col-sm-3 col-md-4 col-form-label">City :</label>
                                                            <div class="col-sm-9 col-md-8 lversion-padding">
                                                                <input type="text" class="form-control-plaintext" id="corrCity">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3 mversion-mgt20">
                                                            <label for="corrPin" class="col-sm-4 col-md-5 col-form-label lversion-padding">Pin Code :</label>
                                                            <div class="col-sm-8 col-md-7 lversion-padding">
                                                                <input type="text" value=" <?=$student->pin?>"  class="form-control-plaintext" id="corrPin">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3 mversion-mgt20 tversion-mgt20 print-break">
                                                            <label for="corrPhone" class="col-sm-6 col-md-7 col-form-label lversion-padding print-pdr0">Phone (with STD) :</label>
                                                            <div class="col-sm-6 col-md-5 lversion-padding print-pdl0">
                                                                <input type="text" class="form-control-plaintext" id="corrPhone">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-3 mversion-mgt20 tversion-mgt20 print-break">
                                                            <label for="corrMobile" class="col-sm-3 col-md-4 col-form-label lversion-padding">Mobile No.:</label>
                                                            <div class="col-sm-9 col-md-8 lversion-padding">
                                                                <input type="text" value=" <?=$student->phone?>" class="form-control-plaintext" id="corrMobile">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mgt20">
                                                        <div class="col-xs-12 col-sm-6 col-md-6 print-break">
                                                            <label for="corrPlace" class="col-xs-12 col-sm-5 col-md-5 col-form-label print-pdr0">Place of Residence :</label>
                                                            <div class="form-check form-check-inline col-xs-6 col-sm-4 col-md-3 print-pdr0">
                                                                <input class="form-check-input print-inline" type="checkbox" id="rural" value="option1">
                                                                <label class="form-check-label" for="rural">Rural</label>
                                                            </div>
                                                            <div class="form-check form-check-inline col-xs-6 col-sm-3 col-md-3 print-pdr0">
                                                                <input class="form-check-input print-inline" type="checkbox" id="urban" value="option2">
                                                                <label class="form-check-label" for="urban">Urban</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 mversion-mgt20 print-break">
                                                            <label for="aadhar" class="col-sm-6 col-md-4 col-form-label lversion-padding">Aadhar Card No. :</label>
                                                            <div class="col-sm-6 col-md-8 lversion-padding">
                                                                <input type="text" value=" <?=$student->aadhar?>" class="form-control-plaintext" id="aadhar">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="nationality" class="col-sm-5 col-md-4 col-form-label">6. Nationality :</label>
                                                    <div class="col-sm-7 col-md-8">
                                                        <input type="text" value="<?=$student->nationality?>" class="form-control-plaintext" id="nationality">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <label for="email" class="col-sm-4 col-md-4 col-form-label mversion-mgt20">Email ID (Student):</label>
                                                    <div class="col-sm-8 col-md-8">
                                                        <input type="email" value="<?=$student->email?>" class="form-control-plaintext" id="email">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row mgt30">
                                        <p class="text-center mgt20 print-office"><strong>(For Office use only)</strong></p>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="officeNo" class="col-sm-1 col-md-1 col-form-label">No.</label>
                                                    <div class="col-sm-4 col-md-4">
                                                        <input type="text" class="form-control-plaintext" id="officeNo">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mgt20">
                                                <div class="col-sm-6 col-md-4">
                                                    <label for="receivedDate" class="col-sm-5 col-md-4 col-form-label print-pdr0">Received on</label>
                                                    <div class="col-sm-7 col-md-4 print-pdl0">
                                                        <input type="text" class="form-control-plaintext" id="receivedDate">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-8 mversion-mgt20">
                                                    <label for="requirements" class="col-sm-6 col-md-3 col-form-label">Other Requirements</label>
                                                    <div class="col-sm-6 col-md-9s">
                                                        <input type="text" class="form-control-plaintext" id="requirements">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--<input type="button" value="Submit" class="btn btn-primary btn-submit">-->

                                    <div class="row flexFooter mgt20">
                                        <div class="col-sm-10 col-md-6 footer-text">
                                            <p>Undertaking enclosed[Y][N] <br/> Documents not attached <br /> 1/2/3/4/5/6/7/8/9/10/11</p>
                                        </div>
                                        <div class="col-sm-2 col-sm-offset-3 col-md-2 col-md-offset-4">
                                            <img src="<?php echo base_url()?>images/stu_img/Home_Page_Footer.jpg" alt="20 Years" class="img-responsive" width="250" style="height: 200px;">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                

                <!--  page 2 print -->



                    <div class="container wrapper">
                        <img src="<?php echo base_url()?>images/stu_img/Bottom_Design.jpg" alt="Border" width="1170" class="printimg-break">
                        <form class="form2">
                            <div class="row mgt20">
                                <div class="col-sm-12 col-md-12">
                                    <label class="col-sm-12 col-md-12 col-form-label">10. Details of Previous / Qualifying Examination (Attached attested Mark Sheets):</label>                                     
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name of Exam</th>
                                                <th>Name of Institution</th>
                                                <th>Board / University</th>
                                                <th>Year of Passing</th>
                                               <th>Marks Obtained</th>
                                                <th>Max. Marks</th>
                                                <th>% Marks / CGPA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                                foreach ($education as $key => $value) 
                                                {
                                            ?> 
                                                    <?php
                                                        if($value->education=='10th Class')
                                                        {
                                                    ?>
                                                            <tr>
                                                                <th>X</th>
                                                                <td>-</td>
                                                                <td><?php echo $value->board_name;?></td>
                                                                <td><?php echo $value->year_passing; ?></td>
                                                            
                                                                <td> </td>
                                                                <td> </td>
                                                                <td><?php echo $value->percentage; ?></td>
                                                            </tr>
                                                    <?php        
                                                        }

                                                        if($value->education=='12th Class')
                                                        {
                                                    ?>
                                                           <tr>
                                                                <th>XII</th>
                                                                <td>-</td>
                                                                <td><?php echo $value->board_name;?></td>
                                                                <td><?php echo $value->year_passing; ?></td>
                                                            
                                                                <td></td>
                                                                <td></td>
                                                                <td><?php echo $value->percentage; ?></td>
                                                            </tr> 
                                                    <?php
                                                        }
                                                    ?>

                                                     <?php 
                                                } 
                                            ?>

                                            <tr>
                                                <th>Diploma</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>Gradu</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>P.G.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>Others</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            
<tr>
                                                <th>Others</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row mgt20">
                                <div class="col-sm-12 col-md-12">
                                    <label class="col-sm-12 col-md-12 col-form-label">11.Details of subject marks of qualifying exam (X/XII/Diploma/Graduation/etc.) :</label>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Subject</th>
                                            <th>Max. Marks</th>
                                            <th>Marks obatined</th>
                                            <th>% Marks / CGPA</th>
                                            <th>% PCM</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>1.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>2.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>3.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>4.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>5.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>6.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th>7.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <label for="name" class="col-sm-12 col-md-6 col-form-label">12. Ever disqualified from studies (if yes, give reasons for the same) : </label> <div class="col-sm-12 col-md-6">
                                        <input type="text" class="form-control-plaintext" id="name">
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <input type="text" class="form-control-plaintext" id="name">
                                    </div>
                                </div>
                            </div>

                            <div class="row mgt20">
                                <div class="col-sm-6 col-md-6">
                                    <label for="f2name" class="col-sm-6 col-md-4 col-form-label">13. Name of Father :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" value="<?=$student->father_name?>" class="form-control-plaintext" id="f2name">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <label for="m2name" class="col-sm-6 col-md-4 col-form-label">Name of Mother :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text"  value="<?=$student->mother_name?>" class="form-control-plaintext" id="m2name">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="foccupation" class="col-sm-7 col-md-4 col-form-label">Occupation of Father :</label>
                                    <div class="col-sm-5 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="foccupation">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="moccupation" class="col-sm-7 col-md-4 col-form-label">Occupation of Mother :</label>
                                    <div class="col-sm-5 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="moccupation">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="moffce" class="col-sm-6 col-md-4 col-form-label">Office Address :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="moffce">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="foffce" class="col-sm-6 col-md-4 col-form-labe">Office Address :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="foffce">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="fmobile" class="col-sm-6 col-md-4 col-form-label">Mobile No. :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="fmobile">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="mmobile" class="col-sm-6 col-md-4 col-form-label">Mobile No. :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="mmobile">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="femail" class="col-sm-6 col-md-4 col-form-label mversion-mgt20">Email ID :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="femail">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="memail" class="col-sm-6 col-md-4 col-form-label mversion-mgt20">Email ID :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="memail">
                                    </div>
                                </div>
                            </div>

                            <div class="row mgt20">
                                <div class="col-sm-8 col-md-6">
                                    <label for="guardian" class="col-sm-7 col-md-6 col-form-label ">14. Name of Local Guardian (if any)</label>
                                    <div class="col-sm-5 col-md-6">
                                        <input type="text" class="form-control-plaintext" id="guardian">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-6">
                                    <label for="relationship" class="col-sm-7 col-md-4 col-form-label mversion-mgt20 ">Relationship :</label>
                                    <div class="col-sm-5 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="relationship">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="loccupation" class="col-sm-7 col-md-4 col-form-label">Occupation of Guardian</label>
                                    <div class="col-sm-5 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="loccupation">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="laddress" class="col-sm-6 col-md-4 col-form-label mversion-mgt20">Address :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="laddress">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="lmobile" class="col-sm-6 col-md-4 col-form-label mversion-mgt20">Mobile No. :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="lmobile">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 mgt20">
                                    <label for="lemail" class="col-sm-6 col-md-4 col-form-label">Email ID :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="email" class="form-control-plaintext" id="lemail">
                                    </div>
                                </div>
                            </div>

                            <div class="row mgt20">
                                <div class="col-md-12">
                                    <label for="annualIncome" class="col-sm-6 col-md-4 col-form-label">15. Annual Income of Parents/Guardian :</label>
                                    <div class="col-sm-6 col-md-8">
                                        <input type="text" class="form-control-plaintext" id="annualIncome">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <img src="<?php echo base_url()?>images/stu_img/Top_Design.jpg" alt="" width="1170" class="printimg-bottom_break">
                    </div>


                <!-- page 3 print -->




                <div class="container wrapper">
                    <img src="<?php echo base_url()?>images/stu_img/Bottom_Design.jpg" alt="Border" width="1170" class="printimg-break">
                    <form class="form3">
                        <div class="row mgt20">
                            <div class="col-sm-12 col-md-12">
                                <h4 class="text-uppercase text-center" style="font-weight: bold">
                                    <u >Undertaking by the student / parent / guardian</u>
                                </h4>
                                <ol class="mgt20">
                                    <li>I do hereby certify that the particulars furnished in the application form are true correct. I also agree                     that admission may be granted as per rules, regulations, terms and conditions of the University. I fully understand that admission will be cancelled and fee paid by me will be forfeited, if any of the particulars given by me at the time of admission or at later stage, are found to be incorrect,concealed or false.</li>
                                    <li>I agree that Vice Chancellor decision in all matters concerning my admission, studies, discipline and conduct will be final and binding upon me.</li>
                                    <li>I understand that the student's association, active or passive, with any unlawful organization unions etc, is forbidden and I shall not have any objection, if I am expelled or rusticated from the University due to my participation in any union/unlawful activity or any other activity which violates code of conduct of the University.</li>
                                    <li>I understand that the consumption of alcohol, drugs or any other intoxicant within the University 
                                    /Institute/Hostel premises is forbidden.</li>
                                    <li>I am aware that as per Hon'ble Supreme Court orders, ragging in any form whatsoever is totally  banned and criminal FIR will be lodged in case, I Am involved in any case of ragging. This will result in any rustication from the University and I will not be able to take admission anywhere in India.</li>
                                    <li>I hereby declare that I was never involved or punished in any case of indiscipline during my school or college career. There is no inquiry pending against me with the school/college/police/other authorities of the state to which I belong or resided in any other state at any time.</li>
                                    <li>I Confirm that I have submitted the original certificates to the University on my own will and shall be returned to me after the same are verified by the University. In case, I discontinue or leave the University in between the duration of the course, I shall have to pay the fee for the remaining duration of the course. Failing to do so the University can take any legal action against me.</li>
                                    <li>I agree, that if any component of fee is revised by the competent authority, the enhanced amount if any, shall be deposited within two weeks of issue of such notification. I understand that the fee has to be paid in advance semester wise.</li>
                                    <li>I understand that I have to undergo mandatory practical training for specified periods in accordance with the relevance of me being placed in an industrial organization within or outside the state in which campus is located, for such practical training. I will not have any objection to my being placed so. The University shall not be held responsible in any way for any mishap that may occur to me during my tenure in the University/Institute/Hostel or during the training outside the University.</li>
                                    <li>I hereby agree to make all the necessary arrangements for the Boarding and Lodging, if hostel accommodation is not available or availed.</li>
                                    <li>I am aware that my name will be stuck off from the rolls of the University, if I fail to pay fee on time, remain absent continuously for a period of 10 days without information or behave in a manner subversive to the discipline or commit grave misconduct such as ragging or indulge in any activity which may bring disrepute to the University.</li>
                                    <li>I am aware that the use of mobile phones in the University premises is banned. In case I violate this, strict action will be taken against me as per the disciplinary rules of the University.</li>
                                    <li>I shall acknowledge that my testimonials and photographs taken during functions/events/workshops/Seminars are deemed to be the property of the University. University has the right to the usage of students photograph in the University website, brochure and other related publications and materials.</li>
                                    <li>I understand that I have to follow all University Rules and Regulations in the subjects of attendance, examination, discipline and  in all other matters.</li>
                                    <li>I will report for Registration as per the schedule announced by the University failing which my admission will stand cancelled without any intimation from the University.</li>
                                    <li>I understand that in the event of breach of any of the aforementioned conditions of the undertaking my admission will stand  cancelled without any obligation on the part of the University to issue show cause notice. In such case, the fee other than the refundable amount shall stand forfeited.</li>
                                    <li>In case, I avail the University transport facility, the University is not responsible for any mishap or discontinuation of bus service. I shall be availing the transport facility at my own risk and peril. The necessary undertaking in this regard shall be submitted as and when I start availing the transport facility.</li>
                                    <li>l agree that I have taken admission in Lingaya's Institution after understanding all the admission procedures of the University.</li>
                                    <li>I would be attending the classes regularly as I am aware that 75% attendance is compulsory in every subject/course.</li>
                                    <li>The use of Laptop is compulsory and I would be using laptop regularly during the full duration of course.</li>
                                    <li>Refund policy is governed as per the process given in the brochure or given on the University website. I have read & understood the FEE REFUND POLICY of the University.</li>
                                    <li>I have received the copy of Brochure along with this form.</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-md-6 signature">
                                <p class="text-left"><strong>Signature of Parents/Guardian</strong></p>

                            </div>
                            <div class="col-sm-6 col-md-6 signature photo2"  style="padding-left: 250;">
                                  
                                               <?=img(base_url('uploads/signature/'.$student->signature))?>
                                             
                                <p class="text-right"><strong>Signature of Student</strong></p>
                            </div>
                        </div>

                    </form>
                    <img src="<?php echo base_url()?>images/stu_img/Top_Design.jpg" alt="" width="1170" class="printimg-bottom_break">
                </div>


                <!-- page 4 print -->




                <div class="container wrapper">
                    <img src="<?php echo base_url()?>images/stu_img/Bottom_Design.jpg" alt="Border" width="1170" class="printimg-break">
                    <form class="form4">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h4 class="text-uppercase text-center" style="font-weight: bold"><u>Declaration</u></h4>
                                <p class="mgt20">I hereby solemnly confirm that I have read the University Prospectus and have clearly understood the
                                    points mentioned in the undertaking. I shall abide by the rules and regulations of the University as
                                    mentioned in the prospectus and also the changes which may be notified from time to time. I further
                                    confirm that the information given in the application form given by me is correct and true to the best of
                                    my knowledge.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-8">
                                <p class="text-right signature">Signature of the candidate</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>I shall abide by the conditions of the undertaking duly signed by my ward.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-8">
                                <p class="text-right signature">Signature of Parent/Guardian</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-4 mgt20">
                                <label for="place" class="col-sm-3 col-md-5 col-form-label print-pdr0">Place:</label>
                                <div class="col-sm-9 col-md-7 print-pdl0">
                                    <input type="text" class="form-control-plaintext"id="place" style="margin:  -15px;">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-8 mgt20">
                                <label for="address" class="col-sm-3 col-md-4 col-form-label print-pdr0" style="text-align: right; padding-right: 174px;">Address:</label>
                                <div class="col-sm-12 col-md-8 print-pdl0">
                                    <input type="text" class="form-control-plaintext" id="address"style=" margin: -15px 0px 0px -174px;">

                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-4 mgt20">
                                <label for="date" class="col-sm-3 col-md-5 col-form-label print-pdr0">Date:</label>
                                <div class="col-sm-9 col-md-7 print-pdl0">
                                    <input type="text" class="form-control-plaintext" id="date"style="margin: -15px ;">
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-8 mgt20">
                                 
                                <div class="col-sm-12 col-md-8 print-pdl0">
                                    <input type="text" class="form-control-plaintext" id="date"style="margin: -15px 0px 0px 90px;">
                                </div>
                            </div>
                            

                        </div>
                            <div class="row mgt20 printimg-break">
                                <div class="col-md-12">
                                    <p>Following Attested Copies are enclosed with the Application Form: (Tick appropriate one's)</p>
                                    <ol>
                                        <li>Result Card of any other entrance examination</li>
                                        <li>Admit Card of entrance exam.</li>
                                        <li>Matriculation Certificate/Proof of age.</li>
                                        <li>Detailed Marks Card (10+2) or Equivalent Exam.</li>
                                        <li>Detailed Marks Card of /Diploma /Graduation /or Equivalent Exam.</li>
                                        <li>Character Certificate from Institution of Last attended.</li>
                                        <li>Migration/School Leaving Certificate.</li>
                                        <li>Domicile Certificate/ Affidavit/ Aadhar Card</li>
                                        <li>Passport (if applicable)</li>
                                        <li>Medical Certificate (as per attached format)</li>
                                        <li>Six colored and two black passport size photographs.</li>
                                    </ol>
                                    <p  style="font-weight: bold;">Note:</p>
                                    <ul>
                                        <li>Incomplete application forms shall not be accepted</li>
                                        <li>It must be understood that the mere submission of the form, in no way confirms admission to the
                                            Institution. After the completion of the admission process by the competent authority the admission of
                                            lapse seats shall be made the sole discretion of the Governing Body of the Institution in accordance with the relevant Rules &amp; Regulations of the Competent Authority.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <img src="<?php echo base_url()?>images/stu_img/Last_Page_Center.jpg" alt="Lingaya's Group of Institutions" width="1000" class="banner">
                                </div>
                            </div>
                            <div class="row printimg-break">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Vidyapeeth,</strong><br/>
                                            Faridabad<br/>
                                            <i>(Approved by MHRD/AICTE/ PCI/ 
                                            BCI / COA/NCTE, Govt. of India
                                                u/s 3 of UGC Act 1956)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Lalita Devi Institute
                                            of Management &amp; Sciences,</strong><br/>
                                            Mandi, New Delhi<br/>
                                            <i>(Approved by Govt. of NCT of
                                                Delhi and Affiliated to GGSIP
                                                University)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Institute of
                                            Management &amp; Technology,</strong><br/>
                                            Near Nunna, Vijayawada (A.P)<br/>
                                            <i>(Approved by AICTE Govt. of A.P.
                                                and Affiliated to JNTU)</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Institute of Health
                                            Sciences,Faridabad</strong><br/>
                                            <i>(Approved by Indian Nursing
                                                Council & Govt. of Haryana and
                                                Affiliated to Pt. B.D. Sharma
                                                University)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Public School,</strong><br/>
                                            Faridabad<br/>
                                            <i>(Affiliated to CBSE & Approved by
                                                Govt. of Haryana)</i>
                                    </div>
                                    <div class="col-sm-4 col-md-4 text-center">
                                        <p><strong>Lingaya's Academy,</strong><br/>
                                            Saket, New Delhi<br/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                <img src="<?php echo base_url()?>images/stu_img/Last_Page_Footer.jpg" alt="" width="1170" class="printimg-bottom_break">
                </div>
                </body>

<?php } ?>

</html>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <script language="javascript" type="text/javascript">

        function printDiv(divID) {
            //Get the HTML of div

            var divElements = document.getElementById(divID).innerHTML;

            //Get the HTML of whole page

            var oldPage = document.body.innerHTML;



            //Reset the page's HTML with div's HTML only

            document.body.innerHTML =

              "<html><head><title></title></head><body>" +

              divElements + "</body>";



            //Print Page

            window.print();



            //Restore orignal HTML

            document.body.innerHTML = oldPage;

        }

</script>