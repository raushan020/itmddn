

<div class="">

 <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="ti ti-id-badge"></i> <?=$this->lang->line('panel_title')?> </h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">

                       <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                      <li class="active"> <?=$this->lang->line('menu_teacher')?></li>
                        </ol>
                    </div>
            </div>
    <!-- form start -->

    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">
            <div class="col-sm-12">

              <div class="col-sm-6">
                
              </div>
              <div class="col-sm-6">
          <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin_btech"){
            ?>
   <div class="pull-right">
      <div class="btn-group">
   <a href="<?php echo base_url('teacher/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span>Add Academic Partner</a>
      </div>
   </div>
   <?php  }  ?>
              </div>
              <div class="clearfix"></div>
              <?php
              if ($usertype == "Academic")
                { ?>
               
              
                <div class="col-sm-4">
                  <div class="">
                     <form style="" class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Relationship Manager 
                           </label>
                           <div class="selectdiv">
                              <select id="subCourseID" class="form-control" onchange="ajaxGet_Teacher($(this).val())">
                                 <option value="">Relationship Manager</option>
                                 <?php  foreach ($total_rm as $key => $value) {
                                    if ($value->userID==$this->session->userdata('rmFilter')) {
                                        $selected =  "Selected";
                                    }else{
                                      $selected =  "";
                                    }
                                    ?>
                                 <option value="<?php echo $value->userID ?>" <?php echo $selected ?>><?php echo $value->name ?></option>
                                 <?php } ?>
                              </select>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetrmFilter()">Reset This Filter </a></div>
                        </div>
                     </form>
                  </div>
               </div>
              <?php }
               ?>



                <div id="hide-table">

                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>

                                <!-- <th class="col-sm-2"><?=$this->lang->line('teacher_photo')?></th> -->

                                <th class="col-sm-2"><?=$this->lang->line('organization_name')?></th>


                                <th class="col-sm-2"><?=$this->lang->line('teacher_name')?></th>

                                 <th class="col-sm-2">Username</th>

                                <th class="col-sm-2"><?=$this->lang->line('teacher_email')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('phone')?></th>

                                <?php if($usertype == "Admin"){ ?>

                                <!-- <th class="col-sm-2"><?=$this->lang->line('teacher_status')?></th> -->

                                <?php } ?>

                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>

                            </tr>

                        </thead>

                        <tbody>

                            

                        </tbody>

                    </table>

                </div>



            </div> <!-- col-sm-12 -->

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->
</div>
</div>


<script>

  var status = '';

  var id = 0;

  $('.onoffswitch-small-checkbox').click(function() {

      if($(this).prop('checked')) {

          status = 'chacked';

          id = $(this).parent().attr("id");

      } else {

          status = 'unchacked';

          id = $(this).parent().attr("id");

      }



      if((status != '' || status != null) && (id !='')) {

          $.ajax({

              type: 'POST',

              url: "<?=base_url('teacher/active')?>",

              data: "id=" + id + "&status=" + status,

              dataType: "html",

              success: function(data) {

                  if(data == 'Success') {

                      toastr["success"]("Success")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  } else {

                      toastr["error"]("Error")

                      toastr.options = {

                        "closeButton": true,

                        "debug": false,

                        "newestOnTop": false,

                        "progressBar": false,

                        "positionClass": "toast-top-right",

                        "preventDuplicates": false,

                        "onclick": null,

                        "showDuration": "500",

                        "hideDuration": "500",

                        "timeOut": "5000",

                        "extendedTimeOut": "1000",

                        "showEasing": "swing",

                        "hideEasing": "linear",

                        "showMethod": "fadeIn",

                        "hideMethod": "fadeOut"

                      }

                  }

              }

          });

      }

  }); 

</script>
<script type="text/javascript">
   function  ajaxGet_Teacher(rmFilter){
      $('#example4').DataTable().state.clear();
      // alert(rmFilter);
    $.ajax({
            
                type: 'POST',
                url: "<?=base_url('teacher/index')?>",
                data: "rmFilter=" + rmFilter,
                dataType: "html",
                success: function(data) {
                     location.reload();
                }
   
    
   
            }); 
   }
</script>
<script type="text/javascript">
  function ResetrmFilter(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetrmFilter')?>",
   
   
   
               data:{ResetCourses:'ResetCourses'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
</script>