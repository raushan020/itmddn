<style>
  .AddNewSubjetcsDetails{
    margin-bottom: 10px;
  }
  .margin20{
    margin-bottom: 20px;
  }
  .add-more {
	  display:none;
  }
</style>
<?php 
              if($this->input->get('type')){
              $type = $_GET['type'];
              }else{
              $type = 'clg';
              }
?>

<div class="">

    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-book"></i> <?=$this->lang->line('panel_title')?>/ Syllabus/ Assignment / Unit</h3>
        </div>
        <div class="col-md-6 align-self-center">
            <ol class="breadcrumb">
              <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

              <li><a href="<?=base_url("subject/index")?>"><?=$this->lang->line('menu_subject')?></a></li>

              <li class="active"><?=$this->lang->line('menu_add')?> Syllabus</li>
            </ol>
        </div>          
     </div>

    <!-- /.box-header -->

    <!-- form start -->

     <div class="container-fluid">
      <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

            <div class="col-sm-12">
              <h3 style="text-align:center;"><?php 
              if($subject->subject){
              echo $subject->subject; 
              }
               ?></h3>
               <hr>
                 <div class="row">
                       <ul class="nav nav-tabs">

  <li  <?php if($type=='pdf'){ echo "class='active'"; } ?> ><a href="<?php echo base_url() ?>syllabus/add_syllabus/<?php echo $this->uri->segment(3) ?>?type=pdf">Add Syllabus</a></li>
  <li <?php if($type=='notes_pdf'){ echo "class='active'"; } ?>><a href="<?php echo base_url() ?>syllabus/add_syllabus/<?php echo $this->uri->segment(3) ?>?type=notes_pdf">Add Assignments</a></li>
   <li <?php if($type=='unit'){ echo "class='active'"; } ?>><a href="<?php echo base_url() ?>syllabus/add_syllabus/<?php echo $this->uri->segment(3) ?>?type=unit">Add Unit</a></li>
</ul>
                     </div>
               
                 

<style type="text/css">
  .AddNewSyllabus{
    box-shadow: 0px 1px 0px 0px;
    padding: 22px;
  }
  .AddNewSyllabus{
    position:relative;
  }
  .delete_unit{
    position:absolute;
    right:33px;
  }
  .ds_s{
display:none;
  }
</style>
<?php if($type=="unit"){ ?>
<div id="appendUnitDetails">
  
<?php
	if($subjects)
	{
		foreach ($subjects as $key => $value) 
		{ 
?>
			<div class='AddNewSubjetcsDetails AddNewSyllabus' id="unit_<?php echo $key ?>">
				<div class="alert alert-success ds_s" id="success_msg_<?php echo $key ?>">
					<strong> Success!</strong>
				</div>
				<!-- use for progres bar -->
				<div class="delete_unit">
					<button class="btn btn-danger btn-xs mrg for_margR" onclick="delete_unit('<?php echo $value['unitID'] ?>', 'unit_<?php echo $key ?>','<?php echo $key ?>')">Delete Unit</button>
				</div>
				<h3>Unit <?php echo $value['unit_code'] ?></h3>

				<div class='col-sm-6'>
					<div class=''>
						<input type='text' class='form-control' id='subjectNameClear' onBlur='unitEdit(this, "unit_name", "<?php echo $value['unitID'] ?>",$(this).val(),"<?php echo $key ?>")'  placeholder='Unit Name' name='unit_name' value='<?php echo set_value('unit_name' , $value['unit_name']) ?>' >
					</div>
				</div>
				<div class='col-sm-6'>
					<div class=''>
						<input type='text' class='form-control' id='subjectCodeClear' onBlur='unitEdit(this,"unit_code","<?php echo $value['unitID'] ?>",$(this).val(),"<?php echo $key ?>")' placeholder='Unit Code' name='unit_code' value="<?php echo set_value('unit_code',$value['unit_code']) ?>" >
					</div>
				</div>
				<!-- <div class="col-sm-12">&nbsp;</div> -->
			<?php 
				if($value['topic'])
				{
					foreach($value['topic'] as $key2 => $value2) 
					{ 
			?>
						<div class='col-sm-3' id="topics_<?php echo $key.$key2  ?>">
							<div class=''>
								<!-- <input type='text' class='form-control' id='subjectCodeClear' onBlur='EditSubject(this,\"subject_code\","<?php $value->topic_name ?>",$(this).val())' placeholder='Unit Code' name='topic_name' value='<?php echo set_value('topic_name',$tn[$i]) ?>' > -->
								<div class="control-group input-group" style="margin-top:10px">
									<input type="text" name="topic_name[]" id="topic_name[]" class="form-control" onBlur='topicEdit(this,"topic_name","<?php echo $value2['topicID'] ?>",$(this).val(),"<?php echo $key ?>")' placeholder='Unit Code' name='topic_name' value='<?php echo set_value('topic_name',$value2['topic_name']) ?>'>
									<div class="input-group-btn"> 
										<button class="btn btn-danger" type="button" onclick="delete_topic('<?php echo $value2['topicID'] ?>', 'topics_<?php echo $key.$key2  ?>','<?php echo $key ?>')">
											<i class="glyphicon glyphicon-remove"></i>
										</button>
									</div>
								</div><hr>
							</div>
						</div>
			<?php 
						}  
					} 
			?>
				<!--<div class="col-sm-12">
					<div class="input-group control-group after-add-more<?php echo $key ?>">
						<input type="hidden" name="addmore[]" id="addTopicMore<?php echo $key ?>" class="form-control" placeholder="Enter Name Here">
						<div class="input-group-btn"> 
							<button class="btn btn-success" type="button" onclick="addTopicsMore('<?php echo $value['unitID'] ?>','<?php echo $value['subjectID'] ?>','addTopicMore<?php echo $key ?>')">
							Add Topic</button>
						</div>
					</div>
				</div>-->
				<div class='clearfix'></div> 
			</div>
<?php 
		} 
	}
?>

</div>

<div class="">
<div class="inner_part">
<h3>Add Units</h3>
<div class="copy hide">
  <div class="control-group input-group" style="margin-top:10px">
    <input type="hidden" name="addmore[]" class="form-control" placeholder="Enter Topic Name Here">
    <div class="input-group-btn"> 
      <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
    </div>
  </div>
</div>
<form method="post" id="form_unit">
<div class="col-sm-6">
<input type="text" class="form-control" id="unit_name" placeholder="Unit Name" name="unit_name" value="<?=set_value('unit_name')?>" >
</div>
<div class="col-sm-6">
<input type="text" class="form-control" id="unit_code" placeholder="Unit Number" name="unit_number" value="<?=set_value('unit_number')?>" >
</div>
<input type="hidden" id="subjectID" name="subjectID" value="<?= $this->uri->segment(3)?>">
<!-- <div class="col-sm-12">&nbsp;</div> -->
<div class="col-sm-12">
  <div class="input-group control-group after-add-more">
          <input type="hidden" name="addmore[]" id="addmore[]" class="form-control" placeholder="Enter Topic Name Here">
          <div class="input-group-btn"> 
            <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="copy hide">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="hidden" name="addmore[]" id="addmore[]" class="form-control" placeholder="Enter Name Here">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove" type="button">
                <i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>
        </div>
</div>

<div class="col-sm-12" style="text-align: center; padding: 15px;">
  <input type="button" id="addun" class="btn btn-success add-btn update-btn" value="Submit">
</div>
</form> 
<!-- <div class="col-sm-6">
<button>   </button>
</div> -->
  <div class="clearfix"></div>
</div>                 
              
                    </div>
<?php } ?>     

               <style type="text/css">

.pdf_sylnus{
  padding:10px 10px 10px 10px;
}
.biit_dd{
  margin:3%;
}
               </style>
<?php if($type=="pdf"){ ?> 
        <h3 style="padding: 15px;">Syllabus</h3>
              <div class="pdf_sylnus" id="afx_khjk">
              <?php if($pdf_slybus){?>
                <button class="btn btn-primary"><i class="fa fa-file-pdf-o" style="color: #fff;"></i> &nbsp;<?php echo $pdf_slybus->pdf ?></button>


<button class="btn btn-danger" type="button" onclick='delete_pdf_sylabus("<?php echo $pdf_slybus->id  ?>")'><i class="glyphicon glyphicon-remove"></i></button>

<span><button class="btn btn-success"><a target="_blank" href="<?php echo base_url() ?>uploads/syllabus/<?php echo $pdf_slybus->pdf ?>"><i class="fa fa-eye" style="color: #fff;"></i></a></button></span>

   <?php } else
    {?>
     <div class="biit_dd">    
      <div class="col-md-4">
    <input type="file" id="multiFiles" name="files"/></div>
      <div class="col-md-4">
        <button id="upload_file_multiple" class="btn btn-success add-btn update-btn">Upload</button> 
      </div> 
    </div>
   <?php } ?>

<div > 
</div>
              </div> 
          
  <?php } ?>

     <?php if($type=="notes_pdf"){ ?> 
          <h3 style="padding: 15px;">Subject Assignment</h3>
              <div class="pdf_sylnus" id="afx_note">
                <?php if($pdf_slybus_notes){?>

    <button class="btn btn-primary"><i class="fa fa-file-pdf-o" style="color: #fff;"></i> &nbsp;<?php echo $pdf_slybus_notes->pdf ?></button>

      <button class="btn btn-danger" type="button" onclick='delete_pdf_note("<?php echo $pdf_slybus_notes->id ?>")'>
  
                <i class="glyphicon glyphicon-remove"></i></button>

                <span style="">
                  <button class="btn btn-success"><a target="_blank" href="<?php echo base_url() ?>uploads/syllabus/<?php echo $pdf_slybus_notes->pdf ?>"><i class="fa fa-eye" style="color: #fff;"></i></a></button></span>



   <?php } else
    {?>
     <div class="biit_dd">    
      <div class="col-md-4">
    <input type="file" id="multiFiles_notes" name="files[]" multiple="multiple"/></div>
      <div class="col-md-4">
        <button id="upload_file_multiple_notes" class="btn btn-success add-btn update-btn">Upload</button> 
      </div> 
    </div>
   <?php } ?>

<div > 
</div>
              </div> 
          
  <?php } ?>


            </div> 

        </div>

    </div>

</div>

</div>

</div>

<script type="text/javascript">
           
                $('#upload_file_multiple').on('click', function () {
                  var id =  "<?php echo $this->uri->segment(3) ?>";
                    var form_data = new FormData();
                    var ins = document.getElementById('multiFiles').files.length;
                    for (var x = 0; x < ins; x++) {
                        form_data.append("files[]", document.getElementById('multiFiles').files[x]);
                    }
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/UploadFile/"+id, // point to server-side PHP script 
                        dataType: 'text', // what to expect back from the PHP script
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#afx_khjk').html(response); // display success response from the PHP script
							$("#multiFiles").val('');
                        },
                        error: function (response) {
                            $('#afx_khjk').html(response); // display error response from the PHP script
                        }
                    });
                });
      
        </script>
        <script type="text/javascript">
           
                $('#upload_file_multiple_notes').on('click', function () {
                  var id =  "<?php echo $this->uri->segment(3) ?>";
                    var form_data = new FormData();
                    var ins = document.getElementById('multiFiles_notes').files.length;
                    for (var x = 0; x < ins; x++) {
                        form_data.append("files[]", document.getElementById('multiFiles_notes').files[x]);
                    }
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/UploadFile_notes/"+id, // point to server-side PHP script 
                        dataType: 'text', // what to expect back from the PHP script
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#afx_note').html(response); // display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_note').html(response); // display error response from the PHP script
                        }
                    });
                });
      
        </script>
        <script type="text/javascript">
           
                function delete_pdf_sylabus(id){
                

                  var confirmation = confirm("are you sure you want to remove the item?");

                  var subjectID =  "<?php echo $this->uri->segment(3) ?>";
                  if(confirmation){
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/delete_pdf_sylabus/"+id, // point to 
                        data:{subjectID:subjectID},
                        type: 'post',
                        success: function (response) {
                          console.log(response);
                            $('#afx_khjk').html(response);
                            location.reload(); // display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_khjk').html(response); // display error response from the PHP script
                        }
                    });
            
            }
          }
      


                function delete_pdf_note(id){
                

                  var confirmation = confirm("are you sure you want to remove the item?");

                  var subjectID =  "<?php echo $this->uri->segment(3) ?>";
                  if(confirmation){
                    $.ajax({
                        url: "<?php echo base_url() ?>syllabus/delete_pdf_notes/"+id, // point to 
                        data:{subjectID:subjectID},
                        type: 'post',
                        success: function (response) {
                          console.log(response);
                            $('#afx_note').html(response); 
                            location.reload();// display success response from the PHP script
                        },
                        error: function (response) {
                            $('#afx_notes').html(response); // display error response from the PHP script
                        }
                    });
            
            }
          }
      

        </script>
<script type="text/javascript">
  function delete_topic(id,id_forHide,key){
  $.ajax({
    type: "POST", 
  url:base_url+'syllabus/DeleteTopic',
  data:{id:id},
  success: function(response) {
     $('#success_msg_'+key).show();
$("#"+id_forHide).hide();

        }

  });
    }
</script>

<script type="text/javascript">
	function delete_unit(id,id_forHide,key){
		$.ajax({
			type:"POST", 
			data:{"id":id},
			url:"<?php echo base_url(); ?>syllabus/deleteUnit",
			success: function(data) {
				location.reload();
			}
		});
    } 

function unitEdit(name,colum_name,id,val,key){
 $.ajax({ 
  type: "POST", 
  url:base_url+'syllabus/unitEdit',
  data:{colum_name:colum_name,id:id,val:val},
  success: function(response) {
    $('#success_msg_'+key).show();
        }

  });
}

function topicEdit(name,colum_name,id,val,key){
 $.ajax({
  type: "POST", 
  url:base_url+'syllabus/topicEdit',
  data:{colum_name:colum_name,id:id,val:val},
  success: function(response) {
    $('#success_msg_'+key).show();
        }

  });
}

function addTopicsMore(unitID,subjectID,val){
  var topic_name  =$('#'+val).val();  
  $.ajax({
    type: "POST", 
  url:base_url+'AjaxController/addTopicsMore',
  data:{val:topic_name,subjectID:subjectID,unitID:unitID},
  success: function(response) {
// $('#appendUnitDetails').html(response);
 location.reload(true); 

        }

  });

}

</script>

<style type="text/css">
button, input, optgroup, select, textarea{    
  
  color: inherit;
}
</style>

<script type="text/javascript"> 
$("#addun").click(function(){
	$.ajax({
		type: "POST", 
		url:base_url+'AjaxController/AddUnit',
		data:$('#form_unit').serialize(),
		success: function(response) {
			//alert(response);
			location.reload();
		}
	});
});
</script>