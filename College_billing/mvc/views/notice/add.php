<!-- <?php $usertype =  $this->session->userdata('usertype'); ?> -->
<style type="text/css">

.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

.tab button:hover {
  background-color: #ddd;
}

.tab button.active {
  background-color: #ccc;
}

.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>

<div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>

                <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_notice')?></li>

            </ol>
        </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body">                  

                        <div class="col-sm-12">

                            <div class="tab">

                              <?php 
                                $usertype = $this->session->userdata("usertype");
                               if( $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin"|| $usertype == "Academic" || $usertype == "Accountant"|| $usertype == "Super_A_P"|| $usertype == "Teacher"){
                                if($usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin" || $usertype == "Academic"){?>

  <button class="tablinks" onclick="openCity(event, 'admin')" id="defaultOpen" style="color: black">Admin</button>

  <button class="tablinks" onclick="openCity(event, 'rm')" style="color: black">Relationship Manager</button>
    <?php }
    if($usertype == "Accountant" || $usertype == "Super_A_P" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin" || $usertype == "Academic" || $usertype == "Teacher"){
   if($usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin" || $usertype == "Academic"){?>
    <button class="tablinks" onclick="openCity(event, 'super_ap')" id="defaultOpen" style="color: black">Super Academic Partner</button>
        <?php }
    if($usertype == "Super_A_P" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin" || $usertype == "Academic"){
   ?>
  <button class="tablinks" onclick="openCity(event, 'academic_partner')" id="defaultOpen"  style="color: black">Academic Partner</button>
  <?php }
  if($usertype == "Teacher" || $usertype == "Super_A_P" || $usertype == "Accountant" || $usertype == "ClgAdmin" || $usertype == "Support" || $usertype == "Admin" || $usertype == "Academic"){
  ?>
  <button class="tablinks" onclick="openCity(event, 'student')"  id="defaultOpen" style="color: black">Students</button>
<?php  }}}?>
</div>
	<div id="admin" class="tabcontent">
		<form class="form-horizontal" role="form" method="post"> 

    <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Admin" >

            <div class="custom-control custom-checkbox" for="check">
                <input type="checkbox" class="custom-control-input chkPassport1"  name="all_admin" value="All" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">All Admin</label>
            </div>

            <div class="auto-widget" id="dvPassport1" style="display: none">
                
                <p>Tag Your Admin: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
            </div>

            <script type="text/javascript">
    $(function () {
        $(".chkPassport1").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport1").hide();
            } else {
                $("#dvPassport1").show();
            }
        });
    });
</script> 


            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                      	<span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form>
	</div>

	<div id="rm" class="tabcontent">
		<form class="form-horizontal" role="form" method="post">
        <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Accountant" >

            <div class="custom-control custom-checkbox" for="check">
                <input type="checkbox" class="custom-control-input chkPassport2"  name="all_accountant" value="All" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">All Relationship Manager</label>
            </div>

            <div class="auto-widget" id="dvPassport2" style="display: none">
                
                <p>Tag Your Relationship Manager: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
            </div>

            <script type="text/javascript">
    $(function () {
        $(".chkPassport2").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport2").hide();
            } else {
                $("#dvPassport2").show();
            }
        });
    });
</script>                
            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                      	<span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form> 
	</div>

    <div id="super_ap" class="tabcontent">
        <form class="form-horizontal" role="form" method="post">
        <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="super_AP" >

            <div class="custom-control custom-checkbox" for="check">
                <input type="checkbox" class="custom-control-input chkPassport3"  name="all_Super_ap" value="All" id="defaultUnchecked">
                <label class="custom-control-label" for="defaultUnchecked">All Super Academic Partners</label>
            </div>

            <div class="auto-widget" id="dvPassport3" style="display: none">
                
                <p>Tag Your Super Academic Partners: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
            </div>

            <script type="text/javascript">
    $(function () {
        $(".chkPassport3").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport3").hide();
            } else {
                $("#dvPassport3").show();
            }
        });
    });
</script>                
            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                        <span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form> 
    </div>

	<div id="academic_partner" class="tabcontent">
		<form class="form-horizontal" role="form" method="post">
 <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Teacher" >

			<div class="custom-control custom-checkbox" for="check">
			    <input type="checkbox" class="custom-control-input chkPassport4"  name="all_academic" value="All" id="defaultUnchecked">
			    <label class="custom-control-label" for="defaultUnchecked">All Academic Partners</label>
			</div>

			<div class="auto-widget" id="dvPassport4" style="display: none">
                
    			<p>Tag your Academic Partner: <input type="text" id="skill_input" class="txtPassportNumber" /></p>
			</div>

			<script type="text/javascript">
    $(function () {
        $(".chkPassport4").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport4").hide();
            } else {
                $("#dvPassport4").show();
            }
        });
    });
</script>

            <?php 
                if(form_error('title')) 
                   echo "<div class='form-group has-error' >";
                else     
                    echo "<div class='form-group' >";
            ?>
              <label for="title" class="col-sm-2 control-label">
                Notice Title
              </label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >
              </div>
                <span class="col-sm-4 control-label">
                    <?php echo form_error('title'); ?>
                </span>
    </div>
                <?php 
                    if(form_error('date')) 
                      echo "<div class='form-group has-error' >";
                    else     
                      echo "<div class='form-group' >";
                ?>
                    <label for="date" class="col-sm-2 control-label">
                        <?=$this->lang->line("notice_date")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('date'); ?>
                        </span>
    </div>
                <?php 
                    if(form_error('notice')) 
                       echo "<div class='form-group has-error' >";
                    else     
                        echo "<div class='form-group' >";
                ?>
                    <label for="notice" class="col-sm-2 control-label">
                                            Descriptions
                    </label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
                    </div>
                      	<span class="col-sm-4 control-label">
                          <?php echo form_error('notice'); ?>
                        </span>
    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                        </div>
                    </div>
        </form>
	</div>
			<div id="student" class="tabcontent">

        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
          <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Student" >
                        <?php if(form_error('classesID[]')){ ?>
                        <div class="alert alert-danger">
                           <strong>Danger!</strong><?php echo  form_error('classesID[]'); ?>
                        </div>
                        <?php } ?>
                        <!-- <label for="title" class="col-sm-1 control-label">
                           Course
                           
                           </label> -->
                        <!-- <?php 
                           if(form_error('classesID')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                           <label for="classesID" class="col-sm-2 control-label">
                           
                              Course<span style="color: red;">*</span>
                           
                           </label>
                           
                           <div class="col-sm-6" >
                                  
                                                   
                               <?php
                              $array = array();
                              
                              
                              $array[0] = "Select Course";
                              
                              
                              foreach ($classes as $classa) {
                              
                              
                                  $array[$classa->classesID] = $classa->classes;
                              
                              }
                              
                              
                              echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control multipleSelectOption'  onchange = 'CourseSDependent1($(this).val())' value='Select Course' ");
                              
                              ?>
                           
                                  
                           </div> 
                           
                               
                           <span class="col-sm-4 control-label">
                           
                               <?php echo form_error('classesID'); ?>
                           
                           </span>
                           
                           </div> -->
                        <div id="subCourseID">
                        </div>
                        <div id="fetchYearsAndSem"  multiple="multiple">
                        </div>
                        <div id="appendSujectsDetails">
                        </div>
                        <?php 
                           if(form_error('title')) 
                           
                               echo "<div class='form-group has-error' >";
                           
                           else     
                           
                               echo "<div class='form-group' >";
                           
                           ?>
                        <label for="title" class="col-sm-2 control-label">
                        Notice Title
                        </label>
                        <div class="col-sm-6">
                           <input type="text" class="form-control" placeholder="Notice Title" autocomplete="off" name="title" value="<?=set_value('title')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('title'); ?>
                        </span>
                  </div>
                  <?php 
                     if(form_error('date')) 
                     
                         echo "<div class='form-group has-error' >";
                     
                     else     
                     
                         echo "<div class='form-group' >";
                     
                     ?>
                  <label for="date" class="col-sm-2 control-label">
                  <?=$this->lang->line("notice_date")?>
                  </label>
                  <div class="col-sm-6">
                  <input type="" class="form-control datepicker_quiz_data" autocomplete="off" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >
                  </div>
                  <span class="col-sm-4 control-label">
                  <?php echo form_error('date'); ?>
                  </span>
               </div>
               <?php 
                  if(form_error('notice')) 
                  
                      echo "<div class='form-group has-error' >";
                  
                  else     
                  
                      echo "<div class='form-group' >";
                  
                  ?>
               <label for="notice" class="col-sm-2 control-label">
               Description
               </label>
               <div class="col-sm-6">
               <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>
               </div>
               <span class="col-sm-4 control-label">
               <?php echo form_error('notice'); ?>
               </span>
            </div>
            <label for="date" class="col-sm-2 control-label">
            Select Course
            </label>
            <div class="col-sm-6">
            <input type="button" class="btn btn-success" onclick='selectAll()' value="Check All Course"/>
            <input type="button" class="btn btn-success" onclick='selectAllsemester()' value="Uncheck"/>
            </div>
            <?php foreach ($classes as $key => $value) {   ?>          
            <div class="col-sm-5" style="box-shadow: 0px 0px 2px 1px #bfbfbf;padding: 5px;margin: 10px"> 
            <div class="dsd">
            <label class="nexCheckbox"><?php echo $value->classes ?>
            <input type="checkbox" name="classesID[]" onclick="select_course(this,'<?php echo $key ?>')" value="<?php echo $value->classesID ?>" id="select_all" >
            <span class="checkmark checkmark-action-layout"></span>
            </label> 
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>' name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(1);}else{echo CallSemester(1);} ?>" > 1
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(2);}else{echo CallSemester(2);} ?>"> 2
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(3);}else{echo CallSemester(3);} ?>"> 3
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(4);}else{echo CallSemester(4);} ?>"> 4
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(5);}else{echo CallSemester(5);} ?>"> 5
            </div>
            <div class="col-sm-2">
            <input type="checkbox" class="yearOrSemester yearOrSemester<?php echo $key ?>" id='yearOrSemester_<?php echo $value->classesID ?>'  name="yearOrSemester<?php echo $value->classesID ?>[]" value="<?php if($value->mode==1){echo CallYears(6);}else{echo CallSemester(6);} ?>"> 6
            </div>
            </div>
            </div>
            <br>
            <?php } ?>
            <div>&nbsp;</div>
            <?php 
               if(form_error('document')) 
               
                   echo "<div class='form-group has-error' >";
               
               else     
               
                   echo "<div class='form-group' >";
               
               ?>
            <label for="document" class="col-sm-2 control-label">
            Upload Document
            </label>
            <div class="col-sm-6">
            <input type="file" name="userfile">
            </div>
            <span class="col-sm-4 control-label">
            <?php echo form_error('document'); ?>
            </span>
         </div>
         <div class="col-md-12" >
          <div class="col-md-6 pull-right">
            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
          </div>                               
         </div>
       
                                                                                
      </div>
      </form>


			 <!--  <form class="form-horizontal" role="form" method="post">
                <input type="hidden" class="form-control" placeholder="Notice Title" name="usertype[]" value="Student"> -->
                                     <!-- <label for="title" class="col-sm-1 control-label">

                                           Course

                                        </label> -->
                           <!--          
                            <?php 

                                if(form_error('classesID')) 

                                    echo "<div class='form-group has-error' >";

                                else     

                                    echo "<div class='form-group' >";

                            ?>

                        <label for="classesID" class="col-sm-2 control-label">

                           Course*

                        </label>

                        <div class="col-sm-6">
                            
                            <?php

                                $array = array();


                                $array[0] = "Select Course";
                              

                                foreach ($classes as $classa) {

                                    $array[$classa->classesID] = $classa->classes;

                                }
                                
                                echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control' onchange = 'CourseSDependent1($(this).val())' value='Select Course' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classesID'); ?>

                        </span>

                    </div>

                    <div id="subCourseID">
                    </div>      

                    <div id="fetchYearsAndSem">
                        
                    </div>

                    <div id="appendSujectsDetails">
                        
                    </div>
                
                                   <?php 

                                        if(form_error('title')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="title" class="col-sm-2 control-label">

                                            Notice Title

                                        </label>

                                        <div class="col-sm-6">

                                            <input type="text" class="form-control" placeholder="Notice Title" name="title" value="<?=set_value('title')?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('title'); ?>

                                        </span>

                                    </div>

  
                                    <?php 

                                        if(form_error('date')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="date" class="col-sm-2 control-label">

                                            <?=$this->lang->line("notice_date")?>

                                        </label>

                                        <div class="col-sm-6">

                                            <input type="" class="form-control datepicker_quiz_data" id="date" name="date" placeholder="Date" value="<?=set_value('date')?>" >

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('date'); ?>

                                        </span>

                                    </div>

                                    <?php 

                                        if(form_error('notice')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="notice" class="col-sm-2 control-label">

                                            Descriptions

                                        </label>

                                        <div class="col-sm-6">

                                            <textarea class="form-control" id="notice" placeholder="Desription" name="notice" ><?=set_value('notice')?></textarea>

                                        </div>

                                        <span class="col-sm-4 control-label">

                                            <?php echo form_error('notice'); ?>

                                        </span>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-8">
                                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >
                                        </div>
                                    </div>
                                </form> -->
			</div>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>

<script type="text/javascript">

jq('#date').datepicker();

jq('#notice').jqte();

</script>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>
<script type="text/javascript">
   jq('#date').datepicker();
   
   jq('#notice').jqte();
   
</script>
<script>
   function mail_send() {
       $.ajax({
   
          type: 'POST',
   
          url: "<?=base_url('notice/send_notice_email')?>",
   
          dataType: "html",
   
          success: function(data) {
   
              location.reload(); 
   
          }
   
      });
   
   }
</script>
<script type="text/javascript">
   $(document).ready(function() {
   $('#classesID').multiselect({
   nonSelectedText: 'Select Course'
   });
   });
</script>
<!-- <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
<link href="./jquery.multiselect.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="./jquery.multiselect.js"></script> -->
<script>
   $(function () {
       $('select[multiple].active.3col').multiselect({
           columns: 3,
           placeholder: 'Select States',
           search: true,
           searchOptions: {
               'default': 'Search States'
           },
           selectAll: true
       });
   
   });
</script>
<script type="text/javascript">
   function selectAll(){
      var items=document.getElementsByName('classesID[]');
      var items_sem=document.getElementsByClassName('yearOrSemester');
   
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=true;
      }
      for(var i=0; i<items_sem.length; i++){
          if(items_sem[i].type=='checkbox')
              items_sem[i].checked=true;
      }
   }
   
   function select_course(val,key){
   
      var items_sem=document.getElementsByClassName('yearOrSemester'+key);
   
      if(val.checked){
   
          $('.yearOrSemester'+key).each(function(){
              this.checked = true;
          });
      }else{
           $('.yearOrSemester'+key).each(function(){
              this.checked = false;
              $(".etsfilertButton").addClass("disabled");
          });
      }                
   
   }
   
   function selectAllsemester(){
      var items=document.getElementsByName('classesID[]');
      var items_sem=document.getElementsByClassName('yearOrSemester');
      
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=false;
      }
      for(var i=0; i<items_sem.length; i++){
          if(items_sem[i].type=='checkbox')
              items_sem[i].checked=false;
      }
   }
   
   function UnSelectAll(){
      var items=document.getElementsByName('classesID[]');
      for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox')
              items[i].checked=false;
      }
   }           
</script>
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script type="text/javascript">
   tinymce.init({
   
       selector:"textarea",
   
       width:"700",
   
       height:"200",
   
       relative_urls:false,
   
       remove_script_host:false,
   
       theme: "modern",
   
       plugins: [
   
           "advlist autolink lists link image charmap print preview hr anchor pagebreak",
   
           "searchreplace wordcount visualblocks visualchars code fullscreen",
   
           "insertdatetime media nonbreaking save table contextmenu directionality",
   
           "emoticons template paste textcolor colorpicker textpattern"
       ],
   
       toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager | fontsizeselect",
   
       toolbar2: "print preview media | forecolor backcolor emoticons",
   
       fontsize_formats: "1px 2px 3px 4px 5px 6px 7px 8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 32px 34px 36px",
   
         style_formats: [
   
               {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
   
               {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
   
               {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
   
               {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
   
               {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
   
               {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
   
               {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
   
               {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
   
               {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
   
               {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
   
               {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
   
               {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
   
               {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
   
           ],
   
       image_advtab: true,
   
       templates: [
   
           {title: 'Test template 1', description: 'Test 1'},
   
           {title: 'Test template 2', description: 'Test 2'}
       ],
   
      external_filemanager_path:"<?php echo base_url() ?>filemanager/",
   
      filemanager_title:"Responsive Filemanager" ,
   
      external_plugins: { "filemanager" : "<?php echo base_url() ?>filemanager/plugin.min.js"}
   
   });
   
</script>

