    <div class="">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-fa fa-bell-o"></i> <?=$this->lang->line('panel_title')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li><a href="<?=base_url("notice/index")?>"><?=$this->lang->line('menu_notice')?></a></li>
                <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_notice')?></li>
            </ol>
        </div>
    </div>

    <div class="container-fluid">
        <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
        <div class="row">
            <div class="">
                <div class="card">
                    <div class="card-body">                  

                        <div class="col-sm-12">

                            <form class="form-horizontal" role="form" method="post">
                            
                                    
                            <?php 

                                if(form_error('classesID')) 

                                    echo "<div class='form-group has-error' >";

                                else     

                                    echo "<div class='form-group' >";

                            ?>

                        <label for="classesID" class="col-sm-2 control-label">

                           Course

                        </label>

                        <div class="col-sm-6">
                            
                            <?php

                                $array = array();


                                $array[0] = "select Course";
                              

                                foreach ($classes as $classa) {

                                    $array[$classa->classesID] = $classa->classes;
                                }
                                
                                echo form_dropdown("classesID", $array, set_value("classesID",$notice->classesID), "id='classesID' class='form-control' onchange = 'CourseSDependent1($(this).val())' value='Select Course' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classesID'); ?>

                        </span>

                    </div>


                    <div id="subCourseID">
                            <?php 

                                if(form_error('subCourseID')) 

                                    echo "<div class='form-group has-error' >";

                                else     

                                    echo "<div class='form-group' >";

                            ?>

                        <label for="subCourseID" class="col-sm-2 control-label">

                          Sub Course

                        </label>

                        <div class="col-sm-6">
                            
                            <?php

                                $array = array();


                                $array[0] = "select Sub Course";
                              

                                foreach ($get_sub_courses as $get_sub_course) {

                                    $array[$get_sub_course->sub_coursesID] = $get_sub_course->sub_course;
                                }
                                
                                echo form_dropdown("sub_coursesID", $array, set_value("sub_coursesID",$notice->sub_coursesID), "id='sub_CourseID' class='form-control' required value='' ");

                            ?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('subCourseID'); ?>

                        </span>

                    </div>

                    </div>      

                    <div id="fetchYearsAndSem">
                        <?php 

                                if(form_error('subCourseID')) 

                                    echo "<div class='form-group has-error' >";

                                else     

                                    echo "<div class='form-group' >";

                            ?>

                        <label for="subCourseID" class="col-sm-2 control-label">

                           Year/Semester 

                        </label>

                        <div class="col-sm-6">
                            
                            <select class='form-control' name='yearsOrSemester' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('yearsOrSemester'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                        if ($classesRow->mode==1) {
                                            for ($i=1; $i <=$looping; $i++) {
                                                if (CallYears($i)==$notice->yearsOrSemester) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$notice->yearsOrSemester) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('subCourseID'); ?>

                        </span>

                    </div>     


                    </div>

                    <div id="appendSujectsDetails">
                        

                    </div>

                    

                        <?php 

                            if(form_error('title')) 

                                echo "<div class='form-group has-error' >";

                            else     

                                echo "<div class='form-group' >";

                        ?>

                        <label for="title" class="col-sm-2 control-label">

                            <?=$this->lang->line("notice_title")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" name="title" value="<?=set_value('title', $notice->title)?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('title'); ?>

                        </span>

                    </div>


<?php 
      if(form_error('type_notice')) 

                                            echo "<div class='form-group has-error' >";

                                        else     

                                            echo "<div class='form-group' >";

                                    ?>

                                        <label for="title" class="col-sm-2 control-label">

                                        Show Notice For

                                        </label>

                                        <div class="col-sm-9">

                                    <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type1" value="course" <?php if($notice->notice_type=='course'){ echo "checked"; } ?>><label for="notice_type1">Only Course</label>
                                    </div>
                                        <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type2" value="sub_course" <?php if($notice->notice_type=='sub_course'){ echo "checked"; } ?>><label for="notice_type2">Only Sub Course</label>
                                    </div>
                                        <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type3" value="with_year_semester" <?php if($notice->notice_type=='with_year_semester'){ echo "checked"; } ?>><label for="notice_type3">With Year/Semester</label>
                                    </div>
                                          <div class="col-sm-3">
                                     <input type="radio" required name="notice_type" id="notice_type4" value="only_year_semester" <?php if($notice->notice_type=='only_year_semester'){ echo "checked"; } ?>><label for="notice_type4">Only Year/Semester</label>
                                    </div>

                                        </div>

                                        <span class="col-sm-1 control-label">

                                            <?php echo form_error('type_notice'); ?>

                                        </span>

                                    </div>



                    <?php 

                        if(form_error('date')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="date" class="col-sm-2 control-label">

                            <?=$this->lang->line("notice_date")?>

                        </label>

                        <div class="col-sm-6">

                            <input type="" class="form-control datepicker_quiz_data" id="date" name="date" value="<?=set_value('date', date("d-m-Y", strtotime($notice->date)))?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('date'); ?>

                        </span>

                    </div>

                                      <?php 
                                        if(form_error('notice')) 
                                            echo "<div class='form-group has-error' >";
                                        else                                     echo "<div class='form-group' >";
                                    ?>

                                    <label for="notice" class="col-sm-2 control-label">

                                    <?=$this->lang->line("notice_notice")?>

                                    </label>

                                    <div class="col-sm-6">

                                        <textarea class="form-control" id="notice" name="notice" ><?=set_value('notice', $notice->notice)?></textarea>

                                    </div>

                                    <span class="col-sm-4 control-label">

                                        <?php echo form_error('notice'); ?>

                                    </span>

                                </div>

                                <div class="form-group">

                                    <div class="col-sm-offset-2 col-sm-8">

                                    <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_class")?>" >

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/editor/jquery-te-1.4.0.min.js'); ?>"></script>

<script type="text/javascript">

jq('#date').datepicker();

jq('#notice').jqte();

</script>

