  <div class="">

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="fa fa-list"></i> Attendance </h3>

        </div>

        <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Attendance</li>

            </ol>

        </div>

    </div>

   <!-- /.box-header -->



    <!-- form start -->

   <div class="container-fluid">

      <div class="row">         

        <div class="card">

            <div class="card-body">    



    <div class="box-body">

     

        <div class="row">

            <div class="col-sm-12">



                <div class="theme_input_blue">

            <?php 

            $usertype = $this->session->userdata("usertype");



                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin")

               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {

               $display =  "block";

               $addmorebutton = "none";

               $Removemorebutton = "inline-block";

               }else{

               $display = "none";

               $addmorebutton = "inline-block";

               $Removemorebutton = "none";

               }

               

               ?>

                

                <div class="col-sm-4">

                    

                </div>



                <div class="col-sm-4">

                    <div class="">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group sem-filter">

                                <label for="classesID" class="control-label">Semester/Year</label>



                                <div class="">

                                    <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'> 

                                    <option>Select</option>      

                                     <?php 

                                        if ($classesRow) {

                                       $looping    =  (int) $classesRow->duration;

                                       if ($classesRow->mode==1) {

                                          for ($i=1; $i <=$looping; $i++) {

                                                 if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {

                                            $select = 'Selected';

                                         }else{

                                         $select = '';

                                         }

                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

                                         

                                       }

                                    }



                                    else{

                                          for ($i=1; $i <=(2*$looping); $i++) {

                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {

                                            $select = 'Selected';

                                         }else{

                                         $select = '';

                                         }

                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

                                       }

                                       }

                                    }

                                    ?>

                                    </select>

                                </div>

                                <div class="clearfix"></div>

                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>

                </div>



                <div class="col-sm-4">

                    

                </div>

            </div>

            <div class="clearfix"></div>

            







                <?php



                    $usertype = $this->session->userdata("usertype");



                    if($usertype == "Student" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Admin") {

?>

              <div class="col-sm-4 list-group for-formGroup ">



                    <!-- <div class="list-group-item list-group-item-warning">



                        <form style="" class="form-horizontal" role="form" method="post">



                            <div class="form-group">



                                <label for="classesID" class="col-sm-3 control-label">



                                Semester/Year



                                </label>



                                <div class="col-sm-9">

                                       

                                <?php 



    if ($classesRow) {

     $looping    =  (int) $classesRow->duration;

   if ($classesRow->mode==1) {

     echo"<select class='form-control' name='yearID' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='set_value('yearId');'>

     <option value = 'all'>All</option>";

      for ($i=1; $i <=$looping; $i++) {

     if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {

        $select = 'Selected';

     }else{

     $select = '';

     }

     echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";

     

   }

   echo  "</select>";

}



else{

    

     echo"<select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'><option value = 'all'>All</option>";

      for ($i=1; $i <=(2*$looping); $i++) {



     if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {

        $select = 'Selected';

     }else{

     $select = '';

     }



     echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";

   }

   echo  "</select>";

   }

}



                                ?>



                                </div>

                                <div class="clearfix"></div>

                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>



                            </div>



                        </form>



                    </div> -->



                </div>

                <div class="clearfix"></div>



                <?php } ?>

                <div id="hide-table">



                    <table id="subjectsTablesStudentSyllabus" class="table table-striped table-bordered table-hover dataTable no-footer table-responsive">



                        <thead>



                            <tr>

                                <th><?=$this->lang->line('slno')?></th>

                                

                                <th><?=$this->lang->line('subject_name')?></th>                                

                                <th>Semester/Year</th>

                                <!-- <th>Syllabus Pdf</th> -->

                                <th>Attendance</th>

                            </tr>



                        </thead>







                        <tbody>



                            <?php 
                        
                            if(count($subjects)) {$i = 1; foreach($subjects as $subject) { ?>



                                <tr>

                                <td><?php echo $i++ ?></td>

                               <!--  <td data-title="<?=$this->lang->line('subject_code')?>">



                                        <?php echo $subject['subject_code']; ?>



                               </td> -->



                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php echo $subject['subject']; ?>

                                    </td>

                                    <td data-title="Semester/Year">

                                    <?php echo str_replace('_', ' ', $subject['yearsOrSemester']);?>

                                    </td>

                                    <!-- <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php
                                            if ($subject['pdf']!='') {?>

                                               <a href="<?php echo base_url()?>uploads/syllabus/<?php echo $subject['pdf']; ?>"> <img src="<?php echo base_url()?>"> <i class="fa fa-file-pdf-o fa-2x"></i></a> <a href="<?php echo base_url() ?>syllabus/view/<?php echo $subject['subjectID'] ?>"> View</a>
                                            <?php }else{  ?>
                                      
                                                No Pdf <a href="<?php echo base_url() ?>syllabus/view/<?php echo $subject['subjectID'] ?>"> View</a>
                                        <?php } ?>

                                    </td> -->

                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                       &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>attendance/attendance_view?date=<?php echo time() ?>&subjectID=<?php  echo $subject['subjectID'] ?>">View</a>&nbsp;&nbsp;&nbsp;
                                       <strong>
                                          <?php
                                            if ($subject['atd']->num_attd>0) {
                                             echo '&nbsp;&nbsp;('.$subject['atd']->present.'/'.$subject['atd']->num_attd.')&nbsp;&nbsp;&nbsp;';
                                             echo '('.number_format($subject['atd']->present*100/$subject['atd']->num_attd,2).'&nbsp;%)';
                                            }else{
                                               echo '(0/0)';
                                            }
                                          ?>  
                                          </strong>
                                    </td>

                                    

                                </tr>

                            <?php $i+1; }} ?>

                        </tbody>

                    </table>

                </div>


                



            </div>



        </div>



    </div>



</div>

</div>

</div>

</div>

</div>













<script type="text/javascript">



    $('#classesID').change(function() {



        var classesID = $(this).val();



        if(classesID == 0) {



            $('#hide-table').hide();



        } else {



            $.ajax({



                type: 'POST',



                url: "<?=base_url('subject/subject_list')?>",



                data: "id=" + classesID,



                dataType: "html",



                success: function(data) {



                    window.location.href = data;



                }



            });



        }



    });



</script>

<script type="text/javascript">



    $('#yearSemesterID').change(function() {



        var yearSemesterID = $(this).val();



            $.ajax({



                type: 'POST',



                url: "<?=base_url('subject/index')?>",



                data: "yearSemesterID=" + yearSemesterID,



                dataType: "html",



                success: function(data) {



                     location.reload();



                }



            });



    });



</script>



<script type="text/javascript">

        function ResetSemesterYear(){

            $.ajax({

                type: 'POST',



                url: "<?=base_url('subject/ResetSemesterYear')?>",



                data:{ResetSesession:'ResetSesession'},



                dataType: "html",



                success: function(data) {

                    location.reload();



                }



            });

}

</script>

