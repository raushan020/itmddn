

<div class="">


    <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="fa fa-book"></i> <?=$this->lang->line('panel_title')?> </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                  <li class="active"><?=$this->lang->line('menu_subject')?></li>
                </ol>
            </div>
    </div>


    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">
                <?php 
                $usertype = $this->session->userdata("usertype");
                 $SpecialUsertype = $this->session->userdata('SpecialUsertype');
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support"  || $usertype == "Academic" || $SpecialUsertype=='Btech' || $usertype == "Teacher") {
                ?>
         <div class="col-md-12 nopading">

            <div class="col-sm-6 nopading">
                <div class="advance-tab boredr_none">
                  <?php if($SpecialUsertype!='Btech'){ ?>
                   <ul class="nav nav-tabs" role="tablist">
                        <?php 
                        if ( empty($this->session->userdata('DraftSubjects')) && empty($this->session->userdata('TrashSubjects'))) {
                        $active = "active";
                        }else{
                         $active = "";
                         }
                        ?>
                      
                         <?php if($usertype != "Teacher")
                         {?>
                        <li class="<?php echo $active ?>">
                          <a aria-expanded="true" onclick="ActiveSubjects()"> Active <span style="font-size: 11px;">(<?php echo $ActiveSubjects_count; ?>)</span></a>
                        </li>
                        <li <?php if ($this->session->userdata('DraftSubjects')){ ?> class="active" <?php  } ?>>
                          <a  aria-expanded="false" onclick="DraftSubjects()">Draft <span style="font-size: 11px;">(<?php echo $DraftSubjects_count; ?>)</span></a>
                        </li>
                       <li <?php if ($this->session->userdata('TrashSubjects')){ ?> class="active"     <?php  } ?>>
                          <a aria-expanded="false" onclick="TrashSubjects()">Trash <span style="font-size: 11px;">(<?php echo $TrashSubjects_count; ?>)</span></a>
                        </li>
                    <?php }?>
                    </ul>
         <?php } ?>
                </div>
            </div>

            <div class="col-sm-6 nopading">
               <div class="pull-right">
                  <div class="btn-group">
                    <?php $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Academic" || $SpecialUsertype=='Btech') {
                    ?>                  
                    <a href="<?php echo base_url('subject/add') ?>" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span> Add & Edit </a>
              
                    <a onclick="ResetAllfilter_subject()" class="btn-top check-all btn bg-success pull-right"> <span class="glyphicon glyphicon-plus"></span>Reset All Filters</a>
                   <?php  } ?>
                  
                  </div>
               </div>
            </div>


            
         </div>
            <div class="col-sm-12">
            <div class="clearfix"></div>

         </div>
         <div class="clearfix"></div>
        <div class="col-sm-12">

            <div class="theme_input_blue">
            <?php 
               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
               $display =  "block";
               $addmorebutton = "none";
               $Removemorebutton = "inline-block";
               }else{
               $display = "none";
               $addmorebutton = "inline-block";
               $Removemorebutton = "none";
               }
               
               ?>
                
                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterSubjectclassesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label"> Sub Courses </label>

                                <div class="">
                                    <select id="subCourseID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                            if ($value->sub_coursesID==$this->session->userdata('FilterSubjectsubCourseID')) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php } ?> 

            <?php
             $SpecialUsertype = $this->session->userdata('SpecialUsertype');
                if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $SpecialUsertype=='Btech')
                {
                    if ($subjects>0) 
                    {
             ?>
                        <form action="<?php echo base_url() ?>subject/multipleAction" method = "post" onsubmit="return confirm('Do you really want to submit the form?');">
                            <div class="action-layout">
                                <ul>
                                    <li>
                                        <label class="nexCheckbox">Check
                                            <input type="checkbox" name="select_all" id="select_all">
                                            <span class="checkmark checkmark-action-layout"></span>
                                        </label>
                                    </li>
                                    <li class="active-btn">
                                        <input type="submit" class="btn btn-success etsfilertButton disabled"  name="Active" value="Active" disabled>
                                    </li>
                                    <li class="active-btn">
                                        <input type="submit" class="btn btn-info etsfilertButton disabled" name="Draft" value="Draft" disabled>
                                    </li>
                                    <li class="active-btn">
                                        <input type="submit" class="btn btn-danger etsfilertButton disabled" name="Delete" value="Delete" disabled>
                                    </li>
                                </ul>
                            </div> 
                            <table id="subjectsTables" class="table table-striped table-bordered table-hover dataTable no-footer">

                                <thead>

                                    <tr>

                                        <th><?=$this->lang->line('slno')?></th>

                                        <th>S no.</th>

                                        <th><?=$this->lang->line('subject_code')?></th>

                                        <th><?=$this->lang->line('subject_name')?></th>

                                        <th>Subject Credit</th>

                                        <th>Course Name</th>
                                        

                                        <th>Semester/Year</th>

                                        <th><?=$this->lang->line('action')?></th>

                                    </tr>

                                </thead>

                            </table>
                        </form>
        <?php 
                    } 
                else
                { 
        ?>
                    <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
                        <p>Maybe your search was too specific, please try searching with another term.</p>
                        <img src="<?php echo base_url() ?>uploads/images/crying.png">
                    </div>
        <?php 
                } 
            } 
            else if($usertype == "Academic")
            {
                if ($subjects>0) 
                {
        ?>
                    <table id="subjectsTablesAcademic" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th>S no.</th>

                                <th><?=$this->lang->line('subject_code')?></th>

                                <th><?=$this->lang->line('subject_name')?></th>

                                <th>Subject Credit</th>

                                <th>Course Name</th>

                                <th>Semester/Year</th>

                                <th><?=$this->lang->line('action')?></th>

                            </tr>

                        </thead>

                    </table>
        <?php  
                }
                else
                {
        ?>
                    <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
                        <p>Maybe your search was too specific, please try searching with another term.</p>
                        <img src="<?php echo base_url() ?>uploads/images/crying.png">
                    </div>
        <?php            
                }      
            }
            else if($usertype == "Teacher")
            {
                if ($subjects>0) 
                {
        ?>
                    <table id="subjectsTablesTeacher" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th><?=$this->lang->line('slno')?></th>

                                <th><?=$this->lang->line('subject_code')?></th>

                                <th><?=$this->lang->line('subject_name')?></th>

                                <th>Subject Credit</th>                                

                                <th>Semester/Year</th>
                                <th>Assignment</th>
                                <th>Syllabus</th>
                                <!-- <th>Subject Progress</th> -->

                            </tr>

                        </thead>
                        <tbody>



                            <?php 
                        // print_r($subjects);die;
                            if(count($subjects)) {$i = 1; foreach($subjects as $subject) { ?>



                                <tr>

                                <td><?php echo $i++ ?></td>

                                <td data-title="<?=$this->lang->line('subject_code')?>">



                                        <?php echo $subject['subject_code']; ?>



                               </td>



                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php echo $subject['subject']; ?>

                                    </td>

                                    <td>

                                        <?php echo $subject['subject_credit']; ?>

                                    </td>

                                    <td data-title="Semester/Year">

                                    <?php echo str_replace('_', ' ', $subject['yearsOrSemester']);?>

                                    </td>

                                    <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php
                                            if ($subject['notesPdf']!='') {?>

                                               <a href="<?php echo base_url()?>uploads/syllabus/<?php echo $subject['notesPdf']; ?>" target="_blank"><!-- <img src="<?php echo base_url()?>"> --><i class="fa fa-file-pdf-o fa-2x"></i>&nbsp;&nbsp; Download</a> <!-- <a href="<?php echo base_url() ?>syllabus/view/<?php echo $subject['subjectID'] ?>"> View</a> -->
                                            <?php }else{  ?>
                                      
                                                Not Available
                                        <?php } ?>

                                    </td>
                                 <td data-title="<?=$this->lang->line('subject_name')?>">

                                        <?php
                                            if ($subject['sylabusPdf']!='') {?>

                                               <a href="<?php echo base_url()?>uploads/syllabus/<?php echo $subject['sylabusPdf']; ?>" target="_blank"><!-- <img src="<?php echo base_url()?>"> --><i class="fa fa-file-pdf-o fa-2x"></i>&nbsp;&nbsp; Download</a> <!-- <a href="<?php echo base_url() ?>syllabus/view/<?php echo $subject['subjectID'] ?>"> View</a> -->
                                            <?php }else{  ?>
                                      
                                                Not Available
                                        <?php } ?>

                                    </td>

                                   
                                </tr>

                            <?php $i+1; }} ?>

                        </tbody>

                    </table>
        <?php  
                }
                else
                {
        ?>
                    <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
                        <p>Maybe your search was too specific, please try searching with another term.</p>
                        <img src="<?php echo base_url() ?>uploads/images/crying.png">
                    </div>
        <?php            
                }      
            }
            else
            { 
        ?> 
                <table id="subjectsTables_views" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th>S no.</th>

                                <th><?=$this->lang->line('subject_code')?></th>

                                <th><?=$this->lang->line('subject_name')?></th>

                                <th>Subject Credit</th>

                                <th>Course Name</th>
                                

                                <th>Semester/Year</th>

                            </tr>

                        </thead>

                    </table>
        <?php 
            } 
        ?>

            </div> <!-- col-sm-12 -->

            

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->

<script type="text/javascript">

    $('#classesID').change(function() {

$('#subjectsTables').DataTable().state.clear();

        var classesID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>


<script type="text/javascript">

    $('#subCourseID').change(function() {

$('#subjectsTables').DataTable().state.clear();

        var subCourseID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">



    $('#yearSemesterID').change(function() {

$('#subjectsTables').DataTable().state.clear();

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>
<script type="text/javascript">
    
    function ResetCourses(){
        $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
        $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
        $('#subjectsTables').DataTable().state.clear();
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter_subject(){
    $('#subjectsTables').DataTable().state.clear();
           
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                   
                    location.reload();

                }

            });
}



</script>
