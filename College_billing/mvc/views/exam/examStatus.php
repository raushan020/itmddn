       <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Exam Status</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Exam Status </li>
            </ol>
          </div>
        </div>
        <div class="container-fluid">
          
        <?php

        $usertype = $this->session->userdata("usertype");

        if($usertype == "Admin" || $usertype == "superadmin" || $usertype == "ClgAdmin") {

        ?>
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <!-- <a href="<?=base_url('')?>"> -->
              <div class="widget smart-standard-widget">
                <div class="row">
                  <div class="widget-caption info">
                    <div class="col-xs-4 no-pad">
                      <i class="icon fa fa-graduation-cap"></i>
                    </div>
                    <div class="col-xs-8 no-pad">
                      <div class="widget-detail student-back">
                        <h3 class="cl-info"> <?=$totalResult?></h3>
                        <span>Total Exam</span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </a> -->
            </div>
            
            <div class="col-md-4 col-sm-6">
           <!--  <a href="<?=base_url('exam')?>"> -->
              <div class="widget smart-standard-widget">
                <div class="row">
                  <div class="widget-caption danger">
                    <div class="col-xs-4 no-pad">
                      <i class="icon ti ti-id-badge"></i>
                    </div>
                    <div class="col-xs-8 no-pad">
                      <div class="widget-detail teacher-back">
                        <h3 class="cl-danger"> <?=$OpenResult ?></h3>
                        <span>Current Exam</span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="widget-line bg-danger">
                        <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </a> -->
            </div>
            
            <div class="col-md-4 col-sm-6">
              <!-- <a href="<?=base_url('exam')?>"> -->
              <div class="widget smart-standard-widget">
                <div class="row">
                  <div class="widget-caption warning">
                    <div class="col-xs-4 no-pad">
                      <i class="icon fa fa-book" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8 no-pad">
                      <div class="widget-detail course-back">
                        <h3 class="cl-warning"><?= $CompResult ?></h3>
                        <span>Complete Exam</span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </a> -->
            </div>         
            
            
          </div>
          <!-- /row -->


          <!-- New Row -->
       
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <!-- <a href="<?=base_url('')?>"> -->
              <div class="widget smart-standard-widget">
                <div class="row">
                  <div class="widget-caption info">
                    <div class="col-xs-4 no-pad">
                      <i class="icon fa fa-graduation-cap"></i>
                    </div>
                    <div class="col-xs-8 no-pad">
                      <div class="widget-detail student-back">
                        <h3 class="cl-info"><?= $totalStu ?></h3>
                        <span>Total Student</span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="widget-line bg-info">
                        <span style="width:48%;" class="bg-info widget-horigental-line"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </a> -->
            </div>
            
            
            <div class="col-md-4 col-sm-6">
           <!--  <a href="<?=base_url('exam')?>"> -->
              <div class="widget smart-standard-widget">
                <div class="row">
                  <div class="widget-caption danger">
                    <div class="col-xs-4 no-pad">
                      <i class="icon ti ti-id-badge"></i>
                    </div>
                    <div class="col-xs-8 no-pad">
                      <div class="widget-detail teacher-back">
                        <h3 class="cl-danger"><?= $totalOpenStu ?></h3>
                        <span>Total Open Student</span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="widget-line bg-danger">
                        <span style="width:70%;" class="bg-danger widget-horigental-line"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </a> -->
            </div>

            
            <div class="col-md-4 col-sm-6">
              <!-- <a href="<?=base_url('exam')?>"> -->
              <div class="widget smart-standard-widget">
                <div class="row">
                  <div class="widget-caption warning">
                    <div class="col-xs-4 no-pad">
                      <i class="icon fa fa-book" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-8 no-pad">
                      <div class="widget-detail course-back">
                        <h3 class="cl-warning"><?= $totalCompStu ?></h3>
                        <span>Total Complete Student</span>
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="widget-line bg-warning">
                        <span style="width:55%;" class="bg-warning widget-horigental-line"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- </a> -->
            </div>


            
            
          </div>
          <!-- /row -->



        <?php } ?>


        </div>

        




  <script type="text/javascript">
   $('#fpaidfilter').click(function() {
       var payment_status = 2;
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('student/index')?>",
   
               data: "payment_status=" + payment_status,
   
               dataType: "html",
   
               success: function(data) {
   
                  window.location.href=base_url+"student"; 
   
               }
   
           });
   
   });

     $('#ppaidfilter').click(function() {
       var payment_status = 1;
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('student/index')?>",
   
               data: "payment_status=" + payment_status,
   
               dataType: "html",
   
               success: function(data) {
   
                   window.location.href=base_url+"student";
   
               }
   
           });
   
   });
     $('#npaidfilter').click(function() {
       var payment_status = 3;
   
           $.ajax({
   
               type: 'POST',
   
               url: "<?=base_url('student/index')?>",
   
               data: "payment_status=" + payment_status,
   
               dataType: "html",
   
               success: function(data) {
   
                   window.location.href=base_url+"student";
   
               }
   
           });
   
   });
  </script>  
  <style type="text/css">
  .fpaid{
    cursor:pointer;
  }
  </style>
  <style type="text/css">
  .font-icon-shortcut{
    color: #e20b0b;
  }
  .title-font-size{
    font-size: 16px;
  }
  .widget-shortcut-height{
    height: 50px;
  }
  </style>