
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor"><i class="fa fa-pencil"></i> <?=$this->lang->line('panel_title')?> </h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

          <li><a href="<?=base_url("exam/index")?>"><?=$this->lang->line('menu_exam')?></a></li>

          <li class="active">Edit</li>
        </ol>
      </div>
    </div>

    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <button class="btn btn-info" id="back" onclick="goBack()" style="margin-bottom: 10px;">BACK</button>
      <div class="row">
        <div class="card">
          <div class="card-body">
            <div class="col-sm-9">
              <p>Field are required with <span class="red-color">*</span></p>
              <h3 class="border_heading">Exam Information</h3>
              <form class="form-horizontal" role="form" method="post">
                 <div class="col-md-11">
                  <div class="col-md-6">
                    <div class="form-group">
                    <label>Select Exam Type<span class="red-color">*</span></label>
                    <select name="exam_for" required  class="form-control" id="exam_for" value="<?php echo set_value('classesID'); ?>">
                        <option value="" selected>Select</option>
                  <option value="1" <?php if($exam->exam_for==1){echo "selected";} ?>>Subject</option>
                  <option value="2" <?php if($exam->exam_for==2){echo "selected";} ?>>Unit</option>
                        </select>
                    </div>
                  </div>
                    <div class="">
                        <div class="panel-body">
                          <?php 
                            if(form_error('classesID'))
                              echo "<div class='form-group has-error' >";
                            else     
                              echo "<div class='form-group' >";
                          ?>
                          <label for="classesID" class="">Course<span class="red-color">*</span></label>
                      
                        <select name="classesID" required onchange="ajaxGet_subCourses($(this).val())" id="classesID" class="form-control" value="<?php echo set_value('classesID'); ?>">
                        <option value="">Select Course</option>
                        <?php foreach($classes as $courses){ 
                            if ($courses->classesID==$exam->classesID) {
                            $selected = "selected";
                            }else{
                             $selected = "";
                            }

                            ?>

                          <option value="<?php echo $courses->classesID ?>" <?php echo $selected ?> ><?php echo $courses->classes ?> </option>
                          <?php } ?>
                        </select>                       
                      
                    <span class="col-sm-4 showLoaderSubcour">
                        <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                    </span>
                        <span class="col-sm-4 control-label">
                          <?php echo form_error('classesID'); ?>
                        </span>
                  </div>
                  <div id="subCourseID">
                            

                             <?php 
                if (count($get_sub_courses)>0) {
              if(form_error('sub_coursesID')) 

                echo "<div class='form-group has-error' >";

              else     

                echo "<div class='form-group' >";

            ?>
            <label for="classesID" class="">Sub Course<span class="red-color">*</span></label>
                      
                        <select name="sub_coursesID" id ="sub_CourseID" required onchange="ajaxGet_subCourses($(this).val())" id="classesID" class="form-control">
                        <option value="">Select Sub Course</option>
                        <?php foreach($get_sub_courses as $get_sub_course){
                    if ($get_sub_course->sub_coursesID==$exam->sub_coursesID) {
                            $selected = "selected";
                            }else{
                             $selected = "";
                            }
                         ?>
                          <option value="<?php echo $get_sub_course->sub_coursesID ?>" <?php echo $selected ?>><?php echo $get_sub_course->sub_course ?> </option>
                          <?php } ?>
                        </select>                       
                      
                    <span class="col-sm-4 showLoaderSubcour">
                        <img src="<?php echo  base_url() ?>uploads/images/loaderIcon.gif">
                    </span>
                        <span class="col-sm-4 control-label">
                          <?php echo form_error('sub_coursesID'); ?>
                        </span>
<?php } ?>

                  </div>


                  </div>    
                  <div id="yearAndSemester">
                    
<?php  if ($classes_single->mode==1) { ?>
<div class="form-group">
<label>Semester/Year</label>
 <select class='form-control' name='yearsOrSemester' onchange='Getsubjects($(this).val())' id='semester_idSelect' value='set_value('yearId')>
    <option value="">Year</option>
     <?php  for ($i=1; $i <=$classes_single->duration; $i++) {  ?>
     <option value="<?php echo CallYears($i) ?>" <?php if(CallYears($i)==$exam->yearsOrSemester){echo "selected";} ?> > <?php echo str_replace('_', ' ', CallYears($i) ) ?></option>
<?php    } ?>
    </select>
   </div>
<?php  } else { ?>
<div class="form-group">
     <label>Semester/Year</label>
     <select class='form-control' name='yearsOrSemester' onchange='Getsubjects($(this).val())' id='semester_idSelect' value='<?php echo set_value('yearsOrSemester'); ?>'>
     <option value=''>Semester</option>";
      <?php  for ($i=1; $i <=(2*$classes_single->duration); $i++) { ?>
     <option value="<?php echo CallSemester($i) ?>" <?php if(CallSemester($i)==$exam->yearsOrSemester){echo "selected";} ?>><?php echo  str_replace('_', ' ', CallSemester($i) ) ?></option>
   <?php  } ?>
   </select>
</div>
 <?php } ?>
                  </div>
                   <div id="subjectID">
           
               <?php 

              if(form_error('subjectID')) 

                echo "<div class='form-group has-error' >";

              else     

                echo "<div class='form-group' >";

              ?>
            <label for="classesID" class="control-label">Subject<span class="red-color">*</span></label>
          
                        <select name="subjectID" id ="subject_id" required onchange="AddQuizName($(this).val())" class="form-control">
                        <option value="">Select Subject</option>
                        <?php foreach($subject as $subjects){ 
                     if ($subjects->subjectID==$exam->subjectID) {
                            $selected = "selected";
                            }else{
                             $selected = "";
                            }
                            ?>
                          <option value="<?php echo $subjects->subjectID ?>" <?php echo $selected ?> ><?php echo $subjects->subject ?> </option>
                          <?php } ?>
                        </select>                       
                        <span class="col-sm-4 control-label">
                          <?php echo form_error('subjectID'); ?>
                        </span>
                

</div>
                  </div>
            <div class="form-group">     
               <label for="quiz_name" class="">Exam Name<span class="red-color">*</span></label> 
               <input type="text"  name="quiz_name" value="<?php echo $exam->quiz_name ?>" id="quizID" class="form-control" placeholder="Quiz Name"  required autofocus>
            </div>
            <div class="form-group">     
               <label for="">Description</label> 
               <textarea   name="description"  class="form-control tinymce_textarea" ><?php echo $exam->description ?></textarea>
            </div>
            <div class="form-group">     
               <label for="">Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )</label> 
               <input type="text" name="start_date"  value="<?php echo date("Y-m-d H:i:s" , $exam->start_date) ?>" class="form-control form-control datetimepicker_quiz_start" placeholder="Start Date (Exam can be attempted after this date. YYYY-MM-DD HH:II:SS )"   required >
            </div>
            <div class="form-group">     
            <label for="">End Date (Exam can be attempted before this date. eg. 2017-12-31 23:59:00 )</label> 
               <input type="text" name="end_date"  value="<?php echo date("Y-m-d H:i:s" , $exam->end_date) ?>" class="form-control form-control datetimepicker_quiz_start" placeholder="End Date (Exam can be attempted before this date. eg. 2017-12-31 23:59:00 )"   required >
            </div>
            <div class="form-group">     
               <label for=""  >Duration (in min.)</label> 
               <input type="text" name="duration"  value="<?php echo $exam->duration ?>" class="form-control" placeholder="Duration (in min.)"  required  >
            </div>

            <div class="form-group">     
               <label for=""  >Correct Score</label> 
               <input type="text" name="correct_score"  value="<?php echo $exam->correct_score ?>" class="form-control" placeholder="Correct Score"   required >
            </div>
            <div class="form-group">     
               <label for=""  >InCorrect Score</label> 
               <input type="text" name="incorrect_score"  value="<?php echo $exam->incorrect_score ?>" class="form-control" placeholder="InCorrect Score"  required  >
            </div>
            <div class="form-group">
              <label for="courseName" class="col-md-2 col-form-label">Exam Type</label>
              <div class="col-sm-4">
                <input type="radio" name="extype" id="extype1" value="1" <?php if($exam->extype==1){echo "checked";} ?> ><label for="extype1">Main</label>
              </div>
              <div class="col-sm-4">
                <input type="radio" name="extype" value="2" id="extype2" <?php if($exam->extype==2){echo "checked";} ?>><label for="extype2">Assignment</label>
              </div>
              <div class="clearfix"></div>
              
            </div>
            <button class="btn btn-success add-btn" type="submit">Edit</button>
         </div>
      </div>
   </div>
</form>

            </div>    

        </div>

    </div>

</div>



<script type="text/javascript">
$('#date').datepicker();
</script>

<script type="text/javascript">
  function Getsubjects(){
       var  classesID  = $("#classesID").val();
       var  sub_CourseID  = $("#sub_CourseID").val();
       var  semester_idSelect  = $("#semester_idSelect").val();
     $.ajax({
        type: "POST",
        url:"<?=base_url('exam/Getsubjects')?>",
        data:{"classesID":classesID,"sub_coursesID":sub_CourseID,"semester_idSelect":semester_idSelect},
        success: function(response) {
            $("#subjectID").html(response);
        }
    });
   }
</script>
<script type="text/javascript">
  function AddQuizName(val){
$("#quizID").val($("#subject_id option:selected").text());
  }
</script>
