<style>
  .allSignleFilter {
    width: 50%;
    margin: auto;
  }
  #left {
    float: left;
    width: 25%;
  }
  #right {
    float: right;
    width: 75%;
  }
  .le {
    float: left;
    width: 60%;
  }
  .ri {
    float: right;
    width: 40%;
  }
  .left {
    float: left;
    width: 30%;
  }
  .right {
    float: right;
    width: 70%;
  }
  #lefts {
    float: left;
    width: 30%;
  }
  #rights {
    float: right;
    width: 70%;
  }
</style>
<?php
  foreach($exams as $row)
  {
    $student_name = $row['name'];
    $roll = $row['roll'];
    $father_name = $row['father_name'];
    $mother_name = $row['mother_name'];
    $session = $row['session'];
    $classesID = $row['classesID'];
    $credit_earned = $row['credit_earned'];
    $total_subject = count($row['result']);
  }
?>
<div class="">
  <div class="row page-titles">
    <div class="col-md-5 align-self-center">
      <h3 class="text-themecolor"><i class="fa fa-flask"></i> Result</h3>
    </div>
    <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
        <li>
          <a href="<?=base_url("dashboard/index")?>">
            <i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?>
          </a>
        </li>
        <li class="active"> Result</li>
      </ol>
    </div>
  </div>
  <div class="container-fluid">
    <a href="javascript:void(0)" class="btn btn-success" style="margin-bottom: 10px;" onclick='printDiv();'>Print</a>
    <div class="row">
      <div class="card">
        <div class="card-body">
          <?php
            if($usertype == "Admin" || $usertype == "Teacher" || $usertype == "ClgAdmin" || $usertype == "Academic" || $usertype == "Support")
            {
          ?>
              <div class="allSignleFilter">
                <div class="col-sm-12 theme_input_blue" style="margin-bottom: 5rem;">
                  <form style="" class="form-horizontal" role="form" method="post">
                    <div class="form-group">
                      <label for="classesID" class="">Semester/Year</label>
                      <div class="">
                        <?php 
                          if ($classes) 
                          {
                            $looping = (int) $classes->duration;
                            if ($classes->mode==1) 
                            {
                              echo"<select class='form-control' name='yearID' onchange='get_semester_for_result($(this).val())' value='set_value('yearId');'>
                                  <option value = 'all'>Select</option>";
                                  for ($i=1; $i <=$looping; $i++) 
                                  {
                                    if (CallYears($i)==$yearsOrSemester) 
                                    {
                                      $select = 'Selected';
                                    }
                                    else
                                    {
                                      $select = '';
                                    }
                                    echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                  }
                              echo  "</select>";
                            }
                            else
                            {
                              echo"<select class='form-control' name='semesterId' onchange='get_semester_for_result($(this).val())'  value='<?php echo set_value('semesterId'); ?>'><option value = 'all'>Select</option>";
                              for ($i=1; $i <=(2*$looping); $i++) 
                              {
                                if (CallSemester($i)==$yearsOrSemester) 
                                {
                                  $select = 'Selected';
                                }
                                else
                                {
                                  $select = '';
                                }
                                echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                              }
                              echo  "</select>";
                            }
                          }
                        ?>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </form>
                </div>
              </div>
          <?php
            }
          ?>
          <div class="col-xs-12 col-sm-12" style="color: #000;" id="print_wrapp">
            <div class="col-xs-12">
              <div class="col-xs-2 text-left">
                <img src="<?php echo base_url('uploads/images/Lingayas Vidyapeeth Logo.png'); ?>" style="width: 150px;height: 150px;border: 1px solid #8080804a;">
              </div>
              <div class="col-xs-7 text-center">
                <h2><b>Lingaya's Vidyapeeth, Faridabad</b></h2>
                <p>(Formerly Lingaya's University)</p>
                <p>(Deemed to be University, Approved t1i5 3 of UGC Act 1950)</p>
                <h3><b>GRADE CARD</b></h3>
              </div>
              <div class="col-xs-3 text-right">
                <!-- <h4><b>Grade Card No:</b></h4> -->
                <!-- <p style="font-family: Algerian, fantasy;"><?php echo $roll; ?></p> -->
              </div>
            </div>
            <div class="col-xs-12">
              <div class="col-xs-6"  style="line-height: 3rem;">
                <?php
                  $result = $this->db->select('classes,departmentID')->from('classes')->where('classesID',$classesID)->get()->row();
                ?>
                <p>
                  <div id="left">Program</div>
                  <div id="right">: <?php echo $result->classes; ?></div>
                </p>
                <?php
                  $roww = $this->db->select('department_name')->from('department')->where('departmentID',$result->departmentID)->get()->row();
                ?>
                <div id="left">Stream</div>
                <div id="right">: <?php echo $roww->department_name; ?></div>
                <div id="left">Name</div>
                <div id="right">: <?php echo $student_name; ?></div>
                <div id="left">Father's Name</div>
                <div id="right">: <?php echo $father_name; ?></div>
                <div id="left">Mother's Name</div>
                <div id="right">: <?php echo $mother_name; ?></div>
              </div>
              <div class="col-xs-6" style="line-height: 3rem;">
                <div id="left">Regn. No.</div> 
                <div id="right">: <?php echo $roll; ?></div>
                <div id="left">Session</div>
                <div id="right">: <?php echo $session; ?></div>
                <div id="left">Semester</div>
                <div id="right">: <?php echo str_replace("_"," ",$yearsOrSemester); ?></div>
              </div>
            </div>
            <div class="col-xs-12">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>S. No.</th>
                    <th>Course No.</th>
                    <th>Course</th>
                    <th>Credits</th>
                    <th>Grade Obtained</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $i=1;
                    $total_credit = 0;
                    $grade_point_cgpa = 0;
                    $sgpa_p = 0;
                    foreach($exams as $fetch)
                    {
                      foreach ($fetch['result'] as $show) 
                      {
                        $sql = $this->db->select('subject,subject_code,subject_credit')->from('subject')->where('classesID',$classesID)->where('subject_code',$show['subject_code'])->group_by('subject_code')->get()->result_array();
                        if(empty($sql))
                        {
                          $total_credit = 0;
                          $point = 0;
                          $sgpa_p = 0;
                          $grade_point_cgpa = 0;
                          $CGPA = 0;
                        }
                        else
                        {
                          foreach ($sql as $key => $value) 
                          {

                            $total_credit +=$value['subject_credit'];
                            $grade_point = $this->db->select('point')->from('grade')->where('grade',$show['Grade'])->get()->row();
                            $point = $grade_point->point;

                            $sgpa_point = $value['subject_credit']*$point;
                            $sgpa_p += $sgpa_point; 

                            $grade_point_cgpa +=$grade_point->point;
                            $CGPA = $grade_point_cgpa/$total_subject;
                            echo "<tr>
                                  <td>".$i."</td>
                                  <td>".$value['subject_code']."</td>
                                  <td>".$value['subject']."</td>
                                  <td>".$value['subject_credit']."</td>
                                  <td>".$show['Grade']."</td>  
                                </tr>";
                            $i++;
                          }
                        }
                      }
                    }
                  ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="text-align: right;">TOTAL CREDITS</td>
                    <td><?php echo $total_credit; ?></td>
                    <td>SGPA : <?php if($total_credit!=0){echo round($sgpa_p/$total_credit,2);}else{ echo "0"; } ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="text-align: right;">CREDITS EARNED</td>
                    <td><?php echo $credit_earned; ?></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-xs-5">
              <div class="left">TOTAL CREDITS</div>
              <div class="right">: <?php echo $total_credit; ?></div>
              <div class="left">Date of Issue</div>
              <div class="right">: <?php echo date('d F, Y', strtotime(date('d-m-Y'))); ?></div>
            </div>
            <div class="col-xs-5">
              <div class="le">CREDITS EARNED (CUMULATIVE)</div>
              <div class="ri">: <?php echo $credit_earned; ?></div>
            </div>
            <div class="col-xs-2">
              <div id="lefts">CGPA</div>
              <div id="rights">: <?php echo round($CGPA, 2);; ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function get_semester_for_result(val){
    var rid = "<?php echo $this->uri->segment(3) ?>";
    window.location.href=base_url+"mark/view/"+rid+"?yearsOrSemester="+val;
  }
  function printDiv() 
  {
    var divToPrint=document.getElementById('print_wrapp');
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write('<html><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet"/><link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/prints.css" rel="stylesheet"/><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
  }
</script>