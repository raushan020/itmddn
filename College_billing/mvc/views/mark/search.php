
<div class="">


    <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="fa fa-flask"></i> Result </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                  <li class="active">Result</li>
                </ol>
            </div>
    </div>


    <!-- /.box-header -->

    <!-- form start -->

    <div class="container-fluid">
      <div class="row">
            <div class="card">
            <div class="card-body">

         
                   
                 
          
            <div class="col-sm-12">

                <?php 

                    $usertype = $this->session->userdata("usertype");

                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support" || $usertype == 'Academic' ) {

                ?>
            <div class="clearfix"></div>

         </div>
         <div class="clearfix"></div>
        <div class="col-sm-12">

            <div class="theme_input_blue">
            <?php 
               if ($this->session->userdata('sessionType') || $this->session->userdata('examType') || $this->session->userdata('education_mode') || $this->session->userdata('teacherID')) {
               $display =  "block";
               $addmorebutton = "none";
               $Removemorebutton = "inline-block";
               }else{
               $display = "none";
               $addmorebutton = "inline-block";
               $Removemorebutton = "none";
               }
               
               ?>
                
                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">
                                    Course
                                </label>
                                <div class="">
                                    <?php
                                        $array = array("0" => "Select Course");
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterMarkclassesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");
                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label"> Sub Courses </label>

                                <div class="">
                                    <select id="subCourseID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                            if ($value->sub_coursesID==$this->session->userdata('FilterMarksubCourseID')) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="control-label">Semester/Year</label>

                                <div class="">
                                    <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'> 
                                    <option>Select</option>      
                                     <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterMarkyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php } ?> 


            <?php
            if (count($subjects)>0) {
                
             ?>
            <form action="<?php echo base_url() ?>subject/multipleAction" method = "post">
            
            <table id="MarkTables" class="table table-striped table-bordered table-hover dataTable no-footer">

                        <thead>

                            <tr>

                                <th><?=$this->lang->line('slno')?></th>

                                <th>Student Name</th>

                                <th>Course Name</th>
                                
                                <th>Semester/Year</th>

                                <th><?=$this->lang->line('action')?></th>

                            </tr>

                        </thead>

                    </table>
                </form>
        <?php } else{ ?>
        <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
<p>Maybe your search was too specific, please try searching with another term.</p>
<img src="<?php echo base_url() ?>uploads/images/crying.png">
</div>
         <?php } ?>


            </div> <!-- col-sm-12 -->

            

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->


<!-- <iframe id='fp_embed_player' src='https://ec2-13-233-113-212.ap-south-1.compute.amazonaws.com:8888/embed_player?urlServer=wss://ec2-13-233-113-212.ap-south-1.compute.amazonaws.com:8443&streamName=stream&mediaProviders=WebRTC,Flash,MSE,WSPlayer' marginwidth='0' marginheight='0' frameborder='0' width='100%' height='100%' scrolling='no' allowfullscreen='allowfullscreen'></iframe> -->

<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('mark/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>


<script type="text/javascript">

    $('#subCourseID').change(function() {

        var subCourseID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('mark/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('mark/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>
<script type="text/javascript">
    
    function ResetCourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('mark/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('mark/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('mark/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){
           
            $.ajax({
                type: 'POST',

                url: "<?=base_url('mark/ResetAllfilter')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                   
                    location.reload();

                }

            });
}



</script>
