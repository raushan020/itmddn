<?php
    if ($this->input->get('yearsOrSemester')) {
    $yearsOrSemester = $this->input->get('yearsOrSemester');
    }else{
    $yearsOrSemester = "1st_Semester";
    }

 ?>
<style type="text/css">
    tr
    {mso-height-source:auto;}
col
    {mso-width-source:auto;}
br
    {mso-data-placement:same-cell;}
.style0
    {mso-number-format:General;
    text-align:general;
    vertical-align:bottom;
    white-space:nowrap;
    mso-rotate:0;
    mso-background-source:auto;
    mso-pattern:auto;
    color:black;
    font-size:11.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Calibri, sans-serif;
    mso-font-charset:0;
    border:none;
    mso-protection:locked visible;
    mso-style-name:Normal;
    mso-style-id:0;}
td
    {mso-style-parent:style0;
    padding-top:1px;
    padding-right:1px;
    padding-left:1px;
    mso-ignore:padding;
    color:black;
    font-size:11.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Calibri, sans-serif;
    mso-font-charset:0;
    mso-number-format:General;
    text-align:general;
    vertical-align:bottom;
    border:none;
    mso-background-source:auto;
    mso-pattern:auto;
    mso-protection:locked visible;
    white-space:nowrap;
    mso-rotate:0;}
.xl65
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;}
.xl66
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border:.5pt solid windowtext;}
.xl67
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:.5pt solid windowtext;
    border-right:.5pt solid windowtext;
    border-bottom:.5pt solid windowtext;
    border-left:none;}
.xl68
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:none;
    border-right:.5pt solid windowtext;
    border-bottom:.5pt solid windowtext;
    border-left:.5pt solid windowtext;
    background:#D8D8D8;
    mso-pattern:black none;}
.xl69
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:none;
    border-right:.5pt solid windowtext;
    border-bottom:.5pt solid windowtext;
    border-left:none;
    background:#D8D8D8;
    mso-pattern:black none;}
.xl70
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:none;
    border-right:.5pt solid windowtext;
    border-bottom:.5pt solid windowtext;
    border-left:.5pt solid windowtext;}
.xl71
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:none;
    border-right:.5pt solid windowtext;
    border-bottom:.5pt solid windowtext;
    border-left:none;}
.xl72
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border:.5pt solid windowtext;
    background:#D8D8D8;
    mso-pattern:black none;}
.xl73
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:.5pt solid windowtext;
    border-right:.5pt solid windowtext;
    border-bottom:.5pt solid windowtext;
    border-left:none;
    background:#D8D8D8;
    mso-pattern:black none;}
.xl74
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    background:white;
    mso-pattern:black none;}
.xl75
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:.5pt solid windowtext;
    border-right:none;
    border-bottom:.5pt solid windowtext;
    border-left:none;}
.xl76
    {mso-style-parent:style0;
    text-align:center;
    vertical-align:middle;
    border-top:.5pt solid windowtext;
    border-right:none;
    border-bottom:.5pt solid windowtext;
    border-left:.5pt solid windowtext;
  }

  #subject_theory{
    white-space: normal;
  }

</style>

    <style type="text/css">
      body{
        background-color: white;
      }
        * {box-sizing: border-box;}
        .result {
            border: 1px solid #333;
            max-width: 1140px;  
            display: block;
            margin: 0 auto;
        }
        .result .result-header{
            background:white;
            padding: 10px;
            border-top:white;
            margin: 0;
        }
        .result .result-header table tr td {
            padding: 2px 10px;
            text-align: left;
        } 
        .result .main-result {
            margin: 10px;
            border: 1px solid #333;
            padding: 20px 10px;
        }
        .result .main-result table tr th {
            vertical-align: top;
            padding-bottom: 5px;
            padding-left: 10px;
        }
        .result .main-result table tr td {
            padding: 5px 10px;
            text-align: center;
        }
        .result .result-footer {
            background: white;
            padding: 30px 10px;
            margin: 0;
            font-size: 15px;
        }
        .allSignleFilter{
            width:50%;
            margin:auto;
        }
    </style>
</head>
<body style="background-color: white !important">
    <?php
// header("Content-Type: application/xls");    
// header("Content-Disposition: attachment; filename=filename.xls");  
// header("Pragma: no-cache"); 
// header("Expires: 0");

     ?>
     <div class="">

      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-flask"></i> Result</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active"> Result</li>
          </ol>
        </div>
      </div>

        <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">
                  <div class="allSignleFilter">
                    <div class="col-sm-12 theme_input_blue">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="">
                                Semester/Year
                                </label>
                                <div class="">
                                
                                <?php 
    if ($classes) {
     $looping    =  (int) $classes->duration;
   if ($classes->mode==1) {
     echo"<select class='form-control' name='yearID' onchange='get_semester_for_result($(this).val())'  value='set_value('yearId');'>
     <option value = 'all'>Select</option>";
      for ($i=1; $i <=$looping; $i++) {
     if (CallYears($i)==$yearsOrSemester) {
        $select = 'Selected';
     }else{
     $select = '';
     }
     echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
     
   }
   echo  "</select>";
}

else{
     echo"<select class='form-control' name='semesterId' onchange='get_semester_for_result($(this).val())'  value='<?php echo set_value('semesterId'); ?>'><option value = 'all'>Select</option>";
      for ($i=1; $i <=(2*$looping); $i++) {

     if (CallSemester($i)==$yearsOrSemester) {
        $select = 'Selected';
     }else{
     $select = '';
     }

     echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
   }
   echo  "</select>";
   }
}

                    ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <div class="clearfix"></div>



<!-- new -->

<div class="row">
        <div class="card">
            <div class="card-body">
    <?php 
    if ($exams) {
  ?>
    <div class="result">
     <center> <img height="100px" width="100px" src="<?php echo base_url("uploads/images/9393_thumb.png"); ?>">
              <h2 style="line-height: 0.382;"><b>STATEMENT OF MARKS</b></h2>
                <p><b style="font-size: 18px; font-weight:700"><?php echo $classes->classes ?></b></p>
                <p><b><?php echo  str_replace('_', ' ', $exams->yearsOrSemester) ?></b></p>
            </center>
        <div class="row result-header">
            <div class="col-md-6">
              <!-- <img src="<?php echo base_url("uploads/images/9393_thumb.png"); ?>">
              <center><h1>STATEMENT OF MARKS</h1></center> -->
                <table>
                    <tr>
                        <td><strong>Roll No.</strong></td>
                        <td>:</td>
                        <td><?php echo $student->roll  ?></td>
                        <!-- <br>
                          <?php
if (isset($sub_course->sub_course)) {
  echo "with";
         echo $sub_course->sub_course ;} ?> -->
                    <!-- </tr> -->

                    <tr>
                        <td><strong>Student Name</strong></td>
                        <td>:</td>
                        <td><?php echo $student->name ?></td>
                    </tr>
                    <tr>
                        <td><strong>Session</strong></td>
                        <td>:</td>
                        <td><?php echo $student->session  ?></td>
                    </tr>
                    
                </table>
            </div>
            <div class="col-md-6">
                <table>
                    <tr>
                        <td><strong>Student ID</strong></td>
                        <td>:</td>
                        <td><?php echo $student->username ?></td>
                    </tr>
                    <tr>
                        <td><strong>Father's Name</strong></td>
                        <td>:</td>
                        <td><?php echo $student->father_name  ?></td>
                    </tr>
                    <tr>
                        <td><strong>Examination</strong></td>
                        <td>:</td>
                        <td>DEC-2018

                   
<!--                    <tr>
                        <td><strong>Name of Programme</strong></td>
                        <td>:</td>
                        <td><?php echo $classes->classes ?> -->
                        <!-- <br>
                          <?php
if (isset($sub_course->sub_course)) {
  echo "with";
         echo $sub_course->sub_course ;} ?> -->
       </td>
                    </tr>

                </table>
            </div>
        </div>
        <div class="row main-result">
            <div class="col-md-12">
                <table border=0 cellpadding=0 cellspacing=0 width=670 style='border-collapse:
                     collapse;table-layout:fixed;width:100%'>
                     <col class=xl65 width=78 style='mso-width-source:userset;mso-width-alt:2852;
                     width:59pt'>
                     <col class=xl65 width=89 style='mso-width-source:userset;mso-width-alt:3254;
                     width:67pt'>
                     <col class=xl65 width=110 style='mso-width-source:userset;mso-width-alt:4022;
                     width:83pt'>
                     <col class=xl65 width=106 style='mso-width-source:userset;mso-width-alt:3876;
                     width:80pt'>
                     <col class=xl65 width=46 style='mso-width-source:userset;mso-width-alt:1682;
                     width:35pt'>
                     <col class=xl65 width=56 style='mso-width-source:userset;mso-width-alt:2048;
                     width:42pt'>
                     <col class=xl65 width=58 style='mso-width-source:userset;mso-width-alt:2121;
                     width:44pt'>
                     <col class=xl65 width=38 style='mso-width-source:userset;mso-width-alt:1389;
                     width:29pt'>
                     <col class=xl65 width=45 style='mso-width-source:userset;mso-width-alt:1645;
                     width:34pt'>
                     <col class=xl65 width=44 style='mso-width-source:userset;mso-width-alt:1609;
                     width:33pt'>
                     <col class=xl76 width=44 style='mso-width-source:userset;mso-width-alt:1609;
                     width:5pt;'>
                     <tr height=20 style='height:15.0pt'>
                          <td height=20 class=xl66 width=78 style='height:15.0pt;width:59pt'>&nbsp;</td>
                          <td class=xl67 width=89 style='width:67pt'>&nbsp;</td>
                          <td colspan=3 class=xl76 width=262 style='border-right:.5pt solid black;
                          border-left:none;width:198pt;text-align: center'>Maximum Marks</td>
                          <td colspan=3 class=xl76 width=262 style='border-right:.5pt solid black;
                          border-left:none;width:198px'>Marks Obtained</td>
                          <td class=xl67 width=45 style='width:34pt'>Credit</td>
                          <td class=xl67 width=44 style='width:33pt'>Grade</td>
                     </tr>
                     <tr height=20 style='height:15.0pt'>
                          <td height=20 class=xl68 style='height:15.0pt'>Paper Code</td>
                          <td class=xl69>Theory Paper</td>
                          <td class=xl69>Internal</td>
                          <td class=xl69>External</td>
                          <td class=xl69>Total</td>
                          <td class=xl69>Internal</td>
                          <td class=xl69>External</td>
                          <td class=xl69>Total</td>
                          <td class=xl69>-</td>
                          <td class=xl69>-</td>
                     </tr>
                     <?php
                     $total_mark = 0;
                     $markobtain =0;
                      foreach ($marks as $key => $value) { 
                        $total_mark += $value->External_Marks_Max+$value->Internal_Marks_Max;
                        $markobtain += $value->Iternal_marks+$value->Exteral_Marks; 
                        ?>
                     <tr height=20 style='height:15.0pt'>
                          <td height=20 class=xl70 style='height:15.0pt'><?php echo $value->subject_code; ?></td>

                          <td class=xl71 id="subject_theory"><?php echo $value->subject; ?></td>
                          <td class=xl71><?php echo $value->Internal_Marks_Max; ?></td>
                          <td class=xl71><?php echo $value->External_Marks_Max; ?></td>
                          <td class=xl71><?php echo $value->External_Marks_Max+$value->Internal_Marks_Max; ?></td>
                          <td class=xl71><?php echo $value->Iternal_marks; ?></td>
                          <td class=xl71><?php echo $value->Exteral_Marks; ?></td>
                          <td class=xl71><?php echo $value->Iternal_marks+$value->Exteral_Marks; ?></td>
                          <td class=xl71><?php echo $value->Credit; ?></td>
                          <td class=xl71><?php echo $value->Grade; ?></td>
                     </tr>
                    <?php } ?>
                     </table>
                     <br>
                    <table border=0 cellpadding=0 cellspacing=0 width=670 style='border-collapse:
                     collapse;table-layout:fixed;width:100%;'> 
                     <tr height=20 style='height:15.0pt'>
                          <td height=20 class=xl72 style='height:15.0pt;width:16.6%;'>Paper Code</td>
                          <td class=xl73 style="width:16.6%">Practical Paper</td>
                          <td class=xl73 style="width:16.6%">Maximum Marks</td>
                          <td class=xl73 style="width:16.6%">Marks Obtained</td>
                          <td class=xl73 style="width:16.6%">Credit</td>
                          <td class=xl72 style='border-left:none;width:16.6%'>Grade</td>
                          <td colspan=4 class=xl65 style='mso-ignore:colspan;'></td>
                     </tr>
                      <?php foreach ($get_mark_practical as $key => $value) {
                                               $total_mark += $value->External_Marks_Max+$value->Internal_Marks_Max;
                        $markobtain += $value->Iternal_marks+$value->Exteral_Marks;  
                       ?>
                     <tr height=20 style='height:15.0pt'>
                     
                          <td height=20 class=xl70 style='height:15.0pt'><?php echo $value->subject_code ?></td>
                          <td class=xl71 id="subject_theory"><?php echo $value->subject ?></td>
                          <td class=xl71><?php echo $value->External_Marks_Max+$value->Internal_Marks_Max; ?></td>
                          <td class=xl71><?php echo $value->Iternal_marks+$value->Exteral_Marks; ?></td>
                          <td class=xl71><?php echo $value->Credit; ?></td>
                          <td class=xl70 style='border-left:none'><?php echo $value->Grade; ?></td>
                          <td colspan=4 class=xl65 style='mso-ignore:colspan'></td>
                     </tr>
                   <?php } ?>
                   </table>
                   <br>
                   <table border=0 cellpadding=0 cellspacing=0 width=670 style='border-collapse:
                     collapse;table-layout:fixed;width:100%;'>
                     
                     <tr class=xl74 height=20 style='height:15.0pt'>
                          <td height=20 class=xl72 style='height:15.0pt;width:20%;'>Total Marks</td>
                          <td class=xl73 style="width:20%;">Marks Obtained</td>
                          <td class=xl73 style="width:20%;">Percentage</td>
                          <td class=xl73 style="width:20%;">SGPA</td>
                          <td class=xl72 style='border-left:none;width:20%;'>Result</td>
                          
                     </tr>
                     <tr height=20 style='height:15.0pt'>
                          <td height=20 class=xl70 style='height:15.0pt'><?php echo $total_mark ?></td>
                          <td class=xl71><?php echo $markobtain ?></td>
                          <td class=xl71><?php 

                        $per   = $markobtain/$total_mark*100;
                        echo round($per).'%';
                           ?></td>
                          <td class=xl71>4</td>
                          <td class=xl71>Pass</td>
                          
                     </tr>
                     
                </table>



            </div>
        </div>
        <div class="row result-footer">

            <div class="col-md-12">
              <div class="col-md-4" style="line-height: 13px">
                <p><strong style="font-size: 13px;">Verified & Checked by</strong></p><span><b style="font-size: 13px;">Dated: Friday,August 16,2019</b></span>
              </div>
              <div class="col-md-5">
              </div>
              <div class="col-md-3">
                <p><strong style="font-size: 13px;">Controller of Examination</strong></p>
              </div>
              </div>
              <br>
              <br>
              <br>
                <p style="font-size:14px">Note: To Pass, a student required to obtain minimum 40% marks in individual subject.</p>
            
        </div>
    </div>

    
</div>
</div>
    <?php }  else { ?>
        <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
<p>Maybe your search was too specific, please try searching with another term.</p>
<img src="<?php echo base_url() ?>uploads/images/crying.png">
</div>
  <?php }  ?>
</body>
</html>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {

        var yearSemesterID = $(this).val();
            $.ajax({

                type: 'POST',

                url: "<?=base_url('mark/view')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">
        function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('mark/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
</script>

 <script src="<?php echo base_url('assets/inilabs/table2excel.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnExport").click(function () {
                $("#tblCustomers").table2excel({
                    filename: "Table.xls"
                });
            });
        });
    </script>
    <script type="text/javascript">
         function get_semester_for_result(val){

   var rid = "<?php echo $this->uri->segment(3) ?>";
   
   window.location.href=base_url+"mark/view/"+rid+"?yearsOrSemester="+val;
   
   }
    </script>
    