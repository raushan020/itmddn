function  EditSubCourse(val,column,SubcourseID,editValue){



  $(val).css("background","#FFF url("+base_url+"uploads/images/ajax-loader.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/EditSubCourse',

        data:{"val":editValue,"SubcourseID":SubcourseID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  }

//1. student 

$(document).ready(function(){  

      var dataTable = $('#example4').DataTable({

            dom: 'lBfrtip',
              scrollX:true,
              // sScrollY: "100%",
              "sScrollY": "500px",
              "sScrollCollapse": true,
              "paging": true,
              

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [                  
                   { extend: 'excelHtml5', text: 'Download Excel' }
                  
                   

              ],



          fixedColumns:   {

            leftColumns: 4,

            rightColumns:1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'student/ajaxStudents',  

                type:"POST"  

           }, 

      "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":2},

             { "orderable": false,"targets":7},

             { "orderable": false,"targets":18},

             { "orderable": false,"targets":19},

             { "orderable": false,"targets":20},

             { "orderable": false,"targets":21},

             { "orderable": false,"targets":22},

             { "orderable": false,"targets":23},

              ], 



      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "img" },

              { "data": "username" },

              { "data": "studentRoll" },

              { "data": "name" },              

              { "data": "father_name" },

              { "data": "yearsOrSemester" },

              { "data": "classes" },

              { "data": "counsellor" },

              { "data": "session" },

              { "data": "sessionType" },

              { "data": "student_status" },

              { "data": "phone" },

              { "data": "dob" },

              { "data": "sex" },

              { "data": "email" },

              { "data": "mother_name" },

              { "data": "aadhar" },

              { "data": "nationality" },

              { "data": "create_date" },

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "invoiceStatus" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });
//a/c
$(document).ready(function(){  

      var dataTable = $('#students_for_ac').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

              { extend: 'excelHtml5', text: 'Download Excel' }

              ],

          fixedColumns:   {

            leftColumns: 4,

            rightColumns:1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'student/ajaxStudents',  

                type:"POST"  

           }, 

      "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":2},

             { "orderable": false,"targets":7},

             { "orderable": false,"targets":18},

             { "orderable": false,"targets":19},

             { "orderable": false,"targets":20},

             { "orderable": false,"targets":21},

             { "orderable": false,"targets":22},

             { "orderable": false,"targets":23},

              ], 



      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "img" },

              { "data": "username" },

              { "data": "studentRoll" },

              { "data": "name" },              

              { "data": "father_name" },

              { "data": "yearsOrSemester" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "session" },

              { "data": "sessionType" },

              { "data": "student_status" },

              { "data": "phone" },

              { "data": "dob" },

              { "data": "sex" },

              { "data": "email" },

              { "data": "mother_name" },

              { "data": "aadhar" },

              { "data": "nationality" },

              { "data": "create_date" },

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "invoiceStatus" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });





$(document).ready(function(){  

      var dataTable = $('#teacherStudent').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [                  
                   { extend: 'excelHtml5', text: 'Download Excel' }
                  
                   

              ],

          fixedColumns:   {

            leftColumns: 4,

            rightColumns:1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'student/ajaxStudents',  

                type:"POST"  

           }, 

      "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":2},

             { "orderable": false,"targets":7},

             { "orderable": false,"targets":18},

             { "orderable": false,"targets":19},

             { "orderable": false,"targets":20},

             { "orderable": false,"targets":21},

             { "orderable": false,"targets":22},

             { "orderable": false,"targets":23},

              ], 



      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "img" },

              { "data": "username" },

              { "data": "studentRoll" },

              { "data": "name" },              

              { "data": "father_name" },

              { "data": "yearsOrSemester" },

              { "data": "classes" },

              { "data": "counsellor" },

              { "data": "session" },

              { "data": "sessionType" },

              { "data": "student_status" },

              { "data": "phone" },

              { "data": "dob" },

              { "data": "sex" },

              { "data": "email" },

              { "data": "mother_name" },

              { "data": "aadhar" },

              { "data": "nationality" },

              { "data": "create_date" },

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "invoiceStatus" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });


//view student

//1. student 

$(document).ready(function(){  

      var dataTable = $('#student_view').DataTable({

            dom: 'lBfrtip',
              scrollX:true,
              // sScrollY: "100%",
              "sScrollY": "500px",
              "sScrollCollapse": true,
              "paging": true,
              

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [                  
                   { extend: 'excelHtml5', text: 'Download Excel' }
                  
                   

              ],



          fixedColumns:   {

            leftColumns: 4,

            rightColumns:1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'student/ajaxStudents',  

                type:"POST"  

           }, 

      "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":2},

             { "orderable": false,"targets":7},

             { "orderable": false,"targets":18},

             { "orderable": false,"targets":19},

             { "orderable": false,"targets":20},

             { "orderable": false,"targets":21},


             { "orderable": false,"targets":22},

              ], 



      "columns": [

              { "data": "sn2" },

              { "data": "img" },

              { "data": "username" },

              { "data": "studentRoll" },

              { "data": "name" },              

              { "data": "father_name" },

              { "data": "yearsOrSemester" },

              { "data": "classes" },

              { "data": "counsellor" },

              { "data": "session" },

              { "data": "sessionType" },

              { "data": "student_status" },

              { "data": "phone" },

              { "data": "dob" },

              { "data": "sex" },

              { "data": "email" },

              { "data": "mother_name" },

              { "data": "aadhar" },

              { "data": "nationality" },

              { "data": "create_date" },

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "invoiceStatus" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });


$(document).ready(function(){  

      var dataTable = $('#invoice_data_table').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

                  'excelHtml5',

                 

              ],



          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'invoice/ajaxStudents',  

                type:"POST"  

           }, 





      "columns": [

           
             
              { "data": "roll" },

              { "data": "name" },              

              { "data": "amount" },

              { "data": "paidamount" },

              { "data": "dueamount" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });

// 2.subject 

$(document).ready(function(){  

      var dataTable = $('#subjectsTables').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

                  'excelHtml5',

                  
              ],

              fixedColumns:   {

                  leftColumns: 0,

                  rightColumns:1

              },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'subject/AjaxTable',  

                type:"POST"  

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":1},

    { "orderable": false,"targets":7}

  ],

      "columns": [   

              { "data": "check" },

              { "data": "sn2" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "subject_credit" },

              { "data": "classes" },

              { "data": "yearsOrSemester" },

              { "data": "action" },



           ], 

      });  



dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  


// 2.1 subject views


$(document).ready(function(){  

      var dataTable = $('#subjectsTables_views').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

                  'excelHtml5',

                 

              ],

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'subject/AjaxTable',  

                type:"POST"  

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

  ],

      "columns": [   

              { "data": "sn2" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },



           ], 

      });  



dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  

  
//2.1 subject for Academic

$(document).ready(function(){  

      var dataTable = $('#subjectsTablesAcademic').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

                  'excelHtml5',

                  
              ],

              fixedColumns:   {

                  leftColumns: 0,

                  rightColumns:1

              },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'subject/AjaxTable',  

                type:"POST"  

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":1},

    { "orderable": false,"targets":6}

  ],

      "columns": [   

              

              { "data": "sn2" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "subject_credit" },

              { "data": "classes" },

              { "data": "yearsOrSemester" },

              { "data": "action" },



           ], 

      });  



dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  




// 3. result admin


 $(document).ready(function(){  

      var dataTable = $('#MarkTables').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

                  'excelHtml5'

                  

              ],

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],

           "bStateSave": true,  

           "ajax":{  

                url:base_url+'mark/AjaxTable',  

                type:"POST"  

           }, 

  "columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":4}

  ],

      "columns": [   

              { "data": "sn" },

              { "data": "name" },

              { "data": "classes" },

              { "data": "yearsOrSemester" },

              { "data": "action" },



           ], 

      });  

 });



//4 notice



$(document).ready(function(){

      var dataTable = $('#noticeTables').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

              buttons: [

                  'excelHtml5'
 

              ],

          search: false,

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,

           "serverSide":true,

           "order":[],

           "bStateSave": true, 

           "ajax":{

                url:base_url+'notice/AjaxTable',

                type:"POST"

           },

  "columnDefs": [

    { "orderable": false,"targets":0},

    { "orderable": false,"targets":6}

  ],

      "columns": [

              { "data": "sn" },

              { "data": "title" },

              { "data": "date" },

              { "data": "classesID" },

              { "data": "sub_coursesID" },

              { "data": "yearsOrSemester" },

              { "data": "notice" },

              { "data": "action" },

           ],

      });

 });


 $(document).ready(function() {

    $('#clgforBillingStudent').DataTable( {
        scrollX:true,
             // sScrollY: "100%",
             "sScrollY": "500px",
             "sScrollCollapse": true,
             "paging": true,
        dom: 'lBfrtip',

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],
                language: {

        processing: "<img src='"+base_url+"uploads/images/loading-circle.gif'>",
              paginate: {
      next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
      previous: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>'  
    }

       },

        buttons: [

           'excel'

        ]

    } );

} );


 // 5. Pdf list

 $(document).ready(function(){  

      var dataTable = $('#subjectsTablesPdf1').DataTable({

         
            dom: 'Bfrtip',

              scrollX:true,

              scrollCollapse: true,

              buttons: [

                  'excelHtml5',

                  

              ],

          search: false, 

        language: {

       processing: "<img src='"+ base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'online_reader/AjaxTable',  

                type:"POST"  

           },  

      "columns": [

              { "data": "sn" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "countpdf" },

              { "data": "action" }

           ], 

      });  

 });  


// Online Reader

$(document).ready(function(){  

      var dataTable = $('#subjectsTablesPdf').DataTable({

            dom: 'Bfrtip',

              scrollX:true,

              scrollCollapse: true,

              buttons: [

                  'excelHtml5',

                  

              ],

          search: false, 

        language: {

       processing: "<img src='"+ base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'lms/AjaxTable',  

                type:"POST"  

           },  

      "columns": [

              { "data": "sn" },

              { "data": "subject_code" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "countpdf" },

              { "data": "action" }

           ], 

      });  

 });  


// 6.exam 

$(document).ready(function(){  

      var dataTable = $('#examTables').DataTable({

            dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

            lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":9},

             { "orderable": false,"targets":10},

              ], 

              buttons: [

                  'excelHtml5'

                  

              ],

          fixedColumns:   {

            leftColumns: 3,

            rightColumns: 1

        },

          search: false, 

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'exam/AjaxTable',  

                type:"POST"  

           },  

      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "quiz_name"},

              { "data": "subject_code" },

              { "data": "start_date" },

              { "data": "end_date" },

              { "data": "subject" },

              { "data": "classes" },

              { "data": "sub_course" },

              { "data": "yearsOrSemester" },

              { "data": "action" },

           ], 

      }); 



            dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 





 });  





 // 7. question table 



$(document).ready(function(){  

      var dataTable = $('#questionTables').DataTable({

              dom: 'lBfrtip',

              scrollX:true,

              scrollCollapse: true,

              lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":2},

              ], 

              buttons: [

                  'excelHtml5'

                  

              ],



          search: false, 

        language: {

        processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

        },

           "processing":true,  

           "serverSide":true,  

           "order":[],  

           "ajax":{  

                url:base_url+'exam/AjaxTableQuestion/'+uri,  

                type:"POST"  

           },  

      "columns": [

              { "data": "sn" },

              { "data": "question"},

              { "data": "action" },

           ], 

      });  

 });  



//8. courses clasess

$(document).ready(function(){  

      var dataTable = $('#courses').DataTable({

        dom: 'lBfrtip',

        scrollX:true,

        scrollCollapse: true,

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":1},

             { "orderable": false,"targets":10},

            

              ], 

       buttons: [

                  'excelHtml5',

                  'pdfHtml5'

              ],

        fixedColumns:   {

            leftColumns: 0,

            rightColumns:1

        },      

          search: false,

          language: {

            processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

          },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

              url:base_url+'classes/ajaxCourse',  

              type:"POST"  

            },  

      "columns": [

              { "data": "check" },

              { "data": "sn2" },

              { "data": "name" },

              { "data": "classes" },

              { "data": "duration" },

              { "data": "course_type" },

              { "data": "mode" },

              { "data": "fee" },

              { "data": "notice"},

              { "data": "department_name"},

              { "data": "action" }

           ], 

      }); 



      dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(1, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

});  

 });  


 // 8.1 courcses only view
//8. courses clasess
$(document).ready(function(){  

      var dataTable = $('#courses_views').DataTable({

        dom: 'lBfrtip',

        scrollX:true,

        scrollCollapse: true,

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},
             { "orderable": false,"targets":7},

              ], 

       buttons: [

                  'excelHtml5'
 

              ],

          search: false,

        language: {

       processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[], 

           "bStateSave": true, 

           "ajax":{  

                url:base_url+'classes/ajaxCourse',  

                type:"POST"  

           },  

      "columns": [

              { "data": "sn2" },

              { "data": "name" },

              { "data": "classes" },

              { "data": "duration" },

              { "data": "course_type" },

              { "data": "mode" },

              { "data": "fee" },

              { "data": "notice"},

           ], 

      }); 



      dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

});  

 });  





// 9. teacher 

$(document).ready(function(){  

      var dataTable = $('#example1').DataTable({

        dom: 'lBfrtip',

              buttons: [

                  'excelHtml5',


              ],

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":4},

              ],

          search: false,

        language: {

        processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],

           "bStateSave": true,  

           "ajax":{  

                url:base_url+'teacher/ajaxTeachers',  

                type:"POST"  

           },  

      "columns": [

              { "data": "sn" },

              { "data": "organization_name" },

              { "data": "name" },

              { "data": "username" },

              { "data": "email" },

              { "data": "phone" },
              // {"data":"status"},

              { "data": "action" },

           ], 

      });  

       dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 });  



 // 10.add subject



 $(document).ready(function(){  

      var dataTable = $('#professor').DataTable({

        dom: 'lBfrtip',

              buttons: [

                  'excelHtml5',                

                            ],

        lengthMenu: [[10,25,50,100, -1], [10,25,50,100, "All"]],

         "columnDefs": [

             { "orderable": false,"targets":0},

             { "orderable": false,"targets":6},

              ],

          search: false,

        language: {

        processing: "<img src='"+base_url+"uploads/images/ajax-loader.gif'>",

       },

           "processing":true,  

           "serverSide":true,  

           "order":[],

           "bStateSave": true,  

           "ajax":{  

                url:base_url+'professor/ajaxProfessors',  

                type:"POST"  

           },  

      "columns": [

              { "data": "sn" },

              { "data": "name" },

              { "data": "username" },

              { "data": "designation_name" },

              { "data": "department_name" },

              { "data": "email" },

              { "data": "action" },

           ], 

      });  

       dataTable.on('draw.dt', function () {

    var info = dataTable.page.info();

    dataTable.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {

        cell.innerHTML = i + 1 + info.start;

    });

}); 

 }); 



function  AddSubject(){

  var classesID =  $('#classesID').val();
  var subjectName =  $('#subjectName').val();
  var subjectCode =  $('#subjectCode').val();
  var subjectCredit =  $('#subjectCredit').val();
  var yearSemesterID =  $('#yearSemesterID').val();
  var subCourseID =  $('#sub_CourseID').val();
  var professorID =  $('#professorID').val();
  var startTime =  $('#startTime').val();
  var endTime =  $('#endTime').val();
  // alert(professorID);
// alert(endTime);
    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/AddSubject',

        data:{"classesID":classesID,"subjectName":subjectName,"subjectCode":subjectCode,"subjectCredit":subjectCredit,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID,"professorID":professorID,"startTime":startTime,"endTime":endTime},

        success: function(response) {

            $('#subjectCode').val('');

            $('#subjectName ').val('');

            $('#subjectCredit ').val('');

            // $('#subjectName ').val('');

            $("#appendSujectsDetails").html(response);

          }

       });

  }



//11. call subject 

 function  CallSubject(){

var classesID =  $('#classesID').val();

var subjectName =  $('#subjectName').val();

var subjectCode =  $('#subjectCode').val();

var subjectCredit =  $('#subjectCredit').val();

var yearSemesterID =  $('#yearSemesterID').val();

var subCourseID =  $('#sub_CourseID').val();

var professorID =  $('#professorID').val();

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/CallSubject',

        data:{"classesID":classesID,"subjectName":subjectName,"subjectCredit":subjectCredit,"subjectCode":subjectCode,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID,"professorID":professorID},

        success: function(response) {

            $('#subjectCode').val('');

            $('#subjectName ').val('');

            $("#appendSujectsDetails").html(response);



        }

            });
$.ajax({

        type: "POST",

        // dataType: "json",

        url:base_url+'AjaxController/CallSubjectData',

        data:{"classesID":classesID,"subjectName":subjectName,"subjectCredit":subjectCredit,"subjectCode":subjectCode,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID,"professorID":professorID},

        success: function(response) { 

         
            response2 = JSON.parse(response);
          
            $('#startDate').val(response2.startDate);

            $('#endDate ').val(response2.endDate);

           
        }

            });





  }



//12.delete subject

 function  DeleteSubject(subjectID){

var classesID =  $('#classesID').val();

var yearSemesterID =  $('#yearSemesterID').val();

var subCourseID =  $('#sub_CourseID').val();

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/DeleteSubject',

        data:{"classesID":classesID,"subjectID":subjectID,"yearSemesterID":yearSemesterID,"subCourseID":subCourseID},

        success: function(response) {

         

            $("#appendSujectsDetails").html(response);

        }

            });



  }



 // 13.edit subject         

 function  EditSubject(val,column,subjectID,editValue){

  $(val).css("background","#FFF url("+base_url+"uploads/images/ajax-loader.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/EditSubject',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  }

  function  EditStarttime(val,column,subjectID,editValue){

  $(val).css("background","#FFF url("+base_url+"uploads/images/ajax-loader.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/EditStarttime',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  }

  function  Editendtime(val,column,subjectID,editValue){

  $(val).css("background","#FFF url("+base_url+"uploads/images/ajax-loader.gif) no-repeat right");



    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/Editendtime',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  } 
  function  CallProfessor(val,column,subjectID,editValue){
    

  $(val).css("background","#FFF url("+base_url+"uploads/images/ajax-loader.gif) no-repeat right");

// alert($val);

    $.ajax({

        type: "POST",

        url:base_url+'AjaxController/editProfessor',

        data:{"val":editValue,"subjectID":subjectID,"column":column},

        success: function(response) {

         $(val).css("background","#FDFDFD");

        }

            });



  } 

// Delete Online Reader
         
function  deleteLMS(id){
 var deleteLms = confirm('Are You sure! You want to delete this item ?');
 
// var classesID =  uri;
// alert(classesID);
if (deleteLms == true) {
    $.ajax({
        type: "POST",
        url:base_url+"online_reader/delete_LMS",
        data:{"id":id},
        success: function(response) {
          
          location.reload();
            $("#appendSubcourses").html(response);
        }
    });
}else{
    return false;
    }
  }




$(document).ready(function(){  

      var dataTable = $('#timeTable').DataTable({

            dom: 'lBfrtip',

              scrollX:true,
              scrollCollapse: true,
              "bPaginate": false,
          buttons: [
            'print'
        ],

});
});




$(document).ready(function(){  

      var dataTable = $('#subjectsTablesTeacher').DataTable({

            dom: 'lBfrtip',

              scrollX:true,
              scrollCollapse: true,
              "bPaginate": true,
          buttons: [
            'print'
        ],

});
});

