<?php

$lang['panel_title'] = "Profile";
$lang['profile_roll'] = "Roll";
$lang['profile_email'] = "Email";
$lang['profile_dob'] = "Date of Birth";
$lang['profile_jod'] = "Joining Date";
$lang['profile_sex'] = "Gender";
$lang['profile_religion'] = "Religion";
$lang['profile_phone'] = "Phone";
$lang['profile_address'] = "Address";
$lang['father_name'] = "Father Name";
$lang['mother_name'] = "Mother Name";
$lang['sub_courses'] = "Sub Course";
$lang['Semester_Year'] = "Semester/Year";
$lang['session'] = "Session";
$lang['session_name'] = "Session";
$lang['Calender_From'] = "Calender From";
$lang['Calender_To'] = "Calender To";
$lang['Academic_From'] = "Academic From";
$lang['Academic_To'] = "Academic To";
$lang['nationality'] = "Nationality";

// $lang['profile_guargian_name'] = "Guardian Name";
// $lang['profile_father_name'] = "Father's Name";
// $lang['profile_mother_name'] = "Mother's Name";
// $lang['profile_father_profession'] = "Father's Profession";
// $lang['profile_mother_profession'] = "Mother's Profession";
// $lang['parent_error'] = "Parents have not been added yet! Please add parents information.";

// $lang['personal_information'] = "Personal Information";
// $lang['parents_information'] = "Parents Information";
