<?php echo doctype("html5"); ?>

<html class="white-bg-login" lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <title>Sign in</title>

    <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" />

    <!-- bootstrap 3.0.2 -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- font Awesome -->

    <link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet"  type="text/css">

     <link href="<?php echo base_url('assets/inilabs/EtDesigns/css/Mainlogin.css'); ?>" rel="stylesheet"  type="text/css">

</head>



<body class="" >
    <?php $this->load->view($subview); ?>

<script type="text/javascript" src="<?php echo base_url('assets/inilabs/jquery.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/bootstrap.min.js'); ?>"></script>
</body>

</html>