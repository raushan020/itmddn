    <div class="">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"><i class="ti ti-book"></i> <?=$this->lang->line('add_title')?> </h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">

                    <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                    <li><a href="<?=base_url("classes/index")?>"></i> <?=$this->lang->line('panel_title')?></a></li>
                    <li class="active"><?=$this->lang->line('add_title')?> <?=$this->lang->line('menu_courses')?></li>
                    
                </ol>
            </div>
        </div>

<!-- /.box-header -->

    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">
    <div class="box-body">

        <div class="row">

            <div class="col-sm-8">
                <p>Field are required with<span class="red-color">*</span></p>
                <form class="form-horizontal" role="form" method="post">

                   <?php 

                        if(form_error('classes')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classes" class="col-sm-4 control-label">

                            <?=$this->lang->line("courses_name")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                            <input type="text" class="form-control" required = "required" placeholder="<?=$this->lang->line("courses_name")?>" id="classes" name="classes" value="<?=set_value('classes')?>" >

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('classes'); ?>

                        </span>
                        </div>

                    <?php 

                        if(form_error('education_details[]')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="classes" class="col-sm-4 control-label">

                            Education Qualifications <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6 ulliclass">
                          <ul>
                          <?php 
                          foreach ($education_details as $key => $value) {
                                    
                                    if ($value->education_detailsID==1) {
                                     $required = 'required';

                                    }else{
                                       $required = ''; 
                                    }

                                ?>
                                <li>
                                    <div class="checkbox">
                                        
                                        <label><input type="checkbox" <?php echo $required ?> name="education_details[]" value="<?php echo $value->education_detailsID ?>"><?php echo $value->education ?></label>
                                    </div>
                                </li>
                            <?php } ?>    
                                                          
                          </ul>
                            
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('education_details'); ?>

                        </span>
                    </div>

                    <?php 

                        if(form_error('isRequired')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>
                        <label for="classes" class="col-sm-4 control-label">
                            Validate Qualification <span class="red-color">*</span>               </label>
                        <div class="col-sm-6 ulliclass">
                         <select name="isRequired" class="form-control">
                             <option value="0">
                                 Select 
                             </option>
                             <option value="1">Required</option>
                             <option value="0">Not Required</option>
                         </select>    
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('education_details'); ?>
                        </span>
                    </div>

                                    
                   <?php 

                        if(form_error('course_type')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>
                        <label for="classes" class="col-sm-4 control-label">
                            Course Type<span class="red-color">*</span>               </label>
                        <div class="col-sm-6 ulliclass">
                         <select name="course_type" class="form-control">
                             <option value="0">
                                 Select 
                             </option>
                             <option value="3">PG</option>
                             <option value="2">UG</option>
                             <option value="1">Diploma</option>
                         </select>    
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('course_type'); ?>
                        </span>
                    </div>
    


                    <?php 
                        if(form_error('isRequired')) 

                            echo "<div class='form-group has-error' >";

                        else     

                           echo "<div class='form-group' >";
                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("duration")?><span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                        <input type="number" class="form-control" min="1" max="10" required = "required" placeholder="<?=$this->lang->line("duration")?>" style="resize:none;" id="duration" name="duration"><?=set_value('duration')?>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('duration'); ?>

                        </span>

                    </div>


<!-- end time duration -->




<!-- time mode -->

                    <?php 

                        if(form_error('mode')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("mode")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

                     <select class="form-control" placeholder="<?=$this->lang->line("mode")?>" required = "required" value = "<?=set_value('mode')?>" style="resize:none;" id="mode" name="mode">
                     <option value="">Select</option>
                     <option value="2">Semester</option>
                    <option value="1">Yearly</option>
                    </select>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('mode'); ?>

                        </span>

                    </div>



                    <!-- DepaRTMENT Code Goes here -->

                    <?php 

                        if(form_error('department')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            Department <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">
                            <select class="form-control" required = "required" id="department" name="department">
                                <option value="">Select Department</option>

                                <option value="1">Paramedical</option>
                                <option value="2">Fashion</option>
                                <option value="3">Hospitality</option>
                                <option value="4">Information Technology</option>
                                <option value="5">Mass Comm</option>
                                <option value="6">Engineering</option>
                            </select>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('department'); ?>

                        </span>

                    </div>


<!-- end time mode -->

                    <?php 

                        if(form_error('IsSubCourse')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            Is Sub Course <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">
                     <select class="form-control" required = "required" id="IsSubCourse" name="IsSubCourse">
                     <option value="">Select</option>
                     <option value="1">Yes</option>
                    <option value="0">No</option>
                    </select>
                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('IsSubCourse'); ?>

                        </span>

                    </div>

<!-- time fee -->

                    <?php 

                        if(form_error('fee')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("fee")?> <span class="red-color">*</span>

                        </label>

                        <div class="col-sm-6">

     <div class="input-group control-group after-add-more">
         <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
          <input type="text" name="fee[]" class="form-control" placeholder="Enter Fee">
          <div class="input-group-btn"> 
            <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add More Fee</button>
          </div>
        </div>

               <div class="copy hide">
          <div class="control-group input-group" style="margin-top:10px">
             <span class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></span>
            <input type="text" name="fee[]" class="form-control" placeholder="Enter Fee">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>
        </div>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('fee'); ?>

                        </span>

                    </div>


<!-- end time fee -->


                    <?php 

                        if(form_error('note')) 

                            echo "<div class='form-group has-error' >";

                        else     

                            echo "<div class='form-group' >";

                    ?>

                        <label for="note" class="col-sm-4 control-label">

                            <?=$this->lang->line("courses_note")?>

                        </label>

                        <div class="col-sm-6">

                            <textarea class="form-control" placeholder="<?=$this->lang->line("courses_note")?>" style="resize:none;" id="note" name="note"><?=set_value('note')?></textarea>

                        </div>

                        <span class="col-sm-4 control-label">

                            <?php echo form_error('note'); ?>

                        </span>

                    </div>

                       <div class="form-group">

                        <div class="col-sm-offset-4 col-sm-8">

                            <input type="submit" class="btn btn-success add-btn" value="<?=$this->lang->line("add_course")?>" >

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
    
    $(function()
{
    $(document).ready(function(){
        var buttonadd = '<span><button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span></button></span>';
        var fvrhtmlclone = '<div class="fvrclonned">'+$(".fvrduplicate").html()+buttonadd+'</div>';
        $( ".fvrduplicate" ).html(fvrhtmlclone);
        $( ".fvrduplicate" ).after('<div class="fvrclone"></div>');

        $(document).on('click', '.btn-add', function(e)
        {
            e.preventDefault();
    
            $( ".fvrclone" ).append(fvrhtmlclone);
                  $(this).removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="glyphicon glyphicon-minus"></span>');
            
        }).on('click', '.btn-remove', function(e)
        {
            $(this).parents('.fvrclonned').remove();
    
            e.preventDefault();
            return false;
        });

    });
    
 
// FUNÇÂO DE TESTE    
    

});

</script>
<script>
    $(document).ready(function(){
        $(".add-more").click(function(e){
            e.preventDefault(); 
              var html = $(".copy").html();
              $(".after-add-more").after(html);
          });


          $("body").on("click",".remove",function(){ 
              $(this).parents(".control-group").remove();
          });
    });
      
</script>
<style type="text/css">
    .fvrclonned
{
    margin-top: 10px;
}

.glyphicon
{
    font-size: 14px;
}

</style>

<style type="text/css">
    .red-color{
        color:red;
    }
</style>
