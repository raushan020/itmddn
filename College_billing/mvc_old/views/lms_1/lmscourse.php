 
  <style type="text/css">
    a.disabled {
      pointer-events: none;
      cursor: default;
    }
    .hover:hover{
      background-image: linear-gradient(to right, #3187da  , #2872b8);
       transition: .5s ease;
    }


                 .team-grids {
    position: relative;
    overflow: hidden;
    z-index: 1;
}
.team-info {
    position: absolute;
    bottom: -227px;
    margin: 0;
    background: rgba(0, 0, 0, 0.5);
    padding: 2em 0;
    -webkit-transition: .5s all;
    transition: .5s all;
    -moz-transition: .5s all;
    width: 82%;
    text-align: center;
}
.best-course-pic-text .best-course-pic {
    overflow: hidden;
    border-radius: 4px;
}
img {
    max-width: 100%;
    height: auto;
}
.trend-badge-2 {
    top: -18px;
    left: -50px;
    color: #fff;
    font-size: 12px;
    font-weight: 700;
    position: absolute;
    padding: 30px 35px 5px;
    -webkit-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    transform: rotate(-45deg);
    background-color: #ff5a00;
}
.best-course-pic-text .course-price {
    bottom: 20px;
    z-index: 1;
    top: inherit;
}
.course-price {
    top: 20px;
    left: 20px;
    font-weight: 700;
    padding: 5px 15px;
    border-radius: 4px;
    position: absolute;
}
.blakish-overlay {
    top: 0;
    opacity: 0;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    position: absolute;
    visibility: hidden;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
    background-color: rgba(0, 0, 0, 0.65);
}
.relative-position {
    position: relative;
}
.text-center {
    text-align: center!important;
}
.text-uppercase {
    text-transform: uppercase!important;
}
.best-course-pic-text:hover .blakish-overlay {
    opacity: 1;
    visibility: visible;
    z-index: 0;
}
.best-course-pic-text:hover .course-details-btn {
    opacity: 1;
    right: 25px;
    visibility: visible;
    z-index: 1;
}
.fa, .fas {
    /*font-family: 'Font Awesome 5 Free';*/
    font-weight: 900;
}
.course-details-btn a {
    font-size: 12px;
    color: #fff;
    font-weight: 700;
}
.course-price {
    top: 20px;
    left: 20px;
    font-weight: 700;
    padding: 5px 15px;
    border-radius: 4px;
    position: absolute;
}
.best-course-pic-text .course-price {
    bottom: 20px;
    z-index: 1;
    top: inherit;
}
.best-course-pic-text .best-course-text {
    background-color: #fff;
    border-radius: 4px;
    padding: 20px 25px 30px 25px;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
}
.best-course-pic-text .course-rate {
    position: absolute;
    right: 25px;
    bottom: 25px;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
}
.best-course-pic-text .course-rate li {
    font-size: 14px;
}
.ul-li ul li {
    list-style: none;
    display: inline-block;
}
.best-course-pic-text:hover .course-details-btn {
    opacity: 1;
    right: 25px;
    visibility: visible;
    z-index: 1;
}
.course-details-btn {
    left: 164px;
    opacity: 0;
    bottom: 139px;
    
    visibility: hidden;
    position: absolute;
    -webkit-transition: .3s all ease-in-out;
    -o-transition: .3s all ease-in-out;
    transition: .3s all ease-in-out;
}
.video-play-btn {
    height: 68px;
    width: 68px;
    line-height: 42px;
    border-radius: 100%;
    color: #fff;
    position: absolute;
    top: 50%;
    right: 23px;
    left: 0;
   /* margin: 0 auto;*/
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
}
.gradient-bg, .modal-body .nws-button button, .teacher-pic-content .teacher-img-content:after, .course-details-category li:hover {
    background: #17d0cf;
    background: -moz-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, #4eb3bf), color-stop(51%, #17d0cf), color-stop(100%, #4eb3bf));
    background: -webkit-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #0071b0 100%);
    background: -o-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: -ms-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: -webkit-gradient(linear, left top, right top, from(#01a6fd), color-stop(51%, #17d0cf), to(#01a6fd));
    background: -webkit-linear-gradient(left, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background: linear-gradient(to right, #01a6fd 0%, #17d0cf 51%, #01a6fd 100%);
    background-size: 200% auto;
    -webkit-transition: background 1s ease-out;
    -moz-transition: background 1s ease-out;
    -o-transition: background 1s ease-out;
    transition: background 1s ease-out;
}
.text-center {
    text-align: center!important;
}
               </style>
  <div class="container">



    <div class="page-section">
    	<div class="row">
        <div class="col-md-12">
    	     <div class="panel-footer" style="text-align: center;">

                 <h4 class="theight">

                 	<?php                 				
             			
              			echo $subject->subject;
                			
                 	  ?>
               	  	
                	  </h4>

                 <div>

                   <p class="theight"><strong style="color: #333">Course Status:</strong> In Progress</p>

                 </div>

                 <p class="theight" style="color: green; text-transform: uppercase;">You are enrolled in this course</p>

                 <div class=""></div> 
          </div>
      </div>
  </div>

      <div class="row">
        <div class="col-md-12">



          <!-- <div class="panel panel-default">

            <div class="media v-middle">

              <div class="media-left">

                <div class="bg-green-400 text-white">

                  <div class="panel-body">

                    <i class="fa fa-credit-card fa-fw fa-2x"></i>

                  </div>

                </div>

              </div>

              <div class="media-body">

                Your LMS is Here,  <span class="text-body-2">Start Learning</span>

              </div>

              <div class="media-right media-padding">

                <a class="btn btn-white paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated href="<?php echo base_url('lms/lmssubject');?>">

                Start now

            </a>

              </div>

            </div>

          </div> -->



          <div class="row" data-toggle="isotope">

            <div class="item col-xs-12 col-lg-9">

              <div class="panel panel-default paper-shadow" data-z="0.5">
                <?php if(count($epub)) {

                   foreach($epub as $key => $value2) { 
  
                  ?>

                <div class="col-xs-12 col-lg-6">
                  <div class="best-course-pic-text relative-position "  >
        						<div class="best-course-pic relative-position">
        							<img src="<?php echo base_url() ?>assets/lms/images/e_reader2(1).png" alt="" style="padding: 67px 0px 86px 110px;">        														
        							<div class="course-details-btn">        								
        								<div class="hi-icon-wrap hi-icon-effect-4 hi-icon-effect-4b" style="margin: -96px;margin-right: 56px;">					
                          <a href="<?php echo base_url() ?>online_reader/reader/<?php echo $subject->subjectID.'/'.$value2->id; ?>" class="hi-icon" style="padding: 20px;">Click here To Read</a>
        				        </div> 
        							</div>
        							<div class="blakish-overlay"></div>
        						</div>
        						<div class="best-course-text">
        							<div class="course-title mb20 headline relative-position">
        								<h3 style="text-align: center;"><a href="#">Online Reader</a></h3>
        							</div>							
        						</div>
        					</div>                 
                </div>
                <?php } } ?>
               
               <?php 
                if(count($videos)) {

                 foreach($videos as $key => $value2) { 
                            
                 ?>
                <div class="col-xs-12 col-lg-6">
                  <div class="best-course-pic-text relative-position "  >
        						<div class="best-course-pic relative-position">
        							<img src="<?php echo base_url() ?>assets/lms/images/iconRah.png" alt="" style="padding: 67px 0px 86px 110px;">
                        <div class="course-details-btn">
                          <div class="hi-icon-wrap hi-icon-effect-4 hi-icon-effect-4b" style="margin: -96px;margin-right: 56px;">					
					                   <a href="<?php echo base_url() ?>lms/lmsVideos/<?php echo $subject->subjectID.'/'.$value2->videoID; ?>" class="hi-icon" style="padding: 20px;">Click here To Read</a>
				                  </div>
							          </div>
							          <div class="blakish-overlay"></div>
						          </div>
        						<div class="best-course-text">
        							<div class="course-title mb20 headline relative-position">
        								<h3 style="text-align: center;"><a href="#">Videos</a></h3>
        							</div>							
        						</div>
        					</div>                 
                </div>
                <?php } } ?>

        </div>

      </div>

      <?php $this->load->view("components/page_lmssidebar"); ?>
           
        </div>

</div>

<div class="clearfix"></div>
<div class="col-xs-12 col-lg-12">

              <div class="panel panel-default paper-shadow" data-z="0.5">

                <div class="panel-heading">

                  <h4 class="text-headline margin-none">Assignment</h4>

                  <p class="text-subhead text-light">Your recent performance</p>

                </div>

                <ul class="list-group">

              <?php
              $counter_assign =0;
               foreach ($assignment_recent_three as $key => $value) {
            $counter_assign +=1;
            if($counter_assign<5){
               ?>    
                  <li class="list-group-item media v-middle">

                    <div class="media-body">

                      <h4 class="text-subhead margin-none">

                        <a href="#" class="list-group-link"><?php  echo $value->quiz_name ?></a>

                      </h4>
<!-- 
                      <div class="caption">

                        <span class="text-light">Course:</span>

                        <a href="#">Basics of HTML</a>

                      </div> -->

                    </div>

                    <div class="media-right text-center">

                      <div class="text-display-1"><?php echo $value->score_obtained ?></div>

                      <span class="">Good</span>

                    </div>

                  </li>
                <?php } } ?>
                </ul>

                <div class="panel-footer">
                  <?php  if($counter_assign==0){ ?>

                  <a href="#" disabled="disabled" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php }else{ ?>
                  <a href="#" class="btn btn-primary paper-shadow relative" data-toggle="modal" data-target="#myModal2"> Go to Results</a>
                <?php } ?>

                </div>

              </div>

            </div>

<!-- <script>
			var hash = window.location.hash,
				current = 0,
				demos = Array.prototype.slice.call( document.querySelectorAll( '#codrops-demos > a' ) );
			
			if( hash === '' ) hash = '#set-1';
			setDemo( demos[ parseInt( hash.match(/#set-(\d+)/)[1] ) - 1 ] );

			demos.forEach( function( el, i ) {
				el.addEventListener( 'click', function() { setDemo( this ); } );
			} );

			function setDemo( el ) {
				var idx = demos.indexOf( el );
				if( current !== idx ) {
					var currentDemo = demos[ current ];
					currentDemo.className = currentDemo.className.replace(new RegExp("(^|\\s+)" + 'current-demo' + "(\\s+|$)"), ' ');
				}
				current = idx;
				el.className = 'current-demo'; 
			}
		</script> -->