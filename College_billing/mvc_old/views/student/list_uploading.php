<div class="row page-titles">

        <div class="col-md-5 align-self-center">

            <h3 class="text-themecolor"><i class="ti ti-user"></i>List Student</h3>

        </div>

 <div class="col-md-7 align-self-center">

            <ol class="breadcrumb">

                <li class="breadcrumb-item"><i class="fa fa-bullseye"></i> <a href="<?=base_url("dashboard/index")?>"><?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="breadcrumb-item"><a href="<?=base_url("student/index")?>"><?=$this->lang->line('menu_student')?></a></li>

                <li class="breadcrumb-item active">List Student</li>

            </ol>

       </div>
</div>

    <div class="container-fluid">

      <div class="row">

         <div class="">

            <div class="card">

            <div class="card-body">
<style type="text/css">
    .top_heading{
      text-align:center;
    }
</style>
<div class="top_heading">
    <h2>Student List</h2>

                  <div class="">
                    
                        <div class="form-group">
                           <label for="classesID" class="control-label">
                           Academic Partner
                           </label>
                           <div class="selectdiv">

                              <?php  
                               $array = array("0" => "Select Counsellor");
                                 
                                 
                                 
                                 
                                 
                                 foreach ($counsellor as $counsellors) {
                                 
                                 
                                 
                                     $array[$counsellors->teacherID] = $counsellors->TeacherName;
                                 
                                 
                                 
                                 }
                                 
                                

                                 echo form_dropdown("teacherID", $array, set_value("teacherID",$this->session->userdata('teacherID_extra')), "id='teacherID' class='form-control'");
                                 
                                 
                                 
                                 ?>
                           </div>
                           <div class="clearfix"></div>
                           <div class="forReset" ><a style="cursor:pointer;" onclick="ResetteacherID()">Reset This Filter </a></div>
                        </div>
</div>
<form action="<?php echo base_url() ?>student/bulk_payment" method='post'>
<table class="table" id="Student_list_extra">
    <thead>
        <tr>
        <th>Student name</th>
        <th>Father name</th>
        <th>Academic Partner name</th>
        <th>Course name</th>
        <th>Semester</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
<?php
 foreach ($student as $key => $value) {

 ?>
        <tr>
            <td><?php echo $value->name ?></td>
            <td><?php echo $value->father_name ?></td>
            <td><?php echo $value->TeacherName ?></td>
            <td><?php echo $value->classes ?></td>
            <td><?php echo $value->yearsOrSemester ?></td>
            <td><label><a href="<?php echo base_url() ?>student/view/<?php echo $value->studentID ?>">View</a></label></td>
        </tr>
<?php } ?>
    </tbody>
</table>
<div class="col-md-6"></div>
<div class="col-md-6">
 
</div>

</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function remove_Bpayment(val,amount,paidamount){
    
        var single_amout = +amount - +paidamount;
        var total = $('#total_payment').val();
        if(val.checked==true){
       var total_amount_after = +total + +single_amout; 
        $('#total_payment').val(total_amount_after);
        $('#total_amount_final').html(total_amount_after);
        }
        if(val.checked==false){
       var total_amount_after = +total - +single_amout; 
        $('#total_payment').val(total_amount_after); 
        $('#total_amount_final').html(total_amount_after);  
        }
    }
</script>
<script type="text/javascript">
   $('#teacherID').change(function() {
   
     $('#example4').DataTable().state.clear();
   
   
   
       var teacherID = $(this).val();
   
   
   
           $.ajax({
   
   
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/list_uploading')?>",
   
   
   
               data: " teacherID=" + teacherID,
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
   
   
                    location.reload();
   
   
   
               }
   
   
   
           });
   
   
   
   });
   
   
   
</script>
<script type="text/javascript">
    function ResetteacherID(){
   
     $('#example4').DataTable().state.clear();
   
           $.ajax({
   
               type: 'POST',
   
   
   
               url: "<?=base_url('student/ResetteacherID_extra')?>",
   
   
   
               data:{ResetMorefilter:'ResetMorefilter'},
   
   
   
               dataType: "html",
   
   
   
               success: function(data) {
   
                   location.reload();
   
   
   
               }
   
   
   
           });
   
   }
</script>
