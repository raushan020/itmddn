<style type="text/css">
  
  .form-control{
    width: 58%;
  }
  .form-control, output {
    display: initial;
  }
</style>

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="fa fa-user"></i> Exam Form</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

                <li class="active">Exam Form</li>
            </ol>
        </div>
    </div>
   <!-- /.box-header -->
   <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">

<?php

    $usertype = $this->session->userdata('usertype');   

    if($usertype == "Student") { 
?>

    <section class="panel">
   

        <div class="row">
             <div class="col-sm-4">

            </div>
 
            <div class="col-sm-4">

                <h3><u>Examination Form</u></h3>
                
                <!-- <p>October 2018</p> -->
            </div> 

            <div class="col-sm-4">

            </div>

        </div>



   
    </section>

   
    <section class="panel">        

        <div class="panel-body profile-view-dis">

            <h3 class="border_heading">Personal Information</h3>

            <div class="row">

                <div class="profile-view-tab">

                    <p><span>Student Id </span>: <?=$student->username?></p>

                </div>

                <div class="profile-view-tab">

                    
                <p><span>Enrollment No </span>: <?=$student->roll?></p>
                </div>

                <div class="profile-view-tab">

                    <p><span>DOB </span>: <?=date("d M Y", strtotime($student->dob))?></p>

                </div>

                <div class="profile-view-tab">

                    <p><span>Gender </span>: <?=$student->sex?></p>

                </div>


                <div class="profile-view-tab">

                    <p><span>Email </span>: <?=$student->email?></p>

                </div>

                <div class="profile-view-tab">

                    <p><span>Mobile No. </span>: <?=$student->phone?></p>

                </div>            

            </div>
           
            <h3 class="border_heading">Course Details</h3>

            <div class="row">
                <div class="profile-view-tab">
                    <p><span>Course </span>: <?=$class->classes?></p>
                </div>
                <div class="profile-view-tab">
                    <p><span>Sub Course</span>:
                        <?php
                            if (isset($sub_courses->sub_course)) {
                              echo $sub_courses->sub_course;
                            }else{
                                echo "--";
                            }
                        ?>
                    </p>
                </div>
                <div class="profile-view-tab">

                    <p><span>Apply For Semester</span>:                         
                      <select name="classesID" id="classesID" class="form-control">
                        <option value="0">Select Semester/Year</option>

                        <?php 
                    
                          if ($class->classes) {
                               $looping    =  (int) $class->duration;
                             if ($class->mode==1) {
                                for ($i=1; $i <=$looping; $i++) {
                                       
                                  
                               echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                               
                             }
                          }

                          else{
                            for ($i=1; $i <=(2*$looping); $i++) {
                              
                                  
                               echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                             }
                             }
                          }
                          ?>

                        <!-- <option value="1">Ist Year</option>
                        <option value="2">2nd Year</option>
                        <option value="3">3rd Year</option> -->
                      </select>                     
                        
                    </p>  
                </div>
                <div class="profile-view-tab">

                    <p><span>Mode of Examination</span>: 
                      <select name="classesID" id="classesID" class="form-control">
                        <option value="0">Select Mode of Examination</option>
                        <option value="1">Indian</option>
                        <option value="2">International</option>
                      </select> 
                    </p>

                </div>
                <div class="profile-view-tab">

                    <p><span>Payment Type</span>:  
                      <select name="classesID" id="classesID" class="form-control">
                        <option value="0">Select Mode of Examination</option>
                        <option value="1">Course fee</option>
                        <option value="2">Examination Fee</option>
                        <option value="3">Other Fee</option>
                      </select> 
                    </p>

                </div>
                <div class="profile-view-tab">

                    <p><span>Amount For Payment</span>: 
                      <input type="number" class="form-control" name="amtPayment">
                    </p>

                </div> 
            </div>
            <center><button class="btn btn-success add-btn">Pay Now</button></center>
            <div>&nbsp;</div>

        </section>

    

    
<?php } ?>  




</div>
</div>
</div>
</div>
</div>
 <style type="text/css">
   .boredr_none .nav-tabs{
    border-bottom: 0px solid #e8edef;
   }
   .nopading{
      padding-right:0px;
      padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #2057a8;
   color:#fff;
   }
  .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #fff;
}
.theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */
  color: #fff;
}
.theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */
  color: #fff;
}
.theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */
  color: #fff;
}
.forpostionReletive {
    position: relative;
}
.postionAbsoluter {
    position: absolute;
    
    top: 8px;
}
.action-layout ul li{
   display:inline-block;
}
</style> 