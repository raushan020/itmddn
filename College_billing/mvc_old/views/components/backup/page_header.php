<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?=$this->lang->line('panel_title')?></title>
        <link rel="SHORTCUT ICON" href="<?=base_url("uploads/images/$siteinfos->photo")?>" />
        <script>
        var uri =  "<?php echo $this->uri->segment(3) ?>";
        var uri4 =  "<?php echo $this->uri->segment(4) ?>";
        var base_url="<?php echo base_url();?>";

    </script>
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css">
        
        

        <!-- Font CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/all_icons.css">
        
        <!-- Loader CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/loader.css">

        <!-- MetisMenu CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/metisMenu/metisMenu.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/kavach.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/animate.css">
        <!-- Change Color CSS --> 
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/etslabs/css/skin/default-skin.css" />
        <link href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">

        <link href="<?php echo base_url('assets/etslabs/css/rid.css'); ?>" rel="stylesheet">


        <link href="<?php echo base_url('assets/etslabs/css/rid.css'); ?>" rel="stylesheet">

        <link href="<?php echo base_url('assets/etslabs/css/intlTelInput.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" >

        <link href="<?php echo base_url('assets/etslabs/css/styleExam.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/etslabs/css/login.css');?>" rel="stylesheet">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"> -->

      <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/toastr/toastr.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/webcamjs/webcam.js'); ?>"></script>
        <script src="<?php echo base_url('assets/etslabs/js/basic.js');?>"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <?php
    $URI = "$_SERVER[REQUEST_URI]";
    $uriMain= str_replace('/edgesis/','',$URI);
    ?>    
    <input type="hidden" name="uri_findOutFrom_urls_jquery" value="<?php echo $uriMain ?>" id="uri_findOutFrom_urls_jquery" >
    <script type="text/javascript">
      var redirecturi =  $('#uri_findOutFrom_urls_jquery').val();
    </script>
<div id="wrapper" class="">
<div class="fakeLoader"></div>