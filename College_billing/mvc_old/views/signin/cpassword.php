


<div class="">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor"><i class="ti ti-lock"></i> <?=$this->lang->line('change_password')?> </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
                <li class="active"><?=$this->lang->line('change_password')?></li>
            </ol>
        </div>
    
     </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div class="container-fluid">
      <div class="row">
         <div class="">
            <div class="card">
            <div class="card-body">
            <div class="col-sm-12">




		
		<!-- <div class="form_bdr mrgbot6 col-sm-offset-4 col-sm-4">
			<h1>Change Password</h1>
			
		
			<form class="form-horizontal" method="post" name="form" id="form">				
				<div class="col-sm-12">
						
				<div id="loginPart">
					<div class="form-group">
						<label class="control-label col-sm-3" for="firstName">Email:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="username" id="firstName" placeholder="Enter Email-id" required>
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-sm-3" for="firstName">Password:</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required>
						</div>
					</div>
				<br>
				<div class="form-group">
					<div class="col-sm-3">&nbsp;</div>
					<div class="col-sm-9">
						<button type="submit" name="loginBtnTeacher" class="btn btn-primary">Log in</button>					
					</div>
					
				  
					<div class="col-lg-5 col-md-6 col-sm-4 col-xs-7 text-right">
						<a href="registration.php" alt="Register" title="Registrer Here!!." />Register</a>&nbsp;&nbsp;&nbsp;
						<a href="#" title="Forget Password" />Forgot Password</a>					
					</div>
					
				</div>
				</div> 
				
				<div class="padeql"></div>
				</div>
				
			</form>
			</div> -->
			
			  
			  


                <form class="form-horizontal col-sm-8 col-sm-offset-3" role="form" method="post">

                        <?php 
                            if(form_error('old_password')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="old_password" class="col-sm-3 control-label">
                                <?=$this->lang->line("old_password")?>
                            </label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="old_password" name="old_password" >
                            </div>
                            <span class="col-sm-3 control-label">
                                <?php echo form_error('old_password'); ?>
                            </span>
                        </div>

                        <?php 
                            if(form_error('new_password')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="new_password" class="col-sm-3 control-label">
                                <?=$this->lang->line("new_password")?>
                            </label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="new_password" name="new_password">
                            </div>
                            <span class="col-sm-3 control-label">
                                <?php echo form_error('new_password'); ?>
                            </span>
                        </div>

                        <?php 
                            if(form_error('re_password')) 
                                echo "<div class='form-group has-error' >";
                            else     
                                echo "<div class='form-group' >";
                        ?>
                            <label for="re_password" class="col-sm-3 control-label">
                                <?=$this->lang->line("re_password")?>
                            </label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="re_password" name="re_password">
                            </div>
                            <span class="col-sm-3 control-label">
                                <?php echo form_error('re_password'); ?>
                            </span>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <input type="submit" class="btn btn-success" value="Reset <?=$this->lang->line("change_password")?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <a href="<?=base_url("signin/logoutAllDevices")?>/" class="btn btn-success">Log out from all device </a>
                            </div>
                        </div>

                    </form>
            </div> <!-- col-sm-8 -->
            
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->


<script type="text/javascript">
    
    function logoutAllDevice(){

    }
</script>