<div class="">


      <div class="row page-titles">
        <div class="col-md-5 align-self-center">
          <h3 class="text-themecolor"><i class="fa fa-amazon"></i> Online Reader</h3>
        </div>
        <div class="col-md-7 align-self-center">
          <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-bullseye"></i> <?=$this->lang->line('menu_dashboard')?></a></li>

            <li class="active">LMS PDF</li>
          </ol>
        </div>
      </div>
    <!-- form start -->
<style type="text/css">
       .boredr_none .nav-tabs{
    border-bottom: 0px solid #e8edef;
   }
   .nopading{
      padding-right:0px;
      padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #2057a8;
   color:#fff;
   }
  .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: #fff;
  }
  .theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */
    color: #fff;
  }
  .theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */
    color: #fff;
  }
  .theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */
    color: #fff;
  }
  .forpostionReletive {
      position: relative;
  }
  .postionAbsoluter {
      position: absolute;
      right: 7px;
      top: 8px;
  }
  .action-layout ul li{
     display:inline-block;
  }   
  .boredr_none .nav-tabs{
    border-bottom: 0px solid #e8edef;
   }
   .nopading{
      padding-right:0px;
      padding-left:0px;
   }
   .theme_input_blue .form-control{
   background: #fff;
   color:#fff;
   }
  .theme_input_blue .form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #fff;
}
.theme_input_blue .form-control::-moz-placeholder { /* Firefox 19+ */
  color: #fff;
}
.theme_input_blue .form-control:-ms-input-placeholder { /* IE 10+ */
  color: #fff;
}
.theme_input_blue .form-control:-moz-placeholder { /* Firefox 18- */
  color: #fff;
}
.forpostionReletive {
    position: relative;
}
.postionAbsoluter {
    position: absolute;
    right: 7px;
    top: 8px;
}
.action-layout ul li{
   display:inline-block;
}
</style>
    <div class="container-fluid">
        <div class="row">
        <div class="card">
            <div class="card-body">
            <div class="col-sm-12 theme_input_blue">
                <?php 

                    $usertype = $this->session->userdata("usertype");
                    $SpecialUsertype = $this->session->userdata('SpecialUsertype');
                    if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Academic" || $usertype == "Support" || $SpecialUsertype=='Btech') {

                ?>
<div class="col-sm-12">
                     
<div class="col-md-6">
    
</div>
<div class="col-md-6">
    <div class="pull-right">
      <div class="btn-group">
       <a style="cursor:pointer;"onclick="ResetCourses()">Reset All Filters</a>
      </div>
   </div>
</div>
<div class="clearfix"></div>
            
                <div class="col-sm-4">

                    <div class="">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="control-label">

                                    Course
                                </label>

                                <div class="">
                               <?php
                                        $array = array("0" =>"Select Course");

                                        foreach ($classes as $classa) {

                                            $array[$classa->classesID] = $classa->classes;

                                        }

                                        echo form_dropdown("classesID", $array, set_value("classesID",$this->session->userdata('FilterSubjectclassesID')), "id='classesID' onchange='ajaxGet_subCourses($(this).val())' class='form-control'");

                                    ?>
                                </div>
                                 <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetCourses()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>

                </div>

                <div class="col-sm-4">

                    <div class="">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="control-label">

                                 Sub Courses

                                </label>

                                <div class="">
                                       <select id="subCourseID" class="form-control">
                                        <option value="">Select</option>
                                        <?php  foreach ($subCourses as $key => $value) {
                                            if ($value->sub_coursesID==$this->session->userdata('FilterSubjectsubCourseID')) {
                                                $selected =  "Selected";
                                            }else{
                                              $selected =  "";
                                            }
                                         ?>
                                        <option value="<?php echo $value->sub_coursesID ?>" <?php echo $selected ?>><?php echo $value->sub_course ?></option>
                                        <?php } ?>
                                        </select>

                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSubcourses()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>

                </div>

              <div class="col-sm-4">

                    <div class="">

                        <form style="" class="form-horizontal" role="form" method="post">

                            <div class="form-group">

                                <label for="classesID" class="control-label">

                                Year/Semester

                                </label>

                                <div class="">


                                  <div class="selectdiv">

                        <select class='form-control' name='semesterId' onchange='GetEntryTypeData($(this).val())' id='yearSemesterID' value='<?php echo set_value('semesterId'); ?>'>
                           
                              <option>Year/Semester</option>
                          <?php 
                                        if ($classesRow) {
                                         $looping    =  (int) $classesRow->duration;
                                       if ($classesRow->mode==1) {
                                          for ($i=1; $i <=$looping; $i++) {
                                                 if (CallYears($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallYears($i).">".str_replace('_', ' ', CallYears($i))."</option>";
                                         
                                       }
                                    }

                                    else{
                                          for ($i=1; $i <=(2*$looping); $i++) {
                                        if (CallSemester($i)==$this->session->userdata('FilterSubjectyearSemesterID')) {
                                            $select = 'Selected';
                                         }else{
                                         $select = '';
                                         }
                                         echo "<option ".$select." value=".CallSemester($i).">".str_replace('_', ' ', CallSemester($i))."</option>";
                                       }
                                       }
                                    }
                                    ?>
                                    </select>
                           </select>
                        </div>
                                
                                </div>
                                <div class="clearfix"></div>
                                <div class="forReset"><a style="cursor:pointer;" onclick="ResetSemesterYear()">Reset This Filter </a></div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

                <?php } ?> 



<?php
if ($subjects>0) {
 ?>
 <form action="<?php echo base_url() ?>online_reader/submit_Online_reader" method="post" enctype='multipart/form-data'>
<table id="subjectsTablesPdf" class="table table-striped table-bordered table-hover dataTable no-footer">
<thead>
<tr>
<th><?=$this->lang->line('slno')?></th>
                                <th><?=$this->lang->line('subject_code')?></th>
                                <th><?=$this->lang->line('subject_name')?></th>
                                <th>Course Name</th>
                                <th>Sub Course Name</th>
                                <th>Semester/Year</th>
                                <th>Online Reader</th>
                                <th><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                    </table>
                    <style type="text/css">
                    .pdf_form{
                        text-align:center;
                    }
                        .pdf_form input{
                          text-align: center;
                          padding: 10px 29px;
                          background: #2a6bac;
                          color: #ffff;
                          border: none;
                          border-radius: 4px;
                          margin-left: 100px;
                        }
                    </style>
                    <div class="pdf_form">
                       <input type="submit" name="submit">
                   </div>
                </form>
        <?php } else{ ?>
        <div class="Noresuls"><h1>Sorry we couldn't find any matches</h1>
<p>Maybe your search was too specific, please try searching with another term.</p>
<img src="<?php echo base_url() ?>uploads/images/crying.png">
</div>
         <?php } ?>


            </div> <!-- col-sm-12 -->

            

        </div><!-- row -->

    </div><!-- Body -->

</div><!-- /.box -->
</div></div>
<script type="text/javascript">

    $('#classesID').change(function() {

        var classesID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "classesID=" + classesID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>


<script type="text/javascript">

    $('#subCourseID').change(function() {

        var subCourseID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "subCourseID=" + subCourseID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>

<script type="text/javascript">

    $('#yearSemesterID').change(function() {
      // alert(yearSemesterID);

        var yearSemesterID = $(this).val();

            $.ajax({

                type: 'POST',

                url: "<?=base_url('subject/index')?>",

                data: "yearSemesterID=" + yearSemesterID,

                dataType: "html",

                success: function(data) {

                     location.reload();

                }

            });

    });

</script>
<script type="text/javascript">
    
    function ResetCourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetCourses')?>",

                data:{ResetCourses:'ResetCourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}


    function ResetSubcourses(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetSubcourses')?>",

                data:{ResetSubcourses:'ResetSubcourses'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}
    function ResetSemesterYear(){
            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetSemesterYear')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

 function ResetAllfilter(){

            $.ajax({
                type: 'POST',

                url: "<?=base_url('subject/ResetCourses')?>",

                data:{ResetSesession:'ResetSesession'},

                dataType: "html",

                success: function(data) {
                    location.reload();

                }

            });
}

</script>
