<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Notice_m extends MY_Model {



	protected $_table_name = 'notice';

	protected $_primary_key = 'noticeID';

	protected $_primary_filter = 'intval';

	protected $_order_by = "noticeID desc";



	function __construct() {

		parent::__construct();

	}



	function get_notice($array=NULL, $signal=FALSE) {

		$query = parent::get($array, $signal);

		return $query;

	}


	function get_join_notice_count($adminID) {

		$this->db->select('*');

		$this->db->from('notice');

		$this->db->join('classes', 'classes.ClassesID = notice.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = notice.sub_coursesID', 'LEFT');


		if ($this->session->userdata('FilterNoticeclassesID')) {
		 $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		}

		if ($this->session->userdata('FilterNoticesubCourseID')) {
		 $this->db->where('notice.sub_coursesID', $this->session->userdata('FilterNoticesubCourseID'));
		}

		if ($this->session->userdata('FilterNoticeyearSemesterID')) {
		 $this->db->where('notice.yearsOrSemester', $this->session->userdata('FilterNoticeyearSemesterID'));
		}

	     if ($this->session->userdata('ActiveNotices')) {
		 $this->db->where('notice.status', $this->session->userdata('ActiveNotice'));
		}

	   if ($this->session->userdata('DraftNotices')) {
		 $this->db->where('notice.status', 0);
		}

	   if ($this->session->userdata('TrashNotices')) {
		 $this->db->where('notice.status', $this->session->userdata('TrashNotice'));
		}	

		$this->db->where('notice.adminID', $adminID);

		$query = $this->db->get();

		return $query->num_rows();

	}

	function make_datatables($adminID){  
           $this->get_join_notice();  
           $query = $this->db->get();  
           return $query->result();  
      } 

      function get_join_notice() {

		$adminID = $this->session->userdata("adminID");
		$this->db->select('notice.*,classes.*,sub_courses.*,notice.status as studentStatus,notice.date as noticeDate');
		$this->db->from('notice');
		$this->db->join('classes', 'classes.ClassesID = notice.classesID', 'LEFT');

		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = notice.sub_coursesID', 'LEFT');


		if ($this->session->userdata('FilterNoticeclassesID')) {
		 $this->db->where('notice.classesID', $this->session->userdata('FilterNoticeclassesID'));
		}

		if ($this->session->userdata('FilterNoticesubCourseID')) {
		 $this->db->where('notice.sub_coursesID', $this->session->userdata('FilterNoticesubCourseID'));
		}

		if ($this->session->userdata('FilterNoticeyearSemesterID')) {
		 $this->db->where('notice.yearsOrSemester', $this->session->userdata('FilterNoticeyearSemesterID'));
		}

	   if ($this->session->userdata('ActiveNotices')) {
		 $this->db->where('notice.status', $this->session->userdata('ActiveNotice'));
		}

	   if ($this->session->userdata('DraftNotice')) {
		 $this->db->where('notice.status', 0);
		}

	   if ($this->session->userdata('TrashNotice')) {
		 $this->db->where('notice.status', $this->session->userdata('TrashNotice'));
		}	


		$this->db->where('notice.adminID', $adminID);


if(isset($_POST["search"]["value"]))  
           {  
 $searches  =  $_POST["search"]["value"];

		$order_column = array( 

                            0 =>'sn', 
                            1 =>'title',
                            2 =>'date',
                            3 => 'classes',
                            4 => 'sub_course',
                            5 => 'yearsOrSemester',
                            6 =>'notice',
                            
                            
                        );
           $where = "(notice.title LIKE '%$searches%' OR classes.classes LIKE '%$searches%' OR sub_courses.sub_course LIKE '%$searches%' OR notice.yearsOrSemester LIKE '%$searches%')";
           $this->db->where($where);
           } 
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('notice.noticeID', 'DESC');  
           } 
           if (isset($_POST["length"])) {
  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           } 
       }

	}


	function get_order_by_notice($array=NULL) {

		$query = parent::get_order_by($array);

		return $query;

	}



	function insert_notice($array) {

		$error = parent::insert($array);

		return $error;

	}

	function notice_x_student($array) {

  if ($array['notice_type']=='course') {
 	 $this->db->where('classesID',$array['classesID']);
 }elseif ($array['notice_type']=='sub_course') {
 	 $this->db->where('classesID',$array['classesID']);
 	 $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 }elseif ($array['notice_type']=='with_year_semester') {
 		 $this->db->where('classesID',$array['classesID']);
 	     $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']); 
 }elseif ($array['notice_type']=='only_year_semester') { 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']);
 }

 $object = $this->db->get('student')->result();

 	 foreach ($object as $key => $value) {

 	 	$data = array(
 	    'studentID' =>$value->studentID,
 	    'noticeID'=>$array['noticeID'] 
 	 	);

 	 	$this->db->insert('notice_x_student',$data);

 	 }
 

	}

	function notice_x_email_student($array) {

  if ($array['notice_type']=='course') {
 	 $this->db->where('classesID',$array['classesID']);
 }elseif ($array['notice_type']=='sub_course') {
 	 $this->db->where('classesID',$array['classesID']);
 	 $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 }elseif ($array['notice_type']=='with_year_semester') {
 		 $this->db->where('classesID',$array['classesID']);
 	     $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']); 
 }elseif ($array['notice_type']=='only_year_semester') { 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']);
 }

 $object = $this->db->get('student')->result();
 	 // foreach ($object as $key => $value) {

 	 // 	$data = array(
 	 //    'studentID' =>$value->studentID,
 	 //    'noticeID'=>$array['noticeID'] 
 	 // 	);

 	 // 	$this->db->insert('notice_x_student',$data);
 	 // }
 
 

	}



	function notice_x_student_update($array) {


$this->db->where('noticeID',$array['noticeID']);
$this->db->delete('notice_x_student');

  if ($array['notice_type']=='course') {
 	 $this->db->where('classesID',$array['classesID']);
 }elseif ($array['notice_type']=='sub_course') {
 	 $this->db->where('classesID',$array['classesID']);
 	 $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 }elseif ($array['notice_type']=='with_year_semester') {
 		 $this->db->where('classesID',$array['classesID']);
 	     $this->db->where('sub_coursesID',$array['sub_coursesID']); 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']); 
 }elseif ($array['notice_type']=='only_year_semester') { 
 	     $this->db->where('yearsOrSemester',$array['yearsOrSemester']);
 }

 $object = $this->db->get('student')->result();
 	 foreach ($object as $key => $value) {

 	 	$data = array(
 	    'studentID' =>$value->studentID,
 	    'noticeID'=>$array['noticeID'] 
 	 	);

 	 	$this->db->insert('notice_x_student',$data);
 	 }
 

	}
	
function delete_notice_x_student($id){
	$this->db->where('noticeID',$id);
    $this->db->delete('notice_x_student');
}



	function getExamByID($id){
		$this->db->where('quid',$id);
		$query  = $this->db->get('ets_quiz');
		 return $query->row();
	}

function notice_icon_count($id){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$id);
		$this->db->where('notice_x_student.iconStatus',1);
		$this->db->limit(10);
	    return	$this->db->get()->num_rows();
}

function notice_read_count($id){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$id);
		$this->db->where('notice_x_student.readStatus',1);
	    return	$this->db->get()->num_rows();
}

function notice_list_icon($id){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$id);
		$this->db->order_by('notice.noticeID','desc');
	    $this->db->limit(6);

	    return	$this->db->get()->result();
}


function notice_list_all($id){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$id);
	    return	$this->db->get()->result();
}

function user_notice_list($loginuserID){
		$this->db->from('notice');
	    $this->db->join('notice_x_user', 'notice_x_user.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_user.userID',$loginuserID);
	    return	$this->db->get()->result();
}




function notice_list_all_ap($id){
	// print_r($id);die;
	    $this->db->select('notice.noticeID,notice.title,notice.date,notice.notice,notice_x_user.userID,notice_x_user.usertype');
		$this->db->from('notice_x_user');
	    $this->db->join('notice', 'notice.noticeID = notice_x_user.noticeID', 'LEFT');
		$this->db->where('notice_x_user.userID',$id);
	    return	$this->db->get()->result();
}

function notice_recent($id){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$id);
		$this->db->order_by('notice.noticeID','DESC');
		$this->db->limit('1');
	    return	$this->db->get()->result();
}

function notice_list_last5($id){
	// print_r($id);die;
	    $this->db->select('notice.noticeID,notice.title,notice.date,notice.notice,notice_x_user.userID,notice_x_user.usertype');
		$this->db->from('notice_x_user');
	    $this->db->join('notice', 'notice.noticeID = notice_x_user.noticeID', 'LEFT');
		$this->db->where('notice_x_user.userID',$id);
		$this->db->order_by('notice.date','desc')->limit(5);
	    return	$this->db->get()->result();
}


function notice_recent_ap($id){
		$this->db->select('notice.noticeID,notice.title,notice.date,notice.notice,notice_x_user.userID,notice_x_user.usertype');
		$this->db->from('notice_x_user');
	    $this->db->join('notice', 'notice.noticeID = notice_x_user.noticeID', 'LEFT');
		$this->db->where('notice_x_user.userID',$id);
		$this->db->order_by('notice.date','desc');
		$this->db->limit('1');
	    return	$this->db->get()->result();
}

function notice_single($studentID,$noticeID){
		$this->db->from('notice');
	    $this->db->join('notice_x_student', 'notice_x_student.noticeID = notice.noticeID', 'LEFT');
		$this->db->where('notice_x_student.studentID',$studentID);
		$this->db->where('notice.noticeID',$noticeID);
	    return	$this->db->get()->row();
}

function update_notice_x_student_read($studentID,$noticeID){	
$data = array(
'readStatus'=>0,
'iconStatus'=>0
);
	$this->db->where('studentID',$studentID);
	$this->db->where('noticeID',$noticeID);
	$this->db->update('notice_x_student',$data);
}

	function Getnotice($adminID,$classesID,$sub_coursesID,$semester_idSelect){

		$this->db->where('adminID',$adminID);
		$this->db->where('classesID',$classesID);
		$this->db->where('sub_coursesID',$sub_coursesID);
		$this->db->where('yearsOrSemester',$semester_idSelect);
		$query = $this->db->get('notice');
		 return $query->result();
	}


	function update_notice($data, $id = NULL) {

		parent::update($data, $id);

		return $id;

	}



	public function delete_notice($id){

		parent::delete($id);

	}

	public function get_join_where_notice($classesID,$sub_coursesID){

	$this->db->select('notice.*,classes.*,sub_courses.*,notice.date as noticeDate');
		$this->db->from('notice');
	    $this->db->join('classes', 'classes.ClassesID = notice.classesID', 'LEFT');
		$this->db->join('sub_courses', 'sub_courses.sub_coursesID = notice.sub_coursesID', 'LEFT');
		$this->db->where('notice.classesID',$classesID);
		$this->db->where('notice.sub_coursesID',$sub_coursesID);

	 return	$this->db->get()->result();


}

}



	


/* End of file notice_m.php */

/* Location: .//D/xampp/htdocs/school/mvc/models/notice_m.php */