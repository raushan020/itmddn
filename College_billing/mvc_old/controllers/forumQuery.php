
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Forum extends Admin_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model("subject_m");
		$this->load->model("student_info_m");
		$this->load->model("parentes_info_m");
		$this->load->model("parentes_m");
		$this->load->model("classes_m");
		$this->load->model("sub_courses");
		$this->load->model("teacher_m");
		$this->load->model("student_m");
		$this->load->model("lms_m");
		$this->load->library('pagination');

		$language = $this->session->userdata('lang');

		$this->lang->load('lms', $language);	

	}

	public function index() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		// if($usertype == "Student" && $usertype != "Accountant") {

			$this->data['student'] = $this->student_info_m->get_student_info();

			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));

			$this->data["subview"] = "lms/forum";

			$this->load->view('_layout_lmsstudent', $this->data);

		// } else {

		// 	$this->data["subview"] = "error";

		// 	$this->load->view('_layout_lmsstudent', $this->data);

		// }

	}



	public function lmsLatest() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				$this->data['subjects'] = $this->lms_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$config["per_page"], $this->data['page']);

				if ($this->input->post('yearSemesterID')) {
			if ($this->input->post('yearSemesterID')=='Select') {
				$this->session->unset_userdata('FilterLmsyearSemesterID');
			}else{
				$this->session->set_userdata('FilterLmsyearSemesterID', $this->input->post('yearSemesterID'));
		  	}

			}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


				$this->data['classesRow'] = $this->classes_m->get_single_classes($student->classesID);
                   $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "lms/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {


			
			  

			// $student = $this->student_info_m->get_student_info();
			$this->data['student'] = $this->student_info_m->get_student_info();

			// $this->data["class"] = $this->student_m->get_class($this->data["student"]->classesID);

			

			if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterlmssubCourseID');
					$this->session->unset_userdata('FilterlmsyearSemesterID');
					$this->session->set_userdata('FilterlmsclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterlmssubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
			  	
					$this->session->set_userdata('FilterlmsyearSemesterID', $this->input->post('yearSemesterID'));
				}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session_latest($this->data["student"]->classesID,$this->data["student"]->sub_coursesID);

				// $this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));

				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data["student"]->classesID);
                   // $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				

			$this->data["subview"] = "lms/lmslatest";

			$this->load->view('_layout_lmsstudent', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_lmsstudent', $this->data);

		}

	}




public function lmscourse() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterlmssubCourseID');
					$this->session->unset_userdata('FilterlmsyearSemesterID');
					$this->session->set_userdata('FilterlmsclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterlmssubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterlmsyearSemesterID', $this->input->post('yearSemesterID'));
				}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
                   $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "lms/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$this->data['student'] = $this->student_info_m->get_student_info();

			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));

			$this->data["subview"] = "lms/lmscourse";

			$this->load->view('_layout_lmsstudent', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_lmsstudent', $this->data);

		}

	}


	public function forum() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterlmssubCourseID');
					$this->session->unset_userdata('FilterlmsyearSemesterID');
					$this->session->set_userdata('FilterlmsclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterlmssubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterlmsyearSemesterID', $this->input->post('yearSemesterID'));
				}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
                   $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "lms/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$this->data['student'] = $this->student_info_m->get_student_info();

			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));

			$this->data["subview"] = "lms/forum";

			$this->load->view('_layout_lmsstudent', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_lmsstudent', $this->data);

		}

	}



	public function document() {

		$usertype = $this->session->userdata("usertype");
		 if($usertype == "ClgAdmin" || $usertype == 'Student') {
		$uri = $this->uri->segment(3);
	

		 $this->db->where('genID',$uri);
		$this->data['recievedata'] = $this->db->get('videos')->row();
		
		$this->data["subview"] = "lms/document";

		$this->load->view('_layout_main', $this->data);
		
		}
	}

	public function AjaxTable(){

	$usertype = $this->session->userdata("usertype");
	$adminID = $this->session->userdata("adminID");
	$loginuserID = $this->session->userdata("loginuserID");
	$totalData = $this->subject_m->get_join_subject_count($adminID);

	$totalFiltered = $totalData;
	$posts = $this->subject_m->make_datatables($adminID);
            // $totalFiltered  = $this->student_m->get_filtered_data();

        $data = array();
        if(!empty($posts))
        {
        	$i = 1;
        	$j= 0;
            foreach ($posts as $post)
            {

				$nestedData['sn'] = $i++;
				$nestedData['subject_code'] = $post->subject_code;
				$nestedData['subject'] = $post->subject;
                $nestedData['classes'] = $post->classes;
                $nestedData['sub_course'] = $post->sub_course;
                $nestedData['yearsOrSemester'] = $post->yearsOrSemester;
                $this->db->where('subjectID',$post->subjectID);
                $query   = $this->db->get('pdf_lms');
                $countpdf  = $query->num_rows();
                $nestedData['countpdf'] = $countpdf;

  if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

   $buttons     = "<input type = 'file' name = 'userfile".$j++."' /><input type = 'hidden' name ='subject_ID[]' value = '".$post->subjectID."' /><input type = 'hidden' name ='subject_name[]' value = '".$post->subject."' />";

   $nestedData['action'] = $buttons;
}
            
                $data[] = $nestedData;
            }
        }

       $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalFiltered, 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
}

	protected function rules() {

		$rules = array(

				array(

					'field' => 'classesID', 

					'label' => $this->lang->line("subject_class_name"), 

					'rules' => 'trim|numeric|required|xss_clean|max_length[11]|callback_allclasses'

				),

				array(

					'field' => 'teacherID', 

					'label' => $this->lang->line("subject_teacher_name"), 

					'rules' => 'trim|required|xss_clean|max_length[60]|callback_allteacher'

				),

				array(

					'field' => 'subject', 

					'label' => $this->lang->line("subject_name"), 

					'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_subject'

				), 

				array(

					'field' => 'subject_author', 

					'label' => $this->lang->line("subject_author"), 

					'rules' => 'trim|xss_clean|max_length[100]'

				), 

				array(

					'field' => 'subject_code', 

					'label' => $this->lang->line("subject_code"),

					'rules' => 'trim|required|max_length[20]|xss_clean|callback_unique_subject_code'

				),

			);

		return $rules;

	}


	public function lmssubject() {		

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata("adminID");

		if($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Teacher"  || $usertype == 'ClgAdmin' || $usertype == "superadmin" || $usertype == "Support") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

				$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

				if ($this->input->post('classesID')) {
					$this->session->unset_userdata('FilterlmssubCourseID');
					$this->session->unset_userdata('FilterlmsyearSemesterID');
					$this->session->set_userdata('FilterlmsclassesID', $this->input->post('classesID'));
				}
		        if ($this->input->post('subCourseID')) {
					$this->session->set_userdata('FilterlmssubCourseID', $this->input->post('subCourseID'));
				}

			  if ($this->input->post('yearSemesterID')) {
					$this->session->set_userdata('FilterlmsyearSemesterID', $this->input->post('yearSemesterID'));
				}

				$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));


				$this->data['classesRow'] = $this->classes_m->get_single_classes($this->session->userdata('FilterSubjectclassesID'));
                   $this->data['subjects'] = $this->subject_m->get_join_subject_count($adminID);	
				$this->data["subview"] = "lms/search";

				$this->load->view('_layout_main', $this->data);


		} elseif($usertype == "Student" && $usertype != "Accountant") {

			$student = $this->student_info_m->get_student_info();


		$config = array();
//$config["base_url"] = base_url() . "home/moreviews/pagination/news";
        $seg = $this->uri->segment(3);
        $config['base_url'] = site_url('lms/index/'.$seg);
        
        $re =   $this->lms_m->getPaginationrow($student->classesID,$student->sub_coursesID);
        
        $config['total_rows'] = $re;
        $config['per_page'] = "10";
        $config["uri_segment"] = 4;
        // print_r($config['total_rows']);
        // exit();
        $choice = ($config["total_rows"] / $config["per_page"]);
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
         $this->data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        //call the model function to get the department data
       // $data['selectquery'] = $this->pagination_model->fetch_data2($config["per_page"], $data['page']);           

        $this->data['pagination'] = $this->pagination->create_links();

			$this->data['subjects'] = $this->lms_m->get_join_where_subject($student->classesID,$student->sub_coursesID,$config["per_page"], $this->data['page']);

			$this->data['student'] = $this->student_info_m->get_student_info();

			$this->data['classesRow'] = $this->classes_m->get_single_classes($this->data["student"]->classesID);

			$this->data['subCourses'] = $this->sub_courses->get_sub_courses_by_session($adminID,$this->session->userdata('FilterSubjectclassesID'));

			$this->data["subview"] = "lms/lmssubject";

			$this->load->view('_layout_lmsstudent', $this->data);

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_lmsstudent', $this->data);

		}

	}



	public function add() {

		$usertype = $this->session->userdata("usertype");
		$adminID = $this->session->userdata('adminID');
		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$this->data['classes'] = $this->student_m->get_classes_by_superAdmin($adminID);

			$this->data['teachers'] = $this->teacher_m->get_teacher();

			if($_POST) {

				$rules = $this->rules();

				$this->form_validation->set_rules($rules);

				if ($this->form_validation->run() == FALSE) { 

					$this->data["subview"] = "subject/add";

					$this->load->view('_layout_main', $this->data);			

				} else {

					$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));

					$array = array(

						"classesID" => $this->input->post("classesID"),

						"teacherID" => $this->input->post("teacherID"),

						"subject" => $this->input->post("subject"),

						"subject_author" => $this->input->post("subject_author"),

						"subject_code" => $this->input->post("subject_code"),

						"teacher_name" => $teacher->name,

						"create_date" => date("Y-m-d h:i:s"),

						"modify_date" => date("Y-m-d h:i:s"),

						"create_userID" => $this->session->userdata('loginuserID'),

						"create_username" => $this->session->userdata('username'),

						"create_usertype" => $this->session->userdata('usertype')

					);

					$this->subject_m->insert_subject($array);

					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					redirect(base_url("subject/index"));

				}

			} else {

				$this->data["subview"] = "subject/add";

				$this->load->view('_layout_main', $this->data);

			}

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function edit() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));

			if((int)$id && (int)$url) {

				$this->data['classes'] = $this->subject_m->get_classes();

				$this->data['teachers'] = $this->teacher_m->get_teacher();

				$this->data['subject'] = $this->subject_m->get_subject($id);

				if($this->data['subject']) {

					$this->data['set'] = $url;

					if($_POST) {

						$rules = $this->rules();

						$this->form_validation->set_rules($rules);

						if ($this->form_validation->run() == FALSE) {

							$this->data['form_validation'] = validation_errors(); 

							$this->data["subview"] = "subject/edit";

							$this->load->view('_layout_main', $this->data);			

						} else {

							$teacher = $this->teacher_m->get_teacher($this->input->post("teacherID"));

							$array = array(

								"classesID" => $this->input->post("classesID"),

								"teacherID" => $this->input->post("teacherID"),

								"subject" => $this->input->post("subject"),

								"subject_author" => $this->input->post("subject_author"),

								"subject_code" => $this->input->post("subject_code"),

								"teacher_name" => $teacher->name,

								"modify_date" => date("Y-m-d h:i:s")

							);

							$this->subject_m->update_subject($array, $id);

							$this->session->set_flashdata('success', $this->lang->line('menu_success'));

							redirect(base_url("subject/index/$url"));

						}

					} else {

						$this->data["subview"] = "subject/edit";

						$this->load->view('_layout_main', $this->data);

					}

				} else {

					$this->data["subview"] = "error";

					$this->load->view('_layout_main', $this->data);

				}

			} else {

				$this->data["subview"] = "error";

				$this->load->view('_layout_main', $this->data);

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function pdf(){
		
	$subjectID =  $this->input->post('subject_ID');
	$subject_name =  $this->input->post('subject_name');
	// print_r($_FILES);
	// exit();
	for ($i=0; $i<count($this->input->post('subject_ID')); $i++) { 
		//print_r($i);
		// print_r($_FILES['userfile'.$i]['name']);
		// exit();

	if ($_FILES['userfile'.$i]['name']!='') {
	
	$config['upload_path'] = 'assets/pdfs';
	$config['allowed_types'] = 'pdf';
	$config['max_size'] = '5120';
	$this->load->library('upload', $config);
	$this->upload->do_upload('userfile'.$i);
	$image_data = $this->upload->data();

	$data = array(
	"subjectID"=>$subjectID[$i],
	"title"=>$subject_name[$i],
	"pdf"=>$image_data['file_name'],
	  );
	  $query = $this->db->insert('pdf_lms',$data);
	}
	}

	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	 redirect('lms/index');
		// $this->session->set_flashdata('error', 'Problem');
		// redirect('lms/index');
	}

	public function delete() {

		$usertype = $this->session->userdata("usertype");

		if($usertype == "Admin" || $usertype == 'ClgAdmin' || $usertype == "superadmin") {

			$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

			$url = htmlentities(mysql_real_escape_string($this->uri->segment(4)));

			if((int)$id && (int)$url) {

				$this->subject_m->delete_subject($id);

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				redirect(base_url("subject/index/$url"));

			} else {

				redirect(base_url("subject/index"));

			}	

		} else {

			$this->data["subview"] = "error";

			$this->load->view('_layout_main', $this->data);

		}

	}

	public function unique_subject() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			$subject = $this->subject_m->get_order_by_subject(array("subject" => $this->input->post("subject"), "subjectID !=" => $id, "classesID" => $this->input->post("classesID")));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$subject = $this->subject_m->get_order_by_subject(array("subject" => $this->input->post("subject"), "classesID" => $this->input->post("classesID"), "subject_code" => $this->input->post("subject_code")));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject", "%s already exists");

				return FALSE;

			}

			return TRUE;
		}	

	}

	public function unique_subject_code() {

		$id = htmlentities(mysql_real_escape_string($this->uri->segment(3)));

		if((int)$id) {

			$subject = $this->subject_m->get_order_by_subject(array("subject_code" => $this->input->post("subject_code"), "subjectID !=" => $id));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject_code", "%s already exists");

				return FALSE;

			}

			return TRUE;

		} else {

			$subject = $this->subject_m->get_order_by_subject(array("subject_code" => $this->input->post("subject_code")));

			if(count($subject)) {

				$this->form_validation->set_message("unique_subject_code", "%s already exists");

				return FALSE;

			}

			return TRUE;
		}	
	}

	public function subject_list() {

		$classID = $this->input->post('id');

		if((int)$classID) {

			$string = base_url("subject/index/$classID");

			echo $string;

		} else {

			redirect(base_url("subject/index"));

		}

	}

	public function student_list() {

		$studentID = $this->input->post('id');

		if((int)$studentID) {

			$string = base_url("subject/index/$studentID");

			echo $string;

		} else {

			redirect(base_url("subject/index"));

		}

	}

function ResetCourses(){

	$this->session->unset_userdata('FilterSubjectclassesID');
	   
}

function ResetSubcourses(){

	$this->session->unset_userdata('FilterSubjectsubCourseID');
	   
}

function ResetSemesterYear(){

	$this->session->unset_userdata('FilterSubjectyearSemesterID');
	$this->session->unset_userdata('FilterLmsyearSemesterID');	   
}
function ResetAllfilter(){

	$this->session->unset_userdata('FilterSubjectsubCourseID');
	$this->session->unset_userdata('FilterSubjectclassesID');
	$this->session->unset_userdata('FilterSubjectyearSemesterID');
	   
}

function allclasses() {

		if($this->input->post('classesID') == 0) {

			$this->form_validation->set_message("allclasses", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

	public function allteacher() {

		if($this->input->post('teacherID') === '0') {

			$this->form_validation->set_message("allteacher", "The %s field is required");

	     	return FALSE;

		}

		return TRUE;

	}

}

/* End of file subject.php */

/* Location: .//D/xampp/htdocs/school/mvc/controllers/subject.php */